package info.scce.dime.ui.wizard;

import info.scce.dime.ui.wizard.pages.MainPage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.osgi.framework.Bundle;

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;

/**
 * @author kopetzki
 *
 */
public class DIMEAppWizard extends Wizard implements INewWizard {

	private final String EXAMPLE_BUNDLE_ID = "info.scce.dime.examples";
	private final String EMPTY_PROJECT_PATH = "/emptyproject/";
	
	
	MainPage main = new MainPage("New DIME Application");
	
	public DIMEAppWizard() {
		
	}

	@Override
	public void addPages() {
		addPage(main);
		super.addPages();
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("New DIME Application");
	}
	
	@Override
	public boolean performFinish() {
		String projectName = main.getProjectName();
		if (projectName == null || projectName.isEmpty()) {
			projectName = main.getSelectedExampleProjectName();
		}
		if (projectName == null || projectName.isEmpty()) {
			projectName = "NewApp";
		}
		System.out.println("project.name: " + projectName);
		
		IProject project = createProject(projectName);
		if (project == null) {
			return true;
		}
		
		URL projectURL = getEmptyProjectURL();
		if (main.isCreateExample()) {
			projectURL = main.getSelectedExampleProjectURL();
		}
		System.out.println("project.url: " + projectURL);
		
		copyFiles(project, projectURL);
		return true;
	}
	
	private URL getEmptyProjectURL() {
		Bundle bundle = Platform.getBundle(EXAMPLE_BUNDLE_ID);
		if (bundle != null) {
			final Enumeration<URL> entries = bundle.findEntries(EMPTY_PROJECT_PATH, "*", false);
			if (entries.hasMoreElements()) {
				URL url = entries.nextElement();
				System.out.println("URL: " + url);
				return url;
			}
		}
		return null;
	}

	private IProject createProject(String projectName) {
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		
		if (root == null) throw new RuntimeException("Workspace Root is null...");
		
		final NullProgressMonitor monitor = new NullProgressMonitor();

		List<String> srcFolders = new ArrayList<String>();
		srcFolders.add("src");
		Set<String> reqBundle = new HashSet<String>();
		reqBundle.add("org.eclipse.core.runtime");
		
		IProject project = ProjectCreator.createProject(projectName, 
				srcFolders, 
				Collections.emptyList(), 
				reqBundle, 
				Collections.emptyList(), 
				Collections.emptyList(), 
				monitor, 
				true);
		
		return project;
	}

	private void copyFiles(IProject target, URL selectedProjectURL) {
		File destinationFile = target.getLocation().toFile();
		File sourceFile = getSourceFile(selectedProjectURL);
		copy(sourceFile, destinationFile);
		try {
			target.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	

	private void copy(File sourceFile, File destinationFile) {
		byte[] buffer = new byte[1000];   
		try {
			if (sourceFile.isDirectory()) {
				destinationFile.mkdir();
				if (!destinationFile.exists()) {
						destinationFile.createNewFile();
				}
				for (File file : sourceFile.listFiles()) {
					File newDest = new File(destinationFile, file.getName());
					copy(file, newDest);
				}
			} else {
				if (!destinationFile.exists() || destinationFile.getName().equals(".project")) {
					destinationFile.createNewFile();
					BufferedInputStream fis = new BufferedInputStream(new FileInputStream(sourceFile));
					BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(destinationFile));
					int count;
					while ((count = fis.read(buffer)) != -1) {
						fos.write(buffer, 0 , count);
					}
					fis.close();
					fos.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private File getSourceFile(URL selectedProjectURL) {
		try {
			Path path = new Path(FileLocator.toFileURL(selectedProjectURL).toString());
			URL url = FileLocator.toFileURL(selectedProjectURL);
			URI uri = URI.createFileURI(url.getPath());
			return new File(uri.toFileString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
