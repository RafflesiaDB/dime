package info.scce.dime.ui.wizard

import de.jabc.cinco.meta.core.utils.job.JobFactory
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import info.scce.dime.process.factory.ProcessFactory
import info.scce.dime.process.process.ProcessType
import info.scce.dime.ui.wizard.pages.NativeFrontendSIBLibraryWizardPage
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.wizard.Wizard
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.INewWizard
import org.eclipse.ui.IWorkbench

class NativeFrontendSIBLibraryWizard extends Wizard implements INewWizard {
	private NativeFrontendSIBLibraryWizardPage page;

	protected extension WorkbenchExtension = new WorkbenchExtension

	private IStructuredSelection ssel;

	override performFinish() {
		val job = JobFactory.job("Creating NFSIB Library")

		val root = ResourcesPlugin.getWorkspace().getRoot();
		val containerResource = root.getContainerForLocation(new Path(page.direcory))
		val p = ProcessFactory.eINSTANCE.createProcess().newProcess(containerResource.fullPath.toOSString,
			page.fileName.replaceAll(".nfsl.process", "") + ".nfsl", false)
		p.processType = ProcessType.NATIVE_FRONTEND_SIB_LIBRARY
		p.modelName = page.fileName
		p.transact [
			val sib = p.newNativeFrontendSIBLibrary(0, 0, 1024, 800)
			sib.className = "dart-classes/" + p.modelName + ".dart"
			if (page.checkIsREST) {
				val service = new ImportRESTService
				sib.rootUrl = page.rootURL;
				job.consume(100).task(service.execute(sib, page.jarFile)).onDone([display.syncExec([p.openEditor])]).
					schedule
			} else {
				p.save
				display.syncExec([p.openEditor])
			}
		]

		return true;
	}

	override init(IWorkbench workbench, IStructuredSelection selection) {
		ssel = selection;
	}

	override addPages() {
		page = new NativeFrontendSIBLibraryWizardPage("newProcess");
		addPage(page);

		super.addPages();
	}

	def IStructuredSelection getSelection() {
		return this.ssel
	}

	override createPageControls(Composite pageContainer) {
		super.createPageControls(pageContainer)
	}

	override canFinish() {
		return page.pageComplete
	}

}
