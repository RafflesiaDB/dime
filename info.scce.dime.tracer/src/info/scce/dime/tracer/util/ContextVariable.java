package info.scce.dime.tracer.util;

public class ContextVariable {
	private String id = null;
	private Class<?> type = null;
	private String name = "";
	private Object value = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Class<?> getType() {
		return type;
	}
	public void setType(Class<?> type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		String output = "";
		output += name + ": ";
		output += "(" + type.toString() + ") ";
		output += value.toString();
		return output;
	}
}
