package info.scce.dime.tracer.util;

import java.util.Stack;

import info.scce.dime.tracer.interfaces.ModelAdapter;
import info.scce.dime.tracer.interfaces.TracerUIAdapter;

public class TracerEnvironment {

	private Stack<TracerContext> callStack = new Stack<>();
	private TracerContext activeContext = null;

	private TracerUIAdapter uiAdapter = null;

	public TracerEnvironment(TracerUIAdapter uiAdapter) {
		super();
		this.uiAdapter = uiAdapter;
	}

	public Stack<TracerContext> getCallStack() {
		return callStack;
	}

	public TracerContext getActiveContext() {
		return activeContext;
	}

	public TracerUIAdapter getUiAdapter() {
		return uiAdapter;
	}

	public boolean isRunning() {
		return !callStack.isEmpty();
	}

	public void initializeRun(ModelAdapter modelAdapter) throws Exception {
		TracerContext initContext = new TracerContext(modelAdapter);
		initContext.initializeRun();
		callStack.push(initContext);
		activeContext = initContext;
		uiAdapter.postInitialize(this);
	}

	public void executeNextStep(boolean stepInto) throws Exception {
		if (callStack.size() <= 0)
			return;
		
		activeContext = callStack.pop();

		if (activeContext.isRunning()) {
			uiAdapter.preExecute(this);
			activeContext.executeNextStep(this, stepInto);
			uiAdapter.postExecute(this);
		}
	}

	public void reset() {
		callStack = new Stack<TracerContext>();
		activeContext = null;
	}

	public TracerContext getNextContext() {
		if (!callStack.isEmpty())
			return callStack.peek();
		return null;
	}

}
