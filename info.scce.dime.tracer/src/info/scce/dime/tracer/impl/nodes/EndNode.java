package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.EndSIB;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

import java.util.ArrayList;
import java.util.List;

public class EndNode extends DataFlowTargetNode<EndSIB> {

	public EndNode(EndSIB sib) {
		super(sib);
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		adapter = (ProcessModelAdapter) env.getActiveContext()
				.getModelAdapter();

		TracerContext context = env.getActiveContext();

		List<ContextVariable> outputs = new ArrayList<ContextVariable>();
		outputs.addAll(mapInputPort(context));
		outputs.addAll(mapInputStatic(context));

		context.setOutputs(outputs);
		context.setExitBranch(sib.getBranchName());
		return null;
	}


}
