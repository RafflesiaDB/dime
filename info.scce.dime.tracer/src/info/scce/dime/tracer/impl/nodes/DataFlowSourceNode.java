package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.impl.ProcessTypeMapper;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

public class DataFlowSourceNode<T extends DataFlowSource> implements TracerNode {

	protected ProcessModelAdapter adapter = null;
	protected T sib = null;

	public T getSib() {
		return sib;
	}

	public DataFlowSourceNode(T sib) {
		super();
		this.sib = sib;
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	protected void writeComplex(TracerContext context,
			ComplexOutputPort cOutput, ContextVariable value) throws Exception {
		// TODO implement DyWa types in StartNode
		throw new RuntimeException("Not implemented yet!");
	}

	protected void writePrimitive(TracerContext context,
			PrimitiveOutputPort pOutput, ContextVariable value)
			throws Exception {
		Class<?> outputClass = ProcessTypeMapper
				.getClassForPrimitiveOutputPort(pOutput);
		if (outputClass == null)
			throw new Exception("Class for OutputPort not found!");

		if (pOutput.getPrimitiveVariableSuccessors().size() <= 0)
			return;

		PrimitiveVariable pVar = pOutput.getPrimitiveVariableSuccessors()
				.get(0);
		ContextVariable cv = context.getById(pVar.getId());

		if (!outputClass.equals(cv.getType()))
			throw new Exception("Type missmatch: [OutputPort] "
					+ pOutput.getName()
					+ " ("
					+ ProcessTypeMapper.getPrimitiveTypeForClass(outputClass)
							.getLiteral()
					+ ") vs. [context] "
					+ cv.getName()
					+ " ("
					+ ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType())
							.getLiteral() + ")");

		if (value.getType() != null && !value.getType().equals(outputClass))
			throw new Exception("Type missmatch: "
					+ value.getName()
					+ " ("
					+ ProcessTypeMapper.getPrimitiveTypeForClass(
							value.getType()).getLiteral()
					+ ") vs. [OutputPort] "
					+ pOutput.getName()
					+ " ("
					+ ProcessTypeMapper.getPrimitiveTypeForClass(outputClass)
							.getLiteral() + ")");

		cv.setValue(value.getValue());
	}

}
