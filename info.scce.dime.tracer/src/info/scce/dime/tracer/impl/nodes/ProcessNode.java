package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.ProcessSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.impl.TracerDummyUIAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

import java.util.ArrayList;
import java.util.List;

public class ProcessNode extends DataFlowTargetNode<ProcessSIB> {

	private TracerContext subContext = null;

	public ProcessNode(ProcessSIB sib) {
		super(sib);
	}

	public TracerNode stepInto(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();

		Process processModel = (Process) sib.getProMod();

		if (processModel == null)
			throw new Exception(
					"Could not find ProcessModel (EObject) for ProcessSib!");

		ProcessModelAdapter subAdapter = new ProcessModelAdapter();
		subAdapter.setModel(processModel);

		List<ContextVariable> inputParameters = new ArrayList<ContextVariable>();
		inputParameters.addAll(mapInputPort(context));
		inputParameters.addAll(mapInputStatic(context));

		subContext = new TracerContext(subAdapter);
		subContext.initializeRun();
		subContext.setInputs(inputParameters);

		env.getCallStack().push(context);
		env.getCallStack().push(subContext);

		return this;
	}

	private TracerNode stepOut(TracerEnvironment env) throws Exception {
		TracerNode nextNode = null;
		TracerContext context = env.getActiveContext();

		String branchName = subContext.getExitBranch();
		Branch branch = getBranchForBranchName(branchName);

		if (branch == null)
			throw new Exception("Could not find branch '" + branchName + "'!");

		BranchNode bn = new BranchNode(branch);
		nextNode = bn.executeAfterProcessSib(env, subContext.getOutputs());

		env.getCallStack().push(context);
		return nextNode;
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		adapter = (ProcessModelAdapter) env.getActiveContext()
				.getModelAdapter();

		if (subContext != null)
			return stepOut(env);
		if (stepInto)
			return stepInto(env);

		TracerNode nextNode = null;
		TracerContext context = env.getActiveContext();

		TracerContext subContext = null;
		String branchName = null;

		Process processModel = (Process) sib.getProMod();

		if (processModel == null)
			throw new Exception(
					"Could not find ProcessModel (EObject) for ProcessSib!");

		List<ContextVariable> inputParameters = new ArrayList<ContextVariable>();
		inputParameters.addAll(mapInputPort(context));
		inputParameters.addAll(mapInputStatic(context));

		try {
			subContext = invokeProcessSIB(processModel, env, inputParameters);
			branchName = subContext.getExitBranch();
		} catch (Exception e) {
			branchName = "error";
		}

		Branch branch = getBranchForBranchName(branchName);

		if (branch == null)
			throw new Exception("Could not find branch '" + branchName + "'!");

		BranchNode bn = new BranchNode(branch);
		nextNode = bn.executeAfterProcessSib(env, subContext.getOutputs());

		env.getCallStack().push(context);
		return nextNode;
	}

	private TracerContext invokeProcessSIB(Process processModel,
			TracerEnvironment env, List<ContextVariable> inputParameters)
			throws Exception {

		ProcessModelAdapter subAdapter = new ProcessModelAdapter();
		subAdapter.setModel(processModel);

		TracerEnvironment subEnv = new TracerEnvironment(
				new TracerDummyUIAdapter());
		subEnv.initializeRun(subAdapter);

		TracerContext newContext = subEnv.getActiveContext();
		newContext.setInputs(inputParameters);

		while (subEnv.isRunning())
			subEnv.executeNextStep(false);

		return newContext;
	}

	private Branch getBranchForBranchName(String branchName) {
		for (Branch branch : sib.getSuccessors(Branch.class)) {
			if (branch.getName().equals(branchName)) {
				return branch;
			}
		}
		return null;
	}
}
