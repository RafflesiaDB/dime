package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.InputPort;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.impl.ProcessTypeMapper;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

import java.util.ArrayList;
import java.util.List;

public abstract class DataFlowTargetNode<T extends DataFlowTarget> implements
		TracerNode {

	protected ProcessModelAdapter adapter = null;
	protected T sib = null;

	public T getSib() {
		return sib;
	}

	public DataFlowTargetNode(T sib) {
		super();
		this.sib = sib;
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	protected List<ContextVariable> mapInputStatic(TracerContext context)
			throws Exception {
		List<ContextVariable> outputs = new ArrayList<ContextVariable>();
		for (InputStatic input : sib.getInputStatics()) {
			boolean setDone = false;

			Class<?> inputClass = ProcessTypeMapper
					.getClassForInputStatic(input);
			if (inputClass == null)
				throw new Exception("Class for InputPort not found!");

			ContextVariable inputCV = new ContextVariable();
			inputCV.setName(input.getName());
			inputCV.setType(inputClass);
			inputCV.setValue(adapter.getValueForInputStatic(input));
			outputs.add(inputCV);

			setDone = true;

			if (!setDone)
				throw new Exception("No match for [output]: " + input.getName());
		}
		return outputs;
	}

	protected List<ContextVariable> mapInputPort(TracerContext context)
			throws Exception {
		List<ContextVariable> outputs = new ArrayList<ContextVariable>();
		for (InputPort input : sib.getInputPorts()) {
			boolean setDone = false;

			if (input instanceof PrimitiveInputPort) {
				Class<?> inputClass = ProcessTypeMapper
						.getClassForPrimitiveInputPort((PrimitiveInputPort) input);
				if (inputClass == null)
					throw new Exception("Class for InputPort not found!");

				for (PrimitiveVariable pVar : ((PrimitiveInputPort) input)
						.getPrimitiveVariablePredecessors()) {
					ContextVariable cv = context.getById(pVar.getId());

					if (!inputClass.equals(cv.getType()))
						throw new Exception("Type missmatch: [OutputPort] "
								+ input.getName()
								+ " ("
								+ ProcessTypeMapper.getPrimitiveTypeForClass(
										inputClass).getLiteral()
								+ ") vs. [context] "
								+ cv.getName()
								+ " ("
								+ ProcessTypeMapper.getPrimitiveTypeForClass(
										cv.getType()).getLiteral() + ")");

					ContextVariable inputCV = new ContextVariable();
					inputCV.setName(input.getName());
					inputCV.setType(inputClass);
					inputCV.setValue(cv.getValue());
					outputs.add(inputCV);

					setDone = true;
				}
			}

			if (input instanceof ComplexInputPort) {
				// TODO implement DyWa types in StartNode
				throw new RuntimeException("Not implemented yet!");
			}

			if (!setDone)
				throw new Exception("No match for [output]: " + input.getName());
		}
		return outputs;
	}

}
