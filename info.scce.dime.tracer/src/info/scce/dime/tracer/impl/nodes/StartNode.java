package info.scce.dime.tracer.impl.nodes;

import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.StartSIB;
import info.scce.dime.tracer.impl.ProcessModelAdapter;
import info.scce.dime.tracer.interfaces.TracerNode;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

public class StartNode extends DataFlowSourceNode<StartSIB> {

	public StartNode(StartSIB sib) {
		super(sib);
	}

	@Override
	public TracerNode execute(TracerEnvironment env, boolean stepInto)
			throws Exception {
		adapter = (ProcessModelAdapter) env.getActiveContext()
				.getModelAdapter();

		TracerNode nextNode = null;
		TracerContext context = env.getActiveContext();

		if (context.getInputs() == null)
			return null;

		for (Output output : sib.getOutputs()) {
			for (ContextVariable input : context.getInputs()) {
				if (output.getName().equals(input.getName())) {
					if (output instanceof PrimitiveOutputPort) {
						writePrimitive(context, (PrimitiveOutputPort) output,
								input);
					}
					if (output instanceof ComplexOutputPort) {
						writeComplex(context, (ComplexOutputPort) output, input);
					}
				}
			}
		}

		for (DataFlowTarget successor : sib.getDataFlowTargetSuccessors()) {
			nextNode = adapter.createTracerNode(successor);
		}
		
		env.getCallStack().push(context);
		return nextNode;
	}

}
