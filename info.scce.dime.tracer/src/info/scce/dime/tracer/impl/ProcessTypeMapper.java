package info.scce.dime.tracer.impl;

import info.scce.dime.process.process.BooleanInputStatic;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.IntegerInputStatic;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.process.process.RealInputStatic;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProcessTypeMapper {

	public static final String integerClass = "java.lang.Long";
	public static final String booleanClass = "java.lang.Boolean";
	public static final String realClass = "java.lang.Double";
	public static final String textClass = "java.lang.String";
	public static final String timestampClass = "java.util.Date";

	public static Class<?> getClassForPrimitiveType(PrimitiveType type)
			throws ClassNotFoundException {
		switch (type) {
		case BOOLEAN:
			return Class.forName(booleanClass);
		case INTEGER:
			return Class.forName(integerClass);
		case REAL:
			return Class.forName(realClass);
		case TEXT:
			return Class.forName(textClass);
		case TIMESTAMP:
			return Class.forName(timestampClass);
		default:
			break;
		}
		return null;
	}
	
	public static PrimitiveType getPrimitiveTypeForClass(Class<?> classValue) {
		String classString = classValue.getName();
		switch (classString) {
		case integerClass:
			return PrimitiveType.INTEGER;
		case booleanClass:
			return PrimitiveType.BOOLEAN;
		case realClass:
			return PrimitiveType.REAL;
		case textClass:
			return PrimitiveType.TEXT;
		case timestampClass:
			return PrimitiveType.TIMESTAMP;
		default:
			break;
		}
		return null;
	}
	
	public static String getInputPattern(PrimitiveType type) {
		switch (type) {
		case BOOLEAN:
			return "true/false";
		case INTEGER:
			return "e.g. 1234";
		case REAL:
			return "e.g. 12.34";
		case TEXT:
			return "e.g. hello world";
		case TIMESTAMP:
			return "e.g. 23/07/2015";
		default:
			break;
		}
		return null;
	}
	
	public static Class<?> getClassForPrimitiveVariable(PrimitiveVariable pVar)
			throws ClassNotFoundException {
		PrimitiveType type = pVar.getDataType();
		return getClassForPrimitiveType(type);
	}

	public static Class<?> getClassForPrimitiveOutputPort(
			PrimitiveOutputPort outPort) throws ClassNotFoundException {
		PrimitiveType type = outPort.getDataType();
		return getClassForPrimitiveType(type);
	}

	public static Class<?> getClassForPrimitiveInputPort(
			PrimitiveInputPort inPort) throws ClassNotFoundException {
		PrimitiveType type = inPort.getDataType();
		return getClassForPrimitiveType(type);
	}

	public static Class<?> getClassForInputStatic(InputStatic input)
			throws ClassNotFoundException {
		if (input instanceof IntegerInputStatic)
			return getClassForPrimitiveType(PrimitiveType.INTEGER);
		if (input instanceof BooleanInputStatic)
			return getClassForPrimitiveType(PrimitiveType.BOOLEAN);
		if (input instanceof RealInputStatic)
			return getClassForPrimitiveType(PrimitiveType.REAL);
		if (input instanceof TextInputStatic)
			return getClassForPrimitiveType(PrimitiveType.TEXT);
		if (input instanceof TimestampInputStatic)
			return getClassForPrimitiveType(PrimitiveType.TIMESTAMP);
		return null;
	}

	public static Object convertInput(Class<?> type, String value)
			throws NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			ClassNotFoundException, ParseException {
		Object output = null;
		Constructor<?> constructor = type.getConstructor(value.getClass());
		output = constructor.newInstance(value);

		if (type.equals(getClassForPrimitiveType(PrimitiveType.TIMESTAMP))) {
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			output = (Date) formatter.parse(value);
		}

		if (type.equals(getClassForPrimitiveType(PrimitiveType.BOOLEAN))) {
			if (value.toLowerCase().trim().equals("true"))
				output = new Boolean(true);
			else if (value.toLowerCase().trim().equals("false"))
				output = new Boolean(false);
			else
				throw new ParseException("Please enter true/false", 0);
		}

		return output;
	}
}
