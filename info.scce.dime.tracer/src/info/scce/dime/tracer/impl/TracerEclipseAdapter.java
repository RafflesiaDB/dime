package info.scce.dime.tracer.impl;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import info.scce.dime.tracer.interfaces.ModelAdapter;
import info.scce.dime.tracer.interfaces.TracerUIAdapter;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;
import info.scce.dime.tracer.views.TracerView;

public class TracerEclipseAdapter implements TracerUIAdapter {

	private TracerView tracerView = null;

	public TracerEclipseAdapter(TracerView tracerView) {
		super();
		this.tracerView = tracerView;
	}

	public TracerView getTracerView() {
		return tracerView;
	}

	private List<ContextVariable> getInputs(List<ContextVariable> inputs) {
		for (ContextVariable cv : inputs) {
			boolean done = false;
			
			String msg = "Please enter value for parameter '" + cv.getName()
					+ "' (" + ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()).getLiteral() + "), " + ProcessTypeMapper.getInputPattern(ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()));

			while (!done) {
				String valueString = openInputDialog("Parameter Input", msg);

				if (valueString == null)
					return null;

				try {
					Object value = ProcessTypeMapper.convertInput(cv.getType(),
							valueString);
					cv.setValue(value);
					done = true;
				} catch (Exception e) {
					MessageDialog.openError(tracerView.getParent().getShell(),
							"Error", "Input must be of type "
									+ cv.getType().getSimpleName());
				}
			}
		}
		return inputs;
	}

	private void showOutputs(List<ContextVariable> outputs) {
		String output = "";
		for (ContextVariable cv : outputs) {
			String value = "null";
			if (cv.getValue() != null)
				value = cv.getValue().toString();

			output += cv.getName() + ": (" + ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()).getLiteral()
					+ ") " + value + "\n";
		}
		MessageDialog.openInformation(tracerView.getParent().getShell(),
				"Output of execution", output);
	}

	private String openInputDialog(String title, String message) {
		InputDialog inputDialog = new InputDialog(tracerView.getParent()
				.getShell(), title, message, "", null);
		if (inputDialog.open() != Window.OK) {
			return null;
		}
		return inputDialog.getValue();
	}
	
	@Override
	public void postInitialize(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();
		
		if (context != null) {
			openEditor(context.getModelAdapter());
			context.getModelAdapter().highlight(context.getActiveNode());
		}
		System.out.println("> initializeRun...");
	}

	@Override
	public void preExecute(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();
		
		System.out.println("- " + context.getModelAdapter().getModelName() + ": " + context.getActiveNode().getClass().getSimpleName());
		
		openEditor(context.getModelAdapter());
		
		if (env.getActiveContext().getInputs() == null) {
			List<ContextVariable> inputs = context.getModelAdapter().createInputList();
			inputs = this.getInputs(inputs);
			env.getActiveContext().setInputs(inputs);
		}
	}

	@Override
	public void postExecute(TracerEnvironment env) {
		TracerContext context = env.getActiveContext();
		TracerContext nextContext = env.getNextContext();
		
		if (nextContext != null) {
			ModelAdapter modelAdapter = nextContext.getModelAdapter();
			IEditorPart editorPart = openEditor(modelAdapter);
			try {
				setModel(editorPart, modelAdapter);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			nextContext.getModelAdapter().highlight(nextContext.getActiveNode());
		}
		
		if (nextContext == null && context.getOutputs() != null) {
			showOutputs(env.getActiveContext().getOutputs());
			System.out.println("< Execution finished!");
		}
	}
	
	public void setModel(IEditorPart editorPart, ModelAdapter adapter) throws Exception {
		IFile file = (IFile) editorPart.getEditorInput()
				.getAdapter(IFile.class);

		Resource res = null;

		if (editorPart instanceof DiagramEditor) {
			DiagramEditor deditor = (DiagramEditor) editorPart;
			TransactionalEditingDomain ed = deditor.getEditingDomain();
			ResourceSet rs = ed.getResourceSet();
			res = rs.getResources().get(0);
		}
		adapter.setModel(res, file.getRawLocation().makeAbsolute().toFile());
	}
	
	private IEditorPart openEditor(ModelAdapter modelAdapter) {
		URI uri = EcoreUtil.getURI(modelAdapter.getModelEObject());
		IFile iFile = ResourcesPlugin.getWorkspace().getRoot()
				.getFile(new Path(uri.toPlatformString(true)));
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		try {
			return IDE.openEditor(page, iFile);
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		return null;
	}

	

}
