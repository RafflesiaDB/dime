package info.scce.dime.tracer.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import info.scce.dime.tracer.interfaces.TracerUIAdapter;
import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;
import info.scce.dime.tracer.util.TracerEnvironment;

public class TracerConsoleAdapter implements TracerUIAdapter {

	@Override
	public void preExecute(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();

		System.out.println("- " + context.getModelAdapter().getModelName()
				+ ": " + context.getActiveNode().getClass().getSimpleName());

		if (env.getActiveContext().getInputs() == null) {
			List<ContextVariable> inputs = context.getModelAdapter()
					.createInputList();
			inputs = this.getInputs(inputs);
			env.getActiveContext().setInputs(inputs);
		}

	}

	@Override
	public void postExecute(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();
		TracerContext nextContext = env.getNextContext();
		
		if (nextContext == null && context.getOutputs() != null) {
			System.out.println("< Execution finished!");
			showOutputs(env.getActiveContext().getOutputs());
		}
	}

	@Override
	public void postInitialize(TracerEnvironment env) throws Exception {
		TracerContext context = env.getActiveContext();
		System.out.println("> initializing model '" + context.getModelAdapter().getModelName() + "' ...");
	}
	
	
	private List<ContextVariable> getInputs(List<ContextVariable> inputs) {
		for (ContextVariable cv : inputs) {
			boolean done = false;

			while (!done) {
				String msg = "Please enter value for parameter '" + cv.getName()
						+ "' (" + ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()).getLiteral() + "), " + ProcessTypeMapper.getInputPattern(ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()));

				
				System.out.println(msg);
				
				try {
					BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
					String valueString=buffer.readLine();

					if (valueString == null)
						return null;
					
					Object value = ProcessTypeMapper.convertInput(cv.getType(),
							valueString);
					cv.setValue(value);
					done = true;
				} catch (Exception e) {
					System.err.println(e.getClass().getSimpleName() + ": " + e.getMessage());
				}
			}
		}
		return inputs;
	}

	private void showOutputs(List<ContextVariable> outputs) {
		if (outputs.size() <= 0)
			return;
		
		String output = "Results: \n";
		for (ContextVariable cv : outputs) {
			String value = "null";
			if (cv.getValue() != null)
				value = cv.getValue().toString();

			output += cv.getName() + ": (" + ProcessTypeMapper.getPrimitiveTypeForClass(cv.getType()).getLiteral()
					+ ") " + value + "\n";
		}
		System.out.println(output);
	}

}
