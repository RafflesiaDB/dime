package info.scce.dime.tracer.interfaces;

import info.scce.dime.tracer.util.TracerEnvironment;

public interface TracerUIAdapter {
	
	public void preExecute(TracerEnvironment env) throws Exception;
	
	public void postExecute(TracerEnvironment env) throws Exception;
	
	public void postInitialize(TracerEnvironment env) throws Exception;
	
}
