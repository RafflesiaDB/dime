package info.scce.dime.tracer.interfaces;

import java.io.File;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import info.scce.dime.tracer.util.ContextVariable;
import info.scce.dime.tracer.util.TracerContext;

public interface ModelAdapter {

	public String getModelName();

	public File getModelFile();

	public EObject getModelEObject();

	public void highlight(TracerNode node);

	public TracerNode getStartNode();

	public void createContextVariables(TracerContext context) throws Exception;

	public List<ContextVariable> createInputList() throws Exception;

	public void readModel(File arg0);

	public void setModel(Resource resource, File file) throws Exception;

}
