package info.scce.dime.tracer.interfaces;

import info.scce.dime.tracer.util.TracerEnvironment;

public interface TracerNode {

	public TracerNode execute(TracerEnvironment env, boolean stepInto) throws Exception;
	
}
