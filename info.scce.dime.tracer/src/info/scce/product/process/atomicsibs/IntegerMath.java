package info.scce.product.process.atomicsibs;

public class IntegerMath {

	public static enum numberType {ODD, EVEN};
	
	public static Long add(Long num1, Long num2) {
		return num1 + num2;
	}
	
	public static Long mult(Long num1, Long num2) {
		return num1 * num2;
	}
	
	public static Long negate(Long num) {
		return num * -1;
	}
	
	public static boolean equals(Long num1, Long num2) {
		return num1 == num2;
	}
	
	public static boolean leq(Long num1, Long num2) {
		return num1 <= num2;
	}
	
	public static numberType oddeven(Long number) {
		if (number%2 == 0)
			return numberType.EVEN;
		else
			return numberType.ODD;
	}
}
