package info.scce.product.process.atomicsibs;

public class BooleanLogic {

	public static Boolean not(Boolean var1) {
		return !var1;
	}

	public static Boolean and(Boolean var1, Boolean var2) {
		return var1 && var2;
	}

	public static Boolean or(Boolean var1, Boolean var2) {
		return var1 || var2;
	}

	public static Boolean impl(Boolean var1, Boolean var2) {
		return (!var1) || var2;
	}

	public static Boolean equiv(Boolean var1, Boolean var2) {
		return (var1 && var2) || (!var1 && !var2);
	}

	public static Boolean xor(Boolean var1, Boolean var2) {
		return (var1 && !var2) || (!var1 && var2);
	}

}
