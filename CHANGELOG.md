# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Lucky Lakefront](https://gitlab.com/scce/dime/-/milestones/11) - Unreleased

### Changed

* Improve structure of Wiki ([#653](https://gitlab.com/scce/dime/-/issues/653))

### Removed

* WIKI: Drop distinction between DIME1 and DIME2 ([#741](https://gitlab.com/scce/dime/-/issues/741))

## [Kind Köpi](https://gitlab.com/scce/dime/-/milestones/10) - 2021-02-28

### Changed

* Forbid /app for custom entry points ([#745](https://gitlab.com/scce/dime/-/issues/745))
* Throw Exception instead of returning 1337 ([#740](https://gitlab.com/scce/dime/-/issues/740)

### Fixed

* Repair Deployment View ([#354](https://gitlab.com/scce/dime/-/issues/354))

### Removed

* Remove DyWA ([#583](https://gitlab.com/scce/dime/-/issues/583))

## [Jealous Jever](https://gitlab.com/scce/dime/-/milestones/9) - 2021-01-31

### Added

* Document common pitfalls of working with DIME ([#648](https://gitlab.com/scce/dime/-/issues/648))
* Create logo for DIME test apps and clean up branding folder ([#651](https://gitlab.com/scce/dime/-/issues/651))
* Add progress bar when app is busy ([#657](https://gitlab.com/scce/dime/-/issues/657))

### Changed

* Remove port 8888 from cinco-meta-snapshot repository url ([#718](https://gitlab.com/scce/dime/-/issues/718))

### Changed

* Upgrade to Dart Version 2.10.x ([#558](https://gitlab.com/scce/dime/-/issues/558))

### Fixed

* Error: The method 'clearQueue' isn't defined for the class 'FileUploader' ([#650](https://gitlab.com/scce/dime/-/issues/650))
* Clean up .gitignore of info.scce.dime project ([#700](https://gitlab.com/scce/dime/-/issues/700))

### Removed

* Should we remove the C-generator in order to reduce the complexity of the codebase? ([#643](https://gitlab.com/scce/dime/-/issues/643))

## [Ironic Ipswich](https://gitlab.com/scce/dime/-/milestones/8) - 2020-12-31

### Changed

* Decouple static from generated Dart code by using Interfaces and Dependency Injection ([#625](https://gitlab.com/scce/dime/-/issues/625))
* Increasing test coverage of Dart static sources ([#642](https://gitlab.com/scce/dime/-/issues/642))

### Fixed

* Dart static sources should comply the Effective Dart + Pedantic guidelines ([#635](https://gitlab.com/scce/dime/-/issues/635))

### Removed

* Remove obsolete code from files in the filesupport folder ([#639](https://gitlab.com/scce/dime/-/issues/639))

## [Honest Heineken](https://gitlab.com/scce/dime/-/milestones/7) - 2020-11-30

### Added

* Add Wiki entry for GenericSIBs ([#614](https://gitlab.com/scce/dime/-/issues/614))
* Enable automated QA for static Dart sources ([#618](https://gitlab.com/scce/dime/-/issues/618))
* Introduce separated test folder according to Dart package layout conventions ([#619](https://gitlab.com/scce/dime/-/issues/619))
* Add Dart test package to dev dependencies ([#620](https://gitlab.com/scce/dime/-/issues/620))
* Create basic issue templates with Definition of Done ([#633](https://gitlab.com/scce/dime/-/issues/633))

### Changed

* Backend Port 8080 → 80 ([#501](https://gitlab.com/scce/dime/-/issues/501))
* Improve readability of 'Generate Cinco Product' Section ([#615](https://gitlab.com/scce/dime/-/issues/615))
* [Wiki] Move 'Build and install the libraries bundle' to 'Step 3: Generate Cinco Product' ([#617](https://gitlab.com/scce/dime/-/issues/617))
* Apply Effective Dart code style to static sources ([#621](https://gitlab.com/scce/dime/-/issues/621))
* Reduce size of compiled main.dart.js ([#623](https://gitlab.com/scce/dime/-/issues/623))
* Move static code-generator resources to info.scce.dime.generator ([#629](https://gitlab.com/scce/dime/-/issues/629))

### Fixed

* Click on Logout redirects to 127.0.0.1:9090 ([#371](https://gitlab.com/scce/dime/-/issues/371))
* Toggle button for navbar is not visible when the navbar is collapsed ([#626](https://gitlab.com/scce/dime/-/issues/626))
* Repair CI pipeline to handle moved static resources ([#631](https://gitlab.com/scce/dime/-/issues/631))
* [Wiki] Fix location of static Dart sources ([#632](https://gitlab.com/scce/dime/-/issues/632))

### Removed

* Remove obsolete URLs (frontend) from shiro.ini ([#622](https://gitlab.com/scce/dime/-/issues/622))

## [Grumpy Grolsch](https://gitlab.com/scce/dime/-/milestones/6) - 2020-10-31

## [Friendly Foster](https://gitlab.com/scce/dime/-/milestones/5) - 2020-09-30

### Added

* Added a changelog ([#596](https://gitlab.com/scce/dime/-/issues/596))
* Nagios compatible healthcheck ([#584](https://gitlab.com/scce/dime/-/issues/584))
* Create prototype of DIME Docker image for CI jobs ([#587](https://gitlab.com/scce/dime/-/issues/587))

### Changed

* Update latest tag of dime app compiler docker image ([#588](https://gitlab.com/scce/dime/-/issues/588))

## [Dizzy Dab](https://gitlab.com/scce/dime/-/milestones/4) (and earlier) - 2020-08-31
