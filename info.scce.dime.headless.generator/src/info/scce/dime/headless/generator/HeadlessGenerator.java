package info.scce.dime.headless.generator;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;
import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.generator.dad.Generator;

public class HeadlessGenerator {
	
	public String TARGET_FOLDER = "target/dywa-app";
	
	public enum Status{
		OK,
		ERROR
	}
	
	private IWorkspaceRoot workspaceRoot;
	private IWorkspace workspace;
	private IProgressMonitor monitor = new NullProgressMonitor();
	private WorkspaceExtension workEx = new WorkspaceExtension();
	private String graphPath;
	private IPath outlet;
	private DAD graph;
	private String importPath;
	
	
	
	public Job createGenerationJob(String importPath, String modelPath) {
		graphPath = modelPath;
		this.importPath = importPath;
		
		setUpWorkspace();
		loadDADModel();
		
		CompoundJob job = JobFactory.job("DIME Headless Generator", false);
		job.consume(100, "Starting Code Generation");
		collectCodeGenerationTasks(job);
		
		job.onFinished(this::finish);
		
		return job;
	}
	
	private IPath getAbsoluteOutputPath() {
		IFile modelFile = workspaceRoot.getFile(Path.fromOSString(graphPath));
		IProject project = modelFile.getProject();
		return project.getFolder(TARGET_FOLDER).getLocation();
	}
	
	private void collectCodeGenerationTasks(CompoundJob job) {
		if (graph == null) {
			throw new RuntimeException(String.format("Could not load model: %s", graphPath));
		}
		Generator dimeGraphGen = new Generator();
		dimeGraphGen.setHeadless(true);
		dimeGraphGen.collectTasks(graph, outlet, job);
	}

	private void loadDADModel() {
		System.out.print("Loading DAD Model... ");
		try {
			graph = loadDADModel(graphPath);
			System.out.println("done.");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private void finish() {
		System.out.println("Code Generation Finished");
		try {
			System.out.print("Refreshing workspace... ");
			workspaceRoot.refreshLocal(IWorkspaceRoot.DEPTH_INFINITE, monitor);
			System.out.println("done.");
			System.out.print("Saving workspace... ");
			workspace.save(true, monitor);
			System.out.println("done.");
		} catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Loads DAD Model from given parameter model Path Path is relative to given
	 * Workspace
	 * 
	 * @param graphPath
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("restriction")
	private DAD loadDADModel(String graphPath) throws IOException {
		URI uri = URI.createURI("platform:/resource/" + graphPath);
		System.out.println("URI to be loaded: " + uri.toString());
		IFile modelFile = workEx.getFile(uri);
		return workEx.getGraphModel(modelFile, DAD.class);
	}



	private void setUpWorkspace() {
		System.out.println("Setting up workspace...");
		workspace = workEx.getWorkspace();
		workspaceRoot = workspace.getRoot();
		if (importPath != null) {
			try {
				System.out.print("  Importing projects into workspace... ");
				ProjectImport.importProjectFromString(importPath);
				System.out.println("done.");
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		outlet = getAbsoluteOutputPath();
		System.out.println("  Opening projects in workspace...");
		for (IProject project: workspaceRoot.getProjects()) {
			try {
				System.out.println("    Opening project " + project.getName());
				project.open(monitor);
			} catch (CoreException e) {
				throw new RuntimeException(e);
			}
		}
		System.out.println("  done.");
		
		try {
			System.out.print("  Refreshing workspace... ");
			workspaceRoot.refreshLocal(IWorkspaceRoot.DEPTH_INFINITE, monitor);
			System.out.println("done.");
		} catch (CoreException e) {
			throw new RuntimeException(e);
			
		}

		System.out.println("done.");
	}
	
}
