package info.scce.dime.objectlang

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import graphmodel.GraphModel
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.EnumLiteral
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.objectlang.objectLang.AttributeDef
import info.scce.dime.objectlang.objectLang.AttributeValueDef
import info.scce.dime.objectlang.objectLang.AttributeValueReferenceDef
import info.scce.dime.objectlang.objectLang.BooleanListAttributeValueDef
import info.scce.dime.objectlang.objectLang.ComplexInputDef
import info.scce.dime.objectlang.objectLang.ComplexListAttributeValuesDef
import info.scce.dime.objectlang.objectLang.Import
import info.scce.dime.objectlang.objectLang.InputDef
import info.scce.dime.objectlang.objectLang.IntegerListAttributeValueDef
import info.scce.dime.objectlang.objectLang.NamedObject
import info.scce.dime.objectlang.objectLang.ObjectCreation
import info.scce.dime.objectlang.objectLang.ObjectDef
import info.scce.dime.objectlang.objectLang.ObjectExtend
import info.scce.dime.objectlang.objectLang.ObjectInstantiation
import info.scce.dime.objectlang.objectLang.ObjectNew
import info.scce.dime.objectlang.objectLang.ObjectsModel
import info.scce.dime.objectlang.objectLang.PrimitiveInputDef
import info.scce.dime.objectlang.objectLang.PrimitiveListAttributeValueDef
import info.scce.dime.objectlang.objectLang.PrimitiveTypeDef
import info.scce.dime.objectlang.objectLang.ProcessArgumentDef
import info.scce.dime.objectlang.objectLang.ProcessDef
import info.scce.dime.objectlang.objectLang.ProcessOutput
import info.scce.dime.objectlang.objectLang.ProcessOutputDef
import info.scce.dime.objectlang.objectLang.RealListAttributeValueDef
import info.scce.dime.objectlang.objectLang.TextAttributeValueDef
import info.scce.dime.objectlang.objectLang.TextListAttributeValueDef
import info.scce.dime.objectlang.objectLang.TypeDef
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import java.util.HashSet
import java.util.List
import org.eclipse.core.runtime.Path
import org.eclipse.emf.ecore.EObject

import static org.eclipse.emf.common.util.URI.createPlatformResourceURI
import static org.eclipse.emf.common.util.URI.createURI

import static extension org.eclipse.emf.ecore.util.EcoreUtil.getRootContainer
import org.eclipse.core.resources.IProject
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.StartSIB
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IResource
import org.eclipse.emf.ecore.resource.Resource
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import org.eclipse.core.resources.IResourceChangeListener
import org.eclipse.core.resources.IResourceChangeEvent

class ObjectLangExtension {
	
	extension FileExtension = new FileExtension
	extension ResourceExtension = new ResourceExtension
	extension WorkspaceExtension = new WorkspaceExtension
	extension GraphModelExtension = new GraphModelExtension
	extension DataExtension = DataExtension.instance
	
	static val file_on_objectsModel = <IFile,ObjectsModel> newHashMap
	static IResourceChangeListener workspaceListener
	
	
	def getAttributes(ObjectDef objDef) {
		objDef.creation?.attributes ?: #[]
	}
	
	def getDisplayName(ProcessArgumentDef argDef) {
		switch it : argDef.key {
			OutputPort: name
			default: argDef.eClass.name
		}
	}
	
	def getDisplayName(AttributeDef attrDef) {
		switch it : attrDef.key {
			Attribute: name
			default: attrDef.eClass.name
		}
	}
	
	dispatch def String getDisplayName(ObjectDef objDef) {
		(objDef.getName ?: "<unnamed>")
		+ switch it : objDef.creation {
			ObjectExtend: " < " + it.objSrc.displayName
			default: ''
		}
	}
	
	dispatch def String getDisplayName(InputDef objDef) {
		objDef.getName ?: "<unnamed>"
	}
	
	def getExtendedObjectDef(ObjectCreation objCrt) {
		switch it : objCrt {
			ObjectExtend: it.objSrc
		}
	}
	
	def List<ObjectCreation> getExtensionHierarchy(ObjectCreation objCrt) {
		(#[objCrt] + switch it : objCrt.extendedObjectDef {
			ObjectDef: creation?.extensionHierarchy
			default: #[]
		}).toList
	}
	
	def <T extends EObject> T findFirstParent(EObject eobj, Class<T> cls) {
		var parent = eobj.eContainer
		while (parent !== null) {
			if (cls.isInstance(parent))
				return parent as T
			parent = parent.eContainer
		}
		return null
	}
	
	def findFirstParent(EObject eobj, Class<? extends EObject>... classes) {
		var parent = eobj.eContainer
		while (parent !== null) {
			val obj = parent
			if (classes.exists[it.isInstance(obj)])
				return parent
			parent = parent.eContainer
		}
		return null
	}
	
	def getName(AttributeDef attrDef) {
		switch it : attrDef.key {
			Attribute: name
		}
	}
	
	def getName(IO it) {
		switch it {
			Input: name
			Output: name
		}
	}
	
	def <T extends EObject> List<T> getParents(EObject eobj, Class<T> cls) {
		val parents = <T> newArrayList
		var parent = eobj.eContainer
		while (parent !== null) {
			if (cls.isInstance(parent))
				parents.add(parent as T)
			parent = parent.eContainer
		}
		return parents
	}
	
	def getLinkableNamedObjects(ObjectsModel model) {
		(model.objects + model.inputs).toMap[name].values.toList
	}
	
	def getLinkableNamedObjectsOfMatchingType(ProcessArgumentDef argDef) {
		val outputPort = argDef.key
		if (outputPort instanceof ComplexOutputPort) {
			val objectsModel = argDef.objectsModel
			val objsMap = (objectsModel.objects + objectsModel.inputs).toMap[getName]
			
			objsMap.values.filter[
				type?.isTypeOf(outputPort.dataType)
			].toList
			
		} else if (outputPort instanceof PrimitiveOutputPort) {
			val objectsModel = argDef.objectsModel
			val objDefsMap = objectsModel.inputs.filter(PrimitiveInputDef).toMap[getName]
			
			objDefsMap.values.toList
			
		} else #[]
	}
	
	def getLinkableNamedObjectsOfMatchingType(AttributeDef attrDef) {
		val attribute = attrDef.key as Attribute
		if (attribute instanceof ComplexAttribute) {
			val objectsModel = attrDef.objectsModel
			val objsMap = (objectsModel.objects + objectsModel.inputs).toMap[getName]
			
			objsMap.values.filter[
				type?.isTypeOf(attribute.dataType)
			].toList
			
		} else if (attribute instanceof PrimitiveAttribute) {
			val objectsModel = attrDef.objectsModel
			val objsMap = objectsModel.inputs.filter(PrimitiveInputDef).toMap[getName]
			
			objsMap.values.toList
			
		} else #[]
	}
	
	def getLinkableNamedObjectsOfMatchingType(ComplexListAttributeValuesDef valueDef) {
		val attrDef = valueDef.eContainer as AttributeDef
		getLinkableNamedObjectsOfMatchingType(attrDef)
	}
	
	def getLinkableNamedObjectsOfMatchingType(PrimitiveListAttributeValueDef valueDef) {
		val attrDef = valueDef.eContainer as AttributeDef
		getLinkableNamedObjectsOfMatchingType(attrDef)
	}
	
	def getObjectsModel(EObject it) {
		rootContainer as ObjectsModel
	}
	
	def Type getComplexType(AttributeDef it) {
		(key as ComplexAttribute)?.dataType
	}
	
	def getValues(PrimitiveListAttributeValueDef valueDef) {
		switch it : valueDef {
			BooleanListAttributeValueDef: values
			IntegerListAttributeValueDef: values
			RealListAttributeValueDef: values
			TextListAttributeValueDef: values
			default: #[]
		}
	}
	
	/*
	 * Retrieve the imported graph model from the current project
	 */
	def getImportedGraphModel(Import ^import) {
		ReferenceRegistry.instance.getEObject(import.path)
		?: {
			val project = getProject(import)
			val file = project.getFile(import.path)
			val fileUri = createURI(file.fullPath.toString)
			
			ReferenceRegistry.instance.lookup(fileUri, GraphModel)?.head
			?: if (file?.exists) file?.graphModel
		}
	}
	
	/*
	 * Retrieve the imported objects model from the current project
	 */
	def getImportedObjectsModel(Import ^import) {
		val project = getProject(^import)
		val objmodel =
			import.objectsModel
				.otherObjectsModels
				.findFirst[
					it.generations.exists[it.modelId == import.path]
				]
		if (objmodel instanceof ObjectsModel) {
			return objmodel
		}
		val file = project.getFile(import.path)
		val fileUri = createURI(file.fullPath.toString)
		ReferenceRegistry.instance.lookup(fileUri, ObjectsModel)?.head
		?: if (file?.exists) file?.getContent(ObjectsModel)
	}
	
	def getOtherObjectsModels(ObjectsModel objModel) {
		objModel.project.getObjectsModels(objModel.eResource)
	}
	
	def getObjectsModels(IProject project, Resource excludeResource) {
		val excludeFile = excludeResource.URI.IResource
		project.getFiles[fileExtension == "objects"]
			.filter[it != excludeFile]
			.map[ file |
				if (file_on_objectsModel.containsKey(file)) {
					file_on_objectsModel.get(file)
				} else {
					file_on_objectsModel.put(file, null)
					file.getContent(ObjectsModel) => [
						file_on_objectsModel.put(file, it)
						assertWorkspaceListener
					]
				}
			]
			.filterNull
	}
	
	def getProject(EObject eobj) {
		var uri = eobj.eResource.URI
		if (uri.isFile) {
			val path = new Path(uri.toFileString)
			val file = workspaceRoot.getFileForLocation(path)
			uri = createPlatformResourceURI(file.getFullPath.toString, true)
		}
		uri?.getFile?.project
	}
	
	def getProcessDef(ObjectDef it) {
		switch it : creation {
			ProcessOutput: processDef
		}
	}
	
	def getImportPath(ProcessDef processDef) {
		processDef?.processImport?.path
	}
	
	def getImportedProcessModel(ProcessDef processDef) {
		val processImport = processDef?.processImport
		switch processImport {
			Import: {
				val model = getImportedGraphModel(processImport)
				if (model instanceof Process) {
					return model
				}
			}
		}
	}
	
	def getImportedObjectsModel(ProcessDef processDef) {
		val processImport = processDef?.processImport
		switch processImport {
			Import: {
				val model = getImportedObjectsModel(processImport)
				if (model instanceof ObjectsModel) {
					return model
				}
			}
		}
	}
	
	def Type getType(NamedObject objSrc) {
		switch objSrc {
			ObjectDef: getType(objSrc.creation)
			ComplexInputDef: objSrc.typeDef?.type as Type
		}
	}
	
	def getType(ObjectCreation objCrt) {
		switch objCrt {
			ObjectNew, ObjectExtend: objCrt.typeDef?.type as Type
			ProcessOutput: switch it : objCrt.output {
				ComplexInputPort: dataType
				ObjectDef: type
			}
		}
	}
	
	def TypeDef getTypeDef(ObjectDef it) {
		switch it : creation {
			ObjectInstantiation: typeDef
		}
	}
	
	def TypeDef getTypeDef(ObjectCreation it) {
		switch it {
			ObjectNew: typeDef
			ObjectExtend: objSrc.getTypeDef_recurse(newHashSet)
		}
	}
	
	private def TypeDef getTypeDef_recurse(NamedObject objSrc, HashSet<EObject> seen) {
		if (!seen.add(objSrc)) {
			return null
		}
		switch objSrc {
			ObjectDef: switch creation : objSrc.creation {
				ObjectNew: creation.typeDef
				ObjectExtend: creation.objSrc.getTypeDef_recurse(seen)
			}
			ComplexInputDef: objSrc.typeDef
		}
	}
	
	def getOutput(ProcessOutput processOutput) {
		processOutput.outputDef?.output
		?: processOutput.processDef.importedProcessModel.endSibPorts?.head
		?: processOutput.processDef.importedObjectsModel.outputs?.head
	}
	
	def getStartSibPorts(Process processModel) {
		processModel?.find(StartSIB)?.flatMap[outputPorts]?.filter(OutputPort)
	}
	
	def getEndSibPorts(Process processModel) {
		processModel?.find(EndSIB)?.flatMap[inputPorts]?.filter(InputPort)
	}
	
	def getOutputs(ObjectsModel objectsModel) {
		objectsModel?.objects?.filter[output !== null]
	}
	
	def isAbstract(NamedObject objSrc) {
		switch objSrc {
			ObjectDef: isAbstract(objSrc?.creation)
			InputDef: false
		}
	}
	
	def isAbstract(ObjectCreation it) {
		switch it {
			ObjectNew: isAbstract
			ObjectExtend: isAbstract
		}
	}
	
	def isHashed(AttributeValueDef valueDef) {
		switch it : valueDef {
			TextAttributeValueDef: isHashed
			TextListAttributeValueDef: isHashed
		}
	}
	
	def getComplexType(AttributeValueDef attrDef) {
		switch it : attrDef {
			AttributeValueReferenceDef: value.type
			ObjectCreation: it.type
		}
	}
	
	def hasComplexType(AttributeValueDef attrDef) {
		getComplexType(attrDef) !== null
	}
	
	def getPrimitiveType(InputDef inputDef) {
		mapOnDataType(switch inputDef {
			PrimitiveInputDef: inputDef.typeDef
		})
	}
	
	def hasPrimitiveType(InputDef inputDef) {
		getPrimitiveType(inputDef) !== null
	}
	
	def hasPrimitiveType(InputDef inputDef, PrimitiveType type) {
		getPrimitiveType(inputDef) == type
	}
	
	def getComplexType(InputDef inputDef) {
		switch inputDef {
			ComplexInputDef: inputDef.typeDef?.type as Type
		}
	}
	
	def hasComplexType(InputDef inputDef) {
		getComplexType(inputDef) !== null
	}
	
	def mapOnDataType(PrimitiveTypeDef it) {
		switch it {
			case BOOLEAN: PrimitiveType.BOOLEAN
			case FILE: PrimitiveType.FILE
			case INTEGER: PrimitiveType.INTEGER
			case REAL: PrimitiveType.REAL
			case TEXT: PrimitiveType.TEXT
			case TIMESTAMP: PrimitiveType.TIMESTAMP
			default: null
		}
	}
	
	def mapOnDataType(info.scce.dime.process.process.PrimitiveType it) {
		switch it {
			case BOOLEAN: PrimitiveType.BOOLEAN
			case FILE: PrimitiveType.FILE
			case INTEGER: PrimitiveType.INTEGER
			case REAL: PrimitiveType.REAL
			case TEXT: PrimitiveType.TEXT
			case TIMESTAMP: PrimitiveType.TIMESTAMP
		}
	}
	
	def getEnumType(Attribute it) {
		switch it {
			ComplexAttribute: switch dataType {
				EnumType: dataType
			}
		}
	}
	
	def getEnumLiterals(EnumType enumType) {
		enumType.find(EnumLiteral)
	}
	
	def isEnumAttribute(Attribute attr) {
		getEnumType(attr) !== null
	}
	
	def getEnumType(IO io) {
		val type = switch it : io {
			ComplexInputPort: it.dataType
			ComplexOutputPort: it.dataType
		}
		if (type instanceof EnumType) {
			return type
		}
	}
	
	def isEnumPort(IO io) {
		getEnumType(io) !== null
	}
	
	def getComplexType(Attribute it) {
		switch it {
			ComplexAttribute: dataType
		}
	}
	
	def Type getComplexType(ProcessArgumentDef argDef) {
		switch it : argDef.key {
			IO: complexType
		}
	}
	
	def getPrimitiveType(Attribute it) {
		switch it {
			PrimitiveAttribute: dataType
		}
	}
	
	def getComplexType(IO io) {
		switch io {
			ComplexInputPort: io.dataType
			ComplexOutputPort: io.dataType
		}
	}
	
	def getPrimitiveType(IO io) {
		switch io {
			PrimitiveInputPort: io.dataType.mapOnDataType
			PrimitiveOutputPort: io.dataType.mapOnDataType
		}
	}
	
	def hasPrimitiveType(IO io, PrimitiveType type) {
		io.primitiveType == type
	}
	
	def hasComplexType(IO io, Type type) {
		io.complexType == type
	}
	
	def getTargetFileName(ObjectsModel model) {
		val fullFileName = model.eResource.URI.lastSegment
		var String fileName
		if (!model.generations.nullOrEmpty) {
			val fileNames = newHashSet
			for (gendef : model.generations) {
				if (!gendef.genFile.nullOrEmpty) {
					val file = model.project.getFile(gendef.genFile)
					fileName = file.fullPath.lastSegment.toString
				} else {
					fileName = "Create" + fullFileName.substring(0, fullFileName.lastIndexOf('.'))
					var fileNameCandidate = fileName
					var index = 0
					while (fileNames.contains(fileNameCandidate)) {
						fileNameCandidate = fileName + index++
					}
					fileName = fileNameCandidate
				}
				fileNames.add(fileName)
			}
		} else {
			fileName = "Create" + fullFileName.substring(0, fullFileName.lastIndexOf('.'))
		}
		return fileName
	}
	
	def assertWorkspaceListener() {
		if (workspaceListener === null) {
			workspaceListener = new IResourceChangeListener {
				override resourceChanged(IResourceChangeEvent event) {
					if (event.delta !== null) {
						file_on_objectsModel.clear
					}
				}
			}
			workspace.addResourceChangeListener(workspaceListener)
		}
	}
}