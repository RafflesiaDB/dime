package info.scce.dime.objectlang.scoping

import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.objectlang.ObjectLangExtension
import info.scce.dime.objectlang.objectLang.AttributeDef
import info.scce.dime.objectlang.objectLang.AttributeValueReferenceDef
import info.scce.dime.objectlang.objectLang.ComplexListAttributeValuesDef
import info.scce.dime.objectlang.objectLang.Import
import info.scce.dime.objectlang.objectLang.InputDef
import info.scce.dime.objectlang.objectLang.NamedObject
import info.scce.dime.objectlang.objectLang.ObjectCreation
import info.scce.dime.objectlang.objectLang.ObjectExtend
import info.scce.dime.objectlang.objectLang.ObjectsModel
import info.scce.dime.objectlang.objectLang.ProcessArgumentDef
import info.scce.dime.objectlang.objectLang.ProcessDef
import info.scce.dime.objectlang.objectLang.ProcessOutput
import info.scce.dime.objectlang.objectLang.ProcessOutputDef
import info.scce.dime.objectlang.objectLang.TypeDef
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.StartSIB
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider

/**
 * This class contains custom scoping description.
 * 
 * @author Steve Bosselmann
 */
class ObjectLangScopeProvider extends AbstractDeclarativeScopeProvider {
	
	extension DataExtension = DataExtension.instance
	extension GraphModelExtension = new GraphModelExtension
	extension ObjectLangExtension = new ObjectLangExtension
	
	def IScope scope_TypeDef_dataImport(TypeDef typeDef, EReference ref) {
		getLocalImportsScope(typeDef.objectsModel)
	}
	
	def IScope scope_TypeDef_type(TypeDef typeDef, EReference ref) {
		switch typeDef.dataImport {
			Import: {
				val model = getImportedGraphModel(typeDef.dataImport)
				if (model instanceof Data) {
					return getScopeFor(model.find(Type).filter[!isAbstract]) [Type t | t.name]
				}
			}
		}
		IScope.NULLSCOPE
	}
	
	def IScope scope_ProcessDef_processImport(ProcessDef processDef, EReference ref) {
		getLocalImportsScope(processDef.objectsModel)
	}
	
	def IScope scope_ProcessOutputDef_output(ProcessOutputDef outDef, EReference ref) {
		val execDef = outDef.findFirstParent(ProcessOutput)
		val processImport = execDef?.processDef?.processImport
		
		val outputs = switch processImport {
			Import: {
				val model = getImportedGraphModel(processImport)
				if (model instanceof Process) {
					model.find(EndSIB).flatMap[inputPorts].toList
				}
			}
			default: {
				val models = outDef.objectsModel.imports.map[getImportedGraphModel(it)]
				models.filter(Process).flatMap[find(EndSIB)].flatMap[inputPorts].toList
			}
		} ?: #[]
		
		getScopeFor(outputs) [InputPort inputPort | inputPort.name]
	}
	
	def IScope scope_AttributeDef_key(AttributeDef attrDef, EReference ref) {
		val ObjectCreation objCrt = attrDef.findFirstParent(ObjectCreation)
		var type = objCrt.type
		if (objCrt instanceof ProcessOutput) {
			val outDef = objCrt as ProcessOutput
			var output = outDef.outputDef?.output
			if (output === null) {
				val processImport = outDef.processDef?.processImport
				output = switch processImport {
					Import: {
						val model = getImportedGraphModel(processImport)
						if (model instanceof Process) {
							model.find(EndSIB).flatMap[inputPorts].filter(ComplexInputPort).head
						}
					}
				}
			}
			if (output instanceof ComplexInputPort) {
				type = output.dataType
			}
		}
		
		if (type instanceof Type) {
			return getScopeFor(type.inheritedAttributes) [Attribute attr | attr.name]
		}
		
		IScope.NULLSCOPE
	}
	
	def IScope scope_ProcessArgumentDef_key(ProcessArgumentDef argDef, EReference ref) {
		val outDef = argDef.findFirstParent(ProcessOutput)
		val processImport = outDef.processDef?.processImport
		val inputs = switch processImport {
			Import: {
				val process = getImportedGraphModel(processImport)
				if (process instanceof Process) {
					process.find(StartSIB).flatMap[outputPorts].toList
				}
				else {
					val objModel = getImportedObjectsModel(processImport)
					if (objModel instanceof ObjectsModel) {
						objModel.inputs
					}
				}
			}
			default: {
				val models = outDef.objectsModel.imports.map[getImportedGraphModel(it)]
				models.filter(Process).flatMap[find(StartSIB)].flatMap[outputPorts].toList
			}
		} ?: #[]
		getScopeFor(inputs) [switch it {
			OutputPort: it.name
			InputDef: it.name
		}]
	}
	
	def IScope scope_AttributeValueReferenceDef_value(ProcessArgumentDef argDef, EReference ref) {
		return getLocalNamedObjectsScope(argDef.objectsModel)
	}
	
	def IScope scope_AttributeValueReferenceDef_value(AttributeValueReferenceDef valueDef, EReference ref) {
		val context = valueDef.findFirstParent(AttributeDef, ProcessArgumentDef)
		if (context instanceof AttributeDef) {
			return getLocalNamedObjectsScope(context.objectsModel)
		}
		if (context instanceof ProcessArgumentDef) {
			return scope_AttributeValueReferenceDef_value(context, ref)
		}
		IScope.NULLSCOPE
	}
	
	def IScope scope_ComplexListAttributeValuesDef_valuesDef(ComplexListAttributeValuesDef valuesDef, EReference ref) {
		val attrDef = valuesDef.findFirstParent(AttributeDef)
		if (attrDef instanceof AttributeDef) {
			return getLocalNamedObjectsScope(attrDef.objectsModel)
		}
		IScope.NULLSCOPE
	}
	
	def IScope scope_ObjectExtend_objDef(ObjectExtend objExtend, EReference ref) {
		val attrDef = objExtend.findFirstParent(AttributeDef)
		val candidates = switch attrDef {
			AttributeDef: attrDef.objectsModel.linkableNamedObjects
			default: (objExtend.objectsModel.objects + objExtend.objectsModel.inputs).toList
		}
		getScopeFor(candidates) [NamedObject objsrc | objsrc.getName]
	}
	
	def IScope getLocalNamedObjectsScope(ObjectsModel model) {
		getScopeFor(model.linkableNamedObjects, [NamedObject obj | obj.name])
	}
	
	def IScope getLocalImportsScope(ObjectsModel model) {
		getScopeFor(model.imports, [Import im | im.name])
	}
	
	def <T extends EObject> IScope getScopeFor(Iterable<T> objects, (T)=>String nameProvider) {
		Scopes.scopeFor(objects, QualifiedName.wrapper(nameProvider), IScope.NULLSCOPE)
	}
}