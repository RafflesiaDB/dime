/*
 * generated by Xtext 2.14.0
 */
package info.scce.dime.objectlang


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class ObjectLangStandaloneSetup extends ObjectLangStandaloneSetupGenerated {

	def static void doSetup() {
		new ObjectLangStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
