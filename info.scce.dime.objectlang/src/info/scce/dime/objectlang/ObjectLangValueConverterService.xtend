package info.scce.dime.objectlang

import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.common.services.DefaultTerminalConverters
import org.eclipse.xtext.conversion.ValueConverter
import org.eclipse.xtext.conversion.impl.QualifiedNameValueConverter
import org.eclipse.xtext.nodemodel.ILeafNode
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.conversion.IValueConverter
import org.eclipse.xtext.conversion.ValueConverterException

/**
 * @author Steve Bosselmann
 */
class ObjectLangValueConverterService extends DefaultTerminalConverters {

	@ValueConverter(rule="BooleanValue")
	def IValueConverter<Boolean> getBooleanValueConverter() {
		
		return new IValueConverter<Boolean>() {
		
			override toString(Boolean value) {
				value.toString
			}
			
			override toValue(String str, INode node) {
				if (str.nullOrEmpty)
					throw new ValueConverterException("Failed to convert empty string to boolean", node, null)
				else Boolean.parseBoolean(str) == Boolean.TRUE
			}
		
		};
	}
	
	@ValueConverter(rule="StringOrID")
	def QualifiedNameValueConverter getStringOrIDConverter() {
				
		new QualifiedNameValueConverter() {
			
			// adds leading and trailing quotes if the value contains whitespaces
			override toString(String value) {
				if (value?.contains(' ')) {
					'"' + value + '"'
				} else {
					value
				}
			}
			
			// call the custom delegateToValue method here
			override toValue(String string, INode node) {
				node.leafNodes
					.filter[grammarElement instanceof RuleCall]
					.map[delegateToValue]
					.filterNull
					.join
			}
			
			// removes leading and trailing quotes from the text if existent
			override delegateToValue(ILeafNode leafNode) {
				if (delegateConverter === null) {
					delegateConverter = getConverter("ID");
				}
				delegateConverter.toValue(
					leafNode.text?.replaceAll('^"+', "").replaceAll('"+$', ""), leafNode
				) as String
			}
			
		}
	}
}