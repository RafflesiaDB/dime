package info.scce.dime.objectlang.generator

import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.generator.IGenerator2

interface IObjectLangGenerator extends IGenerator2 {
	
	def void doGenerate(ResourceSet resSet, IFileSystemAccess2 fsa, IGeneratorContext context)
	
}