package info.scce.dime.objectlang.generator

import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import info.scce.dime.objectlang.objectLang.ObjectsModel
import org.eclipse.core.runtime.IPath
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

import static extension info.scce.dime.objectlang.generator.ObjectLangProcessGenerator.generateProcess
import org.eclipse.emf.ecore.resource.ResourceSet

/**
 * Generates an object creation process from an ObjectsModel on save.
 * 
 * @author Steve Bosselmann
 */
class ObjectLangGenerator extends AbstractGenerator implements IObjectLangGenerator {

	extension WorkspaceExtension = new WorkspaceExtension
	
	override void doGenerate(ResourceSet resSet, IFileSystemAccess2 fsa, IGeneratorContext context) {
		resSet.resources.forEach[doGenerate(fsa, context)]
	}
	
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		val model = resource.allContents.filter(ObjectsModel).head
		if (model === null) {
			return;
		}
		
		val fullFileName = resource.URI.lastSegment
		var String fileName
		var IPath outlet
		var String modelId
		
		if (!model.generations.nullOrEmpty) {
			val fileNames = newHashSet
			for (gendef : model.generations) {
				modelId = gendef.modelId
				if (!gendef.genFile.nullOrEmpty) {
					val file = model.project.getFile(gendef.genFile)
					outlet = file.parent.projectRelativePath
					fileName = file.fullPath.lastSegment.toString
				} else {
					fileName = "Create" + fullFileName.substring(0, fullFileName.lastIndexOf('.'))
					var fileNameCandidate = fileName
					var index = 0
					while (fileNames.contains(fileNameCandidate)) {
						fileNameCandidate = fileName + index++
					}
					fileName = fileNameCandidate
					outlet = model.IResource.parent.fullPath.append("model-gen")
				}
				fileNames.add(fileName)
				model.generateProcess(outlet, fileName, modelId)
			}
		} else {
			fileName = "Create" + fullFileName.substring(0, fullFileName.lastIndexOf('.'))
			outlet = outlet = model.IResource.parent.fullPath.append("model-gen")
			model.generateProcess(outlet, fileName, modelId)
		}
	}
	
}
