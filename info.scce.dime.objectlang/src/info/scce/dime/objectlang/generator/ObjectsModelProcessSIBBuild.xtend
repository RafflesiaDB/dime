package info.scce.dime.objectlang.generator

import info.scce.dime.objectlang.ObjectLangExtension
import info.scce.dime.objectlang.objectLang.ComplexInputDef
import info.scce.dime.objectlang.objectLang.InputDef
import info.scce.dime.objectlang.objectLang.ObjectDef
import info.scce.dime.objectlang.objectLang.ObjectsModel
import info.scce.dime.objectlang.objectLang.PrimitiveInputDef
import info.scce.dime.process.build.PrimeSIBBuild
import info.scce.dime.process.helper.PortUtils
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.ProcessSIB

class ObjectsModelProcessSIBBuild extends PrimeSIBBuild<
		  ProcessSIB, /* SIB type */
			InputDef, /* SIB port reference */
			  String, /* Branch reference */
		   ObjectDef  /* Branch port reference */ > {
		   	
	extension ObjectLangExtension = new ObjectLangExtension
	
	ObjectsModel objectsModel
	
	static def initialize(ProcessSIB sib, ObjectsModel objectsModel) {
		new ObjectsModelProcessSIBBuild(sib, objectsModel).initialize
	}

	new(ProcessSIB sib, ObjectsModel objectsModel) {
		super(sib)
		this.objectsModel = objectsModel
	}

	override getSIBReference(ProcessSIB sib) {
		objectsModel
	}

	override isSIBReferenceValid(ProcessSIB sib) {
		true
	}

	override getSIBPortReferences(ProcessSIB sib) {
		objectsModel.inputs
	}

	override isSIBPortReference(Input sibPort, InputDef inputDef) {
		sibPort.name == inputDef.name
	}

	override getBranchName(String branchRef) {
		branchRef
	}

	override getBranchReferences(ProcessSIB sib) {
		#["success"]
	}

	override isBranchReference(Branch branch, String branchRef) {
		"success" == branchRef
	}

	override getBranchPortReferences(String branchRef) {
		objectsModel.objects.filter[it.output !== null]
	}

	override isBranchPortReference(Output branchPort, ObjectDef objectDef) {
		branchPort.name == objectDef.name
	}
	
	override addInput(InputDef inputDef) {
		val input = switch inputDef {
			
			PrimitiveInputDef: sib.newPrimitiveInputPort(0, 0) => [
				it.name = inputDef.name
				it.isList = false
				it.dataType = PortUtils.toPrimitiveType(inputDef.primitiveType)
			]
			
			ComplexInputDef: sib.newComplexInputPort(inputDef.complexType, 0, 0) => [
				it.name = inputDef.name
				it.isList = false
			]
		}
		
		input.postProcessNewSIBPort(inputDef)
	}
	
	override addOutputPort(Branch branch, ObjectDef objectDef) {
		branch.newComplexOutputPort(objectDef.type, 0, 0) => [
			it.name = objectDef.name
		]
	}
	
}