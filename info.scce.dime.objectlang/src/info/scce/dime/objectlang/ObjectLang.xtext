grammar info.scce.dime.objectlang.ObjectLang with org.eclipse.xtext.common.Terminals

import "http://www.eclipse.org/emf/2002/Ecore" as ecore

generate objectLang "http://www.scce.info/dime/objectlang/ObjectLang"

ObjectsModel:
	'name' name=FQN
	(
		generations+=GenerateDef |
		imports+=Import |
		inputs+=InputDef |
		objects+=ObjectDef
	)*
;

GenerateDef:
	'generate' '{'
		( ('file' genFile=STRING) |
		  ('modelId' modelId=StringOrID) )+
	 '}'
;

Import:
	'import' path=StringOrID 'as' name=ID
;

NamedObject:
	InputDef | ObjectDef
;

InputDef:
	ComplexInputDef | PrimitiveInputDef
;

ComplexInputDef:
	'input' name=ID ':' typeDef=TypeDef
;

PrimitiveInputDef:
	'input' name=ID ':' typeDef=PrimitiveTypeDef
;

ObjectDef:
	output=OutputDef?  (name=ID '=')? creation=ObjectCreation
;

OutputDef: {OutputDef}
	'output' ('(' alias=ID ')')?
;

ObjectCreation:
	(ObjectInstantiation | ProcessOutput)
	('{'
		attributes+=AttributeDef*
	 '}')?
;

ObjectInstantiation:
	ObjectNew | ObjectExtend
;

ObjectNew:
	(abstract?='abstract' | 'new') typeDef=TypeDef
;

ObjectExtend:
	abstract?='abstract'? 'extend' objSrc=[ObjectDef]
;

ProcessOutput:
	'output' outputDef=ProcessOutputDef?
	'of' processDef=ProcessDef
	('('
		arguments+=ProcessArgumentDef*
	 ')')?
;

ProcessArgumentDef:
	key=[ecore::EObject|StringOrID] '=' valueDef=AttributeValueDef
;

ProcessDef:
	processImport=[Import]
;

ProcessOutputDef:
	output=[ecore::EObject|StringOrID]
;

TypeDef:
	dataImport=[Import] '.' type=[ecore::EObject|StringOrID]
;

AttributeDef:
	key=[ecore::EObject|StringOrID] '=' valueDef=AttributeValueDef
;

AttributeValueDef:
	PrimitiveAttributeValueDef
	| PrimitiveListAttributeValueDef
	| ComplexListAttributeValuesDef
	| AttributeValueReferenceDef
	| ObjectCreation
;
	
PrimitiveAttributeValueDef:
	BooleanAttributeValueDef
	| RealAttributeValueDef
	| IntegerAttributeValueDef
	| TextAttributeValueDef
;

PrimitiveListAttributeValueDef: 
	BooleanListAttributeValueDef
	| RealListAttributeValueDef
	| IntegerListAttributeValueDef
	| TextListAttributeValueDef
	| EmptyList {EmptyList}
;

BooleanAttributeValueDef:
	value=BOOLEAN
;

BooleanListAttributeValueDef: {BooleanListAttributeValueDef}
	'[' values+=BOOLEAN (',' values+=BOOLEAN)* ']'
;

RealAttributeValueDef:
	value=DoubleValue
;

RealListAttributeValueDef: {RealListAttributeValueDef}
	'[' values+=DoubleValue (',' values+=DoubleValue)* ']'
;

IntegerAttributeValueDef:
	value=LongValue
;

IntegerListAttributeValueDef: {IntegerListAttributeValueDef}
	'[' values+=LongValue (',' values+=LongValue)* ']'
;

TextAttributeValueDef:
	hashed?='hashed'? value=STRING
;

TextListAttributeValueDef: {TextListAttributeValueDef}
	hashed?='hashed'? '[' values+=STRING (',' values+=STRING)* ']'
;

AttributeValueReferenceDef:
	value=[NamedObject]
;

ComplexListAttributeValuesDef: {ComplexListAttributeValuesDef}
	 '[' valuesDef+=(ObjectCreation | AttributeValueReferenceDef)
	(',' valuesDef+=(ObjectCreation | AttributeValueReferenceDef))* ']'
;

EmptyList: 
	'[' ']'
;

StringOrID:
	STRING | ID
;

FQN:
	ID ('.' ID)*
;

DoubleValue returns ecore::EDouble:
	LongValue '.' INT
;

LongValue returns ecore::ELong: 
	SIGN? INT
;

enum PrimitiveTypeDef:
	NONE | Boolean="bool" | File="file" | Integer="integer" | Real="real" | Text="text" | Timestamp="timestamp"
;

terminal SIGN: '+' | '-' ;

terminal BOOLEAN returns ecore::EBoolean:
	'true' | 'false'
;