package info.scce.dime.objectlang

import info.scce.dime.objectlang.generator.IObjectLangGenerator
import info.scce.dime.objectlang.generator.ObjectLangGenerator
import info.scce.dime.objectlang.objectLang.ObjectCreation
import info.scce.dime.objectlang.objectLang.ProcessDef
import info.scce.dime.objectlang.objectLang.ProcessOutput
import info.scce.dime.objectlang.objectLang.TypeDef
import org.eclipse.emf.common.util.AbstractTreeIterator
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.linking.impl.Linker

/**
 * Registers components to be used at runtime / without the Equinox extension registry.
 * 
 * @author Steve Bosselmann
 */
class ObjectLangRuntimeModule extends AbstractObjectLangRuntimeModule {
	
	def Class<? extends IObjectLangGenerator> bindIObjectLangGenerator () {
	    return ObjectLangGenerator
	}
	
    override bindIValueConverterService() {
        ObjectLangValueConverterService
    }

	override bindILinker() {
		ObjectLangLinker
	}

	static class ObjectLangLinker extends Linker {
		
		override protected getAllLinkableContents(EObject model) {
			new AbstractTreeIterator<EObject>(model) {
				override protected getChildren(Object object) {
					// assert that TypeDefs are linked before other children
					switch it : object {
						ProcessOutput: (eContents.filter(ProcessDef) + eContents.filter[it instanceof ProcessDef == false]).iterator
						ObjectCreation: (eContents.filter(TypeDef) + eContents.filter[it instanceof TypeDef == false]).iterator
						EObject: eContents.iterator
					}
				}
			}
		}
		
		override isClearAllReferencesRequired(Resource resource) {
			false
		}
	}
}
