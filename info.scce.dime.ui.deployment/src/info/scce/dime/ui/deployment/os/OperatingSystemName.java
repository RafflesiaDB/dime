package info.scce.dime.ui.deployment.os;

public interface OperatingSystemName {
	String name();
}
