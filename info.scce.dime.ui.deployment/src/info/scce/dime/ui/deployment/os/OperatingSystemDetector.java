package info.scce.dime.ui.deployment.os;

public interface OperatingSystemDetector {
	boolean isLinux();

	boolean isMac();

	boolean isWindows();
}
