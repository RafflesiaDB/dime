package info.scce.dime.ui.deployment.os;

public class DefaultOperatingSystemDetector implements OperatingSystemDetector {
	private final OperatingSystemName operatingSystemName;

	public DefaultOperatingSystemDetector(OperatingSystemName operatingSystemName) {
		this.operatingSystemName = operatingSystemName;
	}

	@Override
	public boolean isLinux() {
		return operatingSystemName.name().startsWith("Linux");
	}

	@Override
	public boolean isMac() {
		return operatingSystemName.name().startsWith("Mac");
	}

	@Override
	public boolean isWindows() {
		return operatingSystemName.name().startsWith("Windows");
	}

}
