package info.scce.dime.ui.deployment.os;

public class DefaultOperatingSystemName implements OperatingSystemName {
	@Override
	public String name() {
		return System.getProperty("os.name");
	}
}
