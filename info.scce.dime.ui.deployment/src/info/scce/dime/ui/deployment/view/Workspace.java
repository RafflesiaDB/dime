package info.scce.dime.ui.deployment.view;

import info.scce.dime.dad.dad.DAD;
import info.scce.dime.property.DimeProperties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import de.jabc.cinco.meta.runtime.xapi.FileExtension;
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;

import java.io.File;
import java.util.List;

public class Workspace {

	private final FileExtension fileExtension;
	private final WorkspaceExtension workspaceExtension;

	public Workspace(FileExtension fileExtension, WorkspaceExtension workspaceExtension) {
		this.workspaceExtension = workspaceExtension;
		this.fileExtension = fileExtension;
	}
	
	public void registerListener(IResourceChangeListener listener) {
		workspaceExtension.getWorkspace().addResourceChangeListener(listener);
	}
	
	public void unregisterListener(IResourceChangeListener listener) {
		workspaceExtension.getWorkspace().removeResourceChangeListener(listener);
	}

	public File targetFolder() throws Exception {
		String dad = dad().eResource().getURI().toPlatformString(true);
		return ResourcesPlugin
				.getWorkspace()
				.getRoot()
				.findMember(dad)
				.getProject()
				.getLocation()
				.append("target")
				.toFile();
	}

	public DAD dad() throws Exception {
		return fileExtension.getGraphModel(dadFile(), DAD.class);

	}
	
	public IFile dadFile() throws Exception {
		List<IFile> files = workspaceExtension.getFiles(
				workspaceExtension.getWorkspaceRoot(),
				f -> f.getName().endsWith(".dad")
        );
		if (files.size() < 1) {
			throw new Exception("Found no DAD file");
		}
		if (files.size() > 1) {
			throw new Exception("Found more than one DAD file");
		}
		return files.get(0);

	}
	
	public String appName() throws Exception {
		DAD dad = dad();
		return dad.getAppName();
	}
	
	public void loadDimeProperties() {
		IProject project = null;
		try {
			project = dadFile().getProject();
		} catch(Exception e) {
			e.printStackTrace();
			IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
			for (IProject p : projects) {
				if (p.isAccessible() && p.getFile(DimeProperties.PROPERTIES_FILE_NAME).exists()) {
					// FIXME handle multiple properties files in different projects
					project = p;
					break;
				}
			}
		}
		if (project != null) {
			DimeProperties.getInstance().load(project);
		}
	}
}
