package info.scce.dime.ui.deployment.view;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;

abstract public class WorkspaceListener implements IResourceChangeListener {

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = event.getDelta();
		if (delta != null) try {
			delta.accept(new IResourceDeltaVisitor() {
				public boolean visit(IResourceDelta delta) throws CoreException {
					final IResource resource = delta.getResource();
					if (resource instanceof IProject) {
						IProject project = (IProject) resource;
						if (delta.getKind() == IResourceDelta.CHANGED
								&& delta.getFlags() == IResourceDelta.OPEN) {
							// project has been opened
							onProjectChanged(project);
							return false; // do not visit children
						}
					} else if (resource instanceof IFile) {
						IFile file = (IFile) resource;
						if ("dad".equals(file.getFileExtension())
								&& delta.getKind() == IResourceDelta.ADDED) {
							// .dad file has been added (DIME project has been created)
							onProjectChanged(file.getProject());
							return false; // do not visit children
						}
					}
					return true;
				}
			});
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	abstract void onProjectChanged(IProject project);
}