package info.scce.dime.ui.deployment;

import info.scce.dime.ui.deployment.os.OperatingSystemDetector;
import info.scce.dime.ui.deployment.view.Workspace;
import info.scce.gantry.Project;
import info.scce.gantry.impl.DockerClientFactory;
import info.scce.gantry.impl.DefaultDockerClientFactory;
import info.scce.gantry.impl.DockerProject;
import info.scce.gantry.impl.WindowsDockerClientFactory;

public class ProjectFactory {
	private final OperatingSystemDetector operatingSystemDetector;
	private final Workspace workspace;
	private final String projectName;

	public ProjectFactory(OperatingSystemDetector operatingSystemDetector, Workspace worspace, String projectName) {
		this.operatingSystemDetector = operatingSystemDetector;
		this.workspace = worspace;
		this.projectName = projectName;
	}

	public Project project() throws Exception {
		DockerClientFactory dockerClientFactory = new DefaultDockerClientFactory();
		if (operatingSystemDetector.isWindows()) {
			dockerClientFactory = new WindowsDockerClientFactory();
		}
		return new DockerProject(workspace.targetFolder(), projectName, dockerClientFactory.build());
	}
}
