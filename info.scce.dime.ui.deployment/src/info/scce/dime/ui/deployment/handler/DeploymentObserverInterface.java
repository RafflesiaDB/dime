package info.scce.dime.ui.deployment.handler;

import info.scce.dime.ui.deployment.DeploymentStateEnum;

public interface DeploymentObserverInterface {
	void updateRuntimeEnvironment(DeploymentStateEnum online);

	void updateApplication(DeploymentStateEnum online);
}
