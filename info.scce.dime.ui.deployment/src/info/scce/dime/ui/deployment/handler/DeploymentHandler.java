package info.scce.dime.ui.deployment.handler;

import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.ui.deployment.handler.threads.AppGenerateThread;
import info.scce.dime.ui.deployment.view.DeploymentView;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class DeploymentHandler {

	private final DeploymentView deploymentView;
	private Display display;
	private final AbstractDeployment runtimeEnvironmentDeployment;
	private final AbstractDeployment applicationDeployment;

	public DeploymentHandler(
			DeploymentView deploymentView,
			RuntimeEnvironmentDeployment runtimeEnvironmentDeployment,
			ApplicationDeployment applicationDeployment
	) {
		this.deploymentView = deploymentView;
		this.runtimeEnvironmentDeployment = runtimeEnvironmentDeployment;
		this.applicationDeployment = applicationDeployment;
	}

	public void start() {
		JobFactory.job("Starting runtime environment", false).consume(20)
				.task(() -> deploymentView.disableButtons())
				.task(this::runtimeEnvironmentStart)
				.onFinished(() -> showInfoMessage("Runtime environment started", "Runtime environment started."))
				.onFailed(() -> showErrorMessage("Error starting Runtime environment", "Error starting Runtime environment."))
				.onDone(() -> deploymentView.enableButtons()).schedule();
	}

	public void stop() {
		JobFactory.job("Stoping runtime environment", false).cancelOnFail(true).consume(20)
				.task(() -> deploymentView.disableButtons())
				.task(this::applicationStop)
				.task(this::runtimeEnvironmentStop)
				.task(this::applicationRemove)
				.task(this::runtimeEnvironmentRemove)
				.onFailedShowMessage("Runtime environment stop failed.")
				.onFinishedShowMessage("Runtime environment stopped successfully")
				.onDone(() -> deploymentView.enableButtons()).schedule();
	}

	public void redeploy() {
		JobFactory.job("Redeploy Application", false).consume(20)
				.task(() -> deploymentView.disableButtons())
				.task("Stopping web application", this::applicationStop)
				.task("Removing web application", this::applicationRemove)
				.task("Generating model code", this::generateCode)
				.task("Refreshing workspace", this::refreshWorkspace)
				.task("Starting web application", this::applicationStart)
				.onFinished(() -> showInfoMessage("Success", "Application build finished. Wait until the app is deployed."))
				.onCanceledShowMessage("Redeploy canceled.").onFailedShowMessage("Redeploy failed.")
				.onDone(() -> deploymentView.enableButtons()).schedule();
	}

	public void purgeAndDeploy() {
		JobFactory.job("Purge and redeploy", false).consume(20)
				.task(() -> deploymentView.disableButtons())
				.task("Stopping web application", this::applicationStop)
				.task("Removing web application", this::applicationRemove)
				.task("Stopping runtime", this::runtimeEnvironmentStop)
				.task("Removing runtime", this::runtimeEnvironmentRemove)
				.task("Deleting model code", this::appPurge)
				.task("Starting runtime", this::runtimeEnvironmentStart)
				.task("Generate model code", this::generateCode)
				.task("Refreshing workspace", this::refreshWorkspace)
				.task("Starting web application", this::applicationStart)
				.onFinished(() -> showInfoMessage("Success", "Purge and Deploy finished. Wait until the app is deployed."))
				.onCanceledShowMessage("Purge and Deploy canceled.").onFailedShowMessage("Purge and Deploy failed.")
				.onDone(() -> deploymentView.enableButtons()).schedule();
	}
	
	private void refreshWorkspace() {
		try {
			ResourcesPlugin.getWorkspace().getRoot().refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		} catch (CoreException e) {
			throw new RuntimeException("Failed refreshing orkspace", e);
		}
	}

	private void generateCode() {
		CompoundJob job = new AppGenerateThread().getGeneratorJob();
		job.setUser(false);
		job.schedule();
		try {
			job.join();
			IStatus status = job.getResult();
			if (status.getSeverity() == IStatus.ERROR)
				throw new RuntimeException(status.getException());
			if (status.getSeverity() == IStatus.CANCEL)
				throw new RuntimeException("Model generation canceled by user");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void appPurge() {
		CompoundJob job = JobFactory.job("Purging app", false);
		job.consume(100, "Initializing...")
			.task("Deleting target folder", () -> {
				AppGenerateThread appGen = new AppGenerateThread();
				appGen.prepare();
				DAD dad = appGen.dad;
				if (dad == null) {
					throw new RuntimeException(appGen.dadStatus.getMessage());
				}
				WorkspaceExtension helper = new WorkspaceExtension();
				IProject project = helper.getProject(dad);
				if (project == null) {
					throw new RuntimeException("Failed to retrieve project of DAD: " + dad);
				}
				IFolder folder = project.getFolder("target");
				if (folder.exists()) try {
					folder.delete(true, null);
				} catch (CoreException e) {
					throw new RuntimeException(e);
				}
			})
			.schedule();
		try {
			job.join();
			IStatus status = job.getResult();
			if (status.getSeverity() == Status.ERROR)
				throw new RuntimeException(status.getException());
			if (status.getSeverity() == Status.CANCEL)
				throw new RuntimeException("App purge canceled by user");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void applicationRemove() {
		this.applicationDeployment.removeContainer();
	}
	
	private void applicationStop() {
		this.applicationDeployment.stopContainer();
	}

	private void applicationStart() {
		this.applicationDeployment.startContainer();
	}

	private void runtimeEnvironmentRemove() {
		this.runtimeEnvironmentDeployment.removeContainer();
	}

	private void runtimeEnvironmentStop() {
		this.runtimeEnvironmentDeployment.stopContainer();
	}

	private void runtimeEnvironmentStart() {
		this.runtimeEnvironmentDeployment.startContainer();
	}

	private void showInfoMessage(String title, String message) {
		if (display == null) {
			display = Display.getDefault();
		}
		display.syncExec(() -> MessageDialog.openInformation(display.getActiveShell(), title, message));
	}

	private void showErrorMessage(String title, String message) {
		if (display == null) {
			display = Display.getDefault();
		}
		display.syncExec(() -> MessageDialog.openError(display.getActiveShell(), title, message));
	}
}
