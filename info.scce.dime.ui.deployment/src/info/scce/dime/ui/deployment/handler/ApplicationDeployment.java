package info.scce.dime.ui.deployment.handler;

import info.scce.gantry.Container;
import java.util.Arrays;
import java.util.List;

public class ApplicationDeployment extends AbstractDeployment {

	public ApplicationDeployment(DeploymentObserverInterface observer, List<Container> containerList) {
		super(Arrays.asList(observer), containerList);
	}

	@Override
	protected void updateObserver(DeploymentObserverInterface observer) {
		observer.updateApplication(deploymentState);
	}
}
