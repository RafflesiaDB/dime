package info.scce.dime.ui.deployment.handler.threads;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement;
import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.plugin.generator.runtime.registry.GeneratorDiscription;
import de.jabc.cinco.meta.plugin.generator.runtime.registry.GraphModelGeneratorRegistry;
import de.jabc.cinco.meta.runtime.xapi.FileExtension;
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension;
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension;
import graphmodel.GraphModel;
import info.scce.dime.dad.dad.DAD;

//TODO convert to Xtend to use extension providers more elegantly
public class AppGenerateThread {
 
	protected WorkspaceExtension _workspaceExtension = new WorkspaceExtension();
	protected FileExtension _fileExtension = new FileExtension();
	protected ResourceExtension _resourceExtension = new ResourceExtension();
	
	
//	public AppGenerateThread(String name) {
//		super(name);
//	}

	public DAD dad;
	public IStatus dadStatus;
	
	private String jobName = "Code Generator Job";
	
	private final String errorPrefix = "Model code generation failed with error:\n";
	private GeneratorDiscription<GraphModel> generatorDescription;
	
	public IStatus getStatus() {
		return dadStatus;
	}
	

//	@Override
	public void prepare() {
		PlatformUI.getWorkbench().getDisplay().syncExec(() -> { 
			getDADForActiveEditor();
			if (dad == null) {
				searchForDad();
			}
		});
		
	}
	
//	@Override
//	protected void repeat() {
//		if (dadStatus != null) {
//			if (dadStatus.equals(Status.OK_STATUS)) {
//				// Get generator over runtime 
//				GeneratorDiscription<GraphModel> dadGenerator = GraphModelGeneratorRegistry.INSTANCE.getGeneratorAt(DAD.class.getName(), 0);
////				Generator generator = new Generator();
//				IPath outlet = getOutlet();
////				generator.generate(dad, outlet, null);
//				dadGenerator.getGenerator().collectTasks(dad, outlet, job);
//			}
//			quit(dadStatus);
//		} 
//	}

	public CompoundJob getGeneratorJob() {
		CompoundJob job = JobFactory.job("Code Generation", false);
		
		job.consume(100, "Initializing...")
		   .task("Retrieve DAD model", () -> prepare())
		   .task("Retrieve generator", this::retrieveGenerator)
		   .cancelIf(this::isGeneratorMissing, "Failed to retrieve generator.\n\n"
					+ "Either this type of model is not associated with a generator "
					+ "or something went seriously wrong. In the latter case, try to "
					+ "restart the application.");
		job.schedule();
		
		try {
			job.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		IStatus result = job.getResult();
		if (Status.OK != result.getSeverity()) {
			return null;
		}
		
		job = JobFactory.job("Code Generation")
				.label("Generating for model " + dad.getAppName() + " ...");
		
		generatorDescription.getGenerator().collectTasks(dad, getOutlet(), job);
		
		return job;
	}
	
	private void retrieveGenerator() {
		try {
			Class<?> clazz = 
				(dad instanceof CModelElement)
					? dad.getClass().getSuperclass()
					: dad.getClass();
			String graphModelClassName = 
					clazz.getName().replace("Impl", "").replace(".impl", "");
			List<GeneratorDiscription<GraphModel>> generatorDescriptions =
					GraphModelGeneratorRegistry.INSTANCE.getAllGenerators(graphModelClassName);
			if (generatorDescriptions != null && !generatorDescriptions.isEmpty()) 
				generatorDescription = generatorDescriptions.get(0);
		} catch(Exception e) {
			throw new RuntimeException("Failed to retrieve generator.", e);
		}
	}
	
	private boolean isGeneratorMissing() {
		return generatorDescription == null
				|| generatorDescription.getGenerator() == null;
	}
	
	private IPath getOutlet() {
		URI uri = dad.eResource().getURI();
		IResource iRes = ResourcesPlugin.getWorkspace().getRoot().findMember(uri.toPlatformString(true));
		if (iRes != null)
			return iRes.getProject().getLocation().append("target").append("dywa-app");
		return null;
	}

	private void getDADForActiveEditor() {
		IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (workbenchWindow == null) {
			dadStatus = new Status(Status.ERROR, jobName, errorPrefix + "Workbench window is null");
			return;
		}
			
		IWorkbenchPage page = workbenchWindow.getActivePage();
		if (page == null) {
			dadStatus = new Status(Status.ERROR, jobName, errorPrefix + "Page is null");
			return;
		}
		
		IEditorPart editor = page.getActiveEditor();
		if (editor == null) {
			dadStatus = new Status(Status.ERROR, jobName, errorPrefix + "No DAD Model found! Open the \"todo.dad\" file!");
			return;
		}
		
		Resource res = getResource(editor);
		if (res != null) {
			DAD eObject = _resourceExtension.getGraphModel(res, DAD.class);
			if (eObject != null) {
				dad = (DAD) eObject;
				dadStatus = Status.OK_STATUS;
				return;
			}
		}
		dadStatus = new Status(Status.ERROR, jobName, errorPrefix + "No DAD Model found! Open the \"todo.dad\" file!");
	}
	
	private void searchForDad() {
		List<IFile> files = _workspaceExtension.getFiles(_workspaceExtension.getWorkspaceRoot(), f -> f.getName().endsWith(".dad"));
		if (files.size() == 1) {
			IFile dadFile = files.get(0);
			
			DAD dadModel = _fileExtension.getGraphModel(dadFile, DAD.class);
			
			if (dadModel == null) {
				dadStatus = new Status(Status.ERROR, jobName, errorPrefix + "Could not load DAD model from file :\"" + dadFile +"\"");
			} else {
				dadStatus = Status.OK_STATUS;
				dad = dadModel;
			}
		} else {
			dadStatus = new Status(Status.ERROR, jobName, errorPrefix + "Found \"" + files.size() + "\" .dad files.");
		}
	}
	
	private Resource getResource(IEditorPart editor) {
		EditingDomain ed = getEditingDomain(editor);
		if (ed != null)
			return ed.getResourceSet().getResources().get(0);
		return null;
	}
	
	private EditingDomain getEditingDomain(IEditorPart editor) {
		return editor instanceof IEditingDomainProvider 
			? ((IEditingDomainProvider) editor).getEditingDomain()
			: editor instanceof DiagramEditor
				? ((DiagramEditor) editor).getEditingDomain()
				: null;
	}
	
}
