package info.scce.dime.ui.deployment;

public enum DeploymentStateEnum {
	STOPPED,
	PROCESSING,
	RUNNING;
}
