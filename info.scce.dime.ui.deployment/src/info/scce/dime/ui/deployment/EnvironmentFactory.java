package info.scce.dime.ui.deployment;

import java.util.Arrays;
import java.util.Collections;

import info.scce.gantry.Project;
import info.scce.gantry.Environment;

public class EnvironmentFactory {

	private final ProjectFactory projectFactory;

	public EnvironmentFactory(ProjectFactory projectFactory) {
		this.projectFactory = projectFactory;
	}

    public Environment environment() throws Exception {
	    Project project = projectFactory.project();
        return project.environment(
                project.container(
                        project.pulledImage("registry.gitlab.com/scce/docker-images/postgres-dime:9.5.4"),
                        project.containerName("postgres"),
                        project.containerConfig(
                                Arrays.asList(
                                        project.environmentVariable(
                                                "POSTGRES_DB",
                                                "dywa"
                                        ),
                                        project.environmentVariable(
                                                "POSTGRES_USER",
                                                "sa"
                                        ),
                                        project.environmentVariable(
                                                "POSTGRES_PASSWORD",
                                                "sa"
                                        )
                                ),
                                Collections.emptyList(),
                                Collections.singletonList(
                                        project.portSpec(
                                                "127.0.0.1:5432",
                                                "5432"
                                        )
                                ),
                                Collections.emptyList()
                        )
                ),
                project.container(
                        project.pulledImage("registry.gitlab.com/scce/docker-images/mailcatcher:0.7.1"),
                        project.containerName("mailcatcher"),
                        project.containerConfig(
                                Collections.emptyList(),
                                Collections.emptyList(),
                                Collections.singletonList(
                                        project.portSpec(
                                                "127.0.0.1:8888",
                                                "1080"
                                        )
                                ),
                                Collections.emptyList()
                        )
                ),
                project.container(
                        project.builtImage("dywa-app", "development.Dockerfile"),
                        project.containerName("dywa-app"),
                        project.containerConfig(
                                Arrays.asList(
                                        project.environmentVariable(
                                                "DIME_APP_ENCRYPT",
                                                "false"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_MAIL_PORT",
                                                "1025"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_MAIL_SERVER",
                                                "mailcatcher"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_SERVER_TIER",
                                                "local"
                                        ),
                                        project.environmentVariable(
                                                "DIME_APP_SERVER_URL",
                                                "http://127.0.0.1:8080/"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_SERVER_NAME",
                                                "postgres"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_DATABASE_NAME",
                                                "postgres"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_USER",
                                                "sa"
                                        ),
                                        project.environmentVariable(
                                                "DATASOURCE_PASSWORD",
                                                "sa"
                                        ),
                                        project.environmentVariable(
                                                "JAVA_OPTS",
                                                "-Xms64m -Xmx2g -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=512m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
                                        )
                                ),
                                Arrays.asList(
                                        project.link("postgres"),
                                        project.link("mailcatcher")
                                ),
                                Collections.emptyList(),
                                Collections.emptyList()
                        )
                ),
                project.container(
                        project.builtImage("webapp", "development.Dockerfile"),
                        project.containerName("webapp"),
                        project.containerConfig(
                                Collections.emptyList(),
                                Collections.emptyList(),
                                Collections.emptyList(),
                                Collections.emptyList()
                                )
                ),
                project.container(
                        project.pulledImage("nginx:1.17.2"),
                        project.containerName("nginx"),
                        project.containerConfig(
                                Collections.emptyList(),
                                Arrays.asList(
                                        project.link("webapp"),
                                        project.link("dywa-app")
                                ),
                                Collections.singletonList(
                                        project.portSpec(
                                                "127.0.0.1:8080",
                                                "80"
                                        )
                                ),
                                Arrays.asList(
                                		project.volumeBinding(
                                				"nginx/nginx.conf",
                                				"/etc/nginx/nginx.conf:ro"
                                		)
                                )                        
                       )
                )
       );
    }
}
