const { description } = require('../../package')
const env = require('./env.default.js');

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'DIME Wiki',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * The URL where the docs are deployed
   */
  base: env.BASE,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  markdown: {
    toc:  {
      includeLevel: [2, 3]
    },
    lineNumbers: true
  },

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: true,
    sidebarDepth: 1,
    nav: [
      {
        text: 'DIME@GitLab',
        link: 'https://gitlab.com/scce/dime'
      }
    ],
    sidebar: [
      ['content/introduction/', 'Introduction'],
      {
        title: 'User Guide',
        children: [
          ['content/user-guide/concepts.md', 'Concepts'],
          {
            title: 'Data Model',
            initialOpenGroupIndex: -1,
            children: [
              'content/user-guide/data-model/create-associations.md',
              'content/user-guide/data-model/create-enums.md',
              'content/user-guide/data-model/create-types.md',
              'content/user-guide/data-model/use-enumerations.md',
              'content/user-guide/data-model/use-inheritance.md',
            ]
          },
          {
            title: 'GUI Model',
            initialOpenGroupIndex: -1,
            children: [
              'content/user-guide/gui/',
              'content/user-guide/gui/gui-docs-alert.md',
              'content/user-guide/gui/gui-docs-bottom-nav-bar.md',
              'content/user-guide/gui/gui-docs-btn-group.md',
              'content/user-guide/gui/gui-docs-btn-toolbar.md',
              'content/user-guide/gui/gui-docs-button.md',
              'content/user-guide/gui/gui-docs-checkbox.md',
              'content/user-guide/gui/gui-docs-col.md',
              'content/user-guide/gui/gui-docs-combobox.md',
              'content/user-guide/gui/gui-docs-container-height.md',
              'content/user-guide/gui/gui-docs-current-user.md',
              'content/user-guide/gui/gui-docs-data-context.md',
              'content/user-guide/gui/gui-docs-description.md',
              'content/user-guide/gui/gui-docs-description-entry.md',
              'content/user-guide/gui/gui-docs-dropdown.md',
              'content/user-guide/gui/gui-docs-embedded.md',
              'content/user-guide/gui/gui-docs-expressions.md',
              'content/user-guide/gui/gui-docs-field.md',
              'content/user-guide/gui/gui-docs-file.md',
              'content/user-guide/gui/gui-docs-for.md',
              'content/user-guide/gui/gui-docs-form.md',
              'content/user-guide/gui/gui-docs-headline.md',
              'content/user-guide/gui/gui-docs-icons.md',
              'content/user-guide/gui/gui-docs-if.md',
              'content/user-guide/gui/gui-docs-image.md',
              'content/user-guide/gui/gui-docs-inputs.md',
              'content/user-guide/gui/gui-docs-jumbotron.md',
              'content/user-guide/gui/gui-docs-list-entry.md',
              'content/user-guide/gui/gui-docs-listing.md',
              'content/user-guide/gui/gui-docs-margin.md',
              'content/user-guide/gui/gui-docs-page-up.md',
              'content/user-guide/gui/gui-docs-panel.md',
              'content/user-guide/gui/gui-docs-placeholder.md',
              'content/user-guide/gui/gui-docs-primitive-for.md',
              'content/user-guide/gui/gui-docs-primitive-variable.md',
              'content/user-guide/gui/gui-docs-progress-bar.md',
              'content/user-guide/gui/gui-docs-radio.md',
              'content/user-guide/gui/gui-docs-row.md',
              'content/user-guide/gui/gui-docs-static-content.md',
              'content/user-guide/gui/gui-docs-styling.md',
              'content/user-guide/gui/gui-docs-tab.md',
              'content/user-guide/gui/gui-docs-tabbing.md',
              'content/user-guide/gui/gui-docs-table.md',
              'content/user-guide/gui/gui-docs-table-entry.md',
              'content/user-guide/gui/gui-docs-text.md',
              'content/user-guide/gui/gui-docs-thumbnail.md',
              'content/user-guide/gui/gui-docs-todolist.md',
              'content/user-guide/gui/gui-docs-tooltip.md',
              'content/user-guide/gui/gui-docs-top-nav-bar.md',
              'content/user-guide/gui/gui-docs-var-attr.md',
            ]
          },
          {
            title: 'Process Model',
            initialOpenGroupIndex: -1,
            children: [
              'content/user-guide/processes/add-to-list.md',
              'content/user-guide/processes/create-process-model.md',
              'content/user-guide/processes/define-signature.md',
              'content/user-guide/processes/iterate-type-instances.md',
              'content/user-guide/processes/read-and-update-variables.md',
              'content/user-guide/processes/retrieve-type-instances.md',
              'content/user-guide/processes/reuse-process.md',
            ]
          },
          ['content/user-guide/development-with-docker.md', 'App Development'],
          {
            title: 'Advanced',
            initialOpenGroupIndex: -1,
            children: [
              'content/user-guide/advanced/creating-a-native-library.md',
              'content/user-guide/advanced/define-native-sib.md',
              'content/user-guide/advanced/generator.md',
              'content/user-guide/advanced/gui-data-flow.md',
              'content/user-guide/advanced/gui-dispatching.md',
              'content/user-guide/advanced/gui-major-minor.md',
              'content/user-guide/advanced/gui-second-order.md',
              'content/user-guide/advanced/performance-optimization.md'
            ]
          },
          ['content/user-guide/examples.md', 'Examples'],
          ['content/user-guide/common-pitfalls/', 'Common Pitfalls']
        ]
      },
      {
        title: 'Operator Guide',
        initialOpenGroupIndex: -1,
        children: [
          'content/operator-guide/headless-generation.md',
          'content/operator-guide/testing-with-alex.md'
        ]
      },
      {
        title: 'Developer Guide',
        children: [
          ['content/developer-guide/development-without-docker.md', 'App Development'],
          ['content/developer-guide/build-dime.md', 'Build DIME'],
          {
            title: 'Extensions',
            initialOpenGroupIndex: -1,
            children: [
              'content/developer-guide/extensions/alternate-generator.md',
              'content/developer-guide/extensions/generic-sibs.md'
            ]
          },
          {
            title: 'Contribute',
            initialOpenGroupIndex: -1,
            children: [
              'content/developer-guide/contribute/frontend.md',
            ]
          },
          ['content/developer-guide/common-pitfalls/', 'Common Pitfalls']
        ]
      }
    ]
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom'
  ]
}
