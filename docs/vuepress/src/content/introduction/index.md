# Introduction

DIME is a programming less environment to easily design, accomplish and contribute your ideas for a Web Application.

![dime-introduction](./assets/dime-introduction.png)

## Overview

![dime-overview](./assets/dime-overview.svg)

## Download the Installer

1. Download an installer:
    * Daily builds: [https://ls5download.cs.tu-dortmund.de/dime/daily/](https://ls5download.cs.tu-dortmund.de/dime/daily/)
    * Latest builds:
        * Linux: [https://ls5download.cs.tu-dortmund.de/dime/daily/dime-latest-linux.zip](https://ls5download.cs.tu-dortmund.de/dime/daily/dime-latest-linux.zip)
        * MacOS: [https://ls5download.cs.tu-dortmund.de/dime/daily/dime-latest-macosx.zip](https://ls5download.cs.tu-dortmund.de/dime/daily/dime-latest-macosx.zip)
        * Windows: [https://ls5download.cs.tu-dortmund.de/dime/daily/dime-latest-win32.zip](https://ls5download.cs.tu-dortmund.de/dime/daily/dime-latest-win32.zip)
2. Unzip the archive
3. Run `dime` or `dime.exe` depending on your OS

**Used Technologies**
* [Java](https://www.java.com/)
* [Eclipse](https://www.eclipse.org/)
* [XTend](https://eclipse.org/xtend/documentation/)
* [Angular](https://angular.io/)
* [Dart](https://www.dartlang.org/)
