# Frontend

The Frontend is written in [Dart](https://dart.dev/) and based on the [Angular Dart](https://github.com/dart-lang/angular) package.
Its source code is separated into static and generated files.

## Static Sources

Location: `info.scce.dime.generator/static-dart-resources/generated-sources/webapp`

The static source code follows basically the [Effective Dart Specification](https://dart.dev/guides/language/effective-dart/documentation) and complies with [guidelines and best practices](https://pub.dev/packages/pedantic) of Google.
The directory layout is compatible with the official [Pub Package Layout](https://dart.dev/tools/pub/package-layout).

### Development Environment

**Preliminaries**

1. Install Dart SDK according to [Getting Started](/content/getting-started/DIME-2-development-with-maven.md#dart-sdk).
2. Resolve dependencies: `pub get`
3. Open `info.scce.dime.generator/static-dart-resources/generated-sources/webapp` as a project in your favorite [Dart compatible IDE](https://dart.dev/tools).

**Pre Commit**

Performing the following steps will reduce the likelihood of breaking the [CI/CD Pipeline](https://gitlab.com/scce/dime/-/pipelines).

1. Analyze source code against rules of [Pedantic](https://pub.dev/packages/pedantic): `dartanalyzer .`
2. Check if source code is well formatted: `dartfmt --dry-run .`
3. Run tests: `pub run test .`
