# Native Library

This tutorial will guide you through the process of creating a library of native service interfaces. It provides you with a best practice approach to integrate it into the build process of your DIME app.

## Create a Service Library Project

The basic approach is to create a Maven project that comprises the service interface and integrate this project into the Maven build of the DIME application.

* For this tutorial the basic structure of the required projects is provided as an archive:

[app-dependency.zip](/dime/assets/downloads/app-dependency.zip)

* Download the archive and extract its contents to the folder "dependency" in your app project. The archive comprises the folders "app-addon" and "app-native" as well as a "pom.xml" file to replace the default one in the folder "dependency".

![dependency](./assets/dependency.png)

* Import the project "app-native" to the workspace by selecting "Import... > Existing Projects into Workspace" from the context menu of the Project Explorer. In the upcoming Wizard select the location of the "dependency" folder of your app project as root directory, select the project "Demo-Native" and hit "Finish". The workspace should now show the project "Demo-Native" besides your DIME app project.

![workspace](./assets/workspace.png)

* Find the Java class "Native" in the package "info.scce.dime.app.demo" in the folder "src/main/java" of the project "Demo-Native". It holds a simple static method "sayHello" with a single argument "name" of type String. This method prints a message to the console on execution and will be used as an example for a simple service.

```
public static void sayHello(String name) {
  System.out.println("---------------------");
  System.out.println("  Hello " + name + "!");
  System.out.println("---------------------");
}
```

* Create the file "app.sibs" in the folder "dime-models" of your app project by choosing "New > File" from its context menu.

* Open the file "app.sibs" and define a package name as well as a SIB that references the "sayHello" method in the class "Native" that has been created above:

```
package app.demo

sib SayHello : info.scce.dime.app.demo.Native#sayHello
  name : text
  -> success
```

* Save the file "app.sibs". Open the view "Control" and find the newly created SIB under "Native/app.sibs/SayHello".

![control](./assets/control.png)

* Open the file "Startup.process" (or any other process model) and insert the SayHello-SIB by dragging it from the Control view and dropping it in the process editor. Integrate the SIB into the control flow by connecting an incoming and outgoing edge.

* Right-click on the Input Port of the SayHello-SIB instance and select "Convert Input to Static Value". Select the static Input Port and enter a name as its value via the "Cinco Properties" view. Save the process model.

![sib](./assets/sib.png)

* In the "Deployment View" hit "Redeploy" or "Purge and Deploy" to build and deploy the app including the native library.

* As soon as the app is deployed have a look at the logs of the Wildfly application server to find the message printed by the sayHello method. If you integrated it in "Startup.process" it is called during the startup of the app.

```
INFO  [stdout] (ServerService Thread Pool -- 65) ---------------------
INFO  [stdout] (ServerService Thread Pool -- 65)   Hello Steve!
INFO  [stdout] (ServerService Thread Pool -- 65) ---------------------
```

* Whenever the service library changes make sure to add eventual new dependencies to the Maven configuration, i.e. the "pom.xml" file of the native library project.

* Any changes to the servie library will be considered on re-deploying the app (and hence integrated into the build process) because the contents of the "dependency" folder of your app project is copied to the folder "target/dywa-app/app-addon" during the app generation step.

## Additional Service Libraries

Follow these steps if you want to replace the minimal native library project from this tutorial with your own project or to add an additional one.

* Create a Maven project in the "dependency" folder of your app project by selecting "New > Other... > Maven Project". In the upcoming Wizard:

  * Mark "Create a simple project (skip archetype selection)"
  * As location create a new sub-folder in the "dependency" folder of your app project ("my-service-lib" in this tutorial) and hit "Next >"
  * Define the Group Id ("info.scce.dime.app.demo" in this tutorial)
  * Define the Artifact Id ("my-service-lib" in this tutorial)
  * Define the version "1.0-SNAPSHOT"
  * Define the parent Group Id: info.scce.dime
  * Define the parent Artifact Id: app-addon-parent
  * Define the parent Version: 1.0-SNAPSHOT
  * Hit "Finish" to create your new service library project in the workspace

* Open the "pom.xml" file in folder "dependency/app-addon" of your app project and add your new project as dependency to the "dependencies" section:

```
<dependency>
  <groupId>info.scce.dime.app.demo</groupId>
  <artifactId>my-service-lib</artifactId>
  <version>1.0-SNAPSHOT</version>
  <type>jar</type>
  <scope>compile</scope>
</dependency>
```

* Open the "pom.xml" file in folder "dependency" of your app project and add your new project to the "modules" section in order to include it in the Maven build process:

```
<modules>
  <module>app-addon</module>
  <module>app-native</module>
  <module>my-service-lib</module>
</modules>
```

* Implement the services you need and create additional Native SIBs on demand. If done, run "Redeploy" or "Purge and Deploy" to build your DIME app including the new service library.
