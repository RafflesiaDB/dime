# TopNavBar

## Attributes

* *inverted*: If true, the TopNavBar is colored in dark gray. If fase, the TopNavBar is colored in light grey.

## Description

The TopNavBar can be separated in a right and a left part. 
Besides these parts a label-image can be placed in the TopNavBar.

The other content can only be added to this parts.
The content of the right part are right aligned.
The content of the left part are left aligned.
