# Dropdown

## Attributes

* *label*: The  label of the Dropdown.
* *size*: The size of the field and its content from XSmall to Large
* *coloring*: The Color of the Field and its content.
* *disabled*: If true, no content is displayed and the content can be changed
* *dropUp*: If true, the Dropdown will drop up instead of down when it is clicked
* *split*: If true, the entries are separated by lines

## Description

The Dropdown is used to group Buttons depending on a specified _label_.
When the labeled button is clicked the other buttons show up as entries.
The specific style configuration of the contained buttons are partial ignored.

