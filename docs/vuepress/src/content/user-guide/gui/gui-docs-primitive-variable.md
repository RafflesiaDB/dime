# PrimitiveVariable

## Attributes

* *name*: The name of the variable, which can be used in Expressions as well
* *isInput*: If true, the variable results as an input port in all corresponding GUISIB where the GUI is used.
* *isList*: If true, the variable is a list of the primitive data type
* *dataType*: The primitive data type of the variable: _Integer_, _Real_, _Boolean_, _Text_, _Timestamp_ or _File_

## Description

Primitive Variables can be used to store primitive data in a GUI Model.