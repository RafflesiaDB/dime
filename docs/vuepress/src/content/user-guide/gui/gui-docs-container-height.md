# ContainerHeight

## Description

Every Component has the attribute _tallness_, which is set to *Fitting* by default.
When the value is changed, the Component has a maximal height. When this height is reached a Scrollbar is added to the Component.