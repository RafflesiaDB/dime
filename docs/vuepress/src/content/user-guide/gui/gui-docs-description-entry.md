# Descriptionentry

## Attributes

* *content*: [StaticContent](gui-docs-static-content)
* *description*: [StaticContent](gui-docs-static-content)

## Description

The Descriptionentry Component can be used in Description Components to render entries of key value pairs.
The _content_ attribute describes the value.
The _description_ attributes describes the key.
To use the Descriptionentry to display variable key-values, use an _Expression_ in the _content_ and the _description_.
To display the values of a list variable connect the list variable to the Descriptionentry Component using an Iteration edge.