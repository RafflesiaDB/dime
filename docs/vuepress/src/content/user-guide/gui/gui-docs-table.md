# Table

## Attributes

* *striped*: If true, every second row is highlighted to enabled better readability
* *bordered*: If true, every cell is bordered
* *hover*: If true, a row is highlighted when the cursor is placed above it
* *codensed*: If true, the table is minimized. The horizontal space between the columns is reduced to the content size
* *responsive*: If true, the table is responsive to the screen size. When the screen is to small for the table, a horizonzal scrollbar is added.
* *numbering*: If true, a first column will be added to the table, which displays the row number.
* *pagination*: If greater than 0, a pagination is added displaying the available pages. The page size correlates to the given number.
* *choices*:
** *NONE*: nothing is added
** *SINGLE*: An additional column with radio-buttons will be added. The User can select one row
** *MULTIPLE*: An additional column with checkboxes will be added. The User can select multiple rows

## Description

The Table has to be connected to a list variable or attribute of a complex type by a TableLoad edge.
The iterator variable corresponds with the current attribute of the list variable and can be expanded to get access to their attributes.
The iterator variable can be used in the connected table and in every of their inner [TableEntry](gui-docs-table-entry).