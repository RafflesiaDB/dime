# Listing

##. Attributes

* *inline*: If true, the List-Entries will be rendered in a horizontal row
* *mode*: 
 * Unordered: The List-Entries will be displayed unordered, starting with a dot
 * Ordered: The List-Entries will be displayed ordered, starting with a number
 * Unstyled: The List-Entries will be displayed unordered, starting with a dot in the second layer
 * Listgroup: The List-Entries will be displayed in panels

## Description

The Listing can be used to display (un)ordered list content.
To add content to the Listing use the Listentry Component.