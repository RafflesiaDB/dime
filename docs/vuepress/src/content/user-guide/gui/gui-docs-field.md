# Field

## Attributes

* *label*: The displayed label of Field. If the corresponding Form is inline, then the label is on the left, otherwise on top of the field.
* *size*: The size of the field and its content from XSmall to Large
* *coloring*: The Color of the Field and its content.
* *disabled*: If true, no content is displayed and the content can be changed
* *required*: If true, content has to be typed in. Otherwise the field is invalid.
* *min*: The content' length has to be equal or more than _min_. Otherwise the field is invalid.
* *max*: If greater than -1, the content' length has to be equal or less than _max_. Otherwise the field is invalid.
* *emptyValue*: The empty value is displayed, while the field is empty. The empty value can not not submitted.
* *errorText*: This text is displayed at the bottom of the field, if it is invalid.
* *readonly*: If true, the content can be changed
* *helptext*: If filled in, the helptext is displayed under the field.
* *isList*: If true, the field accepts multiple values of the chosen _input type_
* *inputType*:
 * Text: Simple Text field
 * TextArea: Multi-line Text field. In combination with _isList_ option, the value is separated in lines and mapped to a list
 * Number
 * Color: RGB
 * Date: Depending on the Internationality. Containing the day, month and year
 * Datetime: Depending on the Internationality. Containing the day, month, year, hour and minute
 * Month: Depending on the Internationality. Containing the month and year
 * Week: Depending on the Internationality. Containing the year and week
 * Time: Depending on the Internationality. Containing the hour and minute
 * Checkbox
 * Tel: Any Phone number 
 * SimpleFile: just File Upload field
 * AdvancedFile: File Upload field with progressbars
 * URL
 * Password 
* *regex*:  [Regex](http://ecma-international.org/ecma-262/5.1/#sec-15.10) which is used for validation if filled in 

## Description

The Field has to be contained in a parent Form.
Every Field has to be connected to a Primitive Variable or Primitive Attribute in a Datacontext.
Depending on the _input type_ the following primitive data types can be connected:

* Input Type > Primitive Type
 * Text > Text
 * TextArea > Text
 * Number > Integer, Real
 * Color > Text
 * Date > Timestamp, Text
 * Datetime > Timestamp, Text
 * Month > Timestamp, Text
 * Week > Timestamp, Text
 * Time > Timestamp, Text
 * Checkbox > Boolean
 * Tel > Text
 * File > File
 * URL > Text
 * Password > Text

You can use other data bindings as well, but you have to make sure that the value of the input field is convertable to the data type.

## Data Bindings

The connection between a variable and a field is created *from the variable to the field*.
To connect a variable in the data context with a field two edges are provided:
* *FormSubmit*: The FormSubmit Edge only writes the value of the field to the variable.
* *FormLoadSubmit*: The FormLoadSubmit Edge loads the value of the variable when the page with the field is initial rendered. The value of the input field will be written to the variable on submission or synchronization.

