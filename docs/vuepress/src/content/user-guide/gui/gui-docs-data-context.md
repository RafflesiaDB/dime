# DataContext

## Description

An arbitrary number of can be added to one GUI Model.
Variables can only be placed in a DataContext.
You can resize and reposition the DataContexts to fit your needs.