# Inputs

To define inputs for a GUI model, select a major variable (not a expanded Attribute) in one of your DataContexts and set the option isInput to true.
This leads to an Input Port of the same data type and equally named in every GUISIB of the current GUI model.