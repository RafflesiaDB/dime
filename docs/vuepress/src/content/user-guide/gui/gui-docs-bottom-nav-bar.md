# BottomNavBar

## Attributes

* *inverted*: If true, the TopNavBar is colored in dark gray. If fase, the TopNavBar is colored in light grey.

## Description

The BottomNavBar should be placed in the footer or at the bottom of the body template.
It will be sticky to the bottom.
