# Alert

## Attributes

* *color*: The color of the Alert block and its content
* *content*: The displayed content. [Static Content](gui-docs-static-content)
* *dismissible*: If true, the alter can be dismissed by the user, by clicking a little close sign on the right of the rendered Alert

## Description

The Alert Component can be used to show important or optional information in colored boxes.