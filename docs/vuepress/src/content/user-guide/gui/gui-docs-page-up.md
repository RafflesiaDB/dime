# PageUp

## Attributes

* *icon* : [Icon](gui-docs-icons)

## Description

Renders as a gray circle, including the selected Icon.
When the circle is clicked the page is scrolled to the top.