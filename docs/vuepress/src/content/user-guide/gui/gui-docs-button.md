# Button

## Attributes

* *label*: The label of Button, which will be used for the corresponding branch in the parent Interaction Process. The label will only be used for the button lable, if the displayLabel is not set.
* *size*: The size of the field and its content from XSmall to Large
* *coloring*: The Color of the Field and its content.
* *disabled*: If true, no content is displayed and the content can be changed
* *displayLabel*: The label of the Button.
* *ButtonType*:
** Submit: Used in Forms to trigger the submission of all form field values
** Button: Used for normal Buttons
* *isLink*: If true, the button is rendered like a plain link
* *fullWidth*: If true the button is rendered using the entire horizontal available space
* *preIcon*: Icon rendered before the label [Icon](gui-docs-icons)
* *postIcon*: Icon rendered after the label [Icon](gui-docs-icons)
* *staticURL* If set, the button redirects to an external page.

## Description

All Buttons of a GUI are corresponding to a branch of the related GUI-SIB in a parent Interaction Process.
The Branches and Buttons are matched by the label.

To emit values of variables on a button click, create an AddComplexToSubmission for complex variables or a AddPrimitiveToSubmission edge from the variable to the button.
Every connected variable will create a corresponding output port on the related branch in the parent Interaction Process.

If the _staticURL_ is set, no Branch will accrue.
If the Button is _disabled_ , no Branch will accrue.
