# Common Pitfalls

## Insufficient memory size for Docker containers

**Appearance:** When deploying a generated DIME application with `docker-compose up` the `dywa_app` container may fail silently due to lack of memory.

**Stacktrace:** Watch out for the Wildfly to suddenly quit:

```
dywa-app_1     | *** JBossAS process (57) received KILL signal ***
```

**Reason:** The memory size available for Docker containers is insufficient.

**Solution:** Increase the available memory size in the Docker settings.

![docker-memory](./assets/docker-memory.png)

## The Deployment View fails to start

**Appearance:** When opening the *Deployment View*, the following error is reported.

```
java.lang.RuntimeException: The Deployment View is not supported on a runtime environment without Docker
	at info.scce.dime.ui.deployment.view.DeploymentView.createPartControl(DeploymentView.java:136)
	at org.eclipse.ui.internal.e4.compatibility.CompatibilityPart.createPartControl(CompatibilityPart.java:153)
	...
Caused by: info.scce.gantry.Environment$EnvironmentException: We failed connecting to the docker daemon
	at info.scce.gantry.impl.DockerEnvironment.checkPreconditions(DockerEnvironment.java:89)
	at info.scce.dime.ui.deployment.view.DeploymentView.createPartControl(DeploymentView.java:134)
	...
Caused by: java.lang.RuntimeException: java.io.IOException: native connect() failed : Permission denied
	at com.github.dockerjava.httpclient5.ApacheDockerHttpClientImpl.execute(ApacheDockerHttpClientImpl.java:162)
	at com.github.dockerjava.httpclient5.ApacheDockerHttpClient.execute(ApacheDockerHttpClient.java:8)
	...
Caused by: java.io.IOException: native connect() failed : Permission denied
	at com.github.dockerjava.httpclient5.UnixDomainSocket.connect(UnixDomainSocket.java:157)
	at org.apache.hc.client5.http.socket.PlainConnectionSocketFactory$1.run(PlainConnectionSocketFactory.java:87)
	...
```

**Observation:** Since Docker requires root privileges, the *Deployment View* fails if DIME was started by a regular user.

**Solution:** Make sure the user, which starts DIME, is part of the `docker` group. See [Note for Linux Users](../development-with-docker.md#note-for-windows-home-users).
