# App Development (Docker)

## Requirements

* Install DIME ([See here](/content/introduction/#get-dime))
* Install [Docker CE (Community Edition)](https://docs.docker.com/install/)
* Install [Docker Compose](https://docs.docker.com/compose/install/#install-compose) (only required if you intend to deploy via CLI)

### Note for Linux Users

Please follow the [Post-installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) and make sure you add your user to the `docker` group.

### Note for Windows Home Users

Please follow the [Windows Subsystem for Linux Installation Guide for Windows 10](https://docs.microsoft.com/en-us/windows/wsl/install-win10) and make sure you have the Windows Subsystem version 2 installed on your system.

Afterwards follow the [Install Docker Desktop on Windows Home](https://docs.docker.com/docker-for-windows/install-windows-home/) instructions. Also take a look at the [Docker Desktop WSL 2 backend guide](https://docs.docker.com/docker-for-windows/wsl/) to set up Docker with the WSL backend.

Please make also sure that you don't have an older version of Docker, i.e. the Docker Toolbox, installed on your system.

## Generate App

1. Open the *.dad* file
2. Generate your app by clicking the `G` button

## Deploy App

The recommended option is the **Deployment View**.

![deployment-view](./assets/deployment-view.png)

The **Runtime Environment** is built upon Mailcatcher (catches and servers mails sent by your app) and PostgreSQL (persists the data stored by your app).

The **Application** is composed of Nginx (serves as the HTTP entry point to your app), webapp (contains your GUI) and dywa-app (an API between the GUI and database).

| Service     | URL                                                        | Description                                    |
| ----------- | ---------------------------------------------------------- | -----------------------------------------------|
| DIME app    | [http://127.0.0.1:8080](http://127.0.0.1:8080)             | The application you've created                 |
| Mailcatcher | [http://127.0.0.1:8888](http://127.0.0.1:8888)             | Retrieves and displays emails sent by your app |
| PostgreSQL  | [postgresql://127.0.0.1:5432](postgresql://127.0.0.1:5432) | Contains the data your app persisted           |

### Debugging

Since the deployment view is using [Gantry](https://gitlab.com/scce/gantry) (a library to orchestrate containers for local development environments),  you can use the default Docker [commands](https://docs.docker.com/engine/reference/commandline/cli/) to inspect your deployment.

#### Containers

Run the following command to request the started containers.

```sh
$ docker ps
```

If everything worked fine, you should receive a similar output.

```
CONTAINER ID   IMAGE                                                        COMMAND                  CREATED        STATUS        PORTS                              NAMES
62105ed47102   nginx:1.17.2                                                 "/docker-entrypoint.…"   1 minute ago   Up 1 minute   127.0.0.1:8080->80/tcp               target_nginx
6b677bb0bd5f   target_webapp                                                "/bin/sh -c 'export …"   1 minute ago   Up 1 minute                                        target_webapp
859d169717fd   target_dywa-app                                              "/opt/jboss/wildfly/…"   1 minute ago   Up 1 minute   8080/tcp                             target_dywa-app
0dc776fe5d55   registry.gitlab.com/scce/docker-images/mailcatcher:0.7.1     "/bin/sh -c 'mailcat…"   1 minute ago   Up 1 minute   1025/tcp, 127.0.0.1:8888->1080/tcp   target_mailcatcher
57e80ffdd734   registry.gitlab.com/scce/docker-images/postgres-dime:9.5.4   "/docker-entrypoint.…"   1 minute ago   Up 1 minute   127.0.0.1:5432->5432/tcp             target_postgres
...
```

#### Images

Run the following command to request the existing images.

```sh
$ docker images
```

If everything worked fine, you should receive a similar output.

```
REPOSITORY                                             TAG            IMAGE ID       CREATED        SIZE
nginx                                                  1.17.2         4733136e5c3c   1 minute ago   126MB
target_webapp                                          latest         7ef8f0d1a0ca   1 minute ago   995MB
target_dywa-app                                        latest         1d70fb0e059c   1 minute ago   857MB
registry.gitlab.com/scce/docker-images/mailcatcher     0.7.1          e8836f4bdf8a   1 minute ago   874MB
registry.gitlab.com/scce/docker-images/postgres-dime   9.5.4          f942ec8c778d   1 minute ago   264MB
google/dart                                            2.4.1          a50b3ee7727d   1 minute ago   569MB
jboss/wildfly                                          14.0.1.Final   8c9bcba630f0   1 minute ago   725MB
maven                                                  3.5.4          985f3637ded4   1 minute ago   635MB
...
```

## Deploy App (Deprecated)

**Note: This option is deprecated and will be removed in a future version.**

#### Start

* Open a terminal
* Change into the project `root/target` folder
* Execute the following command to start the Docker stack:

```sh
/target$ docker-compose up
```

This command performs the following steps:

* Pulls all necessary images
* Builds the webapp and dywa-app images
* Starts all containers

Open [http://127.0.0.1:8080](http://127.0.0.1:8080) to enter your DIME app.

#### Redeploy

* Make sure you are in the project `root/target` folder
* Make sure the infrastructure is running (see above)
* Execute the following command to deploy the web app:

```sh
/target$ docker-compose up --detach --build webapp dywa-app
```

This command performs the following steps:

* Rebuilds the webapp and dywa-app images
* Restarts webapp and dywa-app containers

Refresh [http://127.0.0.1:8080](http://127.0.0.1:8080) to enter the new deployed DIME app.

#### Clear Infrastructure

* Open a terminal
* Change into the project `root/target` folder
* Execute the following command to clear the Docker stack:

```sh
/target$ docker-compose down
```

This command performs the following steps:

* Stops all containers
* Removes all containers

#### Update Dependencies

* Open a terminal
* Change into the project `root/target` folder
* Execute the following command to update the Docker stack dependencies like Nginx, Mailcatcher and PostgeSQL

```sh
/target$ docker-compose pull
```

This command performs the following steps:

* Pulls (all) necessary images

#### Managing Multiple Projects on One Machine

The different Docker containers belonging to one docker-compose project are created with a project prefix.
This shows which containers belong together and avoids naming conflicts.
Per default docker-compose chooses the current folder name as project name.
This leads to some unexpected behavior with multiple Dime projects, as all projects are created in the *target* folder and therefore have the *target* project name.
To avoid conflicts use `-p PROJECT_NAME` with the docker-compose commands, e.g.

```sh
/target$ docker-compose -p todo-app up
```

##### Bash Aliases

Aliases for commands can be defined in Bash and similar command lines.
Those aliases allow then the use of more complex commands with shorter and easier to memorize ones, e.g. `dime-up` instead of the more complex docker-compose line from above.
This can be achieved by adding the following lines to the *.bashrc* file in the home directory (or similar configuration file for other command lines)

```sh
alias dime-up='docker-compose -p $(pwd | rev | cut -d '/' -f 2 | rev) up'
alias dime-down='docker-compose -p $(pwd | rev | cut -d '/' -f 2 | rev) down --volumes'
alias dime-pull='docker-compose -p $(pwd | rev | cut -d '/' -f 2 | rev) pull'
alias dime-deploy='docker-compose -p $(pwd | rev | cut -d '/' -f 2 | rev) run --rm dywa-app -dywa-app'
alias dime-clean='docker-compose -p $(pwd | rev | cut -d '/' -f 2 | rev) down --volumes'
```

With those commands the project name is also automatically adjusted to be the Dime project name.

