package info.scce.dime.gui.preview;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.internal.EditorReference;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;
import graphmodel.GraphModel;
import info.scce.dime.generator.gui.html.HTMLPreviewTemplate;
import info.scce.dime.gui.gui.GUI;

@SuppressWarnings("restriction")
public class PartListner implements IPartListener2 {
	
	
	public PartListner() {
	}

	/**
	 * Part Listener Methods
	 */
	@Override
	public void partActivated(IWorkbenchPartReference partRef) {
//		System.out.println(this.getClass().getSimpleName() + "Part active: "
//				+ partRef.getTitle());
		loadPageByEditor(partRef);
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference partRef) {
//		System.out.println(this.getClass().getSimpleName() + "Part to top: "
//				+ partRef.getTitle());
	}

	@Override
	public void partClosed(IWorkbenchPartReference partRef) {
//		System.out.println(this.getClass().getSimpleName() + "Part closed: "
//				+ partRef.getTitle());
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference partRef) {
//		System.out.println(this.getClass().getSimpleName()
//				+ "Part deactivated: " + partRef.getTitle());
	}

	@Override
	public void partOpened(IWorkbenchPartReference partRef) {
//		System.out.println(this.getClass().getSimpleName() + "Part opened: "
//				+ partRef.getTitle());
	}

	@Override
	public void partHidden(IWorkbenchPartReference partRef) {
//		System.out.println(this.getClass().getSimpleName() + "Part hidden: "
//				+ partRef.getTitle());
		//loadPageByEditor(partRef);
	}

	@Override
	public void partVisible(IWorkbenchPartReference partRef) {
		loadPageByEditor(partRef);
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference partRef) {
//		System.out.println(this.getClass().getSimpleName()
//				+ "Part input changed: " + partRef.getTitle());
	}

	
	@SuppressWarnings({ "restriction" })
	public void loadPageByEditor(IWorkbenchPartReference partRef)
	{
		if (partRef instanceof EditorReference) {
			IEditorPart editor = ((EditorReference) partRef).getEditor(true);

			IFile file = (IFile) editor.getEditorInput()
					.getAdapter(IFile.class);
			
//			if (file == null	|| !file.getFileExtension().equals("gui"))return;
			
//			Display.getDefault().asyncExec(new Runnable() {
//				public void run() {
//					GUI gui = loadModel(file);
//					if(gui != null){
//						File f = new File(file.getProject().getLocation().append("_tmp/"+gui.getTitle()+".html").toString());
//
//						String content = new HTMLPreviewTemplate(GeneratorCompound.instanceNoSelectives()).create(gui);
//						f.getParentFile().mkdirs(); 
//						try {
//							f.createNewFile();
//							FileUtils.writeStringToFile(f,content);
//							
//						} catch (IOException e1) {
//							
//							e1.printStackTrace();
//						}
//					}
//				}
//			});
		}
		
	}
	

	public GUI loadModel(IResource res) {
		
		final long timeStart = System.currentTimeMillis();
		
		File file = new File(res.getLocation().toOSString());
		ResourceSet resSet = new ResourceSetImpl();
		org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI .createFileURI(file.getAbsolutePath());
		
		EObject eObj = ReferenceRegistry.getInstance().getGraphModelFromURI(uri);
		if (eObj != null && (eObj instanceof GUI)) {
			return (GUI) eObj;
		}
			
		Resource resource = resSet.getResource(uri, true);
		for (EObject obj : resource.getContents()) {
			if (obj instanceof GUI) {
				ReferenceRegistry.getInstance().addElement((GraphModel) obj);
	//			System.out.println("Model '" + file.getName() + "' loaded in " + (System.currentTimeMillis() - timeStart) + " ms");
				return (GUI) obj;
			}
				
		}
		System.out.println("Model " + file.getName() + " not found");
		return null;
	}

}
