package info.scce.dime.gui.preview;

import java.io.File;
import java.util.Set;

import org.apache.commons.io.FileUtils;
//import org.apache.poi.poifs.property.Child;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.browser.WebBrowserEditor;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;
import graphmodel.GraphModel;
import info.scce.dime.generator.gui.html.HTMLPreviewTemplate;
import info.scce.dime.gui.gui.GUI;

public class ResourceChangeListener implements IResourceChangeListener {

	public enum ListenerEvents {
		ADDED, REMOVED, CONTENT_CHANGE
	}

	private Set<ListenerEvents> events;
	

	public ResourceChangeListener() {
		super();
	}
	
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = event.getDelta();
		if (delta != null) {
			processAffectedFiles(delta);
		}
	}

	public void processAffectedFiles(IResourceDelta delta) {
//		for (IResourceDelta child : delta.getAffectedChildren()) {
//			IResource res = child.getResource();
//			if (res instanceof IFile) {
//				IFile iFile = (IFile) res;
//				if (hasChanged(child) && iFile.getFileExtension().equals("gui")) {
//					handleGuiChanged(iFile);
//				}
//			}
//			processAffectedFiles(child);
//		}
	}
		
	private void handleGuiChanged(IFile iFile) {
//		Display.getDefault().asyncExec(new Runnable() {
//			@SuppressWarnings("restriction")
//			public void run() {
//				GUI gui = loadModel(iFile);
//				if (gui != null) {
//					try {
//						String content = new HTMLPreviewTemplate(GeneratorCompound.instanceNoSelectives()).create(gui);
//						String guiName = gui.getTitle();
//						File f = new File(iFile.getProject().getLocation()
//								.append("_tmp/" + guiName + ".html").toString());
//						f.getParentFile().mkdirs();
//						f.createNewFile();
//						FileUtils.writeStringToFile(f, content);
//						System.out.println("Create Preview File");
//						for (IEditorReference er : PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences()) {
//							if (er.getId().equals("org.eclipse.ui.browser.editor")) {
//								IEditorPart iep = er.getEditor(false);
//								if (iep instanceof org.eclipse.ui.internal.browser.WebBrowserEditor) {
//									WebBrowserEditor wbe = (WebBrowserEditor) iep;
//									if (wbe.getPartName().endsWith(guiName)) {
//										// TODO Trigger reload of the browser
//										// view for the current edited model
//									}
//								}
//							}
//						}
//					} catch (Exception e) {
//						System.err.println("GUI Preview failed for: " + iFile);
//					}
//				}
//			}
//		});
	}
	
	private boolean hasChanged(IResourceDelta child) {
		return child.getKind() == IResourceDelta.CHANGED && ((child.getFlags() & IResourceDelta.CONTENT) != 0);
	}
	
	public GUI loadModel(IResource res) {
		
		final long timeStart = System.currentTimeMillis();
		
		File file = new File(res.getLocation().toOSString());
		ResourceSet resSet = new ResourceSetImpl();
		org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI .createFileURI(file.getAbsolutePath());
		
		EObject eObj = ReferenceRegistry.getInstance().getGraphModelFromURI(uri);
		if (eObj != null && (eObj instanceof GUI)) {
			return (GUI) eObj;
		}
			
		Resource resource = resSet.getResource(uri, true);
		for (EObject obj : resource.getContents()) {
			if (obj instanceof GUI) {
				ReferenceRegistry.getInstance().addElement((GraphModel) obj);
//				System.out.println("Model '" + file.getName() + "' loaded in " + (System.currentTimeMillis() - timeStart) + " ms");
				return (GUI) obj;
			}
				
		}
		System.out.println("Model " + file.getName() + " not found");
		return null;
	}

	private IFile getFileFromDelta(IResourceDelta delta) {
		for (IResourceDelta child : delta.getAffectedChildren()) {
			IResource res = child.getResource();
			if (res instanceof IFile) {
				IFile file = (IFile) res;
				return file;
			}
			return getFileFromDelta(child);
		}
		return null;
	}

	public class DeltaPrinter implements IResourceDeltaVisitor {
		public boolean visit(IResourceDelta delta) {
			IResource res = delta.getResource();
			switch (delta.getKind()) {
			case IResourceDelta.ADDED:
//				System.out.print("Resource ");
//				System.out.print(res.getFullPath());
//				System.out.println(" was added.");
				events.add(ListenerEvents.ADDED);
				break;
			case IResourceDelta.REMOVED:
//				System.out.print("Resource ");
//				System.out.print(res.getFullPath());
//				System.out.println(" was removed.");
				events.add(ListenerEvents.REMOVED);
				break;
			case IResourceDelta.CHANGED:
//				System.out.print("Resource ");
//				System.out.print(delta.getFullPath());
//				System.out.println(" has changed.");
				int flags = delta.getFlags();
				if ((flags & IResourceDelta.CONTENT) != 0) {
					events.add(ListenerEvents.CONTENT_CHANGE);
//					System.out.println("--> Content Change");
				}
				if ((flags & IResourceDelta.REPLACED) != 0) {
//					System.out.println("--> Content Replaced");
				}
				if ((flags & IResourceDelta.MARKERS) != 0) {
//					System.out.println("--> Marker Change");
					IMarkerDelta[] markers = delta.getMarkerDeltas();
					// if interested in markers, check these deltas
				}
				break;
			}
			return true; // visit the children
		}
	}

}
