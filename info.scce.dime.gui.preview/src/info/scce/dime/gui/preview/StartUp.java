package info.scce.dime.gui.preview;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

public class StartUp implements IStartup{

	@Override
	public void earlyStartup() {
		System.out.println("GUI Preview start up");
		
		info.scce.dime.gui.preview.ResourceChangeListener guiRCL = new info.scce.dime.gui.preview.ResourceChangeListener();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(
				guiRCL);
		
		UIJob job = new UIJob("reloading GUI Preview") {


			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				PartListner pl = new PartListner();
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.addPartListener(pl);
				return Status.OK_STATUS;
			}
		};
		job.schedule();
		
		
	}

}
