package info.scce.dime.gui.preview;

import java.io.File;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

public class EditorCompound {
	
	private IEditorPart previewEditor;
	
	public EditorCompound()
	{
		this.previewEditor = null;
	}
	
	public void openPreviewEditor(File f, IWorkbenchPage page)
	{
		try {
			IFileStore file = EFS.getLocalFileSystem().getStore(f.toURI());
			if(previewEditor == null){
				this.previewEditor = IDE.openEditorOnFileStore(page, file);		
			}else{
				previewEditor.setFocus();
			}
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}
}
