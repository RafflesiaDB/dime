package info.scce.dime.siblibrary

import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.common.services.DefaultTerminalConverters
import org.eclipse.xtext.conversion.ValueConverter
import org.eclipse.xtext.conversion.impl.QualifiedNameValueConverter
import org.eclipse.xtext.nodemodel.ILeafNode
import org.eclipse.xtext.nodemodel.INode

/**
 * @author Steve Bosselmann
 */
class SIBLibraryValueConverterService extends DefaultTerminalConverters {

	@ValueConverter(rule="EString")
	def QualifiedNameValueConverter getDataTypePortConverter() {
				
		return new QualifiedNameValueConverter() {
			
			// adds leading and trailing quotes if the value contains whitespaces
			override toString(String value) {
				if (value?.contains(' ')) {
					'"' + value + '"'
				} else {
					value
				}
			}
			
			// call the custom delegateToValue method here
			override toValue(String string, INode node) {
				node.leafNodes
					.filter[grammarElement instanceof RuleCall]
					.map[delegateToValue]
					.filterNull
					.join
			}
			
			// removes leading and trailing quotes from the text if existent
			override delegateToValue(ILeafNode leafNode) {
				if (delegateConverter == null) {
					delegateConverter = getConverter("ID");
				}
				delegateConverter.toValue(
					leafNode.text?.replaceAll('^"+', "").replaceAll('"+$', ""), leafNode
				) as String
			}
			
		}
	}	
}
