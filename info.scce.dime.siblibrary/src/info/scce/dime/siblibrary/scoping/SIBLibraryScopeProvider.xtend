package info.scce.dime.siblibrary.scoping

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import graphmodel.GraphModel
import info.scce.dime.siblibrary.DataTypePort
import info.scce.dime.siblibrary.Import
import org.eclipse.core.resources.IResourceChangeEvent
import org.eclipse.core.resources.IResourceChangeListener
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider

/**
 * This class contains custom scoping description.
 * 
 * @author Steve Bosselmann
 */
class SIBLibraryScopeProvider extends AbstractDeclarativeScopeProvider {

	protected extension WorkspaceExtension = new WorkspaceExtension
    protected extension FileExtension = new FileExtension
    
    static IResourceChangeListener workspaceListener
    
    val importPath_on_model = <String,GraphModel> newHashMap
    
    
	def IScope scope_DataTypePort_type(DataTypePort port, EReference ref) {
		switch import : port.import {
			Import: {
				Scopes.scopeFor(import.graphModel.dataTypes)
			}
			default: IScope.NULLSCOPE
		}
	}
	
	/*
	 * Retrieve the imported graph model from the current project
	 */
	def getGraphModel(Import ^import) {
		var uri = ^import.eResource.URI
		if (uri.isFile) {
			val path = new Path(uri.toFileString)
			val file = workspaceRoot.getFileForLocation(path)
			uri = URI.createPlatformResourceURI(file.getFullPath.toString, true)
		}
		val project = uri?.getFile?.project
		val file = project.getFile(import.path)
		val fileUri = URI.createURI(file.fullPath.toString)
		ReferenceRegistry.instance.lookup(fileUri, GraphModel)?.head
		?: importPath_on_model.get(import.path)
		?: file?.graphModel => [
			if (it !== null) {
				importPath_on_model.put(import.path, it)
				assertWorkspaceListener
			}
		]
	}
	
	def getProject(EObject eobj) {
		var uri = eobj.eResource.URI
		if (uri.isFile) {
			val path = new Path(uri.toFileString)
			val file = workspaceRoot.getFileForLocation(path)
			uri = URI.createPlatformResourceURI(file.getFullPath.toString, true)
		}
		uri?.getFile?.project
	}
	
	def assertWorkspaceListener() {
		if (workspaceListener === null) {
			workspaceListener = new IResourceChangeListener {
				override resourceChanged(IResourceChangeEvent event) {
					if (event.delta !== null) {
						importPath_on_model.clear
					}
				}
			}
			workspace.addResourceChangeListener(workspaceListener)
		}
	}
	
	/*
	 * We cannot check for the data type here, as we have no dependency
	 * on the data model. Hence, we check against the name 'Type' of any
	 * super type of each node. To be more precise, we only do this if
	 * the nsURI of the graph model matches the one of the data model.
	 */
	def getDataTypes(GraphModel model) {
		if (model?.eClass.EPackage.nsURI == "http://dime.scce.info/data") {
			model.allNodes
				.filter[eClass.EAllSuperTypes.map[name].contains("Type")]
				.map[internalElement]
		} else #[]
	}
}
