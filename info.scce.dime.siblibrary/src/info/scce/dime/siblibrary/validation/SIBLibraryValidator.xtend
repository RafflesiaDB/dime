package info.scce.dime.siblibrary.validation

import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import info.scce.dime.siblibrary.Import
import info.scce.dime.siblibrary.SIB
import info.scce.dime.siblibrary.SIBLibraryPackage
import org.eclipse.xtext.validation.Check

/**
 * Custom validation rules.
 * 
 * @author Steve Bosselmann
 */
class SIBLibraryValidator extends AbstractSIBLibraryValidator {

	protected extension ResourceExtension = new ResourceExtension

	@Check
	def checkImport(Import it) {
		if (path.nullOrEmpty)
			error("No path specified", SIBLibraryPackage.Literals.IMPORT__PATH)
		else if (!eResource?.project?.getFile(path)?.exists)
			error("The specified file does not exist", SIBLibraryPackage.Literals.IMPORT__PATH)
	}
	
	@Check
	def checkSIB(SIB sib) {
		if (!sib.iconPath.nullOrEmpty) {
			val file = sib.eResource.project.getFile(sib.iconPath)
			if (!file.exists) {
				error("The specified file does not exist", SIBLibraryPackage.Literals.SIB__ICON_PATH)
			}
		}
	}
}
