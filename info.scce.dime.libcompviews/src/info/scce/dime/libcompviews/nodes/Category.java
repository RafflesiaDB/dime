package info.scce.dime.libcompviews.nodes;

public class Category {
	private String name;
	private Object type;

	public Category(String name) {
		super();
		this.name = name;
	}
	
	public Category(String name, Object type) {
		super();
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getType() {
		return type;
	}

	public void setType(Object type) {
		this.type = type;
	}

}
