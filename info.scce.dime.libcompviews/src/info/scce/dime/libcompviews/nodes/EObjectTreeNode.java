package info.scce.dime.libcompviews.nodes;

import org.eclipse.emf.ecore.EObject;

public class EObjectTreeNode extends TreeNode {

	public EObjectTreeNode(Object data) {
		super(data);
	}

	@Override
	public String getId() {
		if (data instanceof EObject) {
			EObject eObject = (EObject) data;
			return eObject.toString();
		}
		return null;
	}

}
