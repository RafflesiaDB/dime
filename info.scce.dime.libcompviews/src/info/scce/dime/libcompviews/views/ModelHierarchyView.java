package info.scce.dime.libcompviews.views;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.google.common.collect.Sets;

import info.scce.dime.dad.dad.DAD;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.libcompviews.ResourceChangeListener.ListenerEvents;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.pages.ModelHierarchyPage;
import info.scce.dime.process.process.Process;

public class ModelHierarchyView extends LibCompView<ModelHierarchyPage> {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "info.scce.dime.libcompviews.views.ModelHierarchy";

	public ModelHierarchyView() {
		Map<String, Set<ListenerEvents>> obsDefinition = new HashMap<>();
		obsDefinition.put("process",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED, ListenerEvents.CONTENT_CHANGE));
		obsDefinition.put("gui",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED, ListenerEvents.CONTENT_CHANGE));
		obsDefinition.put("dad",
				Sets.newHashSet(ListenerEvents.REMOVED, ListenerEvents.ADDED, ListenerEvents.CONTENT_CHANGE));

		this.obsDefinition = obsDefinition;
		this.genericParameterClass = ModelHierarchyPage.class;
	}

	@Override
	protected void fillLocalPullDown(IMenuManager manager) {
		IMenuManager filterSubmenu = new MenuManager("Filters");
		//filterSubmenu.add(filterAllowedTypesAction);

		//IMenuManager viewSubmenu = new MenuManager("Display Mode");
		//viewSubmenu.add(showFolderStructViewAction);

		manager.add(filterSubmenu);
		//manager.add(viewSubmenu);

		manager.add(new Separator());
	}

	@Override
	protected void fillContextMenu(IMenuManager manager) {
		manager.add(openModelAction);
		// manager.add(new Separator());
		// // Other plug-ins can contribute there actions here
		// manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	@Override
	protected void fillLocalToolBar(IToolBarManager manager) {
		// manager.add(createFileAction);
		manager.add(reloadAction);
		manager.add(expandAllAction);
		manager.add(collapseAllAction);
		manager.add(linkWithEditorAction);
		manager.add(new Separator());
	}

	@Override
	protected void makeActions() {
		super.makeActions();

		/*
		 * ------------------------------------------
		 */
		/*
		filterAllowedTypesAction = new Action() {
			public void run() {
				if (!activePage.isShowOnlyAllowedTypes()) {
					activePage.getTreeViewer().addFilter(
							activePage.getModelTypeFilter());
					activePage.setShowOnlyAllowedTypes(true);
				} else {
					activePage.getTreeViewer().removeFilter(
							activePage.getModelTypeFilter());
					activePage.setShowOnlyAllowedTypes(false);
				}
				reloadViewState();
			}
		};
		filterAllowedTypesAction.setText("filter allowed types");
		filterAllowedTypesAction.setToolTipText("filter allowed types");
		filterAllowedTypesAction.setChecked(false);
		*/
		/*
		 * ------------------------------------------
		 */
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				if (obj instanceof TreeNode) {
					activePage.storeTreeState();
					Object data = ((TreeNode) obj).getData();
					if (data instanceof Process) {
						activePage.openAndHighlight(data);
					} else if (data instanceof GUI) {
						activePage.openAndHighlight(data);
					} else if (data instanceof DAD) {
						activePage.openAndHighlight(data);
					} else {
						activePage.toggleExpand(obj);
						activePage.storeTreeState();
					}
				}
			}
		};

	}

	@Override
	protected void reloadViewState() {
		activePage.getTreeViewer().refresh();
	}

	@Override
	protected void editorChanged() {
		super.editorChanged();
		activePage.getTreeViewer().refresh();
		activePage.restoreTreeState();
	}
}
