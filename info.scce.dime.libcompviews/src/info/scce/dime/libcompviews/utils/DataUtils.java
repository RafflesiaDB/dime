package info.scce.dime.libcompviews.utils;

import graphmodel.Edge;
import graphmodel.ModelElementContainer;
import info.scce.dime.data.data.Association;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.BidirectionalAssociation;
import info.scce.dime.data.data.BidirectionalAttribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.Data;
import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.ReferencedBidirectionalAttribute;
import info.scce.dime.data.data.ReferencedComplexAttribute;
import info.scce.dime.data.data.ReferencedPrimitiveAttribute;
import info.scce.dime.data.data.ReferencedType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserAssociation;
import info.scce.dime.data.data.UserAttribute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class DataUtils extends GraphModelUtils {

	public enum AssociationType {
		USER, BIDIRECTIONAL, UNIDIRECTIONAL
	}

	public DataUtils(String[] fileExtensions) {
		super(fileExtensions);
	}

	public List<Type> getRootTypes(Data model) {
		ArrayList<Type> rootTypes = new ArrayList<>();
		if (model != null)
			for (Type type : model.getTypes()) {
				boolean hasInheritance = false;
				for (Edge edge : type.getOutgoing()) {
					if (edge instanceof Inheritance)
						hasInheritance = true;
				}
				if (!hasInheritance)
					rootTypes.add(type);
			}
		return rootTypes;
	}

	public List<Type> getSubTypes(Type type) {
		ArrayList<Type> subTypes = new ArrayList<>();
		if (type != null)
			for (Edge edge : type.getIncoming()) {
				if (edge instanceof Inheritance) {
					Inheritance inheritance = (Inheritance) edge;
					subTypes.add((Type) inheritance.getSourceElement());
				}

			}
		return subTypes;
	}

	public Type getSuperType(Type type) {
		if (type != null)
			for (Edge edge : type.getOutgoing()) {
				if (edge instanceof Inheritance) {
					Inheritance inheritance = (Inheritance) edge;
					// TODO: remove cast once incoming/outgoing with inheritance
					// has
					// been fixed
					return (Type) inheritance.getTargetElement();
				}
			}
		return null;
	}

	public Type getOriginType(Type type) {
		if (type instanceof ReferencedType)
			return (Type) ((ReferencedType) type).getReferencedType();
		return type;
	}

	public Attribute getOriginAttribute(Attribute attr) {
		// if (attr instanceof ReferencedComplexAttribute)
		// return (Attribute) ((ReferencedComplexAttribute) attr)
		// .getReferencedAttribute();
		// if (attr instanceof ReferencedBidirectionalAttribute)
		// return (Attribute) ((ReferencedBidirectionalAttribute) attr)
		// .getReferencedAttribute();
		// if (attr instanceof ReferencedPrimitiveAttribute)
		// return (Attribute) ((ReferencedPrimitiveAttribute) attr)
		// .getReferencedAttribute();

		if (attr instanceof ReferencedComplexAttribute
				|| attr instanceof ReferencedPrimitiveAttribute
				|| attr instanceof ReferencedBidirectionalAttribute) {

			Type type = getOriginType(getTypeForAttribute(attr));
			while (type != null) {
				Attribute refAttribute = getAttributeByName(type,
						attr.getName());
				if (refAttribute == null)
					type = getOriginType(getSuperType(type));
				else
					return refAttribute;
			}
		}

		return attr;
	}

	private Attribute getAttributeByName(Type type, String name) {
		for (Attribute attr : type.getAttributes()) {
			if (name.equals(attr.getName()))
				return attr;
		}
		return null;
	}

	public Type getTypeForAttribute(Attribute attr) {
		ModelElementContainer mec = attr.getContainer();
		if (mec instanceof Type)
			return (Type) mec;
		return null;
	}

	public List<Attribute> getAllAttributes(Type type) {
		ArrayList<Attribute> attributes = new ArrayList<>();
		ArrayList<Attribute> possibleAttr = new ArrayList<>();
		ArrayList<Attribute> overwrittenAttr = new ArrayList<>();
		Type actType = type;
		HashSet<Type> visited = new HashSet<>();
		while (actType != null) {
			for (Attribute attribute : actType.getAttributes()) {

				Attribute superCa = null;
				if (attribute instanceof ComplexAttribute)
					superCa = ((ComplexAttribute) attribute).getSuperAttr();
				if (attribute instanceof PrimitiveAttribute)
					superCa = ((PrimitiveAttribute) attribute).getSuperAttr();
				if (attribute instanceof BidirectionalAttribute)
					superCa = ((BidirectionalAttribute) attribute)
							.getSuperAttr();

				if (superCa != null && superCa != attribute)
					overwrittenAttr.add(getOriginAttribute(superCa));
				possibleAttr.add(attribute);
			}
			Type superType = getOriginType(getSuperType(actType));
			actType = visited.add(superType) ? superType : null;
		}

		for (Attribute pAttr : possibleAttr) {
			boolean found = false;
			for (Attribute oAttr : overwrittenAttr) {
				if (pAttr.getId().equals(oAttr.getId()))
					found = true;
			}
			if (!found)
				attributes.add(pAttr);
		}

		return attributes;
	}

	public AssociationType getAssociationTypeFromAttribute(ComplexAttribute attr) {
		if (attr instanceof UserAttribute)
			return AssociationType.USER;
		if (attr instanceof BidirectionalAttribute)
			return AssociationType.BIDIRECTIONAL;
		return AssociationType.UNIDIRECTIONAL;
	}

}
