package info.scce.dime.libcompviews.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorPart;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.ProcessEntryPointComponent;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.libcompviews.nodes.Category;
import info.scce.dime.libcompviews.provider.LibCompTreeProvider.ModelType;
import info.scce.dime.libcompviews.provider.LinkCompTreeProvider;
import info.scce.dime.libcompviews.utils.LibCompUtils;

public class LinkCompPage extends LibCompPage {

	private LinkCompTreeProvider data = new LinkCompTreeProvider();

	private HashMap<String, Image> icons = new HashMap<>();

	private boolean showOnlyAllowedTypes = false;

	private LinkLabelProvider labelProvider = new LinkLabelProvider();
	private LinkNameSorter nameSorter = new LinkNameSorter();
	private LinkModelTypeFilter modelTypeFilter = new LinkModelTypeFilter();

	private ArrayList<ModelType> hideModelTypes = new ArrayList<LinkCompTreeProvider.ModelType>();

	/*
	 * Getter / Setter
	 */
	@Override
	public LinkCompTreeProvider getDataProvider() {
		return data;
	}

	public boolean isShowOnlyAllowedTypes() {
		return showOnlyAllowedTypes;
	}

	public void setShowOnlyAllowedTypes(boolean showOnlyAllowedTypes) {
		this.showOnlyAllowedTypes = showOnlyAllowedTypes;
	}

	public ArrayList<ModelType> getHideModelTypes() {
		return hideModelTypes;
	}

	@Override
	public LabelProvider getDefaultLabelProvider() {
		return labelProvider;
	}

	@Override
	public ViewerSorter getDefaultSorter() {
		return nameSorter;
	}

	public LinkModelTypeFilter getModelTypeFilter() {
		return modelTypeFilter;
	}

	/*
	 * Methods
	 */

	@Override
	protected void loadIcons() {
		try {
			super.loadIcons();

			icons.put(
					"LinkModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/linkSIB16.png"), true)));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openAndHighlight(Object obj) {
		obj = getTreeNodeData(obj);
		if (obj instanceof ModelElement) {
			ModelElement me = (ModelElement) obj;
			IEditorPart editorPart = openEditor(me.getRootElement());
			
			if (me instanceof URLProcess) {
				DAD wrapper = data.getUtils().getDADWrapperFromEditor(editorPart);
				data.getUtils().highlightElement(wrapper, me.getId());
			}
			
			if (me.getRootElement() instanceof DAD) {
				DAD wrapper = data.getUtils().getDADWrapperFromEditor(editorPart);
				data.getUtils().highlightElement(wrapper, me.getId());
			}
		}
		else if (obj instanceof EObject) {
			openEditor((EObject) obj);
		}
	}

	@Override
	public void reload() {
		storeTreeState();
		data.reset();
		treeViewer.setInput(parentViewPart.getViewSite());
		restoreTreeState();
	}

	public void calculateModelTypesToHide(IResource res) {
		hideModelTypes.clear();

		EObject activeModel = data.getEObjectForResource(res);
		ModelType modelType = data.mapEObjectToType(activeModel);
		List<ModelType> allowedTypes = data.getAllowedModels().get(modelType);

		for (ModelType type : ModelType.values()) {
			if (allowedTypes == null || !allowedTypes.contains(type))
				hideModelTypes.add(type);
		}
	}

	/*
	 * Provider / Classes for TreeViewer
	 */
	private class LinkLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			obj = getTreeNodeData(obj);
			if (obj instanceof Category)
				return ((Category) obj).getName();
			if (obj instanceof IFolder)
				return ((IFolder) obj).getName();
			if (obj instanceof GraphModel) {
				String label = data.getModelToResource().get(obj).getName();
				return label;
			}
			if(obj instanceof URLProcess){
				String l = "";
				String url = ((URLProcess) obj).getUrl();
				if(obj instanceof ProcessComponent) {
					l=((ProcessComponent) obj).getModel().getModelName();
				}
				if(obj instanceof ProcessEntryPointComponent) {
					l=((ProcessEntryPointComponent) obj).getEntryPoint().getLabel();
				}
				if(url==null) {
					url = l;
				}
				return "/"+url+" ("+l+")";
			}
			
	
			return "unknown";
		}

		public Image getImage(Object obj) {
			obj = getTreeNodeData(obj);
			if (obj instanceof IFolder)
				return icons.get("Folder");
			if (obj instanceof URLProcess)
				return icons.get("LinkModel");
			return null;
		}
	}

	private class LinkModelTypeFilter extends ViewerFilter {

		@Override
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {

			parentElement = getTreeNodeData(parentElement);
			element = getTreeNodeData(element);

			if (element instanceof EObject)
				if (hideModelTypes.contains(data
						.mapEObjectToType((EObject) element)))
					return false;
			return true;
		}
	}

	private class LinkNameSorter extends ViewerSorter {

		@Override
		public int category(Object element) {
			if (element instanceof IFolder)
				return 1;
			if (element instanceof GraphModel)
				return 2;
			if (element instanceof URLProcess)
				return 3;
			return 99;
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			e1 = getTreeNodeData(e1);
			e2 = getTreeNodeData(e2);

			int cat1 = category(e1);
			int cat2 = category(e2);
			if (cat1 != cat2)
				return cat1 - cat2;

			if (e1 instanceof EObject && e2 instanceof EObject) {
				IResource res1 = data.getModelToResource().get(e1);
				IResource res2 = data.getModelToResource().get(e2);
				if (res1 != null && res2 != null)
					return res1.getName().toLowerCase()
							.compareTo(res2.getName().toLowerCase());
			}

			if (e1 instanceof IFolder && e2 instanceof IFolder) {
				IFolder folder1 = (IFolder) e1;
				IFolder folder2 = (IFolder) e2;
				return folder1.getName().toLowerCase()
						.compareTo(folder2.getName().toLowerCase());
			}

			return super.compare(viewer, e1, e2);
		}

	}

}
