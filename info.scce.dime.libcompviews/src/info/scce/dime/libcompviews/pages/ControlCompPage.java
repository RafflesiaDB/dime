package info.scce.dime.libcompviews.pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.gUIPlugin.AbstractParameter;
import info.scce.dime.gUIPlugin.ComplexInputParameter;
import info.scce.dime.gUIPlugin.Function;
import info.scce.dime.gUIPlugin.Output;
import info.scce.dime.gUIPlugin.Plugin;
import info.scce.dime.gUIPlugin.Plugins;
import info.scce.dime.gUIPlugin.PrimitiveInputParameter;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.libcompviews.nodes.Category;
import info.scce.dime.libcompviews.provider.ControlCompTreeProvider;
import info.scce.dime.libcompviews.provider.LibCompTreeProvider.ModelType;
import info.scce.dime.libcompviews.utils.LibCompUtils;
import info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.process.process.EntryPointProcessSIB;
import info.scce.dime.process.process.GUIBlueprintSIB;
import info.scce.dime.process.process.NativeFrontendSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessBlueprintSIB;
import info.scce.dime.search.search.Search;
import info.scce.dime.siblibrary.Branch;
import info.scce.dime.siblibrary.DataTypePort;
import info.scce.dime.siblibrary.JavaTypePort;
import info.scce.dime.siblibrary.Port;
import info.scce.dime.siblibrary.PrimitivePort;
import info.scce.dime.siblibrary.SIB;
import info.scce.dime.siblibrary.SIBLibrary;
import info.scce.dime.siblibrary.Type;

public class ControlCompPage extends LibCompPage {

	private ControlCompTreeProvider data = new ControlCompTreeProvider();

	private HashMap<String, Image> icons = new HashMap<>();

	private boolean showOnlyAllowedTypes = false;

	private ControlLabelProvider labelProvider = new ControlLabelProvider();
	private ControlNameSorter nameSorter = new ControlNameSorter();
	private ControlModelTypeFilter modelTypeFilter = new ControlModelTypeFilter();

	private ArrayList<ModelType> hideModelTypes = new ArrayList<ControlCompTreeProvider.ModelType>();

	/*
	 * Getter / Setter
	 */
	@Override
	public ControlCompTreeProvider getDataProvider() {
		return data;
	}

	public boolean isShowOnlyAllowedTypes() {
		return showOnlyAllowedTypes;
	}

	public void setShowOnlyAllowedTypes(boolean showOnlyAllowedTypes) {
		this.showOnlyAllowedTypes = showOnlyAllowedTypes;
	}

	public ArrayList<ModelType> getHideModelTypes() {
		return hideModelTypes;
	}

	@Override
	public LabelProvider getDefaultLabelProvider() {
		return labelProvider;
	}

	@Override
	public ViewerSorter getDefaultSorter() {
		return nameSorter;
	}

	public ControlModelTypeFilter getModelTypeFilter() {
		return modelTypeFilter;
	}

	/*
	 * Methods
	 */

	@Override
	protected void loadIcons() {
		try {
			super.loadIcons();

			icons.put("Folder", PlatformUI.getWorkbench().getSharedImages()
					.getImage(ISharedImages.IMG_OBJ_FOLDER));

			icons.put(
					"DadModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/dime16.png"),
									true)));

			icons.put(
					"ProcessModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/ProcessModel.png"), true)));
			
			icons.put(
					"EntryPointSIB",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/entryProcessSIB16.png"), true)));

			icons.put(
					"GuiModel",
					new Image(LibCompUtils.getDisplay(),
							FileLocator.openStream(bundle, new Path(
									"icons/GUI.png"), true)));

			icons.put(
					"SearchModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/SearchModel.png"), true)));
			
			icons.put(
					"ProcessBlueprintSIB",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/processBlueprintSIB16.png"), true)));
			
			icons.put(
					"GUIBlueprintSIB",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/guiBlueprintSIB16.png"), true)));

			icons.put(
					"AtomicSibLibrary",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/AtomicSibLib.png"), true)));
			
			icons.put(
					"GUIPlugins",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/guiPlugin.png"), true)));
			
			icons.put(
					"GUIPlugin",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/gPlugin.png"), true)));

			icons.put(
					"AtomicSibLibraryContent",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/r_attr.png"),
									true)));
			icons.put(
					"ReferencingComponent",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/r_attr.png"),
									true)));
			icons.put(
					"NativeFrontendSibLibrary",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/nfsl.png"),
									true)));
			icons.put(
					"NativeFrontendSibLibraryContents",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/r_attr.png"),
									true)));
			for (IModeltrafoSupporter<EObject> provider : ModeltrafoExtensionProvider.getAllModeltrafoSupporter()) {
				icons.put(provider.getSIBName(), new Image(LibCompUtils.getDisplay(), new FileInputStream(new File(provider.getIconPath()))));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openAndHighlight(Object obj) {
		obj = getTreeNodeData(obj);
		if (obj instanceof ModelElement) {
			ModelElement me = (ModelElement) obj;
			IEditorPart editorPart = openEditor(me.getRootElement());
			
			if (me.getRootElement() instanceof Process) {
				Process wrapper = data.getUtils().getProcessWrapperFromEditor(editorPart);
				data.getUtils().highlightElement(wrapper, me.getId());
			}
			if (me.getRootElement() instanceof GUI) {
				GUI wrapper = data.getUtils().getGuiWrapperFromEditor(editorPart);
				data.getUtils().highlightElement(wrapper, me.getId());
			}
		}
		else if (obj instanceof EObject) {
			openEditor((EObject) obj);
		}
	}

	@Override
	public void reload() {
		storeTreeState();
		data.reset();
		treeViewer.setInput(parentViewPart.getViewSite());
		restoreTreeState();
	}

	public void calculateModelTypesToHide(IResource res) {
		hideModelTypes.clear();

		EObject activeModel = data.getEObjectForResource(res);
		ModelType modelType = data.mapEObjectToType(activeModel);
		List<ModelType> allowedTypes = data.getAllowedModels().get(modelType);

		for (ModelType type : ModelType.values()) {
			if (allowedTypes == null || !allowedTypes.contains(type))
				hideModelTypes.add(type);
		}
	}

	/*
	 * Provider / Classes for TreeViewer
	 */
	private class ControlLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			obj = getTreeNodeData(obj);
			if (obj instanceof Category)
				return ((Category) obj).getName();
			if (obj instanceof IFolder)
				return ((IFolder) obj).getName();
			if (obj instanceof IFile) {
				return ((IFile) obj).getName();
			}
			if (obj instanceof EObject) {
				EObject eObject = (EObject) obj;
				IModeltrafoSupporter<EObject> provider = ModeltrafoExtensionProvider.getSupportedModelElement(eObject);
				if (provider != null) {
					return provider.getModellingProvider().getLabel(eObject);
				}
			}
			if (obj instanceof GraphModel) {
				String label = data.getModelToResource().get(obj).getName();
//				if (obj instanceof Process)
//					label += " (" + ((Process) obj).getProcessType() + ")";
				return label;
			}
			if(obj instanceof NativeFrontendSIB){
				NativeFrontendSIB sib =(NativeFrontendSIB)obj;
				return sib.getLabel()+ " "+ sib.getMethodName();
			}
			
			if (obj instanceof info.scce.dime.process.process.EntryPointProcessSIB) {
				info.scce.dime.process.process.SIB sib = (info.scce.dime.process.process.SIB) obj;
				return sib.getLabel();
			}
			
			if (obj instanceof info.scce.dime.process.process.BlueprintSIB) {
				info.scce.dime.process.process.BlueprintSIB sib = (info.scce.dime.process.process.BlueprintSIB) obj;
				return sib.getLabel();
			}
			
			if (obj instanceof info.scce.dime.process.process.SIB) {
				info.scce.dime.process.process.SIB sib = (info.scce.dime.process.process.SIB) obj;
				Process process = sib.getRootElement();
				//return process.getModelName() + " (" + obj.getClass().getSimpleName().replace("Impl", "") + ")";
				return sib.getLabel() + " (" + process.getModelName() + ")";
			}
			
			if (obj instanceof ProcessSIB) {
				GUI gui = ((ProcessSIB) obj).getRootElement();
				return gui.getTitle() + " (Embedded Process)";
			}
			
			if (obj instanceof SIBLibrary)
				return data.getModelToResource().get(obj).getName();
			
			if (obj instanceof info.scce.dime.siblibrary.SIB) {
				info.scce.dime.siblibrary.SIB sib = (info.scce.dime.siblibrary.SIB) obj;
				String label = sib.getName();
				if (sib.getInputPorts().size() > 0) {
					label += " ( ";
					for (Port port : sib.getInputPorts()) {
						if (port instanceof PrimitivePort)
							label += ((PrimitivePort) port).getType();
						if (port instanceof JavaTypePort)
							label += ((JavaTypePort) port).getType().getName();
						if (port instanceof DataTypePort) {
							EObject type = ((DataTypePort) port).getType();
							if (type instanceof info.scce.dime.data.data.Type) {
								label += ((info.scce.dime.data.data.Type) type).getName();
							} else {
								label += "DataType";
							}
						}
						label += " ";
					}
					label += ")";
				}

				label += " -> |";
				for (Branch branch : sib.getBranches()) {
					label += branch.getName() + "|";
				}
				return label;
			}
			if (obj instanceof Plugins)
				return data.getModelToResource().get(obj).getName();
			
			if (obj instanceof Plugin)
				return ((Plugin)obj).getPath();
			
			if (obj instanceof Function) {
				Function sib = (Function) obj;
				String label = sib.getFunctionName();
				if (sib.getParameters().size() > 0) {
					label += " ( ";
					for (AbstractParameter port : sib.getParameters()) {
						if(port instanceof ComplexInputParameter){
							label += ((ComplexInputParameter) port).getParameter().getName()+ " ";
						}
						else if(port instanceof PrimitiveInputParameter){
							label += ((PrimitiveInputParameter) port).getParameter().getName()+ " ";
						}
						else {
							label += port.getName()+ " ";							
						}
					}
					label += ")";
				}
				if(sib.getOutputs().size() > 0) {
					label += " -> |";
					for (Output branch : sib.getOutputs()) {
						label += branch.getOutputName() + "|";
					}					
				}
				return label;
			}
			
			if (obj instanceof Type)
				return ((Type) obj).getName();
			
			return "unknown";
		}

		public Image getImage(Object obj) {
			obj = getTreeNodeData(obj);
			if (obj instanceof IFolder)
				return icons.get("Folder");
			if (obj instanceof IFile) {
				IFile ifile = (IFile) obj;
				if (ifile.getFileExtension() != null) {
					IModeltrafoSupporter<EObject> provider = ModeltrafoExtensionProvider.getSupportedModelElement(ifile.getFileExtension());
					return icons.get(provider.getSIBName());
				}
			}
			if (obj instanceof EObject) {
				EObject eObject = (EObject) obj;
				IModeltrafoSupporter<EObject> provider = ModeltrafoExtensionProvider.getSupportedModelElement(eObject);
				if (provider != null) {
					return icons.get(provider.getSIBName());
				}
			}
			if (obj instanceof DAD)
				return icons.get("DadModel");
			if(obj instanceof Plugins){
				return icons.get("GUIPlugins");
			}
			if(obj instanceof Plugin){
				return icons.get("GUIPlugin");
			}
			if(obj instanceof Function){
				return icons.get("AtomicSibLibraryContent");
			}
			if (obj instanceof GUI)
				return icons.get("GuiModel");
			if (obj instanceof Search)
				return icons.get("SearchModel");
			if (obj instanceof Process)
				return icons.get("ProcessModel");
			if (obj instanceof EntryPointProcessSIB) 
				return icons.get("EntryPointSIB");
			if (obj instanceof info.scce.dime.process.process.SIB) 
				return icons.get("ReferencingComponent");
			if (obj instanceof info.scce.dime.gui.gui.ProcessSIB) 
				return icons.get("ReferencingComponent");
			if (obj instanceof ProcessBlueprintSIB)
				return icons.get("ProcessBlueprintSIB");
			if (obj instanceof GUIBlueprintSIB)
				return icons.get("GUIBlueprintSIB");
			if (obj instanceof info.scce.dime.process.process.SIB) 
				return icons.get("ReferencingComponent");
			if (obj instanceof SIBLibrary)
				return icons.get("AtomicSibLibrary");
			if (obj instanceof NativeFrontendSIB)
				return icons.get("NativeFrontendSibLibraryContents");
			if (obj instanceof SIB)
				return icons.get("AtomicSibLibraryContent");
			if (obj instanceof Category) {
				Category cat = (Category) obj;
				Object typeObj = cat.getType();
				if (typeObj instanceof ModelType) {
					ModelType type = (ModelType) typeObj;
					switch (type) {
					case SEARCH:
						return icons.get("SearchModel");
					case SIBLIBRARY:
						return icons.get("AtomicSibLibrary");
					case NATIVE_FRONTEND_LIBRARY:
						return icons.get("NativeFrontendSibLibrary");
					case PROCESS:
					case PROCESS_BASIC:
					case PROCESS_LONGRUNNING:
					case PROCESS_SECURITY:
						return icons.get("ProcessModel");
					default: 
					}
				} else {
					if (icons.get(cat.getName()) != null) {
						return icons.get(cat.getName());
					}
				}
			}
			return null;
		}
	}

	private class ControlModelTypeFilter extends ViewerFilter {

		@Override
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {

			parentElement = getTreeNodeData(parentElement);
			element = getTreeNodeData(element);

			if (element instanceof EObject)
				if (hideModelTypes.contains(data
						.mapEObjectToType((EObject) element)))
					return false;
			return true;
		}
	}

	private class ControlNameSorter extends ViewerSorter {

		@Override
		public int category(Object element) {
			if (element instanceof IFolder)
				return 1;
			if (element instanceof GraphModel)
				return 2;
			if (element instanceof SIBLibrary)
				return 3;
			if (element instanceof Type)
				return 4;
			if (element instanceof SIB)
				return 5;
			if (element instanceof Plugins)
				return 6;
			return 99;
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			e1 = getTreeNodeData(e1);
			e2 = getTreeNodeData(e2);

			int cat1 = category(e1);
			int cat2 = category(e2);
			if (cat1 != cat2)
				return cat1 - cat2;

			if (e1 instanceof EObject && e2 instanceof EObject) {
				IResource res1 = data.getModelToResource().get(e1);
				IResource res2 = data.getModelToResource().get(e2);
				if (res1 != null && res2 != null)
					return res1.getName().toLowerCase()
							.compareTo(res2.getName().toLowerCase());
			}

			if (e1 instanceof IFolder && e2 instanceof IFolder) {
				IFolder folder1 = (IFolder) e1;
				IFolder folder2 = (IFolder) e2;
				return folder1.getName().toLowerCase()
						.compareTo(folder2.getName().toLowerCase());
			}

			return super.compare(viewer, e1, e2);
		}

	}

}
