package info.scce.dime.libcompviews.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.gUIPlugin.Plugin;
import info.scce.dime.gUIPlugin.Plugins;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.libcompviews.nodes.GraphModelTreeNode;
import info.scce.dime.libcompviews.nodes.PrimeTreeNode;
import info.scce.dime.libcompviews.provider.LibCompTreeProvider.ModelType;
import info.scce.dime.libcompviews.provider.ModelHierarchyTreeProvider;
import info.scce.dime.libcompviews.utils.LibCompUtils;
import info.scce.dime.process.process.Process;
import info.scce.dime.search.search.Search;
import info.scce.dime.siblibrary.SIB;


public class ModelHierarchyPage extends LibCompPage {

	private ModelHierarchyTreeProvider data = new ModelHierarchyTreeProvider();

	private HashMap<String, Image> icons = new HashMap<>();

	private ModelHierarchyLabelProvider labelProvider = new ModelHierarchyLabelProvider();
	private ModelHierarchyNameSorter nameSorter = new ModelHierarchyNameSorter();
	private ModelHierarchyModelTypeFilter modelTypeFilter = new ModelHierarchyModelTypeFilter();

	private ArrayList<ModelType> hideModelTypes = new ArrayList<ModelHierarchyTreeProvider.ModelType>();

	/*
	 * Getter / Setter
	 */
	@Override
	public ModelHierarchyTreeProvider getDataProvider() {
		return data;
	}

	public ArrayList<ModelType> getHideModelTypes() {
		return hideModelTypes;
	}

	@Override
	public LabelProvider getDefaultLabelProvider() {
		return labelProvider;
	}

	@Override
	public ViewerSorter getDefaultSorter() {
		return nameSorter;
	}

	public ModelHierarchyModelTypeFilter getModelTypeFilter() {
		return modelTypeFilter;
	}

	/*
	 * Methods
	 */

	@Override
	protected void loadIcons() {
		try {
			super.loadIcons();

			icons.put("Folder", PlatformUI.getWorkbench().getSharedImages()
					.getImage(ISharedImages.IMG_OBJ_FOLDER));

			icons.put(
					"DadModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/dime16.png"),
									true)));
			icons.put(
					"ProcessModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/ProcessModel.png"), true)));
			icons.put(
					"GuiModel",
					new Image(LibCompUtils.getDisplay(),
							FileLocator.openStream(bundle, new Path(
									"icons/GUI.png"), true)));
			icons.put(
					"SearchModel",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/SearchModel.png"), true)));
			
			
			icons.put(
					"GUIPlugin",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path(
									"icons/gPlugin.png"), true)));

			icons.put(
					"ReferencingComponent",
					new Image(LibCompUtils.getDisplay(), FileLocator
							.openStream(bundle, new Path("icons/r_attr.png"),
									true)));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openAndHighlight(Object obj) {
		obj = getTreeNodeData(obj);
		if (obj instanceof ModelElement) {
			ModelElement me = (ModelElement) obj;
			IEditorPart editorPart = openEditor(me.getRootElement());
			
			if (me.getRootElement() instanceof Process) {
				Process wrapper = data.getUtils().getProcessWrapperFromEditor(editorPart);
				data.getUtils().highlightElement(wrapper, me.getId());
			}
			if (me.getRootElement() instanceof GUI) {
				GUI wrapper = data.getUtils().getGuiWrapperFromEditor(editorPart);
				data.getUtils().highlightElement(wrapper, me.getId());
			}
		}
		else if (obj instanceof EObject) {
			openEditor((EObject) obj);
		}
	}

	@Override
	public void reload() {
		storeTreeState();
		data.reset();
		treeViewer.setInput(parentViewPart.getViewSite());
		restoreTreeState();
	}

	/*
	 * Provider / Classes for TreeViewer
	 */
	private class ModelHierarchyLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			String label = "";
			if (obj instanceof PrimeTreeNode) {
				PrimeTreeNode node = (PrimeTreeNode) obj;
				
				
				/*
				 * element labels
				 */
				if (node.getElement() instanceof info.scce.dime.dad.dad.ProcessComponent) {
					info.scce.dime.dad.dad.ProcessComponent sib = (info.scce.dime.dad.dad.ProcessComponent) node.getElement();
					label += sib.getIncoming().get(0).getClass().getSimpleName();
				}
				
				if (node.getElement() instanceof info.scce.dime.gui.gui.SIB) {
					info.scce.dime.gui.gui.SIB sib = (info.scce.dime.gui.gui.SIB) node.getElement();
					label += sib.getLabel();
				}
				
				if (node.getElement() instanceof info.scce.dime.process.process.SIB) {
					info.scce.dime.process.process.SIB sib = (info.scce.dime.process.process.SIB) node.getElement();
					label += sib.getLabel();
				}
				
				/*
				 * model labels
				 */
				if (node.getModel() instanceof GraphModel) {
					String text = data.getModelToResource().get(node.getModel()).getName();
					if (obj instanceof Process)
						text += " (" + ((Process) obj).getProcessType() + ")";
					label += " ⇒ " + text;
				}
				return label; 
			}
			
			if (obj instanceof GraphModelTreeNode) {
				GraphModelTreeNode node = (GraphModelTreeNode) obj;
				if (node.getData() instanceof GraphModel) {
					String text = data.getModelToResource().get(node.getData()).getName();
					if (obj instanceof Process)
						text += " (" + ((Process) obj).getProcessType() + ")";
					label += text;
				}
				return label; 
			}
			
			return "unknown: " + obj;
		}

		public Image getImage(Object obj) {
			if (obj instanceof PrimeTreeNode) {
				obj = ((PrimeTreeNode) obj).getModel();
			} else {
				obj = getTreeNodeData(obj);
			}
			if (obj instanceof IFolder)
				return icons.get("Folder");
			if (obj instanceof DAD)
				return icons.get("DadModel");
			if(obj instanceof Plugin){
				return icons.get("GUIPlugin");
			}
			if (obj instanceof GUI)
				return icons.get("GuiModel");
			if (obj instanceof Search)
				return icons.get("SearchModel");
			if (obj instanceof Process)
				return icons.get("ProcessModel");
			if (obj instanceof info.scce.dime.process.process.SIB) 
				return icons.get("ReferencingComponent");
			return icons.get("ReferencingComponent");
		}
	}

	private class ModelHierarchyModelTypeFilter extends ViewerFilter {

		@Override
		public boolean select(Viewer viewer, Object parentElement,
				Object element) {

			parentElement = getTreeNodeData(parentElement);
			element = getTreeNodeData(element);

			if (element instanceof EObject)
				if (hideModelTypes.contains(data
						.mapEObjectToType((EObject) element)))
					return false;
			return true;
		}
	}

	private class ModelHierarchyNameSorter extends ViewerSorter {

		@Override
		public int category(Object element) {
			if (element instanceof IFolder)
				return 1;
			if (element instanceof GraphModel)
				return 2;
			if (element instanceof SIB)
				return 5;
			if (element instanceof Plugins)
				return 6;
			return 99;
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			e1 = getTreeNodeData(e1);
			e2 = getTreeNodeData(e2);

			int cat1 = category(e1);
			int cat2 = category(e2);
			if (cat1 != cat2)
				return cat1 - cat2;

			if (e1 instanceof EObject && e2 instanceof EObject) {
				IResource res1 = data.getModelToResource().get(e1);
				IResource res2 = data.getModelToResource().get(e2);
				if (res1 != null && res2 != null)
					return res1.getName().toLowerCase()
							.compareTo(res2.getName().toLowerCase());
			}

			if (e1 instanceof IFolder && e2 instanceof IFolder) {
				IFolder folder1 = (IFolder) e1;
				IFolder folder2 = (IFolder) e2;
				return folder1.getName().toLowerCase()
						.compareTo(folder2.getName().toLowerCase());
			}

			return super.compare(viewer, e1, e2);
		}

	}

}
