package info.scce.dime.libcompviews.provider;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.Data;
import info.scce.dime.data.data.ReferencedComplexAttribute;
import info.scce.dime.data.data.ReferencedExtensionAttribute;
import info.scce.dime.data.data.ReferencedPrimitiveAttribute;
import info.scce.dime.data.data.ReferencedType;
import info.scce.dime.data.data.ReferencedUserAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.libcompviews.nodes.ContainerTreeNode;
import info.scce.dime.libcompviews.nodes.GraphModelTreeNode;
import info.scce.dime.libcompviews.nodes.ModelElementTreeNode;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.utils.DataUtils;
import info.scce.dime.siblibrary.SIBLibrary;

import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;

public class DataCompTreeProvider extends TreeProvider {
	private String[] fileExtensions = { "data", "sibs" };

	public enum ViewType {
		FLAT, HIERARCHY
	}

	private HashMap<EObject, IResource> modelToResource = null;

	private ContainerTreeNode flatRoot;
	private ContainerTreeNode hierarchyRoot;

	private ViewType activeView = ViewType.FLAT;
	private DataUtils utils;

	public DataCompTreeProvider() {
		super();
		this.utils = new DataUtils(fileExtensions);
	}

	@Override
	public TreeNode getTree() {
		switch (activeView) {
		case HIERARCHY:
			return hierarchyRoot;
		case FLAT:
		default:
			return flatRoot;
		}
	}

	public String[] getFileExtensions() {
		return fileExtensions;
	}

	public DataUtils getUtils() {
		return utils;
	}

	public HashMap<EObject, IResource> getModelToResource() {
		return modelToResource;
	}

	public ViewType getActiveView() {
		return activeView;
	}

	public void setActiveView(ViewType activeView) {
		this.activeView = activeView;
	}

	@Override
	public void loadData(IProject project) {
		final long timeStart = System.currentTimeMillis();
		modelToResource = new HashMap<>();
		if (project == null)
			return;
		try {
			for (IResource iResource : project.members()) {
				loadContentModels(iResource);
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		final long timeLoad = System.currentTimeMillis();
		
		switch (activeView) {
		case HIERARCHY:
			hierarchyRoot = new ContainerTreeNode(null, "root");
			for (EObject model : modelToResource.keySet()) {
				buildHierarchyTree(model, hierarchyRoot);
			}
			break;
		case FLAT:
		default:
			flatRoot = new ContainerTreeNode(null, "root");
			for (EObject model : modelToResource.keySet()) {
				buildFlatTree(model, flatRoot);
			}
			break;
		}
		final long timeBuild = System.currentTimeMillis();
		
		System.out.println("DataCompView - load Models: " + (timeLoad-timeStart) + " ms / create Tree: " + (timeBuild-timeLoad) + " ms");
	}

	@SuppressWarnings("restriction")
	private void loadContentModels(IResource iResource) {
		if (iResource instanceof org.eclipse.core.internal.resources.File)
			if (Arrays.asList(fileExtensions).contains(
					iResource.getFileExtension())) {
				EObject model = utils.loadModel(iResource);
				modelToResource.put(model, iResource);
			}

		if (iResource instanceof org.eclipse.core.internal.resources.Folder)
			try {
				for (IResource subRes : ((org.eclipse.core.internal.resources.Folder) iResource)
						.members()) {
					loadContentModels(subRes);
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}
	}

	private TreeNode buildFlatTree(Object obj, TreeNode parentNode) {
		TreeNode node = new ContainerTreeNode(null, "dummy");

		/*
		 * data model
		 */
		if (obj instanceof Data) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			if (((Data) obj).getTypes().size() <= 0)
				return node;
			for (Type type : ((Data) obj).getTypes()) {
				buildFlatTree(type, node);
			}
		}

		if (obj instanceof Type) {
			node = new ModelElementTreeNode(obj);
			if (obj instanceof ReferencedType)
				return node;
			for (Attribute attr : utils.getAllAttributes((Type) obj)) {
				buildFlatTree(attr, node);
			}
		}

		if (obj instanceof Attribute) {
			node = new ModelElementTreeNode(obj);

			if (obj instanceof ReferencedPrimitiveAttribute) {
				ReferencedPrimitiveAttribute attr = (ReferencedPrimitiveAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}
			if (obj instanceof ReferencedExtensionAttribute) {
				ReferencedExtensionAttribute attr = (ReferencedExtensionAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}
			if (obj instanceof ReferencedUserAttribute) {
				ReferencedUserAttribute attr = (ReferencedUserAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}
			if (obj instanceof ReferencedComplexAttribute) {
				ReferencedComplexAttribute attr = (ReferencedComplexAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}

		}

		/*
		 * sibs model
		 */
		if (obj instanceof SIBLibrary) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			SIBLibrary sl = (SIBLibrary) obj;
			if (sl.getTypes().size() <= 0)
				return node;
			for (info.scce.dime.siblibrary.Type type : sl.getTypes()) {
				buildFlatTree(type, node);
			}
		}

		if (obj instanceof info.scce.dime.siblibrary.Type)
			node = new ContainerTreeNode(obj,
					((info.scce.dime.siblibrary.Type) obj).getName());

		/*
		 * post processing
		 */
		TreeNode existingNode = parentNode.find(node.getId());
		if (existingNode == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		} else {
			return existingNode;
		}
		return node;
	}

	private TreeNode buildHierarchyTree(Object obj, TreeNode parentNode,
			boolean buildParent) {
		TreeNode node = new ContainerTreeNode(null, "dummy");

		/*
		 * data model
		 */
		if (obj instanceof Data) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			if (((Data) obj).getTypes().size() <= 0)
				return node;
			for (Type type : utils.getRootTypes((Data) obj)) {
				buildHierarchyTree(type, node);
			}
		}
		if (obj instanceof Type) {
			node = new ModelElementTreeNode(obj);
			if (obj instanceof ReferencedType) {
				ReferencedType rType = (ReferencedType) obj;
				Type refType = utils.getOriginType(rType);
				if (refType != null)
					node.setData(refType);
				if (utils.getSuperType(refType) != null)
					parentNode = buildHierarchyTree(
							utils.getSuperType(refType), parentNode, true);
			}
			for (Attribute attr : ((Type) node.getData()).getAttributes()) {
				buildHierarchyTree(attr, node);
			}
			if (!buildParent)
				for (Type type : utils.getSubTypes((Type) obj)) {
					buildHierarchyTree(type, node);
				}
		}
		if (obj instanceof Attribute) {
			node = new ModelElementTreeNode(obj);
			if (obj instanceof ReferencedPrimitiveAttribute) {
				ReferencedPrimitiveAttribute attr = (ReferencedPrimitiveAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}
			if (obj instanceof ReferencedExtensionAttribute) {
				ReferencedExtensionAttribute attr = (ReferencedExtensionAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}
			if (obj instanceof ReferencedUserAttribute) {
				ReferencedUserAttribute attr = (ReferencedUserAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}
			if (obj instanceof ReferencedComplexAttribute) {
				ReferencedComplexAttribute attr = (ReferencedComplexAttribute) obj;
				node.setData(attr.getReferencedAttribute());
			}
		}

		/*
		 * sibs model
		 */
		if (obj instanceof SIBLibrary) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			SIBLibrary sl = (SIBLibrary) obj;
			if (sl.getTypes().size() <= 0)
				return node;
			for (info.scce.dime.siblibrary.Type type : sl.getTypes()) {
				buildHierarchyTree(type, node);
			}
		}
		if (obj instanceof info.scce.dime.siblibrary.Type)
			node = new ContainerTreeNode(obj,
					((info.scce.dime.siblibrary.Type) obj).getName());

		/*
		 * post processing
		 */
		TreeNode existingNode = parentNode.find(node.getId());
		if (existingNode == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		} else {
			return existingNode;
		}
		return node;
	}

	private TreeNode buildHierarchyTree(Object obj, TreeNode parentNode) {
		return buildHierarchyTree(obj, parentNode, false);
	}
}
