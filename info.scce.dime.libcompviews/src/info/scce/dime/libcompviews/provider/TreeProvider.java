package info.scce.dime.libcompviews.provider;

import info.scce.dime.libcompviews.nodes.TreeNode;

import org.eclipse.core.resources.IProject;

public abstract class TreeProvider {
	
	private boolean resetted = true;
	
	public boolean isResetted() {
		return resetted;
	}

	public void reset() {
		resetted = true;
	}
	
	public void load(IProject project) {
		resetted = false;
		loadData(project);
	}

	abstract public void loadData(IProject project);
	
	abstract public TreeNode getTree();
}
