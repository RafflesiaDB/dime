package info.scce.dime.libcompviews.provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;

import graphmodel.GraphModel;
import info.scce.dime.libcompviews.nodes.Category;
import info.scce.dime.libcompviews.nodes.ContainerTreeNode;
import info.scce.dime.libcompviews.nodes.EObjectTreeNode;
import info.scce.dime.libcompviews.nodes.GraphModelTreeNode;
import info.scce.dime.libcompviews.nodes.ModelElementTreeNode;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.utils.GraphModelUtils;
import info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.process.process.BlueprintSIB;
import info.scce.dime.process.process.EntryPointProcessSIB;
import info.scce.dime.process.process.GUIBlueprintSIB;
import info.scce.dime.process.process.NativeFrontendSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessBlueprintSIB;
import info.scce.dime.process.process.ProcessType;
import info.scce.dime.siblibrary.SIB;
import info.scce.dime.siblibrary.SIBLibrary;

public class ControlCompTreeProvider extends LibCompTreeProvider {

	public enum ViewType {
		CATEGORY, FOLDERSTRUCT
	}

	private String[] fileExtensions = { "process", "search", "sibs" };

	private ContainerTreeNode categoryRoot;
	private ContainerTreeNode folderstructRoot;
	private IProject project;

	private ViewType activeView = ViewType.CATEGORY;

	public ControlCompTreeProvider() {
		super();
		this.utils = new GraphModelUtils(fileExtensions);
	}

	@Override
	public TreeNode getTree() {
		switch (activeView) {
		case CATEGORY:
			return categoryRoot;
		case FOLDERSTRUCT:
		default:
			return folderstructRoot;
		}
	}

	public String[] getFileExtensions() {
		return fileExtensions;
	}

	public ViewType getActiveView() {
		return activeView;
	}

	public void setActiveView(ViewType activeView) {
		this.activeView = activeView;
	}
	
	@Override
	public void loadData(IProject project) {
		this.project = project;
		final long timeStart = System.currentTimeMillis();
		modelToResource = new HashMap<>();
		resourceToModel = new HashMap<>();

		ArrayList<IResource> contents = new ArrayList<IResource>();
		try {
			for (IResource res : project.members()) {
				contents.addAll(utils.loadRecursive(res));
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}

		modelReferencedIn = new HashMap<String, ArrayList<EObject>>();
		for (IResource iResource : contents) {
			if (utils.isModelResource(iResource)) {
				EObject model = utils.loadModel(iResource);
				modelToResource.put(model, iResource);
				resourceToModel.put(iResource, model);
				if (isShowReferencedInInfo()) {
					utils.collectReferences(model, modelReferencedIn);
				}
			}
		}
		
		
		final long timeLoad = System.currentTimeMillis();

		
		switch (activeView) {
		case CATEGORY:
			categoryRoot = new ContainerTreeNode(null, "root");
			for (ModelType mType : ModelType.values()) {
				buildCategoryTree(mType, categoryRoot);
			}
			for (IModeltrafoSupporter<EObject> externProvider : ModeltrafoExtensionProvider.getAllModeltrafoSupporter()) {
				buildCategoryTree(externProvider, categoryRoot);
			}
			
			break;
		case FOLDERSTRUCT:
		default:
			folderstructRoot = new ContainerTreeNode(null, "root");
			for (Object obj : getDirectChilds(project)) {
				buildFolderTree(obj, folderstructRoot);
			}
			break;
		}
		final long timeBuild = System.currentTimeMillis();
		
		System.out.println("ControlCompView - load Models: " + (timeLoad-timeStart) + " ms / create Tree: " + (timeBuild-timeLoad) + " ms");
	}

	private TreeNode buildFolderTree(Object obj, TreeNode parentNode) {
		TreeNode node = new ContainerTreeNode(null, "dummy");

		/*
		 * iFolder
		 */
		if (obj instanceof IFolder) {
			node = new ContainerTreeNode(obj, ((IFolder) obj).getName());
			for (Object child : getDirectChilds((IContainer) obj)) {
				buildFolderTree(child, node);
			}
		}

		/*
		 * models
		 */
		if (obj instanceof GraphModel) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			if (isShowReferencedInInfo())
				buildReferencedInSubTree(obj, node);
		}

		/*
		 * sibs model
		 */
		if (obj instanceof SIBLibrary) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			SIBLibrary sl = (SIBLibrary) obj;
			if (sl.getSibs().size() <= 0)
				return node;
			for (SIB sib : sl.getSibs()) {
				buildFolderTree(sib, node);
			}
		}
		
		if (obj instanceof info.scce.dime.siblibrary.SIB)
			node = new ContainerTreeNode(obj, ((SIB) obj).getName());
		
		if (obj instanceof ProcessBlueprintSIB
				|| obj instanceof GUIBlueprintSIB) {
			node =  new ModelElementTreeNode(obj);
		}
		

		/*
		 * post processing
		 */
		if (parentNode.find(node.getId()) == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		}
		return node;
	}

	private TreeNode buildCategoryTree(Object obj, TreeNode parentNode) {
		TreeNode node = new ContainerTreeNode(null, "dummy");
		
		/*
		 * Category
		 */
		if (obj instanceof ModelType) {
			ModelType type = (ModelType) obj;
			node = new ContainerTreeNode(new Category(
					getNameForModelType(type), type), getNameForModelType(type));
			ArrayList<EObject> list = new ArrayList<EObject>();

			for (EObject model : modelToResource.keySet()) {
				if (type.equals(mapEObjectToType(model)))
					list.add(model);
			}
			if (list.size() <= 0)
				return node;
			for (EObject eObject2 : list) {
				buildCategoryTree(eObject2, node);
			}
		}
		
		
		/*
		 * Externally registered Models
		 */
		if (obj instanceof IModeltrafoSupporter) {
			IModeltrafoSupporter<EObject> externProvider = (IModeltrafoSupporter<EObject>) obj;
			node = new ContainerTreeNode(new Category(
					externProvider.getSIBName(), externProvider), externProvider.getSIBName());
			for (IResource resource : ModeltrafoExtensionProvider.getAllSupportedResourcesForProject(externProvider, project)) {
				if (resource instanceof IFile) {
					IFile file = (IFile) resource;
					buildCategoryTree(file, node);
				}
			}
		}

		if (obj instanceof NativeFrontendSIB){
			NativeFrontendSIB sib=(NativeFrontendSIB) obj;
			node = new ContainerTreeNode(obj, sib.getLabel()+ "_"+ sib.getMethodName());
		}
		
		/*
		 * models
		 */
		if (obj instanceof GraphModel) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			if (isShowReferencedInInfo())
				buildReferencedInSubTree(obj, node);
			if (obj instanceof Process) {
				Process p = (Process) obj;
				
				if (p.getProcessType() == ProcessType.NATIVE_FRONTEND_SIB_LIBRARY) {
					List<NativeFrontendSIB> nfs = p.getNativeFrontendSIBLibrarys().stream()
							.map(x -> x.getNativeFrontendSIBs().stream()).flatMap(Function.identity())
							.collect(Collectors.toList());
					for (NativeFrontendSIB sib : nfs) {
						buildCategoryTree(sib, node);
					}
				}
				
				for (EntryPointProcessSIB sib : p.getEntryPointProcessSIBs()) {
					buildCategoryTree(sib, node);
				}
					
				for (BlueprintSIB sib : p.getBlueprintSIBs()) {
					buildCategoryTree(sib, node);
				}
			}
		}

		/*
		 * sibs model
		 */
		if (obj instanceof SIBLibrary) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			SIBLibrary sl = (SIBLibrary) obj;
			if (sl.getSibs().size() <= 0)
				return node;
			for (SIB sib : sl.getSibs()) {
				buildCategoryTree(sib, node);
			}
		}
		
		if (obj instanceof info.scce.dime.siblibrary.SIB)
			node = new ContainerTreeNode(obj, ((SIB) obj).getName());
		
		if (obj instanceof EntryPointProcessSIB
				|| obj instanceof ProcessBlueprintSIB
				|| obj instanceof GUIBlueprintSIB) {
			
			node =  new ModelElementTreeNode(obj);
		}

		if (obj instanceof EObject && ModeltrafoExtensionProvider.getSupportedModelElement((EObject) obj) != null) {
			node = new EObjectTreeNode(obj);
		}
		
		if  (obj instanceof IFile) {
			IModeltrafoSupporter<EObject> provider = null;
			if (parentNode.getData() instanceof Category) {
				Category category = (Category) parentNode.getData();
				if (category.getType() instanceof IModeltrafoSupporter) {
					provider = (IModeltrafoSupporter<EObject>) category.getType();
				}
			}
			if (provider != null) {				
				node = new GraphModelTreeNode(obj, (IFile) obj);
				for (EObject content : ModeltrafoExtensionProvider.getAllSupportedElementsInResource((IFile) obj, provider)) {
					if (!(content instanceof GraphModel)) {
						buildCategoryTree(content, node);
					}
				}
			}
		}

		/*
		 * post processing
		 */
		if (parentNode.find(node.getId()) == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		}
		
		return node;
	}
}
