package info.scce.dime.libcompviews.provider;

import java.util.ArrayList;

import java.util.List;
import java.util.HashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;

import graphmodel.GraphModel;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.dad.dad.StartupProcessPointer;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.libcompviews.nodes.Category;
import info.scce.dime.libcompviews.nodes.ContainerTreeNode;
import info.scce.dime.libcompviews.nodes.GraphModelTreeNode;
import info.scce.dime.libcompviews.nodes.ModelElementTreeNode;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.utils.GraphModelUtils;
import info.scce.dime.process.process.NativeFrontendSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;
import info.scce.dime.siblibrary.SIB;
import info.scce.dime.siblibrary.SIBLibrary;

public class LinkCompTreeProvider extends LibCompTreeProvider {

	public enum ViewType {
		CATEGORY, FOLDERSTRUCT
	}

	private String[] fileExtensions = { "dad" };

	private ContainerTreeNode categoryRoot;
	private ContainerTreeNode folderstructRoot;

	private ViewType activeView = ViewType.CATEGORY;

	public LinkCompTreeProvider() {
		super();
		this.utils = new GraphModelUtils(fileExtensions);
	}

	@Override
	public TreeNode getTree() {
		switch (activeView) {
		case CATEGORY:
			return categoryRoot;
		case FOLDERSTRUCT:
		default:
			return folderstructRoot;
		}
	}

	public String[] getFileExtensions() {
		return fileExtensions;
	}

	public ViewType getActiveView() {
		return activeView;
	}

	public void setActiveView(ViewType activeView) {
		this.activeView = activeView;
	}
	
	@Override
	public void loadData(IProject project) {
		final long timeStart = System.currentTimeMillis();
		modelToResource = new HashMap<>();
		resourceToModel = new HashMap<>();

		ArrayList<IResource> contents = new ArrayList<IResource>();
		try {
			for (IResource res : project.members()) {
				contents.addAll(utils.loadRecursive(res));
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}

		modelReferencedIn = new HashMap<String, ArrayList<EObject>>();
		for (IResource iResource : contents) {
			if (utils.isModelResource(iResource)) {
				EObject model = utils.loadModel(iResource);
				modelToResource.put(model, iResource);
				resourceToModel.put(iResource, model);
				if (isShowReferencedInInfo()) {
					utils.collectReferences(model, modelReferencedIn);
				}
			}
		}
		final long timeLoad = System.currentTimeMillis();

		
		switch (activeView) {
		case CATEGORY:
			categoryRoot = new ContainerTreeNode(null, "root");
			for (ModelType mType : ModelType.values()) {
				buildCategoryTree(mType, categoryRoot);
			}
			break;
		case FOLDERSTRUCT:
		default:
			folderstructRoot = new ContainerTreeNode(null, "root");
			for (Object obj : getDirectChilds(project)) {
				buildFolderTree(obj, folderstructRoot);
			}
			break;
		}
		final long timeBuild = System.currentTimeMillis();
		
		System.out.println("LinkCompView - load Models: " + (timeLoad-timeStart) + " ms / create Tree: " + (timeBuild-timeLoad) + " ms");
	}

	private TreeNode buildFolderTree(Object obj, TreeNode parentNode) {
		TreeNode node = new ContainerTreeNode(null, "dummy");
		/*
		 * sibs model
		 */
		if (obj instanceof DAD) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			for (URLProcess u : ((DAD) obj).getURLProcesss()) {
				buildFolderTree(u, node);
			}
		}
		
		if (obj instanceof URLProcess) {
			node = new ModelElementTreeNode(obj);
		}
		
		/*
		 * post processing
		 */
		if (parentNode.find(node.getId()) == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		}
		return node;
	}

	private TreeNode buildCategoryTree(Object obj, TreeNode parentNode) {
		TreeNode node = new ContainerTreeNode(null, "dummy");
		
		/*
		 * Category
		 */
		if (obj instanceof ModelType) {
			ModelType type = (ModelType) obj;
			node = new ContainerTreeNode(new Category(
					getNameForModelType(type), type), getNameForModelType(type));
			ArrayList<EObject> list = new ArrayList<EObject>();

			for (EObject model : modelToResource.keySet()) {
				if (type.equals(mapEObjectToType(model)))
					list.add(model);
			}
			if (list.size() <= 0)
				return node;
			for (EObject eObject2 : list) {
				buildCategoryTree(eObject2, node);
			}
		}
		
		/*
		 * models
		 */
		if (obj instanceof GraphModel) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			if (isShowReferencedInInfo())
				buildReferencedInSubTree(obj, node);
			if (obj instanceof DAD) {
				DAD p = (DAD) obj;
				
				for (URLProcess up : p.getURLProcesss()) {
					if(up.getIncoming(StartupProcessPointer.class).isEmpty()) {
						buildCategoryTree(up, node);						
					}
				}
			}
		}
		
		if(obj instanceof URLProcess) {
			node = new ModelElementTreeNode(obj);
		}

		/*
		 * post processing
		 */
		if (parentNode.find(node.getId()) == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		}
		return node;
	}
}
