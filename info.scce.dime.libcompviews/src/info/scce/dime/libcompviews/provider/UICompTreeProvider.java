package info.scce.dime.libcompviews.provider;

import graphmodel.GraphModel;
import info.scce.dime.gUIPlugin.Function;
import info.scce.dime.gUIPlugin.Plugin;
import info.scce.dime.gUIPlugin.Plugins;
import info.scce.dime.libcompviews.nodes.Category;
import info.scce.dime.libcompviews.nodes.ContainerTreeNode;
import info.scce.dime.libcompviews.nodes.GraphModelTreeNode;
import info.scce.dime.libcompviews.nodes.TreeNode;
import info.scce.dime.libcompviews.utils.GraphModelUtils;
import info.scce.dime.siblibrary.SIB;
import info.scce.dime.siblibrary.SIBLibrary;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;

public class UICompTreeProvider extends LibCompTreeProvider {

	public enum ViewType {
		CATEGORY, FOLDERSTRUCT
	}

	private String[] fileExtensions = { "gui", "gp" };

	private ContainerTreeNode categoryRoot;
	private ContainerTreeNode folderstructRoot;

	private ViewType activeView = ViewType.CATEGORY;

	public UICompTreeProvider() {
		super();
		this.utils = new GraphModelUtils(fileExtensions);
	}

	@Override
	public TreeNode getTree() {
		switch (activeView) {
		case CATEGORY:
			return categoryRoot;
		case FOLDERSTRUCT:
		default:
			return folderstructRoot;
		}
	}

	public String[] getFileExtensions() {
		return fileExtensions;
	}

	public ViewType getActiveView() {
		return activeView;
	}

	public void setActiveView(ViewType activeView) {
		this.activeView = activeView;
	}
	
	@Override
	public void loadData(IProject project) {
		final long timeStart = System.currentTimeMillis();
		modelToResource = new HashMap<>();
		resourceToModel = new HashMap<>();

		ArrayList<IResource> contents = new ArrayList<IResource>();
		try {
			for (IResource res : project.members()) {
				contents.addAll(utils.loadRecursive(res));
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}

		modelReferencedIn = new HashMap<String, ArrayList<EObject>>();
		for (IResource iResource : contents) {
			if (utils.isModelResource(iResource)) {
				EObject model = utils.loadModel(iResource);
				modelToResource.put(model, iResource);
				resourceToModel.put(iResource, model);
				if (isShowReferencedInInfo()) {
					utils.collectReferences(model, modelReferencedIn);
				}
			}
		}
		final long timeLoad = System.currentTimeMillis();

		switch (activeView) {
		case CATEGORY:
			categoryRoot = new ContainerTreeNode(null, "root");
			for (ModelType mType : ModelType.values()) {
				buildCategoryTree(mType, categoryRoot);
			}
			break;
		case FOLDERSTRUCT:
		default:
			folderstructRoot = new ContainerTreeNode(null, "root");
			for (Object obj : getDirectChilds(project)) {
				buildFolderTree(obj, folderstructRoot);
			}
			break;
		}
		final long timeBuild = System.currentTimeMillis();
		
		System.out.println("UICompView - load Models: " + (timeLoad-timeStart) + " ms / create Tree: " + (timeBuild-timeLoad) + " ms");
	}

	private TreeNode buildFolderTree(Object obj, TreeNode parentNode) {
		TreeNode node = new ContainerTreeNode(null, "dummy");

		/*
		 * iFolder
		 */
		if (obj instanceof IFolder) {
			node = new ContainerTreeNode(obj, ((IFolder) obj).getName());
			for (Object child : getDirectChilds((IContainer) obj)) {
				buildFolderTree(child, node);
			}
		}

		/*
		 * models
		 */
		if (obj instanceof GraphModel) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			if (isShowReferencedInInfo())
				buildReferencedInSubTree(obj, node);
		}

		/*
		 * sibs model
		 */
		if (obj instanceof SIBLibrary) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			SIBLibrary sl = (SIBLibrary) obj;
			if (sl.getSibs().size() <= 0)
				return node;
			for (SIB sib : sl.getSibs()) {
				buildFolderTree(sib, node);
			}
		}
		if (obj instanceof SIB)
			node = new ContainerTreeNode(obj, ((SIB) obj).getName());

		/*
		 * gui plugins model
		 */
		if (obj instanceof Plugins) {
			Plugins gp = (Plugins) obj;
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			//node = new ContainerTreeNode(obj,gp.toString());
			for (Plugin sib : gp.getPlugins()) {
				buildFolderTree(sib, node);
			}
		}
		if(obj instanceof Plugin) {
			Plugin gp = (Plugin) obj;
			node = new ContainerTreeNode(obj,gp.getPath());
			for (Function sib : gp.getFunctions()) {
				buildFolderTree(sib, node);
			}
		}
		
		if(obj instanceof Function) {
			node = new ContainerTreeNode(obj, ((Function) obj).getFunctionName());
		}
		/*
		 * post processing
		 */
		if (parentNode.find(node.getId()) == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		}
		return node;
	}

	private TreeNode buildCategoryTree(Object obj, TreeNode parentNode) {
		TreeNode node = new ContainerTreeNode(null, "dummy");

		/*
		 * Category
		 */
		if (obj instanceof ModelType) {
			ModelType type = (ModelType) obj;
			node = new ContainerTreeNode(new Category(
					getNameForModelType(type), type), getNameForModelType(type));
			ArrayList<EObject> list = new ArrayList<EObject>();

			for (EObject model : modelToResource.keySet()) {
				if (type.equals(mapEObjectToType(model)))
					list.add(model);
			}
			if (list.size() <= 0)
				return node;
			for (EObject eObject2 : list) {
				buildCategoryTree(eObject2, node);
			}
		}

		/*
		 * models
		 */
		if (obj instanceof GraphModel) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			if (isShowReferencedInInfo())
				buildReferencedInSubTree(obj, node);
		}

		/*
		 * sibs model
		 */
		if (obj instanceof SIBLibrary) {
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
			SIBLibrary sl = (SIBLibrary) obj;
			if (sl.getSibs().size() <= 0)
				return node;
			for (SIB sib : sl.getSibs()) {
				buildCategoryTree(sib, node);
			}
		}
		if (obj instanceof SIB)
			node = new ContainerTreeNode(obj, ((SIB) obj).getName());

		/*
		 * gui plugins model
		 */
		if (obj instanceof Plugins) {
			Plugins gp = (Plugins) obj;
			node = new GraphModelTreeNode(obj, modelToResource.get(obj));
//			node = new ContainerTreeNode(obj,gp.toString());
			for (Plugin sib : gp.getPlugins()) {
				buildCategoryTree(sib, node);
			}
		}
		if(obj instanceof Plugin) {
			Plugin gp = (Plugin) obj;
			node = new ContainerTreeNode(obj,gp.getPath());
			for (Function sib : gp.getFunctions()) {
				buildCategoryTree(sib, node);
			}
		}
		
		if(obj instanceof Function) {
			node = new ContainerTreeNode(obj, ((Function) obj).getFunctionName());
		}

		
		/*
		 * post processing
		 */
		if (parentNode.find(node.getId()) == null) {
			parentNode.getChildren().add(node);
			node.setParent(parentNode);
		}
		return node;
	}
}
