grammar info.scce.dime.GUIPlugin with org.eclipse.xtext.common.Terminals

import "http://www.eclipse.org/emf/2002/Ecore" as ecore
// import "platform:/resource/info.scce.dime/src-gen/model/Data.ecore" as data
import "http://www.GUIPlugin"

//generate gUIPlugin "http://www.GUIPlugin"

/**
 * Plugins is a list of multiple GUI plug ins
 */
Plugins :
	(imports+=Import)*
	(plugins+=DartPlugin)*
	(plugins+=Plugin)*
;

DartPlugin:
	'dartPlugin' path=ID '{'
		(style=Style)?
		(script=Script)?
		(template=Template)?
		(functions+=DartClass)
	'}'
;

DartClass returns Function:
	'component' functionName=ID (isReCalled?='recalled')? '{'
		(parameters+=InputParameter)*
		(events+=EventListener)*
		(outputs+=Output)*
	'}'
;

Import:
	'import' path=STRING 'as' name=ID
;

/**
 * A plug in is defined by a unique name
 * and combines Stylesheet, Javascript files and a HTML file.
 * In addition multiple functions can be defined, which has to be defined in
 * the Javascript files. This provides the possibility to used multiple functions
 * of one file and just load the resources once in the running application
 */
Plugin:
	'plugin' path=ID '{'
		(style=Style)?&
		(script=Script)?&
		(template=Template)?
		(functions+=Function)*
	'}'
;

/**
 * The function is defined by the Javascript function name
 * present in one of the plugin script files.
 * For each argument of the Javascript function, an input
 * parameter has to be defined. For every defined input, a
 * corresponding input port will be added to the GUI plug in SIB
 * For every possible branch of the corresponding SIB an output
 * has to be defined. The output branch is fired by a callback
 * which has to specified as the last argument of the Javascript function
 */
Function:
	'function' functionName=ID (isReCalled?='recalled')? '{'
		(parameters+=InputParameter)*
		(events+=EventListener)*
		(outputs+=Output)*
	'}'
;

EventListener:
	'event' name=ID '{'
		(parameters+=AbstractParameter)*
	'}'
;

/**
 * The output represents a branch of the resulting GUI plug in SIB.
 * As a result of this the branch has to be uniquely named for the surrounding function.
 * The output can be extended by parameters which will result in output ports of the branch.
 * The last argument of the corresponding Javascript function is the callback which has to be called
 * when a branch should be taken.
 * The callback expects a JSON object in the form:
 * {
 * 	branchName: "<name of the branch taken>",
 * 	values: {
 * 			<port name>: "port value",
 * 			...
 * 		}
 * }
 */
Output:
	'output' outputName=ID '{'
		(parameters+=AbstractParameter)*
	'}'
;

/**
 * The input parameter defines an input port of the GUI plug in SIB.
 * The input parameter can be extended by the 'sync' option.
 * The sync option means that changes done to the variable binded to the input
 * port can be propagated back to the GUI model which surrounds this GUI plug in SIB.
 * The corresponding Javascript function has to be extended with a second callback argument
 * after the original argument for this input parameter:
 * function <function name>(<input parameter>,<input parameter sync callback>,...)
 * The given input sync callback can be used to send updates back to the GUI model in the form:
 * <input parameter sync callback>(<updated value>);
 * 
 */
InputParameter:
	(ComplexInputParameter|PrimitiveInputParameter|GenericInputParameter) (isSync?='sync')?
;

ComplexInputParameter returns ComplexInputParameter:
	parameter=ComplexParameter 
;

PrimitiveInputParameter:
	parameter=PrimitiveParameter
;

GenericInputParameter:
	parameter=GenericParameter
;

/**
 * A parameter which represents a port in the GUI plug in SIB
 * has to be specified by a unique name for the surrounding output or function,
 * a list status and a primitive data type.
 */
AbstractParameter:
	(ComplexParameter|PrimitiveParameter|GenericParameter)
;

ComplexParameter:
	'complexParameter' name=STRING
	':' (alias=[Import] '.' type=[ecore::EObject|StringOrID])
	isList?=('[]')?
	(selective=Selective)?
;

Selective: {Selective}
	'with' (attributePaths+=AttributePath | '{' attributePaths+=AttributePath (',' attributePaths+=AttributePath)* '}')
;

AttributePath:
	head=[ecore::EObject|StringOrID] (('.' tail=AttributePath) | (selective=Selective)?)
;

StringOrID:
	STRING | ID
;

PrimitiveParameter:
	'primitiveParameter' name=STRING ':' (type=PrimitiveType) isList?=('[]')?
;

GenericParameter:
	'genericParameter' name=STRING ':<' (typeParameterName=ID) '>' isList?=('[]')?
;

/**
 * The Javascript files for a plugin can be defined in a list of workspace relative paths to
 * the files. The order in which the files are listed is the same order in which the files
 * are included in the HTML file by script tags.
 */
Script:
{Script}
	'scripts' '[' files+=File(',' files+=File)* ']'
;

/**
 * The file path is a String
 */
File:
	path=STRING
;

/**
 * The template is defined by the workspace relative path to the HTML file.
 * The HTML content of the specified file will be included in the spot where the 
 * corresponding GUI plug in SIB is placed.
 */
Template:
{Template}
	'template' file=File (placholders=Placeholders)?
;

Placeholders:
	{Placeholders}
	'placeholder' '[' placeholders+=Placeholder(',' placeholders+=Placeholder)* ']'
;

Placeholder:
	{Placeholder}
	name=ID
;

/**
 * The Stylesheet CSS files for a plugin can be defined in a list of workspace relative paths to
 * the files. The order in which the files are listed is the same order in which the files
 * are included in the HTML file by link tags.
 */
Style:
{Style}
	'styles' '[' files+=File(',' files+=File)* ']'
;

/**
 * For the input and output parameters which represent ports in the GUI model
 * only primitive types are available.
 */
enum PrimitiveType returns PrimitiveType:
				Text = 'text' | Integer = 'integer' | Real = 'real' | Boolean = 'boolean' | Timestamp = 'timestamp';

