package info.scce.dime.validation

import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import info.scce.dime.gUIPlugin.GUIPluginPackage
import info.scce.dime.gUIPlugin.Import
import org.eclipse.xtext.validation.Check

/**
 * Custom validation rules.
 * 
 * @author Steve Bosselmann
 */
class GUIPluginValidator extends AbstractGUIPluginValidator {

	protected extension ResourceExtension = new ResourceExtension

	@Check
	def checkImport(Import it) {
		if (path.nullOrEmpty)
			error("No path specified")
		else if (!eResource?.project?.getFile(path)?.exists)
			error('''The specified file: "«path»" does not exist''')
	}
	
	def error(Import imp, String msg) {
		error(msg, GUIPluginPackage.Literals.IMPORT__PATH, "Failed to load resource")
	}
	
}
