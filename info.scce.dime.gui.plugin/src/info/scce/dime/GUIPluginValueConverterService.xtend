package info.scce.dime

import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.common.services.DefaultTerminalConverters
import org.eclipse.xtext.conversion.ValueConverter
import org.eclipse.xtext.conversion.impl.QualifiedNameValueConverter
import org.eclipse.xtext.nodemodel.ILeafNode
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter
import org.eclipse.xtext.conversion.IValueConverter
import org.eclipse.emf.ecore.EObject
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.scoping.GUIPluginScopeProvider
import graphmodel.Container
import graphmodel.Node
import com.google.common.collect.HashBiMap

/**
 * @author Steve Bosselmann
 */
class GUIPluginValueConverterService extends DefaultTerminalConverters {

	@ValueConverter(rule="StringOrID")
	def QualifiedNameValueConverter getStringOrIDConverter() {
				
		return new QualifiedNameValueConverter() {
			
			// adds leading and trailing quotes if the value contains whitespaces
			override toString(String value) {
				if (value?.contains(' ')) {
					'"' + value + '"'
				} else {
					value
				}
			}
			
			// call the custom delegateToValue method here
			override toValue(String string, INode node) {
				node.leafNodes
					.filter[grammarElement instanceof RuleCall]
					.map[delegateToValue]
					.filterNull
					.join
			}
			
			// removes leading and trailing quotes from the text if existent
			override delegateToValue(ILeafNode leafNode) {
				if (delegateConverter === null) {
					delegateConverter = getConverter("ID");
				}
				delegateConverter.toValue(
					leafNode.text?.replaceAll('^"+', "").replaceAll('"+$', ""), leafNode
				) as String
			}
			
		}
	}
}
