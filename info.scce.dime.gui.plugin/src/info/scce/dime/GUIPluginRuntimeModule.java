package info.scce.dime;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.linking.ILinker;
import org.eclipse.xtext.linking.impl.Linker;

/**
 * Registers components to be used at runtime / without the Equinox extension registry.
 * 
 * @author Steve Bosselmann
 */
public class GUIPluginRuntimeModule extends info.scce.dime.AbstractGUIPluginRuntimeModule {

	@Override
    public Class<? extends IValueConverterService> bindIValueConverterService() {
        return GUIPluginValueConverterService.class;
    }

	public java.lang.Class<? extends ILinker> bindILinker() {
		return GUIPluginLinker.class;
	}

	public static class GUIPluginLinker extends Linker {
		@Override
		protected boolean isClearAllReferencesRequired(Resource resource) {
			return false;
		}
	}
	
}
