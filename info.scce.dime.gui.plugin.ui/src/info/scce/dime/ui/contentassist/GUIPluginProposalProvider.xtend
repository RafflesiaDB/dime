package info.scce.dime.ui.contentassist

import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import graphmodel.Container
import graphmodel.Node
import info.scce.dime.gUIPlugin.AttributePath
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.scoping.GUIPluginScopeProvider
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * @author Steve Bosselmann
 */
class GUIPluginProposalProvider extends AbstractGUIPluginProposalProvider {

	protected extension WorkspaceExtension = new WorkspaceExtension
	protected extension GUIPluginScopeProvider = new GUIPluginScopeProvider

	override completeImport_Path(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		model.IResource.project
			.getFiles[fileExtension == "data"]
			.map['''"«projectRelativePath»"''']
			.forEach[
				acceptor.accept(createCompletionProposal(context))
			]
	}
	
	override completeAttributePath_Head(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val path = switch it : model {
			AttributePath case head.nameMatchesPrefix(context.prefix): it
			default: findParent(AttributePath)
		}
		val typeNode = switch it : path {
			AttributePath case head.isOfType("ComplexAttribute"): (head as Node).complexType
			default: model.findParent(ComplexParameter)?.type as Container
		}
		typeNode.collectAttributes.values.forEach[
			acceptor.accept(createCompletionProposal(context))
		]
	}
	
	def <T extends EObject> T findParent(EObject eobj, Class<T> clazz) {
		var parent = eobj.eContainer
		while (parent !== null) {
			if (clazz.isInstance(parent)) {
				return parent as T
			}
			parent = parent.eContainer
		}
	}
	
	def nameMatchesPrefix(EObject attribute, String prefix) {
		prefix.isEmpty || (attribute as Node)?.getValueOfAttribute("name") == prefix
	}
}
