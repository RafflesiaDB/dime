package info.scce.dime.objectlang.ui

import com.google.inject.Inject
import info.scce.dime.objectlang.generator.IObjectLangGenerator
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.xtext.builder.BuilderParticipant
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2
import org.eclipse.xtext.builder.MonitorBasedCancelIndicator
import org.eclipse.xtext.generator.GeneratorContext
import org.eclipse.xtext.resource.IContainer
import org.eclipse.xtext.resource.IResourceDescription.Delta
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider

class ObjectLangBuilderParticipant extends BuilderParticipant {

//    @Inject ResourceDescriptionsProvider resourceDescriptionsProvider
//
//    @Inject IContainer.Manager containerManager
//
//    @Inject(optional = true) IObjectLangGenerator generator
//
//    protected val buildSemaphor = new ThreadLocal<Boolean>()
//
//    override void build(IBuildContext context, IProgressMonitor monitor) throws CoreException {
//    	buildSemaphor.set(false)
//        super.build(context, monitor)
//    }
//
//    override void handleChangedContents(Delta delta, IBuildContext context,
//            EclipseResourceFileSystemAccess2 fileSystemAccess) throws CoreException {
//        
//        System.err.println("super.handleChangedContents START")
//        super.handleChangedContents(delta, context, fileSystemAccess);
//        System.err.println("super.handleChangedContents DONE")
//        if (!buildSemaphor.get && generator !== null) {
//        	System.err.println("invokeGenerator START") 
//            invokeGenerator(delta, context, fileSystemAccess)
//        	System.err.println("invokeGenerator DONE") 
//        }
//    }
//    
//    def void invokeGenerator(Delta delta, IBuildContext context, EclipseResourceFileSystemAccess2 access) {
//    	buildSemaphor.set(true)
//        val resource = context.resourceSet.getResource(delta.uri, true)
//        if (shouldGenerate(resource, context)) {
//            val index = resourceDescriptionsProvider.createResourceDescriptions
//            val resDesc = index.getResourceDescription(resource.URI)
//            val visibleContainers = containerManager.getVisibleContainers(resDesc, index)
//            for (c : visibleContainers) {
//                for (rd : c.resourceDescriptions) {
//                    context.resourceSet.getResource(rd.URI, true)
//                }
//            }
//
//            val cancelIndicator = new MonitorBasedCancelIndicator(new NullProgressMonitor)
//            val generatorContext = new GeneratorContext
//            generatorContext.setCancelIndicator(cancelIndicator)
//            generator.doGenerate(context.resourceSet, access, generatorContext)
//        }
//    }

}