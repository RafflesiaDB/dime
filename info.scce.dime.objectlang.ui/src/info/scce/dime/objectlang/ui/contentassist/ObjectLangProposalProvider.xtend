package info.scce.dime.objectlang.ui.contentassist

import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.objectlang.ObjectLangExtension
import info.scce.dime.objectlang.objectLang.AttributeDef
import info.scce.dime.objectlang.objectLang.AttributeValueDef
import info.scce.dime.objectlang.objectLang.ComplexListAttributeValuesDef
import info.scce.dime.objectlang.objectLang.ObjectDef
import info.scce.dime.objectlang.objectLang.ObjectsModel
import info.scce.dime.objectlang.objectLang.PrimitiveListAttributeValueDef
import info.scce.dime.objectlang.objectLang.ProcessArgumentDef
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * @author Steve Bosselmann
 */
class ObjectLangProposalProvider extends AbstractObjectLangProposalProvider {

	extension DataExtension = DataExtension.instance
	extension WorkspaceExtension = new WorkspaceExtension
	extension ObjectLangExtension = new ObjectLangExtension
	extension FileExtension = new FileExtension

	override completeGenerateDef_ModelId(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		acceptor.accept(createCompletionProposal(EcoreUtil.generateUUID, context))
	}
	
	override completeImport_Path(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		model.IResource.project
			.getFiles[#["objects"].contains(fileExtension)]
			.flatMap[
				val path = projectRelativePath
				getContent(ObjectsModel)
					?.generations
					?.map[modelId]?.filterNull
					?.map[it -> '''«it» («path»)''']
			]
			.forEach[ pair |
				acceptor.accept(createCompletionProposal(pair.key, pair.value, null, context))
			]
			
		model.IResource.project
			.getFiles[#["data", "process"].contains(fileExtension)]
			.map['''"«projectRelativePath»"''']
			.forEach[
				acceptor.accept(createCompletionProposal(context))
			]
	}
	
	override completeAttributeDef_Key(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val objDef =
			if (model instanceof ObjectDef) {
				model
			} else if (model.eContainer instanceof ObjectDef) {
				model.eContainer
			}
		if (objDef instanceof ObjectDef) {
			val usedNames = objDef.attributes.map[key].filter(Attribute).map[name].toSet
			val type = objDef.type
			if (type instanceof Type) {
				val candidates = type.inheritedAttributes.map[name].toList
				candidates.filter[!usedNames.contains(it)].forEach[
					acceptor.accept(createCompletionProposal(context))
				]
			}
		}
		else super.completeAttributeDef_Key(model, assignment, context, acceptor)
	}
	
	override completeAttributeValueReferenceDef_Value(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val attrDef = 
			if (model instanceof AttributeDef) {
				model
			} else if (model.eContainer instanceof AttributeDef) {
				model.eContainer
			}
		if (attrDef instanceof AttributeDef) {
			val attribute = attrDef.key as Attribute
			if (!attribute?.isList) {
				attrDef.linkableNamedObjectsOfMatchingType.map[name].toList.forEach[
					acceptor.accept(createCompletionProposal(context))
				]
			}
		}
		else super.completeAttributeValueReferenceDef_Value(model, assignment, context, acceptor)
	}
	
	override completeComplexListAttributeValuesDef_ValuesDef(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof ObjectDef) {
			if (assignment.feature == "values") {
				return // propose nothing here
			}
		}
		else if (model instanceof AttributeDef) {
			val attribute = model.key as Attribute
			if (attribute.isList) {
				val candidates = model.linkableNamedObjectsOfMatchingType
				candidates.map[name].forEach[
					acceptor.accept(createCompletionProposal(context))
				]
			}
		}
		else if (model instanceof AttributeValueDef) {
			val valueDef = model as AttributeValueDef
			switch valueDef {
				ComplexListAttributeValuesDef: {
					valueDef.linkableNamedObjectsOfMatchingType.map[name].forEach[
						acceptor.accept(createCompletionProposal(context))
					]
				}
				PrimitiveListAttributeValueDef: {
					valueDef.linkableNamedObjectsOfMatchingType.map[name].forEach[
						acceptor.accept(createCompletionProposal(context))
					]
				}
			}
		}
		else super.completeComplexListAttributeValuesDef_ValuesDef(model, assignment, context, acceptor)
	}
	
	override complete_BOOLEAN(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof ObjectDef
				&& ruleCall.eContainer instanceof Assignment) {
			val feature = (ruleCall.eContainer as Assignment).feature
			if (feature == "values") {
				return // propose nothing here
			}
		}
		if (model instanceof AttributeDef) {
			val attrDef = model as AttributeDef
			val attr = attrDef.key
			switch it : attr {
				PrimitiveAttribute case isBooleanAttribute:
					#["true","false"].forEach[
						acceptor.accept(createCompletionProposal(context))
					]
			}
		}
		else #["true","false"].forEach[
			acceptor.accept(createCompletionProposal(context))
		]
	}
	
	override complete_INT(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof ObjectDef
				&& ruleCall.eContainer instanceof Assignment) {
			val feature = (ruleCall.eContainer as Assignment).feature
			if (feature == "values") {
				return // propose nothing here
			}
		}
		if (model instanceof AttributeDef) {
			val attrDef = model as AttributeDef
			val attr = attrDef.key
			switch it : attr {
				PrimitiveAttribute case isIntegerAttribute,
					PrimitiveAttribute case isTimestampAttribute:
						super.complete_INT(model, ruleCall, context, acceptor)
			}
		}
		else super.complete_INT(model, ruleCall, context, acceptor)
	}
	
	override complete_LongValue(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof ObjectDef
				&& ruleCall.eContainer instanceof Assignment) {
			val feature = (ruleCall.eContainer as Assignment).feature
			if (feature == "values") {
				return // propose nothing here
			}
		}
		if (model instanceof AttributeDef) {
			val attrDef = model as AttributeDef
			val attr = attrDef.key
			switch it : attr {
				PrimitiveAttribute case isIntegerAttribute,
					PrimitiveAttribute case isTimestampAttribute:
						super.complete_INT(model, ruleCall, context, acceptor)
			}
		}
		else super.complete_INT(model, ruleCall, context, acceptor)
	}
	
	override complete_STRING(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof ObjectDef
				&& ruleCall.eContainer instanceof Assignment) {
			val feature = (ruleCall.eContainer as Assignment).feature
			if (feature == "values") {
				return // propose nothing here
			}
		}
		
		else if (model instanceof AttributeDef) {
			val attrDef = model as AttributeDef
			val attr = attrDef.key
			switch it : attr {
				PrimitiveAttribute case isTextAttribute:
					super.complete_STRING(model, ruleCall, context, acceptor)
					
				ComplexAttribute case isEnumAttribute: {
					enumType?.enumLiterals?.map['''"«name»"'''].forEach[
						acceptor.accept(createCompletionProposal(context))
					]
				}
			}
		}
		
		else if (model instanceof ProcessArgumentDef) {
			val argDef = model
			val attr = argDef.key
			switch it : attr {
				PrimitiveOutputPort case hasPrimitiveType(PrimitiveType.TEXT):
					super.complete_STRING(model, ruleCall, context, acceptor)
					
				ComplexOutputPort case isEnumPort: {
					enumType?.enumLiterals?.map['''"«name»"'''].forEach[
						acceptor.accept(createCompletionProposal(context))
					]
				}
			}
		}
		
		else if (model instanceof AttributeValueDef) {
			val valueDef = model as AttributeValueDef
			val container = valueDef.eContainer
			if (container instanceof AttributeDef) {
				val attrDef = container
				val attr = attrDef.key
				switch it : attr {
					PrimitiveAttribute case isTextAttribute:
						super.complete_STRING(model, ruleCall, context, acceptor)
						
					ComplexAttribute case isEnumAttribute: {
						enumType?.enumLiterals?.map['''"«name»"'''].forEach[
							acceptor.accept(createCompletionProposal(context))
						]
					}
				}
			}
			
			if (container instanceof ProcessArgumentDef) {
				val argDef = container
				val attr = argDef.key
				switch it : attr {
					PrimitiveOutputPort case hasPrimitiveType(PrimitiveType.TEXT):
						super.complete_STRING(model, ruleCall, context, acceptor)
						
					ComplexOutputPort case isEnumPort: {
						enumType?.enumLiterals?.map['''"«name»"'''].forEach[
							acceptor.accept(createCompletionProposal(context))
						]
					}
				}
			}
		}
		
		else super.complete_STRING(model, ruleCall, context, acceptor)
	}
	
	override complete_ID(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof ComplexListAttributeValuesDef) {
			return // propose nothing here
		}
		else super.complete_ID(model, ruleCall, context, acceptor)
	}
}