package info.scce.dime.siblibrary.ui.contentassist

import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * @author Steve Bosselmann
 */
class SIBLibraryProposalProvider extends AbstractSIBLibraryProposalProvider {
	
	protected extension WorkspaceExtension = new WorkspaceExtension

	override completeImport_Path(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		model.IResource.project
			.getFiles[fileExtension == "data"]
			.map['''"«projectRelativePath»"''']
			.forEach[
				acceptor.accept(createCompletionProposal(context))
			]
	}
}
