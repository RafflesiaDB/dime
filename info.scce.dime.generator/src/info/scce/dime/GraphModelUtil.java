package info.scce.dime;

import graphmodel.GraphModel;

public class GraphModelUtil {
//	public static void setId(ModelElement modelElement, String id) {
//		org.eclipse.emf.transaction.TransactionalEditingDomain dom = org.eclipse.emf.transaction.TransactionalEditingDomain.Factory.INSTANCE.getEditingDomain(modelElement.eResource().getResourceSet());
//		if (dom == null)
//			dom = org.eclipse.emf.transaction.TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain(modelElement.eResource().getResourceSet());
//		dom.getCommandStack().execute(new org.eclipse.emf.transaction.RecordingCommand(dom) {
//		
//			@Override
//			protected void doExecute() {
//				modelElement.setId(id);
//			}
//		});
//	}
	
	public static void execWithTransaction(GraphModel modelElement, Runnable impl) {
		org.eclipse.emf.transaction.TransactionalEditingDomain dom = org.eclipse.emf.transaction.TransactionalEditingDomain.Factory.INSTANCE.getEditingDomain(modelElement.eResource().getResourceSet());
		if (dom == null)
			dom = org.eclipse.emf.transaction.TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain(modelElement.eResource().getResourceSet());
		dom.getCommandStack().execute(new org.eclipse.emf.transaction.RecordingCommand(dom) {
		
			@Override
			protected void doExecute() {
				impl.run();
			}
		});
	}
}
