package info.scce.dime.generator.dad

import de.jabc.cinco.meta.plugin.dsl.ProjectDescriptionLanguage
import info.scce.dime.dad.dad.DAD
import info.scce.dime.generator.dad.operations.OperationsGenTemplate

class OperationsGenerator {
	
	protected extension ProjectDescriptionLanguage = new ProjectDescriptionLanguage
	
	DAD dad
	
	new(DAD dad) {
		this.dad = dad	
	}
	
	def generate() {
		if (!dad.serverss.nullOrEmpty && !dad.serverss.head.servers.nullOrEmpty) {
			new OperationsGenTemplate(dad).createProject	
			
		}
	}
	
}