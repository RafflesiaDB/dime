package info.scce.dime.generator.dad

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.UserType
import info.scce.dime.data.factory.DataFactory
import graphmodel.internal.InternalModelElementContainer

class DataPreprocessing extends CincoRuntimeBaseClass {
	
	public val static SWITCHED_TO_ATTR = "dywaSwitchedTo"

	val registry = ReferenceRegistry.instance
	
	val generatedAttributes = <ComplexAttribute> newArrayList
	
	def execute() {
		registry.lookup(Data)
			.flatMap[types]
			.drop(ReferencedType)
			.drop(UserType)
			.filter[!userAttributes.isEmpty]
			.forEach[ type |
				println("[DataPreprocessing] Generate dywaSwitchedTo attribute for " + type.name + " | workbench running: " + org.eclipse.ui.PlatformUI.isWorkbenchRunning)
				val attr =
					if (org.eclipse.ui.PlatformUI.isWorkbenchRunning) {
						type.newComplexAttribute(new String(type.id.bytes.reverse), 0, 0) => [
							name = SWITCHED_TO_ATTR
							dataType = type
							generatedAttributes.add(it)
						]
					}
					/*
					 * workaround that uses the basic DataFactory in headless mode
					 */
					else new DataFactory().createComplexAttribute(
						new String(type.id.bytes.reverse),
						null, // internal model element
						type.internalElement as InternalModelElementContainer,
						false // run hooks
					) => [
						type.internalContainerElement.modelElements.add(it.internalElement)
					]
				attr => [
					it.internalElement.eSetDeliver(false)
					name = SWITCHED_TO_ATTR
					dataType = type
					it.internalElement.eSetDeliver(true)
					generatedAttributes.add(it)
				]
			]
	}
	
	def cleanup() {
		generatedAttributes.forEach[it.delete]
	}
	
}