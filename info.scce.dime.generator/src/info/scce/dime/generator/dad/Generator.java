package info.scce.dime.generator.dad;

import static info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.generateContent;
import static info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.isTransformable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;
import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;
import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.StartupProcessPointer;
import info.scce.dime.dad.dad.SystemUser;
import info.scce.dime.dad.mcam.cli.DADExecution;
import info.scce.dime.data.data.Data;
import info.scce.dime.data.data.UserType;
import info.scce.dime.data.mcam.cli.DataExecution;
import info.scce.dime.generator.dad.replacement.blueprint.BlueprintReplacement;
import info.scce.dime.generator.dad.replacement.generic.GenericReplacement;
import info.scce.dime.generator.data.DywaDataGenerator;
import info.scce.dime.generator.data.LongRunningDataGenerator;
import info.scce.dime.generator.gui.dart.GUIDartGenerator;
import info.scce.dime.generator.migration.EnumMigrator;
import info.scce.dime.generator.process.BackendProcessGenerator;
import info.scce.dime.generator.process.BackendProcessGeneratorHelper;
import info.scce.dime.generator.process.StartupProcessGenerator;
import info.scce.dime.generator.process.TODOListGenerator;
import info.scce.dime.generator.rest.FileControllerGenerator;
import info.scce.dime.generator.rest.TOGenerator;
import info.scce.dime.gui.helper.GUIExtensionProvider;
import info.scce.dime.gui.mcam.cli.GUIExecution;
import info.scce.dime.process.mcam.cli.ProcessExecution;
import info.scce.dime.process.process.GuardContainer;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;
import info.scce.dime.profile.actions.ApplyProfile;
import info.scce.mcam.framework.processes.CheckProcess;


public class Generator implements IGenerator<DAD> {
	
	private IPath outlet;
	private IProgressMonitor monitor = new NullProgressMonitor();
	
	private DAD model;
	private GenerationContext genctx;
	private BackendProcessGeneratorHelper backendProcessGeneratorHelper;
	
	private Data processDataModel;
	private List<Data> dataModels;
	private List<CheckProcess<?,?>> checkresults;
	private List<ApplyProfile> applyProfileActions;
	
	private boolean proceedWithInvalidModels = true;
	private boolean headless = false;
	private BlueprintReplacement blueprintReplacement;
	private GenericReplacement genericReplacement;
	private DataPreprocessing dataPreprocessing;
	private long debugTime;

	public void collectTasks(DAD model, IPath outlet, CompoundJob job) {
		this.model = model;
		this.outlet = outlet;
		GUIExtensionProvider.disableRefresh();
		this.genctx = new GenerationContext(model);
		
//		Map<String, Runnable> checks = new HashMap<>();
		
		job.consume(5)
			.task("Waiting for builds to finish...",
					this::waitForBuilds)
			
			.task("Creating basic backend template...",
					this::copyStaticResources)
			
			.task("Copy additional resources...",
					this::copyAdditionalResources)
		
			.task("Initializing Generator...",
					this::initialize)

		    .task("Applying profiles...",
		    		this::applyProfiles)
		    
		    .task("Replacing blueprints...",
		    		this::replaceBlueprints)
		    
		    .task("Replacing generic SIBs...",
		    		this::replaceGenericSIBs)
		    
		    .task("Pre-processing data models...",
		    		this::preprocessDataModels)
		    
		    .task("Collecting models...",
		    		this::collectModels)
		    
//		    .task("Collecting checks...",
//		    		() -> collectChecks(checks))
//		    			
//		  .consume(20, "Validating models...")
//		    .taskForEach(() -> checks.entrySet().stream(),
//		    		entry -> entry.getValue().run(), // Runnable
//		    		Entry::getKey) // Task name
//		    .task("Evaluating validation results...",
//		    		() -> processValidationResults())
//		    	.cancelIf(() -> !proceedWithInvalidModels)
		  
		  .consume(50, "Generating...") // Sequential, as these generators produce inputs for subsequent ones
//		    .task("Generating Interaction/Process context data model...",
//		    		this::generateLongRunningData)
		    .task("Generating data models to DyWA...",
		    		this::generateDataModels)
		  
		  .consume(30) // Sequential, as some of these generators output nothing when used concurrently

		  	// generate GUI before process + REST backend because it pre-computes the necessary selectives
		    .task("Generating GUI...",
		    		this::generateGUI)
		    .task("Generating process class for backend processes...",
		    		this::generateBackendProcess)
		    .task("Generating startup process...",
		    		this::generateStartupProcess)
		    .task("Generating external SIBs",
		    		this::generateGenericSIBModels)
//		    .task("Generating TODOList retriever...",
//		    		this::generateTODOList)
		    .task("Generate Authenticator",
		    		this::generateAuthenticator)
//		    .task("Generating Search Models...",
//		    		this::generateSearchModels)
		    .task("Generating default REST TOs...",
		    		this::generateDefaultRestTOs)
		    .task("Generate File Token Cache",
		    		this::generateFileTokenCache)
		    .task("Generate Migrators",
		    		this::generateMigrators)
		    /**
		     * Interactions are no longer present
		     */
//		    .task("Generating Interaction adapters...",
//		    		this::generateInteractionAdapters)
		    .task("Generating Operations folder structure", 
		    		this::generateOperations)
		    .task(this::enableExtensionCacheRefresh)
		
		  .onDone(this::cleanup);
	}
	
	void waitForBuilds() {
		IJobManager jobManager = Job.getJobManager();
		
		Object family = ResourcesPlugin.FAMILY_AUTO_BUILD;
		Job[] jobs = jobManager.find(family);
		if (jobs.length < 1) {
			family = ResourcesPlugin.FAMILY_MANUAL_BUILD;
			jobs = jobManager.find(family);
		}
		while (jobs.length > 0) try { // Wait for manual build to finish if running
			System.out.println("[INFO] Waiting for running build(s) to finsh...");
			jobManager.join(family, monitor);
			family = ResourcesPlugin.FAMILY_AUTO_BUILD;
			jobs = jobManager.find(family);
			if (jobs.length < 1) {
				family = ResourcesPlugin.FAMILY_MANUAL_BUILD;
				jobs = jobManager.find(family);
			}
		} catch (OperationCanceledException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("[INFO] No running builds, asserting ReferenceRegistry is initialized...");
		ReferenceRegistry.getInstance().lookup(DAD.class);
		System.out.println("[INFO] ReferenceRegistry is initialized, proceed.");
	}
	
	public void generate(DAD model, IPath outlet, IProgressMonitor monitor) {
		CompoundJob job = JobFactory.job("DAD Generator", monitor, false);
		collectTasks(model, outlet, job);
		job.onFinishedShowMessage("Code generation successful.")
		   .schedule();
	}
	
	void initialize() {
		this.debugTime = System.currentTimeMillis();
		this.genctx = new GenerationContext(model);
		this.backendProcessGeneratorHelper = new BackendProcessGeneratorHelper(genctx);
		this.applyProfileActions = new ArrayList<>();
	}
	
	void collectModels() {
		// trigger model collection
		genctx.getUsedModels();
	}
	
	void applyProfiles() {
		model.getProfileContainers().stream()
			.flatMap((container) -> container.getProfileSIBs().stream())
			.filter((sib) -> sib.isActive())
			.map((sib) -> sib.getReferencedProfile())
			.filter((profile) -> profile != null)
			.forEach((profile) -> {
				ApplyProfile applyProfile = new ApplyProfile();
				applyProfileActions.add(applyProfile);
				applyProfile.execute(profile);
			});
	}
	
	void replaceBlueprints() {
		blueprintReplacement = new BlueprintReplacement(model.getBlueprintingConfig());
		blueprintReplacement.execute();
	}
	
	void replaceGenericSIBs() {
		genericReplacement = new GenericReplacement();
		genericReplacement.execute();
	}
	
	void preprocessDataModels() {
		dataPreprocessing = new DataPreprocessing();
		dataPreprocessing.execute();
	}
	
	void cleanup() {
		if(!headless) {
			blueprintReplacement.cleanup();
			genericReplacement.cleanup();
			dataPreprocessing.cleanup();
			ReferenceRegistry.getInstance().clearRegistry();
			System.out.println("[INFO] Generation finished in " + (System.currentTimeMillis() - debugTime) + " ms");
		}
	}
	
	private void processValidationResults() {
		proceedWithInvalidModels = true;
		checkresults.stream()
			.filter(result -> result.hasErrors()) // || result.hasWarnings()
			.forEach(System.out::println);
		
		if (checkresults.stream().anyMatch(CheckProcess::hasErrors)) {
			Display display = Display.getDefault();
			if(display!=null) {
				display.syncExec(new Runnable() {
					@Override
					public void run() {
						proceedWithInvalidModels = MessageDialog
							.openConfirm(null, "Invalid models!",
								"Some models used in code-generation are not valid! Check Project-Validation-View for more information... "
								+ "Continue anyway?");
					}
				});
			}else {
				proceedWithInvalidModels = false;
			}
		}
	}

	// Generate interaction/process context data model
	private void generateLongRunningData() {
		final LongRunningDataGenerator lrdGen = new LongRunningDataGenerator(genctx);
		processDataModel = lrdGen.generate(model, genctx.getUsedProcesses(), outlet, monitor);
	}
	
	// Generate data models to dywa.
	private void generateDataModels() {
		dataModels = model.getDataComponents().stream()
				.map(dc -> dc.getModel())
				.collect(Collectors.toList());

		final DywaDataGenerator dywaDataGenerator = new DywaDataGenerator();
		dywaDataGenerator.generate(dataModels, outlet);
	}

	private void generateGenericSIBModels() {
		Set<EObject> referencedObjects = genctx.getGenericSIBReferences();
		for (final EObject referencedObject : referencedObjects) {
			if (!isTransformable(referencedObject)) {
				generateContent(referencedObject);
			}
		}
	}
	
	// Generate process class for all backend processes.
	private void generateBackendProcess() {
		final BackendProcessGenerator backendProcessGenerator = new BackendProcessGenerator(backendProcessGeneratorHelper);
		backendProcessGenerator.setProject(ProjectCreator.getProject(model.eResource()));
		backendProcessGenerator.setUserType(model.getSystemUsers().stream().findFirst().map(SystemUser::getSystemUser));
		
		System.out.println("Generating ContextTransformer");
		backendProcessGenerator.generateContextTransformer(outlet, monitor);
		
		for (final Process p :
				genctx.getUsedProcesses().stream()
				.filter(p -> !p.getProcessType().equals(ProcessType.NATIVE_FRONTEND_SIB_LIBRARY))
				.collect(Collectors.toList())) {
			System.out.println("Generating Process: " + (p).getModelName());
			backendProcessGenerator.generate(p, outlet, monitor);
		}
	}
	
	// Copy backend processarchetpye
	private void copyStaticResources() {
		StaticResourcesHandler handler = new StaticResourcesHandler(outlet);
		handler.copyStaticJavaResources();
		handler.copyStaticDartResources();
		handler.copyStaticDockerResources();
	}
	
	private void copyFolder(String source,String target) {
		File sourceFolder = new File(outlet.append(source).toOSString());
		File targetFolder = new File(outlet.append(target).toOSString());
		System.out.println("[INFO] Target folder "  + targetFolder + " (exists: " + targetFolder.exists() + ")");
		if (sourceFolder.exists()) {
			 try {
				 FileUtils.copyDirectory(sourceFolder, targetFolder);				 
			 } catch (IOException ex) {
				 System.err.println(ex.getMessage());
			 }
		} else {
			System.out.println("[INFO] Skip copying static resources (source folder "+sourceFolder.toString()+" does not exist)");
		}
	}
	
	private void clearFolder(String target) {
		File targetFolder = new File(outlet.append(target).toOSString());
		System.out.println("[INFO] Target folder "  + targetFolder + " (exists: " + targetFolder.exists() + ")");
		if (targetFolder.exists()) {
			 try {
				 FileUtils.cleanDirectory(targetFolder);			 
			 } catch (IOException ex) {
				 System.err.println(ex.getMessage());
			 }
		} else {
			System.out.println("[INFO] Skip copying static resources (source folder "+targetFolder.toString()+" does not exist)");
		}
	}
	
	// Copy backend additional resources
	private void copyAdditionalResources() {
		copyFolder("../../dependency", "app-addon");
		copyFolder("../../asset", "app-presentation/src/main/webapp/asset");
		copyFolder("../../initFiles", "app-presentation/src/main/resources");
	}

	// Generate startup process
	private void generateStartupProcess() {
		final IGenerator<Process> startupProcessGenerator = new StartupProcessGenerator(model);
		final Optional<Process> startupProcess = 
				model.getEdges(StartupProcessPointer.class).stream()
					.findFirst()
					.map(StartupProcessPointer::getTargetElement)
					.map(ProcessComponent::getModel);
		
		if (startupProcess.isPresent()) {
			startupProcessGenerator.generate(startupProcess.get(), outlet, monitor);
		}
	}

	// Generate TODOList retriever.
	private void generateTODOList() {
		final Optional<SystemUser> systemUser = model.getSystemUsers().stream()
				.findFirst();
		if (systemUser.isPresent()) {
			final UserType userType = systemUser.get().getSystemUser();

			// Generate TODOList.
			final List<GuardContainer> guardContainers = genctx.getUsedProcesses()
					.stream()
					.filter(p -> p.getProcessType().equals(
							ProcessType.LONG_RUNNING))
					.map(Process::getGuardContainers).flatMap(List::stream)
					.collect(Collectors.toList());
			final TODOListGenerator todoGen = new TODOListGenerator();
			todoGen.generate(model, userType, guardContainers, outlet, monitor);
		}
	}
	
	private void generateAuthenticator() {
		final AuthenticationGenerator ag = new AuthenticationGenerator();
		ag.generate(model, outlet.append("app-business/target/generated-sources"));
	}
	
	// Generate Search Models
//	private void generateSearchModels() {
		// int workedSearchModels = 0;
		// monitor.beginTask("Generating Search Models", 100);
		// SearchGenerator searchGen = new
		// SearchGenerator(ProjectCreator.getProject(model.eResource()),
		// monitor);
		// for (SearchComponent s : model.getSearchComponents()) {
		// workedSearchModels++;
		// searchGen.generateForModel((Search)s.getModel(), outlet, monitor);
		// monitor.worked((workedSearchModels/model.getSearchComponents().size())*100);
		// }
		// searchGen.finishFiles();
//	}

	private void generateGUI() {
		
//		Map<Process, List<GuardContainer>> toGuardContainers = new HashMap<Process, List<GuardContainer>>();
//		//Find All Interactions and create an Entry
//		processes
//		.stream()
//		.filter(p -> 
//				p.getProcessType().equals(
//						ProcessType.INTERACTION)).forEach(n->toGuardContainers.put(n, new LinkedList<GuardContainer>()));
//		//Find all References to this Interactions which are Guarded in any other Interactions or LongRunnings
//		toGuardContainers.entrySet().forEach(n->{
//			n.getValue().addAll(
//			processes
//			.stream()
//			.filter(e->(e.getProcessType() == ProcessType.INTERACTION)||(e.getProcessType() == ProcessType.LONG_RUNNING))
//			.flatMap(e->e.getGuardContainers().stream().filter(f->f.getGuardedProcessSIBs().get(0).getProMod().equals(n.getKey()))).collect(Collectors.toList())
//			);
//		});
		
		
		// see #16562
//				processes
//				.stream()
//				.filter(p -> p.getProcessType().equals(
//						ProcessType.LONG_RUNNING) || 
//						p.getProcessType().equals(
//								ProcessType.INTERACTION))
//				.map(Process::getGuardContainers).flatMap(List::stream)
//				.collect(Collectors.groupingBy(
//						gc -> gc.getGuardedProcessSIBs().stream().findFirst().map(GuardedProcessSIB::getProMod).get()));
		
		final IGenerator<DAD> dartGenerator = new GUIDartGenerator(genctx, backendProcessGeneratorHelper);
		dartGenerator.generate(model, outlet, monitor);
		// //Philip does the real stuff here
	}
	
	private void generateOperations() {
		new OperationsGenerator(model).generate();
	}
	

	private void generateDefaultRestTOs() {
		new TOGenerator().generate(dataModels, outlet);
	}
	
	private void generateFileTokenCache() {
		new FileControllerGenerator().generate(outlet);
	}

	private void generateMigrators() {
		new EnumMigrator().generate(dataModels, outlet);
	}
	
	private void enableExtensionCacheRefresh() {
		GUIExtensionProvider.enableRefresh();
	}

	private void copyRessourceToDartLibFolder(Process p, IPath outlet) {
		final IPath targetDir = outlet.append("app-presentation/target/generated-sources/app/lib/nativefrontend/");
		final IResource iRes = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(p.eResource().getURI().toPlatformString(true));
		final IPath projectPath = iRes.getProject().getLocation();

		p.getNativeFrontendSIBReferences().stream()
				.map(sib -> sib.getReferencedSib().getRootElement().getNativeFrontendSIBLibrarys().stream())
				.flatMap(Function.identity()).collect(Collectors.toSet()).forEach(x -> {
					File referencedFile = new File(x.getClassName());
					if (!referencedFile.isAbsolute()) {
						referencedFile = new File(projectPath.append(x.getClassName()).toString());
					}
					try {
						final String path = referencedFile.getAbsolutePath();
						if (referencedFile.isFile() && referencedFile.canRead()) {
							FileUtils.copyFileToDirectory(referencedFile, targetDir.toFile());
						} else {
							Display.getDefault().syncExec(new Runnable() {
								@Override
								public void run() {
									MessageDialog.openError(null, "Invalid file reference in model!",
											"The Native-Frontend SIB Library " + x.getRootElement().getModelName()
													+ " references non readable/existing file:\n " + path);
								}
							});
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
	}

	private void toConsole(Set<Process> processes2) {
		genctx.getUsedProcesses().stream().forEach(p -> System.out.println(String.format("%s (%s)", p.getModelName(), p.getProcessType())));
	}
	
	private void collectChecks(Map<String,Runnable> checks) {
		checkresults = new ArrayList<>();
		
		DADExecution dadFE = new DADExecution();
		DataExecution dataFE = new DataExecution();
		ProcessExecution processFE = new ProcessExecution();
		GUIExecution guiFE = new GUIExecution();
		
		checks.put("DAD model " + model.getId(), () -> 
			checkresults.add(
					dadFE.executeCheckPhase(
							dadFE.initApiAdapterFromResource(
						model.eResource(), getFileForModel(model)))));
		
		model.getDataComponents().forEach(comp -> 
			checks.put("Data model " + comp.getId(), () ->
				checkresults.add(
						dataFE.executeCheckPhase(
								dataFE.initApiAdapterFromResource(
							comp.getModel().eResource(), getFileForModel(comp.getModel()))))));
		
		genctx.getUsedProcesses().forEach(process ->
			checks.put("Process model " + process.getId(), () ->
				checkresults.add(
						processFE.executeCheckPhase(
								processFE.initApiAdapterFromResource(
							process.eResource(), getFileForModel(process))))));
		
		genctx.getUsedGUIs().forEach(gui ->
			checks.put("GUI model " + gui.getId(), () ->
				checkresults.add(
						guiFE.executeCheckPhase(
								guiFE.initApiAdapterFromResource(
							gui.eResource(), getFileForModel(gui))))));
	}

	private File getFileForModel(EObject model) {
		// get file for model (eObject)
		URI resolvedFile = CommonPlugin.resolve(EcoreUtil.getURI(model));
		IFile iFile = ResourcesPlugin.getWorkspace().getRoot()
				.getFile(new Path(resolvedFile.toFileString()));

		File file = iFile.getFullPath().toFile();
		if (file == null)
			throw new RuntimeException("Could not find file for " + model);
		if (!file.exists())
			throw new RuntimeException("File does not exist for " + model);

		return file;
	}
	
	public boolean isHeadless() {
		return this.headless;
	}
	public void setHeadless(boolean headless) {
		this.headless = headless;
	}

}
