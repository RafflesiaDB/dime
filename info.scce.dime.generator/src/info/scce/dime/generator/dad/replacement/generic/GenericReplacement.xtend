package info.scce.dime.generator.dad.replacement.generic

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.process.process.GenericSIB

import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.*

class GenericReplacement extends CincoRuntimeBaseClass {
	
	val registry = ReferenceRegistry.instance
	val replacements = <GenericSIBReplacement> newArrayList 
	
	def execute() {
		val genericSIBs = registry.lookup(GenericSIB).toSet
		for (genericSIB : genericSIBs) {
			if (genericSIB.referencedObject.isTransformable) {
				if (genericSIB.referencedObject.isGUI) {
					new GenericGUISIBReplacement => [
						replacements.add(it)
						replace(genericSIB)
					]
				} else {
					new GenericProcessSIBReplacement => [
						replacements.add(it)
						replace(genericSIB)
					]
				}
			}
		}
	}
	
	def cleanup() {
		replacements.forEach[it.cleanup]
	}
}
