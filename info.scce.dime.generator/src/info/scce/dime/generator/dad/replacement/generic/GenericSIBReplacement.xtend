package info.scce.dime.generator.dad.replacement.generic

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.api.modelgen.GUIModelGenerationLanguage
import info.scce.dime.api.modelgen.ProcessModelGenerationLanguage
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.GenericSIB
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.SIB
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.URI

abstract class GenericSIBReplacement extends CincoRuntimeBaseClass {
	
	protected extension ProcessModelGenerationLanguage = new ProcessModelGenerationLanguage
	protected extension GUIModelGenerationLanguage = new GUIModelGenerationLanguage
	
	extension ProcessExtension = new ProcessExtension
	
	protected val generatedModelFiles = <URI> newArrayList
		
	def void replace(GenericSIB sib)
	
	def replaceWith(GenericSIB genericSib, SIB newSib) {
		debug(" > Replace GenericSIB " + genericSib.displayName
				+ " in " + genericSib.rootElement.modelName
				+ " with " + newSib.eClass.name + newSib.displayName
		)
		
		val defaultContentSIBs
			= genericSib.rootElement.find(SIB)
				.filter[hasDefaultContent(genericSib)]
				.toSet
		newSib.overtakeIncomingEdgesOf(genericSib)
		
		for (port : genericSib.inputPorts) {
			debug("   > Input port " + port.name)
			val newPort = newSib.findThe(InputPort)[name == port.name]
			debug("     > Found input port " + newPort?.name)
			newPort.overtakeIncomingEdgesOf(port)
		}
		
		for (branch : genericSib.branchSuccessors) {
			debug("   > Branch " + branch.name)
			val newBranch = newSib.branchSuccessors.filter[name == branch.name].head
			debug("     > Found branch " + newBranch?.name)
			newBranch.moveTo(newBranch.container, branch.x, branch.y)
			newBranch.overtakeOutgoingEdgesOf(branch)
			for (port : branch.outputPorts) {
			debug("     > Output port " + port.name)
				val newPort = newBranch.findThe(OutputPort)[name == port.name]
				debug("       > Found output port " + newPort?.name)
				newPort.overtakeOutgoingEdgesOf(port)
			}
		}
		
		defaultContentSIBs.forEach[defaultContent = newSib]
		
		debug("   > Delete GenericSIB")
		genericSib.delete
	}
	
	def getDisplayName(SIB it) {
		if (label.nullOrEmpty) id else label
	}
	
	def cleanup() {
		generatedModelFiles.forEach[
			log("Deleting " + it)
			getFile?.delete( /*force*/ true, new NullProgressMonitor)
		]
	}
	
	def debug(String msg) {
//		println("[DEBUG] " + msg)
	}
	
	def log(String msg) {
		println("[INFO] " + msg)
	}
}
