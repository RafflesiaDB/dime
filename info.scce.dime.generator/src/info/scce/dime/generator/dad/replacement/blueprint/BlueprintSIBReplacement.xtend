package info.scce.dime.generator.dad.replacement.blueprint

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.api.modelgen.GUIModelGenerationLanguage
import info.scce.dime.api.modelgen.ProcessModelGenerationLanguage
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.HeadlineSize
import info.scce.dime.gui.gui.Row
import info.scce.dime.gui.gui.Template
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.BlueprintSIB
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.SIB
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.URI

abstract class BlueprintSIBReplacement<T extends BlueprintSIB> extends CincoRuntimeBaseClass {
	
	protected extension ProcessModelGenerationLanguage = new ProcessModelGenerationLanguage
	protected extension GUIModelGenerationLanguage = new GUIModelGenerationLanguage
	
	extension ProcessExtension = new ProcessExtension
	
	protected val generatedModelFiles = <URI> newArrayList
	
	def void replace(T sib)
	
	def cleanup() {
		generatedModelFiles.forEach[
			log("Deleting " + it)
			getFile?.delete( /*force*/ true, new NullProgressMonitor)
		]
	}
	
	def getDisplayName(SIB it) {
		if (label.nullOrEmpty) id else label
	}
	
	def addDummyGUIContent(GUI gui, T blueSib) {
		val template = gui.findThe(Template) ?: gui.newTemplate(0,0)
		debug("  > Template: " + template)
		val row = template.findThe(Row) ?: template.newRow(0,0)
		debug("  > Row: " + row)
		if (blueSib.defaultBranch === null) {
			row.generalStyle?.setRawContent("display:none;")
		}
		val col = row.findThe(Col) ?: row.newCol(0,0)
		debug("  > Col: " + col)
		col.newHeadline(0, col.height-1) => [
			content?.head?.setRawContent("Not Implemented")
			size = HeadlineSize.SMALL
		]
		col.newText(0, col.height-1) => [
			content?.head?.setRawContent(
				'''GUIBlueprintSIB «blueSib.displayName» in model «blueSib.rootElement.modelName»'''
			)
		]
	}
	
	def replaceWith(BlueprintSIB blueSib, SIB newSib) {
		debug(" > Replace BlueprintSIB " + blueSib.displayName
				+ " in " + blueSib.rootElement.modelName
				+ " with " + newSib.eClass.name + newSib.displayName
		)
		
		val defaultContentSIBs
			= blueSib.rootElement.find(SIB)
				.filter[hasDefaultContent(blueSib)]
				.toSet
		newSib.overtakeIncomingEdgesOf(blueSib)
		
		for (port : blueSib.inputPorts) {
			debug("   > Input port " + port.name)
			val newPort = newSib.findThe(InputPort)[name == port.name]
			debug("     > Found input port " + newPort?.name)
			newPort.overtakeIncomingEdgesOf(port)
		}
		
		for (branch : blueSib.branchBlueprintSuccessors) {
			debug("   > Branch " + branch.name)
			val newBranch = newSib.branchSuccessors.filter[name == branch.name].head
			debug("     > Found branch " + newBranch?.name)
			newBranch.moveTo(newBranch.container, branch.x, branch.y)
			newBranch.overtakeOutgoingEdgesOf(branch)
			for (port : branch.outputPorts) {
			debug("     > Output port " + port.name)
				val newPort = newBranch.findThe(OutputPort)[name == port.name]
				debug("       > Found output port " + newPort?.name)
				newPort.overtakeOutgoingEdgesOf(port)
			}
		}
		
		defaultContentSIBs.forEach[defaultContent = newSib]
		
		debug("   > Delete BlueprintSIB")
		blueSib.delete
	}
	
	def getOutlet(T blueSib) {
		blueSib.project.fullPath.append(".gen")
	}
	
	def debug(String msg) {
//		println("[DEBUG] " + msg)
	}
	
	def log(String msg) {
		println("[INFO] " + msg)
	}
}
