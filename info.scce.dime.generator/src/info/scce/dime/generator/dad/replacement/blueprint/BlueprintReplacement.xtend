package info.scce.dime.generator.dad.replacement.blueprint

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import info.scce.dime.dad.dad.BlueprintGUIReplacement
import info.scce.dime.dad.dad.BlueprintingConfig
import info.scce.dime.process.process.BlueprintSIB
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.ProcessBlueprintSIB

class BlueprintReplacement extends CincoRuntimeBaseClass {
	
	val registry = ReferenceRegistry.instance
	
	val BlueprintingConfig config
	val replacements = <BlueprintSIBReplacement<?>> newArrayList 
	
	new(BlueprintingConfig config) {
		this.config = config
	}
	
	def execute() {
		val blueSIBs = registry.lookup(BlueprintSIB).toSet
		for (blueSIB : blueSIBs) switch blueSIB {
			
			GUIBlueprintSIB: new GUIBlueprintSIBReplacement => [
				replacements.add(it)
				simpleDialog = switch config?.getGuiReplacement {
					BlueprintGUIReplacement case SIMPLE_DIALOG: true
					default: false
				}
				replace(blueSIB)
			]
			
			ProcessBlueprintSIB: new ProcessBlueprintSIBReplacement => [
				replacements.add(it)
				replace(blueSIB)
			]
		}
	}
	
	def cleanup() {
		replacements.forEach[it.cleanup]
	}
	
}
