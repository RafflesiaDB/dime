package info.scce.dime.generator.dad.replacement.generic

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.process.process.GenericSIB
import info.scce.dime.process.process.Process

import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.*

class GenericProcessSIBReplacement extends GenericSIBReplacement {
	
	val registry = ReferenceRegistry.instance
	
	override replace(GenericSIB sib) {
		val Process process = sib.referencedObject.transformedProcess
		generatedModelFiles.add(process.eResource.URI)
		registry.register(process)
		process.save
		sib.replaceWith(process)
	}
	
	def replaceWith(GenericSIB sib, Process process) {
		sib.replaceWith(
			sib.rootElement.newProcessSIB(process, sib.x, sib.y, sib.width, sib.height) => [
				label = sib.label
				name = sib.name
			]
		)
	}
}
