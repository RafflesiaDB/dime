package info.scce.dime.generator.dad.replacement.generic

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.GenericSIB

import static extension info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider.*

class GenericGUISIBReplacement extends GenericSIBReplacement {
	
	val registry = ReferenceRegistry.instance
	
	override replace(GenericSIB sib) {
		val GUI gui = sib.referencedObject.transformedGUI
		generatedModelFiles.add(gui.eResource.URI)
		registry.register(gui)
		gui.save
		sib.replaceWith(gui)
	}
	
	def void replaceWith(GenericSIB sib, GUI gui) {
		
		sib.replaceWith(
			sib.rootElement.newGUISIB(gui, sib.x, sib.y, sib.width, sib.height) => [
				label = sib.label
				name = sib.name
				majorPage = sib.guiOptions.majorPage
				majorBranch = sib.guiOptions.majorBranch
				cacheGUI = sib.guiOptions.cacheGUI
				defaultContent = sib.guiOptions.defaultContent
			]
		)
	}
	
}
