package info.scce.dime.generator.dad

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import graphmodel.GraphModel
import graphmodel.IdentifiableElement
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.StartupProcessPointer
import info.scce.dime.data.data.Data
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessType
import java.util.Collection
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import org.eclipse.emf.ecore.EObject

import static java.util.Collections.disjoint

import static extension info.scce.dime.generator.gui.data.GUICompoundViewCreator.createCompoundView

class GenerationContext {
	
	/*
	 * The singleton pattern works as long as there is only one Generation at a time.
	 * To enable the generation of multiple DADs in parallel this GenerationContext
	 * needs to be passed around the various generator fragments.
	 */
	public static GenerationContext instance
	
	public extension GUIExtension guiExtension = new GUIExtension(/*cacheValues*/true)
	
	// lambda expression to resolve prime references
	val followPrimeRefs = [IdentifiableElement elm | elm.primeReferencedContainer]
	
	// lambda expression to resolve prime references
	val digIntoProcessAndGUI = [IdentifiableElement it | 
		switch it { 
			info.scce.dime.gui.gui.GUISIB: gui
			info.scce.dime.process.process.ProcessSIB: proMod
			info.scce.dime.process.process.GUISIB: gui
			GuardContainer: findThe(GuardedProcessSIB)?.proMod
		} as GraphModel
	]
	
	public val DAD dad
	
	/*
	 * Caches
	 */
	Set<GraphModel> _usedModels
	Set<Process> _usedProcesses
	Set<Process> _basicProcesses
	Set<Process> _guiProcesses
	Set<Process> _longrunningProcesses
	Set<Process> _rootProcesses
	Set<Process> _visibleProcesses
	Set<EObject> _genericSIBReferences
	Set<GUI> _usedGUIs
	Set<Data> _usedDatas
	Map<GUI,Set<GUI>> _subGUIsMap
	Map<GUI,List<GUI>> _superGUIsMap
	Map<GUI,GUICompoundView> _compoundViewsMap
	
	
	package new(DAD dad) {
		this.dad = dad
		GenerationContext.instance = this
	}
	
	def getUsedModels() {
		_usedModels ?: (_usedModels = collectModels)
	}
	
	def collectModels() {
		val allModels = lookup(GraphModel).toSet
		=> [ println("[INFO] All Models collected: " + it.size) ]
		
		val usedModels = dad.findDeeply(GraphModel, followPrimeRefs).toSet
		=> [ println("[INFO] Referenced models collected deeply: " + it.size) ]
		
		val unusedModels = new HashSet(allModels) => [ removeAll(usedModels) ]
		
		usedModels.collectDependentModels(unusedModels)
		println("[INFO] All relevant Models collected: " + usedModels.size)
		println("[INFO] Unused Models: " + unusedModels.size)
		
		return usedModels
	}
	
	def getDependentModels(GraphModel... models) {
		new HashSet(models).addDependentModels => [ removeAll(models) ]
	}
	
	def addDependentModels(GraphModel... models) {
		val otherModels = new HashSet(usedModels)
		models.collectDependentModels(otherModels)
		return models
	}
	
	def getUsedProcesses() {
		_usedProcesses ?: (
			_usedProcesses = usedModels.filter(Process).toSet
			=> [ println("[INFO] All used Processes collected: " + it.size) ]
		)
	}
	
	def getGenericSIBReferences() {
		_genericSIBReferences ?: (
			_genericSIBReferences = getUsedProcesses.flatMap[genericSIBs].map[referencedObject].toSet
			=> [ println("[INFO] All generic SIB References collected: " + it.size) ]
		)
	}
	
	def getBasicProcesses() {
		_basicProcesses ?: (
			_basicProcesses = usedProcesses.filter[processType == ProcessType.BASIC].toSet
		)
	}
	
	def getGUIProcesses() {
		_guiProcesses ?: (
			_guiProcesses = usedGUIs.flatMap[
				find(SecuritySIB, ProcessSIB).map[switch it {
					SecuritySIB: proMod as Process
					ProcessSIB: proMod as Process
				}]
			].toSet
		)
	}
	
	def getLongrunningProcesses() {
		_longrunningProcesses ?: (
			_longrunningProcesses = usedProcesses.filter[processType == ProcessType.LONG_RUNNING].toSet
		)
	}
	
	def getRootProcesses() {
		_rootProcesses ?: (
			_rootProcesses = usedProcesses.filter[isRootProcess].toSet
		)
	}
	
	def getVisibleProcesses() {
		_visibleProcesses ?: (
			_visibleProcesses = usedProcesses.filter[isVisible].toSet
		)
	}
	
	/**
	 * A process is considered "visible" if it either is "root" or
	 * contains at least one GUI in any depth.
	 */
	def isVisible(Process process) {
		if (_visibleProcesses !== null) {
			_visibleProcesses.contains(process)
		} else {
			process.isRootProcess
			|| !process.findDeeply(GUI, digIntoProcessAndGUI).isEmpty
		}
	}
	
	/**
	 * A process is considered "root" if it is embedded in the DAD
	 * or in at least one GUI directly.
	 */
	def isRootProcess(Process process) {
		if (_rootProcesses !== null) {
			_rootProcesses.contains(process)
		} else {
			usedGUIs.flatMap[find(ProcessSIB)].exists[it?.proMod == process]
			|| dad.processComponents.filter[getIncoming(StartupProcessPointer).isEmpty].exists[it?.model == process]
		}
	}
	
	def getUsedGUIs() {
		_usedGUIs ?: (
			_usedGUIs = usedModels.filter(GUI).toSet
			=> [ println("[INFO] All used GUIs collected: " + it.size) ]
		)
	}
	
	def getUsedDatas() {
		_usedDatas ?: (
			_usedDatas = dad.dataComponents.map[model].toSet
			=> [ println("[INFO] All used Datas collected: " + it.size) ]
		)
	}
	
	/**
	 * Moves models from unusedModels to usedModels if they depend on any of those.
	 */
	private def void collectDependentModels(Collection<GraphModel> usedModels, Collection<GraphModel> unusedModels) {
		val extendingModels
			= unusedModels
				.filter[extensionHierarchy.containsAnyOf(usedModels)]
				.toSet
		
		if (extendingModels.isEmpty) return;
		
		usedModels.addAll(extendingModels)
		unusedModels.removeAll(extendingModels)
		
		val modelsUsedByExtendingModels
			= extendingModels
				.flatMap[findDeeply(GraphModel, followPrimeRefs)]
				.toSet
		
		if (modelsUsedByExtendingModels.isEmpty) return;
		
		unusedModels.removeAll(modelsUsedByExtendingModels)
		modelsUsedByExtendingModels.collectDependentModels(unusedModels)
		usedModels.addAll(modelsUsedByExtendingModels)
	}
	
	def <T extends IdentifiableElement> findDeeply(Class<T> clazz) {
		dad.findDeeply(clazz, followPrimeRefs)
	}
	
	def <T extends EObject> lookup(Class<T> clazz) {
		ReferenceRegistry.instance.lookup(clazz)
	}
	
	def containsAnyOf(Collection<?> c1, Collection<?> c2) {
		!disjoint(c1, c2)
	}
	
	def List<GraphModel> getExtensionHierarchy(GraphModel model) {
		( #[model] + (model.extendedModel?.extensionHierarchy ?: #[]) ).toList
	}
	
	def List<GUI> getExtensionHierarchy(GUI gui) {
		getExtensionHierarchy(gui as GraphModel).filter(GUI).toList
	}
	
	def List<Process> getExtensionHierarchy(Process process) {
		getExtensionHierarchy(process as GraphModel).filter(Process).toList
	}
	
	def getExtendedModel(GraphModel it) {
		switch it {
			GUI: extendedGUI
		}
	}
	
	/**
	 * Retrieves all sub-GUIs of the specified GUI.
	 */
	def getUsedSubGUIs(GUI gui) {
		subGUIsMap.get(gui)
	}
	
	private def getSubGUIsMap() {
		_subGUIsMap ?: (_subGUIsMap = newHashMap => [
			lookup(GUI).forEach[ gui |
				put(gui, gui.calcExtendingGUIs)
			]
		])
	}
	
	private def calcExtendingGUIs(GUI gui) {
		superGUIsMap.values
			.filter[contains(gui)]
			.flatMap[subList(0, indexOf(gui))]
			.toSet
	}
	
	private def getSuperGUIsMap() {
		_superGUIsMap ?: (_superGUIsMap = newHashMap => [
			usedGUIs.forEach[ gui |
				put(gui, gui.extensionHierarchy)
			]
		])
	}
	
	def getCompoundViews() {
		compoundViewsMap.values.toSet
	}
	
	private def Map<GUI,GUICompoundView> getCompoundViewsMap() {
		_compoundViewsMap ?: {
			_compoundViewsMap = newHashMap // we need to init the map here as the CVs register to it themselves
			usedGUIs.forEach[_compoundViewsMap.put(it, createCompoundView(it))]
			println("[INFO] GUI Compound Views created for all used GUIs")
			_compoundViewsMap
		}
	}
	
	def getCompoundView(GUI gui) {
		compoundViewsMap.get(gui) ?: (createCompoundView(gui) => [_compoundViewsMap.put(gui, it)])
	}
	
	def addGuiCV(GUICompoundView cv) {
		if (_compoundViewsMap !== null)
			_compoundViewsMap.put(cv.gui, cv)
	}
}