package info.scce.dime.generator.dad

import de.jabc.cinco.meta.plugin.dsl.FileDescription
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.Path

class CopyFileDescription extends FileDescription {
	
	IFile sourceFile
	
	new (String name, IFile sourceFile) {
		super(name)
		this.sourceFile = sourceFile
	}
	
	override create() {
		val targetFile = parent.IResource.getFile(new Path(name))
		if (targetFile.exists) {
			if (overwrite) {
				targetFile.delete(true, null)
				sourceFile.copy(targetFile.fullPath, true, null)
				return targetFile
			}	
		}
		else {
			sourceFile.copy(targetFile.fullPath, true, null)
			return targetFile
		}
		return null
	}
	
}