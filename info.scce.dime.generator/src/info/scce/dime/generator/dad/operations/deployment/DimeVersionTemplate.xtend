package info.scce.dime.generator.dad.operations.deployment

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate

class DimeVersionTemplate extends OperationsFileTemplate {
	
	Servers servers
	
	new (Servers servers) {
		this.servers = servers
	}
	
	override template() '''
	«IF servers.dimeVersion.nullOrEmpty»latest«ELSE»«servers.dimeVersion»«ENDIF»
	#### everything but the first line in this file is ignored ####
	
	First line defines the version of DIME this app state depends
	on. Valid values are "latest" or a path snippet relative to the dime folder from
	ls5download (https://ls5download.cs.tu-dortmund.de/dime), e.g.
	"daily/2019-09-06/DIME-1.201909060855"
	'''
}