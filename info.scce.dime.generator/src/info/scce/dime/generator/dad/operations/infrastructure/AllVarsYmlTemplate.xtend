package info.scce.dime.generator.dad.operations.infrastructure

import info.scce.dime.generator.dad.operations.OperationsFileTemplate
import info.scce.dime.dad.dad.Servers

class AllVarsYmlTemplate extends OperationsFileTemplate {

	new (Servers servers) {
		this.servers = servers
	}
	
	override template() '''
	ansible_python_interpreter: '/usr/bin/python3'
	ansible_user: «servers.ansibleUser»
	deploy_user: «servers.deployUser»
	certbot_email: «servers.certbotEmail»
	maven_edu_password: «servers.mavenEduPassword»
	maven_edu_username: «servers.mavenEduUsername»
	dywa_http_auth_password: «servers.dywaHttpAuthPassword»
	dywa_http_auth_username: «servers.dywaHttpAuthUsername»
	dywa_database_user: sa
	dywa_database_password: sa
	client_max_body_size: 104857600
	«FOR property : servers.wildflyProperty BEFORE "wildfly_system_properties:\n"»
		«"  "»- name: "«property.name»"
		«"  "»  value: "«property.value»"
	«ENDFOR»
	'''
	
	/*
	
	  - name: "info.scce.dime.app.equinocs.server.url"
	    value: "https://{{ domain }}/"
	  - name: "info.scce.dime.app.equinocs.server.tier"
	    value: "{{ deployment_tier }}"
	  - name: "info.scce.dime.app.encrypt"
	    value: "{{ 'true' if deployment_tier == 'production' else 'false' }}"
	  - name: "info.scce.dime.app.equinocs.mail.server"
	    value: "{{ mail_server }}"
	  - name: "info.scce.dime.app.equinocs.mail.port"
	    value: "{{ mail_port }}"
	    * 
	    * 
	    */
	
}