package info.scce.dime.generator.dad.operations.infrastructure

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate
import info.scce.dime.dad.dad.Server
import info.scce.dime.dad.dad.ProductionServer
import info.scce.dime.dad.dad.NonProductionServer

class InventoryTemplate extends OperationsFileTemplate {
	
	Server server
	
	new (Servers servers, Server server) {
		this.servers = servers
		this.server = server
	}
	
	override template() {
		switch server {
			ProductionServer : server.productionTemplate
			NonProductionServer : server.nonProductionTemplate
		}			
	}
	
	def productionTemplate(Server server) '''
		---
		all:
		  hosts:
		    «server.hostName»:
		      deployment_tier: «server.deploymentTier»
	'''
	
	def nonProductionTemplate(Server server) '''
		---
		all:
		  children:
		    non_production:
		      hosts:
		        «server.hostName»:
		          deployment_tier: «server.deploymentTier»
	'''
}