package info.scce.dime.generator.dad.operations.infrastructure

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate
import info.scce.dime.dad.dad.Server
import info.scce.dime.dad.dad.ProductionServer
import info.scce.dime.dad.dad.NonProductionServer

class HostVarsTemplate extends OperationsFileTemplate {
	
	Server server
	
	new (Servers servers, Server server) {
		this.servers = servers
		this.server = server
	}
	
	override template() {
		switch server {
			ProductionServer : server.productionTemplate
			NonProductionServer : server.nonProductionTemplate
		}			
	}
	
	def productionTemplate(ProductionServer server) '''
		domain: «server.domainName»
		mail_server: «server.mailServer»
		mail_port: «server.mailPort»
	'''
	
	def nonProductionTemplate(NonProductionServer server) '''
		domain: «server.domainName»
	'''
}