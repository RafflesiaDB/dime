package info.scce.dime.generator.dad.operations

import de.jabc.cinco.meta.plugin.template.FileTemplate
import info.scce.dime.dad.dad.Servers

abstract class OperationsFileTemplate extends FileTemplate {
	
	var String targetFileName
	protected var Servers servers
	
	
	override final getTargetFileName() {
		targetFileName
	}
	
	def setTargetFileName(String targetFileName) {
		this.targetFileName = targetFileName
	}
	
	
	def getProjectFolder() {
		servers.project.location.lastSegment
	}
	
}