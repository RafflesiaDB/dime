package info.scce.dime.generator.dad.operations.infrastructure

import info.scce.dime.generator.dad.operations.OperationsFileTemplate
import info.scce.dime.dad.dad.Servers

class NonProductionVarsYmlTemplate extends OperationsFileTemplate {

	new (Servers servers) {
		this.servers = servers
	}
	
	override template() '''
	mailcatcher_http_auth_password: '{{ vault_mailcatcher_http_auth_password }}'
	mailcatcher_http_auth_username: '{{ vault_mailcatcher_http_auth_username }}'
	mail_server: mailcatcher
	mail_port: 1025
	'''
}