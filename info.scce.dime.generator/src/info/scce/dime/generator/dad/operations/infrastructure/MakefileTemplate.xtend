package info.scce.dime.generator.dad.operations.infrastructure

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate

class MakefileTemplate extends OperationsFileTemplate {
	
	new (Servers servers) {
		this.servers = servers
	}
	
	override template() '''
		ANSIBLE=ansible-playbook --ask-become-pass --diff --vault-id @prompt --inventory inventories/
		«FOR it : servers.servers»

			«serverName»-install:
				$(ANSIBLE)«serverName».yml install.yml
			
			«serverName»-check-install:
				$(ANSIBLE)«serverName».yml --check install.yml
		«ENDFOR»
	'''
}