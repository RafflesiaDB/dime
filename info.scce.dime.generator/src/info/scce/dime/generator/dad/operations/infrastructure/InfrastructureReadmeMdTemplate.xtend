package info.scce.dime.generator.dad.operations.infrastructure

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate

class InfrastructureReadmeMdTemplate extends OperationsFileTemplate {

	new (Servers servers) {
		this.servers = servers
	}
	
	override template() '''
		# Infrastructure

		## Server

		| Name              | Environment       | Server        | Description                                               | Ansible Role Version  | 
		| -                 | -                 | -             | -                                                         | -                     |
		«FOR server : servers.servers»
		| «server.serverName»  | «server.deploymentTier» | «server.hostName» | description incoming | «server.ansibleRoleVersion» |
		«ENDFOR»

		[Source](https://en.wikipedia.org/wiki/Deployment_environment)
	'''

}