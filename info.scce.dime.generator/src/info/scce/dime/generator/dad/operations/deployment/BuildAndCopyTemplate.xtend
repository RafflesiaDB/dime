package info.scce.dime.generator.dad.operations.deployment

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate

class BuildAndCopyTemplate extends OperationsFileTemplate {
	
	new (Servers servers) {
		this.servers = servers
	}
	
	
	override template() '''
		#!/bin/bash
		
		DEPLOY_USER="«servers.deployUser»"
		APP_DIRECTORY="/home/${DEPLOY_USER}/app"
		RSYNC_CMD="/usr/bin/rsync --recursive --update --progress --delete"
		MAVEN_REPOSITORY="${APP_DIRECTORY}/maven/repository"
		MAVEN_REPOSITORY_DIME_DIRECTORY="${MAVEN_REPOSITORY}/info/scce/dime/"
		set -e
		
		function copy() {
			${RSYNC_CMD} ../../«projectFolder»/target/{webapp,dywa-app} --exclude "dywa-app/app-dywa-bridge/*/target" --exclude "dywa-app/app-dywa-bridge/*wildfly.path*" ${DEPLOY_USER}@$1:${APP_DIRECTORY}/src/
			${RSYNC_CMD} ../../maintenance-page ${DEPLOY_USER}@$1:${APP_DIRECTORY}/src/
			${RSYNC_CMD} ~/.m2/repository/info/scce/dime/{app-parent,app-business,app-addon,app-addon-parent} ${DEPLOY_USER}@$1:${MAVEN_REPOSITORY_DIME_DIRECTORY}
			«IF !(servers.nativeLibraryGroupId.nullOrEmpty || servers.nativeLibraryArtifactId.nullOrEmpty)»
				MAVEN_REPOSITORY_NATIVE_DIRECTORY="${MAVEN_REPOSITORY}/«servers.nativeLibraryGroupId.dotToSlash»/"
				/usr/bin/ssh ${DEPLOY_USER}@$1 /bin/mkdir -p ${MAVEN_REPOSITORY_NATIVE_DIRECTORY}
				${RSYNC_CMD} ~/.m2/repository/«servers.nativeLibraryGroupId.dotToSlash»/«servers.nativeLibraryArtifactId» ${DEPLOY_USER}@$1:${MAVEN_REPOSITORY_NATIVE_DIRECTORY}
			«ENDIF»
		}
		
		function repairshiro() {
			sed -i "/logout.redirectUrl = .*/c\logout.redirectUrl = https://$1" ../../«projectFolder»/target/dywa-app/app-presentation/src/main/webapp/WEB-INF/shiro.ini
		}
		
		function build() {
			cd ../../«projectFolder»/target/dywa-app
			mvn install -U -Ddime.native=true
			cd -
		}
		
		case "$1" in
		«FOR server : servers.servers »
			"--«server.serverName»")
				repairshiro «server.domainName» 
				build
				copy «server.hostName»
				;;
		«ENDFOR»
		  *)
			echo "Please specify the server: «serversOptions»"
			exit 0
			;;
		esac
	'''
	
	def serversOptions() '''
		«FOR server : servers.servers BEFORE '[' SEPARATOR ', ' AFTER ']' »\"--«server.serverName»\"«ENDFOR»
	'''
	
	def dotToSlash(String s) {
		s.replaceAll("\\.", "/")	
	}

}