package info.scce.dime.generator.dad.operations.infrastructure

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate

class AnsibleReadmeMdTemplate extends OperationsFileTemplate {

	new (Servers servers) {
		this.servers = servers
	}
	
	override template() '''
	# Ansible
	
	## Preparation of the control machine
	
	### Change directory
	
	```sh
	cd operations/infrastructure/ansible
	```
	### Installation of Ansible
	Have a look at http://docs.ansible.com/ansible/latest/intro_installation.html#installing-the-control-machine, but we recommend to use pip.
	
	```sh
	pip install -r requirements.txt
	```
	
	## Installation of deploy target
	
	```sh
	make {testing-install,integration-install,staging-install,demo-install,production-install}
	```
	
	## How to edit a vault
	
	```sh
	ansible-vault edit group_vars/all/vault.yml
	```	
	'''

	def serverNamesOptions() '''
		«FOR server : servers.servers BEFORE '{' SEPARATOR '|' AFTER '}' »--«server.serverName»«ENDFOR»
	'''

	def serverHostnamesOptions() '''
		«FOR server : servers.servers BEFORE '{' SEPARATOR '|' AFTER '}' »«server.hostName»«ENDFOR»
	'''

}