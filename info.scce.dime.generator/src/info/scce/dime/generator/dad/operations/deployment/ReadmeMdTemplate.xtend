package info.scce.dime.generator.dad.operations.deployment

import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.operations.OperationsFileTemplate

class ReadmeMdTemplate extends OperationsFileTemplate {

	new (Servers servers) {
		this.servers = servers
	}
	
	override template() '''
	# Deployment
	
	## Context: Local Mashine
	
	```bash
	./generate.sh «serverNamesOptions»
	./build-and-copy.sh «serverNamesOptions»
	```
	## Context: Deploy Target
	
	```bash
	ssh «servers.deployUser»@«serverHostnamesOptions»
	cd ~/app
	./app maintenance on
	./app deploy         # EITHER this
	./app deploy --native  # OR this
	./app maintenance off
	```
	'''

	def serverNamesOptions() '''
		«FOR server : servers.servers BEFORE '{' SEPARATOR '|' AFTER '}' »--«server.serverName»«ENDFOR»
	'''

	def serverHostnamesOptions() '''
		«FOR server : servers.servers BEFORE '{' SEPARATOR '|' AFTER '}' »«server.hostName»«ENDFOR»
	'''

}