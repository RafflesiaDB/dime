package info.scce.dime.generator.dad.operations

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.plugin.dsl.FolderDescription
import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.Servers
import info.scce.dime.generator.dad.CopyFileDescription
import info.scce.dime.generator.dad.operations.deployment.BuildAndCopyTemplate
import info.scce.dime.generator.dad.operations.deployment.DimeVersionTemplate
import info.scce.dime.generator.dad.operations.deployment.GenerateShTemplate
import info.scce.dime.generator.dad.operations.deployment.ReadmeMdTemplate
import info.scce.dime.generator.dad.operations.infrastructure.AllVarsYmlTemplate
import info.scce.dime.generator.dad.operations.infrastructure.AnsibleReadmeMdTemplate
import info.scce.dime.generator.dad.operations.infrastructure.HostVarsTemplate
import info.scce.dime.generator.dad.operations.infrastructure.InfrastructureReadmeMdTemplate
import info.scce.dime.generator.dad.operations.infrastructure.InventoryTemplate
import info.scce.dime.generator.dad.operations.infrastructure.MakefileTemplate
import info.scce.dime.generator.dad.operations.infrastructure.NonProductionVarsYmlTemplate

class OperationsGenTemplate extends ProjectTemplate {
	
	DAD dad
	Servers servers
	
	new (DAD dad) {
		this.dad = dad
		this.servers = dad.serverss.head
	}
	
	private def file(FolderDescription fd, String filename, OperationsFileTemplate template, () => Boolean condition) {
		if (condition.apply) {
			template.targetFileName = filename
			fd.file(template)
		} 
	}
	
	private def file(FolderDescription fd, String filename, OperationsFileTemplate tmpl) {
		tmpl.targetFileName = filename
		fd.file(tmpl)		
	}
	
	private def copyFile(FolderDescription folderD, String targetFilename, String sourcePath) {
		val fileD = new CopyFileDescription(targetFilename, servers.project.getFile(servers.project.projectRelativePath.append(sourcePath)))
		folderD.add(fileD)
		folderD
	}

	private def copyFile(FolderDescription folderD, String targetFilename, String sourcePath, () => Boolean condition) {
		if (condition.apply) {
			copyFile(folderD, targetFilename, sourcePath)
		}
	}
	
	override projectDescription() {
		val project = ProjectCreator::getProject(dad.eResource())
		project(project.name) [
			deleteIfExistent = false
			folder ("operations-gen") [
				folder ("deployment") [
					file ("build-and-copy.sh", new BuildAndCopyTemplate(servers))
					file ("dime.version", new DimeVersionTemplate(servers))
					file ("generate.sh", new GenerateShTemplate(servers))
					file ("README.md", new ReadmeMdTemplate(servers))
				]
				folder ("infrastructure") [
					folder ("ansible") [
						folder ("group_vars") [
						 	folder ("all") [
						 		file ("vars.yml", new AllVarsYmlTemplate(servers))
								copyFile ("vault.yml", servers.allVault) [!servers.allVault.nullOrEmpty]
						 	]	
						 	folder ("non_production") [
						 		file ("vars.yml", new NonProductionVarsYmlTemplate(servers))
								copyFile ("vault.yml", servers.nonProductionVault) [!servers.nonProductionVault.nullOrEmpty]								
						 	]
						]
						folder ("host_vars") [
							forEachOf(servers.servers) [ server |
								folder (server.hostName) [
									file("vars.yml", new HostVarsTemplate(servers, server))	
									copyFile ("vault.yml", server.vault) [!server.vault.nullOrEmpty]
								]
							]
						]
						folder ("inventories") [
							forEachOf(servers.servers) [ server |
								println('''inventory for «server.serverName»''')
								file('''«server.serverName».yml''', new InventoryTemplate(servers, server))	
							]	
						]
						/* ansible.cfg
						 * install.yml
						 * requirements.txt
						 */ filesFromBundle("info.scce.dime.generator" -> "static-resources/dad/operations/infrastructure/ansible")	
						file ("Makefile", new MakefileTemplate(servers))
						file ("README.md", new AnsibleReadmeMdTemplate(servers))
					]
					file ("README.md", new InfrastructureReadmeMdTemplate(servers))

					
				]		
			]	
			
		]
		
		/* 
		
		Example internal DSL usage from primeviewer meta plug-in
		
		project [
			folder ("src") [
				pkg [
					file (ActivatorTmpl)
				]
				forEachOf(primeNodes) [n |
					pkg (subPackage(n.primeTypePackagePrefix)) [
						files = #[
							new ContentProviderTmpl(n),
							new LabelProviderTmpl(n),
							new ProviderHelperTmpl(n)
						]
					]
				]
			]
			file (PluginXmlTmpl)
			
			activator = '''«basePackage».Activator'''
			lazyActivation = true
			requiredBundles = #[
				model.projectSymbolicName,
				"org.eclipse.ui",
				"org.eclipse.core.runtime",
				"org.eclipse.core.resources",
				"org.eclipse.ui.navigator",
				"org.eclipse.emf.common",
				"org.eclipse.emf.ecore"
			]
			binIncludes = #[
				"plugin.xml"
			]
			]
			* 
			*/
	}
	
	override projectSuffix() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}