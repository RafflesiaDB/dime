package info.scce.dime.generator.dad

import java.io.File
import java.net.URI
import org.apache.commons.io.FileUtils
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Platform
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

@FinalFieldsConstructor
class StaticResourcesHandler {
	
	static val STATIC_RESOURCES_BUNDLE = Platform.getBundle("info.scce.dime.generator")
	
	static val STATIC_DART_RESOURCES_FOLDER = "static-dart-resources/generated-sources"
	static val STATIC_DOCKER_RESOURCES_FOLDER = "static-docker-resources"
	static val STATIC_JAVA_RESOURCES_FOLDER = "static-java-resources"
	
	val IPath outlet
	
	def copyStaticDartResources() {
		val targetFolder = new File(outlet.append("../").toOSString)
		val webappFolder = new File(outlet.append("../webapp").toOSString)
		if (!webappFolder.exists) {
			println('''[INFO] Copy static Dart resources to «targetFolder» (exists: «targetFolder.exists»)''')
			copyBundlePathContent(STATIC_DART_RESOURCES_FOLDER, targetFolder)
		} else {
			println("[INFO] Skip copying static Dart resources (target folder already exists)");
		}
	}
	
	def copyStaticDockerResources() {
		val targetFolder = new File(outlet.append("../").toOSString)
		println('''[INFO] Copy static Docker resources to «targetFolder» (exists: «targetFolder.exists»)''')
		copyBundlePathContent(STATIC_DOCKER_RESOURCES_FOLDER, targetFolder)
	}
	
	def copyStaticJavaResources() {
		val targetFolder = new File(outlet.append("../").toOSString)
		println('''[INFO] Copy static Java resources to «targetFolder» (exists: «targetFolder.exists»)''')
		copyBundlePathContent(STATIC_JAVA_RESOURCES_FOLDER, targetFolder)
	}
	
	def copyBundlePathContent(String path, File targetFolder) {
		try {
			println("[INFO] Copy bundle entries in " + path)
			val pathEntries = STATIC_RESOURCES_BUNDLE.getEntryPaths(path)
			while (pathEntries.hasMoreElements) {
				val pathEntry = pathEntries.nextElement
				println("[INFO]  > path entry: " + pathEntry)
				val fileURL = FileLocator.toFileURL(STATIC_RESOURCES_BUNDLE.getEntry(pathEntry))
				println("[INFO]    > fileURL: " + fileURL)
				val resolvedURI = new URI(fileURL.protocol, fileURL.path, null)
				println("[INFO]    > resolvedURI: " + resolvedURI)
			    val file = new File(resolvedURI)
				println("[INFO]    > file: " + file)
				println("[INFO]    > file is directory: " + file?.isDirectory)
			    if (file.isDirectory) {
				    FileUtils.copyDirectoryToDirectory(file, targetFolder)
			    } else {
				    FileUtils.copyFileToDirectory(file, targetFolder)
			    }
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}