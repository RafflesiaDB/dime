package info.scce.dime.generator.data;

import info.scce.dime.data.data.Data;
import info.scce.dime.data.data.Type;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.xtext.util.StringInputStream;

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;
import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;


public class PojoGenerator implements IGenerator<Data>{
	
	public void generate(Data model,IPath outlet, IProgressMonitor monitor){
		monitor.beginTask("Generating Data Model", 100);
		monitor.subTask("Generating Java Classes");
		
		PojoGeneratorHelper helper = new PojoGeneratorHelper();
		IProject project = ProjectCreator.getProject(model.eResource());
		for(Type type: model.getTypes()){
			
			CharSequence contents = helper.doSwitch(type);
			if(contents!=null){
				String fileName = String.format("%s.java",type.getName());
				IFile output = project.getFolder(outlet.lastSegment()).getFile(fileName);
				try {
					if(!output.exists()){
						
							output.create(new StringInputStream(contents.toString()), true, monitor);
						
					}else{
							output.setContents(new StringInputStream(contents.toString()), true, true, monitor);;
					}
				
				} catch (CoreException e) {
					throw(new RuntimeException(e));
					
				}
			}
		}
		monitor.worked(90);
		monitor.subTask("Writing Class Files");
		
	}
	
}
