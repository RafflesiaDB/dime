package info.scce.dime.generator.data

import com.google.common.base.Charsets
import com.google.common.io.Files
import de.ls5.dywa.entities.object.DBField
import de.ls5.dywa.entities.object.DBObject
import de.ls5.dywa.entities.object.DBPackage
import de.ls5.dywa.entities.object.DBType
import de.ls5.dywa.entities.property.PropertyType
import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.BidirectionalAttribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumLiteral
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.generator.scheme.NativeDomainGenerator
import info.scce.dime.util.DataID
import java.util.Collections
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import org.eclipse.core.runtime.IPath

import static info.scce.dime.generator.util.DyWAExtension.*

import static extension info.scce.dime.generator.rest.DyWAAbstractGenerator.*

class DywaDataGenerator extends DataModelsGeneratorHelper {

	var idCounter = 0;
	val packageName = "de.ls5.dywa.generated"
	val migrationCSVContent = new StringBuilder();
	val cincoIdToTypeMap = new HashMap<Long, String>

	def generate(List<Data> models, IPath outlet) {

		val data = createMockupData(models)
		val eas = models
			.map[filterOriginalTypes]
			.flatten
			.toMap([generateInnerDywaTOName(dywaPkg).toString], [it.extensionAttributes.toList])
			.filter[p1, p2| !p2.empty]
		generateNativeDomain(data, outlet, eas);

		val output = outlet.removeLastSegments(1).append("/migration.csv").toFile;
		if (!output.exists()) {
			output.createNewFile()
		}
		Files.write(migrationCSVContent, output, Charsets.UTF_8)
	}

	def generateNativeDomain(Map<DBType, Set<DBObject>> data, IPath outlet,
		Map<String, List<ExtensionAttribute>> extensionAttributes) {
		val targetDirectoryApi = outlet.append("app-persistence-api/target/generated-sources");
		val targetDirectoryImpl = outlet.append("app-persistence-impl/target/generated-sources");
		NativeDomainGenerator.generateApi(data, extensionAttributes, cincoIdToTypeMap, packageName, targetDirectoryApi)
		NativeDomainGenerator.generateImpl(data, extensionAttributes, cincoIdToTypeMap, packageName, targetDirectoryImpl)

	}

	def createMockupData(List<Data> models) {
		val allTypes = models.map[filterOriginalTypes].flatten

		val typeMap = allTypes.cacheTypes
		typeMap.forEach[key, value|cincoIdToTypeMap.put(value.id, DataID.from(key).escapedLowerCase16)]
		val fieldMap = allTypes.cacheFields
		fieldMap.forEach[key, value|cincoIdToTypeMap.put(value.id, DataID.from(key).escapedLowerCase16)]
		val packageMap = allTypes.cachePackages

		configureTypes(allTypes, typeMap, packageMap)
		configureFields(allTypes, typeMap, fieldMap)

		val result = new HashMap<DBType, Set<DBObject>>

		typeMap.forEach[k, v|result.put(v, configureEnumLiterals(k, v))]
		
			
		typeMap.forEach[k,v|migrationCSVContent.append(k.id+";"+DataID.from(k).escapedLowerCase16+";"+v.name+";type\n")]
		fieldMap.forEach[k,v|if(!( k instanceof ExtensionAttribute))migrationCSVContent.append(k.type.id+";"+DataID.from(k.type).escapedLowerCase16+";"+k.id+";"+DataID.from(k).escapedLowerCase16+";"+v.type.name +";"+v.name+";field\n")]

		return result
	}

	def cacheTypes(Iterable<Type> types) {
		val result = new HashMap<Type, DBType>()
		types.forEach[result.put(it, new DBType => [id = idCounter++])]

		return result
	}

	def cacheFields(Iterable<Type> types) {
		val result = new HashMap<Attribute, DBField>()
		types.map[attributes].flatten.forEach[result.put(it, new DBField => [id = idCounter++])]

		return result
	}

	def cachePackages(Iterable<Type> types) {
		val result = new HashMap<String, DBPackage>()

		for (p : types.map[localPkgWithFilename]) {
			if (!result.containsKey(p)) {
				val subPackages = p.split("\\.")
				var DBPackage iter

				for (var i = 0; i < subPackages.length; i++) {
					val currentPackage = subPackages.get(i)
					val packageFQN = subPackages.subList(0, i + 1).join(".")
					var currentDBPackage = result.get(packageFQN)

					if (currentDBPackage === null) {
						currentDBPackage = new DBPackage => [id = idCounter++]
						currentDBPackage.name = currentPackage
						currentDBPackage.parentPackage = iter
						if (iter !== null) {
							iter.subPackages.add(currentDBPackage)
						}
						result.put(packageFQN, currentDBPackage)
					}

					iter = currentDBPackage
				}
			}
		}
		return result
	}

	def configureTypes(Iterable<Type> types, Map<Type, DBType> typeCache, Map<String, DBPackage> packageCache) {
		for (t : types) {
			val dbType = typeCache.get(t)

			// direct contents
			dbType.name = t.name
			dbType.abstractType = (t instanceof AbstractType)
			dbType.enumerable = (t instanceof EnumType)
			dbType.dyWAPackage = packageCache.get(t.localPkgWithFilename)

			// inheritance
			t.directSuperTypes.forEach [ st |
				val dbSuperType = typeCache.get(st)
				dbType.superTypes.add(dbSuperType)
				dbSuperType.subTypes.add(dbType)
			]
		}
	}

	def configureFields(Iterable<Type> types, Map<Type, DBType> typeCache, Map<Attribute, DBField> attributeCache) {
		for (t : types) {
			val dbType = typeCache.get(t)
			/*
			 * TODO Markus
			 */
			for (a : t.attributes.filter[!(it instanceof ExtensionAttribute)]) {
				val dbField = attributeCache.get(a);

				// direct contents
				dbField.name = a.name
				dbField.propertyType = PropertyType.valueOf(a.dyWATypeLiteral.toString)
				dbField.type = dbType

				// type constraint check
				dbField.typeConstraint = switch a {
					ComplexAttribute: typeCache.get(a.originalType)
					EnumLiteral: typeCache.get(a.container as EnumType)
				}

				// overriding check
				dbField.overriddenField = switch a {
					PrimitiveAttribute: attributeCache.getOrDefault(a.superAttr, null)
					ComplexAttribute: attributeCache.getOrDefault(a.superAttr, null)
				}

				// bidirectional check
				if (a instanceof BidirectionalAttribute) {
					val bidiDbField = attributeCache.get(a.oppositeAttribute)
					dbField.bidirectionalReference = bidiDbField
				}

				// add to type
				if (dbField.overriddenField !== null) {
					dbType.overridingFields.add(dbField)
				} else {
					dbType.fields.add(dbField)
				}
			}
		}
	}

	def configureEnumLiterals(Type type, DBType dbType) {
		if (type instanceof EnumType) {
			return type.enumLiterals.map[lit|new DBObject => [id = idCounter++; name = lit.name; type = dbType]].toSet
		}

		return Collections.emptySet
	}

}
