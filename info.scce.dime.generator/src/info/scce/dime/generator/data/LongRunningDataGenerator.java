package info.scce.dime.generator.data;

import static info.scce.dime.GraphModelUtil.execWithTransaction;
import static info.scce.dime.generator.process.BackendProcessGeneratorUtil.getContextTypeName;
import static info.scce.dime.generator.process.BackendProcessGeneratorUtil.getInteractionTypeName;
import static info.scce.dime.generator.process.BackendProcessGeneratorUtil.getInteractionInputsTypeNameJava;
import static info.scce.dime.generator.process.BackendProcessGeneratorUtil.getSecurityInputsTypeName;
import static org.eclipse.xtext.xbase.lib.StringExtensions.toFirstLower;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.data.data.AbstractType;
import info.scce.dime.data.data.Association;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.Data;
import info.scce.dime.data.data.DataPackage;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.data.data.ReferencedEnumType;
import info.scce.dime.data.data.ReferencedType;
import info.scce.dime.data.data.ReferencedUserType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserType;
import info.scce.dime.data.factory.DataFactory;
import info.scce.dime.generator.dad.GenerationContext;
import info.scce.dime.generator.process.BackendProcessGeneratorHelper;
import info.scce.dime.process.helper.ProcessExtension;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.GuardContainer;
import info.scce.dime.process.process.GuardProcessSIB;
import info.scce.dime.process.process.IterateSIB;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.OutputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;
import info.scce.dime.process.process.Variable;

public final class LongRunningDataGenerator {

	public static final String PROCESS_CONTEXT_ASSOCIATION = "processContext";
	public static final String INTERACTION_INPUTS_ASSOCIATION = "interactionInputs";
	
	private final ProcessExtension _extension = new ProcessExtension();
	
	private final GenerationContext genctx;
	
	public LongRunningDataGenerator(GenerationContext genctx) {
		this.genctx = genctx;
	}
	
	public Data generate(DAD dad, Set<Process> processes, IPath outlet, IProgressMonitor monitor) {
		monitor.beginTask("Generating Process Model Contexts", 100);
		monitor.subTask("Generating DyWA Data Connector");
		
		final IProject project = ProjectCreator.getProject(dad.eResource());
		final IFolder outputFolder = project.getFolder("gen");
		
		final List<Process> longRunningProcesses = processes.stream().filter(p -> p.getProcessType().equals(ProcessType.LONG_RUNNING))
				.collect(Collectors.toList());
		
		final List<Process> securityProcesses = processes.stream().filter(p -> p.getProcessType().equals(ProcessType.SECURITY))
				.collect(Collectors.toList());
		
		final List<Process> interactionProcesses = processes.stream().filter(p -> p.getProcessType().equals(ProcessType.BASIC))
				.collect(Collectors.toList());
		
		/*
		 * Use the basic API here instead of the C-API in order to boost
		 * generation due to not creating and updating diagram-specific stuff.
		 */
		DataFactory orgDataFactory = DataFactory.eINSTANCE;
		DataFactory.eINSTANCE = new DataFactory();
		
		try {
			DataFactory.eINSTANCE = new DataFactory();
			Data data = DataFactory.eINSTANCE.createData(outputFolder.getFullPath().toOSString(), "ProcessContexts");
			execWithTransaction(data, () -> {
				// FIXME use an API-provided method, e.g. 'deleteModelElements' to get rid of post-created stuff
				for (graphmodel.Node node : data.getAllNodes()) {
					node.delete();
				}
				createDataModel(dad, data, longRunningProcesses, securityProcesses, 
						interactionProcesses, monitor, outputFolder);
			});
			
			data.save();
			monitor.worked(90);
			
			return data;
		}
		catch (Exception e) {
			throw(new RuntimeException(e));
		}
		finally {
			DataFactory.eINSTANCE = orgDataFactory;
		}
	}

	private void createDataModel(DAD dad, Data data, List<Process> longRunningProcesses, List<Process> securityProcesses,
			List<Process> interactionProcceses,	IProgressMonitor monitor, final IFolder outputFolder) {
		// create super type for all process contexts
		final AbstractType abstractPC = data.newAbstractType("pc-" + dad.getId(), 0, 0); {
			abstractPC.setName("ProcessContext");
		}
		
		// create super type for all interaction process inputs. 
		final AbstractType abstractII = data.newAbstractType("ii-" + dad.getId(), 0, 0); {
			abstractII.setName("InteractionInputs");
		}
		
		// create super type for all security process inputs
		final AbstractType abstractSI = data.newAbstractType("si-" + dad.getId(), 0, 0); {
			abstractSI.setName("SecurityInputs");
		}
		
		// create super type for all interactions.
		final ComplexAttribute abstractPCAttr, abstractIIAttr;
		final AbstractType abstractInteract = data.newAbstractType("interact-" + dad.getId(), 0, 0); {
			abstractInteract.setName("Interaction");
			
			// add interactId for identifying the interaction process behind this interaction 
			// (so that a 'user' doesn't need to 'instanceof' the interaction class).
			final PrimitiveAttribute interactId = abstractInteract.newPrimitiveAttribute("interactId-" + dad.getId(), 0, 0);
			interactId.setName("interactId");
			interactId.setDataType(PrimitiveType.TEXT);
			
			// add interactId for identifying the interaction process behind this interaction 
			// (so that a 'user' doesn't need to 'instanceof' the interaction class).
			final PrimitiveAttribute guardContainerId = abstractInteract.newPrimitiveAttribute("guardContainerId-" + dad.getId(), 0, 0);
			guardContainerId.setName("guardContainerId");
			guardContainerId.setDataType(PrimitiveType.TEXT);
			
			// add a usage counter which tells how often this interaction has been execution so far.
			final PrimitiveAttribute usageCount = abstractInteract.newPrimitiveAttribute("usageCount-" + dad.getId(), 0, 0);
			usageCount.setName("usageCount");
			usageCount.setDataType(PrimitiveType.INTEGER);

			// add association to process context
			final Association abstractPCAssoc = abstractInteract.newAssociation(abstractPC);
			// retrieve attribute from waiting interaction to context of corresponding waiting process.
			abstractPCAttr = abstractPCAssoc.getSourceAttr();
			abstractPCAttr.setName(PROCESS_CONTEXT_ASSOCIATION);
			// FIXME set ID via C-API?
			EcoreUtil.setID(abstractPCAttr, "abstracPCAttr-" + dad.getId());

			// add association to interaction inputs.
			final Association abstractIIAssoc = abstractInteract.newAssociation(abstractII);
			// retrieve attribute from waiting interaction to interaction inputs.
			abstractIIAttr = abstractIIAssoc.getSourceAttr();
			abstractIIAttr.setName(INTERACTION_INPUTS_ASSOCIATION);
			// FIXME set ID via C-API?
			EcoreUtil.setID(abstractIIAttr, "abstractIIAttr-" + dad.getId());
		}

		final Map<String, Type> referencedTypes = new HashMap<>();
		final Map<String, ConcreteType> createdTypes = new HashMap<>();
		final Map<String, AbstractType> interactionSuperTypes = new HashMap<>();

		// create concrete process context types.
		for (final Process lrProcess: longRunningProcesses) {
			createContext(data, referencedTypes, createdTypes, lrProcess, abstractPC);
		}
		
		// create concrete interaction process inputs types.
		for (final Process iProcess: interactionProcceses) {
			createInputs(data, referencedTypes, createdTypes, iProcess, 
					abstractII, () -> getInteractionInputsTypeNameJava(iProcess), "ii", x -> true);
		}
		
		// create concrete security process inputs types.
		for (final Process sProcess: securityProcesses) {
			createInputs(data, referencedTypes, createdTypes, sProcess, 
					abstractSI, () -> getSecurityInputsTypeName(sProcess), "si", x -> !x.getName().equals("currentUser"));
		}
		
		// create abstract interaction subtypes on a interaction process level
		for (final Process inter: interactionProcceses) {
			final AbstractType interactionPerProcess = data.newAbstractType("interact-" + inter.getId(), 0, 0);
			interactionPerProcess.setName("Interaction" + inter.getModelName());
			interactionPerProcess.newInheritance(abstractInteract);

			final ConcreteType inputType = createdTypes.get(inter.getId());
			final Association iiAssoc = interactionPerProcess.newAssociation(inputType);
			// retrieve corresponding attribute and set superAttr.
			final ComplexAttribute iiAttr = iiAssoc.getSourceAttr();
			iiAttr.setSuperAttr(abstractIIAttr);
			iiAttr.setName(INTERACTION_INPUTS_ASSOCIATION);
			
			// FIXME set ID via C-API?
			EcoreUtil.setID(iiAttr, "iiAttr-" + inter.getId());
			
			interactionSuperTypes.put(inter.getId(), interactionPerProcess);
		}
		
		// Generate data types for inputs of all interaction sibs in long running processes.
		for (final Process lrProcess: longRunningProcesses) {
			lrProcess.getGuardContainers().stream().forEach(guardContainer -> {
				createInteraction(data, guardContainer, lrProcess,
						interactionSuperTypes.get(guardContainer.getGuardedProcessSIBs().get(0).getProMod().getId()), 
						abstractPCAttr, abstractIIAttr, createdTypes);
			});
		}
	}

	private void createInteraction(final Data data,  
			GuardContainer guardContainer, Process lrProcess, final AbstractType abstractInteract, 
			final ComplexAttribute abstractPCAttr, final ComplexAttribute abstractIIAttr, 
			final Map<String, ConcreteType> createdTypes) {
		final ConcreteType inter = data.newConcreteType("interact-" + guardContainer.getId(), 0, 0);
		inter.setName(getInteractionTypeName(guardContainer));
		inter.newInheritance(abstractInteract);
		
		final Association pcAssoc = inter.newAssociation(createdTypes.get(lrProcess.getId()));
		// retrieve corresponding attribute and set superAttr.
		final ComplexAttribute pcAttr = pcAssoc.getSourceAttr();
		pcAttr.setSuperAttr(abstractPCAttr);
		pcAttr.setName(PROCESS_CONTEXT_ASSOCIATION);
		// FIXME set ID via C-API?
		EcoreUtil.setID(pcAttr, "pcAttr-" + guardContainer.getId());
		

		for (final GuardProcessSIB guard: guardContainer.getGuardProcessSIBs()) {
			ConcreteType siType = createdTypes.get(guard.getSecurityProcess().getId());
			Association secInputsAssoc = inter.newAssociation(siType);
			final ComplexAttribute siAttr = secInputsAssoc.getSourceAttr();
			siAttr.setName(toFirstLower(siType.getName()));
			// FIXME set ID via C-API?
			EcoreUtil.setID(siAttr, "siAttr-" + guard.getId());
		}
	}
	
	private void createContext(Data data,
			final Map<String, Type> referencedTypes, final Map<String, ConcreteType> createdTypes,
			final Process lrProcess, final AbstractType abstractPC) {
		final ConcreteType pc = data.newConcreteType("pc-" + lrProcess.getId(), 0, 0);
		createdTypes.put(lrProcess.getId(), pc);
		pc.setName(getContextTypeName(lrProcess));
		pc.newInheritance(abstractPC);

		// Create attribute for each variable in the data contexts of a process.
		for (final Variable v: _extension.getVariables(lrProcess)) createAttribute(data, referencedTypes, pc, v);
		
		// Create attribute for each unique source of all direct data flows.
		for (final OutputPort ddf: _extension.getUniqueSources(_extension.getDirectDataFlows(lrProcess))) {
			createAttribute(data, referencedTypes, pc, ddf, new BackendProcessGeneratorHelper(genctx).getVarName(ddf), "pc");
		}
		
		// Create attribute for each iteration variable of iterate sibs.
		for (final IterateSIB iter: lrProcess.getIterateSIBs()) {
			createPrimitiveAttribute("pc-" + iter.getId(), pc, new BackendProcessGeneratorHelper(genctx).getIterVarName(iter), PrimitiveType.INTEGER,false);
		}
	}
	
	private void createInputs(final Data data, 
			final Map<String, Type> referencedTypes, 
			final Map<String, ConcreteType> createdTypes,
			final Process sProcess, final AbstractType abstractType, 
			final Supplier<String> supplyTypeName, 
			final String idPrefix, final Predicate<Output> inputsFilter) {
		final ConcreteType ct = data.newConcreteType(idPrefix + "-" + sProcess.getId(), 0, 0);
		createdTypes.put(sProcess.getId(), ct);
		ct.setName(supplyTypeName.get());
		ct.newInheritance(abstractType);
		
		sProcess.getStartSIBs().get(0).getOutputs().stream().filter(inputsFilter).forEach(
				output -> createAttribute(data, referencedTypes, ct, output, output.getName(), idPrefix));
	}
	
	private void createAttribute(final Data data, Map<String, Type> referencedTypes,
			final ConcreteType ct, Output output, String name, String idPrefix) {
		if (output instanceof PrimitiveOutputPort) {
			createPrimitiveAttribute(idPrefix + "-" + output.getId(), ct, name, convertPrimitiveType(((PrimitiveOutputPort)output).getDataType()),((PrimitiveOutputPort) output).isIsList());
		}
		else if (output instanceof ComplexOutputPort) {
			createComplexAttribute(idPrefix + "-" + output.getId(), data, referencedTypes, ct, name, ((ComplexOutputPort)output).getDataType(),((ComplexOutputPort) output).isIsList());
		}
		else {
			throw new IllegalStateException("Interaction/Security Processes may use "
					+ "primitive types and complex types as inputs, only. But found: "
					+ output.getClass().getSimpleName());
		}
	}

	private void createAttribute(final Data data,
			Map<String, Type> referencedTypes,
			final ConcreteType pc, Variable v) {
		final Attribute attr;
		if (v instanceof PrimitiveVariable) {
			attr = createPrimitiveAttribute("pc-" + v.getId(), pc, v.getName(), convertPrimitiveType(((PrimitiveVariable)v).getDataType()),v.isIsList());
		}
		else if (v instanceof ComplexVariable) {
			attr = createComplexAttribute("pc-" + v.getId(), data, referencedTypes, pc, v.getName(), ((ComplexVariable)v).getDataType(),v.isIsList());
		}
		else {
			throw new IllegalStateException("Long running processes may store "
					+ "primitive types and complex types only, but found: "
					+ v.getClass().getSimpleName());
		}
	}

	private ComplexAttribute createComplexAttribute(String id, final Data data, Map<String, Type> referencedTypes,
			final ConcreteType concType, String name, Type dataType,boolean isList) {
		final ComplexAttribute attr;
		final Type rtype;
		if (referencedTypes.containsKey(dataType.getId())) {
			rtype = referencedTypes.get(dataType.getId());
		}
		else {
			if(dataType instanceof EnumType) {
				rtype = data.newReferencedEnumType(dataType, id, 0, 0);
			}
			else if(dataType instanceof UserType) {
				rtype = data.newReferencedUserType(dataType, id, 0, 0);
			}
			else {
				rtype = data.newReferencedType(dataType, id, 0, 0);
			}
			referencedTypes.put(dataType.getId(), rtype);
		}
		
		final Association assoc;
		if(rtype instanceof ReferencedEnumType) {
			assoc = concType.newAssociation((ReferencedEnumType)rtype);
		}
		else if(rtype instanceof ReferencedUserType) {
			assoc = concType.newAssociation((ReferencedUserType)rtype);
		}
		else {
			assoc = concType.newAssociation((ReferencedType)rtype);
		}
		
		attr = assoc.getSourceAttr();
		attr.setIsList(isList);
		attr.setName(name);
		EcoreUtil.setID(attr, "attr-" + id);
		return attr;
	}
	
	private PrimitiveAttribute createPrimitiveAttribute(String id, final ConcreteType concType, String name,
			PrimitiveType ptype,boolean isList) {
		final PrimitiveAttribute pattr = concType.newPrimitiveAttribute(id, 0, 0);
		pattr.setDataType(ptype);
		pattr.setName(name);
		pattr.setIsList(isList);
		return pattr;
	}
	
	private PrimitiveType convertPrimitiveType(final info.scce.dime.process.process.PrimitiveType primDataType) {
		final PrimitiveType ptype;
		switch (primDataType) {
		case TEXT:
			ptype = PrimitiveType.TEXT;
			break;
		case INTEGER:
			ptype = PrimitiveType.INTEGER;
			break;
		case BOOLEAN:
			ptype = PrimitiveType.BOOLEAN;
			break;
		case REAL:
			ptype = PrimitiveType.REAL;
			break;
		case TIMESTAMP:
			ptype = PrimitiveType.TIMESTAMP;
			break;
		case FILE:
			ptype = PrimitiveType.FILE;
			break;
		default: throw new IllegalStateException("Unknown primitive type "
				+ "occured: " + primDataType);
		}
		return ptype;
	}
}
