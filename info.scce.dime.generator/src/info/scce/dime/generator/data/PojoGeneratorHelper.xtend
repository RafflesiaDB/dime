package info.scce.dime.generator.data

import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Inheritance
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.util.DataSwitch

public class PojoGeneratorHelper extends DataSwitch<CharSequence> {
	
	override caseType(Type type) '''
		import java.util.List;
«««		TODO: revert back to version without explicit cast once incoming/outgoing with inheritance has been fixed	
«««		public «IF type instanceof AbstractType»abstract «ENDIF»class «type.name» «IF type.getOutgoing(Inheritance).size()==1»extends «(type.getOutgoing(Inheritance).get(0)).targetElement.name»«ENDIF» {
		public «IF type instanceof AbstractType»abstract «ENDIF»class «type.name» «IF type.getOutgoing(Inheritance).size()==1»extends «((type.getOutgoing(Inheritance).get(0)).targetElement as Type).name»«ENDIF» {
			
			«FOR attribute: type.attributes»
				private «typeName(attribute)» «attribute.name»;
			«ENDFOR»
			
			«FOR attribute: type.attributes»
				«doSwitch(attribute)»
			«ENDFOR»
		}
	'''
	
	override caseAbstractType(AbstractType aType){
		caseType(aType)
	}
	
	override caseConcreteType(ConcreteType cType){
		caseType(cType)
	}
	
	override caseAttribute(Attribute attr){
		return getterSetter(typeName(attr), attr.name)
	}
	
	def typeName(Attribute attr) '''«IF attr.isList»List<«innerTypeName(attr)»>«ELSE»«innerTypeName(attr)»«ENDIF»'''
	
	private def innerTypeName(Attribute attr) {
		switch(attr) {
			case attr instanceof PrimitiveAttribute:
				return getLiteral((attr as PrimitiveAttribute).dataType)
			case attr instanceof ComplexAttribute:
				return (attr as ComplexAttribute).dataType.name
		}
	}
	
	def getLiteral(PrimitiveType pType){
		switch(pType){
			case BOOLEAN: "boolean"
			case INTEGER: "long"
			case REAL: "double"
			case TEXT: "String"
			case TIMESTAMP: "java.util.Date"
		}
	}
	
	def getterSetter(CharSequence typeName, String name)'''
	public «typeName» get«name.toFirstUpper»() {
		return this.«name»;
	}
	
	public void set«name.toFirstUpper»(«typeName» «name») {
		this.«name» = «name»;
	}
	
	'''
	
	override casePrimitiveAttribute(PrimitiveAttribute attr){
		caseAttribute(attr)
	}
	
	override caseComplexAttribute(ComplexAttribute attr)'''
		«getterSetter(typeName(attr), attr.name)»
	'''
	
}