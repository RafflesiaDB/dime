package info.scce.dime.generator.data

import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.data.data.ComplexAttribute

class DartDataField {
	
	
	public String restname
	public String attrname
	public Attribute attribute
	
	
}