package info.scce.dime.generator.process

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.UserType
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.process.process.Process
import java.util.Optional
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.xtext.util.StringInputStream

import static info.scce.dime.generator.process.BackendProcessGeneratorUtil.processPkg

import static extension info.scce.dime.generator.util.EclipseUtils.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class BackendProcessGenerator implements IGenerator<Process> {
	var IProject project
	var Optional<UserType> userType = Optional.empty()
	
	

	extension ResourceExtension = new ResourceExtension
	extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	extension BackendProcessGeneratorHelper helper
	protected extension RESTExtension = new RESTExtension

//	new() {
//		this.helper = new BackendProcessGeneratorHelper
//	}
	
	new(BackendProcessGeneratorHelper helper) {
		this.helper = helper
	}

	/** 
	 * In default case, {@link #generate(Process, IPath, IProgressMonitor)} will generate into a static path
	 * within the Project the passed Process belongs to. But there are cases where a special target project
	 * might be desired, e.g. when generating processes from multiple projects into one target application. In
	 * this case, this setProject method can be used to override the default behavior.
	 * @param project
	 */
	def void setProject(IProject project) {
		this.project = project
	}

	/** 
	 * An additional field required for generation, if certain security features are required.
	 * However we still want to implement {@link #generate(Process, IPath, IProgressMonitor)}
	 * @param userType
	 */
	def void setUserType(Optional<UserType> userType) {
		this.userType = userType
	}

	override void generate(Process model, IPath outlet, IProgressMonitor monitor) {
		
		val outputFolder = {
			// TODO get this folder information from some central place
			var outputFolder = project.getFolder("target").getFolder("dywa-app").getFolder("app-business")
				.getFolder("target").getFolder("generated-sources")
				
			for (pkgElement : processPkg.split("\\.")) {
				outputFolder = outputFolder.getFolder(pkgElement)
			}
			for (pkgElement : model.localPkg.pkgEscape.split("\\.")) {
				outputFolder = outputFolder.getFolder(pkgElement)
			}
			outputFolder
		}
		
		val outputFile = {
			// TODO create this filename at some central place
			val fileName = '''«getSimpleTypeName(model)».java'''
			outputFolder.getFile(fileName)
		}
		
		// Validate that the generated file either does not yet exists or is newer than the model file or else do not generate (unnecessarily)
		outputFolder.mkdirs
		if (!outputFile.exists) outputFile.create
		else if(outputFile.lastModified >= model.file?.lastModified) {
			return
		}
		
		val content = model.generate

		outputFile.writeContent(content)
	}

	def void generateContextTransformer(IPath outlet, IProgressMonitor monitor) {
		
		val outputFolder = {
			// TODO get this folder information from some central place
			var outputFolder = project.getFolder("target").getFolder("dywa-app").getFolder("app-business")
				.getFolder("target").getFolder("generated-sources")
				
			for (pkgElement : processPkg.split("\\.")) {
				outputFolder = outputFolder.getFolder(pkgElement)
			}
			
			outputFolder
		}
		
		val transformerFile = {
			val fileName = '''ContextTransformer.java'''
			outputFolder.getFile(fileName)
		}
		
		outputFolder.mkdirs
		if (!transformerFile.exists) transformerFile.create
		transformerFile.writeContent(generateJSONToContextTransformer)
	}
	
	private def toInputStream(String str) {
		new StringInputStream(str)
	}
	
	// TODO move to ProcessExtension?
	private def getFile(Process model) {
		model.eResource.file
	}
	
	// TODO move to EclipseExtension / IFileExtension?
	private def lastModified(IFile file) {
		file.rawLocation.makeAbsolute.toFile.lastModified
	}
	
	// TODO move to EclipseExtension / IFileExtension?
	private def create(IFile file) {
		file.create("".toInputStream, true, null)
	}
	
	// TODO move to EclipseExtension / IFileExtension?
	private def writeContent(IFile file, CharSequence content) {
		file.setContents(content.toString.toInputStream, true, true, null)
	}
}
