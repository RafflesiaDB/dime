package info.scce.dime.generator.process.util

import info.scce.dime.data.helper.DataExtension
import info.scce.dime.process.process.util.ProcessSwitch
import de.jabc.cinco.meta.runtime.xapi.CodingExtension
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.generator.util.DyWAExtension
import de.jabc.cinco.meta.runtime.xapi.CollectionExtension
import info.scce.dime.process.helper.ProcessExtension

class DIMEProcessSwitch<T> extends ProcessSwitch<T> {
	
	protected extension CollectionExtension = new CollectionExtension
	protected extension DataExtension = DataExtension.instance
	protected extension CodingExtension = new CodingExtension
	protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	protected extension DyWAExtension = new DyWAExtension
	protected extension ProcessExtension = new ProcessExtension
}