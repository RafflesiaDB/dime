package info.scce.dime.generator.process;


import static info.scce.dime.generator.util.JavaIdentifierUtils.escapeJava;
import static org.eclipse.xtext.xbase.lib.StringExtensions.toFirstUpper;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.data.data.UserType;
import info.scce.dime.generator.util.EclipseUtils;
import info.scce.dime.process.process.GuardContainer;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.xtext.util.StringInputStream;

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;

public class TODOListGenerator {

	public void generate(DAD dad, UserType subject, List<GuardContainer> guardContainers, IPath outlet, IProgressMonitor monitor) {
		monitor.beginTask("Generating TODO List", 100);
		monitor.subTask("Generating TODO List Class");
		
		final TODOListGeneratorHelper helper = new TODOListGeneratorHelper();
		final IProject project = ProjectCreator.getProject(dad.eResource());

		final CharSequence contents = helper.generate(dad, subject, guardContainers);
		if(contents != null){
			final String fileName = String.format("%s.java", "TODOList" + toFirstUpper(escapeJava(dad.getAppName())));
			
			IFolder outputFolder = project.getFolder("target").getFolder("dywa-app").getFolder("app-business").getFolder("target").getFolder("generated-sources")
					.getFolder("info").getFolder("scce").getFolder("dime").getFolder("process");
			
			final IFile output = outputFolder.getFile(fileName);
			try {
				EclipseUtils.mkdirs(outputFolder);
				
				if(!output.exists()) {
					output.create(new StringInputStream(contents.toString()), true, monitor);
				}
				else {
					output.setContents(new StringInputStream(contents.toString()), true, true, monitor);
				}
			}
			catch (CoreException e) {
				throw(new RuntimeException(e));
			}
		}
		final CharSequence dartContents = helper.generateDartClass(guardContainers);
		if(dartContents != null){
			final String fileName = "Todos.dart";
			
			IFolder outputFolder = project.getFolder("target").getFolder("dywa-app").getFolder("app-presentation").getFolder("target").getFolder("generated-sources")
					.getFolder("app").getFolder("lib").getFolder("models");
			
			final IFile output = outputFolder.getFile(fileName);
			try {
				EclipseUtils.mkdirs(outputFolder);
				
				if(!output.exists()) {
					output.create(new StringInputStream(dartContents.toString()), true, monitor);
				}
				else {
					output.setContents(new StringInputStream(dartContents.toString()), true, true, monitor);
				}
			}
			catch (CoreException e) {
				throw(new RuntimeException(e));
			}
		
			
		}
		
		monitor.worked(90);
		monitor.subTask("Writing Java Files");
	}
	
}
