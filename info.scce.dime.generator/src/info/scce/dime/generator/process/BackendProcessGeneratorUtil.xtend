package info.scce.dime.generator.process

import graphmodel.Node
import info.scce.dime.data.data.Type
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.AbstractIterateSIB
import info.scce.dime.process.process.Attribute
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.ComplexAttribute
import info.scce.dime.process.process.ComplexAttributeConnector
import info.scce.dime.process.process.ComplexExtensionAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexListAttribute
import info.scce.dime.process.process.ComplexListAttributeConnector
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.ControlFlow
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DataFlowSource
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.ExtensionAttribute
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.JavaNativeVariable
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveAttribute
import info.scce.dime.process.process.PrimitiveExtensionAttribute
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveListAttribute
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.PrimitiveVariable
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessInputStatic
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.StartSIB
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.process.process.Variable
import info.scce.dime.siblibrary.JavaType

import static info.scce.dime.generator.data.LongRunningDataGenerator.*
import static info.scce.dime.generator.util.DyWAExtension.*
import static info.scce.dime.process.process.ProcessType.ASYNCHRONOUS

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class BackendProcessGeneratorUtil extends GeneralProcessGeneratorHelper {
	
	protected extension RESTExtension = new RESTExtension
	protected extension DyWAExtension = new DyWAExtension
	
	// TODO should be defined in model or dad or somewhere?
	public static String processPkg = "info.scce.dime.process"
	public static String guiPkg = "info.scce.dime.gui"
	
	var long valueVariableIndex = 0
	
	
	dispatch def getPkg(Process it) '''«processPkg.pkgEscape».«localPkg.pkgEscape»''' 
	
	dispatch def getPkg(Type it) '''«dywaPkg.pkgEscape».entity.«localPkgWithFilename.pkgEscape»'''
	
	dispatch def getPkg(GUI it) '''«guiPkg.pkgEscape».«localPkgWithFilename.pkgEscape»'''

	
	override caseIntegerInputStatic(IntegerInputStatic it) '''«value»l'''
	
	override caseBooleanInputStatic(BooleanInputStatic it) '''«value»'''
	
	override caseRealInputStatic(RealInputStatic it) '''«value»d'''
	
	override caseTextInputStatic(TextInputStatic it) '''"«value.escapeString»"'''
	
	override caseProcessInputStatic(ProcessInputStatic input) {
		val it = input
		val baseProcess = (container as ProcessSIB).proMod.processPlaceholderSIBs.findFirst[label == input.name]?.proMod
		val replaceProcess = value
		
		'''(«baseProcess.processAllInputs.join(", ") [ switch it {
			OutputPort: name
			ProcessPlaceholderSIB: label
		} ]») -> {
				// lookup
				«replaceProcess.buildBackendProcessBeanLookup(name)»
				
				// execute
				final «replaceProcess.externalResultReturnTypeName» result«name» = instance«name».execute(«replaceProcess.processAllInputs.join(", ") [
					switch it {
						OutputPort: name
						ProcessPlaceholderSIB: label
					}
				]»);
				
				// evaluate branch
				switch(result«name».getBranchName()) {
					«FOR endSIB: replaceProcess.endSIBs»
					case "«endSIB.branchName.escapeJava»": 
						// create new base process result
						return new «baseProcess.externalResultReturnTypeName»(
							«val baseEndSIB = baseProcess.endSIBs.findFirst[ branchName == endSIB.branchName ]»
							new «baseEndSIB.externalStoredTypeName»(
								«baseEndSIB.inputs.join(", ") [ 
									'''result«name».get«endSIB.branchName.toFirstUpper.escapeJava»Return().get«name.toFirstUpper.escapeJava»''' 
								]»
							)
						);
					«ENDFOR»
					default: throw new IllegalStateException("returned unknown branch " + result«name».getBranchName());
				}
			}'''
		}
	
	
	def buildBackendProcessBeanLookup(Process it) {
		buildBackendProcessBeanLookup("")
	}
	
	def buildBackendProcessBeanLookup(Process it,String name) {
		buildBackendProcessBeanLookup(name,"")
	}
	
	def buildBackendProcessBeanLookup(Process it, String name,String suffix) '''
	«IF requiresDependencyInjection»
		final «typeName» instance«name» = CDIUtil.getManagedInstance(ctx.beanManager, «typeName».class);
	«ELSE»
		final «typeName» instance«name» = new «typeName»(ctx.beanManager);
	«ENDIF»
	'''
	
	override caseTimestampInputStatic(TimestampInputStatic input) '''new Date(«input.value»000l)'''
	
	def getInputByName(SIB sib, String name) {
		sib.inputs.filter[i | i.name.equals(name)].head
	}
	
	

	def getExternalResultTypeName(Process it) '''«typeName».«resultTypeName»'''
	
	def getExternalResultReturnTypeName(Process it) {
		buildInternalResultReturnTypeName(externalResultTypeName)
	} 
	
	def getResultReturnTypeName(Process it) {
		buildInternalResultReturnTypeName(resultTypeName)
	}
	
	private def static buildInternalResultReturnTypeName(Process it, CharSequence resultTypeName) '''«
		IF processType == ASYNCHRONOUS
			»Future<«
		ENDIF»«resultTypeName»«
		IF processType == ASYNCHRONOUS
			»>«
		ENDIF
	»'''
	
	def getExternalResultTypeName(GUI it) '''«pkg».«resultTypeName»'''

	def getExternalResultReturnTypeName(GUI it, String branch) '''«externalResultTypeName».«branch.escapeJava»Return'''

	def getTypeName(Process it) '''«pkg».«simpleTypeName»'''
	
	def getTypeName(GUI it) '''«pkg».«simpleTypeName»'''

	def getControllerTypeName(Type it) '''«controllerPkg».«name.escapeJava»Controller'''

	def getInteractionControllerTypeName(GuardContainer it) '''«controllerPkg».«interactionTypeName.escapeJava»Controller'''
	
	def getInteractionInputsControllerTypeName(Process subProcess) '''«subProcess.controllerPkg».«subProcess.interactionInputsTypeNameJava.escapeJava»Controller'''
	
	def getSecurityInputsControllerTypeName(Process subProcess) '''«subProcess.controllerPkg».«subProcess.securityInputsTypeName.escapeJava»Controller'''
	
	def getContextControllerTypeName(Process process) '''«process.controllerPkg».«process.contextTypeName.escapeJava»Controller'''
	
	def lookupSecurityInputsController(Process guardProcess, String name) {
		guardProcess.securityInputsControllerTypeName.lookupController(name)
	}
	
	def lookupInteractionInputsController(Process subProcess, String name) {
		subProcess.interactionInputsControllerTypeName.lookupController(name)
	}
	
	def lookupContextController(String name) {
		process.contextControllerTypeName.lookupController(name)
	}
	
	def lookupInteractionController(GuardContainer guardContainer, String name) {
		guardContainer.interactionControllerTypeName.lookupController(name)
	}
	
	def lookupDomainController(Type type, String name) '''
		final «type.controllerTypeName» «name» = this.«type.controllerSimpleName»;
	'''
	
	def lookupController(CharSequence typeName, String name) '''
		final «typeName» «name» = this.«typeName»;
	'''
	
	def callDeleteInteraction(GuardContainer guardContainer, String processVarName, String interactionVarName) '''
		«processVarName».deleteInteraction«guardContainer.id.escapeJava»(«interactionVarName»);
	'''
	
	def deleteInteraction(GuardContainer guardContainer) '''
		public void deleteInteraction«guardContainer.id.escapeJava»(«guardContainer.interactionTypeName.processContextsTypeName» interaction) {
			ctx.beanManager = this.beanManager;
			«val subProcess = guardContainer.subProcess»
			// delete interaction
			«"contextController".lookupContextController»
			contextController.delete«process.contextTypeName.escapeJava»(interaction.get«PROCESS_CONTEXT_ASSOCIATION.escapeJava»());
			
			«subProcess.lookupInteractionInputsController("interInputsController")»
			interInputsController.delete«subProcess.interactionInputsTypeNameJava.escapeJava»(interaction.get«INTERACTION_INPUTS_ASSOCIATION.escapeJava»());
			
			«FOR guardProcessSIB: guardContainer.guardProcessSIBs»
				{
					«val guardProcess = guardProcessSIB.securityProcess»
					«guardProcess.lookupSecurityInputsController("securityInputsController")»
					securityInputsController.delete«guardProcess.securityInputsTypeName.escapeJava»(interaction.get«guardProcess.securityInputsTypeName.toFirstLower.escapeJava»());
				}
			«ENDFOR»
			«guardContainer.lookupInteractionController("interactionController")»
			interactionController.delete«guardContainer.interactionTypeName.escapeJava»(interaction);
		}
	'''
	
	def checkInteraction(GuardContainer guardContainer) '''
		// Security checks for executing the «guardContainer.subProcess.modelName» interaction
		public boolean checkInteraction«guardContainer.id.escapeJava»(final «guardContainer.interactionTypeName.processContextsTypeName» interaction, «guardContainer.subProcess.interactionInputsTypeNameJava.processContextsTypeName» parametersToCheck) {
			
			final «guardContainer.subProcess.interactionInputsTypeNameJava.processContextsTypeName» interactionInputs = interaction.getinteractionInputs();
			
			«FOR input : guardContainer.subProcess.processInputs»
				// if both objects are null, we consider them equal. Otherwise compare:
				if (interactionInputs.get«input.name.escapeJava»() != null || parametersToCheck.get«input.name.escapeJava»() != null) {

					// if one object is null and the other is not, then not equal
					if (interactionInputs.get«input.name.escapeJava»() == null || parametersToCheck.get«input.name.escapeJava»() == null) {
						return false;
					}
					//compare
					if (!interactionInputs.get«input.name.escapeJava»().equals(parametersToCheck.get«input.name.escapeJava»())) {
						return false;
					}
				}
			«ENDFOR»
			
			«FOR processGuard: guardContainer.guardProcessSIBs»
				{
					«val guardProcess = processGuard.securityProcess»
					final «guardProcess.securityInputsTypeName.processContextsTypeName» inputs = interaction.get«guardProcess.securityInputsTypeName.toFirstLower.escapeJava»();
					final «guardProcess.typeName».«guardProcess.resultTypeName» result = «guardProcess.simpleTypeName.toFirstLower».execute(«processGuard.securityProcess.processInputs.sortBy[name].join(', ') [ '''«IF name.equals("currentUser")»getConcreteUser(«guardProcess.concreteUserType.typeName».class)«ELSE»inputs.get«name.escapeJava»()«ENDIF»''' ]»);
					if ("permanently denied".equals(result.getBranchName())) {
						this.deleteInteraction«guardContainer.id.escapeJava»(interaction);
						return false;
					}
					if (!"granted".equals(result.getBranchName())) {
						return false;
					}
				}
			«ENDFOR»
			
			return true;
		}
	'''
	
	def String getTypeName(ProcessPlaceholderSIB it) {
		'''org.jooq.lambda.function.Function«inputs.size»<«proMod.processAllInputs.join(", ") [ 
			switch it {
				OutputPort: typeName
				ProcessPlaceholderSIB: typeName
				default: throw new IllegalArgumentException('''No case for «class».''')
			}
		]»«
		IF !proMod.processAllInputs.empty», «
		ENDIF»«proMod.externalResultReturnTypeName»>'''
	}
	
	override getTypeName(OutputPort outputPort)
		'''«IF (outputPort.isIsList)»List<«ENDIF»«outputPort.singularTypeName»«IF (outputPort.isIsList)»>«ENDIF»'''
		
		
	def getSingularTypeName(OutputPort outputPort)
		'''«switch outputPort {
			PrimitiveOutputPort: outputPort.trgtLangLiteral
			ComplexOutputPort: outputPort.dataType.typeName
			JavaNativeOutputPort: (outputPort.dataType as JavaType).fqn
		}»'''
		
	def getRouterTypeName(OutputPort outputPort)
	'''«IF (outputPort.isIsList)»List<«ENDIF»«outputPort.singularRestTypeName»«IF (outputPort.isIsList)»>«ENDIF»'''
	
	def getRouterTypeName(InputPort inputPort)
	'''«IF (inputPort.isIsList)»List<«ENDIF»«inputPort.singularRestTypeName»«IF (inputPort.isIsList)»>«ENDIF»'''
	
	
	def getSingularRouterTypeName(OutputPort outputPort)
		'''«switch outputPort {
			PrimitiveOutputPort: outputPort.restTypeName
			ComplexOutputPort: "java.lang.Long"
			JavaNativeOutputPort: (outputPort.dataType as JavaType).fqn
		}»'''
		
	def getSingularRouterTypeName(InputPort outputPort)
		'''«switch outputPort {
			PrimitiveInputPort: outputPort.restTypeName
			ComplexInputPort: "java.lang.Long"
		}»'''
		
	def getRestTypeName(InputPort outputPort)
		'''«IF (outputPort.isIsList)»List<«ENDIF»«outputPort.singularRestTypeName»«IF (outputPort.isIsList)»>«ENDIF»'''
	
	
	def getRestTypeName(OutputPort outputPort)
		'''«IF (outputPort.isIsList)»List<«ENDIF»«outputPort.singularRestTypeName»«IF (outputPort.isIsList)»>«ENDIF»'''
		
	def getSingularRestTypeName(OutputPort outputPort)
		'''«switch outputPort {
			PrimitiveOutputPort: outputPort.dataType.restLangLiteral
			ComplexOutputPort: outputPort.dataType.restTypeName
			JavaNativeOutputPort: (outputPort.dataType as JavaType).fqn
		}»'''
		
	def getSingularRestTypeName(InputPort outputPort)
		'''«switch outputPort {
			PrimitiveInputPort: outputPort.dataType.restLangLiteral
			ComplexInputPort: outputPort.dataType.restTypeName
			JavaNativeInputPort: (outputPort.dataType as JavaType).fqn
		}»'''
	
	def getSimpleTypeName(OutputPort outputPort)
		'''«	switch outputPort {
			PrimitiveOutputPort: outputPort.trgtLangLiteral
			ComplexOutputPort: outputPort.dataType.name.escapeJava
			JavaNativeVariable: (outputPort.dataType as JavaType).fqn.split("\\.").last
		}»'''
	
	override getTypeName(InputPort inputPort)
		'''«IF (inputPort.isIsList)»List<«ENDIF»«switch inputPort {
			PrimitiveInputPort: inputPort.trgtLangLiteral
			ComplexInputPort: inputPort.dataType.typeName
			JavaNativeInputPort: (inputPort.dataType as JavaType).fqn
		}»«IF (inputPort.isIsList)»>«ENDIF»'''
		
	override getTypeName(InputStatic input)
		'''«input.dataType.trgtLangLiteral»'''
	
	def getSimpleTypeName(InputPort inputPort)
		'''«switch inputPort {
			PrimitiveInputPort: inputPort.trgtLangLiteral
			ComplexInputPort: inputPort.dataType.name.escapeJava
			JavaNativeInputPort: (inputPort.dataType as JavaType).fqn.split("\\.").last
		}»'''
		
	def buildGetterNoValueSemantics(InputPort inputPort) {
		inputPort.buildGetterInternal[ it.buildGetterNoValueSemantics(inputPort) ]
	}
		
	override buildGetter(InputPort inputPort) {
		inputPort.buildGetterInternal[ it.buildGetter(inputPort) ]
	}
	
	private def buildGetterInternal(InputPort inputPort, (Node) => CharSequence buildGetter) {
		val incoming = inputPort.dataEdge
		if (incoming != null) {
			'''«switch source: incoming.sourceElement {
				PrimitiveVariable: '''ctx.«source.name.escapeJava»'''
				JavaNativeVariable: '''ctx.«source.name.escapeJava»'''
				ComplexVariable: 
					buildGetter.apply(source)
				PrimitiveAttribute: 
					if(source.isIsList) '''new Array«inputPort.typeName»(«source.getter».orElse(new LinkedList<>()))'''
					else '''«source.getter».orElse(«(inputPort as PrimitiveInputPort).dataType.trgtLangDeclaration»)'''
				PrimitiveExtensionAttribute: 
					if(source.isIsList) '''new Array«inputPort.typeName»(«source.getter».orElse(new LinkedList<>()))'''
					else '''«source.getter».orElse(«(inputPort as PrimitiveInputPort).dataType.trgtLangDeclaration»)'''
				ComplexAttribute, ComplexExtensionAttribute:
					buildGetter.apply(source)
				PrimitiveListAttribute: '''Long.valueOf(«source.parent.getter».map(List::size).orElse(0))'''
				ComplexListAttribute: {
					'''«source.parent.getter».«switch source.attributeName {
						case FIRST: '''map(List::stream).flatMap(Stream::findFirst).orElse(null)'''
						case LAST: '''map(List::stream).flatMap(stream -> stream.reduce((previous, current) -> current)).orElse(null)'''
					}»'''
				}
				OutputPort: '''ctx.«source.varName»'''
			}»'''
		}
	}
	
	dispatch def buildGetterNoValueSemantics(ComplexVariable source, InputPort inputPort) {
		if (source.complexVariablePredecessor != null) {
			if(source.isIsList) '''«source.getter».orElse(new LinkedList<>())'''
			else '''«source.getter».orElse(null)''' 
		}
		else '''ctx.«source.name.escapeJava»'''
	}
	
	dispatch def buildGetterNoValueSemantics(ComplexAttribute source, InputPort inputPort) {
		if(source.isIsList) '''«source.getter».orElse(new LinkedList<>())'''
		else '''«source.getter».orElse(null)'''
	}
	
	dispatch def buildGetterNoValueSemantics(ComplexExtensionAttribute source, InputPort inputPort) {
		if(source.isIsList) '''«source.getter».orElse(new LinkedList<>())'''
		else '''«source.getter».orElse(null)'''
	}
	
	dispatch def buildGetter(ComplexVariable source, InputPort inputPort) {
		if(source.isIsList) '''new Array«inputPort.typeName»(«source.buildGetterNoValueSemantics(inputPort)»)'''
		else source.buildGetterNoValueSemantics(inputPort)
	}
	
	dispatch def buildGetter(ComplexAttribute source, InputPort inputPort) {
		if(source.isIsList)  '''new Array«inputPort.typeName»(«source.buildGetterNoValueSemantics(inputPort)»)'''
		else source.buildGetterNoValueSemantics(inputPort)
	}
	
	dispatch def buildGetter(ComplexExtensionAttribute source, InputPort inputPort) {
		if(source.isIsList)  '''new Array«inputPort.typeName»(«source.buildGetterNoValueSemantics(inputPort)»)'''
		else source.buildGetterNoValueSemantics(inputPort)
	}
	
	def buildSetterForSingleOutputPortNoValueSemantics(OutputPort outputPort, String value) '''
		«FOR outgoing : outputPort.outgoingDataFlows.sortTopologically»
			«outgoing.buildSetterNoValueSemantics(value)»
		«ENDFOR»
	'''
	
	override buildSetterForSingleOutputPort(OutputPort outputPort, String value) '''
		«FOR outgoing : outputPort.outgoingDataFlows.sortTopologically»
			«outgoing.buildSetter(value)»
		«ENDFOR»
	'''
	
	override buildSetter(DataFlow outgoing, String value) {
		buildSetterInternal(outgoing, value, true)
	}
	
	def buildSetterNoValueSemantics(DataFlow outgoing, String value) {
		buildSetterInternal(outgoing, value, false)
	}
	
	def String buildSetterInternal(DataFlow flow, String valueStr, boolean valueSemantics) {
		var sourceIsList = false
		var targetIsList = false
		var Node writeSource = flow.sourceElement
		var Node writeTarget = flow.targetElement
		var IO port = null
		var CharSequence sourceTypeName
		
		if (flow.sourceElement instanceof OutputPort) {
			val source = flow.sourceElement as OutputPort
			sourceIsList = source.isIsList
			port = source
		}
		else if (flow.targetElement instanceof InputPort) {
			val target = flow.targetElement as InputPort
			targetIsList = target.isIsList
			port = target
		}

		if (writeSource instanceof PrimitiveVariable) {
			sourceIsList = writeSource.isIsList
			sourceTypeName = writeSource.typeName
		} else if (writeSource instanceof ComplexVariable) {
			sourceIsList = writeSource.isIsList
			sourceTypeName = writeSource.typeName
		} else if (writeSource instanceof JavaNativeVariable) {
			sourceIsList = writeSource.isIsList
			sourceTypeName = writeSource.typeName
		} else if (writeSource instanceof Attribute) {
			sourceIsList = writeSource.isIsList
			sourceTypeName = writeSource.typeName
		} else if (writeSource instanceof Input) {
			sourceIsList = writeSource.isIsList
			if (writeSource instanceof InputPort) {
				sourceTypeName = writeSource.typeName
			}
		} else if (writeSource instanceof Output) {
			sourceIsList = writeSource.isIsList
			if (writeSource instanceof OutputPort) {
				sourceTypeName = writeSource.typeName
			}
		}
		
		if (writeTarget instanceof PrimitiveVariable) {
			targetIsList = writeTarget.isIsList
		} else if (writeTarget instanceof ComplexVariable) {
			targetIsList = writeTarget.isIsList
		} else if (writeTarget instanceof JavaNativeVariable) {
			targetIsList = writeTarget.isIsList
		} else if (writeTarget instanceof Attribute) {
			targetIsList = writeTarget.isIsList
		} else if (writeTarget instanceof Input) {
			targetIsList = writeTarget.isIsList
		} else if (writeTarget instanceof Output) {
			targetIsList = writeTarget.isIsList
		}
		
		val value = if (targetIsList) '''value«valueVariableIndex++»''' else valueStr
		
		'''
		«IF targetIsList»
			«sourceTypeName» «value» = «valueStr»;
			if («value» != null) { // prevent null in lists
		«ENDIF»
		«switch target: writeTarget {
			PrimitiveVariable:
				target.buildSetterToVariable(sourceIsList, value)
			ComplexVariable: 
				if (target.complexVariablePredecessor != null) {
					if (target.isIsList && !sourceIsList) '''«target.setter».add(«value»)'''
					else if (target.isIsList && sourceIsList) {
						if(valueSemantics) '''«target.listSourceSetter»(new Array«target.typeName»(«value»))'''
						else '''«target.listSourceSetter»(«value»)'''
					} 
					else '''«target.setter»(«value»)'''
				}
				else
					target.buildSetterToVariable(sourceIsList,
						if(target.isIsList && sourceIsList && valueSemantics)
							'''new Array«target.typeName»(«value»)''' 
						else value
					)
			JavaNativeVariable: target.buildSetterToVariable(sourceIsList, value)
			ComplexAttribute: 
				if (target.isIsList && !sourceIsList) '''«target.setter».add(«value»)'''
				else if (target.isIsList && sourceIsList && valueSemantics)
					'''«target.listSourceSetter»(new Array«target.typeName»(«value»))'''
				else '''«target.listSourceSetter»(«value»)'''
			PrimitiveAttribute: 
				if (target.isIsList && !sourceIsList) '''«target.setter».add(«value»)'''
				else if (target.isIsList && sourceIsList && valueSemantics)
					'''«target.listSourceSetter»(new Array«target.typeName»(«value»))'''
				else '''«target.listSourceSetter»(«value»)'''
			ComplexListAttribute: {
				'''«target.parent.getter».get().«switch target.attributeName {
					case FIRST: '''set(0, «value»)'''
					case LAST: '''set(«target.parent.getter».get().size()-1, «value»)'''
				}»'''
			}
			InputPort: // TODO switch over outgoing DDF and then use one variable per input...
				if (target.isIsList && !sourceIsList) '''ctx.«(port as OutputPort).varName».add(«value»)'''
				else if (target.isIsList && sourceIsList && valueSemantics)
					'''ctx.«(port as OutputPort).varName» = new Array«(port as OutputPort).typeName»(«value»)'''
				else '''ctx.«(port as OutputPort).varName» = «value»'''
			Input:
				if (target.isIsList && !sourceIsList) '''ctx.«(port as OutputPort).varName».add(«value»)'''
				else '''ctx.«(port as OutputPort).varName» = «value»'''
			OutputPort: // TODO switch over outgoing DDF and then use one variable per input...
				if (target.isIsList && !sourceIsList) '''ctx.«(port as InputPort).varName».add(«value»)'''
				else if (target.isIsList && sourceIsList && valueSemantics)
					'''ctx.«(port as InputPort).varName» = new Array«(port as InputPort).typeName»(«value»)'''
				else '''ctx.«(port as InputPort).varName» = «value»'''
			Output:
				if (target.isIsList && !sourceIsList) '''ctx.«(port as InputPort).varName».add(«value»)'''
				else '''ctx.«(port as InputPort).varName» = «value»'''
		}»;
		«IF targetIsList»}«ENDIF»
		'''
	}
		
	override buildSetterToVariable(Variable target, OutputPort outputPort, String value) {
		if (target.isIsList && !outputPort.isIsList)
			'''ctx.«target.name.escapeJava».add(«value»)'''
		else
			'''ctx.«target.name.escapeJava» = «value»'''
	}
	
	def buildSetterToVariable(Variable target, boolean isList, String value) {
		if (target.isIsList && !isList)
			'''ctx.«target.name.escapeJava».add(«value»)'''
		else
			'''ctx.«target.name.escapeJava» = «value»'''
	}
	
	def getListSourceSetter(PrimitiveAttribute attr) '''«attr.parent.getter».get().set«attr.nameOfAccessor»'''
	
	def getSetter(PrimitiveAttribute attr) '''«attr.parent.getter».«IF attr.isIsList»map(«attr.parent.type.typeName»::get«attr.nameOfAccessor»).get()«ELSE»get().set«attr.nameOfAccessor»«ENDIF»'''
	
	def getListSourceSetter(ComplexAttribute attr) '''«attr.parent.getter».get().set«attr.nameOfAccessor»'''
	
	def getSetter(ComplexAttribute attr) '''«attr.parent.getter».«IF attr.isIsList»map(«attr.parent.type.typeName»::get«attr.nameOfAccessor»).get()«ELSE»get().set«attr.nameOfAccessor»«ENDIF»'''
	
	def getListSourceSetter(ComplexVariable cv) '''«cv.complexVariablePredecessor.getter».get().set«cv.complexVariablePredecessor.type.getAttributeByName(cv.attributeName).nameOfAccessor»'''
	
	def getSetter(ComplexVariable cv) '''«IF cv.complexVariablePredecessor != null»«cv.complexVariablePredecessor.getter».«cv.innerSetter»«ELSE»Optional.ofNullable(ctx.«cv.name.escapeJava»)«ENDIF»'''
	
	def getGetter(PrimitiveAttribute attr) '''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.nameOfAccessor»)'''
	
	def getGetter(PrimitiveExtensionAttribute attr) '''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.attribute.nameOfAccessor»)'''
	
	def getGetter(ComplexAttribute attr) '''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.nameOfAccessor»)'''
	
	def getGetter(ComplexExtensionAttribute attr) '''«attr.parent.getter».map(«attr.parent.typeName»::get«attr.attribute.nameOfAccessor»)'''
	
	def CharSequence getGetter(ComplexVariable cv) '''«IF cv.complexVariablePredecessor != null»«cv.complexVariablePredecessor.getter».«cv.innerGetter»«ELSE»Optional.ofNullable(ctx.«cv.name.escapeJava»)«ENDIF»'''
	
	def getLiteralOrGetterNoValueSemantics(Input input) {
		input.buildLiteralOrGetter[ it.buildGetterNoValueSemantics ]
	}
	
	override getLiteralOrGetter(Input input) {
		input.buildLiteralOrGetter[ it.buildGetter ]
	}
	
	private def buildLiteralOrGetter(Input input, (InputPort) => CharSequence buildGetter) {
		switch it: input {
			InputStatic: doSwitch(it)
			InputPort: 
				if (dataEdge == null) {
					if(isList) '''new LinkedList<>()''' 
					else switch it: input {
						ComplexInputPort: '''null'''
						JavaNativeInputPort: '''null'''
						PrimitiveInputPort: nullDeclaration
					}
				}
				else buildGetter.apply(it)
		}
	}
	
	def isIsList(Attribute attr) {
		switch it : attr {
			ComplexAttribute: isIsList
			ExtensionAttribute: isIsList
			PrimitiveAttribute: isIsList
		}
	}
	
	def isIsList(ComplexAttribute attr) {
		attr.attribute.isIsList
	}
	
	def isIsList(ExtensionAttribute attr) {
		attr.attribute.isIsList
	}
	
	def isIsList(PrimitiveAttribute attr) {
		attr.attribute.isIsList
	}
	
	def isIsList(Input input) {
		switch(it: input) {
			InputPort: isList
			default: false
		}
	}
	
	def isIsList(Output output) {
		switch(it: output) {
			OutputPort: isList
			default: false
		}
	}
	
	def getNameOfAccessor(PrimitiveAttribute attr) {
		attr.attribute.nameOfAccessor
	}
	
	def getNameOfAccessor(ComplexAttribute attr) {
		attr.attribute.nameOfAccessor
	}
	
	def getNameOfAccessor(Type type, String name) {
		type.getAttributeByName(name).nameOfAccessor
	}
	
	def getParent(Attribute attr) {
		(attr.container as ComplexVariable)
	}
	
	def getAttributeName(ComplexVariable cv) {
		cv.getIncoming(ComplexAttributeConnector).head.attributeName
	}
	
	def getAttributeByName(Type type, String name) {
		type.inheritedAttributes.findFirst[a | a.name == name]
	}
	
	def getType(ComplexVariable cv) {
		cv.dataType
	}
	
	def getUnfoldedListAttributeName(ComplexVariable cv) {
		cv.getIncoming(ComplexListAttributeConnector).head?.attributeName
	}
	
	def getUnfoldListParent(ComplexVariable cv) {
		cv.getIncoming(ComplexListAttributeConnector).head?.sourceElement	
	}
	
	def getInnerGetter(ComplexVariable cv) {
		val parentCV = cv.complexVariablePredecessor
		if (parentCV.isIsList) {
			switch cv.getUnfoldedListAttributeName {
			case FIRST: '''map(List::stream).flatMap(Stream::findFirst)'''
			case LAST: '''map(List::stream).flatMap(stream -> stream.reduce((previous, current) -> current))'''
			}
		}
		else {
			'''map(«parentCV.type.typeName»::get«parentCV.type.getAttributeByName(cv.attributeName).nameOfAccessor»)'''
		}
	}
	
	def getInnerSetter(ComplexVariable cv) {
		val parentCV = cv.complexVariablePredecessor
		if (cv.isIsList) {
			'''map(«parentCV.type.typeName»::get«parentCV.type.getAttributeByName(cv.attributeName).nameOfAccessor»).get()'''
		}
		else {
			'''get().set«parentCV.type.getAttributeByName(cv.attributeName).nameOfAccessor»'''
		}
	}
	
	def getTypeNameOfDataSource(InputPort inputPort) {
		val incoming = inputPort.dataEdge
		if (incoming != null)
			switch source: incoming.sourceElement {
				Variable: source.typeName
				OutputPort: source.typeName
			}
	}
	
	def getTypeName(Attribute attr)
		'''«IF (attr.isIsList)»List<«ENDIF»«switch it : attr {
					ComplexAttribute: it.attribute.dataType.typeName
					ComplexListAttribute: it.listType.typeName
					PrimitiveAttribute: it.attribute.dataType.trgtLangLiteral
//					PrimitiveListAttribute: (it.listType as info.scce.dime.data.data.PrimitiveType).trgtLangLiteral
					default: "?"
			}»«IF (attr.isIsList)»>«ENDIF»'''
	
	override getTypeName(Variable variable)
		'''«IF (variable.isIsList)»List<«ENDIF»«variable.singularTypeName»«IF (variable.isIsList)»>«ENDIF»'''
	
	def getSingularTypeName(Variable variable)
		'''«switch variable {
			ComplexVariable: variable.dataType.typeName
			PrimitiveVariable: variable.dataType.trgtLangLiteral
			JavaNativeVariable: (variable.dataType as JavaType).fqn
		}»'''
		
	def getRestTypeName(Variable variable)
		'''«IF (variable.isIsList)»List<«ENDIF»«variable.singularRestTypeName»«IF (variable.isIsList)»>«ENDIF»'''
		
	def getSingularRestTypeName(Variable variable)
		'''«switch variable {
			ComplexVariable: variable.dataType.restTypeName
			PrimitiveVariable: variable.dataType.getRestLangLiteral
			JavaNativeVariable: (variable.dataType as JavaType).fqn
		}»'''
	
	def getRestTypeName(Type it) '''«dywaPkg.pkgEscape».rest.types.«name.escapeJava»'''
	
	override getTypeName(Type it) '''«originalType.pkg».«name.escapeJava»'''
	
	def getControlFlowSucc(DataFlowSource source) {
		source.getOutgoing(ControlFlow).head?.targetElement
	}
	
	def getIdentifier(DataFlowTarget sib) {
		switch sib {
			SIB: sib.label
			EndSIB: sib.branchName
		}
	}
	
	def getTypeName(EndSIB it) '''«branchName.toFirstUpper.escapeJava»Return'''
	def getImplTypeName(EndSIB it) '''«typeName»Impl'''
	
	def getExternalStoredTypeName(EndSIB it) '''«rootElement.typeName».«implTypeName»'''
	
	def getSuccSIB(AbstractBranch branch) {
		branch.dataFlowTargetSuccessors.head
	}
	
	override getVarName(OutputPort ddf) {
		'''«switch parent: ddf.container {
			StartSIB: "start"
			AbstractBranch: '''«parent.SIBPredecessors.head.label»«parent.name.toFirstUpper»'''
		}»«ddf.name.toFirstUpper»«ddf.id»'''.escapeJava
	}
	
	def getVarName(InputPort ddf) {
		'''«switch parent: ddf.container {
			SIB: '''«parent.label»«parent.name.toFirstUpper»'''
		}»«ddf.name.toFirstUpper»«ddf.id»'''.escapeJava
	}
	
	
	def getVarName(ProcessPlaceholderSIB it) {
		'''placeholder«label.toFirstUpper»«id»'''.escapeJava
	}
	
	override getIterVarName(AbstractIterateSIB iter) {
		'''counter«iter.label.toFirstUpper»«iter.id»'''.escapeJava
	}
	
	override getTrgtLangLiteral(PrimitiveOutputPort output) {
		if (output.isIsList && output.dataType != PrimitiveType.FILE) {
			return output.dataType.trgtLangLiteral
		}
		else {
			return output.dataType.trgtLangLiteral
		}
	}
	
	override getTrgtLangLiteral(PrimitiveInputPort input) {
//		if (input.isIsList) {
//			return input.dataType.trgtLangLiteral.toFirstUpper
//		}
//		else {
			return input.dataType.trgtLangLiteral

//		}
	}
	
	def getTrgtLangLiteral(info.scce.dime.data.data.PrimitiveType pType) {
		switch pType {
			case BOOLEAN: "java.lang.Boolean"
			case INTEGER: "java.lang.Long"
			case REAL: "java.lang.Double"
			case TEXT: "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE: '''«dywaPkg».util.FileReference'''
		}
	}
	
	def getRestLangLiteral(PrimitiveType pType) {
		switch pType {
			case BOOLEAN: "java.lang.Boolean"
			case INTEGER: "java.lang.Long"
			case REAL: "java.lang.Double"
			case TEXT: "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE: '''«dywaPkg».rest.util.FileReference'''
		}
	}
	
	override getTrgtLangLiteral(PrimitiveType pType) {
		switch pType {
			case BOOLEAN: "java.lang.Boolean"
			case INTEGER: "java.lang.Long"
			case REAL: "java.lang.Double"
			case TEXT: "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE: '''«dywaPkg».util.FileReference'''
		}
	}
	
	override getTrgtLangDeclaration(PrimitiveType pType) {
		switch pType {
			case BOOLEAN: "false"
			case INTEGER: "0l"
			case REAL: "0.0d"
			case TEXT: "new java.lang.String()"
			case TIMESTAMP: "new java.util.Date()"
			case FILE: "null"
		}
	}
	
	override getNullDeclaration(PrimitiveInputPort input) {
		if (input.isIsList) {
			"null"
		}
		else {
			input.dataType.nullDeclaration
		}
	}
	
	def getNullDeclaration(info.scce.dime.data.data.Attribute it) {
		if (isList)
			"new java.util.LinkedList<>()"
		else switch type {
			case info.scce.dime.data.data.PrimitiveType.BOOLEAN: "false"
			case info.scce.dime.data.data.PrimitiveType.INTEGER: "0l"
			case info.scce.dime.data.data.PrimitiveType.REAL: "0.0d"
			default: "null"
		}
	}
	
	override getNullDeclaration(PrimitiveType pType) {
		switch pType {
			case BOOLEAN: "false"
			case INTEGER: "0l"
			case REAL: "0.0d"
			case TEXT: "null"
			case TIMESTAMP: "null"
			case FILE: "null"
		}
	}
	
	def getGuardedProcessSIB(GuardContainer guardContainer) {
		guardContainer.guardedProcessSIBs.head
	}
	
	static def getSubProcess(GuardContainer guardContainer) {
		guardContainer.guardedProcessSIBs.head?.proMod
	}
	
	private static def String getInteractionInputsTypeNameInternal(Process interaction) '''InteractionInputsFor«interaction.modelName»'''
	static def String getInteractionInputsTypeNameJava(Process interaction) '''«interaction.interactionInputsTypeNameInternal.escapeJava»'''
	static def String getInteractionInputsTypeNameDart(Process interaction) '''«interaction.interactionInputsTypeNameInternal.escapeDart»'''
	
	static def String getSecurityInputsTypeName(Process security) '''SecurityInputsFor«security.modelName.escapeJava»'''
	
	static def String getInteractionTypeName(GuardContainer guardContainer) '''Interaction«guardContainer.subProcess.modelName.escapeJava»In«(guardContainer.container as Process).modelName.escapeJava»«guardContainer.id.escapeJava»'''

	static def String getContextTypeName(Process longrunning) '''«longrunning.modelName.escapeJava»Context'''
}
