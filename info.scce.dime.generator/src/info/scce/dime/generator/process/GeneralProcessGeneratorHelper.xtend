package info.scce.dime.generator.process

import info.scce.dime.data.data.Type
import info.scce.dime.generator.process.util.DIMEProcessSwitch
import info.scce.dime.process.process.AbstractIterateSIB
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DirectDataFlow
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.Variable

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

abstract class GeneralProcessGeneratorHelper extends DIMEProcessSwitch<CharSequence> {
	protected Process process;
	
	def generate(Process process) {
		this.process = process
		doSwitch(process)
	}
	
	static def getSimpleTypeName(Process process) {
		process.modelName.escapeJava.toFirstUpper
	}
	
	def getVarName(DirectDataFlow ddf) {
		ddf.source.varName
	}
	
	def String getVarName(OutputPort ddf)
	
	def String getTypeName(OutputPort outputPort)
	def String getTypeName(Variable variable)
	def String getTypeName(Type type)
	def String getTypeName(InputPort inputPort)
	def String getTypeName(InputStatic input)
	def String getTrgtLangLiteral(PrimitiveOutputPort output)
	def String getTrgtLangLiteral(PrimitiveInputPort input)
	def String getTrgtLangLiteral(PrimitiveType pType)
	def String getTrgtLangDeclaration(PrimitiveType pType)
	def String getNullDeclaration(PrimitiveInputPort input)
	def String getNullDeclaration(PrimitiveType pType)
	def String getIterVarName(AbstractIterateSIB iter)
	def String buildSetter(DataFlow outgoing, String value)
	def String buildSetterToVariable(Variable target, OutputPort outputPort, String value)
	
	def CharSequence getLiteralOrGetter(Input input)
	def CharSequence buildGetter(InputPort inputPort)
	
	def CharSequence buildSetterForSingleOutputPort(OutputPort outputPort, String value)
}