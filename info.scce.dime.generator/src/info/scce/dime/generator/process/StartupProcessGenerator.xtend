package info.scce.dime.generator.process

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.dad.dad.DAD
import info.scce.dime.generator.util.EclipseUtils
import info.scce.dime.process.process.Process
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.xtext.util.StringInputStream

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class StartupProcessGenerator implements IGenerator<Process> {
	DAD dad
	
	extension val DIMEGraphModelExtension = new DIMEGraphModelExtension

	new(DAD dad) {
		this.dad = dad
	}

	override void generate(Process model, IPath outlet, IProgressMonitor monitor) {
		monitor.beginTask("Generating Startup Process Model", 100)
		monitor.subTask("Generating Startup Bean Class")
		val StartupProcessGeneratorHelper helper = new StartupProcessGeneratorHelper()
		val IProject project = ProjectCreator::getProject(dad.eResource())
		val CharSequence contents = helper.generate(model)
		if (contents !== null) {
			
			val String fileName = escapeJava(
				'''Startup«model.getModelName()»'''.
					toString)
					val String upperCaseFileName = '''«Character::toUpperCase(fileName.charAt(0))»«fileName.substring(1)».java'''.
						toString
					var IFolder outputFolder = project.getFolder("target").getFolder("dywa-app").getFolder(
						"app-business").getFolder("target").getFolder("generated-sources")
					for (String pkgElement : BackendProcessGeneratorUtil::processPkg.split("\\.")) {
						outputFolder = outputFolder.getFolder(pkgElement)
					}
					for (String pkgElement : model.localPkg.pkgEscape.split("/")) {
						outputFolder = outputFolder.getFolder(pkgElement)
					}
					val IFile output = outputFolder.getFile(upperCaseFileName)
					try {
						EclipseUtils::mkdirs(outputFolder)
						if (!output.exists()) {
							output.create(new StringInputStream(contents.toString()), true, monitor)
						} else {
							output.setContents(new StringInputStream(contents.toString()), true, true, monitor)
						}
					} catch (CoreException e) {
						throw (new RuntimeException(e))
					}

				}
				monitor.worked(90)
				monitor.subTask("Writing Java Files")
			}
		}
		