package info.scce.dime.generator.rest

import info.scce.dime.data.data.AbstractType
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.FieldView
import info.scce.dime.generator.gui.rest.model.PortRepresentant
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.process.BackendProcessGeneratorUtil
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardProcessSIB
import java.io.File
import java.util.HashSet
import java.util.Map
import java.util.Set
import org.apache.commons.io.FileUtils
import org.eclipse.core.runtime.IPath

import static info.scce.dime.generator.util.DyWAExtension.*

import static extension info.scce.dime.generator.data.DataGeneratorHelper.*
import static extension info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator.*
import static extension info.scce.dime.generator.process.BackendProcessGeneratorUtil.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import java.util.Map.Entry
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import org.jooq.lambda.Seq
import java.util.List
import info.scce.dime.data.data.ExtensionAttribute

abstract class DyWAAbstractGenerator {
	static protected extension DyWAExtension = new DyWAExtension
	static protected extension DataExtension = DataExtension.instance
	static protected extension ProcessExtension = new ProcessExtension
	
	static def generate(String content, String packageName, String fileName, IPath targetDir,long lastModified) {
		
		val String typePath = packageName.replace('.', File.separatorChar)
		val File file = new File(targetDir.toFile(), typePath + fileName);
		if(file.exists && lastModified > 0) {
			if(file.lastModified>=lastModified) {
//				println('''«fileName» not gen «file.lastModified» > «lastModified»''')
				return;
			}
		}
		//println('''«fileName» is gen''')
		FileUtils.writeStringToFile(file, content);
	}
	
	
	static def Iterable<ComplexTypeView> fetchTypeViewsDeeply(Iterable<ComplexTypeView> tvs) {
		val Set<ComplexTypeView> cache = new HashSet<ComplexTypeView>()
		collectRecursive(tvs, cache);
		return cache;
	}
	
	private static def void collectRecursive(Iterable<ComplexTypeView> tvs, Set<ComplexTypeView> cache) {
		for (tv : tvs) {
//			if (!cache.contains(tv)) {
				cache.add(tv);
				tv.displayedFields.filter(ComplexFieldView).map([view]).filter(ComplexTypeView).collectRecursive(cache);
//			}
		}
	}


	// only reasonable for complex views, so any ClassCastException indicates a semantic error
	static def generateTOName(TypeView typeView)
		'''«(typeView as BaseComplexTypeView).typeName.escapeJava»'''


	// only reasonable for complex views, so any ClassCastException indicates a semantic error
	static def generateTOName(FieldView fieldView)
		'''«((fieldView as ComplexFieldView).view as BaseComplexTypeView).typeName.escapeJava»'''
	
	static def generateRestTOName(TypeView typeView, String packageName) '''
		«IF typeView instanceof PrimitiveTypeView»
			«generateRestTOName(typeView, packageName)»
		«ELSE»
			«generateRestTOName(typeView as BaseComplexTypeView, packageName)»
		«ENDIF»
	'''
	static def generateInnerRestTOName(TypeView typeView, String packageName) '''
		«IF typeView instanceof PrimitiveTypeView»
			«generateInnerRestTOName(typeView, packageName)»
		«ELSE»
			«generateInnerRestTOName(typeView as BaseComplexTypeView, packageName)»
		«ENDIF»
	'''
	static def generateRestTOName(PrimitiveTypeView typeView, String packageName)
		'''«evaluateList(typeView, generateInnerRestTOName(typeView, packageName).toString)»'''
	static def generateInnerRestTOName(PrimitiveTypeView typeView, String packageName)
		'''«typeView.primitiveType.getRestLiteral()»'''
	static def generateRestTOName(BaseComplexTypeView typeView, String packageName)
		'''«evaluateList(typeView, generateInnerRestTOName(typeView, packageName).toString)»'''
	static def generateInnerRestTOName(BaseComplexTypeView typeView, String packageName)
		'''«packageName».rest.types.«typeView.type.RESTTOSimpleName»'''
		
	static def generateRestTOName(FieldView fieldView, String packageName,boolean considerList) '''
	«IF fieldView instanceof PrimitiveFieldView»
		«generateRestTOName(fieldView, packageName,considerList)»
	«ELSE»
		«generateRestTOName(fieldView as ComplexFieldView, packageName)»
	«ENDIF»
	'''
	static def generateRestTOName(FieldView fieldView, String packageName) '''
		«fieldView.generateRestTOName(packageName,true)»
	'''
	static def generateInnerRestTOName(FieldView fieldView, String packageName) '''
		«IF fieldView instanceof PrimitiveFieldView»
			«generateInnerRestTOName(fieldView, packageName)»
		«ELSE»
			«generateInnerRestTOName(fieldView as ComplexFieldView, packageName)»
		«ENDIF»
	'''
	
	static def generateRestTOName(PrimitiveFieldView fieldView, String packageName,boolean considerList)
		'''«IF considerList»«evaluateList(fieldView, generateInnerRestTOName(fieldView, packageName).toString)»«ELSE»«generateInnerRestTOName(fieldView, packageName)»«ENDIF»'''
		
	static def generateRestTOName(PrimitiveFieldView fieldView, String packageName)
		'''«fieldView.generateRestTOName(packageName,true)»'''
	
	static def generateInnerRestTOName(PrimitiveFieldView fieldView, String packageName)
		'''«fieldView.primitiveType.getRestLiteral()»'''
	
	static def generateRestTOName(ComplexFieldView fieldView, String packageName)
		'''«generateRestTOName(fieldView.view as ComplexTypeView, packageName)»'''
	
	static def generateInnerRestTOName(ComplexFieldView fieldView, String packageName)
		'''«generateInnerRestTOName(fieldView.view as ComplexTypeView, packageName)»'''
		

	static def generateDywaTOName(TypeView typeView, String packageName) '''
		«IF typeView instanceof PrimitiveTypeView»
			«generateDywaTOName(typeView, packageName)»
		«ELSEIF typeView instanceof BaseComplexTypeView»
			«generateDywaTOName(typeView as BaseComplexTypeView, packageName)»
		«ELSE»
			«generateDywaTOName(typeView as ComplexTypeView, packageName)»
		«ENDIF»
	'''
	static def generateInnerDywaTOName(TypeView typeView, String packageName) '''
		«IF typeView instanceof PrimitiveTypeView»
			«generateInnerDywaTOName(typeView, packageName)»
		«ELSEIF typeView instanceof BaseComplexTypeView»
			«generateInnerDywaTOName(typeView as BaseComplexTypeView, packageName)»
		«ELSE»
			«generateInnerDywaTOName(typeView as ComplexTypeView, packageName)»
		«ENDIF»
	'''
	static def generateDywaTOName(PrimitiveTypeView typeView, String packageName)
		'''«evaluateList(typeView, generateInnerDywaTOName(typeView, packageName).toString)»'''
	static def generateInnerDywaTOName(PrimitiveTypeView typeView, String packageName)
		'''«typeView.primitiveType.getDyWaLiteral()»'''
	static def generateDywaTOName(BaseComplexTypeView typeView, String packageName)
		'''«evaluateList(typeView,  generateInnerDywaTOName(typeView, packageName).toString)»'''
	static def generateInnerDywaTOName(ComplexTypeView typeView, String packageName)
		'''«DyWASelectiveDartGenerator.dataModelType(typeView.data).dyWATypeName»'''
	static def generateInnerDywaTOName(Type t, String packageName)
		'''«DyWASelectiveDartGenerator.dataModelType(t).dyWATypeName»'''
	static def generateInnerDywaTOName(BaseComplexTypeView typeView, String packageName)
		'''«DyWASelectiveDartGenerator.dataModelType(typeView.type).dyWATypeName»'''

		
	static def generateDywaTOName(FieldView fieldView, String packageName) '''
		«IF fieldView instanceof PrimitiveFieldView»
			«generateDywaTOName(fieldView, packageName)»
		«ELSE»
			«generateDywaTOName(fieldView as ComplexFieldView, packageName)»
		«ENDIF»
	'''
	static def generateInnerDywaTOName(FieldView fieldView, String packageName) '''
		«IF fieldView instanceof PrimitiveFieldView»
			«generateInnerDywaTOName(fieldView, packageName)»
		«ELSE»
			«generateInnerDywaTOName(fieldView as ComplexFieldView, packageName)»
		«ENDIF»
	'''
	static def generateDywaTOName(PrimitiveFieldView fieldView, String packageName)
		'''«evaluateList(fieldView, generateInnerDywaTOName(fieldView, packageName).toString)»'''
	static def generateInnerDywaTOName(PrimitiveFieldView fieldView, String packageName)
		'''«fieldView.primitiveType.getDyWaLiteral()»'''
	static def generateDywaTOName(ComplexFieldView fieldView, String packageName)
		'''«evaluateList(fieldView, generateInnerDywaTOName(fieldView, packageName).toString)»'''
	static def generateInnerDywaTOName(ComplexFieldView fieldView, String packageName)
		'''«generateInnerDywaTOName(fieldView.view as ComplexTypeView, packageName)»'''


	static def generateBasicTOName(TypeView typeView, String packageName) '''
		«IF typeView instanceof PrimitiveTypeView»
			«generateBasicTOName(typeView, packageName)»
		«ELSE»
			«generateBasicTOName(typeView as BaseComplexTypeView, packageName)»
		«ENDIF»
	'''
	static def generateInnerBasicTOName(TypeView typeView, String packageName) '''
		«IF typeView instanceof PrimitiveTypeView»
			«generateInnerBasicTOName(typeView, packageName)»
		«ELSE»
			«generateInnerBasicTOName(typeView as BaseComplexTypeView, packageName)»
		«ENDIF»
	'''
	static def generateBasicTOName(PrimitiveTypeView typeView, String packageName)
		'''«evaluateList(typeView, generateInnerBasicTOName(typeView, packageName).toString)»'''
	static def generateInnerBasicTOName(PrimitiveTypeView typeView, String packageName)
		'''«typeView.primitiveType.getRestLiteral()»'''
	static def generateBasicTOName(BaseComplexTypeView typeView, String packageName)
		'''«evaluateList(typeView,  generateInnerBasicTOName(typeView, packageName).toString)»'''
	static def generateInnerBasicTOName(BaseComplexTypeView typeView, String packageName)
		'''«packageName».rest.types.«typeView.type.name.escapeJava»'''
		
	static def generateBasicTOName(FieldView fieldView, String packageName) '''
		«IF fieldView instanceof PrimitiveFieldView»
			«generateBasicTOName(fieldView, packageName)»
		«ELSE»
			«generateBasicTOName(fieldView as ComplexFieldView, packageName)»
		«ENDIF»
	'''
	static def generateInnerBasicTOName(FieldView fieldView, String packageName) '''
		«IF fieldView instanceof PrimitiveFieldView»
			«generateInnerBasicTOName(fieldView, packageName)»
		«ELSE»
			«generateInnerBasicTOName(fieldView as ComplexFieldView, packageName)»
		«ENDIF»
	'''
	static def generateBasicTOName(PrimitiveFieldView fieldView, String packageName)
		'''«evaluateList(fieldView, generateInnerBasicTOName(fieldView, packageName).toString)»'''
	static def generateInnerBasicTOName(PrimitiveFieldView fieldView, String packageName)
		'''«fieldView.primitiveType.getRestLiteral()»'''
	static def generateBasicTOName(ComplexFieldView fieldView, String packageName)
		'''«evaluateList(fieldView, generateInnerBasicTOName(fieldView, packageName).toString)»'''
	static def generateInnerBasicTOName(ComplexFieldView fieldView, String packageName)
		'''«packageName».rest.types.«fieldView.field.complexDataType.name.escapeJava»'''
		
		
//	protected def generateTOName(TypeView typeView, String packageName)
//		'''«packageName».rest.types.«typeView.name.escape»_«typeView.id.escape»'''
//	
//	protected def generateTOName(FieldView fieldView, String packageName) '''
//		«IF fieldView.field.realAttribute instanceof ComplexAttribute»
//			«packageName».rest.types.«generateTOName((fieldView as ComplexFieldView))»
//		«ELSE»
//			«getRESTLiteral((fieldView.field.realAttribute as PrimitiveAttribute).dataType, packageName)»
//		«ENDIF»
//	'''
//	
//	protected def originalType(FieldView fieldView, String packageName) '''
//		«IF fieldView.field.realAttribute instanceof ComplexAttribute»
//			«packageName».entity.«originalType(fieldView)»
//		«ELSE»
//			«getJavaLiteral((fieldView.field.realAttribute as PrimitiveAttribute).dataType, packageName)»
//		«ENDIF»
//	'''
	
	static def originalType(FieldView fieldView)
		'''«(fieldView.field.originalAttribute as ComplexAttribute).dataType.name.escapeJava»'''

//	protected def attributeType(FieldView fieldView, String packageName) '''
//		«IF fieldView.field.isList»
//			java.util.List<
//		«ENDIF»
//		«IF fieldView.field instanceof PrimitiveAttribute»
//			«getRESTLiteral((fieldView.field.realAttribute as PrimitiveAttribute).dataType, packageName)»
//		«ELSE»	
//			«generateTOName(fieldView, packageName)»
//		«ENDIF»
//		«IF fieldView.field.isList»
//			>
//		«ENDIF»
//	'''
//
//	protected def attributeType(CompoundFieldView fieldView, String packageName) '''
//		«IF fieldView.isList»
//			java.util.List<
//		«ENDIF»
//		«IF fieldView instanceof PrimitveCompoundFieldView»
//			«getJavaLiteral(fieldView.pt, packageName)»
//		«ELSE»	
//			«generateTOName((fieldView as ComplexCompoundFieldView).typeView, packageName)»
//		«ENDIF»
//		«IF fieldView.isList»
//			>
//		«ENDIF»
//	'''
//
//	protected def originalAttributeType(CompoundFieldView fieldView, String packageName) '''
//		«IF fieldView.isList»
//			java.util.List<
//		«ENDIF»
//		«IF fieldView instanceof PrimitveCompoundFieldView»
//			«getJavaLiteral(fieldView.pt, packageName)»
//		«ELSE»	
//			«packageName».entity.«generateRestTOName((fieldView as ComplexCompoundFieldView).typeView)»
//		«ENDIF»
//		«IF fieldView.isList»
//			>
//		«ENDIF»
//	'''

	public static def evaluateList(TypeView typeView, String content) '''
		«IF typeView.isList
			»java.util.List<«
		ENDIF
		»«content»«
		IF typeView.isList
			»>«
		ENDIF»
	'''

	public static def evaluateList(FieldView typeView, String content) '''
		«IF typeView.isList
			»java.util.List<«
		ENDIF
		»«content»«
		IF typeView.isList
			»>«
		ENDIF»
	'''
	
	public static def getJavaLiteral(PrimitiveType pType){
		if(pType == null) {
			throw new IllegalStateException()
		}
		switch(pType){
			case BOOLEAN: "java.lang.Boolean"
			case INTEGER: "java.lang.Long"
			case REAL: "java.lang.Double"
			case TEXT: "java.lang.String"
			case TIMESTAMP: "java.util.Date"
			case FILE: "FileReference"
		}
	}
	
	public static def getDyWaLiteral(PrimitiveType pType){
		switch(pType){
			case FILE: "de.ls5.dywa.generated.util.FileReference"
			default: getJavaLiteral(pType)
		}
		
	}
	
	public static def getRestLiteral(PrimitiveType pType){
		switch(pType){
			case FILE: "de.ls5.dywa.generated.rest.util.FileReference"
			default: getJavaLiteral(pType)
		}
	}
	
	static def copyParametersIntoLocalVariables(Iterable<TypeView> typeViews, String packageName){
		val typeViewNameMap = typeViews.groupBy[n|n.name]
		copyParametersIntoLocalVariables(typeViewNameMap,"ctx",packageName, false, true)
	}
	
	static def copyParametersIntoLocalVariables(Map<String,List<TypeView>> typeViews, String getVariableName, String packageName) {
		return copyParametersIntoLocalVariables(typeViews, getVariableName, packageName, false, false)
	}
	
	static def copyParametersIntoLocalVariables(Map<String,List<TypeView>> typeViews, String getVariableName, String packageName, boolean serverSideSource) {
		return copyParametersIntoLocalVariables(typeViews, getVariableName, packageName, serverSideSource, false)
	}
	
	static def getSuperSelectives(Iterable<? extends TypeView> selectives) {
		if(selectives.length == 1) {
			return selectives.head
		}
		
		if(selectives.filter(ComplexTypeView).size > 1) {
			val selective = new ComplexTypeView(selectives.filter(ComplexTypeView).get(0))
				//select union type
				selectives.filter(ComplexTypeView).forEach[s|{
					if(!s.type.equals(selective.type)) {
						
						val superTypes = s.type.superTypes
						val equalSuperTypes = selective.type.superTypes.filter[superTypes.contains(it)].toList
						if(!equalSuperTypes.empty) {
							selective.type = equalSuperTypes.get(0)
						}
						//find best super type
						equalSuperTypes.forEach[est|{
							if(selective.type.superTypes.length < est.superTypes.length) {
								selective.type = est
							}
						}]
					}
				}]
			return selective
		}
		null
	}

	static def copyParametersIntoLocalVariables(Map<String,List<TypeView>> typeViews, String getVariableName, String packageName, boolean serverSideSource, boolean considerSubTypes) '''
		«val selectiveCache = SelectiveCache.instance»
		«FOR typeViewCompound: typeViews.entrySet»
			«val name = typeViewCompound.key»
			«val superCTV = typeViewCompound.value.superSelectives»
			«val firstCompond = typeViewCompound.value.get(0)»
			«val dywaTOName = superCTV.generateDywaTOName(packageName)»
				final «dywaTOName» «name.escapeJava»;
				«IF superCTV instanceof BaseComplexTypeView»
					«val complexTV = superCTV as BaseComplexTypeView»
					«val isEnum = (complexTV.type instanceof EnumType)»
					«val isAbstract = (complexTV.type instanceof AbstractType)»
					«IF complexTV.isList»
						«name.escapeJava» = new java.util.ArrayList<>();
						
						if («getVariableName».get«name.toFirstUpper.escapeJava»() != null) {
							for (final «superCTV.generateInnerBasicTOName(packageName)» o: «getVariableName».get«name.toFirstUpper.escapeJava»()) {
							«IF !isEnum»
								// create new object
								if (o.getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
									«IF serverSideSource»
										throw new java.lang.IllegalArgumentException("The server never returns objects with id '-1'");
									«ELSE»
										«FOR subComplexTV:typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + o.getClass()); }'''»
										if(o instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
											«IF subComplexTV.type.isAbstract && !considerSubTypes»
												throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
											«ELSE»
												«subComplexTV.preprocessObject(packageName, '''o''', name)»
												final long id;
												«IF considerSubTypes»
													«FOR st : subComplexTV.type.knownSubTypes.sortTopologically SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + o.getClass()); }'''»
														if(o instanceof «st.RESTTOName») {
															«IF st.isAbstract»
																throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
															«ELSE»
																id = «st.RESTControllerSimpleName».create(dywaName);
															«ENDIF»
														}
													«ENDFOR»
												«ELSE»
													id = «subComplexTV.type.RESTControllerSimpleName».create(dywaName);
												«ENDIF»
												o.setDywaId(id);
												
												//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
												«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)o);
												«name.escapeJava».add(«subComplexTV.type.controllerSimpleName».read(o.getDywaId()));
											«ENDIF»
											}
											«ENDFOR»
									«ENDIF»
								}
								// transient object
								else if (o.getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
									«FOR subComplexTV:typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + o.getClass()); }'''»
										if(o instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
											«IF subComplexTV.type.isAbstract && !considerSubTypes»
												throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
											«ELSE»
												«subComplexTV.preprocessObject(packageName, "o", name)»
			
												«subComplexTV.createTransientObject(packageName, "o")»

												//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
												«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)o, transientObject);
												«name.escapeJava».add(transientObject);
											«ENDIF»
										}
										«ENDFOR»
								}
								// regular object
							«ELSE»
								// for enum types ignore non-persisted objects
								if (o.getDywaId() < 0) {
									«name.escapeJava».add(null);
								}
							«ENDIF»
								else {
									«FOR subComplexTV:typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else "»
										if(o instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
											«IF !serverSideSource»
												//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
												«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)o);
											«ENDIF»
											«name.escapeJava».add(«subComplexTV.type.controllerSimpleName».read(o.getDywaId()));
										}
									«ENDFOR»
								}
							}
						}
					«ELSE»
						if («getVariableName».get«name.toFirstUpper.escapeJava»() != null) {
						«IF !isEnum»
							// create new object
							if («getVariableName».get«name.toFirstUpper.escapeJava»().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
								«IF serverSideSource»
									throw new java.lang.IllegalArgumentException("The server never returns objects with id '-1'");
								«ELSE»
									«FOR subComplexTV:typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else " AFTER  ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «getVariableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
											if(«getVariableName».get«name.toFirstUpper.escapeJava»() instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
												«IF subComplexTV.type.isAbstract && !considerSubTypes»
													throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
												«ELSE»
													«subComplexTV.preprocessObject(packageName, '''«getVariableName».get«name.toFirstUpper.escapeJava»()''', name)»
													final long id;
													«IF considerSubTypes»
														«FOR st : subComplexTV.type.knownSubTypes.sortTopologically SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «getVariableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
															if(«getVariableName».get«name.toFirstUpper.escapeJava»() instanceof «st.RESTTOName») {
															«IF st.isAbstract»
																throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
															«ELSE»
																id = «st.RESTControllerSimpleName».create(dywaName);
															«ENDIF»
															}
														«ENDFOR»
													«ELSE»
														id = «subComplexTV.type.RESTControllerSimpleName».create(dywaName);
													«ENDIF»
													«getVariableName».get«name.toFirstUpper.escapeJava»().setDywaId(id);
													//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
													«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)«getVariableName».get«name.toFirstUpper.escapeJava»());
													«name.escapeJava» = («dywaTOName») «subComplexTV.type.controllerSimpleName».read(«getVariableName».get«name.toFirstUpper.escapeJava»().getDywaId());
												«ENDIF»
											}
									«ENDFOR»
								«ENDIF»
							}
							// transient object
							else if («getVariableName».get«name.toFirstUpper.escapeJava»().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
								«FOR subComplexTV:typeViewCompound.value.filter(BaseComplexTypeView) SEPARATOR "else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «getVariableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
									if(«getVariableName».get«name.toFirstUpper.escapeJava»() instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
										«IF subComplexTV.type.isAbstract && !considerSubTypes»
											throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
										«ELSE»
											«subComplexTV.preprocessObject(packageName, '''«getVariableName».get«name.toFirstUpper.escapeJava»()''', name)»
											«subComplexTV.createTransientObject(packageName, '''«getVariableName».get«name.toFirstUpper.escapeJava»()''')»

											//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
											«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)«getVariableName».get«name.toFirstUpper.escapeJava»(), transientObject);
											«name.escapeJava» = («dywaTOName») transientObject;
										«ENDIF»
									}
								«ENDFOR»
							}
							// regular object
						«ELSE»
							// for enum types ignore non-persisted objects
							if («getVariableName».get«name.toFirstUpper.escapeJava»().getDywaId() < 0) {
								«name.escapeJava» = null;
							}
						«ENDIF»
							else {
								«FOR tvCompound:typeViewCompound.value SEPARATOR "else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «getVariableName».get«name.toFirstUpper.escapeJava»().getClass()); }'''»
									«val subComplexTV = tvCompound as BaseComplexTypeView»
									if(«getVariableName».get«name.toFirstUpper.escapeJava»() instanceof «subComplexTV.generateInnerBasicTOName(packageName)») {
										«IF !serverSideSource»
											//update_«DyWASelectiveDartGenerator.getSelectiveNameJava(subComplexTV)»
											«subComplexTV.type.RESTControllerSimpleName».update_«DyWASelectiveDartGenerator.getSelectiveNameJava(selectiveCache.getRepresentative(subComplexTV))»((«subComplexTV.generateInnerBasicTOName(packageName)»)«getVariableName».get«name.toFirstUpper.escapeJava»());
										«ENDIF»
										«name.escapeJava» = («dywaTOName») «subComplexTV.type.controllerSimpleName».read(«getVariableName».get«name.toFirstUpper.escapeJava»().getDywaId());
									}
								«ENDFOR»
							}
						
						}
						else {
							«name.escapeJava» = null;
						}
					«ENDIF»
				«ELSEIF firstCompond instanceof PrimitiveTypeView»
					«val primitiveTV = firstCompond as PrimitiveTypeView»
					«IF primitiveTV.primitiveType == PrimitiveType.FILE»
						«IF primitiveTV.isList»
							«name.escapeJava» = new java.util.LinkedList<>();
							
							if («getVariableName».get«name.toFirstUpper.escapeJava»() != null) {
								for (final «primitiveTV.generateInnerBasicTOName(packageName)» o: «getVariableName».get«name.toFirstUpper.escapeJava»()) {
									«name.escapeJava».add(domainFileController.getFileReference(o.getDywaId()));
								}
							}
						«ELSE»
							if («getVariableName».get«name.toFirstUpper.escapeJava»() != null) {
								«name.escapeJava» = domainFileController.getFileReference(«getVariableName».get«name.toFirstUpper.escapeJava»().getDywaId());
							}
							else {
								«name.escapeJava» = null;
							}
						«ENDIF»
					«ELSE»
					 	«name.escapeJava» = «getVariableName».get«name.toFirstUpper.escapeJava»();
					 «ENDIF»
				«ENDIF»
		«ENDFOR»
	'''
	
	private static def preprocessObject(BaseComplexTypeView tv, String packageName, CharSequence attr, CharSequence name) '''
		final java.lang.String dywaName;
		if («attr».getDywaName() == null || «attr».getDywaName().isEmpty()) {
			dywaName = "«name»";
		} else {
			dywaName = «attr».getDywaName();
		}
	'''

	private static def createTransientObject(BaseComplexTypeView tv, String packageName, CharSequence name) {
		var subTypes = tv.type.knownSubTypes.sortTopologically

		if (subTypes.isEmpty) {
			'''final «tv.generateInnerDywaTOName(packageName)» transientObject = «tv.type.controllerSimpleName».createTransient(dywaName);'''
		}
		else {
			'''
				final «tv.generateInnerDywaTOName(packageName)» transientObject;
				«FOR t : subTypes SEPARATOR " else " AFTER ''' else { throw new java.lang.IllegalArgumentException("Unexpected type " + «name».getClass()); }'''»
					if («name» instanceof «t.RESTTOName») {
						«IF t.isAbstract»
							throw new java.lang.IllegalArgumentException("Cannot instantiate abstract type");
						«ELSE»
							transientObject = «t.controllerSimpleName».createTransient(dywaName);
						«ENDIF»
					}
				«ENDFOR»
			'''
		}
	}

	public static def generateCheckForInteraction(GuardContainer guardContainer) '''
		private boolean checkInteractionFor«guardContainer.id.escapeJava»(final long interactionId) {
			
			final «"Interaction".processContextsTypeName» interaction = interactionController.read(interactionId);
			
			if (interaction instanceof «guardContainer.interactionTypeName.processContextsTypeName») {
				final «guardContainer.interactionTypeName.processContextsTypeName» currInteraction = («guardContainer.interactionTypeName.processContextsTypeName») interaction;
				
				// TODO: Use correct interaction inputs
				if (!«guardContainer.rootElement.id.escapeJava».checkInteraction«guardContainer.id.escapeJava»(currInteraction, currInteraction.getinteractionInputs())) {
					return false;
				}
				
				return true;
			}
			else {
				return false;
			}
		}
	'''
	
//	public static def generateCheckForGuardProcess(GuardContainer guardContainer, Map<GuardProcessSIB, Map<Input, TypeView>> guardProcessMap, String securityInputClass) '''
	public static def generateCheckForGuardProcess(Map<GuardProcessSIB, Map<PortRepresentant, TypeView>> guardProcessMap, String methodIdentifier, String securityInputClass) '''
		private boolean checkGuardContainersFor«methodIdentifier»(«securityInputClass» secCtx) {
			
			«val extension BackendProcessGeneratorUtil = new BackendProcessGeneratorUtil()»
			«FOR guardProcessEntry: guardProcessMap.entrySet»
				«val guardProcessSIB = guardProcessEntry.key»
				«val guardProcess = guardProcessSIB.securityProcess»
				«val guardProcessInputMap = guardProcessEntry.value»
				«val guardProcessInputNames = guardProcess.startSIB.outputs.map[name].sort»
				«val nameToInputMap = guardProcessEntry.value.keySet.toMap[name]»
				«val userTypeName = guardProcess.concreteUserType.dyWATypeName»
				{
					// inputs for «guardProcess.simpleTypeName»
					final «securityInputClass».«guardProcessSIB.label.toFirstUpper.escapeJava»«guardProcessSIB.id.escapeJava» secInputs = secCtx.get«guardProcessSIB.label.escapeJava»«guardProcessSIB.id.escapeJava»();
					
					«userTypeName» currentUser = this.currentUserController.getCurrentUser(«userTypeName».class);
					«guardProcess.typeName».«guardProcess.simpleTypeName»Result result = this.«guardProcess.simpleTypeName».execute(
							«guardProcessInputNames.map[ x | 
								if ("currentUser".equals(x)) {
									'''currentUser'''
								}
								else {
									val input = nameToInputMap.get(x);
									'''«generateSecurityProcessInputLookup(input, guardProcessInputMap.get(input))» '''
								}							
							].join(", ")»
						);
					
					if (!"granted".equals(result.getBranchName())) {
						return false;
					}
				}
			«ENDFOR»
			
			return true;
		}
	'''
	
	static def generateSecurityProcessInputLookup(PortRepresentant input, TypeView typeView) '''
		«IF typeView == null» ««« == null if static»
			«input.resolveStaticValue»
		«ELSE»
			«val isComplex = (typeView instanceof ComplexTypeView)»
			«val isFile = if(typeView instanceof PrimitiveTypeView) {typeView.primitiveType == PrimitiveType.FILE} else false»
			«val lookupRequired = isComplex || isFile»
			
			«IF lookupRequired»
				«IF typeView.isList»
					secInputs.get«typeView.name.escapeJava»()
						.stream()
						.map(«typeView.generateInnerBasicTOName(DyWAExtension.dywaPkg)»::getDywaId)
						.map(«
							IF isFile
								»domainFileController::getFileReference«
							ELSE
								»«(typeView as ComplexTypeView).type.name.escapeJava»Controller::read«
							ENDIF
						»)
						.collect(java.util.stream.Collectors.toList())
				«ELSE»
					«IF isFile
						»domainFileController.getFileReference«
					ELSE
						»«(typeView as ComplexTypeView).type.name.escapeJava»Controller.read«
					ENDIF»
					(secInputs.get«typeView.name.escapeJava»().getDywaId())
				«ENDIF»
			«ELSE»
				secInputs.get«typeView.name.escapeJava»()
			«ENDIF»
		«ENDIF»
	'''
	
	static def generateInteractableInputs(Map.Entry<PortRepresentant, TypeView> entry) '''
		«val key = entry.key»
		«val value = entry.value»
		«IF value == null» ««« == null if static»
			«key.resolveStaticValue»
		«ELSE»
			«value.name.escapeJava»
		«ENDIF»
	'''
	
	static def resolveStaticValue(PortRepresentant input) '''
		«IF input.isStaticBoolean»
			«input.staticBooleanValue»
		«ELSEIF input.isStaticInt»
			«input.staticIntValue»l
		«ELSEIF input.isStaticReal»
			«input.staticRealValue»d
		«ELSEIF input.isStaticText»
			"«input.staticTextValue»"
		«ELSEIF input.isStaticTimestamp»
			new java.util.Date(«input.staticTimestampValue»l)
		«ELSE»
			null
		«ENDIF»
	'''
	
	
	public static def generateSecurityInputClass(Map<GuardProcessSIB, Map<PortRepresentant, TypeView>> inputMap, String securityInputPackage, String securityInputClass, String packageName) '''
		package «securityInputPackage»;
		
		public class «securityInputClass» {
			
			«FOR entry : inputMap.entrySet» 
				«val gcs = entry.key»
				«val inputs = entry.value»
				
				private «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» «gcs.label.escapeJava»«gcs.id.escapeJava»;
					
				@com.fasterxml.jackson.annotation.JsonProperty("«gcs.label.escapeString»«gcs.id.escapeString»")
				public «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» get«gcs.label.escapeJava»«gcs.id.escapeJava»() {
					return this.«gcs.label.escapeJava»«gcs.id.escapeJava»;
				}

				@com.fasterxml.jackson.annotation.JsonProperty("«gcs.label.escapeString»«gcs.id.escapeString»")
				public void set«gcs.label.escapeJava»«gcs.id.escapeJava»(final «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» «gcs.label.escapeJava»«gcs.id.escapeJava») {
					this.«gcs.label.escapeJava»«gcs.id.escapeJava» = «gcs.label.escapeJava»«gcs.id.escapeJava»;
				}
				
				static class «gcs.label.toFirstUpper.escapeJava»«gcs.id.escapeJava» {
					«FOR tv : inputs.values.filter[x | x != null].toList» ««« == null if static
						private «generateBasicTOName(tv, packageName)» «tv.name.escapeJava»;
							
						@com.fasterxml.jackson.annotation.JsonProperty("«tv.name.escapeString»")
						public «generateBasicTOName(tv, packageName)» get«tv.name.escapeJava»() {
							return this.«tv.name.escapeJava»;
						}
						
						@com.fasterxml.jackson.annotation.JsonProperty("«tv.name.escapeString»")
						public void set«tv.name.escapeJava»(final «generateBasicTOName(tv, packageName)» «tv.name.escapeJava») {
							this.«tv.name.escapeJava» = «tv.name.escapeJava»;
						}
					«ENDFOR»
				}
			«ENDFOR»
		}
	'''
	
	public static def generateInputWrapperClassForSecuredProcess(String packageName, String wrapperName, String inputClass,
		String inputName, String securityClassForInteractable, String securityClassForInteraction
	) '''
		package «packageName»;
		
		public class «wrapperName» {
				
				«IF inputClass != null»
				private «packageName».«inputClass» «inputName»;
				«ENDIF»
				«IF securityClassForInteractable != null»
				private «packageName».«securityClassForInteractable» securityInputsForInteractable;
				«ENDIF»
				«IF securityClassForInteraction != null»
				private «packageName».«securityClassForInteraction» securityInputsForInteraction;
				«ENDIF»
				
				«IF inputClass != null»
				@com.fasterxml.jackson.annotation.JsonProperty("«inputName»")
				public «packageName».«inputClass» get«inputName.toFirstUpper»() {
					return this.«inputName»;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("«inputName»")
				public void set«inputName.toFirstUpper»(final «packageName».«inputClass» «inputName») {
					this.«inputName» = «inputName»;
				}
				«ENDIF»

				«IF securityClassForInteractable !== null»
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteractable")
				public «packageName».«securityClassForInteractable» getSecurityInputsForInteractable() {
					return this.securityInputsForInteractable;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteractable")
				public void setSecurityInputsForInteractable(final «packageName».«securityClassForInteractable» securityInputsForInteractable) {
					this.securityInputsForInteractable = securityInputsForInteractable;
				}
				«ENDIF»

				«IF securityClassForInteraction !== null»
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteraction")
				public «packageName».«securityClassForInteraction» getSecurityInputsForInteraction() {
					return this.securityInputsForInteraction;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("securityInputsForInteraction")
				public void setSecurityInputsForInteraction(final «packageName».«securityClassForInteraction» securityInputsForInteraction) {
					this.securityInputsForInteraction = securityInputsForInteraction;
				}
				«ENDIF»
		}
	'''
	
	
	protected static def renderDeclaringRestType(ComplexTypeView typeView, FieldView fieldView)
		'''«restTypePkg».«typeView.getDeclaringType(fieldView).name.escapeJava»'''
	
	protected static def renderDeclaringDywaType(ComplexTypeView typeView, FieldView fieldView)
		'''«typeView.getDeclaringType(fieldView).dyWATypeName»'''
	
	// FIXME move to GUIExtension
	protected static def getDeclaringType(ComplexTypeView typeView, FieldView fieldView) {
		val data = fieldView.data
		switch (data) {
			PrimitiveAttribute: (data.container as Type)
			ComplexAttribute: (data.container as Type)
			ExtensionAttribute: (data.container as Type)
			info.scce.dime.gui.gui.PrimitiveAttribute: (data.attribute.container as Type)
			info.scce.dime.gui.gui.ComplexAttribute: (data.attribute.container as Type)
			info.scce.dime.gui.gui.ExtensionAttribute: (data.attribute.container as Type)
			ComplexVariable: {
				if (!data.complexVariablePredecessors.isEmpty) {
					val dataDataType = data.dataType.originalType
					val effectiveType = data.complexVariablePredecessors.get(0).dataType
					val superTypes = #[effectiveType] + effectiveType.superTypes
					val type = superTypes.findFirst[
						attributes.map[originalAttribute].filter[isComplex].exists[
							name == data.name && complexDataType.originalType == dataDataType
						]
					]
					if (type === null) {
						throw new IllegalStateException("Declaring type not calculatable: " + data)
					}
					type
				}
				else {
					throw new IllegalStateException("Top level variable declaring type not calculatable: " + data)
				}
			}
			default: typeView.type
		}.originalType
	}
	
	// FIXME move to GUIExtension
	protected static def needRuntimeCheck(ComplexTypeView typeView, FieldView fieldView) {
		!typeView.type.originalType.equals(typeView.getDeclaringType(fieldView))
	}
	
	static def getPossibleConcreteSubTypes(ComplexTypeView typeView) {
		typeView.displayedFields
			.filter[needRuntimeCheck(typeView, it)]
			.map[getDeclaringType(typeView, it)]
			.filter[!(it instanceof AbstractType)]
			.toSet
	}
	
	static def renderCacheLookup(ComplexTypeView view, String source, String target) '''
		«view.generateInnerBasicTOName(DyWAExtension.dywaPkg)» «target» = objectCache.getRestTo(«source»);

		if («target» == null) {
			«target» = «view.type.RESTTOName».fromDywaEntity(«source», objectCache);
		}

		if (!objectCache.containsSelective(«target», "«view.selectiveNameJava»")) {
			«restTypePkg».«view.selectiveNameJava».copy(«source», «target», objectCache);
		}
	'''
	
	static def typeRESTControllerInjection(Type t)
	'''
	@javax.inject.Inject
	private «t.RESTControllerName» «t.RESTControllerSimpleName»;
	@javax.inject.Inject
	private «t.controllerTypeName» «t.controllerSimpleName»;
	'''
	
}