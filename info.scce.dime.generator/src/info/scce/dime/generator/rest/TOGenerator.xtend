package info.scce.dime.generator.rest

import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.generator.data.DataGeneratorHelper
import java.util.ArrayList
import java.util.List
import org.eclipse.core.runtime.IPath

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import static extension info.scce.dime.generator.data.DataGeneratorHelper.*
import info.scce.dime.data.data.InheritorType
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Inheritance
import info.scce.dime.data.data.UserType
import info.scce.dime.data.data.EnumType

import static extension info.scce.dime.data.helper.DataExtension.*
import info.scce.dime.generator.util.DyWAExtension
import static extension info.scce.dime.generator.util.DyWAExtension.*
import info.scce.dime.data.data.TypeAttribute

class TOGenerator extends DyWAAbstractGenerator {

	def void generate(List<Data> models, IPath outlet) {
		
		val targetDir = outlet.append("app-business/target/generated-sources")
		
		models.extractTypes.filter(InheritorType).forEach[generateForInheritors(targetDir)];
		models.extractTypes.filter(UserType).forEach[generateForGeneralTypes(targetDir)];
		models.extractTypes.filter(EnumType).forEach[generateForGeneralTypes(targetDir)];
		
		generateFileDiscriptor(targetDir)
	}

	private def void generateForInheritors(InheritorType type, IPath outlet) {
		
		DyWAAbstractGenerator.generate(
			generateInterface(type).toString,
			'''«restTypePkg».''',
			'''«type.RESTTOSimpleName».java''',
			outlet,0
		);
		
		if (type instanceof ConcreteType) {
			DyWAAbstractGenerator.generate(
				generateInheritorImpl(type).toString,
				'''«restTypePkg».''',
				'''«type.RESTTOImplSimpleName».java''',
				outlet,0
			);
		}
	}

	private def void generateForGeneralTypes(Type type, IPath outlet) {
		DyWAAbstractGenerator.generate(
			generateGeneralImpl(type).toString,
			'''«restTypePkg».''',
			'''«type.RESTTOSimpleName».java''',
			outlet,0
		);
	}

	private def void generateFileDiscriptor(IPath outlet) {
		val String fileReferenceContent = renderFileReference(dywaPkg).toString();
		val String fileReferencePackage = '''«restUtilPkg».''';
		val String fileReferenceFileName = "FileReference.java";
		
		DyWAAbstractGenerator.generate(fileReferenceContent, fileReferencePackage, fileReferenceFileName, outlet,1);
	}

	private def generateInterface(InheritorType type) '''
		package «restTypePkg»;
		
		@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
		@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
		@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.dime.util.Constants.DYWA_RUNTIME_TYPE)
		public interface «type.RESTTOSimpleName» extends info.scce.dime.rest.RESTBaseType «type.renderSuperTypes» {
		
			«FOR attr : type.inheritedAttributes.map[originalAttribute].filter[it != null]»
				«attr.renderBasicAttributeType» get«attr.nameOfAccessor»();
				boolean is«attr.nameOfAccessor»Set();

				void set«attr.nameOfAccessor»(final «attr.renderBasicAttributeType» «attr.name.escapeJava»);
			«ENDFOR»

			«type.renderCreateFromMethod»
		}
	'''

	private def generateInheritorImpl(InheritorType type) {
		val headerGenerator = [InheritorType t | '''
			@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
			@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
			@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.dime.util.Constants.DYWA_RUNTIME_TYPE)
			public class «type.RESTTOImplSimpleName» extends info.scce.dime.rest.RESTBaseImpl implements «type.name.escapeJava»
		'''.toString]
		
		generateImpl(type, headerGenerator, true)
	}
	
	private def generateGeneralImpl(Type type) {
		val headerGenerator = [Type t | '''
			@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
			@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
			@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.dime.util.Constants.DYWA_RUNTIME_TYPE)
			public class «type.RESTTOSimpleName» extends info.scce.dime.rest.RESTBaseImpl implements info.scce.dime.rest.RESTBaseType
		'''.toString]
		
		generateImpl(type, headerGenerator, false)
	}
	
	private def <T extends Type> generateImpl(T type, (T) => String headerGenerator, boolean addOverrides) '''
		package «restTypePkg»;
		
		«headerGenerator.apply(type)» {

			«FOR attr : type.inheritedAttributes.map[originalAttribute].filter(TypeAttribute)»
				private «attr.renderBasicAttributeType» «attr.name.escapeJava»«IF attr.isIsList» = new java.util.LinkedList<>()«ENDIF»;
				private boolean is«attr.name.escapeJava»Set;
				
				@com.fasterxml.jackson.annotation.JsonProperty("«attr.name.escapeString»")
				@info.scce.dime.rest.JsonRenderIndicator("is«attr.nameOfAccessor»Set")
				«IF addOverrides»@java.lang.Override«ENDIF»
				public «attr.renderBasicAttributeType» get«attr.nameOfAccessor»() {
					return this.«attr.name.escapeJava»;
				}
				
				@com.fasterxml.jackson.annotation.JsonIgnore
				public boolean is«attr.nameOfAccessor»Set() {
					return this.is«attr.name.escapeJava»Set;
				}

				@com.fasterxml.jackson.annotation.JsonProperty("«attr.name.escapeString»")
				«IF addOverrides»@java.lang.Override«ENDIF»
				public void set«attr.nameOfAccessor»(final «attr.renderBasicAttributeType» «attr.name.escapeJava») {
					this.«attr.name.escapeJava» = «attr.name.escapeJava»;
					this.is«attr.name.escapeJava»Set = true;
				}

				«IF attr instanceof ComplexAttribute»
					«attr.renderGetterSetterForSuperAttributes»
				«ENDIF»
			«ENDFOR»
			
			«IF !addOverrides»
				«type.renderCreateFromMethod»
			«ENDIF»
		}
	'''
	
	private def renderGetterSetterForSuperAttributes(ComplexAttribute attr) {
		var String result = ""
		var iter = attr.superAttr
		
		while (iter != null) {
			result += '''
				«IF attr.isIsList»
					// overridden by «attr.name.escapeString»
					@com.fasterxml.jackson.annotation.JsonIgnore
					@java.lang.Override
					public «iter.renderBasicAttributeType» get«iter.nameOfAccessor»() {
						return new java.util.ArrayList<>(this.get«attr.nameOfAccessor»());
					}
					
					// overridden by «attr.name.escapeString»
					@com.fasterxml.jackson.annotation.JsonIgnore
					@java.lang.Override
					public boolean is«iter.nameOfAccessor»Set() {
						return this.is«attr.nameOfAccessor»Set();
					}
					
					// overridden by «attr.name.escapeString»
					@com.fasterxml.jackson.annotation.JsonIgnore
					@java.lang.Override
					public void set«iter.nameOfAccessor»(final «iter.renderBasicAttributeType» «iter.name.escapeJava») {
						throw new java.lang.UnsupportedOperationException();
					}
				«ELSE»
					// overridden by «attr.name.escapeString»
					@com.fasterxml.jackson.annotation.JsonIgnore
					@java.lang.Override
					public void set«iter.nameOfAccessor»(final «iter.renderBasicAttributeType» «iter.name.escapeJava») {
						throw new java.lang.UnsupportedOperationException();
					}
				«ENDIF»
			'''
			
			iter = iter.superAttr
		}
		
		return result
	} 
	
	private def renderSuperTypes(InheritorType type) '''
		«type.getOutgoing(Inheritance).map[targetElement].join(", ", ", ", "")[name.escapeJava]»
	'''
	
	private def renderFileReference(String packageName) '''
		package «restUtilPkg»;
		
		import java.util.Date;
		
		public class FileReference {

			private long dywaId;
			private String fileName;
			private String contentType;
			private Date createdAt;

			public FileReference() {}
		
			public FileReference(final «dywaUtilPkg».FileReference delegate) {
				this.setDywaId(delegate.getDywaId());
		
				this.setFileName(delegate.getFileName());
				this.setContentType(delegate.getContentType());
				this.setCreatedAt(delegate.getCreatedAt());
			}
		
			@com.fasterxml.jackson.annotation.JsonProperty("dywaId")
			public long getDywaId() {
				return dywaId;
			}
		
			@com.fasterxml.jackson.annotation.JsonProperty("dywaId")
			public void setDywaId(long id) {
				this.dywaId = id;
			}
		
			@com.fasterxml.jackson.annotation.JsonProperty("fileName")
			public String getFileName() {
				return fileName;
			}
		
			@com.fasterxml.jackson.annotation.JsonProperty("fileName")
			public void setFileName(String fileName) {
				this.fileName = fileName;
			}
		
			@com.fasterxml.jackson.annotation.JsonProperty("contentType")
			public String getContentType() {
				return contentType;
			}
		
			@com.fasterxml.jackson.annotation.JsonProperty("contentType")
			public void setContentType(String contentType) {
				this.contentType = contentType;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("createdAt")
			public Date getCreatedAt() {
				return this.createdAt;
			}
					
			@com.fasterxml.jackson.annotation.JsonProperty("createdAt")
			public void setCreatedAt(Date createdAt) {
				this.createdAt = createdAt;
			}
			
			@java.lang.Override
			public boolean equals(final java.lang.Object obj) {
				if (this == obj) {
					return true;
				}
		
				if (!(obj instanceof FileReference)) {
					return false;
				}
		
				final FileReference that = (FileReference) obj;
				if (this.getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT && that.getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
					return this == that;
				}
		
				return this.getDywaId() == that.getDywaId();
			}
		}
	'''
	
	static def List<Type> extractTypes(List<Data> models) {
		val knownVariables = models.map[types].flatten.toMap[id];
		
		var fixpoint = false;  
		while (!fixpoint) {
		        fixpoint = true;
		        for (t : new ArrayList<Type>(knownVariables.values)) {
		                for (attr : t.inheritedAttributes.map[originalAttribute]) {
		                        if (attr.isComplex) {
		                                val realAttr = attr.originalAttribute
		                                fixpoint = fixpoint && (knownVariables.put(realAttr.complexDataType.getId(), realAttr.complexDataType) !== null);
		                        }
		                }
		        }
		}
		
		return knownVariables.values.toList;
	}

	private def renderBasicAttributeType(Attribute attr) '''
		«IF attr.isIsList»java.util.List<«ENDIF»
		«attr.renderInnerBasicAttributeType»
		«IF attr.isIsList»>«ENDIF»
	'''

	private def renderInnerBasicAttributeType(Attribute attr) '''
		«IF attr.isPrimitive»
			«getRestLiteral(attr.primitiveDataType)»
		«ELSE»
			«attr.complexDataType.name.escapeJava»
		«ENDIF»
	'''
	
	private def renderCreateFromMethod(Type type) '''
		«IF !type.knownSubTypes.filter[!isAbstract].empty»
			public static «type.RESTTOSimpleName» fromDywaEntity(final «type.dyWATypeName» entity, info.scce.dime.rest.ObjectCache objectCache) {

				final «type.RESTTOSimpleName» result;

				«FOR t : type.knownSubTypes.filter[!isAbstract].sortTopologically
					SEPARATOR " else "
					AFTER ''' else { throw new java.lang.IllegalArgumentException("Unknown type"); } '''»
					if (entity instanceof «t.dyWATypeName») {
						result = new «t.RESTTOImplName»();
					}
				«ENDFOR»

				objectCache.putRestTo(entity, result);

				return result;
			}
		«ENDIF»
	'''

}
