package info.scce.dime.generator.util

import de.jabc.cinco.meta.runtime.active.Memoizable
import info.scce.dime.api.DIMEGraphModelExtension
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.EnumType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.UserType
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.CreateSIB
import info.scce.dime.process.process.CreateUserSIB
import info.scce.dime.process.process.DeleteSIB
import info.scce.dime.process.process.GetOriginalUserSIB
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardProcessSIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.RetrieveCurrentUserSIB
import info.scce.dime.process.process.RetrieveOfTypeSIB
import info.scce.dime.process.process.SwitchToOriginalUserSIB
import info.scce.dime.process.process.SwitchToUserSIB
import info.scce.dime.process.process.TransientCreateSIB
import java.util.Set

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class DyWAExtension {
	protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	protected extension DataExtension = DataExtension.instance
	protected extension ProcessExtension = new ProcessExtension()
	
	// TODO should be defined in model or dad or somewhere?
	public static String dywaPkg = "de.ls5.dywa.generated"
	public static String dywaControllerPkg = '''«dywaPkg».controller'''
	public static String dywaEntityPkg = '''«dywaPkg».entity'''
	public static String dywaUtilPkg = '''«dywaPkg».util'''
	
	public static String restPkg = '''«dywaPkg».rest'''
	// TODO shouldn't be plural 'types'
	public static String restTypePkg = '''«restPkg».types'''
	public static String restControllerPkg = '''«restPkg».controller'''
	public static String restUserPkg = '''«restPkg».user'''
	public static String restUtilPkg = '''«restPkg».util'''
	
	def String getNameOfAccessor(Attribute attribute) {
		getNameOfAccessorForParent(attribute, getRootOverriddenField(attribute))
	}

	def String getNameOfAccessorWithoutInheritance(Attribute attribute) {
		getNameOfAccessorForParent(attribute, attribute)
	}

	private def String getNameOfAccessorForParent(Attribute attribute, Attribute parentAttribute) {
		if (attribute instanceof ComplexAttribute) {
			if (attribute.isIsList()) {
				return escapeJava('''«parentAttribute.getName()»_«attribute.getDataType().getName()»''')
			}
		}
		return escapeJava(parentAttribute.getName())
	}
	
	def Attribute getRootOverriddenField(Attribute attribute) {
		val Attribute overriddenField = if (attribute instanceof PrimitiveAttribute) {
			((attribute as PrimitiveAttribute)).getSuperAttr() 
		}
		else if (attribute instanceof ComplexAttribute) {
			((attribute as ComplexAttribute)).getSuperAttr() 
		}
		else {
			null 
		}
		if (overriddenField == null) attribute 
		else overriddenField.rootOverriddenField 
	}
	
	def String getDyWATypeName(Type it) '''«dywaEntityPkg».«originalType.localPkgWithFilename.pkgEscape».«dyWATypeSimpleName»'''
	
	def String getDyWATypeSimpleName(Type it) '''«name.escapeJava»'''
	
	def String getRESTTOName(Type it) '''«restTypePkg».«RESTTOSimpleName»'''
	
	def String getRESTTOSimpleName(Type it) '''«name.escapeJava»'''
	
	def String getRESTTOImplName(Type it) '''«restTypePkg».«RESTTOImplSimpleName»'''
	
	def String getRESTTOImplSimpleName(Type it) {
		switch it {
			UserType, EnumType: '''«name.escapeJava»'''
			default : '''«name.escapeJava»Impl'''
		}
	}

	def String getRESTControllerName(Type it) '''«restControllerPkg».«RESTControllerSimpleName»'''

	def String getRESTControllerSimpleName(Type it) '''«name.escapeJava»REST'''

	def String getControllerTypeName(Type it) '''«controllerPkg».«getControllerSimpleName»'''

	def String getControllerSimpleName(Type it) '''«name.escapeJava»Controller'''
	
	dispatch def getControllerPkg(Type it) '''«dywaPkg.pkgEscape».controller.«originalType.localPkgWithFilename.pkgEscape»'''
	// TODO this method heavily uses internal information how interaction types are generated.
	dispatch def getControllerPkg(GuardContainer it) '''«dywaPkg.pkgEscape».controller.gen.processcontexts'''
	// TODO this method heavily uses internal information how interaction types are generated.
	dispatch def getControllerPkg(Process it) '''«dywaPkg.pkgEscape».controller.gen.processcontexts'''
	// TODO this method heavily uses internal information how interaction types are generated.
	def String getProcessContextsTypeName(String simpleName) '''«dywaPkg.pkgEscape».entity.gen.processcontexts.«simpleName.escapeJava»'''
	def String getProcessContextsControllerName(String simpleName) '''«dywaPkg.pkgEscape».controller.gen.processcontexts.«simpleName.escapeJava»'''
	
	def getNameOfUserAssocAccessor(UserType it) {
		userAttributes.head.nameOfAccessor
	}
	def getNameOfUserAssocAccessor(ConcreteType it) {
		userAttributes.head.nameOfAccessor
	}
	
	def getConcreteUserType(GuardProcessSIB it) {
		((securityProcess.startSIB.outputPorts.findFirst[name.equals("currentUser")] as ComplexOutputPort).dataType as ConcreteType).userTypePredecessors.get(0)
	}
	
	@Memoizable
	def Set<Type> controllerTypesToInject(Process it) {
		(dataFlowTargets.map[switch it {
			CreateSIB: #[createdType]
			CreateUserSIB: #[createdType]
			TransientCreateSIB: #[createdType]
			RetrieveOfTypeSIB: #[retrievedType]
			DeleteSIB: complexInputsInUse.map[dataType]
			GuardContainer: guardProcessSIBs.map[getConcreteUserType]
			default: null
		}].filterNull.flatten + userTypes).toSet
	}
	
	@Memoizable
	def Set<UserType> userTypes(Process it) {
		dataFlowTargets.map[switch it {
				RetrieveCurrentUserSIB: currentUser
				SwitchToUserSIB: currentUser
				SwitchToOriginalUserSIB: currentUser
				GetOriginalUserSIB: currentUser
			}].filterNull.toSet
	}
	
	@Memoizable
	def boolean requiresDependencyInjection(Process it) {
		!controllerTypesToInject.isEmpty
	}
	
}