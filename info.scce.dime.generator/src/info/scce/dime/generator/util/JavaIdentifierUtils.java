package info.scce.dime.generator.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class JavaIdentifierUtils {
	private static final Set<String> javaKeywords = new HashSet<>(Arrays.asList( 
			"abstract", "assert", "boolean", "break", "byte", "case", "catch", "char", "class", "const", "continue", "default", "do", "double", "else", "enum", "extends", "false", "final", "finally", "float", "for", "goto", "if", "implements", "import","var", "instanceof", "int", "interface", "long", "native", "new", "null", "package", "private", "protected", "public", "return", "short", "static", "strictfp", "super", "switch", "synchronized", "this", "throw", "throws", "transient", "true", "try", "void", "volatile", "while"
		)
	);
	
	private static final Set<String> dartKeywords = new HashSet<>(javaKeywords);
	
	static {
		dartKeywords.add("bool");
		dartKeywords.add("newList");
		dartKeywords.add("in");
		dartKeywords.add("is");
		dartKeywords.add("rethrow");
		dartKeywords.add("with");
	}
	
	private static final Pattern VALID_DART_IDENTIFIER = Pattern.compile("[a-zA-Z0-9_]");
	
	public static String pkgEscape(String source) {
		return Arrays.stream(source.split("\\.")).map(JavaIdentifierUtils::escapeJava).collect(Collectors.joining("."));
	}
	
	public static String escapeJava(CharSequence source) {
		return escapeJava(source.toString());
	}
	
	public static String escapeJava(String source) {
        if(javaKeywords.contains(source)) {
        		return '_' + source;
        }
        else {
    		char[] sourceAsArray = source.toCharArray();
            StringBuilder result = new StringBuilder();
            
            if(!Character.isJavaIdentifierStart(sourceAsArray[0])) {
                result.append('_');
            }

            for(int i = 0; i < sourceAsArray.length; ++i) {
                char c = sourceAsArray[i];
                if(Character.isJavaIdentifierPart(c)) {
                    result.append(c);
                } else if(Character.isWhitespace(c)) {
                    result.append('_');
                } else if(c == 45) {
                    result.append("__HYPHEN_MINUS__");
                } else {
                    result.append("__" + escapeJava(Character.getName(c)) + "__");
                }
            }
            return result.toString();
        }
	}
	
	public static String escapeDart(CharSequence source) {
		return escapeDart(source.toString());
	}
	
	public static String escapeDart(String source) {
        if(dartKeywords.contains(source)) {
    		return "attr_" + source;
	    }
	    else {
			char[] sourceAsArray = source.toCharArray();
	        StringBuilder result = new StringBuilder();
            
            for(int i = 0; i < sourceAsArray.length; ++i) {
                char c = sourceAsArray[i];
                
                if(VALID_DART_IDENTIFIER.matcher(String.valueOf(c)).matches()) {
                    result.append(c);
                } else if(Character.isWhitespace(c)) {
                    result.append('_');
//                } else if(c == 37) {
//                    result.append('p');
//                } else if(c == 45) {
//                    result.append("__HYPHEN_MINUS__");
                } else {
                    result.append("_" + ((int)c) + "_");
                }
            }
            if(result.charAt(0) == '_'){
            	result.setCharAt(0, 'a');
            }
            return result.toString();
        }
	}

	public static String escapeString(final String source) {

		if (source == null) {
			return null;
		}

		String tmp = source.replace("\\", "\\\\"); // backslashes
		tmp = tmp.replace("\"", "\\\""); // quotes

		return tmp;
	}
	
	public static String trimNL(String s)
	{
		return s.replaceAll("\n", "");
	}
}
