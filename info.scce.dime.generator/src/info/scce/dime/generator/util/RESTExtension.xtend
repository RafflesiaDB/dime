package info.scce.dime.generator.util

import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.process.process.NativeFrontendSIBReference
import info.scce.dime.process.process.Process

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class RESTExtension {
	
	
	def renderOutputForInteractable(Process p, ProcessSIB sib)
		'''«p.renderInputClassPrefix()»Output'''
		
	def renderInputClassPrefix(Process p)
	'''«p.simpleTypeName»'''

	def getSimpleTypeName(Process process) {
		process.getSimpleTypeNameInternal.escapeJava
	}
	
	def getSimpleTypeNameDart(Process process) {
		process.getSimpleTypeNameInternal.escapeDart.toFirstUpper
	}
	
	def getSimpleTypeNameInternal(Process process) {
		process.modelName.toFirstUpper+process.id
	}
	
	def getSimpleTypeName(GUI gui) {
		gui.getSimpleTypeNameInternal.escapeJava
	}
	
	def getSimpleTypeNameDart(GUI gui) {
		gui.getSimpleTypeNameInternal.escapeDart.toFirstUpper
	}
	
	def getSimpleTypeNameInternal(GUI gui) {
		gui.title.toFirstUpper+gui.id
	}
	
	def getSimpleTypeName(SecuritySIB sib) {
		sib.getSimpleTypeNameInternal.escapeJava
	}
	
	def getSimpleTypeNameDart(SecuritySIB sib) {
		sib.getSimpleTypeNameInternal.escapeDart.toFirstUpper
	}
	
	def getSimpleTypeNameInternal(SecuritySIB sib) {
		(sib.proMod as Process).modelName.toFirstUpper
	}
	
	def getSimpleTypeName(NativeFrontendSIBReference nfSIB) {
		nfSIB.getSimpleTypeNameInternal.escapeJava
	}
	
	def getSimpleTypeNameDart(NativeFrontendSIBReference nfSIB) {
		nfSIB.getSimpleTypeNameInternal.escapeDart.toFirstUpper
	}
	
	def getSimpleTypeNameInternal(NativeFrontendSIBReference nfSIB) {
		nfSIB.label.toFirstUpper+nfSIB.id
	}
	
	def getResultTypeName(Process it) '''«simpleTypeName»Result'''
	
	def getResultTypeName(GUI it) '''«simpleTypeName»Result'''
}