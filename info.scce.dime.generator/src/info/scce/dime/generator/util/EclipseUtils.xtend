package info.scce.dime.generator.util

import org.eclipse.core.resources.IFolder

// TODO move up to cinco meta?
// TODO rename to IFolderExtension or EclipseExtension?
class EclipseUtils {
	def static void mkdirs(IFolder folder) {
		if (folder.exists) return;
		val parent = folder.parent
		if (parent instanceof IFolder) parent.mkdirs
		try folder.create(true, true, null)
		catch (Throwable it) {
			// FIXME: This is ugly. Needs to be beautified soon[tm]
			if (!message.startsWith("A resource already exists on disk")) throw it
		}
	}
}
