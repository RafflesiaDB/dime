package info.scce.dime.generator.gui;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import graphmodel.Container;
import graphmodel.Node;
import info.scce.dime.generator.dad.GenerationContext;
import info.scce.dime.generator.util.JavaIdentifierUtils;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttribute;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.FOR;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.Placeholder;
import info.scce.dime.gui.gui.Tab;
import info.scce.dime.gui.gui.Table;
import info.scce.dime.gui.gui.TableLoad;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.gui.ComplexListAttributeName;


/**
 * The template helper provides helper methods for the xtend templates.
 * @author zweihoff
 *
 */
public class TemplateHelper {
	
	private GenerationContext genctx;
	
	Map<MovableContainer,List<Node>> getInputsVariablesCache;
	Map<MovableContainer,List<Node>> getOuterIterationVariablesCache;
	Map<Node,Boolean> isPartOfAnyIterationCache;
	
	public TemplateHelper(GenerationContext genctx){
		this.genctx = genctx;
		getInputsVariablesCache = new ConcurrentHashMap<MovableContainer, List<Node>>();
		getOuterIterationVariablesCache = new ConcurrentHashMap<MovableContainer, List<Node>>();
		isPartOfAnyIterationCache = new ConcurrentHashMap<Node, Boolean>();
	}
	
	/**
	 * Returns a valid string to be a html tag.
	 * Adds -tag to the end and removes - and _.
	 * @param value
	 * @return
	 */
	public static String tagFormat(String value)
	{
		value = JavaIdentifierUtils.escapeDart(value);
		value = value.replace(' ', '-');
		value = value.replace('_', '-');
		value = value.toLowerCase();
		return value +"-tag";
	}
	
	/**
	 * Returns a string to be a valid Dart class name with first upper case string
	 * and without spaces.
	 * @param value
	 * @return
	 */
	public static String classFormat(String value)
	{
		value = value.trim();
		
		return Character.toUpperCase(value.charAt(0)) + value.substring(1);
	}
	
	
	/**
	 * Returns the angular HTML template code for a GUI.
	 * @param engine The GUI code
	 * @param lrpIps The available interaction processes
	 * @param preview Defines if the HTML contains no Angular template code. Otherwise just plain HTML5
	 * @return
	 */
	public String getTemplate(GUI engine, GenerationContext genctx)
	{
		return new BaseTemplate().create(engine);
	}
	
	/**
	 * Collects all input variables needed for a given component and all its children.
	 * This includes iteration variables of surrounding scopes.
	 * @param form
	 * @return
	 */
	private List<Node> getInputsVariablesNoCache(MovableContainer cmc)
	{
		List<Node> vars = new LinkedList<Node>();
		List<Node> output = new LinkedList<Node>();
		vars.addAll(getOuterIterationVariables(cmc,cmc));
		for(Node node : vars)
		{
			output.addAll(node.getOutgoing(ComplexListAttributeConnector.class).
					stream().
					filter(n->n.getAttributeName()==ComplexListAttributeName.CURRENT).
					map(n->n.getTargetElement()).
					collect(Collectors.toList()));
		}
		return output;
	}
	
	/**
	 * Collects all input variables needed for a given component and all its children.
	 * This includes iteration variables of surrounding scopes.
	 * @param form
	 * @return
	 */
	public List<Node> getInputsVariables(MovableContainer cmc)
	{
		if(getInputsVariablesCache.containsKey(cmc)){
			return getInputsVariablesCache.get(cmc);
		}
		getInputsVariablesCache.putIfAbsent(cmc, getInputsVariablesNoCache(cmc));
		return getInputsVariablesCache.get(cmc);
	}
	
	/**
	 * Determines the iteration variables used in the given container or
	 * one of its parents recursively.
	 * @param cmc The current component
	 * @param source The initial component
	 * @return List of variables or attributes
	 */
	private List<Node> getOuterIterationVariablesNoCache(MovableContainer cmc, MovableContainer source)
	{
		List<Node> data = new LinkedList<Node>();
		if(cmc == null){
			return data;
		}
		if(!(cmc instanceof MovableContainer)) {
			return data;
		}
		if(!cmc.getIncoming(FOR.class).isEmpty()){
			data.add(cmc.getIncoming(FOR.class).get(0).getSourceElement());
		}
		if(!source.equals(cmc) &&  !cmc.getIncoming(TableLoad.class).isEmpty()){
			data.add(cmc.getIncoming(TableLoad.class).get(0).getSourceElement());
		}
		if(cmc.getContainer() instanceof MovableContainer) {
			data.addAll(getOuterIterationVariables((MovableContainer) cmc.getContainer(),source));			
		}
		return data;
	}
	
	/**
	 * Determines the iteration variables used in the given container or
	 * one of its parents recursively.
	 * @param cmc The staring component
	 * @return List of variables or attributes
	 */
	private List<Node> getOuterIterationVariables(MovableContainer cmc, MovableContainer source)
	{
		if(getOuterIterationVariablesCache.containsKey(cmc)){
			return getOuterIterationVariablesCache.get(cmc);
		}
		getOuterIterationVariablesCache.putIfAbsent(cmc, getOuterIterationVariablesNoCache(cmc, source));
		return getOuterIterationVariablesCache.get(cmc);
	}
	
	/**
	 * Determines if a given variable or attribute, or one of its parents in the data context
	 * are used in an iteration scope
	 * @param node
	 * @return
	 */
	private boolean isPartOfAnyIterationNoCache(Node node)
	{
		if(node instanceof Attribute){
			if(node instanceof ComplexListAttribute) {
				ComplexListAttribute ccla = (ComplexListAttribute)node;
				if(ccla.getAttributeName() == ComplexListAttributeName.CURRENT){
					return true;
				}
			}
			if(node.getContainer() instanceof Variable){
				return isPartOfAnyIteration((Node)node.getContainer());
			}
		}
		else if(node instanceof Variable)
		{
			Variable cv = (Variable)node;
			for(ComplexListAttributeConnector clac: cv.getIncoming(ComplexListAttributeConnector.class))
			{
				if(clac.getAttributeName() == ComplexListAttributeName.CURRENT)return true;
				return isPartOfAnyIteration(clac.getSourceElement());
			}
			for(ComplexAttributeConnector clac: cv.getIncoming(ComplexAttributeConnector.class))
			{
				return isPartOfAnyIteration(clac.getSourceElement());
			}
		}
		return false;
	}
	
	/**
	 * Determines if a given variable or attribute, or one of its parents in the data context
	 * are used in an iteration scope
	 * @param node
	 * @return
	 */
	public boolean isPartOfAnyIteration(Node node)
	{
		if(isPartOfAnyIterationCache.containsKey(node)){
			return isPartOfAnyIterationCache.get(node);
		}
		isPartOfAnyIterationCache.putIfAbsent(node, isPartOfAnyIterationNoCache(node));
		return isPartOfAnyIterationCache.get(node);
	}
	
	/**
	 * Checks, if a given container is anywhere surrounded by a form component
	 * @param container
	 * @return
	 */
	public boolean isEmbeddedInForm(Container container)
	{
		return getEmbeddingForm(container)!=null;
	}
	
	/**
	 * Checks, if a given container is anywhere surrounded by a table component
	 * @param container
	 * @return
	 */
	public boolean isEmbeddedInTable(Container container)
	{
		return getEmbeddingTable(container) != null;
	}
	
	/**
	 * Determines if a given container is anywhere surrounded by a tab component
	 * and if this tab also contains a placeholder anywhere in the tab itself or
	 * in one of its children.
	 * @param container
	 * @return
	 */
	public boolean isEmbeddedInPlaceholderTab(Container container)
	{
		Tab tab = getEmbeddingTab(container);
		if(tab!=null){
			if(genctx.guiExtension.find(tab,Placeholder.class).isEmpty())return true;
		}
		return false;
	}
	
	/**
	 * Collects a surrounding table component for a given container.
	 * Null, if no table is found.
	 * @param container
	 * @return
	 */
	public Table getEmbeddingTable(Container container)
	{
		return genctx.guiExtension.findFirstParent(container, Table.class);
	}
	
	/**
	 * Collects a surrounding tab component for a given container.
	 * Null, if no tab is found.
	 * @param container
	 * @return
	 */
	public Tab getEmbeddingTab(Container container)
	{
		return genctx.guiExtension.findFirstParent(container, Tab.class);
	}
	
	/**
	 * Collects a surrounding form component for a given container.
	 * Null, if no form is found.
	 * @param container
	 * @return
	 */
	public Form getEmbeddingForm(Container container)
	{
		return genctx.guiExtension.findFirstParent(container, Form.class);
	}
	
}
