package info.scce.dime.generator.gui;

/**
 * The data access type defines for specific components and control edges
 * how the variables and their attributes can be accessed.
 * 
 * TABLE: should be used for variables and attributes used in relation to a table component
 * FORM: should be used for variables and attributes used in relation to a form component
 * CHOICE: should be used for variables and attributes used in relation to one of the selection components like radio, combobox or checkbox
 * FOR: should be used for variables and attributes connected by an iteration edge
 * IF: should be used as default
 * @author zweihoff
 *
 */
public enum DataAccessType {
	TABLE, FORM, IF, FOR, CHOICE
}
