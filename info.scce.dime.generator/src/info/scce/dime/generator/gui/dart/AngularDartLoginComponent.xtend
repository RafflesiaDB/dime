package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.TemplateHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.GUI

class AngularDartLoginComponent extends GUIGenerator{
	
	def createComponent(GUI gui)
	'''
	import 'dart:html';
	
	import 'package:angular/angular.dart';
	import 'dart:async';
	import 'package:app/src/core/Authentication.dart';
	«new AngularDartIncludedTemplate().createImports(gui)»
	
	@Component(
			selector: 'login-form',
			directives: const [
				coreDirectives,«TemplateHelper.classFormat(new AngularDartComponentTemplate().getGUIName(gui).toString).toFirstUpper»
			],
			templateUrl: 'Login.html'
	)
	class Login implements OnInit {
	
		@Output('signedin')
		Stream<dynamic> get evt_signedin => signedin.stream;
		
		final StreamController<dynamic> signedin = new StreamController<dynamic>();
		
		@Input()
		bool modal = false;
		
		bool notCorrect = false;
	
		void ngOnInit()
		{

		}
		
		// Triggered on Form Submit
		void submit(Map formValues) async {
		  var username = formValues['username'];
		  var password = formValues['password'];
		  var correct = await Authentication.performLogin(username, password);
		  notCorrect = !correct;
		  if (correct) {
		    signedin.add(true);
		  }
		}
	
	}
	
	'''
	
	def createHTML(GUI gui)
	'''
	<«TemplateHelper.tagFormat(gui.title)»
		[modalDialog]="modal"
	  	[currentbranch]="'_login_'"
	  	[ismajorpage]="true"
	  	[notCorrect]="notCorrect"
	  	(«ConventionHelper.getEventName("login")»)="submit($event)"
	  >
	  </«TemplateHelper.tagFormat(gui.title)»>
	'''
	
}