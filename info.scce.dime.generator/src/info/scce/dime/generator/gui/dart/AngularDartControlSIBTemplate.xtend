package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.ISSIB
import info.scce.dime.gui.gui.SecuritySIB

class AngularDartControlSIBTemplate extends GUIGenerator {
	
	def dispatch String create(IFSIB sib){
		_iffor.createIFSIB(sib).toString
	}
	
	def dispatch String create(ISSIB sib){
		_iffor.createISSIB(sib).toString
	}
	
	def dispatch String create(SecuritySIB sib){
		new AngularDartSecuritySIBTemplate().create(sib).toString
	}
	
	def dispatch String create(FORSIB sib){
		_iffor.createFORSIB(sib).toString
	}
	
	def dispatch String createMethod(IFSIB sib){
		""
	}
	
	def dispatch String createMethod(FORSIB sib){
		""
	}
	
	def dispatch String createMethod(SecuritySIB sib){
		""
	}
	
	def dispatch String createMethod(ISSIB sib){
		_iffor.createISSIBMethod(sib).toString
	}
}