package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.AngularTODOList
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.TODOListHelper
import info.scce.dime.gui.gui.SpecialElement
import info.scce.dime.gui.gui.TODOList
import info.scce.dime.process.process.ComplexOutputPort

/**
 * Template for the todo list component Dart code
 */
class AngularDartTODOList extends AngularTODOList{
	
	/**
	 * Generates the needed variable declaration for the todo list
	 */
	def createDeclaration(SpecialElement element)
	'''
	DIMEList todoList«ConventionHelper.cincoID(element)»Iterator;
	String todoList«ConventionHelper.cincoID(element)»Filter = "";
	bool hasToSignIn«ConventionHelper.cincoID(element)» = false;
	'''
	
	/**
	 * Generates the needed variable initialization for the todo list
	 */
	def createInitialization(SpecialElement element)
	'''
	this.todoList«ConventionHelper.cincoID(element)»Iterator = new DIMEList();
	'''
	
	/**
	 * Generates the needed variable initialization for the todo list
	 */
	def createOnInit(SpecialElement element)
	'''
	this.loadTODOList«ConventionHelper.cincoID(element)»Entries();
	'''
	
	/**
	 * Generates the needed methods for the todo list
	 */
	def createMethods(SpecialElement element) {
	val todoList = element as TODOList
	'''
	/// callback, after log in to recieve «todoList.caption» todo list entries
	void hasSignedIn«ConventionHelper.cincoID(todoList)»(dynamic event) {
		this.hasToSignIn«ConventionHelper.cincoID(todoList)» = false;
		loadTODOList«ConventionHelper.cincoID(todoList)»Entries();
		
	}
	
	/// fetches the entries for «todoList.caption» todo list
	void loadTODOList«ConventionHelper.cincoID(todoList)»Entries()
	{
		HttpRequest request = new HttpRequest();
	  	request.onReadyStateChange.listen((_) {
	  		if(request.status == 401){
	  			this.hasToSignIn«ConventionHelper.cincoID(todoList)» = true;
  			}
	  		if (request.readyState == HttpRequest.DONE && (request.status == 200))
	  		{
				this.todoList«ConventionHelper.cincoID(todoList)»Iterator = Todos.fromJSON(request.responseText);
				this.hasToSignIn«ConventionHelper.cincoID(todoList)» = false;
			}
		});
		«IF todoList.filteredGuardContainers.empty»
			// public request
			request.open("GET",'rest/todos/all', async: true);
		«ELSEIF !todoList.filterIsExclude»
			// including
			request.open("GET",'rest/todos/including?«todoList.filteredGuardContainers.map[include | '''includes=«include»'''].join("&")»', async: true);
		«ELSE»
			// excluding
			request.open("GET",'rest/todos/excluding?«todoList.filteredGuardContainers.map[exclude | '''excludes=«exclude»'''].join("&")»', async: true);
		«ENDIF»
		request.send('');
	}
	
	/// realizes hovering effect for todo list
	void highlight«ConventionHelper.cincoID(element)»TODO(dynamic event)
	{
		event.target.classList = ['media','hover-todolist'];
	}
	
	/// realizes hovering effect for todo list
	void unHighlight«ConventionHelper.cincoID(element)»TODO(dynamic event)
	{
		event.target.classList = ['media'];
	}
	
	/// callback, if todo list filter has changed
	void filter«ConventionHelper.cincoID(todoList)»TODOs(dynamic event)
	{
		this.todoList«ConventionHelper.cincoID(todoList)»Filter = event.target.value;
	}
	
	/// returns the filtered todo list
	DIMEList get«ConventionHelper.cincoID(todoList)»TODOs()
	{
		if(this.todoList«ConventionHelper.cincoID(todoList)»Filter!=null) {
			return this.todoList«ConventionHelper.cincoID(todoList)»Iterator;
		}
		return this.todoList«ConventionHelper.cincoID(todoList)»Iterator.where((element) {
			return (
				this.getTodoList«ConventionHelper.cincoID(todoList)»Title(element).toString().toLowerCase().indexOf(this.todoList«ConventionHelper.cincoID(todoList)»Filter.toLowerCase()) >0 ||
				this.getTodoList«ConventionHelper.cincoID(todoList)»Description(element).toString().toLowerCase().indexOf(this.todoList«ConventionHelper.cincoID(todoList)»Filter.toLowerCase()) >0
			);
		});
	}
	
	/// returns the todo list entry title
	String getTodoList«ConventionHelper.cincoID(todoList)»Title(entry)
	{
		if(entry.displayName == null)return '';
		return entry.displayName;
	}
	
	/// returns the todo list entry description
	String getTodoList«ConventionHelper.cincoID(todoList)»Description(entry)
	{
		if(entry.description == null)return '';
		return entry.description;
	}
	
	/// returns the todo list empty text
	String getTodoList«ConventionHelper.cincoID(todoList)»EmptyValue()
	{
		«IF todoList.emptyValue.nullOrEmpty»
		return '';
		«ELSE»
		return '«TODOListHelper.parseDescription(todoList.emptyValue,"this.")»';
		«ENDIF»
	}
	
	/// returns the todo list entry overlay image path
	String getTodoList«ConventionHelper.cincoID(element)»ImageOverlay(dynamic entry)
	{
		if(entry.overlayImagePath == null)return 'app/img/todo.png';
		return entry.overlayImagePath;
	}
	
	/// returns the todo list entry image path
	String getTodoList«ConventionHelper.cincoID(todoList)»Image(dynamic entry)
	{
		if(entry.imagePath == null)return 'app/img/todo.png';
		return entry.imagePath;
	}
	
	/// callback, if todo list entry has been clicked
	///
	/// starts continuation of the corresponding longrunning process
	void clickTODOList«ConventionHelper.cincoID(todoList)»Entry(dynamic entry,dynamic event)
	{
		«FOR p : genctx.dad.processComponents.map[model]»
		// longrunning proces «p.modelName.escapeDart»
		if(entry.interactId=='«p.id.escapeString»'){
			 this.router.parent.navigate(['/LRP«ConventionHelper.toLink(p.modelName)»','StartLRP',{
			 	«FOR output : p.startSIBs.get(0).outputPorts»
			 	// port «output.name»
			 	«IF output instanceof ComplexOutputPort»
			 		«IF output.isList»
			 			'«output.name.escapeDart»':entry.interactionInputs.«output.name.escapeDart».join(','),
			 		«ELSE»
			 			'«output.name.escapeDart»':entry.interactionInputs.«output.name.escapeDart»!=null?entry.interactionInputs.«output.name.escapeDart»:0,
			 		«ENDIF»
			 	«ELSE»
			 		«IF output.isIsList»
			 		'«output.name.escapeDart»':entry.interactionInputs.«output.name.escapeDart».join(','),
			 		«ELSE»
			 		'«output.name.escapeDart»':entry.interactionInputs.«output.name.escapeDart»,
			 		«ENDIF»
			 	«ENDIF»
			 	«ENDFOR»
			 	'lrpid': entry.id,
			 	'guardContainerId':entry.guardContainerId
			 }]);
			 event.preventDefault();
		}
		«ENDFOR»
	}
	'''
	}
	
	/**
	 * Generates the import statements for the todo list component
	 */
	def createImports(TODOList element)''''''
	
}
