package info.scce.dime.generator.gui.dart

import graphmodel.ModelElementContainer
import info.scce.dime.dad.dad.URLProcess
import info.scce.dime.generator.gui.TemplateHelper
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.util.RESTExtension
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EntryPointProcessSIB
import info.scce.dime.process.process.EventConnector
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.SIB
import java.util.HashSet
import java.util.LinkedList
import java.util.List

/**
 * Template for the root process Angular Dart component class file.
 * The root process component encapsulates every process reachable in a GUI or the DAD
 * The main task of the root process is the start REST call
 */
class AngularDartProcessTemplate extends GUIGenerator{
	
	extension FrontEndProcessExtension = new FrontEndProcessExtension
	extension RESTExtension = new RESTExtension
	
	/**
	 * Returns the file name of the routable Angular Dart Component class
	 * for the given interaction process
	 */
	def visibleMajorSIBs(Process p) {
		val List<SIB> sibs = new LinkedList()
		sibs += p.GUISIBs.filter[isMajorPage]
		sibs
	}
	
	def dispatch tag(GUISIB sib) '''«TemplateHelper.tagFormat(sib.gui.title)»''' 
	def dispatch tag(ProcessSIB sib) '''«sib.proMod.modelName.escapeDart»-«sib.libraryComponentUID.escapeDart»-process'''
	def dispatch tag(EntryPointProcessSIB sib) '''«sib.proMod.modelName.escapeDart»-«sib.libraryComponentUID.escapeDart»-process'''
	def dispatch tag(GuardedProcessSIB sib) '''«sib.proMod.modelName.escapeDart»-«sib.libraryComponentUID.escapeDart»-process''' 
	
	
	def dispatch bindings(GUISIB sib,boolean major) {
		val i = if(major)'''major«sib.id.escapeDart»Input'''else'''minor«sib.id.escapeDart»Input'''
		'''
		«FOR input:sib.inputs.filter(InputPort).filter[!incoming.empty]»
			[«input.name.escapeDart»]="«i».«input.name.escapeDart»"
		«ENDFOR»
		«FOR input:sib.inputs.filter(InputStatic)»
			[«input.name.escapeDart»]="«input.staticValue»"
		«ENDFOR»
		[guiId]="'«sib.gui.id.escapeJava»'"
		[runtimeId]="runtimeId"
		[currentbranch]="currentBranch"
		[modalDialog]="false"
		[ismajorpage]="«IF major»true«ELSE»false«ENDIF»"
		«sib.guiOutputs»
		'''
	}
	
	def dispatch bindings(SIB sib,boolean major)
	'''
	[majorInput]="majorInput"
	[minorInput]="minorInput"
	[majorSIB]="majorSIB"
	[minorSIB]="minorSIB"
	[guiId]="guiId"
	[parentRuntimeId]="parentRuntimeId"
	[runtimeId]="runtimeId"
	[deserializer]="deserializer"
	
	'''
	
	/**
	 * Generates the routable Angular Dart component class file
	 */
	def create(Process process) {
		val allFrontEndProcesses = process.findFrontProcesses(genctx, new HashSet)
		val embeddedGUIModels = allFrontEndProcesses.map[GUISIBs].flatten.groupBy[gui].keySet
		val embeddedLinkedProcesses = allFrontEndProcesses.map[linkProcessSIBs].flatten.groupBy[model].keySet
		val eventTemplate = new AngularDartEventTemplate()
		val guiSIBs = allFrontEndProcesses.map[GUISIBs].flatten
		val listeners = allFrontEndProcesses.map[eventListeners].flatten
		'''
			// root «process.modelName» process combines all front end sibs reachable
			import 'package:angular/angular.dart';	
			import 'package:angular_router/angular_router.dart';
			import 'dart:async';
			import 'package:app/src/core/dime_process_service.dart';
			import 'package:app/src/core/AbstractRoutes.dart';
			import 'package:app/src/login/Login.dart' as login;
			import 'package:app/src/notification/notification_component.dart';
			//routes
			import 'package:app/src/app.dart' as main;
			//Data
			import 'package:app/src/models/FileReference.dart';
			import 'package:app/src/models/Selectives.dart';
			«FOR data : genctx.usedDatas»
				import 'package:app/src/data/«data.modelName.escapeDart».dart' as «DyWASelectiveDartGenerator.prefix(data)»;
			«ENDFOR»
			//GUI model outputs
			«embeddedGUIModels.map['''
				import 'package:app/src/rest/gui/«getSimpleTypeNameDart»Input.dart';
				import 'package:app/src/rest/gui/«getSimpleTypeNameDart»Branch.dart';
				import 'package:app/src/gui/«className».dart';
			'''].join»
			«embeddedLinkedProcesses.map['''
				import 'package:app/src/rest/«it.generateElementImport»Input.dart';
			'''].join»
			//NF model outputs
			«allFrontEndProcesses.map[nativeFrontendSIBReferences].flatten.map['''
				import 'package:app/src/rest/native/«getSimpleTypeNameDart»Branch.dart';
			'''].join»
			
			@Component(
			  	selector: '«process.modelName.escapeDart»-«process.id.escapeDart»-process',
			  	directives: const [
			  		coreDirectives,login.Login«FOR sib:allFrontEndProcesses.map[embeddedGUIModels].flatten BEFORE "," SEPARATOR ","»«sib.className»«ENDFOR»
			  	],
				template: «"'''"»
				<login-form 
					*ngIf="showLogin"
					(signedin)="retry()"
					[modal]="false"
				></login-form>
				<template [ngIf]="!showLogin">
					«FOR p:allFrontEndProcesses»
						<!-- Process «p.modelName» -->
						«FOR sib:p.visibleMajorSIBs»
						<«sib.tag»
							*ngIf="isVisible(majorSIB,'«sib.id»')"
							«sib.bindings(true)»
						>
							«FOR minorSIB:sib.injectableSIBs»
								<!-- Minor GUI SIB:«minorSIB.label» -->
								<template [ngIf]="isVisible(minorSIB,'«minorSIB.id»')"
									«{
										val placeHolder = if (sib instanceof GUISIB) sib.gui.findThe(Placeholder)
										'''«IF placeHolder!=null»«placeHolder.name.escapeDart.argumentTransclusion(placeHolder.id.escapeDart)»«ENDIF»'''
									}»
								>
								<«minorSIB.tag»
									«minorSIB.bindings(false)»
								></«minorSIB.tag»>
								</template>
							«ENDFOR»
						</«sib.tag»>
						«ENDFOR»
					«ENDFOR»
				</template>
				«"'''"»
			)
			class Process«process.id.escapeDart»Component extends GUIProcess implements OnInit, OnChanges, AfterViewChecked {
				
				@Input()
				String runtimeId;
				
				@Input()
				String parentRuntimeId;
				
				@Input()
				String majorSIB;
				
				@Input()
				String minorSIB;
				
				@Input()
				String guiId;
				
				@Input()
				String sibId;
				
				@Input()
				UserInteractionResponse majorInput;
				
				@Input()
				UserInteractionResponse minorInput;
				
				@Input()
				UserInteractionResponseDeserializer deserializer;
				
				@override
				UserInteractionResponseDeserializer getDeserializer() => deserializer;
				
				«FOR guiSIB:guiSIBs»
				«guiSIB.gui.getSimpleTypeNameDart»Input major«guiSIB.id.escapeDart»Input;
				«guiSIB.gui.getSimpleTypeNameDart»Input minor«guiSIB.id.escapeDart»Input;
				«ENDFOR»
				
				final DIMEProcessService _processService;
				
				final Router _router;
				
				final NotificationService _notificationService;
				
				«eventTemplate.createDeclaration(guiSIBs,listeners)»
				
				Process«process.id.escapeDart»Component(this._processService,this._router,this._notificationService,AbstractRoutes routes): super(routes);

				@override
				ngOnInit() {
					reActivateProcess();
					started = true;
				}
				
				@override
				ngAfterViewChecked() {
					«eventTemplate.createAfterViewChecked(listeners)»
				}
				
				void retry() {
					retryAfterLogin(_processService,"«process.simpleTypeName»",sibId:sibId);
				}
				
				@override
				void ngOnChanges(Map<String, SimpleChange> changes) {
					reActivateProcess();
				}
				
				@override
				Map<String,ActiveProcess> getActiveProcesses() => _processService.activeProcesses;
				
				@override
				String getParentRuntimeId() => parentRuntimeId;
				
				@override
				String getRuntimeId() => runtimeId;
				
				@override
			    String getGUIId() => guiId;
			    
			    @override
			    Router getRouter() => _router;
			    
			    @override
			    NotificationService getNotificationService() => _notificationService;
				
				/// starts a front end routine or a GUI by event
				void reActivateProcess() {
					
					«FOR guiSIB:guiSIBs»
					if(isVisible(majorSIB,'«guiSIB.id»')) {
						major«guiSIB.id.escapeDart»Input = majorInput as «guiSIB.gui.getSimpleTypeNameDart»Input;
						«eventTemplate.updateInputs(guiSIB,"major",false)»
					} else if(isVisible(minorSIB,'«guiSIB.id»')) {
						«guiSIB.gui.getSimpleTypeNameDart»Input newInput = minorInput as «guiSIB.gui.getSimpleTypeNameDart»Input;
						bool hasChanged = minor«guiSIB.id.escapeDart»Input!=null?minor«guiSIB.id.escapeDart»Input.inpusChanged(newInput):true;
						minor«guiSIB.id.escapeDart»Input = newInput;
						«IF !guiSIB.cacheGUI»
						«eventTemplate.updateInputs(guiSIB,"minor",true)»
						«ENDIF»
					}
					«ENDFOR»
					
					«FOR feSIB:allFrontEndProcesses.map[nativeFrontendSIBReferences].flatten»
					// NFSIB «feSIB.label» of Process «feSIB.rootElement.modelName»
					if(isVisible(majorSIB,'«feSIB.id.escapeString»')) {
						/// execute «feSIB.label.escapeString» Native Front End SIB
						/// TODO
						
					}
					«ENDFOR»
					«FOR linkSIB:allFrontEndProcesses.map[linkProcessSIBs].flatten»
					// LinkSIB «linkSIB.label» of Process «linkSIB.rootElement.modelName»
					if(isVisible(majorSIB,'«linkSIB.id.escapeString»')) {
						/// execute «linkSIB.label.escapeString» Link SIB
						var params = (majorInput as «linkSIB.getModel.getSimpleTypeNameDart»LinkSIBInput);
						_router.navigate(main.Routes.«AngularDartAppTemplate.routeName(linkSIB.proMod as URLProcess)».toUrl(«FOR it:linkSIB.inputs.map[routeParams] BEFORE "{" SEPARATOR "," AFTER "}"»«it»«ENDFOR»),NavigationParams(reload:true));
					}
					«ENDFOR»
					
					«FOR guiEvent:listeners»
					«{
						val guiSIB = guiEvent.getIncoming(EventConnector).map[sourceElement].get(0)
						val gui = guiSIB.gui
						'''
						// GUISIB «guiSIB.label» of Process «guiSIB.rootElement.modelName»
						if((isVisible(majorSIB,'«guiSIB.id»')&&(majorInput as «gui.getSimpleTypeNameDart»Input).event_«guiEvent.name.escapeDart»!=null)||(isVisible(minorSIB,'«guiSIB.id»')&&(minorInput as «gui.getSimpleTypeNameDart»Input).event_«guiEvent.name.escapeDart»!=null)) {
							/// execute «guiEvent.name.escapeString» event of «guiSIB.label.escapeString»
							print("execute «guiEvent.name.escapeString» event of «guiSIB.label.escapeString»");
							var input = isVisible(majorSIB,'«guiSIB.id»')?(majorInput as «gui.getSimpleTypeNameDart»Input).event_«guiEvent.name.escapeDart»:(minorInput as «gui.getSimpleTypeNameDart»Input).event_«guiEvent.name.escapeDart»;
							«eventTemplate.createMethods(guiEvent,guiSIB)»
						}
						'''
					}»
					
					«ENDFOR»
				}
				«{
					val nfSIBTemplate = new AngularDartNativeFrontendTemplate()
					allFrontEndProcesses.map[nativeFrontendSIBReferences].flatten.map[nfSIBTemplate.nativeFrontend(it)].join("\n")
				}»
				«FOR guiSIB:guiSIBs»
					// Branches for GUISIB «guiSIB.label» of Process «guiSIB.rootElement.modelName»
					«FOR branch:guiSIB.abstractBranchSuccessors»
					// Branch «branch.name»
					void event«branch.id.escapeDart»«branch.name.escapeDart»Trigger(Map<String,dynamic> map)
					{
						var result = new «guiSIB.gui.getSimpleTypeNameDart»Branch.for«branch.name.escapeDart»Branch(
							«FOR output:branch.outputs SEPARATOR ","»
							«output.name.escapeDart»:map['«output.name.escapeString»']«IF output instanceof ComplexOutputPort» as «IF output.isIsList»DIMEList<«ENDIF»«DyWASelectiveDartGenerator.prefix(output.dataType)».«output.dataType.name.escapeDart»«IF output.isIsList»>«ENDIF»«ENDIF»
							«ENDFOR»
						);
						_processService.continueProcess(
							deserializer,
							getActiveProcesses()[runtimeId].runtime,
							getRuntimeId(),
							'«guiSIB.gui.id.escapeJava»',
							'«branch.name.escapeJava»/branch/public',
							result.toJSOG(),
							parentRuntimeId:getParentRuntimeId()
						)
						.then((cpr)=>processResponse(_processService,cpr))
						.catchError((e)=>processError(e));
					}
					«ENDFOR»
				«ENDFOR»
			}
		'''
	
	}
	
	def updateInputs(AngularDartEventTemplate eventTemplate,GUISIB guiSIB,String sib,boolean restart)
	'''
	if(«eventTemplate.queryList(guiSIB)» != null) {
		«eventTemplate.queryList(guiSIB)».forEach((n)=>n.updateInputs(
			«FOR input:guiSIB.inputs.filter(Input) SEPARATOR ","»
				«IF input instanceof InputPort»
					p«input.name.escapeDart»:«sib»«guiSIB.id.escapeDart»Input.«input.name.escapeDart»
				«ELSEIF input instanceof InputStatic»
					p«input.name.escapeDart»:«input.staticValue»
				«ENDIF»
			«ENDFOR»
		));
		«IF restart && !guiSIB.cacheGUI»
		if(hasChanged) {
			«eventTemplate.queryList(guiSIB)».forEach((n)=>n.restartComponent());			
		}
		«ENDIF»
	}
	'''
		
	def getRouteParams(Input input) 
		''''«input.name.escapeDart»':«input.parseParam»'''
		
	def dispatch getParseParam(InputStatic input)'''«input.portParamParserStatic».toString()'''
	def dispatch getParseParam(InputPort input)'''params.«input.name.escapeDart»«IF input.isIsList».map((n)=>n.toString()).toList().join(',')«ELSE».toString()«ENDIF»'''

	
	def dispatch proMod(ProcessSIB it) {proMod}
	def dispatch proMod(EntryPointProcessSIB it) {proMod}
	def dispatch proMod(GuardedProcessSIB it) {proMod}
	
	def dispatch CharSequence className(Process p) '''Process«p.id.escapeDart»Component'''
	def dispatch CharSequence className(ProcessSIB sib) {sib.proMod.className}
	def dispatch CharSequence className(EntryPointProcessSIB sib) {sib.proMod.className}
	def dispatch CharSequence className(GuardedProcessSIB sib) {sib.proMod.className}
	def dispatch CharSequence className(GUISIB sib) {sib.gui.className}
	def dispatch CharSequence className(GUI gui) {new AngularDartComponentTemplate().getGUIName(gui)}
	
	def dispatch CharSequence classImport(ModelElementContainer p) '''import 'package:app/src/process/«p.className».dart';'''
	def dispatch CharSequence classImport(GUISIB p) '''import 'package:app/src/gui/«p.className».dart';'''
	def dispatch CharSequence classImport(GUI p) '''import 'package:app/src/gui/«p.className».dart';'''
	
}
