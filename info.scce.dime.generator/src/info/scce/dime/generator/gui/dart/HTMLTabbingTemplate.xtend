package info.scce.dime.generator.gui.dart

import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.Tabbing

/**
 * Template for the generation of methods used for the tabbing component
 */
class HTMLTabbingTemplate extends info.scce.dime.generator.gui.html.HTMLTabbingTemplate {

	
	/**
	 * Generate methods required to enable the tabbing component.
	 * The methods are used to change the current active tab.
	 */
	def createTabbingMethods(ModelElementContainer cmc)
	'''
	«FOR tabbing : cmc.find(Tabbing)»
		«FOR Node node : tabbing.allNodes.sortBy[x]»
			«IF node instanceof Tab»
				// methods for the tab «node.label»
				
				«IF !node.find(Placeholder).isEmpty»
					// placeholder is present in the tab «node.label»
					
					«FOR btn : node.findThe(ButtonGroup).buttons»
					/// checks if the tab «node.label» with the button «btn.label» is active
					bool getActive«ConventionHelper.cincoID(btn)»Tab(dynamic id)
					{
						if(this.currentbranch=='«ConventionHelper.cincoID(btn)»'){
							return true;
						}
						return false;
					}
					«ENDFOR»
					
					/// checks if the tab «node.label» is active
					bool getActive«ConventionHelper.cincoID(node)»Tab(dynamic id)
					{
						«FOR btn : node.findThe(ButtonGroup).buttons»
						// check for the button «btn.label»
						if(this.currentbranch =='«ConventionHelper.cincoID(btn)»'){
							return true;
						}
						«ENDFOR»
						return false;
					}
				«ELSE»
					/// checks if the tab «node.label» is active
					bool getActive«ConventionHelper.cincoID(node)»Tab(dynamic id)
					{
						String eq = '«ConventionHelper.cincoID(node)»${id}';
						if(this.currentbranch«ConventionHelper.cincoID(tabbing)»==eq){
							return true;
						}
						return false;
					}
					
					/// callback to set the active tab to «node.label»
					void click«ConventionHelper.cincoID(node)»Tab(dynamic event,dynamic id)
					{
						String eq = '«ConventionHelper.cincoID(node)»${id}';
						this.currentbranch«ConventionHelper.cincoID(tabbing)» = eq;
						event.preventDefault();
					}
				«ENDIF»
			«ENDIF»
		«ENDFOR»
	«ENDFOR»
	'''

}
