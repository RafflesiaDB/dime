package info.scce.dime.generator.gui.dart;

import java.util.stream.Collectors;

import graphmodel.Edge;
import graphmodel.Node;
import info.scce.dime.generator.gui.DataAccessType;
import info.scce.dime.generator.gui.MODE;
import info.scce.dime.generator.gui.data.SelectiveExtension;
import info.scce.dime.generator.gui.rest.model.GUICompoundView;
import info.scce.dime.generator.gui.rest.model.Parent;
import info.scce.dime.generator.gui.rest.model.TypeViewUtils;
import info.scce.dime.generator.dad.GenerationContext;
import info.scce.dime.generator.util.JavaIdentifierUtils;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ChoiceData;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexExtensionAttribute;
import info.scce.dime.gui.gui.ComplexListAttribute;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeName;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataBinding;
import info.scce.dime.gui.gui.Display;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.gui.PrimitiveExtensionAttribute;
import info.scce.dime.gui.gui.PrimitiveListAttribute;
import info.scce.dime.gui.gui.PrimitiveListAttributeName;
import info.scce.dime.gui.gui.PrimitiveType;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Read;
import info.scce.dime.gui.gui.Select;
import info.scce.dime.gui.gui.TableColumnLoad;
import info.scce.dime.gui.gui.TableEntry;
import info.scce.dime.gui.gui.Variable;


/**
 * Helper class to create access path to variables and attributes
 * @author zweihoff
 *
 */
public class DataDartHelper {

	/**
	 * Returns the selective data type for a given variable.
	 * If considreList is false, the returned type has no list type
	 * @param var
	 * @param considerList
	 * @param gcv
	 * @return
	 */
	public static CharSequence getVariableDataType(Variable var,boolean considerList,GUICompoundView gcv, SelectiveExtension selectiveExtension)
	{
		Parent p = TypeViewUtils.getTypeViewForVariable(gcv, var);
		if(p!=null){
			return selectiveExtension.selectiveClassName(p,considerList&&p.isList()&&var.isIsList());
		}
		throw new IllegalStateException("Variable {"+var.getName()+"} is not found in Selectives. ("+gcv.getGUI().getTitle()+")");
	}
	
	/**
	 * Returns the data access path to a variable or attribute connected with the given node
	 * by an iteration edge depended on the mode and access type.
	 * @param cInput
	 * @param dat
	 * @param mode
	 * @return
	 */
	public static <T> String getBindedDataName(Node cInput,DataAccessType dat,MODE mode,Class<T> c)
	{
		return getDataAccess(
				cInput
					.getIncoming()
					.stream()
					.filter((n)->(c.isInstance(n)))
					.map(n->n.getSourceElement())
					.findFirst()
					.get(),
				dat,mode);
	}
	
	
	/**
	 * Checks if the given table column component is connected to a
	 * variable or attribute with positive list flag
	 * @param ctce
	 * @return
	 */
	public static boolean isTableListEntry(TableEntry ctce)
	{
		for(Edge edge:ctce.getIncoming()){
			if(edge instanceof TableColumnLoad){
				TableColumnLoad ctcl = (TableColumnLoad) edge;
				if(ctcl.getSourceElement() instanceof ComplexVariable) {
					if(((ComplexVariable)ctcl.getSourceElement()).isIsList()){
						return true;
					}
				}
				if(ctcl.getSourceElement() instanceof PrimitiveAttribute) {
					PrimitiveAttribute ca = (PrimitiveAttribute) ctcl.getSourceElement();
					info.scce.dime.data.data.PrimitiveAttribute dca = ca.getAttribute();
					if(dca.isIsList()){
						return true;
					}
				}
				if(ctcl.getSourceElement() instanceof ComplexAttribute) {
					ComplexAttribute ca = (ComplexAttribute) ctcl.getSourceElement();
					info.scce.dime.data.data.ComplexAttribute dca = ca.getAttribute();
					if(dca.isIsList()){
						return true;
					}
				}
				if(ctcl.getSourceElement() instanceof PrimitiveAttribute) {
					PrimitiveAttribute ca = (PrimitiveAttribute) ctcl.getSourceElement();
					info.scce.dime.data.data.PrimitiveAttribute dca = ca.getAttribute();
					if(dca.isIsList()){
						return true;
					}
				}
				return false;
			}
		}
		return false;
	}
	
	/**
	 * Returns the local variable name for the binded variable or attribute of a given table column component
	 * @param prefix
	 * @param cte
	 * @return
	 */
	public static String getTableColumnDataName(String prefix,TableEntry cte)
	{
		for(Edge edge:cte.getIncoming().stream().filter(n->n instanceof TableColumnLoad).collect(Collectors.toList())){
			if(edge.getSourceElement() instanceof Variable) {
				return prefix+getVariableDataName((Variable)edge.getSourceElement(), (GUI) edge.getRootElement(),DataAccessType.TABLE,MODE.GET);
			}
			else if(edge.getSourceElement() instanceof Attribute){
				return prefix+getAttributeDataName((Attribute)edge.getSourceElement(), (GUI) edge.getRootElement(),DataAccessType.TABLE,MODE.GET);
			}
		}
		return "\"\"";
	}
	
	/**
	 * Returns the access path to a given variable, depended on the access type and the mode
	 * @param var
	 * @param gui
	 * @param dat
	 * @param mode
	 * @return
	 */
	public static String getVariableDataName(Variable var,GUI gui,DataAccessType dat,MODE mode)
	{
		String type = "";
		MODE m = (mode==MODE.SET_INIT)?MODE.GET_INIT:mode;
		
		if(var instanceof PrimitiveVariable)
		{
			PrimitiveVariable cpa = (PrimitiveVariable)var;
			if(mode == MODE.SET_INIT){
				type += "setValue";
			}
			if(mode == MODE.GET_INIT){
				type += "initOnDemand";
			}
			type += JavaIdentifierUtils.escapeDart(cpa.getName());
			if(mode == MODE.GET_INIT){
				type += "()";
			}
		}
		else if(var instanceof ComplexVariable)
		{
			ComplexVariable ccv = (ComplexVariable)var;
			ComplexVariable cv = (ComplexVariable) var;
			for(Edge edge:ccv.getIncoming()) {
				if(edge instanceof ComplexAttributeConnector) {
					if(mode == MODE.GET){
						type += getVariableDataName(ccv.getComplexVariablePredecessors().get(0),gui,dat,m)+"?."+ JavaIdentifierUtils.escapeDart(((ComplexAttributeConnector) edge).getAssociationName());						
					}
					else if(mode == MODE.GET_INIT){
						type += getVariableDataName(ccv.getComplexVariablePredecessors().get(0),gui,dat,m)+".initOnDemand"+ JavaIdentifierUtils.escapeDart(((ComplexAttributeConnector) edge).getAssociationName())+"()";						
					}
					else if(mode == MODE.SET_INIT){
						type += getVariableDataName(ccv.getComplexVariablePredecessors().get(0),gui,dat,m)+".setValue"+ JavaIdentifierUtils.escapeDart(((ComplexAttributeConnector) edge).getAssociationName());
					}
					else{
						type += getVariableDataName(ccv.getComplexVariablePredecessors().get(0),gui,dat,m)+"."+ JavaIdentifierUtils.escapeDart(((ComplexAttributeConnector) edge).getAssociationName());
					}
					return type;
				}
				if(edge instanceof ComplexListAttributeConnector) {
					ComplexListAttributeConnector ccla = (ComplexListAttributeConnector)edge;
					if(ccla.getAttributeName() == ComplexListAttributeName.CURRENT) {
						if(dat == DataAccessType.TABLE){
							return "";
						}
						return JavaIdentifierUtils.escapeDart(ccv.getName());
						
					}
					if(ccla.getAttributeName() == ComplexListAttributeName.FIRST) {
						if(mode == MODE.SET_INIT){
							return getVariableDataName(ccla.getSourceElement(),gui,dat,m)+".first.setMe";
						}
						return getVariableDataName(ccla.getSourceElement(),gui,dat,m)+".first";
					}
					if(ccla.getAttributeName() == ComplexListAttributeName.LAST) {
						if(mode == MODE.SET_INIT){
							return getVariableDataName(ccla.getSourceElement(),gui,dat,m)+".last.setMe";
						}
						return getVariableDataName(ccla.getSourceElement(),gui,dat,m)+".last";
					}
					break;
				}
			}
			if(mode == MODE.GET_INIT){
				type += "initOnDemand";
			}
			if(mode == MODE.SET_INIT){
				type += "setValue";
			}
			
			type += JavaIdentifierUtils.escapeDart(cv.getName());
			
			if(mode == MODE.GET_INIT){
				type += "()";
			}
		}
		
		return type;
	}
	
	/**
	 * Returns the selective type name of a given attribute
	 * @param var
	 * @param gcv
	 * @return
	 */
	public static String getAttributeDataType(Attribute var,GUICompoundView gcv, SelectiveExtension selectiveExtension)
	{
		
		Parent p = TypeViewUtils.getTypeViewForVariable(gcv, var);
		if(p!=null){
			return selectiveExtension.selectiveClassName(p,p.isList()).toString();
		}
		throw new IllegalStateException("Attribute {"+var.toString()+"} is not found in Selectives. ("+gcv.getGUI().getTitle()+")");
	}
	
	/**
	 * Returns the name of a given attribute, depended on the data access type and mode.
	 * @param var
	 * @param gui
	 * @param dat
	 * @param mode
	 * @return
	 */
	public static String getAttributeDataName(Attribute var,GUI gui,DataAccessType dat,MODE mode)
	{
		String type = "";
		MODE m = (mode==MODE.SET_INIT)?MODE.GET_INIT:mode;
		
		if(var instanceof PrimitiveAttribute)
		{
			PrimitiveAttribute cpa = (PrimitiveAttribute) var;
			if(cpa.getContainer() instanceof ComplexVariable){
				if(mode == MODE.GET){
					type += getVariableDataName((ComplexVariable)cpa.getContainer(), gui, dat,m)+"?.";
					
				}
				else{
					
					type += getVariableDataName((ComplexVariable)cpa.getContainer(), gui, dat,m)+".";
				}
			}
			PrimitiveAttribute pa = (PrimitiveAttribute) cpa;
			info.scce.dime.data.data.PrimitiveAttribute dpa = pa.getAttribute();
			if(mode == MODE.GET_INIT){
				return type +"initOnDemand"+JavaIdentifierUtils.escapeDart(dpa.getName())+"()";
			}
			if(mode == MODE.SET_INIT){
				return type +"setValue"+JavaIdentifierUtils.escapeDart(dpa.getName());
			}
			return type+JavaIdentifierUtils.escapeDart(dpa.getName());
		}
		else if(var instanceof PrimitiveExtensionAttribute)
		{
			PrimitiveExtensionAttribute cpa = (PrimitiveExtensionAttribute) var;
			if(cpa.getContainer() instanceof ComplexVariable){
				if(mode == MODE.GET){
					type += getVariableDataName((ComplexVariable)cpa.getContainer(), gui, dat,m)+"?.";
					
				}
				else{
					
					type += getVariableDataName((ComplexVariable)cpa.getContainer(), gui, dat,m)+".";
				}
			}
			info.scce.dime.data.data.ExtensionAttribute dpa = cpa.getAttribute();
			if(mode == MODE.GET_INIT){
				return type +"initOnDemand"+JavaIdentifierUtils.escapeDart(dpa.getName())+"()";
			}
			if(mode == MODE.SET_INIT){
				return type +"setValue"+JavaIdentifierUtils.escapeDart(dpa.getName());
			}
			return type+JavaIdentifierUtils.escapeDart(dpa.getName());
		}
		else if(var instanceof PrimitiveListAttribute) {
			PrimitiveListAttribute cpla = (PrimitiveListAttribute)var;
			if(cpla.getAttributeName() == PrimitiveListAttributeName.SIZE ){
				if(mode == MODE.GET) {
					return getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+"?.length";					
				}
				else {
					return getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+".length";		
				}
			}
		}
		else if(var instanceof ComplexListAttribute) {
			ComplexListAttribute ccla = (ComplexListAttribute)var;
			if(ccla.getAttributeName() == ComplexListAttributeName.CURRENT ){
				if(dat == DataAccessType.TABLE)return "";
				return "current";
			}
			if(ccla.getAttributeName() == ComplexListAttributeName.FIRST ) return getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+".first";
			if(ccla.getAttributeName() == ComplexListAttributeName.LAST ) return getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+".last";
		}
		else if(var instanceof ComplexAttribute)
		{
			ComplexAttribute ca = (ComplexAttribute) var;
			if(var.getContainer() instanceof ComplexVariable){
				if(mode == MODE.GET) {
					type += getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+"?.";					
				}
				else{
					type += getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+".";	
				}
				
			}
			info.scce.dime.data.data.ComplexAttribute dca = ca.getAttribute();
			if(mode == MODE.GET_INIT){
				return type +"initOnDemand"+JavaIdentifierUtils.escapeDart(dca.getName())+"()";
			}
			if(mode == MODE.SET_INIT){
				return type +"setValue"+JavaIdentifierUtils.escapeDart(dca.getName());
			}
			return type + JavaIdentifierUtils.escapeDart(dca.getName());				
			
		}
		else if(var instanceof ComplexExtensionAttribute) {
			ComplexExtensionAttribute ca = (ComplexExtensionAttribute) var;
			if(var.getContainer() instanceof ComplexVariable){
				if(mode == MODE.GET) {
					type += getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+"?.";					
				}
				else{
					type += getVariableDataName((ComplexVariable)var.getContainer(), gui, dat,m)+".";	
				}
				
			}
			info.scce.dime.data.data.ExtensionAttribute dca = ca.getAttribute();
			if(mode == MODE.GET_INIT){
				return type +"initOnDemand"+JavaIdentifierUtils.escapeDart(dca.getName())+"()";
			}
			if(mode == MODE.SET_INIT){
				return type +"setValue"+JavaIdentifierUtils.escapeDart(dca.getName());
			}
			return type + JavaIdentifierUtils.escapeDart(dca.getName());
		}
		
		return type;
	}
	
	/**
	 * Returns the data access path to a variable or attribute connected with the given node
	 * by an DataBinding, Display or Read edge depended on the mode and access type.
	 * @param cInput
	 * @param dat
	 * @param mode
	 * @return
	 */
	public static String getBindedDataName(Node cInput,DataAccessType dat,MODE mode)
	{
		return getDataAccess(
				cInput
					.getIncoming()
					.stream()
					.filter((n)->(n instanceof DataBinding || n instanceof Display || n instanceof Read))
					.map(n->n.getSourceElement())
					.findFirst()
					.get(),
				dat,mode);
	}
	
	/**
	 * Returns the data access path to a variable or attribute connected with the given node
	 * by an DataBinding, Display or Read edge depended on the mode and access type.
	 * @param cInput
	 * @param dat
	 * @param mode
	 * @return
	 */
	public static Node getBindedDataNode(Node cInput)
	{
		return		cInput
					.getIncoming()
					.stream()
					.filter((n)->(n instanceof DataBinding || n instanceof Display || n instanceof Read))
					.map(n->n.getSourceElement())
					.findFirst()
					.get();
	}
	
	/**
	 * Returns the data access path to a variable or attribute connected with the given node
	 * by an DataBinding, Display or Read edge depended on the mode and access type.
	 * @param cInput
	 * @param dat
	 * @param mode
	 * @return
	 */
	public static boolean isBindedToBoolean(Node cInput,DataAccessType dat,MODE mode)
	{
		return 
				cInput
					.getIncoming()
					.stream()
					.filter((n)->(n instanceof DataBinding || n instanceof Display || n instanceof Read))
					.map(n->n.getSourceElement())
					.filter(n->{
						if(n instanceof PrimitiveVariable){
							return ((PrimitiveVariable) n).getDataType()==PrimitiveType.BOOLEAN;
						}
						if(n instanceof PrimitiveAttribute){
							return ((PrimitiveAttribute) n).getAttribute().getDataType()==info.scce.dime.data.data.PrimitiveType.BOOLEAN;
						}
						return false;
					})
					.findFirst().isPresent();
	}
	
	
	/**
	 * Returns the selective data type of a variable or attribute connected with the given node
	 * by an DataBinding edge depended on the mode and access type.
	 * @param cInput
	 * @param considerList If true, list types are considered
	 * @param gcv
	 * @param c
	 * @return
	 */
	public static <T> CharSequence getBindedDataType(Node cInput,boolean considerList,GUICompoundView gcv,Class<T> c, SelectiveExtension selectiveExtension)
	{
		for(Edge edge:cInput.getIncoming().stream().filter(edge->c.isInstance(edge)).collect(Collectors.toList())){
			if(edge.getSourceElement() instanceof Variable) {
				return getVariableDataType((Variable)edge.getSourceElement(),considerList,gcv, selectiveExtension);
			}
			else if(edge.getSourceElement() instanceof Attribute){
				return getAttributeDataType((Attribute)edge.getSourceElement(), gcv, selectiveExtension);
			}
		}
		return null;
	}
	
	/**
	 * Returns the selective data type of a variable or attribute connected with the given node
	 * by an ChoiceData edge depended on the mode and access type. List types are always considered
	 * @param cSelect
	 * @param gcv
	 * @return
	 */
	public static CharSequence getChoicesDataType(Select cSelect,GUICompoundView gcv, SelectiveExtension selectiveExtension)
	{
		return getBindedDataType(cSelect,true,gcv,ChoiceData.class, selectiveExtension);
	}
	
	
	/**
	 * Returns the data access path to a given variable or attribute, depended on the
	 * given access tyoe and mode
	 * @param data
	 * @param dat
	 * @param mode
	 * @return
	 */
	public static String getDataAccess(Node data,DataAccessType dat,MODE mode)
	{
		if(data instanceof Variable) {
			return getVariableDataName((Variable)data, (GUI) data.getRootElement(),dat,mode);
		}
		else if(data instanceof Attribute){
			return getAttributeDataName((Attribute)data, (GUI) data.getRootElement(),dat,mode);
		}
		
		return "<Data Name not known>";
	}	
	
	/**
	 * Checks if the given node has at least one outgoing edge of the given type t.
	 * @param node
	 * @param t
	 * @return
	 */
	public static <T> boolean isEdgePresent(Node node,Class<T> t)
	{
		return node.
				getIncoming().
				stream().
				filter(n->(t.isInstance(n))).
				findAny().
				isPresent();
	}
	
	/**
	 * Returns the data access path to a variable or attribute connected with the given node
	 * by an ChoiceData edge depended on the mode with the choice access type.
	 * @param cSelect
	 * @param mode
	 * @return
	 */
	public static String getChoicesDataName(Select cSelect,MODE mode)
	{
		return getBindedDataName(cSelect,DataAccessType.CHOICE,mode,ChoiceData.class);
	}
	
	/**
	 * Returns the escaped name of a given variable or attribute
	 * @param node
	 * @return
	 */
	public static String getComponentInputName(Node node)
	{
		if(node instanceof Variable){
			Variable cv = (Variable)node;
			return JavaIdentifierUtils.escapeDart(cv.getName());
		}
		if(node instanceof PrimitiveAttribute){
			PrimitiveAttribute cpa = (PrimitiveAttribute) node;
			PrimitiveAttribute ap = (PrimitiveAttribute) cpa;
			return JavaIdentifierUtils.escapeDart(ap.getAttribute().getName());
		}
		if(node instanceof ComplexAttribute){
			ComplexAttribute cpa = (ComplexAttribute) node;
			ComplexAttribute ap = (ComplexAttribute) cpa;
			return JavaIdentifierUtils.escapeDart(ap.getAttribute().getName());
		}
		if(node instanceof ComplexListAttribute){
			ComplexListAttribute cpa = (ComplexListAttribute) node;
			return JavaIdentifierUtils.escapeDart(cpa.getAttributeName().getLiteral());
		}
		if(node instanceof PrimitiveListAttribute){
			PrimitiveListAttribute cpa = (PrimitiveListAttribute) node;
			return JavaIdentifierUtils.escapeDart(cpa.getAttributeName().getLiteral());
		}
		return "ERROR";
	}
}
