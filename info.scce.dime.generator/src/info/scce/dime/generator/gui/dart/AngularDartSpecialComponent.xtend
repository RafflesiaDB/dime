package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.SpecialElement
import info.scce.dime.gui.gui.TODOList

/**
 * Template for special elements. E.g the todo list component
 */
class AngularDartSpecialComponent extends GUIGenerator{
	
	/**
	 * Generates the needed variable declarations
	 */
	def createDeclaration(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createDeclaration(element);
	}
	
	/**
	 * Generates the needed variable initialization
	 */
	def createInitialization(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createInitialization(element);
	}
	
	/**
	 * Generates the needed variable initialization after the inputs are loaded
	 */
	def createOnInit(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createOnInit(element);
	}
	
	/**
	 * Generates the needed variable initialization after the user has logged in
	 */
	def afterLogin(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createOnInit(element);
	}
	
	/**
	 * Generates the needed methods
	 */
	def createMethods(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createMethods(element);
	}
	
	/**
	 * Generates the needed import statements
	 */
	def createImports(SpecialElement element)
	{
		if(element instanceof TODOList)return new AngularDartTODOList().createImports(element);
	}
}
