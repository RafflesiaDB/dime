package info.scce.dime.generator.gui.dart

import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.ProcessEntryPointComponent
import info.scce.dime.dad.dad.RootInteractionPointer
import info.scce.dime.dad.dad.StartupProcessPointer
import info.scce.dime.dad.dad.URLProcess
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.process.process.GuardContainer
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Map
import java.util.Set

/**
 * Template to create main.dart Angular Component.
 * The root Angular Component "App" holds the top level rooting.
 * For each interaction process, a root is given to access the interaction process by an URL and GET parameters for
 * every input port.
 */
class AngularDartAppTemplate extends GUIGenerator {
	
	extension FrontEndProcessExtension = new FrontEndProcessExtension
	
	/**
	 * Creates the Angular Dart component code for the main Angular App.
	 */
	def create(DAD dad,Map<String,Set<GuardContainer>> toGuardContainers) {
		val routableSibs = dad.URLProcesss.filter[getIncoming(StartupProcessPointer).isEmpty]
		val routbaleProcesses = routableSibs.map[model].toSet
		val startURL = dad.URLProcesss.findFirst[!getIncoming(RootInteractionPointer).isEmpty]
	'''
	/*
	 * Angular
	 */
	import 'package:angular/core.dart';
	import 'package:angular_router/angular_router.dart';
	
	import 'package:app/src/core/Helper.template.dart' as ng;
	import 'package:app/src/core/AbstractRoutes.dart';
	import 'package:app/src/core/dime_process_service.dart';
	import 'package:app/src/notification/notification_component.dart';
	import 'package:app/src/progress-bar/progress_bar_component.dart';
	import 'package:app/src/services/TableDndService.dart';
	import 'package:app/src/services/ProgressService.dart';
	/*
	 * Routables
	 */
	«FOR p:routbaleProcesses»
		//Route to «p.modelName»
		import 'package:app/src/dad/«AngularDartDADProcessTemplate.templateName(p)»' as ng;
	«ENDFOR»
	
	@Component(
	    selector: 'app',
	    template:«"'''"»
	    	  <notification #notification></notification>
	    	  <progress-bar></progress-bar>
	    	  <router-outlet [routes]="Routes.all"></router-outlet>
	    «"'''"»,
	    directives: const [routerDirectives,NotificationComponent,ProgressBarComponent],
	    exports: [Routes],
	    providers: const [
	    	ClassProvider(DIMEProcessService),
	    	ClassProvider(NotificationService),
	    	ClassProvider(ProgressService), 
			ClassProvider(TableDndService),
	        ClassProvider(AbstractRoutes, useClass: Routes),
		]
	)
	class AppComponent implements OnInit {
			
		@ViewChild('notification')
		NotificationComponent notificationComponent;
		
	    final NotificationService _notificationService;
	    final ProgressService _progressService;
	    
	    AppComponent(this._notificationService, this._progressService){}

	    @override
		void ngOnInit() async {
	    	this._notificationService.component = notificationComponent;
	    	print("GENERATED AT «{
	    		val dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
   				val now = LocalDateTime.now()
   				'''«dtf.format(now)»''' 
	    	}»");
		}
	}
	
	
	class Routes implements AbstractRoutes{
		«createRoute(startURL,"root")»
		«FOR p:routableSibs»
		  	«createRoute(p)»
	    «ENDFOR»
	  static final Error = RouteDefinition(
	    routePath: RoutePath(path: 'error'),
	    component: ng.ErrorFoundNgFactory,
	  );
	  static final Maintenance = RouteDefinition(
	  	 routePath: RoutePath(path: 'maintenance'),
	  	 component: ng.MaintenanceNgFactory,
	  );
	  static final NotFound = RouteDefinition(
	    path: '.+',
	    component: ng.NotFoundNgFactory,
	  );

	  static final all = <RouteDefinition>[
	  	  root,
		  RouteDefinition.redirect(
		    path: '/', redirectTo: «startURL.routeName».toUrl()
		  ),
		  «FOR p:routableSibs SEPARATOR "," AFTER ","»«p.routeName»«ENDFOR»
		  Maintenance,
		  NotFound
	  ];

	  @override
	  RouteDefinition getByName(String typeName) {
	  	switch(typeName) {
	  		«FOR p:routableSibs»
	  		case '«p.routeName»': return «p.routeName»;
	  		«ENDFOR»
			case 'root': return root;
	  		case 'Maintenance': return Maintenance;
	  		default: return NotFound;
	  	}
	  }
	}
	'''
	}
	
	
		
	
	
	/**
	 * Helper method to create an Angular route definition for the given interaction process compound view.
	 * If the interaction process is guarded, the guard inputs has to passed as well to access the interaction process.
	 */
	private def createRoute(URLProcess pc) {
		pc.createRoute(null)
	}
	private def createRoute(URLProcess pc,String root)
	'''
	static final «IF root.nullOrEmpty»«pc.routeName»«ELSE»«root»«ENDIF» = RouteDefinition(
	    routePath: RoutePath(path: '«IF pc.url.nullOrEmpty»«pc.model.modelName»«ELSE»«pc.url»«ENDIF»«FOR s:startRouteParameter(pc) BEFORE "/" SEPARATOR "/"»«s»«ENDFOR»'),
	    component: ng.«AngularDartDADProcessTemplate.className(pc.model)»«IF pc instanceof ProcessEntryPointComponent»«pc.entryPoint.id.escapeDart»«ENDIF»NgFactory,
	  );
	'''
	
	static def routeName(URLProcess up)'''Process«up.id.escapeDart»URL'''
	
	
}




