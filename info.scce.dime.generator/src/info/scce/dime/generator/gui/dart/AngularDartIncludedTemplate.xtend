package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.AngularIncludedTemplate
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.TemplateHelper
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB

/**
 * Template for creating the Angular Dart code for an embedded GUI SIB in a GUI model.
 */
class AngularDartIncludedTemplate extends AngularIncludedTemplate{
	
	/**
	 * Generate the import statement for the Angular Dart component of the GUI model referenced by the GUI SIB
	 */
	def CharSequence createImports(GUISIB cgs)
	{
		cgs.gui.createImports
	}
	
	/**
	 * Generate the import statement for the Angular Dart component of the GUI model referenced by the GUI SIB
	 */
	def CharSequence createImports(GUI cgs)
	'''
	import 'package:app/src/gui/«TemplateHelper.classFormat(new AngularDartComponentTemplate().getGUIName(cgs).toString).toFirstUpper».dart';
	'''
	
	
	/**
	 * Generate the on change code, to refresh recursively all embedded GUI models
	 */
	def createOnChange(GUI g)
	'''
	«FOR sp:distinct(g.find(GUISIB))»
		this.refresh«ConventionHelper.cincoID(sp)»Trigger = new Refresher();
	«ENDFOR»
	'''
	
}