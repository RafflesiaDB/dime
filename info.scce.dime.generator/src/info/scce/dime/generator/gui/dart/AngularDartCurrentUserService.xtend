package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.utils.GUIGenerator

/**
 * Template to create an Angular Dart service class file.
 * The service is used to fetch the current user of the application.
 */
class AngularDartCurrentUserService extends GUIGenerator{
	
	/**
	 * Generates the Angular Dart service class file.
	 * Used to fetch the current user of the given complex type view.
	 */
	def create(ComplexTypeView ctv)
	'''
	import 'dart:async';
	import 'dart:html';
	import 'package:angular/core.dart';
	import 'package:app/src/core/dime_process_service.dart';
	
	class «getName(ctv)»Service {
	    
		Future<String> syncUser() async {
			return (await HttpRequest.getString('${DIMEProcessService.getBaseUrl()}/rest/user/current/«ctv.type.name.escapeJava»/«DyWASelectiveDartGenerator.getSelectiveNameJava(ctv)»/private',withCredentials: true));
	    }
	    
	}
	'''
	
	/**
	 * Helper method to get the file name
	 */
	static def getName(ComplexTypeView ctv)'''User«ctv.typeName»«ctv.id»'''
	
}