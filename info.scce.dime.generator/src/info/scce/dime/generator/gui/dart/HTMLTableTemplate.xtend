package info.scce.dime.generator.gui.dart

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.gui.gui.Table

/**
 * Template to generate the methods and declarations for a table component
 */
class HTMLTableTemplate extends info.scce.dime.generator.gui.html.HTMLTableTemplate{

	/**
	 * Generates the refresh flag declarations for a table component 
	 */
	def createTableDeclaration(ModelElementContainer gui)
	'''
	// table refresh flags
	«FOR Table table : gui.find(Table)»
		bool refresh«ConventionHelper.cincoID(table)»Table;
	«ENDFOR»
	'''
	
	
	
}
