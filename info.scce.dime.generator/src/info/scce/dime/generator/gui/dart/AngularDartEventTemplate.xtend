package info.scce.dime.generator.gui.dart

import graphmodel.Container
import graphmodel.GraphModel
import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Registration
import info.scce.dime.gui.gui.SIB
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.OutputPort
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.generator.util.RESTExtension

class AngularDartEventTemplate extends GUIGenerator {
	
	extension RESTExtension = new RESTExtension
	
	def createDeclaration(Iterable<info.scce.dime.process.process.GUISIB> guiSIBs,Iterable<info.scce.dime.process.process.EventListener> listeners)
	'''

	«FOR guiSIB:guiSIBs»
	// GUISIBs of Process «guiSIB.rootElement.modelName»
	// GUISIB «guiSIB.label»
	@ViewChildren(«new AngularDartComponentTemplate().getGUIName(guiSIB.gui)»)
	List<«new AngularDartComponentTemplate().getGUIName(guiSIB.gui)»> «guiSIB.queryList»;
	«ENDFOR»
	«FOR evt:listeners»
	StreamController<dynamic> «evt.queryList»EventStream = new StreamController<dynamic>();
	bool «evt.queryList»IsPresent = false;
	«ENDFOR»
	'''
	
	def createMethods(info.scce.dime.process.process.EventListener evt,info.scce.dime.process.process.GUISIB guiSIB)
	'''
		//switch to GUI
		Map<String,dynamic> map = new Map();
		«evt.processEventInputs("map")»
		if(«evt.guiSIB.queryList»!= null && «evt.guiSIB.queryList».isNotEmpty) {
			//trigger event of gui
			«guiSIB.queryList».forEach((n) => n.recieve«evt.name.escapeDart»Event(map));
		} else {
			«evt.queryList»IsPresent = false;
			if(!«evt.queryList»EventStream.hasListener) {
				«evt.queryList»EventStream.stream.listen((_){
					var input = isVisible(majorSIB,'«guiSIB.id»')?(majorInput as «guiSIB.gui.getSimpleTypeNameDart»Input).event_«evt.name.escapeDart»:(minorInput as «guiSIB.gui.getSimpleTypeNameDart»Input).event_«evt.name.escapeDart»;
					Map<String,dynamic> map_evt = new Map();
					«evt.processEventInputs("map_evt")»
					//trigger event after gui is loaded
					«guiSIB.queryList».forEach((n) => n.recieve«evt.name.escapeDart»Event(map_evt));
				});
			}
		}
		
	'''
	
	def createAfterViewChecked(Iterable<info.scce.dime.process.process.EventListener> listeners)
	'''
	«FOR evt:listeners»
	if(!«evt.queryList»IsPresent && «evt.guiSIB.queryList».isNotEmpty) {
		«evt.queryList»IsPresent = true;
		«evt.queryList»EventStream.add(null);
	}
	«ENDFOR»
	'''
	
	def info.scce.dime.process.process.GUISIB getGuiSIB(info.scce.dime.process.process.EventListener listener)
	{
		listener.GUISIBPredecessors.get(0)
	}
	
	
	def createUpdateMethod(GUI gui,ModelElementContainer mec,GUICompoundView gcv,CharSequence content)
	'''
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
			«FOR form : mec.find(Form)»
			«form.queryList».forEach((n)=>n.updateWithoutInputs());
			«ENDFOR»
			«FOR table : mec.find(Table)»
			«table.queryList».forEach((n)=>n.updateWithoutInputs());
			«ENDFOR»
		}
		«FOR embeddedGUI : mec.find(GUISIB)»
		«embeddedGUI.queryList».forEach((n)=>n.updateWithoutInputs());
		«ENDFOR»
		updateImageHash();
	}
	
	void updateInputs(
		«FOR variable:gcv.compounds.filter[isInput] BEFORE "{" SEPARATOR "," AFTER "}"»
			«IF variable instanceof ComplexTypeView»
		  		«IF variable.list»
			  		DIMEList<«DyWASelectiveDartGenerator.prefix(variable.type.rootElement)».«variable.type.name.escapeDart.toFirstUpper»>  p«variable.name.escapeDart»
			  	«ELSE»
			  		«DyWASelectiveDartGenerator.prefix(variable.type.rootElement)».«variable.type.name.escapeDart.toFirstUpper» p«variable.name.escapeDart»
			  	«ENDIF»
			«ELSE»
			  	«IF (variable.data as PrimitiveVariable).isInput»
				  	«(variable.data as PrimitiveVariable).variableType» p«variable.name.escapeDart»
			  	«ENDIF»
			«ENDIF»
  		«ENDFOR»
	)
	{
		«FOR variable:gcv.compounds.filter[isInput]»
			«variable.name.escapeDart» = p«variable.name.escapeDart»;
  		«ENDFOR»
		
		updateWithoutInputs(updateHidden:false);
		«FOR form : mec.find(Form)»
		«form.queryList».forEach((n)=>n.updateInputs(
			«FOR variable:gcv.compounds.filter[isInput] SEPARATOR ","»
				  p«variable.name.escapeDart»:«variable.name.escapeDart»
	  		«ENDFOR»
		));
		«ENDFOR»
		«FOR table : mec.find(Table)»
		«table.queryList».forEach((n)=>n.updateInputs(
			«FOR variable:gcv.compounds.filter[isInput] SEPARATOR ","»
				  p«variable.name.escapeDart»:«variable.name.escapeDart»
	  		«ENDFOR»
		));
		«ENDFOR»
		«content»
	}
	'''
	
	def createDeclaration(GUI gui,ModelElementContainer cec)
	'''
	«FOR evt:cec.find(GUISIB)»
	/// GUI «evt.label»
	@ViewChildren(«new AngularDartComponentTemplate().getGUIName(evt.gui)»)
	List<«new AngularDartComponentTemplate().getGUIName(evt.gui)»> «evt.queryList»;
		«IF evt instanceof DispatchedGUISIB»
			«FOR subGUI : evt.gui.subGUIs»
				/// Disp GUI «subGUI.title»
				@ViewChildren(«new AngularDartComponentTemplate().getGUIName(subGUI)»)
				List<«new AngularDartComponentTemplate().getGUIName(subGUI)»> sub_«evt.queryList(subGUI)»;
			«ENDFOR»
		«ENDIF»
	«ENDFOR»
	
	
	«FOR p : cec.find(ProcessSIB).map[it.proMod].filter(info.scce.dime.process.process.Process).toSet»
	/// Process «p.modelName»
	@ViewChildren(«AngularDartRootProcessTemplate.className(p)»SIB)
	List<«AngularDartRootProcessTemplate.className(p)»SIB> «p.queryList»;
	«ENDFOR»
	
	«FOR evt:cec.eventGUIPlugins»
	/// GUI Plugin «evt.label»
	@ViewChildren(«new AngularDartGUIPluginTemplate().getPluginClassName(evt)»)
	List<«new AngularDartGUIPluginTemplate().getPluginClassName(evt)»> «evt.queryList»;
	«ENDFOR»
	
	«FOR form : cec.find(Form).filter[it != cec]»
	/// Form
	@ViewChildren(«form.id.escapeDart».«new AngularDartFormTemplate().getFormName(form,gui)»)
	List<«form.id.escapeDart».«new AngularDartFormTemplate().getFormName(form,gui)»> «form.queryList»;
	«ENDFOR»
	
	«FOR table : cec.find(Table).filter[it != cec]»
	/// Table
	@ViewChildren(«table.id.escapeDart».«new AngularDartTableTemplate().getTableName(table,gui)»)
	List<«table.id.escapeDart».«new AngularDartTableTemplate().getTableName(table,gui)»> «table.queryList»;
	«ENDFOR»
	'''
	
	def queryList(info.scce.dime.process.process.GUISIB evt)'''component«evt.id.escapeDart»'''
	def queryList(info.scce.dime.process.process.EventListener evt)'''event«evt.id.escapeDart»'''
	def queryList(GUIPlugin evt)'''plugin«evt.id.escapeDart»'''
	def queryList(GUISIB evt)'''componentSIB«evt.id.escapeDart»'''
	def queryList(GUISIB g,GUI evt)'''component«g.id.escapeDart»«evt.id.escapeDart»'''
	def queryList(info.scce.dime.process.process.Process p)'''component«p.id.escapeDart»'''
	def queryList(Form evt)'''formComponent«evt.id.escapeDart»'''
	def queryList(Table evt)'''tableComponent«evt.id.escapeDart»'''
	
	def createMethods(GUI gui,ModelElementContainer cec)
	'''
	«FOR listener:gui.listeners»
	/// receives the events for the «listener.name» listener
	void recieve«listener.name.escapeDart»Event(Map<String,dynamic> map)
	{
		//write the received data
		«FOR port:listener.connectedPorts»
		//for port «port.name»
			«FOR data:port.successors»
			this.«DataDartHelper.getDataAccess(data,DataAccessType.^FOR,MODE.SET_INIT)»(«'''map['«port.name.escapeString»']'''.getPortListCast(port)»);
			«ENDFOR»
		«ENDFOR»
		//trigger embedded forms and tables
		«FOR form : cec.find(Form).filter[it != cec]»
			«form.queryList».forEach((n) => n.recieve«listener.name.escapeDart»Event(map));
		«ENDFOR»
		«FOR table : cec.find(Table).filter[it != cec]»
			«table.queryList».forEach((n) => n.recieve«listener.name.escapeDart»Event(map));
		«ENDFOR»
		«FOR registeredEvent:listener.guiSIBEventListener(cec)»
			«IF !(registeredEvent.container as GUISIB).gui.events.filter[name.equals(registeredEvent.name)].empty»
			// trigger event «registeredEvent.name» on GUISIB «(registeredEvent.container as GUISIB).label»
			«(registeredEvent.container as GUISIB).queryList».forEach((event) {
				Map<String,dynamic> evtMap = new Map();
				«registeredEvent.eventInputs»
				event.recieve«registeredEvent.name.escapeDart»Event(evtMap);
			});
			«ENDIF»
			«IF (registeredEvent.container as GUISIB) instanceof DispatchedGUISIB»
				«FOR subGUI : (registeredEvent.container as GUISIB).gui.subGUIs.filter[!it.events.exists[name == registeredEvent.name]]»
					// trigger event «registeredEvent.name» of dispatched «subGUI.title»
					sub_«(registeredEvent.container as GUISIB).queryList(subGUI)».forEach((event) {
						Map<String,dynamic> evtMap = new Map();
						«registeredEvent.eventInputs»
						event.recieve«registeredEvent.name.escapeDart»Event(evtMap);
					});
				«ENDFOR»
			«ENDIF»
		«ENDFOR»
		«FOR registeredEvent:listener.guiPluginEventListener(cec)»
			// trigger event «registeredEvent.name» on GUI plug in «(registeredEvent.container as GUIPlugin).label»
			«(registeredEvent.container as GUIPlugin).queryList».forEach((event) {
				Map<String,dynamic> evtMap = new Map();
				«registeredEvent.eventInputs»
				event.recieve«registeredEvent.name.escapeDart»Event(evtMap);
			});
		«ENDFOR»
		«IF cec instanceof Form»
		this.loadFormFieldValues();
		«ENDIF»
	}
	«ENDFOR»
	'''
	def dispatch getPortListCast(CharSequence s,PrimitiveOutputPort port) '''«s»'''
	def dispatch getPortListCast(CharSequence s,ComplexOutputPort port) 
		'''«IF port.isIsList»DIMEList.from(«ENDIF»«s»«IF port.isIsList».cast<«port.dataType.rootElement.modelName.escapeDart».«port.dataType.name.escapeDart»>())«ENDIF»'''
	
	
	def dispatch guiSIBEventListener(Event event,Container cec)
	{
		event.eventListenerSuccessors.filter[container instanceof GUISIB].filter[placedInContainer(cec)]
	}
	
	def dispatch guiSIBEventListener(Event event,GraphModel cec)
	{
		event.eventListenerSuccessors.filter[container instanceof GUISIB]
	}
	
	def boolean placedInContainer(Node node,Container container) {
		if(node.id.equals(container.id)){
			return true;
		}
		if(node.container != null){
			if(node.container instanceof Container){
				return placedInContainer(node.container as Container,container);				
			}
		}
		return false;
	}
	
	def dispatch guiPluginEventListener(Event event,Container cec)
	{
		event.eventListenerSuccessors.filter[container instanceof GUIPlugin].filter[placedInContainer(cec)]
	}
	def dispatch guiPluginEventListener(Event event,GraphModel cec)
	{
		event.eventListenerSuccessors.filter[container instanceof GUIPlugin]
	}
	
	def connectedPorts(Event event)
	{
		event.outputPorts.filter[!outgoing.empty]
	}
	
	def getListeners(GUI cgui)
	{
		cgui.listenerContexts.map[events].flatten
	}
	
	def boolean getHasRegisteredEvents(SIB sib) {
		return !sib.abstractBranchs.filter(EventListener).filter[!getIncoming(Registration).empty].empty
	}
	
	def eventGUISIBs(ModelElementContainer gui)
	{
		gui.find(GUISIB).filter[hasRegisteredEvents]
	}
	
	def eventGUIPlugins(ModelElementContainer gui)
	{
		gui.find(GUIPlugin).filter[hasRegisteredEvents]
	}
	
	
	
}