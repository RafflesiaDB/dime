package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.gui.gui.File
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GuardSIB
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessType
import info.scce.dime.generator.gui.rest.DartFileGuardGenerator

/**
 * Template to generate statically helper methods for every Angular Dart component class
 * to handle files
 */
class HTMLFileTemplate extends info.scce.dime.generator.gui.html.HTMLFileTemplate{
	
	/**
	 * Generates statically helper methods for file handling
	 */
	def createMethods(GUI gui,GUICompoundView gcv)
	'''
	
	
	«FOR fileComponent : gui.find(File)»
	/// starts the file download for a file component
	void download«fileComponent.id.escapeDart»File(«fileComponent.parameterNames.join(",")»,dynamic event)
	{
		«createGuardedFileAccess(fileComponent,gui,gcv,true)»
	}
	«ENDFOR»
	
	«FOR imageComponent : gui.find(Image)»
	/// loads an image file download for an image component	
	String load«imageComponent.id.escapeDart»Image(«imageComponent.parameterNames.join(",")»)
	{
		«createGuardedFileAccess(imageComponent,gui,gcv,false)»
	}
	«ENDFOR»
	'''
	
	
	/**
	 * Generates the guarded file rest call method for the given component,
	 * placed in the given GUI model.
	 * The download flag decides, if the file should be download to the client or returned to
	 * the caller
	 */
	def createGuardedFileAccess(MovableContainer container,GUI gui,GUICompoundView gcv,boolean download)
	{
		val guards = container.allNodes.filter(GuardSIB).filter[isSecurity]
		val fetch = container.allNodes.filter(GuardSIB).filter[isFileFetch]
		val dfgg = new DartFileGuardGenerator()
		return 
		'''
		//prepare guard compound
		«dfgg.getGuardCompoundName(container)» guardContainer = new «dfgg.getGuardCompoundName(container)»();
		«IF fetch.empty»
			guardContainer.context = fileRef;
		«ELSE»
			«FOR input:fetch.get(0).allNodes.filter[n|n instanceof InputPort].map[n|n as InputPort]»
				guardContainer.context.«input.name.escapeDart» = «input.parameterName»;
			«ENDFOR»
		«ENDIF»
		
		«FOR guard:guards»
			«FOR input:guard.allNodes.filter(InputPort)»
				guardContainer.securityInputs.«dfgg.getGuardName(guard)».«input.name.escapeDart» = «input.parameterName»;
			«ENDFOR»
		«ENDFOR»
		// private interactable
		String guardData = guardContainer.toQueryParams();
		«IF download»
		downloadURI('${getRestBaseUrl()}/«new DartFileGuardGenerator().getImageURL(container)»${guardData}');
		«ELSE»
		return guardData;
		«ENDIF»
		
		'''
	}
	
	static def Boolean getIsSecurity(GuardSIB sib){
		return ((sib as GuardSIB).process as Process).processType == ProcessType.SECURITY;
	}
	
	static def Boolean getIsFileFetch(GuardSIB sib){
		return ((sib as GuardSIB).process as Process).processType == ProcessType.FILE_DOWNLOAD_SECURITY;
	}
}
