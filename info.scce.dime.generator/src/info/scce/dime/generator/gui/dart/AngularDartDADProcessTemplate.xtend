package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.process.process.Process

/**
 * Template for the routable Angular Dart component class file.
 * The routable component encapsulates every interaction process to be called via URL.
 * The main task of the routable is the parsing of the route parameters to fit the input ports.
 */
class AngularDartDADProcessTemplate extends GUIGenerator{
	
	extension FrontEndProcessExtension = new FrontEndProcessExtension
	
	/**
	 * Returns the file name of the routable Angular Dart Component class
	 * for the given interaction process
	 */
	static def templateName(Process p)'''Process«p.modelName.escapeDart»«p.id.escapeDart»Component.template.dart'''
	static def fileName(Process p)'''Process«p.modelName.escapeDart»«p.id.escapeDart»Component.dart'''
	static def className(Process p)'''Process«p.id.escapeDart»Component'''
	
	
	/**
	 * Generates the routable Angular Dart component class file
	 */
	def create(Process p)
	{
		val entryPoints = p.entryPointProcessSIBs
		val outputs = p.startSIBs.get(0).outputPorts
	'''
	// routing wrapper for the «p.modelName» process
	import 'package:angular/angular.dart';	
	import 'package:angular_router/angular_router.dart';
	import 'package:app/src/models/Selectives.dart';
	import 'package:app/src/models/FileReference.dart';
	//Data
	//root process
	«AngularDartRootProcessTemplate.classImport(p)»
	
	
	@Component(
	  	selector: 'routable-«p.id.escapeDart»-process',
	  	directives: const [coreDirectives,routerDirectives,«AngularDartRootProcessTemplate.className(p)»],
		template: «"'''"»
		<«AngularDartRootProcessTemplate.classTag(p)»
			*ngIf="loaded"
			[startPointId]="startPointId"
			«FOR port:outputs»
			[«port.name.escapeDart»]="«port.name.escapeDart»"
			«ENDFOR»
		>
		</«AngularDartRootProcessTemplate.classTag(p)»>
		«"'''"»
	)
	class «p.className» implements OnActivate, CanReuse {
		
		
		«FOR port:outputs»
		«port.portParamType» «port.name.escapeDart»;
		«ENDFOR»
		
		bool loaded = false;
		String startPointId = '«p.startSIBs.get(0).id.escapeDart»';
		
		«p.className»() {}
		
		@override
		void onActivate(_, RouterState current) async {
		    «FOR port:outputs»
			«port.name.escapeDart» = «'''current.parameters['«port.name.escapeDart»'].toString()'''.portParamParser(port)»;
			«ENDFOR»
			loaded = true;
		}
		
		@override
		Future<bool> canReuse(RouterState current, RouterState next) {
		    return Future.value(false);
		}
	}
	
	«FOR entryPoint:entryPoints»
	@Component(
		  	selector: 'routable-«p.id.escapeDart»-«entryPoint.id.escapeDart»-process',
		  	directives: const [coreDirectives,routerDirectives,«AngularDartRootProcessTemplate.className(p)»],
			template: «"'''"»
			<«AngularDartRootProcessTemplate.classTag(p)»
				*ngIf="loaded"
				[startPointId]="startPointId"
				«FOR port:entryPoint.inputPorts»
				[«entryPoint.label.escapeDart»_«port.name.escapeDart»]="«port.name.escapeDart»"
				«ENDFOR»
			>
			</«AngularDartRootProcessTemplate.classTag(p)»>
			«"'''"»
		)
		class «p.className»«entryPoint.id.escapeDart» implements OnActivate, CanReuse {
			
			«FOR port:entryPoint.inputPorts»
			«port.portParamType» «port.name.escapeDart»;
			«ENDFOR»
			
			bool loaded = false;
			String startPointId = '«p.startSIBs.get(0).id.escapeDart»';
			
			«p.className»«entryPoint.id.escapeDart»() {}
			
			@override
			void onActivate(_, RouterState current) async {
				startPointId = '«entryPoint.id.escapeDart»';
				«FOR port:entryPoint.inputPorts»
					«port.name.escapeDart» = «'''current.parameters['«port.name.escapeDart»'].toString()'''.portParamParser(port)»;
				«ENDFOR»
				loaded = true;
			}
			@override
			Future<bool> canReuse(RouterState current, RouterState next) {
			    return Future.value(false);
			}
		}
	«ENDFOR»
	'''
	}
	
}
