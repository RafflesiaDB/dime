package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.gui.gui.GUI
import info.scce.dime.generator.gui.TemplateHelper

/**
 * Helper class for the creation of the HTML template code for an Angular component.
 */
class AngularComponentHTMLTemplate extends GUIGenerator{
	
	extension TreeIteratorExtension<GUI> treeEx = new TreeIteratorExtension
	
	/**
	 * Creates the HTML template code for an Angular component.
	 * The template is surrounded by an security DIV tag which hides the
	 * component HTML code if a user has to be loaded.
	 * In this case the login component is displayed.
	 */
	def create(GUI engine)
	'''
	«IF new AngularDartCommonImports().getCurrentUser(engine) != null»
		<login-form 
			*ngIf="currentUser==null && showLogin == true"
			(signedin)="loadCurrentUser()"
			[modal]="modalDialog"
			></login-form>
		<template [ngIf]="currentUser!=null && showLogin == false">
	«ENDIF»
	«engine.getTemplate(genctx)»
	«IF new AngularDartCommonImports().getCurrentUser(engine) != null»
		</template>
	«ENDIF»
	'''
	
	/**
	 * Creates the plain HTML5 code for an Angular component in preview mode.
	 */
	def preview(GUI engine){
		//check if GUI is known
		if(treeEx.cache.contains(engine)) {
			return '''<p>«engine.title» RECURSION</p>'''
		}
		treeEx.cache.add(engine)
		'''«engine.getTemplate(genctx)»'''
	}
}