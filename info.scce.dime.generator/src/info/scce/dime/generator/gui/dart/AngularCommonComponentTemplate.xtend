package info.scce.dime.generator.gui.dart

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.AngularIncludedTemplate
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.TemplateHelper
import info.scce.dime.generator.gui.html.HTMLButtonTemplate
import info.scce.dime.generator.gui.html.HTMLFormTemplate
import info.scce.dime.generator.gui.html.HTMLNavBarTemplate
import info.scce.dime.generator.gui.rest.DartFileGuardGenerator
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.IS
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.gui.SpecialElement
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.Variable
import info.scce.dime.process.process.Process
import java.io.File

/**
 * Helper methods for the generation of Angular Dart components.
 */
class AngularCommonComponentTemplate extends GUIGenerator {
	
	/**
	 * Creates the input HTML attributes for an embedded GUI model Angular component tag, which is used
	 * in another Angular template code. E.g. a table or form component.
	 * For every input variable of the GUI model and every input port for embedded interaction processes
	 * an input attribute is rendered.
	 * For input ports, which are connected by a read edge to a variable or attribute the data binding is rendered.
	 * @param allTopLevel If enabled, all top level variables are included
	 */
	
	def CharSequence tagInputs(MovableContainer cec,GUI gui,boolean allTopLevel,GUICompoundView gcv)
	'''
	«cec.printStyle»
	«cec.printNgIfFor»
	[modalDialog]="modalDialog"
	[currentbranch]="currentbranch"
	[ismajorpage]="false"
	[runtimeId]="runtimeId"
	[guiId]="guiId"
	«FOR variable:gui.findVariables.filter[n|gcv.pairs.containsKey(n)].filter[isInput || allTopLevel]»
		[«DataDartHelper.getComponentInputName(variable)»]="«DataDartHelper.getDataAccess(variable,DataAccessType.^FOR,MODE.GET)»"
	«ENDFOR»
  	«FOR variable : cec.inputsVariables.filter[n|gcv.pairs.containsKey(n)]»
		[«DataDartHelper.getComponentInputName(variable)»]="«DataDartHelper.getDataAccess(variable,DataAccessType.^FOR,MODE.GET)»"
  	«ENDFOR»
	'''
	
	/**
	 * Creates the necessary import statements for an Angular Dart Component class.
	 * The inputs are collected from the used components, present in the given GUI model.
	 */
	def CharSequence imports(ModelElementContainer cec,GUI cgui,GUICompoundView gcv)
	'''
	import 'package:intl/intl.dart';
	import 'package:intl/date_symbol_data_local.dart';
	import 'package:markdown/markdown.dart' as markdown;
	import 'package:app/src/modal/Modal.dart' as modal;
	//Panel
	import 'package:app/src/panel/Panel.dart' as panel;

	//file guard imports
	«new DartFileGuardGenerator().getImports(cgui)»
	//special element imports
	«FOR sp : cec.find(SpecialElement)»
		«new AngularDartSpecialComponent().createImports(sp)»
	«ENDFOR»
	//table component imports
	«FOR sp : cec.find(Table).filter[it != cec]»
		«new AngularDartTableTemplate().createImports(sp,cgui)»
	«ENDFOR»
	//form component imports
	«FOR sp : cec.find(Form).filter[it != cec]»
		«new AngularDartFormTemplate().createImports(sp,cgui)»
	«ENDFOR»
	//GUI plug in component imports
	«FOR sp : cec.find(GUIPlugin)»
		«new AngularDartGUIPluginTemplate().getImports(sp)»
	«ENDFOR»
	«FOR sp : cec.find(SecuritySIB)»
		«new AngularDartSecuritySIBTemplate().imports(sp)»
	«ENDFOR»
	//GUI SIB imports
	«FOR sp:new AngularDartIncludedTemplate().distinct(cec.find(GUISIB))»
		«new AngularDartIncludedTemplate().createImports(sp)»
	«ENDFOR»
	«FOR sp:new AngularDartIncludedTemplate().distinct(cec.find(DispatchedGUISIB).filter(GUISIB).toList)»
		«FOR dispGUI : sp.gui.subGUIs»
			«new AngularDartIncludedTemplate().createImports(dispGUI)»
		«ENDFOR»
	«ENDFOR»
	//Embedded process SIB imports
	«cec.embeddedProcesses.map[AngularDartRootProcessTemplate.classImport(it)].join("\n")»
	
	'''
	
	/**
	 * Creates the needed variable declaration for the different components placed in the given model element container.
	 */
	def declaration(ModelElementContainer cec,GUI cgui,GUICompoundView gcv)
	'''
	// common declarations
	bool refresh;
	@Input()
	bool ismajorpage = false;
	@Input()
	String currentbranch;
	@Input()
	bool modalDialog = false;
	ChangeDetectorRef cdr;
	@Input()
	String runtimeId;
	@Input()
	String guiId;
	«new AngularDartCommonImports().createDeclarations(cgui,gcv)»
	// component Default Declaration
	«new HTMLTableTemplate().createTableDeclaration(cec)»
	«new AngularDartEventTemplate().createDeclaration(cgui,cec)»
	«FOR sp : cec.find(SpecialElement)»
		«new AngularDartSpecialComponent().createDeclaration(sp)»
	«ENDFOR»
	«FOR branchRep:cec.GUIBranchesMerged»
		// branch «branchRep.name» as «branchRep.element» in «branchRep.element.rootElement»
		@Output('«ConventionHelper.getEventName(branchRep.name)»') Stream<Map<String,dynamic>> get evt_«ConventionHelper.getEventName(branchRep.name)» => «ConventionHelper.getEventName(branchRep.name)».stream;
		StreamController<Map<String,dynamic>> «ConventionHelper.getEventName(branchRep.name)» = new StreamController();
	«ENDFOR»
	
	@ViewChildren(modal.Modal)
	List<modal.Modal> modals;
	«FOR guiSib : cec.find(GUISIB).filter[it.modal != null]»
		«new AngularIncludedTemplate().declaration(guiSib)»
	«ENDFOR»
	«new HTMLTabbingTemplate().createTabbingDeclaration(cec)»
	'''
	
	
	/**
	 * Creates the needed variable declaration for the different components placed in the given model element container.
	 */
	def restart(ModelElementContainer cec,GUI cgui,GUICompoundView gcv)
	'''
	«FOR form : cec.find(Form).filter[it != cec]»
		/// Form
		if(«new AngularDartEventTemplate().queryList(form)»!=null) {
			«new AngularDartEventTemplate().queryList(form)».forEach((n) => n.restartComponent());
		}
	«ENDFOR»
	«FOR table : cec.find(Table).filter[it != cec]»
	/// table
	if(«new AngularDartEventTemplate().queryList(table)»!=null) {
		«new AngularDartEventTemplate().queryList(table)».forEach((n) => n.restartComponent());
	}
	«ENDFOR»
	«FOR guiSIB:cec.find(GUISIB)»
			/// GUI «guiSIB.label»
			«IF guiSIB.modal!=null»
				«new AngularIncludedTemplate().restart(guiSIB)»
			«ENDIF»
			if(«new AngularDartEventTemplate().queryList(guiSIB)»!=null) {
				«new AngularDartEventTemplate().queryList(guiSIB)».forEach((n)=>n.restartComponent());
			}
				«IF guiSIB instanceof DispatchedGUISIB»
					«FOR subGUI : guiSIB.gui.subGUIs»
					/// Disp GUI «subGUI.title»
					if(sub_«new AngularDartEventTemplate().queryList(guiSIB,subGUI)»!=null) {
						sub_«new AngularDartEventTemplate().queryList(guiSIB,subGUI)».forEach((n)=>n.restartComponent());
					}
					«ENDFOR»
				«ENDIF»
	«ENDFOR»
	«new HTMLTabbingTemplate().createTabbingRestart(cec)»
	«FOR p : cec.find(ProcessSIB).map[it.proMod].filter(Process).toSet»
		/// Process «p.modelName»
		if(«new AngularDartEventTemplate().queryList(p)»!=null) {
			«new AngularDartEventTemplate().queryList(p)».forEach((n)=>n.restart());			
		}
	«ENDFOR»
	'''
	
	
	
	
	/**
	 * Creates the needed variable initialization for the different components placed in the given model element container.
	 */
	def constructor(ModelElementContainer cec)
	'''
		
		«FOR sp : cec.find(SpecialElement)»
			«new AngularDartSpecialComponent().createInitialization(sp)»
		«ENDFOR»
	  	
	  	«FOR branchRep : cec.GUIBranchesMerged»
  			// branch «branchRep.name» declaration
  			if(this.«ConventionHelper.getEventName(branchRep.name)»!=null) {
	  			this.«ConventionHelper.getEventName(branchRep.name)» = new StreamController<Map<String,dynamic>>();  				
  			}
  		«ENDFOR»
	
	'''
	
	/**
	 * Creates the needed code of the Angular component live cycle event.
	 * The generated code is executed, after the Angular component inputs are loaded but
	 * before the template is initially rendered.
	 */
	def onInit(ModelElementContainer cec)
	'''
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		«FOR sp : cec.find(SpecialElement)»
			«new AngularDartSpecialComponent().createOnInit(sp)»
		«ENDFOR»
		
	'''
	
	/**
	 * Creates the needed code of the Angular component live cycle event.
	 * The generated code is executed, after the user has successfully authenticated himself
	 */
	def afterLogin(ModelElementContainer cec)
	'''
		«FOR sp : cec.find(SpecialElement)»
			«new AngularDartSpecialComponent().afterLogin(sp)»
		«ENDFOR»
		
	'''
	
	/**
	 * Creates the needed methods for the different components placed in the given model element container.
	 */
	def methods(ModelElementContainer cec,GUI cgui,GUICompoundView gcv)
	'''
		@override
		String getRuntimeId() => this.runtimeId;
		
		«new AngularDartEventTemplate().createMethods(cgui,cec)»
		
		«FOR IS cis:cgui.find(IS).filter[n|!n.sourceElement.getIsEnum()].filter[n|!n.exactMatch]»
		bool check«cis.id.escapeDart»Type(dynamic obj)
		{
			if(obj == null)return false;
			return obj is«IF cis.negate»!«ENDIF» «cis.getClassName(true)»;
		}
		«ENDFOR»
		
		«FOR cs : cec.find(ControlSIB)»
			«new AngularDartControlSIBTemplate().createMethod(cs)»
		«ENDFOR»
		
	
		«new HTMLButtonTemplate().createEventHandlers(cec,gcv)»
		
		«new HTMLFileTemplate().createMethods(cgui,gcv)»
	
		«new HTMLFormTemplate().createFormMethods(cec)»
		
		«new HTMLTabbingTemplate().createTabbingMethods(cec)»
		
		«new HTMLNavBarTemplate().createMethods(cec)»
		
		«new AngularCreateOnWriteTemplate().createMethods(cgui.find(Variable), cgui, gcv,cec instanceof Form)»
		
		«FOR sp : cec.find(SpecialElement)»
			«new AngularDartSpecialComponent().createMethods(sp)»
		«ENDFOR»
	
	  	«FOR sp : cec.find(GUIPlugin)»
			// GUI plug in «sp.label»
			«new AngularDartGUIPluginTemplate().createMethods(sp,true)»
		«ENDFOR»
		
		«FOR dispGuiSIb : cec.find(DispatchedGUISIB)»
			//Dispatched GUI SIB «dispGuiSIb.label»
			«new AngularIncludedTemplate().dispatchMethod(cec,dispGuiSIb)»
		«ENDFOR»
		
		«FOR guiSib : cec.find(GUISIB)»
			//GUI SIB «guiSib.label»
			«new AngularIncludedTemplate().methods(cec,guiSib)»
		«ENDFOR»
		
	'''
	
	/**
	 * Creates the needed Angular Dart component interface declaration
	 * for the different components placed in the given model element container.
	 * This includes the included CSS file specification and especially the directives
	 * used in the given model element container like, GUI SIB, plug ins,... which are
	 * Angular Component as well.
	 */
	def CharSequence otherSpecifications(ModelElementContainer cec,GUI gui)
	'''
		«IF !gui.additionalStylesheets.empty»
			styleUrls: const [
			«FOR style:gui.additionalStylesheets SEPARATOR ","»
				«IF style.startsWith("http")»
				'«style»'
				«ELSE»
				'package:app/src/gui/css/«gui.title.escapeDart»/«new File(style).name»'
				«ENDIF»
			«ENDFOR»
				],
		«ENDIF»
		pipes: const [«new AngularDartCommonImports().createPipes»],
		encapsulation: ViewEncapsulation.None,
		exports: const [
			«FOR data:genctx.usedDatas SEPARATOR ","»
			«DyWASelectiveDartGenerator.prefix(data)».«data.modelName.escapeDart.toFirstUpper»CastUtil
			«ENDFOR»
		],
		directives: const [
		«new AngularDartCommonImports().createDirectives»,
		«cec.embeddedProcesses.map['''«AngularDartRootProcessTemplate.className(it)»SIB,'''].join»
		«new HTMLTableTemplate().createDependencies(cec,gui)»
		«new HTMLFormTemplate().createDependencies(cec,gui)»
		«FOR sp : new AngularDartIncludedTemplate().distinct(cec.find(GUISIB))»
			«TemplateHelper.classFormat(new AngularDartComponentTemplate().getGUIName(sp.gui).toString).toFirstUpper»,
		«ENDFOR»
		«FOR sp : new AngularDartIncludedTemplate().distinct(cec.find(DispatchedGUISIB).filter(GUISIB).toList)»
				«FOR dispGUI : sp.gui.subGUIs»
					«TemplateHelper.classFormat(new AngularDartComponentTemplate().getGUIName(dispGUI).toString).toFirstUpper»,
				«ENDFOR»
		«ENDFOR»
		«FOR sp : cec.find(GUIPlugin)»
			«new AngularDartGUIPluginTemplate().getPluginClassName(sp)»,
		«ENDFOR»
		«FOR sp : cec.find(SecuritySIB)»
			«new AngularDartSecuritySIBTemplate().component(sp)»,
		«ENDFOR»
		],
	'''
}