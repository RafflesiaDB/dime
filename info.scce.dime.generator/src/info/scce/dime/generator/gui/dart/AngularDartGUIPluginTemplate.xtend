package info.scce.dime.generator.gui.dart

import graphmodel.Node
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.gUIPlugin.AbstractParameter
import info.scce.dime.gUIPlugin.ComplexInputParameter
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.gUIPlugin.DartPlugin
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.GenericInputParameter
import info.scce.dime.gUIPlugin.GenericParameter
import info.scce.dime.gUIPlugin.InputParameter
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.gUIPlugin.PrimitiveInputParameter
import info.scce.dime.gUIPlugin.PrimitiveParameter
import info.scce.dime.generator.gui.AngularIFFORTemplate
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.html.HTMLCSSTemplate
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.FileUtils
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.helper.ElementCollector
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import org.eclipse.core.runtime.IPath
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Variable

/**
 * Template to create an Angular Dart component class file for a GUI plug in component.
 */
class AngularDartGUIPluginTemplate extends GUIGenerator {
	
	/**
	 * Creates the import statement for a given GUI plug in to import the corresponding GUI plug in Angular component class file
	 */
	def getImports(GUIPlugin gp)
	'''import 'package:app/src/plugin/«getPluginClassName(gp)».dart';'''
	
	/**
	 * Creates the class name for a given GUI plug in to import the corresponding GUI plug in Angular component class
	 */
	def getPluginClassName(GUIPlugin cgp)
	'''Plugin«cgp.label.escapeDart»«cgp.id.escapeDart»«cgp.rootElement.title.escapeDart»«cgp.rootElement.id.escapeDart»'''
	
	/**
	 * Creates the HTML tag name for a given GUI plug in to use the corresponding GUI plug in Angular component template code
	 */
	def getPluginSelector(GUIPlugin cgp)
	'''plugin-«cgp.id.escapeDart»«cgp.rootElement.id.escapeDart»'''
	
	/**
	 * Generates the Angular Dart component class file for a given GUI plug in
	 */
	def createComponents(GUIPlugin gp,GUICompoundView gcv)
	'''
	// GUI plug in «gp.label»
	//import 'dart:html' as html;
	import 'dart:async';
	import 'dart:js' as js;
	import 'package:app/src/models/Selectives.dart';
	«FOR data : genctx.usedDatas»
	import 'package:app/src/data/«data.modelName.escapeDart».dart' as «DyWASelectiveDartGenerator.prefix(data)»;
	«ENDFOR»
	
	import 'package:angular/angular.dart';
	«IF gp.plugin.dartPlugin»
	import '«gp.plugin.path»/«gp.funct.functionName».dart';
	«ENDIF»
	@Component(
			selector: '«getPluginSelector(gp)»',
			directives: const [
				coreDirectives
				«IF gp.plugin.dartPlugin»
					,«gp.funct.functionName»
				«ENDIF»
			],
			templateUrl: '«getPluginClassName(gp)».html'
	)
	class «getPluginClassName(gp)»«IF !gp.plugin.dartPlugin» implements AfterViewChecked«IF gp.funct.isReCalled», OnChanges«ENDIF»«ENDIF» {
		
		«IF gp.plugin.dartPlugin»
		//events
		@ViewChildren(«gp.funct.functionName»)
		List<«gp.funct.functionName»> children;
		«ELSE»
		bool isLoaded = false;
		«ENDIF»
		// outputs
		«FOR out:gp.funct.outputs»
		// branch: «out.outputName»
		@Output('action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin')
		Stream<Map<String,dynamic>> get evt_action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin => action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.stream;
		StreamController<Map<String,dynamic>> action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin;
		«ENDFOR»
		// sync callbacks
		«FOR param:gp.funct.parameters.filter(InputParameter).filter[n|n.isIsSync].map[toParameter]»
		// synchronized input port: «param.name»
		@Output('sync«gp.id.escapeDart»«param.name»event')
		Stream<dynamic> get evt_sync«gp.id.escapeDart»«param.name»event => sync«gp.id.escapeDart»«param.name»event.stream;
		StreamController<dynamic> sync«gp.id.escapeDart»«param.name»event;
		«ENDFOR»
		//Input Parameter
		«FOR param:gp.funct.parameters.filter(InputParameter).map[toParameter]»
			// input port «param.name»
			@Input()
			«IF param instanceof PrimitiveParameter»
				«IF param.isIsList»List<«ENDIF»«param.type.toData.dartType»«IF param.isIsList»>«ENDIF» «param.name.escapeDart»;
			«ELSE»
				«IF param.isDataForParameter(gp)»
				«param.readDataForParameter(gp).selective(param.isList,gcv)» «param.name.escapeDart»;
				«ELSE»
				dynamic «param.name.escapeDart»;
				«ENDIF»
			«ENDIF»
		«ENDFOR»
	
		«getPluginClassName(gp)»()
		{
			// sync callbacks
			«FOR param:gp.funct.parameters.filter(InputParameter).filter[n|n.isIsSync].map[toParameter]»
				this.sync«gp.id.escapeDart»«param.name»event = new StreamController<dynamic>();
			«ENDFOR»
			// outputs
			«FOR out:gp.funct.outputs»
				this.action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin = new StreamController<Map<String,dynamic>>();
			«ENDFOR»
			// input params
			«FOR param:gp.funct.parameters.filter(PrimitiveInputParameter).map[toParameter].filter(PrimitiveParameter)»
				this.«param.name.escapeDart» = «IF param.isIsList»new List()«ELSE»«param.type.toData.defaultValue»«ENDIF»;
			«ENDFOR»
			«FOR param:gp.funct.parameters.filter(ComplexInputParameter).filter[isIsList]»
				this.«param.parameter.name.escapeDart» = new DIMEList();
			«ENDFOR»
		}
		«IF !gp.plugin.dartPlugin»
			/// triggers the connected java script function
			///
			/// passes all input variables and callbacks to the js function
			@override
			void ngAfterViewChecked()
			{
				if(!isLoaded) {
					isLoaded = true;
					callPluginFunction();					
				}
			}
			
			Map<String,dynamic> _toDartSimpleObject(thing) {
		 		if (thing is js.JsObject) {
				  Map<String,dynamic> res = new Map();
				  js.JsObject o = thing as js.JsObject;
				  Iterable<dynamic> k = (js.context['Object'].callMethod('keys', [o]));
				  	k.where((k)=>k is String).forEach((k) {
				  	res[k] = o[k];
				  });
				  return res;
			
				} else {
					return {};
				}
			}
			
			«IF gp.funct.isReCalled»
			/// triggers the connected java script function again
			/// if one of the binded input data changes
			///
			/// passes all input variables and callbacks to the js function
			@override
			ngOnChanges(Map<String, SimpleChange> changes) {
			    callPluginFunction();
			}
			
			
			«ENDIF»
		
		/// triggers the connected java script function
		///
		/// passes all input variables and callbacks to the js function
		void callPluginFunction()
		{
			Map<String,dynamic> cache = new Map();
			js.context.callMethod('«gp.label»',[
			«FOR param:gp.funct.parameters.filter(InputParameter) SEPARATOR ","»
				«param.toParameter.name.escapeDart»
				«IF param.isIsSync»
					,sync«gp.id.escapeDart»«param.toParameter.name»Function
				«ENDIF»
			«ENDFOR»
			«IF hasOutputs(gp)»
				«IF !gp.funct.parameters.filter(InputParameter).empty»,«ENDIF»terminate«gp.id.escapeDart»
			«ENDIF»
			]);
		}
		
		
		«FOR param:gp.funct.parameters.filter(PrimitiveInputParameter).filter[isIsSync].map[toParameter]»
		/// synchronization callback
		///
		/// to update the variable in the component
		void sync«gp.id.escapeDart»«param.name»Function(dynamic value)
		{
			this.sync«gp.id.escapeDart»«param.name»event.add({'«param.name»':value});
		}
		«ENDFOR»
		
		
		«FOR param:gp.funct.parameters.filter(ComplexInputParameter).filter[isIsSync].map[toParameter].filter(ComplexParameter)»
		/// synchronization callback
		///
		/// to update the variable in the component
		void sync«gp.id.escapeDart»«param.name.escapeDart»Function({'«param.name»':value})
		{
			
			«IF param.toInputPort(gp)!=null && param.isDataForParameter(gp)»
			if(value!=null) {
				«IF param.isList»
					«param.readDataForParameter(gp).selective(param.isIsList,gcv)» selective = new «param.readDataForParameter(gp).selective(param.isIsList,gcv)»();
					value.forEach((n) => selective.add(«param.readDataForParameter(gp).selective(param.isIsList,gcv)».fromJSOG(value)));
				«ELSE»
					«param.readDataForParameter(gp).selective(param.isIsList,gcv)» selective = «param.readDataForParameter(gp).selective(param.isIsList,gcv)».fromJSOG(value);
				«ENDIF»
				this.sync«gp.id.escapeDart»«param.name»event.add({
					'«param.name.escapeString»':selective
				});
			}
			«ENDIF»
		}
		«ENDFOR»
		«ELSE»
			«FOR param:gp.funct.parameters.filter(InputParameter).filter[isIsSync].map[toParameter]»
			/// synchronization callback for dart plugin
			///
			/// to update the variable in the component
			void sync«gp.id.escapeDart»«param.name»Function(Map<String,dynamic> value)
			{
				this.sync«gp.id.escapeDart»«param.name»event.add(value);
			}
			«ENDFOR»
		«ENDIF»
		
		«IF hasOutputs(gp) && !gp.plugin.dartPlugin»
			«IF gp.plugin.dartPlugin»
				«FOR out:gp.funct.outputs»
				void triggeraction«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin(Map<String,dynamic> values) {
					Map<String,dynamic> map = new Map();
					«FOR p:out.parameters»
						«IF p.isComplex»
							if(values['«p.name.escapeString»']!=null) {
								«IF p.isList»
								if(values['«p.name»'] is! BaseSelectiveList) {
									map['«p.name.escapeString»']=BaseSelectiveList.fromDelegates(values['«p.name.escapeString»']);
								} else {
									map['«p.name.escapeString»']=values['«p.name.escapeString»'];
								}
								«ELSE»
								if(values['«p.name.escapeString»'] is! BaseSelective){
									map['«p.name.escapeString»']=BaseSelective.fromDelegate(values['«p.name.escapeString»']);
								} else {
									map['«p.name.escapeString»']=values['«p.name.escapeString»'];
								}
								«ENDIF»
							}
						«ELSE»
							map['«p.name.escapeString»']=values['«p.name.escapeString»'];
						«ENDIF»
					«ENDFOR»
					this.action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.add(map);
				}
				«ENDFOR»
			«ELSE»
			/// termination callback
			///
			/// to update the variable in the component and propagate
			/// the event to the parent component
			void terminate«gp.id.escapeDart»(dynamic jsValue)
			{
				Map<String,dynamic> value = _toDartSimpleObject(jsValue);
				String branchName = value["branchName"];
				«FOR out:gp.funct.outputs»
				if('«out.outputName»'== branchName){
					var values = value['values'];
					Map<String,dynamic> map = new Map();
					«FOR p:out.parameters»
						«IF p.isComplex»
						if(values['«p.name»']!=null) {
							«IF p.isList»
							map['«p.name.escapeString»']=BaseSelectiveList.fromDelegates(values['«p.name.escapeString»']);
							«ELSE»
							map['«p.name.escapeString»']=BaseSelective.fromDelegate(values['«p.name.escapeString»']);
							«ENDIF»
						}
						«ELSE»
						map['«p.name.escapeString»']=values['«p.name.escapeString»'];
						«ENDIF»
					«ENDFOR»
					this.action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.add(map);
				}
				«ENDFOR»
			}
			«ENDIF»
		«ENDIF»
		
		«FOR listener:gp.funct.events»
		/// receives the events for the «listener.name» listener
		void recieve«listener.name»Event(Map<String,dynamic> map)
		{
			«IF gp.plugin.dartPlugin»
			this.children.forEach((n)=>n.«listener.name»(
				«FOR param:listener.parameters SEPARATOR ","»
					map['«param.name.escapeString»']
				«ENDFOR»
			));
			«ELSE»
			//write the received data
			Map<String,dynamic> cache = new Map();
			(js.context['«listener.name»'] as js.JsFunction).apply([
			«FOR param:listener.parameters SEPARATOR ","»
				map['«param.name.escapeString»']«IF param.isComplex»«IF param.isList».map((n)=>n.toJSOG(cache)).toList()«ELSE»!=null?map['«param.name.escapeString»'].toJSOG(cache):null«ENDIF»«ENDIF»
			«ENDFOR»
			]);
			«ENDIF»
		}
		«ENDFOR»
	}
	
	'''
	
	
	
	def toInputPort(ComplexParameter parameter, GUIPlugin plugin){
		if(plugin.IOs.findFirst[name.equals(parameter)]!=null){
			plugin.IOs.findFirst[name.equals(parameter)] as Node			
		}
		null
	}

	
	def getToParameter(InputParameter ip) {
		if(ip instanceof ComplexInputParameter)return ip.parameter
		if(ip instanceof PrimitiveInputParameter)return ip.parameter
		if(ip instanceof GenericInputParameter)return ip.parameter
	}
	
	def boolean getIsComplex(InputParameter parameter){
		parameter instanceof ComplexInputParameter || parameter instanceof GenericInputParameter
	}
	
	def boolean getIsComplex(AbstractParameter parameter){
		parameter instanceof ComplexParameter || parameter instanceof GenericParameter
	}
	
	def CharSequence selective(Node data,boolean isList,GUICompoundView gcv)
	{
		val v = gcv.pairs.get(data)
		if(v !== null) {
			if(v instanceof ComplexTypeView) {
				return v.selectiveClassName(isList)
			}
			if(v instanceof ComplexFieldView) {
				return v.selectiveClassName(isList)
			}
		}
		if(data instanceof PrimitiveAttribute) {
			return data.attribute.dataType.dartType(isList)
		}
		if(data instanceof PrimitiveVariable) {
			return data.dataType.toData.dartType(isList)
		}
		throw new IllegalStateException('''No fitting data: «data»''')
	}
	
	def String dartType(PrimitiveType d,boolean isList)
	{
		var s = ""
		if(isList) s ="DIMEList<"	
		switch(d) {
			case BOOLEAN: return "bool"
			case FILE: return "FileReference"
			case INTEGER: return "int"
			case REAL: return "double"
			case TEXT: return "String"
			case TIMESTAMP: return "DateTime"
		}
		
	}
	
	
	/**
	 * Helper method to get the default value for a GUI plug in primitive type
	 */
	private def defaultValue(PrimitiveType type) {
		switch(type)
		{
			case BOOLEAN: return "false"
			case INTEGER: return "0"
			case REAL: return "0.0"
			case TEXT : return "\"\""
			case TIMESTAMP : return "new DateTime.now()"
		}
	}
	
	/**
	 * Helper method to get the type for a GUI plug in primitive type
	 */
	private def dartType(PrimitiveType type) {
		switch(type)
		{
			case BOOLEAN: return "bool"
			case INTEGER: return "int"
			case REAL: return "double"
			case TEXT : return "String"
			case TIMESTAMP : return "DateTime"
			case FILE: return "FileReference"
		}
	}
	
	/**
	 * Generates the HTML template code for the GUI plug in Angular Dart component class
	 */
	def createHTML(GUIPlugin gp)'''«getHTMLContent(gp)»'''
	
	/**
	 * Generates the HTML tag for the GUI plug in used in another Angular component template code
	 */
	def CharSequence create (GUIPlugin gp)
	'''
	«IF preview»
		«getHTMLContent(gp)»
	«ELSE»
		<«getPluginSelector(gp)»
			«gp.printStyle»
		 	«gp.printNgIfFor»
			«gp.guiPluginInputs»
			«FOR out:gp.funct.outputs»
				(action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin)="terminate«gp.id.escapeDart»«out.outputName.escapeDart»($event)"
			«ENDFOR»
			«FOR param:gp.funct.parameters.filter(InputParameter).filter[isIsSync].map[toParameter].filter[!(inputForParameter(gp) instanceof InputStatic)]»
				(sync«gp.id.escapeDart»«param.name»event)="«DataDartHelper.getBindedDataName(param.inputForParameter(gp),DataAccessType.^FOR,MODE.SET_INIT)»($event['«param.name.escapeString»'])"
			«ENDFOR»
		>
		«FOR arg:gp.arguments»
			<ph «arg.blockName.escapeDart.argumentTransclusion('''«IF gp.plugin.isDartPlugin»«gp.id.escapeDart»«ENDIF»''')»>
				«new BaseTemplate().baseContent(ElementCollector.getElementsV(arg.allNodes),true)»
			</ph>
		«ENDFOR»
		</«getPluginSelector(gp)»>
	«ENDIF»
	'''
	
	/**
	 * Returns the list of all variables and attributes connected to the given button by submission edges
	 */
	def Variable getParentVariable(IO port)
	{
		if(port.getIncoming(Read).empty) {
			return null
		}
		val node = port.getIncoming(Read).get(0).sourceElement
		if(node instanceof Attribute) {
			return node.container as Variable
		}
		node as Variable
	}
	
	
	
	/**
	 * Generates the methods used in the parent Angular Dart component 
	 */
	def createMethods(GUIPlugin gp,boolean isForm)
	'''
	«FOR out:gp.funct.outputs»
		/// bracnh callback for branch «out.outputName»
		void terminate«gp.id.escapeDart»«out.outputName.escapeDart»(Map<String,dynamic> values) {
			Map<String,dynamic> map = new Map();
			«FOR p:out.parameters»
			map['«p.name.escapeString»']=values['«p.name.escapeString»'];
			«ENDFOR»
			this.«ConventionHelper.getEventName(out.outputName)».add(map);
		}
	«ENDFOR»
	'''
	
	/**
	 * Helper method to receive the GUI plug in function
	 */
	static def Function funct(GUIPlugin plugin){
		return (plugin as GUIPlugin).function as Function;
	}
	
	/**
	 * Helper method to receive the GUI plug in function
	 */
	static def Plugin plugin(GUIPlugin plugin){
		return ((plugin as GUIPlugin).function as Function).eContainer as Plugin;
	}
	
	/**
	 * Helper method to receive the GUI plug in function
	 */
	static def boolean isDartPlugin(Plugin plugin){
		return plugin instanceof DartPlugin
	}
	
	/**
	 * Helper method to receive HTML content of the GUI plug in, if available
	 * 
	 */
	private def getHTMLContent(GUIPlugin cgp)
	{
		var plugin = cgp.funct.eContainer as Plugin;
		if(plugin.template != null){
			if(plugin instanceof DartPlugin){
				return cgp.funct.dartPluginHTML(plugin.template?.placholders?.placeholders!=null,cgp)
			}
			val we = new WorkspaceExtension
			val p = plugin.template.file.path;
			val pr = we.getProject(cgp.rootElement)
			return new String (Files.readAllBytes(FileUtils.toFile(p, pr).toPath()));
		}
	}
	
	def String dartPluginHTML(Function function,boolean placeholder,GUIPlugin gp)
	'''
	<«function.functionName.escapeString»
		«FOR out:function.outputs»
			(«out.outputName.escapeString»)="action«gp.label.escapeDart.toLowerCase»«out.outputName.escapeDart.toLowerCase»guiplugin.add($event)"
		«ENDFOR»
		«FOR param:function.parameters.filter(InputParameter).map[toParameter]»
			[«param.name.escapeString»]="«param.name.escapeDart»"
		«ENDFOR»
		
		«FOR param:gp.funct.parameters.filter(InputParameter).filter[n|n.isIsSync].map[toParameter]»
			«IF !(param.inputForParameter(gp) instanceof InputStatic) && !param.inputForParameter(gp).incoming.empty»
			(sync«param.name»)="sync«gp.id.escapeDart»«param.name»Function($event)"
			«ENDIF»
		«ENDFOR»
	>
	«IF placeholder»
		«FOR arg:gp.arguments»
		<ph «arg.blockName.escapeString.argumentTransclusion("")»>
			<ng-content «arg.blockName.escapeDart.placeholderTransclusion(gp.id.escapeDart)»></ng-content>
		</ph>
		«ENDFOR»
	«ENDIF»
	</«function.functionName.escapeString»>
	'''
	
	/**
	 * Helper method to copy the JS and CSS resources, defined in the given GUI plug in
	 * to the defined resourcePath
	 */
	def copyResources(GUIPlugin cgp,IPath pluginPath)
	{
		var plugin = cgp.funct.eContainer as Plugin;
		if(plugin instanceof DartPlugin) {
				extension val WorkspaceExtension = new WorkspaceExtension
			var target = pluginPath.append("/" + plugin.path.escapeDart);
			if(plugin.style != null){
				for(String path:plugin.style.files.map[n|n.path].filter[n|!n.startsWith("http")] ){
					var sourceFile = FileUtils.toFile(path, plugin.project);
					copyFile(sourceFile,target);	
				}
			}
			if(plugin.script != null){
				for(String path:plugin.script.files.map[n|n.path].filter[n|!n.startsWith("http")] ){
					var sourceFile = FileUtils.toFile(path, plugin.project);
					if(!plugin.functions.empty){
						copyFile(sourceFile,target,plugin.functions.get(0).functionName+".dart");							
					}
					else{
						copyFile(sourceFile,target);	
					}
				}
			}
			if(plugin.template != null){
				var sourceFile = FileUtils.toFile(plugin.template.file.path, plugin.project);
				copyFile(sourceFile,target);	
			}
		}
	}
	
	/**
	 * Helper method to copy the additional JS and CSS resources, defined in the given GUI model in
	 * to the defined resourcePath
	 */
	def copyResources(GUI gui, IPath cssPath) {
		var targetCSS = cssPath.append('''/css/«gui.title.escapeDart»''');
		for(path: gui.additionalStylesheets.filter[!startsWith("http")]) {
			val sourceFile = FileUtils.toFile(path, gui as GUI);
			copyFile(sourceFile, targetCSS);
		}
	}
	
	/**
	 * Helper method to copy a file to the defined path
	 */
	def copyFile(File sourceFile,IPath target,String newFileName){
		if (!new File(target.toFile.path).exists()) {
			new File(target.toFile.path).mkdirs;					
		}
		try {
			Files.copy(sourceFile.toPath, new File(target.toFile.path +"/"+ newFileName).toPath, StandardCopyOption.REPLACE_EXISTING);									
		}
		catch(Exception e) {
			e.printStackTrace;
		}
	}
	
	/**
	 * Helper method to copy a file to the defined path
	 */
	def copyFile(File sourceFile,IPath target){
		copyFile(sourceFile,target,sourceFile.name)
	}
	
	
	private def inputForParameter(AbstractParameter param,GUIPlugin gp)
	{
		gp.IOs.filter[name.equals(param.name)].get(0)
		
	}
	
	private def isDataForParameter(AbstractParameter param,GUIPlugin gp)
	{
		if(gp.IOs.filter[name.equals(param.name)].empty)return false
		if(gp.IOs.filter[name.equals(param.name)].get(0).getIncoming(Read).empty)return false
		return true
	}
	
	private def readDataForParameter(AbstractParameter param,GUIPlugin gp)
	{
		gp.IOs.filter[name.equals(param.name)].get(0).getIncoming(Read).get(0).sourceElement as Node
	}
	
	
	private def hasOutputs(GUIPlugin gp)
	{
		return !gp.funct.outputs.empty;
	}
}