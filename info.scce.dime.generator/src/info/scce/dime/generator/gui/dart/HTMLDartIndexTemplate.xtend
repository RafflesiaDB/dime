package info.scce.dime.generator.gui.dart

import info.scce.dime.dad.dad.DAD
import info.scce.dime.gUIPlugin.DartPlugin
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.generator.gui.html.HTMLLoadingScreen
import info.scce.dime.generator.gui.utils.FileUtils
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.GUIPlugin
import java.util.Collection
import java.util.Set
import java.io.File

/**
 * Template for the main index.html file
 */
class HTMLDartIndexTemplate extends GUIGenerator {
	
	final static Set<String> APPLE_FAVICONS = #{
		"57", "60", "72", "76", "114", "120", "144", "152", "180"
	}
	
	final static Set<String> ANDROID_FAVICONS = #{
		"192"
	}
	
	final static Set<String> FAVICONS = #{
		"32", "96", "16"
	}
	
	final static Set<String> MS_FAVICONS = #{
		"144"
	}
	
	/**
	 * Generates the index.html file code
	 */
	def create(String appTitle, Collection<GUIPlugin> plugins,String timeStamp,boolean compressedCSS,boolean preCompressedJS,boolean postCompressedJS){
		
		'''
			<!doctype html>
            <!-- production mode -->
			<html lang="en">
			  <head>
			  <meta charset="UTF-8">
			  <base href="/">
			    <title>«appTitle»</title>
			    «IF !genctx.dad.getFaviconDirectory().nullOrEmpty»
			    «{
			    	val faviconDir = FileUtils.toFile(genctx.dad.getFaviconDirectory(), genctx.dad); 
			    	'''
			    	«FOR fav:APPLE_FAVICONS.filter[fav|filePresent('''apple-icon-«fav»x«fav».png''',faviconDir)]»
				    <link rel="apple-touch-icon" sizes="«fav»x«fav»" href="favicon/apple-icon-«fav»x«fav».png">
				    «ENDFOR»
				    «FOR fav:ANDROID_FAVICONS.filter[fav|filePresent('''android-icon-«fav»x«fav».png''',faviconDir)]»
				    <link rel="icon" type="image/png" sizes="«fav»x«fav»"  href="favicon/android-icon-«fav»x«fav».png">
				    «ENDFOR»
				    «FOR fav:FAVICONS.filter[fav|filePresent('''favicon-«fav»x«fav».png''',faviconDir)]»
				    <link rel="icon" type="image/png" sizes="«fav»x«fav»"  href="favicon/favicon-«fav»x«fav».png">
				    «ENDFOR»
				    «FOR fav:MS_FAVICONS.filter[fav|filePresent('''ms-icon-«fav»x«fav».png''',faviconDir)]»
				    <meta name="msapplication-TileImage" content="favicon/ms-icon-«fav»x«fav».png">
				    «ENDFOR»
			    	'''
			    }»
			    «ENDIF»
			    <meta name="msapplication-TileColor" content="#da532c">
			    <meta name="theme-color" content="#ffffff">
	
			    <link type="text/css" rel="stylesheet" href="css/dime.min.css" />
			    <link type="text/css" rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
			    «IF compressedCSS»
			    <link type="text/css" rel="stylesheet" href="css/combined/combined.css" />
			    «ENDIF»
			    «FOR s : genctx.dad.additionalStylesheets.filter[startsWith("http")]»
			    	<link type="text/css" rel="stylesheet" href="«s»" />
			    	«ENDFOR»
			    	«FOR s : genctx.dad.preScripts.filter[startsWith("http")]»
			    	<script type="script" src="«s»"></script>
			    	«ENDFOR»
			    «FOR p:plugins.map[p|p.function as Function].filter[p|!(p.eContainer instanceof DartPlugin)].filter[p|(p.eContainer as Plugin).style !== null]»
					«FOR path:(p.eContainer as Plugin).style.files.map[path].filter[startsWith("http")]»
					    		<link type="text/css" rel="stylesheet" href="«path»"/>
					«ENDFOR»
				«ENDFOR»
				«IF preCompressedJS»
				<script type="text/javascript" src="js/precombined.min.js"></script>
				«ENDIF»
				<!-- Workarround SubmitEvent Bug -->
				<script>
				if (typeof dartNativeDispatchHooksTransformer == "undefined") dartNativeDispatchHooksTransformer=[];
				dartNativeDispatchHooksTransformer.push(
				function(hooks) {
				  var getTag = hooks.getTag;
				  var quickMap = {
				      "SubmitEvent": "Event",
				  };
				  function getTagFixed(o) {
				    var tag = getTag(o);
				    return quickMap[tag] || tag;
				  }
				  hooks.getTag = getTagFixed;
				});
				</script>
				<!-- Workarround Chrome 85/86 Bug -->
				<script>
				if (typeof window.MemoryInfo == "undefined") {
				if (typeof window.performance.memory != "undefined") {
					window.MemoryInfo = function () {};
					window.MemoryInfo.prototype = window.performance.memory.__proto__;
				}
				}
				</script>
				<script defer src="main.dart.js"></script>
			
			    <meta name="viewport" content="width=device-width, initial-scale=1">
			  </head>
			  <body«genctx.dad.settings»>
			    		<app>«new HTMLLoadingScreen().create()»</app>
			    		<script type="text/javascript" src="js/dime.min.js"></script>
			    		<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js" ></script>
					«IF postCompressedJS»
					<script type="text/javascript" src="js/postcombined/postcombined.js"></script>
					«ENDIF»
					«FOR s : genctx.dad.postScripts.filter[startsWith("http")]»
						<script type="text/javascript" src="«s»"></script>
					«ENDFOR»
					«FOR gui : genctx.usedGUIs»
						<!-- additional javascript for gui «gui.title» -->
						«FOR scriptPath: gui.additionalJavaScript.filter[startsWith("http")]»
							<script type="text/javascript" src="«scriptPath»"></script>
						«ENDFOR»
			  		«ENDFOR»
					«FOR p:plugins.map[function as Function].filter[p|!(p.eContainer instanceof DartPlugin)].filter[p|(p.eContainer as Plugin).script != null]»
						«FOR path:(p.eContainer as Plugin).script.files.map[path].filter[startsWith("http")]»
								<script type="text/javascript" src="«path»"></script>
						«ENDFOR»
					«ENDFOR»
			  </body>
			</html>
		'''
	}
		
	def boolean filePresent(String fav, File file) {
		if(file.isDirectory) {
			return file.listFiles.exists[name.equals(fav)]
		}
		false
	}
	
	def getSettings(DAD dad){
		if(dad.advacedSettings!=null){
			return '''«IF !dad.advacedSettings.bodyClass.nullOrEmpty» class="«dad.advacedSettings.bodyClass»" «ENDIF»«IF !dad.advacedSettings.bodyStyle.nullOrEmpty» style="«dad.advacedSettings.bodyStyle»" «ENDIF»'''
		}
		''''''
	}
	
}
