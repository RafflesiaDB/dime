package info.scce.dime.generator.gui.dart;

import static info.scce.dime.gui.helper.GUIExtension.lastModified2;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;
import graphmodel.GraphModel;
import graphmodel.Node;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.dad.dad.StartupProcessPointer;
import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.Data;
import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserAssociation;
import info.scce.dime.data.data.UserType;
import info.scce.dime.gUIPlugin.DartPlugin;
import info.scce.dime.gUIPlugin.Function;
import info.scce.dime.gUIPlugin.Plugin;
import info.scce.dime.generator.dad.GenerationContext;
import info.scce.dime.generator.data.DartDataGenerator;
import info.scce.dime.generator.gui.data.SelectiveExtension;
import info.scce.dime.generator.gui.html.HTMLImageTemplate;
import info.scce.dime.generator.gui.html.HTMLNotificationComponent;
import info.scce.dime.generator.gui.rest.DartFileGuardGenerator;
import info.scce.dime.generator.gui.rest.DartTOGeneratorHelper;
import info.scce.dime.generator.gui.rest.DyWADartGenerator;
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView;
import info.scce.dime.generator.gui.rest.model.ComplexFieldView;
import info.scce.dime.generator.gui.rest.model.ComplexTypeView;
import info.scce.dime.generator.gui.rest.model.GUICompoundView;
import info.scce.dime.generator.gui.rest.model.Parent;
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView;
import info.scce.dime.generator.gui.rest.model.TypeView;
import info.scce.dime.generator.gui.rest.model.TypeViewUtils;
import info.scce.dime.generator.livevariable.LiveVariableGenerator;
import info.scce.dime.generator.process.BackendProcessGeneratorHelper;
import info.scce.dime.generator.rest.DownloadTokenGenerator;
import info.scce.dime.generator.rest.DyWAAbstractGenerator;
import info.scce.dime.generator.rest.GUIProgressGenerator;
import info.scce.dime.generator.rest.SelectiveCache;
import info.scce.dime.generator.rest.SelectiveControllerGenerator;
import info.scce.dime.generator.rest.SelectiveGenerator;
import info.scce.dime.generator.rest.SelectiveInteractableProcessGenerator;
import info.scce.dime.generator.rest.SelectiveUserGenerator;
import info.scce.dime.generator.util.JavaIdentifierUtils;
import info.scce.dime.generator.util.RESTExtension;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.GUIPlugin;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.gui.InputPort;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.gui.gui.SecuritySIB;
import info.scce.dime.gui.gui.Table;
import info.scce.dime.gui.helper.GUIBranch;
import info.scce.dime.gui.helper.GUIExtension;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.GuardContainer;
import info.scce.dime.process.process.NativeFrontendSIBReference;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.SIB;

/**
 * Starts the generation of all front end code.
 * Including the entire Angular application.
 * Transforms all GUI models interaction Processes to code.
 * @author zweihoff
 *
 */
public class GUIDartGenerator implements IGenerator<DAD>{

	private GenerationContext genctx;
	private BackendProcessGeneratorHelper backendProcessGeneratorHelper;

	private IPath appRootPath;
	private IPath webPath;
	private IPath libPath;
	private IPath processRootPath;
	private IPath guiPath;
	private IPath processPath;
	private IPath dadPath;
	private IPath servicePath;
	private IPath webapp;
	private IPath dyawApp;

	private IPath pluginPath;
	
	private SelectiveCache selectiveCache = SelectiveCache.getInstance();

	
	/**
	 * Initializes the Generator.
	 * Requires a Map from every interaction processes to
	 * all guard container which contain a process SIB of this process
	 * @param toGuardContainers2
	 */
	public GUIDartGenerator(GenerationContext genctx, BackendProcessGeneratorHelper helper) {
		this.genctx = genctx;
		this.backendProcessGeneratorHelper = helper;
	}
	

	/**
	 * Starts the generation.
	 */
	@Override
	public void generate(DAD dad, IPath dyawApp, IProgressMonitor monitor) {
		long debugTime = System.currentTimeMillis();
		//define the paths where the generated files will be placed
		this.dyawApp = dyawApp;
		
		System.out.println("[INFO] GUI Generator started...");
		this.webapp = dyawApp.append("../");
		this.appRootPath = this.webapp.append("webapp/");
		this.webPath = appRootPath.append("web/");
		this.libPath = appRootPath.append("lib/src/");
		this.guiPath = libPath.append("gui/");
		this.pluginPath = libPath.append("plugin/");
		this.processPath = libPath.append("process/");
		this.processRootPath = libPath.append("root/");
		this.dadPath = libPath.append("dad/");
		libPath.append("filesupport/");
		this.servicePath = libPath.append("services/");
		
		final String timeStamp = Long.toString(new Timestamp(System.currentTimeMillis()).getTime());
		long dadModified = lastModified2(dad);
		
		copyAdditionalResources();
		fillSelectiveCache();
		
		genctx.getUsedDatas().forEach(n->{
			try {
				generateDataModel(n, genctx);
			}catch(Exception e) {
				printErr(e, n);
			}
		});
		System.out.println("[INFO] All Data Models generated");

		//Create DAD processes
		dad.getProcessComponents().stream()
			.filter(comp -> comp.getIncoming(StartupProcessPointer.class).isEmpty())
			.forEach(comp -> {
				try {					
					generateProcesses(comp.getModel(), genctx, true);
				} catch(Exception e) {
					printErr(e, comp.getModel());
				}
			});
		System.out.println("[INFO] All DAD processes generated");
			
		//Create Processes
		genctx.getVisibleProcesses().forEach(cp->{
			try {
				generateVisibleProcess(cp, Collections.emptyList(), genctx);				
			} catch (Exception e) {
				printErr(e, cp);
			}
		});
		
		//Create GUI Process
		genctx.getGUIProcesses().stream().forEach(n->{
			try {
				generateGUIProcess(n, genctx);
			} catch(Exception e) {
				printErr(e, n);
			}
		});
		
		System.out.println("[INFO] All visible processes generated");
				
		Map<String,GUIPlugin> guiPlugins = new HashMap<String,GUIPlugin>();
		Set<String> services = new HashSet<String>();
		//Create GUIs
		for(GUI g : genctx.getUsedGUIs()){
			try {
				generateGUI(g,guiPlugins,services,genctx);
			} catch(Exception e) {
				 printErr(e, g);
			}
		}
		System.out.println("[INFO] All GUIs generated");
		
		//generate deserializer
		genctx.getRootProcesses().forEach(n->{
			try {
			DyWAAbstractGenerator.generate(new DartTOGeneratorHelper().generateFrontEndInputDeserializer(n).toString(),
					".models.",
					"UserInteraction"+new RESTExtension().getSimpleTypeNameDart(n)+"ResponseDeserializer.dart",
					this.libPath,
					0);
			} catch(Exception e) {
				printErr(e, n);
			}
		});
		
		
		//create notification component
		createFile(new HTMLNotificationComponent().create().toString(),this.libPath+"notification/notification_component.html",dadModified);
		
		final SelectiveGenerator selectiveGenerator = new SelectiveGenerator();
		selectiveGenerator.generate(genctx, dyawApp);
		
		genctx.getUsedDatas().stream().map(Data::getTypes).flatMap(List::stream).forEach(n-> selectiveGenerator.generate(n, dyawApp));
		
		new SelectiveControllerGenerator().generate(genctx, dyawApp);
		System.out.println("[INFO] Selective Controller generated");
		
		new SelectiveUserGenerator().generate(dad, GUIDartGenerator.getUsers(dad), genctx.getCompoundViews(), dyawApp);
		System.out.println("[INFO] Selective User generated");

		new GUIProgressGenerator().generate(genctx, dyawApp);
		System.out.println("[INFO] GUI Resumers generated");

		//Create App with possible routes
		try {
			generateApp(Collections.emptyMap(),guiPlugins.values(),services,timeStamp,genctx,dadModified);
			System.out.println("[INFO] GUI "+dad.getAppName()+" App generated");
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		generateLiveVariables(dad, dyawApp, monitor, genctx.getCompoundViews());
		
		System.out.println("[INFO] GUI successfully generated in " + (System.currentTimeMillis() - debugTime));
	}
	
	
	
	private void copyAdditionalResources() {
		AngularDartGUIPluginTemplate adfuipt = new AngularDartGUIPluginTemplate();
		genctx.dad.getExternalResources().forEach(n->{
			IPath folder = webPath.append("/"+n.getSubfolder()+"/");
			n.getResources().forEach(r->{
				File sourceFile = info.scce.dime.generator.gui.utils.FileUtils.toFile(r, genctx.dad);
				if(sourceFile.isDirectory()){
					try {
						FileUtils.copyDirectory(sourceFile, folder.toFile());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					adfuipt.copyFile(sourceFile, folder);					
				}
			});
			
		});
		
		//copy favicon
		IPath folder = webPath.append("/favicon/");
		if (genctx.dad.getFaviconDirectory() != null && !genctx.dad.getFaviconDirectory().isEmpty()) {
			File sourceFile = info.scce.dime.generator.gui.utils.FileUtils.toFile(genctx.dad.getFaviconDirectory(), genctx.dad);
			if(sourceFile.isDirectory()){
				try {
					FileUtils.copyDirectory(sourceFile, folder.toFile());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				adfuipt.copyFile(sourceFile, folder);
			}
		}
		
	}
	
	private void printErr(Exception e,GraphModel g) {
		System.err.println("Error in "+genctx.guiExtension.graphModelFileName(g));
		throw new RuntimeException(e);
	}

 
	/**
	 * Generate a Dart file containing dart classes for each type defined in the Data model
	 * @param data
	 * @throws IOException
	 */
	private void generateDataModel(Data data, GenerationContext genctx){
		String s = new DartDataGenerator().create(data).toString();
		createFile(s, libPath+"data/"+DartDataGenerator.getName(data)+".dart", lastModified2(data));
	}
	
	
	private boolean combineFiles(Set<String> paths,String targetPath,DAD dad){
		if(paths.isEmpty()){
			return false;
		}
		//combine files to one
		try {
			info.scce.dime.generator.gui.utils.FileUtils.joinFiles(targetPath, paths,dad);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Creates the needed root files for the Angular app.
	 * @param appTitle
	 * @param guis All available GUI models
	 * @param routables All reachable interaction processes
	 * @param longRunningable All interaction processes, which are used in at least one long running process
	 * @param dad The one DAD model
	 * @throws IOException
	 */
	private void generateApp(Map<String,Set<GuardContainer>> toGuardContainer,Collection<GUIPlugin> guiPlugins,Set<String> services,String timeStamp,GenerationContext genctx,long dadChanged) throws IOException {
		//combine files
		Set<String> cssFiles = new HashSet<>();
		Set<String> preJSFiles = new HashSet<>();
		Set<String> postJSFiles = new HashSet<>();
		cssFiles.addAll(genctx.dad.getAdditionalStylesheets().stream().filter(n->!n.startsWith("http")).collect(Collectors.toSet()));
		cssFiles.addAll(
		guiPlugins.stream()
			.map(p->(Function)p.getFunction())
			.filter(p->!(p.eContainer() instanceof DartPlugin))
			.filter(p->((Plugin)p.eContainer()).getStyle() != null)
			.flatMap(p->((Plugin)p.eContainer()).getStyle().getFiles().stream())
			.map(n->n.getPath())
			.filter(n->!n.startsWith("http"))
			.collect(Collectors.toSet())
		);
		preJSFiles.addAll(genctx.dad.getPreScripts().stream().filter(n->!n.startsWith("http")).collect(Collectors.toSet()));
		postJSFiles.addAll(
		guiPlugins.stream()
			.map(p->(Function)p.getFunction())
			.filter(p->!(p.eContainer() instanceof DartPlugin))
			.filter(p->((Plugin)p.eContainer()).getScript() != null)
			.flatMap(p->((Plugin)p.eContainer()).getScript().getFiles().stream())
			.map(n->n.getPath())
			.filter(n->!n.startsWith("http"))
			.collect(Collectors.toSet())
		);
		postJSFiles.addAll(genctx.dad.getPostScripts().stream().filter(n->!n.startsWith("http")).collect(Collectors.toSet()));
		postJSFiles.addAll(genctx.getUsedGUIs().stream().flatMap(n->n.getAdditionalJavaScript().stream().filter(e->!e.startsWith("http"))).collect(Collectors.toSet()));
		
		//Index HTML
		String index = new HTMLDartIndexTemplate().create(
				genctx.dad.getAppName(),
				guiPlugins,
				timeStamp,
				combineFiles(cssFiles,webPath+"css/combined/combined.css", genctx.dad),
				combineFiles(preJSFiles,webPath+"js/precombined/precombined.js", genctx.dad),
				combineFiles(postJSFiles,webPath+"js/postcombined/postcombined.js", genctx.dad)).toString();
		
		createFile(index, webPath+"index.html",0);
		
		//main.dart
		String main = new AngularDartMainTemplate().create().toString();
		createFile(main, webPath+"main.dart",dadChanged);

		//app.dart
		String app = new AngularDartAppTemplate().create(genctx.dad,toGuardContainer).toString();
		createFile(app, libPath+"app.dart",dadChanged);
		new DyWADartGenerator().generate(this.libPath);
		
		//create login form
		if(!genctx.dad.getLoginComponents().isEmpty()) {
			generateLogin();
			System.out.println("[INFO] Custom Login GUI");
		}
		
		//generate root prcesses
		genctx.getRootProcesses().forEach(p->generateRootProcesses(p, genctx));
	}
	
	/**
	 * Generates all files needed for one visible basic process.
	 * the route-able process, which wraps the process and makes it reachable by an URL
	 * the embedded process, which wraps the process as a GUI sub process
	 * 
	 * @param cip The current interaction process
	 * @param gcs The guard containers which hold the given interaction process cip
	 * @param gc compound utilities
	 * @param isTopLevelProcess generates a routing wrapper for the given process
	 * @throws IOException
	 */
	private void generateVisibleProcess(Process cip,List<GuardContainer> gcs, GenerationContext genctx)
	{
		//Process Inputs, Outputs, Resume
		generateProcessDartTOs(cip, genctx);
		System.out.println("[INFO] Visible Process "+cip.getModelName()+" Component generated");
		
	}
	
	/**
	 * Creates an input and an output dart file for each GUI model, Native Front-EndSIB and GUI Events,
	 * placed in the given visible process cip.
	 * @param cip
	 * @param pcv
	 */
	private void generateProcessDartTOs(Process cip, GenerationContext genctx)
	{
		final DartTOGeneratorHelper dartGenerator = new DartTOGeneratorHelper();
		
		for(NativeFrontendSIBReference nfSIB:cip.getAllNodes().stream().filter(n->(n instanceof NativeFrontendSIBReference)).map(n->(NativeFrontendSIBReference)n).collect(Collectors.toList()))
		{
			String nfSibId = JavaIdentifierUtils.escapeDart(nfSIB.getId());
			//Inputs
			DyWAAbstractGenerator.generate(dartGenerator.generateNativeFrontEndSIBInputsDart(nfSIB, buildInputTypeViewMap(nfSIB, genctx.guiExtension), genctx.getUsedDatas(), genctx.guiExtension).toString(),
					".rest.native.",
					new RESTExtension().getSimpleTypeNameDart(nfSIB) + nfSibId+"Input.dart",
					this.libPath,
					lastModified2(cip));
			//Outputs
			DyWAAbstractGenerator.generate(dartGenerator.generateNativeFrontEndSIBOutputsDart(nfSIB, genctx.getUsedDatas(), genctx.guiExtension).toString(),
					".rest.native.",
					new RESTExtension().getSimpleTypeNameDart(nfSIB) + nfSibId+"Branch.dart",
					this.libPath,
					lastModified2(cip));
		}
		
		
		System.out.println("[INFO] Visible Process "+cip.getModelName()+" Dart TOs generated");
	}
	
	

	
	
	
	
	
	/**
	 * Helper method to build a relation from an input port of a given interactable process
	 * to the containing ports and the corresponding selective type views, which are located in pcv.
	 * The resulting map is used to create the compound type classes when the interactable process starts and
	 * the data of the ports has to be send to the backend.
	 * @param p
	 * @param pcv
	 * @return
	 */
	private Iterable<Parent> buildInputTypeViewMap(SIB sib,GUIExtension guiExtension)
	{
		return TypeViewUtils.buildInputTypeViewMap(sib,guiExtension);
	}
	
	
	
	
	
	/**
	 * Generates the routable wrapper Angular component for a given  process, placed in the application DAD.
	 * @param p
	 * @param generatorCompound
	 * @throws IOException
	 */
	private void generateProcesses(Process p, GenerationContext genctx, boolean isInDAD)
	{
		AngularDartDADProcessTemplate adrt = new AngularDartDADProcessTemplate();
		String routable = adrt.create(p).toString();
		createFile(routable, dadPath+AngularDartDADProcessTemplate.fileName(p).toString(), lastModified2(p));
		
		new SelectiveInteractableProcessGenerator(backendProcessGeneratorHelper).generate(p, dyawApp, genctx, isInDAD);
	}
	
	/**
	 * Generates the routable wrapper Angular component for a given interaction process.
	 * @param p
	 * @param generatorCompound
	 * @throws IOException
	 */
	private void generateRootProcesses(Process p, GenerationContext genctx)
	{
		AngularDartRootProcessTemplate adrt = new AngularDartRootProcessTemplate();
		String routable = adrt.create(p).toString();
		createFile(routable,
				processRootPath+AngularDartRootProcessTemplate.fileName(p).toString(),
				lastModified2(p));
		
		AngularDartProcessTemplate template = new AngularDartProcessTemplate();
		//Process Dart File
		String process = template.create(p).toString();
		createFile(process,
				processPath+template.className(p).toString()+".dart",
				lastModified2(p));
		
		final DartTOGeneratorHelper dartGenerator = new DartTOGeneratorHelper();
		DyWAAbstractGenerator.generate(dartGenerator.generateRootProcessInputsDart(p, genctx.guiExtension).toString(),
		".rest.process.",
		new RESTExtension().getSimpleTypeNameDart(p)+"Input.dart",
		this.libPath,
		lastModified2(p));
		
	}
	
	
	private void generateLogin() {
		GUI g = genctx.dad.getLoginComponents().get(0).getModel();
		String ngComponent = new AngularDartLoginComponent().createComponent(g).toString();
		createFile(ngComponent, libPath+"login/Login.dart",0);
		String ngHTMLComponent = new AngularDartLoginComponent().createHTML(g).toString();
		createFile(ngHTMLComponent, libPath+"login/Login.html",0);
	}

	/**
	 * Generates the GUI model Angular component class and HTML file for a given GUI model.
	 * For every found form, table and GUI plug in component placed in the GUI model
	 * another specific Angular component class and HTML file are generated.
	 * In addition to this, the used images, css and js files are copied to the app and
	 * if the current user is used in the GUI model, a user Angular service is generated as well.
	 * @param gui
	 * @param longRunningable
	 * @param lrpIps
	 * @param dad
	 * @throws IOException
	 */
	private void generateGUI(GUI gui,Map<String,GUIPlugin> guiPLugins,Set<String> services, GenerationContext genctx) {
		//Selective-View
		GUICompoundView gcv = genctx.getCompoundView(gui);
		final DartTOGeneratorHelper dartGenerator = new DartTOGeneratorHelper();
		
		//Inputs
		DyWAAbstractGenerator.generate(dartGenerator.generateGUIInputsDart(gui,gcv, genctx.getUsedDatas()).toString(),
				".rest.gui.",
				new RESTExtension().getSimpleTypeNameDart(gui)+"Input.dart",
				this.libPath, lastModified2(gui));
		//Outputs
		DyWAAbstractGenerator.generate(dartGenerator.generateGUIOutputsDart(gui, gcv, genctx.getUsedDatas(), genctx.guiExtension).toString(),
				".rest.gui.",
				new RESTExtension().getSimpleTypeNameDart(gui)+"Branch.dart",
				this.libPath, lastModified2(gui));
		
		//Generate Selectives
		//final DyWASelectiveDartGenerator dartSelectivesGenerator = new DyWASelectiveDartGenerator();
		//dartSelectivesGenerator.generate(gcv,libPath,Collections.emptyList(),false,gui,gc);
		
		//NG-Components
		String ngComponent = new AngularDartComponentTemplate().create(gui,gcv).toString();
		createFile(ngComponent,
				guiPath+JavaIdentifierUtils.escapeDart(new AngularDartComponentTemplate().getGUIName(gui).toString())+".dart",
				lastModified2(gui));
		String htmlComponent = new AngularComponentHTMLTemplate().create(gui).toString();
		createFile(htmlComponent,
				guiPath+JavaIdentifierUtils.escapeDart(new AngularDartComponentTemplate().getGUIName(gui).toString())+".html",
				lastModified2(gui));
		System.out.println("[INFO] GUI "+gui.getTitle()+" Component generated");
		
		// File guard compounds
		if(new DartFileGuardGenerator().isFileGuardPresent(gui)){
			createFile(new DartFileGuardGenerator().generate(gui, gcv, genctx.getUsedDatas()),
					libPath+"/models/"+new DartFileGuardGenerator().fileName(gui).toString(),
					lastModified2(gui));
			new DownloadTokenGenerator().generate(gui, gcv, dyawApp, genctx);			
		}
		
		// Security SIBs
		for(SecuritySIB sib : genctx.guiExtension.find(gui, SecuritySIB.class))
		{
			Process p = (Process) ((SecuritySIB) sib).getProMod();
			createFile(new AngularDartSecuritySIBTemplate().createComponent(sib, p, gcv).toString(),
					libPath+"/security/"+new AngularDartSecuritySIBTemplate().component(sib).toString()+".dart",
					lastModified2(p));
		}
		
		//Additional CSS and JS
		new AngularDartGUIPluginTemplate().copyResources(gui, this.guiPath);
		
		//GUI Plugins
		for(GUIPlugin cgp : genctx.guiExtension.find(gui,GUIPlugin.class))
		{
			if(!guiPLugins.containsKey(cgp.getId())){
				guiPLugins.put(cgp.getId(), cgp);
			}
			String guiPluginComponent = new AngularDartGUIPluginTemplate().createComponents(cgp,gcv).toString();
			createFile(guiPluginComponent,
					pluginPath+new AngularDartGUIPluginTemplate().getPluginClassName(cgp).toString()+".dart",
					lastModified2(gui));
			String guiPluginHTML = new AngularDartGUIPluginTemplate().createHTML(cgp).toString();
			createFile(guiPluginHTML,
					pluginPath+new AngularDartGUIPluginTemplate().getPluginClassName(cgp).toString()+".html",
					lastModified2(gui));
			System.out.println("[INFO] GUI "+gui.getTitle()+" Plugin Component generated");
			new AngularDartGUIPluginTemplate().copyResources(cgp,this.pluginPath);
		}
		
		//GUI Image
		for(Image img : genctx.guiExtension.find(gui,Image.class))
		{
			new HTMLImageTemplate().copyImage(img, this.webPath);
		}
//		System.out.println("[INFO] GUI "+gui.getTitle()+" Images moved");
		
		//Table-Components
		for(Table table : genctx.guiExtension.find(gui, Table.class)) {
			String tableComponent = new AngularDartTableTemplate().create(table,gui,gcv).toString();
			String tableComponentName = new AngularDartTableTemplate().getTableName(table, gui).toString();
			createFile(tableComponent,
					libPath+"tables/"+Character.toUpperCase(gui.getTitle().charAt(0)) + gui.getTitle().substring(1)+"/"+tableComponentName+".dart",
					lastModified2(gui));			
			String tableHtmlComponent = new AngularTableHTMLTemplate().create(table,gui).toString();
			createFile(tableHtmlComponent,
					libPath+"tables/"+Character.toUpperCase(gui.getTitle().charAt(0)) + gui.getTitle().substring(1)+"/"+tableComponentName+".html",
					lastModified2(gui));				
		}
//		System.out.println("[INFO] GUI "+gui.getTitle()+" Tables generated");
		
		//Form-Components
		for(Form form : genctx.guiExtension.find(gui, Form.class)) {
			String formComponent = new AngularDartFormTemplate().create(form,gui,gcv).toString();
			String formComponentName = new AngularDartFormTemplate().getFormName(form, gui).toString();
			createFile(formComponent,
					libPath+"forms/"+Character.toUpperCase(gui.getTitle().charAt(0)) + gui.getTitle().substring(1)+"/"+formComponentName+".dart",
					lastModified2(gui));			
			String formHtmlComponent = new AngularFormHTMLTemplate().create(form,gui).toString();
			createFile(formHtmlComponent,
					libPath+"forms/"+Character.toUpperCase(gui.getTitle().charAt(0)) + gui.getTitle().substring(1)+"/"+formComponentName+".html",
					lastModified2(gui));			
		}
//		System.out.println("[INFO] GUI "+gui.getTitle()+" Forms generated");
		
		//User Service
		ComplexVariable currentUserVar = (ComplexVariable) new AngularDartCommonImports().getCurrentUser(gui);
		if(currentUserVar != null){
			//Find TypeView for current User
			ComplexTypeView ctv = (ComplexTypeView) gcv.getPairs().get(currentUserVar);
			if(ctv != null)
			{
				String userService = new AngularDartCurrentUserService().create(ctv).toString();
				String serviceName = AngularDartCurrentUserService.getName(ctv).toString();
				services.add(serviceName);
				createFile(userService,
						servicePath+serviceName.toString()+"Service.dart",
						lastModified2(gui));
//				System.out.println("[INFO] GUI "+gui.getTitle()+" User Service generated");
			}
		}
		
		System.out.println("[INFO] GUI "+gui.getTitle()+" Selective TOs generated");
		
		
	}
	
	/**
	 * Generated the needed compound classes for all security SIBs placed in a GUI model
	 * @param cip
	 * @throws IOException
	 */
	private void generateGUIProcess(Process p, GenerationContext genctx)
	{
		DartTOGeneratorHelper dartGenerator = new DartTOGeneratorHelper();
		//Dart Inputs
		DyWAAbstractGenerator.generate(dartGenerator.generateSecurityProcessInputsDart(p, genctx.getUsedDatas()).toString(),
		".security.",
		new RESTExtension().getSimpleTypeNameDart(p)+"Input.dart",
		this.libPath,
		lastModified2(p));
		
		final SelectiveInteractableProcessGenerator backendGenerator = new SelectiveInteractableProcessGenerator(backendProcessGeneratorHelper);
		for (GUICompoundView gcv : genctx.getCompoundViews()) {
			final List<ProcessSIB> processes = genctx.guiExtension.find(gcv.getGUI(), ProcessSIB.class);
			

			for (ProcessSIB ps : processes) {
				final Map<InputPort, TypeView> typeMap = new HashMap<>();
				if (ps.getProMod().equals(p)) {
					for (InputPort ip : ps.getInputPorts()) {
						// TODO static ports
						if (!ip.getIncoming().isEmpty()) {
							final Node node = ip.getIncoming().get(0).getSourceElement();
							final Parent portView = gcv.getPairs().get(node);
							if(portView instanceof PrimitiveFieldView) {
								typeMap.put(ip, ((PrimitiveFieldView)portView).getPrimitiveTypeView());
							}
							else if(portView instanceof ComplexFieldView) {
								typeMap.put(ip, ((ComplexFieldView)portView).getView());
							} else {
								typeMap.put(ip, (TypeView)portView);								
							}
						}
						else {
							typeMap.put(ip, null);
						}
					}
					backendGenerator.generate(p, typeMap, dyawApp, ps, genctx);
				}
			}
		}
	
		System.out.println("[INFO] Security SIB Interactable "+p.getModelName()+" REST Service generated");
		
	}
	
//	/**
//	 * Generate the long running process data compound classes.
//	 * @param cip
//	 * @throws IOException
//	 */
//	private void generateLongRunningProcesses(Process cip,Set<GUICompoundView> gcvs,List<Process> allLRPs) {
//
//		new SelectiveLongRunningProcessGenerator().generate(allLRPs, cip, outlet);
//		System.out.println("[INFO] Long Running "+cip.getModelName()+" generated");
//	}

	
	/**
	 * Helper method to create a file with the given content on the given path.
	 * @param content
	 * @param path
	 * @throws IOException
	 */
	private void createFile(String content,String path,long modified)
	{
		File file = new File(path);
		if(file.exists() && modified > 0) {
			if(file.lastModified()>=modified) {
				//System.out.println(path+" not gen "+file.lastModified()+" > "+modified);
				return;
			}
		}
		//System.out.println(path +"is gen");
		try {
			FileUtils.writeStringToFile(file, content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Find all Concrete Types of all Data models referenced in the DAD model,
	 * which are inherit or connected by an user association with an user type.
	 * @param dad
	 * @return
	 */
	public static Set<ConcreteType> getUsers(DAD dad)
	{
		Set<ConcreteType> userTypes = new HashSet<ConcreteType>();
		for(Data d:dad.getDataComponents()
				.stream()
				.map(n->n.getModel())
				.collect(Collectors.toList())
				){
			for(UserType ut: d.getUserTypes())
			{
				 for(Type it:ut.getOutgoing(UserAssociation.class)
						 .stream()
						 .map(n->n.getTargetElement())
						 .collect(Collectors.toList())
						 )
				 {
					 userTypes.addAll(getInheritUserTypes(it)
							 .stream()
							 .filter(n->(n instanceof ConcreteType))
							 .map(n->(ConcreteType)n)
							 .collect(Collectors.toSet())
							 );
				 }
			}
		}
		return userTypes;
	}
	
	/**
	 * Collects the inherit ConcreteType of a given type
	 * @param it
	 * @return
	 */
	private static Set<Type> getInheritUserTypes(Type it)
	{
		return	Stream.concat(
					Stream.of(it).filter(n->(n instanceof ConcreteType)),
					it
					.getIncoming(Inheritance.class)
					.stream()
					.map(n->n.getSourceElement())
					.filter(n->(n instanceof Type))
					.map(n->((Type)n))
					.flatMap(n->(GUIDartGenerator.getInheritUserTypes(n).stream()))
					).collect(Collectors.toSet());
	}
	
	/**
	 * Generates live variables.
	 * 
	 * @param dad
	 * @param path
	 * @param monitor
	 * @param guiCompundViews
	 */
	private void generateLiveVariables(DAD dad, IPath path, IProgressMonitor monitor, Set<GUICompoundView> guiCompundViews)
	{
		new LiveVariableGenerator(guiCompundViews).generate(dad, path, monitor);
	}
	
	private void fillSelectiveCache() {
		final List<BaseComplexTypeView> cache = new ArrayList<>(SelectiveExtension.collectComplexTypeViews(genctx));

		for (GUI g : genctx.getUsedGUIs()) {
			final Set<Type> typeCache = new HashSet<>();
			final Set<BaseComplexTypeView> basicCache = new HashSet<>();
			final Map<GUIBranch, Map<String, List<TypeView>>> btvs = SelectiveExtension.computeBranchTypeViews(genctx,
					g, typeCache, basicCache);
			final Map<EndSIB, Map<String, List<TypeView>>> ptvs = SelectiveExtension.computeProcessTypeViews(g,
					typeCache);

			for (Map<String, List<TypeView>> e : btvs.values()) {
				for (List<TypeView> tvs : e.values()) {
					addIfBaseComplexTypeView(tvs, cache);
				}
			}

			for (Map<String, List<TypeView>> e : ptvs.values()) {
				for (List<TypeView> tvs : e.values()) {
					addIfBaseComplexTypeView(tvs, cache);
				}
			}

			addIfBaseComplexTypeView(basicCache, cache);
		}

		cache.forEach(selectiveCache::registerTypeView);
		genctx.getUsedDatas().stream().map(Data::getTypes).flatMap(List::stream).forEach(selectiveCache::registerBlankTypeView);
		selectiveCache.printStats();
	}
	
	private void addIfBaseComplexTypeView(Collection<? extends TypeView> source, Collection<BaseComplexTypeView> target) {
		final TypeView superSelective = DyWAAbstractGenerator.getSuperSelectives(source);
		if (superSelective instanceof BaseComplexTypeView) {
			target.add((BaseComplexTypeView) superSelective);
		}
	}

}
