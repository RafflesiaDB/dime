package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.html.HTMLFieldTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular HTML template for a form component
 */
class AngularFormHTMLTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular HTML template code for a given form component
	 */
	def create(Form form,GUI g)
	'''
	<form
	 «IF !preview»
	 	[ngFormModel]="«ConventionHelper.getFormName(form)»"
	 	[attr.id]="'form'+«ConventionHelper.getFormName(form)».hashCode.toString()"
	 	data-cinco-id="«form.id.escapeString»"
 	«ENDIF»
	 «form.printStyle»
	 «form.printNgIfFor» «IF form.inline» class="form-horizontal"«ENDIF»
«««	 «FOR primitiveVar:g.dataContexts.map[primitiveVariables].flatten.filter[!isIsList]»
«««	(primitive_«primitiveVar.name.escapeDart»_update)="«primitiveVar.name.escapeDart» = $event"
«««	«ENDFOR»
	 >
	 «new info.scce.dime.generator.gui.BaseTemplate().baseContent(ElementCollector.getElementsV(form.allNodes))»
	</form>
	'''
	
	/**
	 * Helper method to generate the form submit button validation surrounding
	 * error message
	 */
	def preButton(Form form)
	'''
	«IF !form.errorMessage.nullOrEmpty»
	<div
		«IF !preview»*ngIf="«form.getFormHasErrors(true)»"«ENDIF»
		class="alert alert-danger">«form.errorMessage»</div>
	«ENDIF»
	'''
	
	def getFormHasErrors(Form form,boolean considerTouched)'''(!«ConventionHelper.getFormName(form)».valid«IF considerTouched» && !«ConventionHelper.getFormName(form)».pristine «ENDIF»)«form.getFileValidation(considerTouched)»'''
		
		
	/**
	 * Checks if a form contains a validating file input field
	 */
	def getFileValidation(Form form,boolean considerTouched)
	{
		return form.find(Field).map[n|HTMLFieldTemplate.fileValidation(n,form,"||",considerTouched)].join
	}
}