package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.utils.GUIGenerator

/**
 * Template to create the main.dart file. It has been extracted from the static Dart sources to decouple static and
 * and generated code. Some imports of the main.dart file reference generated files. Since those files aren't
 * present in the static sources, the analysis reports errors and fails.
 */
class AngularDartMainTemplate extends GUIGenerator {
		
	/**
	 * Creates the static Angular Dart main.dart file.
	 */
	def create() '''
	import 'package:angular/angular.dart';
	import 'package:angular_router/angular_router.dart';
	import 'package:app/src/app.template.dart' as ng;
	
	import 'main.template.dart' as self;
	
	@GenerateInjector(routerProviders)
	final InjectorFactory injector = self.injector$Injector;
	
	@GenerateInjector(routerProvidersHash)
	final InjectorFactory injectorLocal = self.injectorLocal$Injector;
	
	void main() {
	  final local = const bool.fromEnvironment('local', defaultValue: true);
	  print(local);
	  if (local) {
	    runApp(ng.AppComponentNgFactory, createInjector: injectorLocal);
	  } else {
	    runApp(ng.AppComponentNgFactory, createInjector: injector);
	  }
	}
	'''

}
