package info.scce.dime.generator.gui.dart

import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.gui.gui.GUI

/**
 * Template to generate general methods important for every GUI model Angular Dart component class file
 */
class BaseTemplate extends info.scce.dime.generator.gui.BaseTemplate {
	
	/**
	 * Generates general methods for the GUI model Angular Dart component class file
	 */
	
	def createMethods(GUI gui)
	'''
	/// returns the surrounding container class for major GUI models
	String getContainer«ConventionHelper.cincoID(gui)»RootClass()
	{
		if(this.ismajorpage)return "";
		return "";
	}
	
	/// returns the surrounding wrapper class for major GUI models
	String getContainer«ConventionHelper.cincoID(gui)»Id()
	{
		if(this.ismajorpage)return "wrapper";
		return "«ConventionHelper.cincoID(gui)»";
	}
	
	/// returns the surrounding container class for major GUI models
	String getContainer«ConventionHelper.cincoID(gui)»Class()
	{
		if(this.ismajorpage)return "container-display";
		return "";
	}
	
	/// callback, to go back to the root interaction
	void redirect«ConventionHelper.cincoID(gui)»ToHome(dynamic e)
	{
		e.preventDefault();
		this.router.navigate(Routes.root.toUrl());
	}
	'''
}
