package info.scce.dime.generator.gui

import graphmodel.Node
import info.scce.dime.generator.gui.dart.AngularDartControlSIBTemplate
import info.scce.dime.generator.gui.dart.AngularDartGUIPluginTemplate
import info.scce.dime.generator.gui.dart.AngularFormHTMLTemplate
import info.scce.dime.generator.gui.html.HTMLAlertTemplate
import info.scce.dime.generator.gui.html.HTMLBadgeTemplate
import info.scce.dime.generator.gui.html.HTMLBoxTemplate
import info.scce.dime.generator.gui.html.HTMLButtonGroupTemplate
import info.scce.dime.generator.gui.html.HTMLButtonTemplate
import info.scce.dime.generator.gui.html.HTMLButtonToolbarTemplate
import info.scce.dime.generator.gui.html.HTMLChoiceTemplate
import info.scce.dime.generator.gui.html.HTMLDescriptionTemplate
import info.scce.dime.generator.gui.html.HTMLEmbeddedTemplate
import info.scce.dime.generator.gui.html.HTMLFieldTemplate
import info.scce.dime.generator.gui.html.HTMLFileTemplate
import info.scce.dime.generator.gui.html.HTMLFormTemplate
import info.scce.dime.generator.gui.html.HTMLHeadlineTemplate
import info.scce.dime.generator.gui.html.HTMLImageTemplate
import info.scce.dime.generator.gui.html.HTMLJumbotronTemplate
import info.scce.dime.generator.gui.html.HTMLListTemplate
import info.scce.dime.generator.gui.html.HTMLNavBarTemplate
import info.scce.dime.generator.gui.html.HTMLPageUp
import info.scce.dime.generator.gui.html.HTMLPanelTemplate
import info.scce.dime.generator.gui.html.HTMLPlaceholderTemplate
import info.scce.dime.generator.gui.html.HTMLProcessSIBTemplate
import info.scce.dime.generator.gui.html.HTMLProgressBarTemplate
import info.scce.dime.generator.gui.html.HTMLRowTemplate
import info.scce.dime.generator.gui.html.HTMLTabbingTemplate
import info.scce.dime.generator.gui.html.HTMLTableTemplate
import info.scce.dime.generator.gui.html.HTMLTextTemplate
import info.scce.dime.generator.gui.html.HTMLThumbnailTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Alert
import info.scce.dime.gui.gui.Badge
import info.scce.dime.gui.gui.Bar
import info.scce.dime.gui.gui.BaseElement
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.ButtonToolbar
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.Description
import info.scce.dime.gui.gui.Embedded
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.File
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.Headline
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.gui.gui.Listing
import info.scce.dime.gui.gui.PageUp
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.ProgressBar
import info.scce.dime.gui.gui.Row
import info.scce.dime.gui.gui.Select
import info.scce.dime.gui.gui.SpecialElement
import info.scce.dime.gui.gui.TODOList
import info.scce.dime.gui.gui.Tabbing
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Text
import info.scce.dime.gui.gui.Thumbnail
import info.scce.dime.gui.helper.ElementCollector
import info.scce.dime.process.process.Process
import java.util.List
import info.scce.dime.generator.gui.html.HTMLLinkSIBTemplate

/**
 * The Base Template is used to render the HTML code for all available components placed in a given component.
 * The rendering process used recursion to build up the entire HTML document.
 */
class BaseTemplate extends GUIGenerator {
	
	/**
	 * Creates the entire HTML Angular code for a given GUI model.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def String create(GUI container){
		var html ="";
		html += preContainer(container).toString;
		for(Template node:ElementCollector.getElementsV((container.allNodes).filter(Template).filter[blockName.equals("body")])){
			html += createTemplate(node);
		}
		html += postContainer(container);
		return html;
	}
	
	
	/**
	 * Creates the entire HTML Angular code for a given Template in a GUI model.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def createTemplate(Template ct)
	{
		return baseContent(ElementCollector.getElementsV(ct.allNodes),!ct.disableAutoLayout);
	}
	
	
	/**
	 * Creates the opening tags HTML code for a bootstrap row
	 */
	def preRow()
	'''
	<div class="row">
		<div class="col-sm-12">
	'''
	
	/**
	 * Creates the closing tags HTML code for a bootstrap row
	 */
	def postRow()
	'''
		</div>
	</div>
	'''
	
	/**
	 * Creates the HTML code depended of the type and the order of every component.
	 * The rowing defines whether every component in the list should placed in its own row.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def String baseContent(List<Node> nodes)
	{
		baseContent(nodes,false)
	}
	
	/**
	 * Creates the HTML code depended of the type and the order of every component.
	 * The rowing defines whether every component in the list should placed in its own row.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def String baseContent(List<Node> nodes,boolean rowing)
	{
		var html ="";
		for(BaseElement node:nodes.filter(BaseElement)){
			if(node instanceof Row){
				var row = new HTMLRowTemplate();
				html += row.create(node);
			}
			else{
				if(rowing && (!node.rootElement.disableAutoLayout)){
					html += preRow;
				}
				if(node instanceof Headline){
					var headline = new HTMLHeadlineTemplate();
					html += headline.create(node);
				}
				if(node instanceof Listing){
					var list = new HTMLListTemplate();
					html += list.create(node);
				}
				if(node instanceof Bar){
					var list = new HTMLNavBarTemplate();
					html += list.create(node);
				}
				if(node instanceof GUIPlugin){
					html += new AngularDartGUIPluginTemplate().create(node).toString;
				}
				if(node instanceof ControlSIB){
					html += new AngularDartControlSIBTemplate().create(node).toString;
				}
				if(node instanceof Description){
					var description = new HTMLDescriptionTemplate();
					html += description.create(node);
				}
				if(node instanceof Table){
					var table = new HTMLTableTemplate();
					val gcv = genctx.getCompoundView(node.rootElement)
					html += table.create(node,gcv).toString;
				}
				if(node instanceof Form){
					var form = new HTMLFormTemplate();
					val gcv = genctx.getCompoundView(node.rootElement)
					html += form.create(node,gcv);
				}
				if(node instanceof ButtonToolbar){
					html += new HTMLButtonToolbarTemplate(node).create
				}
				if(node instanceof Button){
					val form = node.findFirstParent(Form)
					if (form !== null && node.options?.submitsForm) {
						html += new AngularFormHTMLTemplate().preButton(form)
					}
					html += new HTMLButtonTemplate().create(node as Button,form);
				}
				if(node instanceof ButtonGroup){
					var group = new HTMLButtonGroupTemplate();
					html += group.create(node);
				}
				if(node instanceof Image){
					var group = new HTMLImageTemplate();
					html += group.create(node,true);
				}
				if(node instanceof Thumbnail){
					var thumbnail = new HTMLThumbnailTemplate();
					html += thumbnail.create(node).toString;
				}
				if(node instanceof Jumbotron){
					var jumbotron = new HTMLJumbotronTemplate();
					html += jumbotron.create(node).toString;
				}
				if(node instanceof Panel){
					var panel = new HTMLPanelTemplate();
					html += panel.create(node);
				}
				
				if(node instanceof Box){
					var box = new HTMLBoxTemplate();
					html += box.create(node).toString;
				}
				if(node instanceof PageUp){
					var box = new HTMLPageUp();
					html += box.create(node);
				}
				if(node instanceof Alert){
					var alert = new HTMLAlertTemplate();
					html += alert.create(node);
				}
				if(node instanceof Tabbing){
					html += new HTMLTabbingTemplate().create(node);
				}
				if(node instanceof Text){
					var text = new HTMLTextTemplate();
					html += text.create(node);
				}
				if(node instanceof Embedded){
					html += new HTMLEmbeddedTemplate().create(node);
				}
				
				if(node instanceof SpecialElement) {
					if(node instanceof TODOList) {
						var row = new AngularTODOList();
						html += row.createTemplate(node as TODOList);				
					}
				}
				if(node instanceof Placeholder) {
						var row = new HTMLPlaceholderTemplate();
						html += row.create(node);				
				}
				if(node instanceof File) {
						var row = new HTMLFileTemplate();
						html += row.createColContent(node as File);				
				}
				if(node instanceof Field) {
						var row = new HTMLFieldTemplate();
						html += row.create(node as Field, node.findFirstParent(Form));				
				}
				if(node instanceof Select) {
						var row = new HTMLChoiceTemplate();
						html += row.create(node as Select, node.findFirstParent(Form));				
				}
				if(node instanceof ProgressBar) {
						var row = new HTMLProgressBarTemplate();
						html += row.create(node as ProgressBar);				
				}
				if(node instanceof GUISIB){
						var row = new AngularIncludedTemplate();
						html += row.create(node as GUISIB)	
				}
				if(node instanceof Badge){
						var row = new HTMLBadgeTemplate();
						html += row.create(node as Badge);	
				}
				if(node instanceof ProcessSIB) {
					var ei = node as ProcessSIB;
					var p = ei.proMod as Process;
					html += new HTMLProcessSIBTemplate().create(node,p);
				}
				if(node instanceof LinkProcessSIB) {
					var ei = node as LinkProcessSIB;
					html += new HTMLLinkSIBTemplate().create(ei);
				}
				if(rowing && (!node.rootElement.disableAutoLayout)){
					html += postRow;					
				}
			}
			
		}
		return html;
	}
	
	
	/**
	 * Creates the opening tags for the GUI model HTML code.
	 * This includes the code for the header template if available.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def preContainer(GUI gui)
	'''
	«IF gui.generalStyle != null»
	<div
		«IF !preview»
		[id]="getContainer«ConventionHelper.cincoID(gui)»Id()"
		[class]="getContainer«ConventionHelper.cincoID(gui)»Class()"
		«ENDIF»
		style="«gui.generalStyle.rawContent»">
	«ENDIF»
	«FOR node:gui.templates.filter[n | n.blockName.equals("header")] »
		<header «IF node.generalStyle != null»style="«node.generalStyle.rawContent»"«ENDIF»>
				«this.createTemplate(node)»
		</header>
	«ENDFOR»
	«IF gui.generalStyle != null»
	</div>
	«ENDIF»
	'''
	
	/**
	 * Creates the closing tags for the GUI model HTML code.
	 * This includes the code for the footer template if available.
	 * If the preview mode is activated, only plain HTML5 is created.
	 */
	def postContainer(GUI gui)
	'''
	«FOR node:gui.templates.filter[n | n.blockName.equals("footer")] »
		<footer style="width: 100%;bottom: 0px;position: fixed;«IF node.generalStyle != null»«node.generalStyle.rawContent»«ENDIF»">
				«this.createTemplate(node)»
		</footer>
	«ENDFOR»
	«IF gui.generalStyle != null»
	</div>
	«ENDIF»
	'''
}
