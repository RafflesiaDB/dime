
package info.scce.dime.generator.gui.html

import graphmodel.Node
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.utils.FileUtils
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import org.eclipse.core.runtime.IPath
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.rest.DartFileGuardGenerator

/**
 * Template to generate the Angular component HTML template code for a image component
 */
class HTMLImageTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for a image component
	 */
	def create(Image image,boolean considerLink)
	'''
	«val button = image.findThe(Button)»
	«IF button !== null && considerLink»
		<a «button.printStyleWith(null)»
		«IF !preview»(click)="«new HTMLButtonTemplate().getAction(button, button.findFirstParent(Form))»"«ENDIF»
		role="button"«IF button.isDisabled» disabled«ENDIF»>
	«ENDIF»
	<img
		«image.printNgIfFor»
		«image.printStyleWith(image.getSize(null,null))»
		alt="«image.rootElement.title» image"
		class="«getClass(image)»«IF image.styling!=null»«IF image.styling.responsive» img-responsive«ENDIF»«ENDIF»"
		«IF preview && !(image.path.startsWith("{{") && image.path.endsWith("}}"))»
			src="«FileUtils.toAbsolutPath(image.path,image.rootElement as GUI)»"
		«ELSE»
			«IF !image.path.nullOrEmpty»
				«IF !(image.path.startsWith("{{") && image.path.endsWith("}}"))»
					src="«getStaticImagePath(image,false)»"
				«ELSE»
					«IF image.guardSIBs.empty»
						[src]="'«ConventionHelper.replaceDataBindingString(image.path)»'"
					«ENDIF»
			 	«ENDIF»
		 	«ELSE»
		 		«{
		 			val params = HTMLFileTemplate.getParameters(image).map[n|DataDartHelper.getBindedDataName(n,DataAccessType.^FOR,MODE.GET)]
		 			'''
		 			«IF !params.empty»
		 				[src]="getRestBaseUrl() + '/«new DartFileGuardGenerator().getImageURL(image)»' + load«image.id.escapeDart»Image(«params.join(",")»)«IF !image.guardSIBs.get(0).cachable» + '&cache=' + imageHash.toString()«ENDIF»"
		 			«ELSE»
		 				[src]="getRestBaseUrl() + '/«new DartFileGuardGenerator().getImageURL(image)»'«IF !image.guardSIBs.get(0).cachable» + '?cache=' + imageHash.toString()«ENDIF»"
		 			«ENDIF»
		 			'''
		 		}»
		 	«ENDIF»
		«ENDIF»
	>
	«IF button !== null && considerLink»
		</a>
	«ENDIF»
	'''
	
	def boolean getIsText(Node node)
	{
		if(node instanceof PrimitiveAttribute){
			return (node as PrimitiveAttribute).attribute.dataType == PrimitiveType.TEXT
		}
		if(node instanceof PrimitiveVariable){
			return node.dataType == info.scce.dime.gui.gui.PrimitiveType.TEXT
		}
		return false;
	}
	
	/**
	 * Returns the CSS class for the image styling type constant
	 */
	private def getClass(Image image)
	{
		if(image.styling==null)return "img";
		switch(image.styling.border)
		{
			case CIRCLE: return "img-circle"
			case ROUNDED: return "img-rounded"
			case THUMBNAIL: return "img-thumbnail"
			default: return "img"
		}
	}
	
	/**
	 * Returns the static image path. This can either be a URL or a local resource which is moved to the server
	 */
	private def getStaticImagePath(Image image,boolean preview)
	{
		if(image.path.startsWith("http")){
			return image.path;
		}
		var sourceFile = new File(image.path);
		if(preview){
			return image.path;
		}
		return "img/"+image.rootElement.title+"/"+sourceFile.name;
		
	}
	
	/**
	 * Copies an image from the specified path to the application resources
	 * which will be moved to the server
	 */
    def copyImage(Image image,IPath resourcePath)
	{
		if(image.path.nullOrEmpty)return;
		if(image.path.startsWith("http") || image.path.containsExpression ){
			return;
		}
		var sourceFile = FileUtils.toFile(image.path,image.rootElement as GUI);
		var targetCSS = resourcePath.append("/img/" +image.rootElement.title);
		if (!new File(targetCSS.toFile.path).exists()){
			new File(targetCSS.toFile.path).mkdirs;			
		}
		Files.copy(sourceFile.toPath, new File(targetCSS.toFile.path +"/"+ sourceFile.name).toPath, StandardCopyOption.REPLACE_EXISTING);		
	}
	
	private def boolean containsExpression(String path)
	{
		return path.trim.startsWith("{{") && path.trim.endsWith("}}")
	}
	
	/**
	 * Returns the width and height CSS properties depended on the given image component
	 */
	private def getSize(Image image,String encapsulation,String separator){
		val e = if(encapsulation===null)"" else encapsulation
		val s = if(separator===null)";" else separator
		
		var css="";
		if(image.dimensionX > 0){
			css+= ''' «e»width«e»:«e»«image.dimensionX»px«e»«s»''';
		}
		else if(image.dimensionX == 0) {
			
		}
		else {
			css+= ''' «e»width«e»:«e»100%«e»«s»'''
		}
		if(image.dimensionY > 0){
			css+= ''' «e»height«e»:«e»«image.dimensionY»px«e»«s»''';
		}
		else if(image.dimensionY == 0) {
			
		}
		else {
			css+= ''' «e»height«e»:«e»100%«e»«s»''';
		}
		
		return css;
	}
}
