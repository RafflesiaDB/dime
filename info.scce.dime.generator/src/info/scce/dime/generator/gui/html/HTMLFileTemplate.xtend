package info.scce.dime.generator.gui.html

import graphmodel.Container
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.gui.gui.Display
import info.scce.dime.gui.gui.File
import info.scce.dime.gui.gui.GuardSIB
import info.scce.dime.gui.gui.IO
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Size
import java.util.Arrays

/**
 * Template to generate the Angular component HTML template code for a file component
 */
class HTMLFileTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given file component
	 */
	def createColContent(File file)
	'''
	<div
	«file.printNgIfFor» «file.printStyle»
	«IF isList(file)» 
		«IF !preview»*ngFor="let file«ConventionHelper.cincoID(file)»Iterator of «DataDartHelper.getBindedDataName(file,DataAccessType.^FOR,MODE.GET_INIT)»"«ENDIF»
	«ENDIF»
	>
		«createButton(file)»
	</div>
	'''
	
	/**
	 * Generates the HTML code for a file download button, placed in a file component
	 */
	private def createButton(File file)
	'''
	<button type="button"
		data-cinco-id="«file.id.escapeString»"
		«IF !preview»
			«IF file.parameters.empty»
			(click)="download«file.id.escapeDart»File(«DataDartHelper.getBindedDataName(file,DataAccessType.^FOR,MODE.GET)»,$event)"
			«ELSE»
			(click)="download«file.id.escapeDart»File(«file.parameters.map[toPatameter].join(",")»,$event)"
			«ENDIF»
		«ENDIF»
		class="btn btn-«new HTMLColoringTemplate().createClass(file.styling)»«getSize(file)»"«IF file.disabled» disabled«ELSE» [disabled]="isBusy()"«ENDIF»
		>«new HTMLIconTemplate().icons(file.icon,'''«ConventionHelper.replaceDataBinding(file.label)»''')»
	</button>
	'''
	
	private def toPatameter(IO port){
		if(port instanceof InputPort && !port.incoming.empty){
			return DataDartHelper.getBindedDataName(port,DataAccessType.^FOR,MODE.GET)
		}
		return port.defaultValue
	}
	
	/**
	 * Returns the variables used as parameters for a file download method
	 * for the button used in the file component
	 */
	
	/**
	 * Returns the parameter name for a given guard SIB port
	 */
	def parameterName(IO port)'''«port.id.escapeDart+port.name.escapeDart»'''
	
	/**
	 * Returns the parameter list for a given file or image component
	 */
	def getParameterNames(Container container){
		var guards = getParameters(container).map[n|n.parameterName]
		if(guards.isEmpty){
			return Arrays.asList("fileRef")
		}
		
		return guards;
		
	}
	
	/**
	 * Returns the input port list for a given file or image component
	 */
	static def getParameters(Container container){
		var guards = container.allNodes.filter[n|n instanceof GuardSIB].map[n|n as GuardSIB]
		var inputs = guards.map[guard|guard.allNodes.filter(InputPort)].flatten
		return inputs
		
	}
	
	/**
	 * Checks, if the given file component is connected to a list variable or attributes,
	 * so that multiple files has to be displayed in a list
	 */
	private def isList(File file)
	{
		if(!file.getIncoming(Display).empty)
		{
			var cpa = file.getIncoming(Display).get(0);
			if(cpa.sourceElement instanceof PrimitiveAttribute)
			{
				var pa = cpa.sourceElement as PrimitiveAttribute;
				return pa.attribute.isIsList
			}
			if(cpa.sourceElement instanceof PrimitiveVariable)
			{
				return (cpa.sourceElement as PrimitiveVariable).isIsList;
			}
		}
		return false
	}
	
	/**
	 * Returns the CSS class for the file download button size of the given file component
	 */
	def CharSequence getSize(File file){
		if(file.styling==null)return "";
		if(file.styling.size != Size.DEFAULT)return " btn-"+HTMLSizeTemplate.create(file.styling.size);
		return ""
	}
}
