package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.gui.gui.Alignment
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.StaticContent
import info.scce.dime.generator.gui.utils.GUIGenerator

/**
 * Template to generate the Angular component HTML template code for a static context type
 */
class HTMLStaticContextTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given static context type
	 */
	def create(StaticContent staticContent) {
		var html = this.pre(staticContent).toString.replaceAll("\t", "");
		html += createRaw(staticContent);
		html += this.post(staticContent).toString.replaceAll("\t", "");
		return html;
	}
	
	/**
	 * Generates the Angular component HTML template code for the given static context type
	 */
	def createRaw(StaticContent staticContent) {
		var html ="";
		html += new HTMLIconTemplate().icons(staticContent.icon,'''
		«{
			
			var innerHtml = "";
			if(staticContent.maxChars > 0 && !preview){
				innerHtml+=getMaxLengthDirective(staticContent);
			}
			if((staticContent.renderMarkdown || staticContent.html) && !preview){
				innerHtml += '''<span [safeInnerHtml]="getSafeHtml(«IF staticContent.renderMarkdown»renderMarkdown(«ENDIF»'«ConventionHelper.replaceDataBindingString(staticContent.rawContent)»'«IF staticContent.renderMarkdown»)«ENDIF»)"></span>'''
			}
			else{
				if(staticContent.renderMarkdown) {
					innerHtml += '''{{renderMarkdown(«ConventionHelper.replaceDataBindingString(staticContent.rawContent)»)}}'''		
			
				} else {
					innerHtml += '''«ConventionHelper.replaceDataBinding(staticContent.rawContent)»'''		
			
				}
			}
			
			if(staticContent.maxChars > 0 && !preview){
				innerHtml+="</span>"
			}
			'''«innerHtml»'''
			
		}»
		''').toString.replaceAll("\t", "");
		return html;
	}
	
	/**
	 * Generates the max length option opening HTML tag
	 * which enables the max length tooltip
	 */
	private def getMaxLengthDirective(StaticContent sc)
	'''<span [maxLength]="'«sc.maxChars»'">'''
	
	/**
	 * Generates the opening HTML tags for the given static context type
	 */
	private def pre(StaticContent content)'''
	<span aria-live="polite" aria-atomic="true" class="text-«getAlignment(content)»«IF content.styling!=null»«IF content.styling.color != Coloring.DEFAULT» text-«new HTMLColoringTemplate().createClass(content.styling)»«ENDIF»«ENDIF»">
	«IF content.styling !=null»
		«IF content.styling.deleted»<del>«ENDIF»
		«IF content.styling.bold»<strong>«ENDIF»
		«IF content.styling.highlight»<mark>«ENDIF»
		«IF content.styling.underline»<u>«ENDIF»
		«IF content.styling.small»<small>«ENDIF»
		«IF content.styling.italic»<em>«ENDIF»
	«ENDIF»
	'''
	
	/**
	 * Generates the closing HTML tags for the given static context type
	 */
	private def post(StaticContent content)'''
	«IF content.styling !=null»
		«IF content.styling.italic»</em>«ENDIF»
		«IF content.styling.small»</small>«ENDIF»
		«IF content.styling.underline»</u>«ENDIF»
		«IF content.styling.highlight»</mark>«ENDIF»
		«IF content.styling.bold»</strong>«ENDIF»
		«IF content.styling.deleted»</del>«ENDIF»
	«ENDIF»
	</span>
	'''
	
	/**
	 * Returns the CSS alignment class depended on the given static content type.
	 * By default the content is left aligned
	 */
	private def CharSequence getAlignment(StaticContent content) {
		if(content.styling!=null){
			
			if(content.styling.alignment == Alignment.CENTER)return "center";
			if(content.styling.alignment == Alignment.JUSTIFIED)return "justify";
			if(content.styling.alignment == Alignment.RIGHT)return "right";
			if(content.styling.alignment == Alignment.CENTER)return "center";
			if(content.styling.alignment == Alignment.NO_WRAP)return "nowrap";
		}
		
		return "left";
	}
}
