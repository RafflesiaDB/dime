package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Badge
import info.scce.dime.gui.gui.StaticContent

class HTMLBadgeTemplate extends GUIGenerator {
	
	/**
	 * Generates the box component Angular HTML template code
	 */
	def create(Badge badge)
	{
		var html = this.pre(badge,preview).toString;
		html += '''
		«FOR StaticContent sc : badge.content»
			«new HTMLStaticContextTemplate().create(sc)»
		«ENDFOR»
		'''
		html += this.post(badge).toString;
		return html;
	}
	
	private def pre(Badge badge,boolean preview)'''
	<!-- beginn badge -->
	<span «badge.printNgIfFor» «badge.printStyle» class="label label-«badge.coloring»">
	'''
	
	private def post(Badge badge)'''
	</span>
	<!-- end badge -->
	'''
	
	private def getColoring(Badge badge)
	{
		switch badge.color {
			case BLUE: return '''primary'''
			case DEFAULT: return '''default'''
			case GREEN: return '''success'''
			case LIGHTBLUE: return '''info'''
			case RED: return '''danger'''
			case YELLOW: return '''warning'''
		}
	}
}