package info.scce.dime.generator.gui.html

import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.ComponentColoring
import info.scce.dime.generator.gui.utils.GUIGenerator

/**
 * Template to convert color constants
 */
class HTMLColoringTemplate extends GUIGenerator {
	
	/**
	 * Converts a color constant to the corresponding HEX code
	 */
	static def createHex(Coloring coloring){
		if(coloring == Coloring.BLUE)return "#337AB7";
		if(coloring == Coloring.DEFAULT)return "#000000";
		if(coloring == Coloring.GREEN)return "#DFF0D8";
		if(coloring == Coloring.LIGHTBLUE)return "#D9EDF7"
		if(coloring == Coloring.RED)return "#F2DEDE"
		if(coloring == Coloring.YELLOW)return "#FCF8E3"
	}
	
	/**
	 * Converts a color constant to the corresponding CSS class
	 */
	static def createClass(Coloring coloring){
		if(coloring == Coloring.BLUE)return "primary";
		if(coloring == Coloring.DEFAULT)return "default";
		if(coloring == Coloring.GREEN)return "success";
		if(coloring == Coloring.LIGHTBLUE)return "info"
		if(coloring == Coloring.RED)return "danger"
		if(coloring == Coloring.YELLOW)return "warning"
	}
	
	/**
	 * Converts a color constant to the corresponding CSS class
	 * or the default class, if no color constant is given
	 */
	def createClass(ComponentColoring coloring){
		if(coloring != null)return createClass(coloring.color);
		return createClass(Coloring.DEFAULT);
	}
	
	/**
	 * Converts a color constant to the corresponding HEX code
	 * or the default HEX code, if no color constant is given
	 */
	def createHex(ComponentColoring coloring){
		if(coloring!=null)return createHex(coloring.color);
		return createHex(Coloring.DEFAULT);
	}
}
