package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Display
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.StaticContent
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular component HTML template code for a panel component
 */
class HTMLPanelTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for a panel component
	 */
	def  CharSequence create(Panel panel){
		var html = this.pre(panel,new HTMLStaticContextTemplate()).toString;
		html += new BaseTemplate().baseContent(ElementCollector.getElementsV(panel.allNodes));
		html += this.post(panel,new HTMLStaticContextTemplate()).toString;
		return html;
	}
	
	/**
	 * Generates the Angular component HTML template code for a panel heading
	 */
	private def pre(Panel panel,HTMLStaticContextTemplate header)'''
	<dime-panel
		«panel.printNgIfFor»
		#dime«panel.id.escapeDart»panel
		[styling]="«panel.justStyle(null)»"
		[cincoId]="'«panel.id.escapeString»'"
		«panel.generalStyle.customAttributes("attributes")»
		[coloring]="'«new HTMLColoringTemplate().createClass(panel.coloring)»'"
		[hasHeading]="«IF panel?.heading?.nullOrEmpty»false«ELSE»true«ENDIF»"
		[collapsable]="«IF panel.collapsable»true«ELSE»false«ENDIF»"
		«IF panel.collapsable»
			«IF !preview»
				«val displayEdge = panel.getIncoming(Display).head»
				«IF displayEdge !== null»
					[initial]="«panel.printIFCondition(displayEdge)»"
					«IF DataDartHelper.isBindedToBoolean(panel,DataAccessType.^FOR,MODE.GET)»
						(clickHeading)="«DataDartHelper.getBindedDataName(panel,DataAccessType.^FOR,MODE.SET_INIT)»($event)"
					«ENDIF»
				«ELSE»
					[initial]="«IF panel.defaultOpen.nullOrEmpty»true«ELSE»«panel.defaultOpen»«ENDIF»"
				«ENDIF»
			«ENDIF»
		«ELSE»
		[initial]="«IF panel.defaultOpen.nullOrEmpty»true«ELSE»«panel.defaultOpen»«ENDIF»"
		«ENDIF»
	>
	«IF panel.heading !== null»
		«IF !panel.heading.nullOrEmpty»
			<span dime-panel-heading«IF !panel.collapsable»-false«ENDIF»>
				«FOR StaticContent sc : panel.heading»
					«header.create(sc)»
				«ENDFOR»
			</span>
		«ENDIF»
	«ENDIF»
		<div *ngIf="!dime«panel.id.escapeDart»panel.isCollapsed||dime«panel.id.escapeDart»panel.isLoaded" class="panel-body">
	'''
	
	/**
	 * Generates the Angular component HTML template code for a panel footer
	 */
	private def post(Panel panel,HTMLStaticContextTemplate footer)
	'''
		</div>
		«IF panel.footer !== null»
			«IF !panel.footer.nullOrEmpty»
				<div class="panel-footer">
				«FOR StaticContent sc : panel.footer»
					«footer.create(sc)»
				«ENDFOR»
				</div>
			«ENDIF»
		«ENDIF»
	</dime-panel>
	'''
}
