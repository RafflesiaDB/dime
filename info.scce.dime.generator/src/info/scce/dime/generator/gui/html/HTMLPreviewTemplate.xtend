package info.scce.dime.generator.gui.html

import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.generator.gui.dart.AngularComponentHTMLTemplate
import info.scce.dime.generator.gui.utils.FileUtils
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin

/**
 * Template to generate the preview plain HTML 5 file
 */
class HTMLPreviewTemplate extends GUIGenerator {
	
	/**
	 * Generates the preview plain HTML 5 file for a given GUI model
	 */
	def String create(GUI gui)
	{
		var styles = gui.additionalStylesheets.map[n|FileUtils.toAbsolutPath(n,gui as GUI)]
			return '''
			<!doctype html>
			<html>
			  <head>
			    <title>Preview «gui.title»</title>
			    <meta charset="utf-8"> 
			    <link type="text/css" rel="stylesheet" href="../target/webapp/app/web/css/bootstrap.min.css" />
			    <link type="text/css" rel="stylesheet" href="../target/webapp/app/web/css/dime.css" />
			    «FOR style:styles»
			    <link type="text/css" rel="stylesheet" href="«style»" />
			    «ENDFOR»
				«FOR p : gui.find(GUIPlugin)»
					«IF (((p as GUIPlugin).function as Function).eContainer as Plugin).style !== null»
						«FOR style:(((p as GUIPlugin).function as Function).eContainer as Plugin).style.files»
							<link type="text/css" rel="stylesheet" href="«FileUtils.toAbsolutPath(style.path,gui as GUI)»"/>
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
			    <script type="text/javascript" src="../target/webapp/app/web/js/jquery-1.12.2.min.js"></script>
				<script type="text/javascript" src="../target/webapp/app/web/js/bootstrap.min.js"></script>
				<script>
					$.noConflict();
				</script>
			    <meta name="viewport" content="width=device-width, initial-scale=1">
			  </head>
			  <body>
				«new AngularComponentHTMLTemplate().preview(gui)»
				«FOR script:gui.additionalJavaScript»
					<script type="text/javascript" src="«FileUtils.toAbsolutPath(script,gui as GUI)»"></script>
				«ENDFOR»
				«FOR p : gui.find(GUIPlugin).filter[((it.function as Function).eContainer as Plugin).script !== null]»
					«FOR script:(((p as GUIPlugin).function as Function).eContainer as Plugin).script.files»
						<script type="text/javascript" src="«FileUtils.toAbsolutPath(script.path,gui as GUI)»"></script>
					«ENDFOR»
				«ENDFOR»
			  </body>
			  
			</html>
			'''
		
	
	}
}