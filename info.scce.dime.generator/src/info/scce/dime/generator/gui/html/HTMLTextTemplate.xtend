package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.StaticContent
import info.scce.dime.gui.gui.Text

/**
 * Template to generate the Angular component HTML template code for a text component
 */
class HTMLTextTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for a text component
	 */
	def create(Text node)
	{
		var staticContent = new HTMLStaticContextTemplate();
		var html = pre(node).toString.replaceAll("\t","");
		for(StaticContent sc : node.content) {
			html += staticContent.create(sc);
		}
		html += post(node).toString.replaceAll("\t","");
		return html
	}
	
	/**
	 * Generates the closing HTML tag for a text component
	 */
	private def post(Text text)
	'''
	</span>
	'''
	
	/**
	 * Generates the opening HTML tag for a text component
	 */
	def pre(Text text)
	'''
	<span «text.printNgIfFor» «text.printStyle»>
	'''
	
	/**
	 * Generates the static content for a text component
	 */
	def createRaw(Text node)
	'''
	«FOR StaticContent content:node.content SEPARATOR " "»
		«content.rawContent»
	«ENDFOR»
	''' 

}
