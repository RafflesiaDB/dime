package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Embedded

/**
 * Template to generate the Angular component HTML template code for a embedded component
 */
class HTMLEmbeddedTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for a embedded component
	 */
	def create(Embedded embedded)
	'''
	<div «embedded.printNgIfFor» «embedded.printStyle» class="embed-responsive embed-responsive-16by9">
	  <iframe class="embed-responsive-item" src="«ConventionHelper.replaceDataBinding(embedded.source)»"></iframe>
	</div>
	'''
}
