package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Placeholder

/**
 * Template to generate the Angular component HTML template code for a placeholder component
 */
class HTMLPlaceholderTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given placeholder component
	 */
	def create(Placeholder cph)
	'''
	<div «cph.printStyle»«cph.printNgIfFor»>
		<ng-content «cph.name.escapeDart.placeholderTransclusion(cph.id.escapeDart)»></ng-content>
	</div>
	'''
}
