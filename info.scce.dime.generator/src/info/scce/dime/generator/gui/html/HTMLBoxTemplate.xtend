package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template for the box component Angular HTML template content
 */
class HTMLBoxTemplate extends GUIGenerator{
	
	/**
	 * Generates the box component Angular HTML template code
	 */
	def create(Box box)
	{
		var html = this.pre(box).toString;
		html += new BaseTemplate().baseContent(ElementCollector.getElementsV(box.allNodes));
		html += this.post(box).toString;
		return html;
	}
	
	private def pre(Box box)'''
	<!-- beginn box -->
	<div «box.printNgIfFor» «box.printStyleWith(box.padding)» class="container«IF !box.isInvisible» page-content«ENDIF»">
	'''
	
	private def post(Box box)'''
	</div>
	<!-- end box -->
	'''
	
	private def getPadding(Box box) {
		if (box.isInvisible) "padding:0px;" else ""
	}
}
