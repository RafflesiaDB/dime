package info.scce.dime.generator.gui.html

import graphmodel.Node
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.ButtonGroupAlignment
import info.scce.dime.gui.gui.ButtonOptions
import info.scce.dime.gui.gui.Dropdown
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.Size
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template for the button group component Angular HTML template content
 */
class HTMLButtonGroupTemplate extends GUIGenerator{
	
	/**
	 * Generates the button group component Angular HTML template code
	 */
	def create(ButtonGroup buttonGroup){
		var html = this.pre(buttonGroup).toString;
		for(Node node:ElementCollector.getElementsH(buttonGroup.allNodes)){
			if(node instanceof Button){
				var button = new HTMLButtonTemplate();
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += node.preButton();
				}
				html += button.create(node);
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += postButton;
				}
			}
			if(node instanceof LinkProcessSIB){
				var button = new HTMLLinkSIBTemplate();
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += node.preButton();
				}
				html += button.create(node);
				if(!node.options.isIsLink && buttonGroup.alignment!=ButtonGroupAlignment.VERTICAL){
					html += postButton;
				}
			}
			if(node instanceof Dropdown){
				var dropdown = new HTMLDropdownTemplate();
				html += node.preButton();
				html += dropdown.create(node);
				html += postButton;
			}
		}
		html += this.post(buttonGroup);
		return html;
	}
	
	/**
	 * Checks if a button should be displayed as a link
	 */
	def boolean getIsIsLink(ButtonOptions options){
		if(options != null)return options.isIsLink;
		return false;
	}
	
	
	
	private def preButton(MovableContainer button)
	'''<div class="btn-group" «button.printNgIf» role="navigation" aria-label="button group">'''
	
	private def postButton()
	'''</div>'''
	
	private def pre(ButtonGroup buttonGroup)
	'''
	<!-- beginn button group -->
	<div «buttonGroup.printNgIfFor»«buttonGroup.printStyle» class="«getAlignment(buttonGroup)»«getSize(buttonGroup)»" role="navigation" aria-label="button group">
	'''
	
	private def post(ButtonGroup buttonGroup)
	'''
	</div>
	<!-- end button group -->
	'''
	
	/**
	 * Generates the CSS classes for the defined size
	 */
	private def getSize(ButtonGroup buttonGroup){
		if(buttonGroup.size == Size.DEFAULT || buttonGroup.alignment == ButtonGroupAlignment.JUSTIFIED)return "";
		return " btn-group-"+ HTMLSizeTemplate.create(buttonGroup.size);
	}
	
	/**
	 * Generates the CSS classes for the defined alignment
	 */
	private def getAlignment(ButtonGroup buttonGroup){
		if(buttonGroup.alignment == ButtonGroupAlignment.VERTICAL)return " btn-group-vertical"
		if(buttonGroup.alignment == ButtonGroupAlignment.JUSTIFIED)return " btn-group btn-group-justified"
		return " btn-group"
	}
}
