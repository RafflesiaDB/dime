package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.ProgressBar

/**
 * Template to generate the Angular component HTML template code for a progress bar component
 */
class HTMLProgressBarTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given progress bar component
	 */
	def create(ProgressBar cpb)
	'''
	<div «cpb.printNgIfFor» «cpb.printStyle» class="progress">
		<div class="progress-bar «getColor(cpb)»«IF cpb.styling!=null»«IF cpb.styling.striped» progress-bar-striped«ENDIF»«IF cpb.styling.animate» active«ENDIF»«ENDIF»" role="progressbar"
		«IF preview»
			style="width:50%;min-width:2em;"
		«ELSE»
			[ngStyle]="{ 'width': («DataDartHelper.getBindedDataName(cpb,DataAccessType.^FOR,MODE.GET)»«IF !cpb.percent»*100.0«ENDIF»).toString() + '%', 'min-width':'2em' }"
		«ENDIF»
		>
		«IF preview»
			50%
		«ELSE»
			{{«DataDartHelper.getBindedDataName(cpb,DataAccessType.^FOR,MODE.GET)»«IF !cpb.percent»*100.0«ENDIF» | number:'1.2-2'}}%
		«ENDIF»
		</div>
	</div>
	'''
	
	/**
	 * Returns the CSS class for the color of the given progress bar component
	 */
	private def CharSequence getColor(ProgressBar cpb)
	{
		if(cpb.styling==null)return '''progress-bar-«HTMLColoringTemplate.createClass(Coloring.DEFAULT)»'''
		return
		'''
		«IF cpb.styling.color != Coloring.DEFAULT || cpb.styling.color != Coloring.BLUE»
		 progress-bar-«HTMLColoringTemplate.createClass(cpb.styling.color)»
		«ENDIF»
		'''
	}
	
}
