package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator

class HTMLLoadingScreen extends GUIGenerator {
	
	def create()
	'''
	<div aria-busy="true" aria-live="polite" aria-atomic="true" class="show-delayed" style="width:100%;height:100%;text-align:center;padding-top: 5%;">
	    	<p style="color: #000000;">Loading..</p>
	    	<div class="progress" style="
	    	    width: 30%;
	    	    margin-top: 20px;
	    	    position: relative;
	    	    left: 35%;">
	    	    <div class="progress-bar progress-bar-striped active" style="width: 100%;"></div>
	    	    </div>
	</div>
	'''
}