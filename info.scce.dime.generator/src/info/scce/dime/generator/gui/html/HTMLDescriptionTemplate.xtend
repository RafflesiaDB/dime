package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Description
import info.scce.dime.gui.gui.DescriptionType
import info.scce.dime.gui.gui.Descriptionentry
import info.scce.dime.gui.gui.StaticContent
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular component HTML template code for a description component
 */
class HTMLDescriptionTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for a given description component
	 */
	def create(Description description) {
		var html = this.pre(description).toString;
		for(Descriptionentry node:ElementCollector.getElementsV(description.allNodes).filter(Descriptionentry)){
			var context = new HTMLStaticContextTemplate();
			html += listEntry(node, description,context);			
		}
		html += this.post(description).toString;
		return html;
	}
	
	/**
	 * Generates the Angular component HTML template code for a given description entry placed
	 * in a description component
	 */
	private def listEntry(Descriptionentry entry,Description list,HTMLStaticContextTemplate context)'''
	«IF list.mode == DescriptionType.LISTGROUP»
	<li «entry.printNgIfFor» «entry.printStyleWith('''«IF list.inline»display:flex;«ENDIF»''')» class="list-group-item">
	    <h4 class="list-group-item-heading" «IF list.inline»style="margin-top:0;"«ENDIF»>
	    «FOR StaticContent sc : entry.description»
	    «context.create(sc)»
	    «ENDFOR»
	    </h4>
	    <p class="list-group-item-text" «IF list.inline»style="margin:4px;"«ENDIF»>
	    «FOR StaticContent sc : entry.content»
		«context.create(sc)»
		«ENDFOR»
		</p>
	 </li>
	«ELSE»
		«entry.printOpeningNgForTemplateTag»
		<dt «entry.printNgIf» «entry.printStyle»>
		«FOR StaticContent sc : entry.description»
		«context.create(sc)»
		«ENDFOR»
		</dt>
		<dd «entry.printNgIf» «entry.printStyle»>
		«FOR StaticContent sc : entry.content»
		«context.create(sc)»
		«ENDFOR»
		</dd>
		«entry.printClosingNgForTemplateTag»
	«ENDIF»
	'''
	
	/**
	 * Generates the opening tag for the given description component
	 */
	private def pre(Description container)'''
	<«getListModeType(container)» «container.printNgIfFor»«container.printStyle» «getListModeTypeClass(container)»>
	'''
	
	/**
	 * Generates the closing tag for the given description component
	 */
	private def post(Description container)'''
	</«getListModeType(container)»>
	'''
	
	/**
	 * Generates the closing and opening tag type for the given description component
	 */
	private def getListModeType(Description list){
		if(list.mode == DescriptionType.LISTGROUP)return "ul";
		return "dl";
	}
	
	/**
	 * Generates the CSS class for the given description component
	 */
	private def getListModeTypeClass(Description list){
		if(list.mode == DescriptionType.LISTGROUP)return '''class="list-group"''';
		if(list.inline)return '''class="dl-horizontal"'''
		return "";
	}
}
