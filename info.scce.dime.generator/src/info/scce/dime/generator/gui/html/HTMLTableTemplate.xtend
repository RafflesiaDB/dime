package info.scce.dime.generator.gui.html

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.AngularCommonComponentTemplate
import info.scce.dime.generator.gui.dart.AngularTableHTMLTemplate
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.DataTarget
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.TableChoice
import info.scce.dime.gui.gui.TableLoad

/**
 * Template to generate the Angular component HTML template code for a table component
 */
class HTMLTableTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given table component
	 */
	def create(Table table,GUICompoundView gcv) {
		if(preview){
			return new AngularTableHTMLTemplate().create(table,table.rootElement);
		}
		return getTabelTag(table,table.rootElement,gcv);
		
	}
	
	/**
	 * Generates the Angular component HTML template code for the given table Angular component tag
	 */
	private def getTabelTag(Table table,GUI gui,GUICompoundView gcv)
	'''
	<table-«ConventionHelper.cincoID(table)»-«gui.title.escapeDart.toFirstLower»
			«new AngularCommonComponentTemplate().tagInputs(table,gui,true,gcv)»
		
			[source«table.id.escapeDart»]="«DataDartHelper.getDataAccess(table.getIncoming(TableLoad).get(0).sourceElement,DataAccessType.^FOR,MODE.GET)»"
		  	(source_update)="«DataDartHelper.getDataAccess(table.getIncoming(TableLoad).get(0).sourceElement,DataAccessType.^FOR,MODE.GET)».replaceAll($event)"
		  	«IF table.choices == TableChoice.SINGLE»
		  	(dime_single_choice)="«DataDartHelper.getBindedDataName(table,DataAccessType.CHOICE,MODE.SET_INIT,DataTarget)»($event)"
		  	«ENDIF»
			«table.componentOutputs("table")»
	>
	</table-«ConventionHelper.cincoID(table)»-«gui.title.escapeDart.toFirstLower»>
	'''
	
	/**
	 * Generates the Angular component dependency declaration, for all
	 * table components in the given container
	 */
	def createDependencies(ModelElementContainer cmc,GUI gui)
	'''
	«FOR Table table : cmc.find(Table).filter[it != cmc]»
		«table.id.escapeDart».Table«ConventionHelper.cincoID(table)»«gui.title.escapeDart.toFirstUpper»,
	«ENDFOR»
	'''

}
