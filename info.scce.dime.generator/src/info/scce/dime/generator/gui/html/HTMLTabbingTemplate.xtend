package info.scce.dime.generator.gui.html

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.ComplexListAttribute
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeName
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GuiFactory
import info.scce.dime.gui.gui.Iteration
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.PrimitiveFOR
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.TabType
import info.scce.dime.gui.gui.Tabbing
import info.scce.dime.gui.helper.ElementCollector
import java.util.ArrayList

/**
 * Template to generate the Angular component HTML template code for a tabbing component
 */
class HTMLTabbingTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given tabbing component
	 */
	def create(Tabbing tabbing){
		var html = this.pre(tabbing).toString;
		for(Tab node:ElementCollector.getElementsH(tabbing.allNodes).filter(Tab)){
			html += createTabHeader(node);
		}
		html += this.prePost().toString;
		for(Tab node:ElementCollector.getElementsH(tabbing.allNodes).filter(Tab)){
			html += tabContent(node);
		}
		html += postPost();
		return html;
	}
	
	/**
	 * Generates the opening HTML tag for the given tabbing component
	 */
	private def pre(Tabbing tabbing)
	'''
	<ul «tabbing.printNgIfFor» «tabbing.printStyle» role="tablist" class="nav nav-«getType(tabbing)»«IF tabbing.justified» nav-justified«ENDIF»">
	'''
	
	/**
	 * Generates the opening HTML tag for the tabs placed in the tabbing component
	 * and the closing HTML tag for the tabbing component
	 */
	private def prePost()
	'''
	</ul>
	<div class="tab-content">
	'''
	/**
	 * Generates the closing HTML tag for the tabs placed in the tabbing component
	 */
	private def postPost()
	'''
	</div>
	'''
	
	/**
	 * Generates the Angular component HTML template code for the given tab component
	 * placed in a tabbing component
	 */
	private def tabContent(Tab tab){
		var elements = new ArrayList();
		if(!tab.find(Placeholder).isEmpty){
			elements.addAll(ElementCollector.getElementsV(tab.allNodes).filter[n|!(n instanceof ButtonGroup)].toList);
		}
		else{
			elements.addAll(ElementCollector.getElementsV(tab.allNodes));
		}
		'''
		«IF !preview»
			«tab.printOpeningNgForTemplateTag»
		«ENDIF»
		«tab.printOpeningNgIfTemplateTag»
		<div
			«IF !preview»
				*ngIf="getActive«ConventionHelper.cincoID(tab)»Tab(«tab.iteratorName»)"
			«ENDIF»
			«tab.printStyle»>
			«new BaseTemplate().baseContent(elements)»
		</div>
		«tab.printClosingNgIfTemplateTag»
		«IF !preview»
			«tab.printClosingNgForTemplateTag»
		«ENDIF»
		'''
	}
	
	/**
	 * Returns the tabbing CSS class for the defined tab type
	 */
	private def getType(Tabbing tabbing){
		if(tabbing.tabtype == TabType.PILL)return "pills";
		if(tabbing.tabtype == TabType.TAB)return "tabs";
		if(tabbing.tabtype == TabType.STACKED_PILLS)return "pills nav-stacked";
	}
	
	/**
	 * Generates the Angular component HTML template code header for the given tab component
	 * placed in a tabbing component
	 */
	private def createTabHeader(Tab tab)
	{
		var iteratorName = tab.iteratorName;
		'''
		«IF !tab.find(Placeholder).isEmpty»
			«FOR node:ElementCollector.getElementsH(tab.findThe(ButtonGroup).buttons)»
				«IF node instanceof Button»
					<li role="presentation"
					«IF !preview»
						[class.disabled]="isBusy()"
						[class.active]="getActive«ConventionHelper.cincoID(node)»Tab(«iteratorName»)"
					«ENDIF»
					>«new HTMLButtonTemplate().create(editButtonForTab(node))»</li>
					«ENDIF»
			«ENDFOR»
		«ELSE»
			«IF !preview»
				«tab.printOpeningNgForTemplateTag»
			«ENDIF»
			<li role="presentation"
			«IF !preview»
				[class.active]="getActive«ConventionHelper.cincoID(tab)»Tab(«iteratorName»)"
				«tab.printNgIfFor»
			«ENDIF»
			>
				<a href
				«IF !preview»
					[class.disabled]="isBusy()"
					data-cinco-id="«tab.id.escapeString»"
					(click)="click«ConventionHelper.cincoID(tab)»Tab($event,«iteratorName»)"
				«ENDIF»
				>«ConventionHelper.replaceDataBinding(tab.label)»
				</a>
			</li>
			«IF !preview»
				«tab.printClosingNgForTemplateTag»
			«ENDIF»
		«ENDIF»
		'''
	}
	
	/**
	 * Returns the constructed iterator name to determine the current active tab in a dynamic context.
	 * If a tab is used in iteration scopes the iterator variables are used the create an ID for the tab
	 * to decide which one is active. 
	 */
	private def String getIteratorName(MovableContainer tab)
	{
		if(!tab.getIncoming(Iteration).empty){
			var iterationEdge =tab.getIncoming(Iteration).get(0);
			var sourceList = iterationEdge.sourceElement;
			//Complex List
			if(sourceList instanceof ComplexVariable){
				if(sourceList.isIsList){
					//search for expanded iterating
					for(clac:sourceList.getOutgoing(ComplexListAttributeConnector).filter[n|n.attributeName==ComplexListAttributeName.CURRENT])
					{
						return (clac.targetElement as ComplexVariable).name.escapeDart+".dywa_id";
					}
					//search for inlined
					if(!sourceList.attributes.filter[n|n instanceof ComplexListAttribute].filter[n|(n as ComplexListAttribute).attributeName==ComplexListAttributeName.CURRENT].empty)
					{
						return "current.dywa_id";
					}
				}
			}
			//Primitive List
			if(iterationEdge instanceof PrimitiveFOR){
				return iterationEdge.iterator.escapeDart;
			}
		}
		if(tab.container != null && tab.container instanceof MovableContainer){
			return getIteratorName(tab.container as MovableContainer);
		}
		return "''";
	}
	
		
	def createTabbingDeclaration(ModelElementContainer gui)
	'''
	«FOR tabbing: gui.find(Tabbing)»
		«IF ElementCollector.getElementsH(tabbing.allNodes).filter(Tab).exists[it.find(Placeholder).isEmpty]»
			String currentbranch«ConventionHelper.cincoID(tabbing)»«IF tabbing.tabs.findFirst[isDefault]!=null» = "«ConventionHelper.cincoID(tabbing.tabs.findFirst[isDefault])»"«ENDIF»;
		«ENDIF»
	«ENDFOR»
	'''
	
	def createTabbingRestart(ModelElementContainer gui)
	'''
	«FOR tabbing: gui.find(Tabbing)»
		«IF ElementCollector.getElementsH(tabbing.allNodes).filter(Tab).exists[it.find(Placeholder).isEmpty]»
			 «IF tabbing.tabs.findFirst[isDefault]!=null»currentbranch«ConventionHelper.cincoID(tabbing)» = "«ConventionHelper.cincoID(tabbing.tabs.findFirst[isDefault])»"«ENDIF»;
		«ENDIF»
	«ENDFOR»
	'''
	
	/**
	 * Transforms a given button component, so that its link option is enabled
	 */
	private def Button editButtonForTab(Button button)
	{
		if(button.options==null){
			button.options = GuiFactory.eINSTANCE.createButtonOptions;
		}
		button.options.isLink = true;
		return button;
	}
}
