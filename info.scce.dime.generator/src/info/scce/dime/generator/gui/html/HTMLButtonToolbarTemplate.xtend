package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.ButtonToolbar
import info.scce.dime.gui.gui.GuiFactory

import static extension info.scce.dime.generator.gui.ConventionHelper.cincoID

/**
 * Template for the button tool bar component Angular HTML template content
 */
class HTMLButtonToolbarTemplate extends GUIGenerator {
	
	ButtonToolbar toolbar
		
	new(ButtonToolbar toolbar) {
		super()
		this.toolbar = toolbar
	}
	
	/**
	 * Generates the button tool bar component Angular HTML template code
	 */
	def create() {
		switch toolbar.role {
			case MENU: createMenu
			case BREADCRUM: createBreadcrum
			default: createDefault
		}
	}
	
	/**
	 * Generates the tool bar default component Angular HTML template code
	 */
	private def createDefault()
	'''
	<!-- beginn button tool bar -->
	<div «ngIfFor» «style» class="btn-toolbar" role="menubar" aria-label="button group">
		«toolbar.allNodes.filter(ButtonGroup).sortBy[x].map[new HTMLButtonGroupTemplate().create(it)].join»
	</div>
	<!-- end button tool bar -->
	'''
	
	/**
	 * Generates the tool bar menu component Angular HTML template code
	 */
	private def createMenu()
	'''
	<!-- beginn button tool bar menu -->
	<nav «ngIfFor» «style» class="navbar navbar-default" role="menubar" aria-label="menu">
		<div class="container-fluid">
			<div class="navbar-header">
			    <button type="button" class="navbar-toggle collapsed"
			            data-toggle="collapse"
			            data-target="#«toolbar.cincoID»"
			            aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			</div>
		    <div class="collapse navbar-collapse"  id="«toolbar.cincoID»">
		        <ul class="nav navbar-nav">
					«toolbar.toHtml»
		        </ul>
		    </div>
		</div>
	</nav>
	<!-- end button tool bar menu -->
	'''
	
	/**
	 * Generates the tool bar bread crumb component Angular HTML template code
	 */
	private def createBreadcrum()
	'''
	<!-- beginn button tool bar breadcrumb -->
	<ol «ngIfFor» «styleWith("background: #F8F8F8;")» class="breadcrumb" role="menubar" aria-label="breadcrumb">
		«toolbar.toHtml»
	</ol>
	<!-- end button tool bar breadcrumb -->
	'''
	
	def toHtml(ButtonToolbar toolbar) {
		toolbar.buttonGroups.sortBy[x].map[
			buttons.sortBy[x].map[toHtml].join
		].join
	}
	
	def toHtml(Button button) {
		new HTMLButtonTemplate().create(button.asLink)
	}
	
	def style() {
		styleWith(null)
	}
	
	def styleWith(String additionalCss) {
		toolbar.printStyleWith(additionalCss)
	}
	
	def ngIfFor() {
		if (preview) toolbar.printNgIf else toolbar.printNgIfFor
	}
	
	/**
	 * Converts a button to a link button
	 */
	static def asLink(Button button) {
		button => [
			options = options ?: GuiFactory.eINSTANCE.createButtonOptions
			=> [ isLink = true ]
		]
	}
}
