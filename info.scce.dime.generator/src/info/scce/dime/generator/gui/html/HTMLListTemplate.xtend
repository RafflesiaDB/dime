package info.scce.dime.generator.gui.html

import graphmodel.Node
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.ListType
import info.scce.dime.gui.gui.Listentry
import info.scce.dime.gui.gui.Listing
import info.scce.dime.gui.gui.StaticContent
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular component HTML template code for a list component
 */
class HTMLListTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given list component
	 */
	def create(Listing list) {
		var html = this.pre(list).toString;
		for(Node node:ElementCollector.getElementsV(list.allNodes)){
			if(node instanceof Listentry){
				var context = new HTMLStaticContextTemplate();
				html += listEntry(node, list,context)
			}
			if(node instanceof Button){
				html += button(node, list);
			}
		}
		html += this.post(list).toString;
		return html;
	}
	
	/**
	 * Generates the Angular component HTML template code for the given list entry component
	 * placed in the list component
	 */
	private def listEntry(Listentry listentry,Listing list,HTMLStaticContextTemplate context)
	'''
	«IF list.mode == ListType.ORDERED || list.mode == ListType.UNORDERED»
		<li	«listentry.printNgIfFor»>
		«FOR StaticContent sc:listentry.content»
			«context.create(sc)»
		«ENDFOR»
		</li>
	«ELSE»
		«FOR StaticContent sc:listentry.content»
			<li «listentry.printNgIfFor» class="list-group-item">«context.create(sc)»</li>
		«ENDFOR»
	«ENDIF»
	'''
	
	/**
	 * Generates the Angular component HTML template code for the given button component
	 * placed in the list component
	 */
	private def button(Button button,Listing list)
	'''
	«IF list.mode == ListType.LISTGROUP»
		«IF new HTMLButtonGroupTemplate().getIsIsLink(button.options)»
			<a href="#"</a>
		«ELSE»
			<button type="button"
		«ENDIF»
		class="list-group-item«IF button.isDisabled» disabled«ENDIF»«getButtonColor(button)»">«button.label»
		«IF new HTMLButtonGroupTemplate().getIsIsLink(button.options)»
			</a>
		«ELSE»
			</button>
		«ENDIF»
	«ENDIF»
	'''
	
	/**
	 * Returns the CSS class for the given button
	 */
	private def getButtonColor(Button button){
		if(button.styling==null)return "";
		if(button.styling.color == Coloring.DEFAULT)return "";
		return " list-group-item-"+new HTMLColoringTemplate().createClass(button.styling);
	}
	
	/**
	 * Returns the opening HTML tag for the given listing component
	 */
	def pre(Listing container)
	'''
	<«getListModeType(container)» «container.printNgIfFor» «container.printStyle» «getListModeTypeClass(container)»>
	'''
	
	/**
	 * Returns the closing HTML tag for the given listing component
	 */
	def post(Listing container)'''
	</«getListModeType(container)»>
	'''
	
	/**
	 * Returns the surrounding HTML tag type for the given listing component
	 */
	private def getListModeType(Listing list){
		if(list.mode == ListType.ORDERED)return "ol";
		return "ul";
	}
	
	/**
	 * Returns the CSS class attribute for the surrounding tags
	 */
	private def getListModeTypeClass(Listing list){
		if(list.inline)return " class=\"list-inline\"";
		if(list.mode == ListType.UNSTYLED)return " class=\"list-unstyled\"";
		if(list.mode == ListType.LISTGROUP)return " class=\"list-group\"";
		return "";
	}
}
