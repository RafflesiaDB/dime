package info.scce.dime.generator.gui.html

import info.scce.dime.dad.dad.URLProcess
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.AngularDartAppTemplate
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.Glyphicon
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.LinkProcessSIB
import info.scce.dime.gui.gui.LinkSIB
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.gui.Size
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.PrimitiveType

/**
 * Template for the button component Angular HTML template content
 */
class HTMLLinkSIBTemplate extends GUIGenerator {
	
	/**
	 * Generates the button component Angular HTML template content.
	 * If present, the surrounding form can be provided.
	 * Additional CSS can be provided.
	 */
	def create(LinkProcessSIB sib)
	'''
	<!-- beginn Link SIB «sib.label» -->
	«IF new HTMLButtonGroupTemplate().getIsIsLink(sib.options)»
		«IF sib.disabled&&sib.hasTooltip»<div style="display: inline-block;" role="tooltip" aria-label="tooltip" «sib.generalStyle.tooltip.createTooltip»>«ENDIF»
		<a «sib.printNgIfFor» «sib.printStyle»
			data-cinco-id="«sib.id.escapeString»"
			tabindex="0" aria-label="«sib.label»"
		«sib.createHref»
		role="button"«IF sib.disabled» disabled«ELSE» [class.disabled]="isBusy()"«ENDIF»>
	«ELSE»
		«IF sib.disabled&&sib.hasTooltip»<div «sib.generalStyle.tooltip.createTooltip»>«ENDIF»
		<a «sib.printNgIfFor» «sib.printStyle»
			data-cinco-id="«sib.id.escapeString»"
			tabindex="0" aria-label="«sib.label»"
		«IF sib.disabled» disabled="disabled" disabled«ELSE» [class.disabled]="isBusy()"«ENDIF»
		«sib.createHref»
		class="btn btn-«getColor(sib)»«getSize(sib)»«IF sib.fullWidth» btn-block«ENDIF»">
	«ENDIF»
	«getButtonLabel(sib)»
	«IF new HTMLButtonGroupTemplate().getIsIsLink(sib.options)»
		</a>
		«IF sib.disabled&&sib.hasTooltip»</div>«ENDIF»
	«ELSE»
		</a>
		«IF sib.disabled&&sib.hasTooltip»</div>«ENDIF»
	«ENDIF»
	<!-- end button «sib.label» -->
	'''
	
	def createHref(LinkProcessSIB sib)
	'''
	«IF sib.isStatic»
		[attr.href]="'«ConventionHelper.replaceDataBindingString(sib.staticURL)»'"
	«ELSEIF !preview»
		«IF !sib.disabled»[routerLink]="«getAction(sib)»"«ENDIF»
	«ENDIF»
	'''
	
	/**
	 * Checks if a button contains a static URL
	 */
	private def boolean getIsStatic(LinkSIB sib){
		if(sib.options!=null){
			return !sib.options?.staticURL.nullOrEmpty
		}
		return false;
	}
	
	
	
	/**
	 * Checks if a button should be displayed in full width
	 */
	private def boolean fullWidth(LinkSIB sib){
		if(sib.styling != null)return sib.styling.fullWidth;
		return false;
	}
	
	/**
	 * Returns the button display label.
	 * If no display label is provided, the label is returned
	 */
	 def getButtonLabel(LinkSIB sib){
		return new HTMLIconTemplate().icons(sib.icon,
		'''
		«IF sib.displayLabel.nullOrEmpty && sib.icon.postIcon==Glyphicon.NONE && sib.icon.preIcon==Glyphicon.NONE»
			«sib.label»
		«ELSE»
			«ConventionHelper.replaceDataBinding(sib.displayLabel)»
		«ENDIF»
		'''
		)
	}
	
	
	/**
	 * Returns the method call for the given button.
	 * This includes the parameter list for all binded variables
	 */
 	def getAction(LinkProcessSIB sib)
	'''getUrl('«AngularDartAppTemplate.routeName(sib.proMod as URLProcess)»',{«sib.parameters("")»})'''
	
	def parameters(LinkSIB sib,String before) 
	'''«FOR input:sib.IOs BEFORE before SEPARATOR ","»'«input.name.escapeDart»': «input.value»«ENDFOR»'''
	
	
	def dispatch value(InputStatic input) {
		input.staticValue
	}
	
	def dispatch value(InputPort input) {
		if(input.incoming.empty) return input.defaultValue
		val node = input.getIncoming(Read).get(0).sourceElement
		return '''«DataDartHelper.getDataAccess(node,DataAccessType.^FOR,MODE.GET)»«IF input.isList».toJoinedQueryString()«ELSEIF input instanceof ComplexInputPort || ((input instanceof PrimitiveInputPort) && (input as PrimitiveInputPort).dataType==PrimitiveType.FILE)»?.dywa_id«ENDIF»'''
		
	}

	
	
	/**
	 * Returns the defined button size CSS class
	 */
	private def CharSequence getSize(LinkSIB sib){
		if(sib.styling == null)return "";
		if(sib.styling.size != Size.DEFAULT)return " btn-"+HTMLSizeTemplate.create(sib.styling.size);
		return ""
	}
	
	/**
	 * Returns the defined button color CSS class
	 */
	private def CharSequence getColor(LinkSIB sib){
		if(sib.styling == null)return HTMLColoringTemplate.createClass(Coloring.DEFAULT);
		return HTMLColoringTemplate.createClass(sib.styling.color);
	}
	

	
	/**
	 * Returns the static URL of the given Link SIB if one is present
	 */
	private def hasTooltip(LinkSIB button){
		button.generalStyle?.tooltip != null	
	}
	
	
}
