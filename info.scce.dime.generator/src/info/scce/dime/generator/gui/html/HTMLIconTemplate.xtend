package info.scce.dime.generator.gui.html

import info.scce.dime.gui.gui.Glyphicon
import info.scce.dime.gui.gui.Icon
import info.scce.dime.generator.gui.utils.GUIGenerator

/**
 * Template to generate the Angular component HTML template code for icons surrounding static content
 */
class HTMLIconTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given icons, which can be used in every static content
	 */
	def icons(Icon icon, String s) {
		if (icon == null)
			return s;
		val labelExists = !s.nullOrEmpty
		return '''«pre(icon.preIcon, labelExists)» «s» «post(icon.postIcon, labelExists)»'''
	}
	
	def pre(Glyphicon g, boolean labelExists) {
		g.create('''style="right:«if (labelExists) 2 else 0»px"''')
	}
	
	def post(Glyphicon g, boolean labelExists) {
		g.create('''style="left:«if (labelExists) 2 else 0»px"''')
	}
	
	/**
	 * Generates the Angular component HTML template code for a given icon.
	 * Applies the specified style String in format 'style="..."'.
	 */
	def create(Glyphicon g, String style) '''
		«IF g != Glyphicon.NONE»
			<span aria-hidden="true" aria-label="icon «getClass(g)»" class="«cssClass(g)»" «style»></span>
		«ENDIF»
	'''
	
	/**
	 * Generates the Angular component HTML template code for a given icon
	 */
	def create(Glyphicon g) {
		create(g, "")
	}
	
	def cssClass(Glyphicon g) '''glyphicon glyphicon-«getClass(g)»'''
	
	/**
	 * Returns the CSS glyphicon class for the given constant
	 */
	private def String getClass(Glyphicon glyphicon) {
	   return glyphicon.literal.replaceAll(
	      String.format("%s|%s|%s",
	         "(?<=[A-Z])(?=[A-Z][a-z])",
	         "(?<=[^A-Z])(?=[A-Z])",
	         "(?<=[A-Za-z])(?=[^A-Za-z])"
	      ),
	      "-"
	   ).toLowerCase;
	}
}
