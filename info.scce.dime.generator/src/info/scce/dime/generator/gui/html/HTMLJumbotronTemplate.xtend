package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.dart.BaseTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular component HTML template code for a jumbotron component
 */
class HTMLJumbotronTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given jumbotron component
	 */
	def create(Jumbotron jumbotron){
		var html = this.pre(jumbotron).toString;
		html += new BaseTemplate().baseContent(ElementCollector.getElementsV(jumbotron.allNodes));
		html += this.post(jumbotron).toString;
		return html;
	}
	
	/**
	 * Returns the opening HTML tag for the given jumbotron component
	 */
	private def pre(Jumbotron jumbotron)
	'''
	<div «jumbotron.printNgIfFor» «jumbotron.printStyle» class="jumbotron">
	'''
	
	/**
	 * Returns the opening HTML tag for the given jumbotron component
	 */
	private def post(Jumbotron jumbotron)
	'''
	</div>
	'''
	
}
