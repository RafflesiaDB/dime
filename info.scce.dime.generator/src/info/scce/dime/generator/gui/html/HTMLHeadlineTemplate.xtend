package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Headline
import info.scce.dime.gui.gui.HeadlineSize
import info.scce.dime.gui.gui.StaticContent

/**
 * Template to generate the Angular component HTML template code for a headline component
 */
class HTMLHeadlineTemplate extends GUIGenerator {

	/**
	 * Generates the Angular component HTML template code for the given headline component
	 */
	def create(Headline node) {
		var staticContent = new HTMLStaticContextTemplate();
		return content(node,staticContent);
	}
	
	/**
	 * Generates the Angular component HTML template code for the given headline component
	 */
	private def content(Headline node,HTMLStaticContextTemplate staticContextTemplate)'''
		<h«getHeadlineSize(node.size)»
		«node.printNgIfFor»
		«_htmlcssTemplate.printStyle(node)»> 
		«FOR StaticContent sc:node.content SEPARATOR "<br />"»
		«staticContextTemplate.create(sc)»
		«ENDFOR»
		</h«getHeadlineSize(node.size)»>
	'''
	
	/**
	 * Returns the headline HTML tag type for the defined headline size
	 */
	private def getHeadlineSize(HeadlineSize hs)
	{
		if(hs == HeadlineSize.BIG) return 1;
		if(hs == HeadlineSize.MEDIUM)return 2;
		if(hs == HeadlineSize.NORMAL)return 3;
		if(hs == HeadlineSize.SMALL)return 4;
	}
	
}
