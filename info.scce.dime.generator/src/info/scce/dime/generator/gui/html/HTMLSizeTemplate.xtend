package info.scce.dime.generator.gui.html

import info.scce.dime.gui.gui.Size

/**
 * Template to generate the Angular component HTML template code for a component size
 */
class HTMLSizeTemplate {
	
	/**
	 * Returns the CSS class suffix depended on the given size constant
	 */
	static def create(Size size){
		if(size == Size.DEFAULT)return "";
		if(size == Size.LARGE)return "lg";
		if(size == Size.SMALL)return "sm";
		if(size == Size.XSMALL)return "xs";
		if(size == Size.MEDIUM)return "md";
		return null;
	}
}
