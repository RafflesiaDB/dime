package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Coloring
import info.scce.dime.gui.gui.DataBinding
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.FormLoadSubmit
import info.scce.dime.gui.gui.Input
import info.scce.dime.gui.gui.InputType
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Size

/**
 * Template to generate the Angular component HTML template code for a form field component
 */
class HTMLFieldTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for a form field component
	 */
	def create(Field field,Form form){
		val previewOrDisabled = preview || field.isDisabled
		val dataBinding = field.getIncoming(DataBinding).head?.sourceElement
	'''
	<div
		#form«field.id.escapeDart»field
«««		«new HTMLCSSTemplate().style(field,preview)»
		«field.printNgIfFor»
	«IF isValidating(field)»
		«IF !previewOrDisabled»[class.has-error]="!«ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').valid && !«ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').pristine"«ENDIF»
	«ENDIF»
«««		«IF field.disabled&&field.hasTooltip»«gc.htmlcssTemplate.createTooltip(field.generalStyle.tooltip)»«ENDIF»
		class="«IF field.inputType != InputType.CHECKBOX»form-group«ENDIF»«getColoring(field)»«getSize(field)»"
		>
	«IF field.inputType != InputType.CHECKBOX»
    		«IF !field.label.nullOrEmpty»<label [attr.for]="form«field.id.escapeDart»field.hashCode.toString()" «field.printStyle»>«ConventionHelper.replaceDataBinding(field.label)»</label>«ENDIF»	
		«IF !form.inline»«helpText(field)»«ENDIF»
	«ENDIF»
   	«IF field.inputType==InputType.SIMPLE_FILE || field.inputType==InputType.ADVANCED_FILE»
   			«createFileField(field,form)»
	«ELSE»
			<«IF field.inputType == InputType.TEXT_AREA»textarea«ELSE»input«ENDIF»
			data-cinco-id="«field.id.escapeString»"
			«IF !previewOrDisabled»[attr.aria-invalid]="!«ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').valid && !«ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').pristine"«ENDIF»
			[attr.id]="form«field.id.escapeDart»field.hashCode.toString()"
			«field.printStyle»
				«IF !preview»ngControl="«ConventionHelper.getFormInputName(field)»"«ENDIF»
				«IF field.inputType == InputType.TEXT_AREA»
					rows="3" cols="50" wrap="hard"
					aria-multiline="true"
					«IF !previewOrDisabled»
					(keyup)="setTextarea«ConventionHelper.cincoID(field)»Change($event)"
						«IF !field.getIncoming(FormLoadSubmit).empty»
							«IF isSubmitButtonPresent(form)»
							[value]="«ConventionHelper.getFormInputName(field)»"
							«ELSE»
							[value]="«DataDartHelper.getDataAccess(dataBinding,DataAccessType.^FOR,MODE.GET)»"
							«ENDIF»
						«ENDIF»
					«ENDIF»
				«ELSE»
					«IF !previewOrDisabled»
						«IF isSubmitButtonPresent(form)»
							[ngModel]="«ConventionHelper.getFormInputName(field)»"
							(ngModelChange)="«ConventionHelper.getFormInputName(field)» = $event"
						«ELSE»
							[ngModel]="«DataDartHelper.getDataAccess(dataBinding,DataAccessType.^FOR,MODE.GET)»"
							«IF dataBinding instanceof PrimitiveVariable»
								(ngModelChange)="«DataDartHelper.getDataAccess(dataBinding,DataAccessType.^FOR,MODE.SET_INIT)»(propagate«(dataBinding as PrimitiveVariable).name.escapeDart»Primitives($event))"
							«ELSE»
								(ngModelChange)="«DataDartHelper.getDataAccess(dataBinding,DataAccessType.^FOR,MODE.SET_INIT)»($event)"
							«ENDIF»
						«ENDIF»
					«ENDIF»
				type="«field.inputType.toHTML»"
				«IF !field.accept.nullOrEmpty»accept="«field.accept»"«ENDIF»
				«ENDIF»
				«IF field.inputType != InputType.CHECKBOX»
				class="form-control «field.inputType.htmlClass»"
				«ENDIF»
				«IF field.disabled» disabled«ELSE» [disabled]="isBusy()"«ENDIF»
				«IF field.additionalOptions != null»
					placeholder="«field.additionalOptions.emptyValue»"
					«IF !field.additionalOptions.helpText.nullOrEmpty» [attr.aria-describedby]="'help'+form«field.id.escapeDart»field.hashCode.toString()"«ENDIF»
					«IF field.additionalOptions.readonly ||field.isDateTime» readonly aria-readonly="true"«ENDIF»
				«ENDIF»
				«IF field.isDateTime»
				[attr.data-date-field]="«ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').hashCode.toString()"
				«ENDIF»
				«IF field.validation != null»
					«IF field.validation.required» required aria-required="true"«ENDIF»
				«ENDIF»
				>«IF field.inputType == InputType.TEXT_AREA»</textarea>«ENDIF»
	
				«IF field.inputType == InputType.CHECKBOX»
		    			«IF !field.label.nullOrEmpty»<label [attr.for]="form«field.id.escapeDart»field.hashCode.toString()" «field.printStyle»>«ConventionHelper.replaceDataBinding(field.label)»</label>«ENDIF»		
				«ENDIF»	
				«IF form.inline»
				«helpText(field)»
				«ENDIF»
	«ENDIF»
	«IF !field.isDisabled && field.validation!=null»
		«IF !field.validation.errorText.nullOrEmpty && isValidating(field)»
		<div
			«IF !previewOrDisabled»
				«IF field.inputType==InputType.SIMPLE_FILE || field.inputType == InputType.ADVANCED_FILE»
					*ngIf="«field.fileValidation(form,"",true)»"
				«ELSE»
					*ngIf="(!«ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').valid) && (!«ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').pristine)"
				«ENDIF»
			«ENDIF»
			class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">Error:</span>
				«field.validation.errorText»
		</div>
		«ENDIF»
	«ENDIF»
	</div>
	
	'''
	}
		
	def getHtmlClass(InputType type) {
		return switch(type) {
			case DATE:'''form_datetime'''
			case DATETIME:'''form_datetime'''
			case MONTH:'''form_datetime'''
			case TIME:'''form_datetime'''
			case WEEK: '''form_datetime'''
			default: ""
		}
	}
		
	def getToHTML(InputType type) {
		return switch(type) {
			case ADVANCED_FILE:'''file'''
			case SIMPLE_FILE:'''file'''
			case DATE:'''text'''
			case DATETIME:'''text'''
			case MONTH:'''text'''
			case TIME:'''text'''
			case WEEK: '''text'''
			default: type.toString.toLowerCase
		}
	}
	
	def boolean getHasTooltip(Field field) {
		field.generalStyle?.tooltip !=null
	}
	
	/**
	 * Generates the HTML code for to display a help text for a given input field component
	 */
	def helpText(Input field)
	'''
	«IF field.additionalOptions !== null»
		«IF !field.additionalOptions.helpText.nullOrEmpty»
		<span [attr.id]="'help'+form«field.id.escapeDart»field.hashCode.toString()" class="help-block">«field.additionalOptions.helpText»</span>
		«ENDIF»
	«ENDIF»
	'''
	
	
	/**
	 * Generates the Angular component HTML template code for a form field component
	 * with FILE input type. An upload form is generated.
	 */
	private def createFileField(Field field,Form form)
	'''
	«IF field.inputType==InputType.SIMPLE_FILE»
			<div
			«IF isValidating(field)»
					«IF !preview»[class.has-error]="«field.fileValidation(form,"",true)»"«ENDIF»
			«ENDIF»
				class="form-group«getColoring(field)»«getSize(field)»"
				>
				<div *ngIf="uploader«ConventionHelper.cincoID(field)».isUploading" style="display: inline-flex;">
					<div style="margin-right:10px;" class="dime-file-loader"></div><span style="margin:auto;">Uploading...</span>
				</div>
					
					<input «field.printStyle» type="file"
					data-cinco-id="«field.id.escapeString»"
					[disabled]="uploader«ConventionHelper.cincoID(field)».isUploading||isBusy()"
					«IF !field.accept.nullOrEmpty»accept="«field.accept»"«ENDIF»
					«IF !preview»
					ngControl="«ConventionHelper.getFormInputName(field)»"
					ng2-file-select [uploader]="uploader«ConventionHelper.cincoID(field)»"
					«ENDIF»
		           	/><br/>
			«IF !preview»
	            <div
	            *ngIf="uploader«ConventionHelper.cincoID(field)».hasError()"
	             class="alert alert-danger" role="alert">
	             {{uploader«ConventionHelper.cincoID(field)».errorMessage()}}
	             </div>
			«ENDIF»
		</div>
	«ELSE»
	<div class="row" «field.printNgIfFor»>
		<div class="col-xs-4">
			<div
			
			«IF isValidating(field)»
			«IF !preview»[class.has-error]="«field.fileValidation(form,"",true)»"«ENDIF»
			«ENDIF»
			class="form-group«getColoring(field)»«getSize(field)»"
			>
				<input «field.printStyle» type="file"
				data-cinco-id="«field.id.escapeString»"
				id="«ConventionHelper.cincoID(field)»"
				«IF !field.accept.nullOrEmpty»accept="«field.accept»"«ENDIF»
				«IF !preview»
				[disabled]="uploader«ConventionHelper.cincoID(field)».isUploading||isBusy()"
				ngControl="«ConventionHelper.getFormInputName(field)»"
				ng2-file-select [uploader]="uploader«ConventionHelper.cincoID(field)»"
				«ENDIF»
	           	/><br/>
	           	«IF !preview»
	            <div
	            *ngIf="uploader«ConventionHelper.cincoID(field)».hasError()"
	             class="alert alert-danger" role="alert">
	             {{uploader«ConventionHelper.cincoID(field)».errorMessage()}}
	             </div>
	             «ENDIF»
			</div>
		</div>

		<div class="col-xs-6">
			<div>
			    <div>
			        Upload progress:
			        <div class="progress">
			            <div
			            	«IF preview»
			            	style="widht:50;min-width:2em"
			            	«ELSE»
		            		style="min-width:2em"
			            	[ngClass]="getUploaderClass(uploader«ConventionHelper.cincoID(field)»)"
			            	[style.width]="uploader«ConventionHelper.cincoID(field)».percentProgress"
			            	«ENDIF»
			            	 role="progressbar" >«IF preview»50«ELSE»{{uploader«ConventionHelper.cincoID(field)».progress}}«ENDIF»%</div>
			        </div>
			    </div>
			    <button type="button" class="btn btn-success btn-s"
			    	«IF !preview»
			            (click)="uploader«ConventionHelper.cincoID(field)».uploadAll()"
			            [disabled]="uploader«ConventionHelper.cincoID(field)».getNotUploadedItems().isEmpty||isBusy()"
			            «ENDIF»
			            >
			        <span class="glyphicon glyphicon-upload"></span> Upload
			    </button>
			    <button type="button" class="btn btn-warning btn-s"
			    	«IF !preview»
			            (click)="uploader«ConventionHelper.cincoID(field)».cancelAll()"
			            [disabled]="!uploader«ConventionHelper.cincoID(field)».isUploading||isBusy()"
			            «ENDIF»
			            >
			        <span class="glyphicon glyphicon-ban-circle"></span> Cancel
			    </button>
			    <button type="button" class="btn btn-danger btn-s"
			    	«IF !preview»
			            (click)="uploader«ConventionHelper.cincoID(field)».clearQueue()"
			            [disabled]="uploader«ConventionHelper.cincoID(field)».queue.isEmpty||isBusy()"
			            «ENDIF»
			            >
			        <span class="glyphicon glyphicon-trash"></span> Remove
			    </button>
			</div>
		</div>
	</div>
	«ENDIF»
	'''
	
	/**
	 * Returns a validation condition for a given file upload form field.
	 */
	static def String fileValidation(Field field,Form form, String operator,boolean considerTouched)
	{
		if(field.validation.required && (field.inputType == InputType.ADVANCED_FILE||field.inputType == InputType.SIMPLE_FILE)){
			return ''' «operator» (uploader«ConventionHelper.cincoID(field)».isUploading«IF field.validation.required»||(uploader«ConventionHelper.cincoID(field)».getFileReferences().isEmpty«IF considerTouched» && «ConventionHelper.getFormName(form)».find('«ConventionHelper.getFormInputName(field)»').touched«ENDIF»)«ENDIF»)''';	
		}
		return "";
	}
	
	/**
	 * Returns the CSS class for the defined color of the given form field component
	 */
	private def getColoring(Field field){
		if (field.styling?.color == Coloring.DEFAULT) return "";
		return " has-"+new HTMLColoringTemplate().createClass(field.styling);
	}
	
	/**
	 * Returns the CSS class for the defined size of the given form field component
	 */
	private def CharSequence getSize(Field field){
		if(field.styling ==null)return "";
		if(field.styling.size == Size.DEFAULT)return "";
		return " form-group-"+HTMLSizeTemplate.create(field.styling.size);
	}
	
	/**
	 * Checks, if the given form field component has to be validated
	 */
	def isValidating(Field field)
	{
		if(field.validation==null)return false;
		return field.validation.required || field.validation.min != -1 || field.validation.max != -1 ;
	}
	
	/**
	 * Checks if a button component is placed in the given form
	 */
	private def isSubmitButtonPresent(Form form)
	{
		return form.find(Button).exists[it.options?.submitsForm]
	}
}
