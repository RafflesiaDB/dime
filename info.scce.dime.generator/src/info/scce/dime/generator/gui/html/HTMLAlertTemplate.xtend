package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.dart.BaseTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Alert
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template for the alter component Angular HTML template content
 */
class HTMLAlertTemplate extends GUIGenerator {
	
	/**
	 * Generates the alter component Angular HTML template code
	 */
	def create(Alert alert)
	'''
	<!-- beginn alert -->
	<div «alert.printNgIfFor»«alert.printStyle» class="alert alert-«new HTMLColoringTemplate().createClass(alert.coloring)»" role="alert">
	«IF alert.isDismissible»
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
	«ENDIF»
		«new BaseTemplate().baseContent(ElementCollector.getElementsV(alert.allNodes))»
	</div>
	<!-- end alert -->
	'''
}
