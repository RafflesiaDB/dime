package info.scce.dime.generator.gui.html

import graphmodel.Node
import info.scce.dime.generator.gui.BaseTemplate
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Image
import info.scce.dime.gui.gui.Thumbnail
import info.scce.dime.gui.helper.ElementCollector
import java.util.ArrayList

/**
 * Template to generate the Angular component HTML template code for a thumb nail component
 */
class HTMLThumbnailTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given thumb nail component
	 */
	def create(Thumbnail thumbnail){
		var html = this.pre(thumbnail).toString;
		var startIdx = 0;
		var list = ElementCollector.getElementsV(new ArrayList<Node>(thumbnail.allNodes));
		if(!list.isEmpty){
			var node = list.get(0)
			if(node instanceof Image){
				var field = new HTMLImageTemplate();
				html += field.create(node,true);
				startIdx = 1;
			}
		}
		html += "<div class=\"caption\">";
		
		html += new BaseTemplate().baseContent(list.subList(startIdx,list.size))
		html += this.post(thumbnail).toString;
		return html;
	}
	
	/**
	 * Generates the opening HTML tag for a thumb nail component
	 */
	def pre(Thumbnail thumbnail)
	'''
	<div «thumbnail.printNgIfFor» «thumbnail.printStyle» class="thumbnail">
	'''
	
	/**
	 * Generates the closing HTML tag for a thumb nail component
	 */
	def post(Thumbnail thumbnail)
	'''
		</div>
	</div>
	'''
}
