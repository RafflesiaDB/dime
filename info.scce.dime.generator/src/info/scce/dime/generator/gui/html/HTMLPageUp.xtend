package info.scce.dime.generator.gui.html

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.PageUp

/**
 * Template to generate the Angular component HTML template code for a page up component
 */
class HTMLPageUp extends GUIGenerator {
	
	/**
	 *Generates the Angular component HTML template code for the given page up component
	 */
	def create(PageUp pu)
	'''
	<a «pu.printNgIfFor» «pu.printStyle» href class="img-circle totop" onclick="jQuery('html, body').animate({ scrollTop: 0 }, 'slow'); return false;">
	    «IF pu.icon !== null»
	    		«new HTMLIconTemplate().create(pu.icon)»
	    	«ENDIF»
	</a>
	'''
}