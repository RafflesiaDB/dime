package info.scce.dime.generator.gui.html

import graphmodel.Node
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.Row
import info.scce.dime.gui.helper.ElementCollector

/**
 * Template to generate the Angular component HTML template code for a row component
 */
class HTMLRowTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given row component
	 */
	def create(Row row) {
		var html = this.pre(row).toString;
		for(Node node:ElementCollector.getElementsH(row.allNodes)){
			if(node instanceof Col){
				var col = new HTMLColTemplate();
				html += col.create(node);
			}
		}
		html += this.post(row).toString;
		return html;
	}
	
	/**
	 * Generates the opening HTML tag for the given row component
	 */
	def pre(Row container)'''
	<div «container.printNgIfFor» «container.printStyle»class="row">
	'''
	
	/**
	 * Generates the closing HTML tag for the given row component
	 */
	def post(Row container)'''
	</div>
	'''
	
}
