package info.scce.dime.generator.gui.html

import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Bar
import info.scce.dime.gui.gui.BottomNavBar
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ButtonGroup
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.Dropdown
import info.scce.dime.gui.gui.Field
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.Glyphicon
import info.scce.dime.gui.gui.LeftNavBarPart
import info.scce.dime.gui.gui.NavBarPart
import info.scce.dime.gui.gui.RightNavBarPart
import info.scce.dime.gui.gui.Row
import info.scce.dime.gui.gui.Text
import info.scce.dime.gui.gui.TopNavBar
import info.scce.dime.gui.helper.ElementCollector
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.gui.LinkProcessSIB

/**
 * Template to generate the Angular component HTML template code for a bar component
 */
class HTMLNavBarTemplate extends GUIGenerator {
	
	/**
	 * Generates the Angular component HTML template code for the given bar component
	 */
	def String create(Bar bar) {
		var html = ""
		if(bar instanceof TopNavBar){
			html += this.preTOP(bar).toString;
			for(LeftNavBarPart part:bar.leftNavBarParts){
				html += getTopNavBar(part);
			}
			for(RightNavBarPart part:bar.rightNavBarParts){
				html += getTopNavBar(part);
			}
			html += this.postTOP(bar).toString;
		}
		else {
			html += this.bottomNavBar(bar as BottomNavBar).toString;
		}
		return html;
	}
	
	/**
	 * Generates the Angular component HTML template code for the given top navigation bar part component
	 */
	def getTopNavBar(NavBarPart part)
	{
		var html ="";
		html += preNav(part instanceof LeftNavBarPart);
			for(Node node:ElementCollector.getElementsH(part.allNodes)){
				if(node instanceof Button){
					html += createButton(node,null);
				}
				if(node instanceof LinkProcessSIB){
					html += createButton(node,null);
				}
				if(node instanceof Text){
					html += navText(node);
				}
				if(node instanceof Form){
					html += preForm(node,part instanceof LeftNavBarPart);
					for(Node element:ElementCollector.getElementsV(node.allNodes)){
						if(element instanceof Field){
							var field = new HTMLFieldTemplate();
							html += field.create(element, node);
						}
						if(element instanceof Button){
							var button = new HTMLButtonTemplate();
							html += button.create(element);
						}
						if(element instanceof LinkProcessSIB){
							var button = new HTMLLinkSIBTemplate();
							html += button.create(element);
						}		
					}
					html += postForm();
				}
				if(node instanceof Dropdown){
					html += createDropdown(node);
				}
				if(node instanceof ButtonGroup){
					for(Node element:ElementCollector.getElementsH(node.allNodes)){
						if(element instanceof Button){
							html += createButton(element,null);
						}
						if(node instanceof LinkProcessSIB){
							html += createButton(node,null);
						}
						if(element instanceof Dropdown){
							html += createDropdown(element);
						}
					}
				}		
			}
			html += postNav();
			return html;
	}
	
	/**
	 * Generates the Angular component HTML template code for the given bottom navigation bar component
	 */
	private def bottomNavBar(BottomNavBar bar)
	'''
	«val row = bar.allNodes.filter(Row).head»
	«IF row !== null»
		<div class="container" role="grid">
		<div «row.printNgIfFor» class="row" role="row"
		«IF bar.isInverted»
			«bar.printStyleWith("border-color: #080808;color: #fff; background-color: #222;")»
		«ELSE»
			«bar.printStyleWith("")»
		«ENDIF»
		>
		«FOR col: row.allNodes.filter(Col)»
        «new HTMLColTemplate().create(col)»
		«ENDFOR»
		    </div>
		</div>
	«ENDIF»
	'''
	
	/**
	 * Generates the Angular opening HTML tags for the given top navigation bar component.
	 * This includes all images placed in the navigation bar. The images are rendered on the left as logos.
	 */
	private def preTOP(TopNavBar bar)
	'''
		<nav «bar.printNgIfFor» «bar.printStyle» role="navigation" class="navbar navbar-«IF bar.fixed»fixed-top«ELSE»static-top«ENDIF» navbar-«IF bar.isInverted»inverse«ELSE»default«ENDIF»" data-spy="affix" data-offset-top="90">
		    <div class="container">
		        <div class="navbar-header">
		            <button role="button" aria-label="navigation toggle" type="button" class="navbar-toggle navbar-brand collapsed" data-toggle="collapse" data-target="#navbar-collapse-«ConventionHelper.cincoID(bar)»" aria-expanded="false">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="glyphicon glyphicon-menu-hamburger"></span>
		            </button>
		            «IF (!bar.label.nullOrEmpty) || bar.hasIcon»
		            <a role="link" tabindex="-1" aria-label="navigation label" class="navbar-brand" href="#" (click)="$event.preventDefault()" style="cursor:default;">«new HTMLIconTemplate().icons(bar.icon,ConventionHelper.replaceDataBinding(bar.label))»</a>
		            «ENDIF»
		        </div>
		    		<div class="collapse navbar-collapse" id="navbar-collapse-«ConventionHelper.cincoID(bar)»">
    '''
	
	def boolean getHasIcon(TopNavBar bar){
		(bar?.icon?.preIcon != Glyphicon.NONE || bar?.icon?.postIcon != Glyphicon.NONE)
	}
    
    /**
	 * Generates the Angular closing HTML tags for the given top navigation bar component.
	 */
    private def postTOP(TopNavBar bar)
    '''
		        </div>
		    </div>
		</nav>
«««	</div>
	'''
	
	/**
	 * Generates the Angular component HTML template code for the given text placed in the top navigation bar component.
	 */
	private def navText(Text text)'''
	<p «text.printNgIfFor» class="navbar-text" «text.printStyle»>«new HTMLTextTemplate().createRaw(text)»</p>
	'''
	
	/**
	 * Generates the Angular opening HTML tags for a top navigation bar part component.
	 */
	private def preNav(boolean left)
	'''
	<ul class="nav navbar-nav«IF !left» navbar-right«ENDIF»" role="menubar">
	'''
	
	
	/**
	 * Generates the Angular closing HTML tags for a top navigation bar part component.
	 */
	private def postNav()'''
	</ul>
	'''
	
	/**
	 * Generates the Angular opening HTML tags for the given form placed in a top navigation bar part component.
	 */
	private def preForm(Form form,boolean left)'''
	 <form «form.printNgIfFor» «form.printStyle»class="navbar-form navbar-«IF left»left«ELSE»right«ENDIF»" role="form">
	'''
	
	/**
	 * Generates the Angular closing HTML tags for the given form placed in a top navigation bar part component.
	 */
	private def postForm()'''
	</form>
	'''
	
	/**
	 * Generates the Angular component HTML template code for the given button placed in a top navigation bar part component.
	 */
	private def createButton(Button button,String additionalCss)
	'''
	<li
	role="menuitem"
	«button.printNgIfFor»
	«button.printStyle»
	«IF !preview»[ngClass]="getActive«ConventionHelper.cincoID(button)»Nav()"«ENDIF»
	>
		<a «button.printStyleWith('''cursor:pointer;''')»
			role="button" tabindex="0" aria-label="«button.label»"
		«IF !preview»
			[class.disabled]="isBusy()"
			(click)="«IF new HTMLButtonTemplate().getEventTriggerParams(button).empty»click«ConventionHelper.cincoID(button)»Nav($event)«ELSE»«new HTMLButtonTemplate().getAction(button, button.findFirstParent(Form))»«ENDIF»"
		«ENDIF»
		>
		«new HTMLButtonTemplate().getButtonLabel(button)»
		</a>
	</li>
	'''
	
	/**
	 * Generates the Angular component HTML template code for the given button placed in a top navigation bar part component.
	 */
	private def createButton(LinkProcessSIB button,String additionalCss)
	'''
	<li
	role="menuitem"
	«button.printNgIfFor»
	«button.printStyle»
	>
		<a «button.printStyleWith('''cursor:pointer;''')»
			role="button" tabindex="0" aria-label="«button.label»"
		«IF !preview»
			«new HTMLLinkSIBTemplate().createHref(button)»
		«ENDIF»
		>
		«new HTMLLinkSIBTemplate().getButtonLabel(button)»
		</a>
	</li>
	'''
	
	/**
	 * Generates the Angular component HTML template code for the given dropdown placed in a top navigation bar part component.
	 */
	def createDropdown(Dropdown dropdown)
	'''
	<li «dropdown.printNgIfFor»«dropdown.printStyle» class="dropdown" role="menuitem">
	  <a href class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" «dropdown.printStyle» aria-expanded="false" tabindex="0">
	   <span style="margin-right: 10px;">«new HTMLIconTemplate().icons(dropdown.icon,ConventionHelper.replaceDataBinding(dropdown.label))»</span>
		<span class="caret"></span>
	  	</a>
	      <ul class="dropdown-menu">
	      «new HTMLDropdownTemplate().createEntries(dropdown)»
	      </ul>
	 </li>
	'''
	
	/**
	 * Generates the needed Angular component Dart class methods for all bar components present in the given container
	 */
	def createMethods(ModelElementContainer gui)
	'''
	«FOR button : gui.find(Bar).flatMap[it.find(Button)]»
		String getActive«ConventionHelper.cincoID(button)»Nav()
		{
			if(this.currentbranch=='«ConventionHelper.cincoID(button)»'){
				return "active";
			}
			return "";
		}
		«IF new HTMLButtonTemplate().getEventTriggerParams(button).empty»
			void click«ConventionHelper.cincoID(button)»Nav(event)
			{
				this.currentbranch = '«ConventionHelper.cincoID(button)»';
				this.«ConventionHelper.getEventName(GUIBranch.getBranchName(button))»«button.id.escapeDart»EventTrigger();
				event.preventDefault();
			}
		«ENDIF»
	«ENDFOR»
	'''
}