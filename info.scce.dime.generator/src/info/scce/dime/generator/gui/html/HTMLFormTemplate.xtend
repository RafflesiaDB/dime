package info.scce.dime.generator.gui.html

import graphmodel.ModelElementContainer
import info.scce.dime.generator.gui.ConventionHelper
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.AngularCommonComponentTemplate
import info.scce.dime.generator.gui.dart.AngularFormHTMLTemplate
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.DataBinding
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.helper.GUIBranch

class HTMLFormTemplate extends GUIGenerator {
	
	def create(Form form,GUICompoundView gcv)
	'''
	«IF preview»
	«new AngularFormHTMLTemplate().create(form,form.rootElement)»
	«ELSE»
	<form-«ConventionHelper.cincoID(form)»-«form.rootElement.title.escapeDart.toFirstLower»
		«new AngularCommonComponentTemplate().tagInputs(form,form.rootElement,true,gcv)»
		«IF!form.getIncoming(DataBinding).empty»
			[formLoad]="«DataDartHelper.getDataAccess(form.getIncoming(DataBinding).get(0).sourceElement,DataAccessType.^FOR,MODE.GET)»"
		«ENDIF»
	  	«form.componentOutputs("form")»
	  	«FOR primitiveVar:form.rootElement.dataContexts.map[primitiveVariables].flatten»
	  	(primitive_«primitiveVar.name.escapeDart»_update)="«primitiveVar.name.escapeDart» = $event"
	  	«ENDFOR»
	  	«FOR complexVar:form.rootElement.dataContexts.map[complexVariables].flatten.filter[complexVariablePredecessors.empty].filter[!isIsList]»
	  	(complex_«complexVar.name.escapeDart»_update)="«complexVar.name.escapeDart» = $event"
	  	«ENDFOR»
	  	role="form"
	>
	</form-«ConventionHelper.cincoID(form)»-«form.rootElement.title.escapeDart.toFirstLower»>
	«ENDIF»
	'''

	def createDependencies(ModelElementContainer cmc,GUI gui)
	'''
	«FOR Form form : cmc.find(Form).filter[it != cmc]»
		«form.id.escapeDart».Form«ConventionHelper.cincoID(form)»«gui.title.escapeDart.toFirstUpper»,
	«ENDFOR»
	'''
	

	
	def createFormMethods(ModelElementContainer gui) {
		val buttons = gui.find(Form).flatMap[it.find(Button)].filter[!disabled].groupBy[label].values.map[head]
	'''
		«FOR button:buttons»
		void formEvent«ConventionHelper.getEventName(GUIBranch.getBranchName(button))»Trigger(Map<String,dynamic> data)
		{
			«IF button.outgoing.empty»
			this.«ConventionHelper.getEventName(GUIBranch.getBranchName(button))».add(data);
			«ENDIF»
		}
	  	«ENDFOR»
	'''
	}
	
	
}
