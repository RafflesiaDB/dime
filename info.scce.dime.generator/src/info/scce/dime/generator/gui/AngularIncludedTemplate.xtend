package info.scce.dime.generator.gui

import info.scce.dime.generator.gui.dart.AngularComponentHTMLTemplate
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.helper.ElementCollector
import java.util.LinkedList
import java.util.List
import graphmodel.ModelElementContainer
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.Icon
import info.scce.dime.gui.gui.Glyphicon
import info.scce.dime.generator.gui.html.HTMLIconTemplate

/**
 * Template for creating the Angular HTML code for an embedded GUI SIB in a GUI model.
 */
class AngularIncludedTemplate extends GUIGenerator{ 

	/**
	 * Returns the HTML Code for a given GUI SIB in a GUI model.
	 * The GUI SIB is rendered as the representing Angular component tag for the corresponding GUI model. 
	 * If the openAsModel flag is true of the GUI SIB, a modal dialog is rendered around the GUI SIB tag.
	 * If the preview mode is activated, the template code for the corresponding GUI model is in lined.
	 */
	def CharSequence create(GUISIB cg)
	{
		val guiModel = (cg as GUISIB).gui
		val spezializingGUIs = guiModel.subGUIs
		var result = '''«cg.printOpeningNgForTemplateTag»'''
		result += createSingleSIB(cg,guiModel)
		if(preview || !(cg instanceof DispatchedGUISIB)) {
			result += '''«cg.printClosingNgForTemplateTag»'''
			return result
		}
		for(subGUI:spezializingGUIs) {
			result+= createSingleSIB(cg,subGUI)
		}
		result += '''«cg.printClosingNgForTemplateTag»'''
		return result
	}
	
	def dispatchMethod(ModelElementContainer cec,DispatchedGUISIB cg)
	{
		if(!(cg instanceof DispatchedGUISIB)) return ""
		val guiModel = cg.gui
		val spezializedGUIs = guiModel.subGUIs
		val sortedSpezializedGUIs = getSortedDispatchedGUIs(guiModel,spezializedGUIs)
		var result = cg.createSingleSIBcheckMethod(guiModel,guiModel.findAllSubGUIs(sortedSpezializedGUIs).toList,cec)
		for(subGUI:spezializedGUIs) {
			result+= cg.createSingleSIBcheckMethod(subGUI,subGUI.findAllSubGUIs(sortedSpezializedGUIs).toList,cec)
		}
		return result
	}
	
	def String dispatchMethodName(GUISIB cg,GUI dispatchedGUI)
	'''check«cg.id.escapeDart»Dispatch«dispatchedGUI.id.escapeDart»'''
	
	def String createSingleSIBcheckMethod(GUISIB cg,GUI dispatchedGUI,List<GUI> directSubGUIs,ModelElementContainer cec)
	{
		val form = cg.embeddingForm
		'''
		/// checks if the «dispatchedGUI.title» GUI
		/// is dispatched for the «cg.label» GUI SIB
		bool «cg.dispatchMethodName(dispatchedGUI)»(«cg.guiDispatchableInputs.map[name.escapeDart].join(",")»)
		{
			return 
			(«FOR directSub:directSubGUIs»
			// check not dispatchable sub GUI «directSub.title»
			!(«cg.generateInputDispatch(directSub)») &&
			«ENDFOR» true) &&
			«cg.generateInputDispatch(dispatchedGUI)»;
		}
		«IF cg.modal!=null»
		void extendClosed«cg.id.escapeDart»Modal(dynamic e) {
			hasBeen«cg.id.escapeDart»Opened.add(e);
			«IF cg.modal.options.submitsForm && form!=null && cec instanceof Form»
			//submit form
			this.«ConventionHelper.getFormName(form)»Submit(«ConventionHelper.getFormName(form)».value);
			«ENDIF»
		}
		
		bool isInClosed«cg.id.escapeDart»Modal(String s) {
			return hasBeen«cg.id.escapeDart»Opened.contains(s);
		}
		«ENDIF»
		'''
	}
	
	def String methods(ModelElementContainer cec,GUISIB cg)
	{
		val form = cg.embeddingForm
		'''
		«IF cg.modal!=null »
		
		void extendClosed«cg.id.escapeDart»Modal(dynamic e) {
			hasBeen«cg.id.escapeDart»Opened.add(e.toString());
			«IF cg.modal.options?.submitsForm && form!=null && cec instanceof Form»
			//submit form
			this.«ConventionHelper.getFormName(form)»Submit(«ConventionHelper.getFormName(form)».value);
			«ENDIF»
		}
		
		bool isInClosed«cg.id.escapeDart»Modal(String s) {
			return hasBeen«cg.id.escapeDart»Opened.contains(s);
		}
		«ENDIF»
		'''
	}
	
	def String generateInputDispatch(GUISIB cg,GUI dispatchedGUI)
	'''
	«FOR input:cg.guiDispatchableInputs SEPARATOR " &&"»
		«IF input.isIsList»
		(«input.name.escapeDart».every((n) => n is «DyWASelectiveDartGenerator.prefix(input.toSpecialType(dispatchedGUI))».«input.toSpecialType(dispatchedGUI).name.escapeDart»))
		«ELSE»
		(«input.name.escapeDart» is «DyWASelectiveDartGenerator.prefix(input.toSpecialType(dispatchedGUI))».«input.toSpecialType(dispatchedGUI).name.escapeDart»)
		«ENDIF»
	«ENDFOR»
	'''
	
	
	
	def String createSingleSIB(GUISIB cg,GUI dispatchedGUI)
	'''
	«IF cg instanceof DispatchedGUISIB»<template [ngIf]="check«cg.id.escapeDart»Dispatch«dispatchedGUI.id.escapeDart»(«cg.guiDispatchableInputsData.join(",")»)">«ENDIF»
	«IF cg.modal != null»
		<dime-modal
			«cg.printNgIf»
			#dime«cg.id.escapeDart»modal
			[isLink]="«IF cg.modal.options?.isIsLink»true«ELSE»false«ENDIF»"
			[isDisbaled]="«IF cg.modal?.styling?.disabled»true«ELSE»false«ENDIF»"
			«IF !cg.modal.buttonLabel.nullOrEmpty»
			[label]="'«ConventionHelper.replaceDataBindingString(cg.modal.buttonLabel)»'"
			«ELSE»
			[label]="'«ConventionHelper.replaceDataBindingString(cg.name)»'"
			«ENDIF»
			«IF cg.modal.icon!==null»
				«IF cg.modal.icon.preIcon!==Glyphicon.NONE»
				[hasBeforeIcon]="true"
				[beforeIconClass]="'«new HTMLIconTemplate().cssClass(cg.modal.icon.preIcon)»'"
				«ENDIF»
				«IF cg.modal.icon.preIcon!==Glyphicon.NONE»
				[hasAfterIcon]="true"
				[afterIconClass]="'«new HTMLIconTemplate().cssClass(cg.modal.icon.postIcon)»'"
				«ENDIF»
			«ENDIF»
			[reload]="«IF cg.modal.refreshContent»true«ELSE»false«ENDIF»"
			[title]="'«ConventionHelper.replaceDataBindingString(cg.modal.modalTitle)»'"
			[modalClass]="'btn btn-«cg.modal.printColorClass»«cg.modal.printSizeClass»«IF cg.modal?.styling?.fullWidth» btn-block«ENDIF»'"
			(opened)="extendClosed«cg.id.escapeDart»Modal($event)"
			>
			<template [ngIf]="dime«cg.id.escapeDart»modal.isShown||dime«cg.id.escapeDart»modal.isLoaded">
	«ENDIF»
		«IF preview»
		«{
			var g = cg.gui;
			
			'''«new AngularComponentHTMLTemplate().preview(g)»'''
			}»
		«ELSE»
			
			<«TemplateHelper.tagFormat(dispatchedGUI.title)»
				«cg.printStyle»
				«cg.printNgIf»
				[modalDialog]="«IF cg.modal !=null»true«ELSE»false«ENDIF»"
			  	[currentbranch]="currentbranch"
			  	[ismajorpage]="false"
			  	[runtimeId]="runtimeId"
			  	[guiId]="guiId"
			  	«cg.guiInputs(dispatchedGUI)»
			  	«cg.guiOutputs(dispatchedGUI,cg)»
			  >
			«FOR placeHolder:cg.arguments.map[arg|dispatchedGUI.find(Placeholder).findFirst[name.equals(arg.blockName)]]»
				<div «placeHolder.name.escapeDart.argumentTransclusion(placeHolder.id.escapeDart)»>
				«new BaseTemplate().baseContent(ElementCollector.getElementsV(cg.arguments.findFirst[blockName.equals(placeHolder.name)].allNodes))»
				</div>
			«ENDFOR»
			  </«TemplateHelper.tagFormat(dispatchedGUI.title)»>
	  	«ENDIF»
	 	«IF cg.modal !=null»
	 	</template>
		</dime-modal>
	 	«ENDIF»
	 	«IF cg instanceof DispatchedGUISIB»</template>«ENDIF»
	'''
	

	
	/**
	 * Returns the distinct set of all given GUI SIBs depended on the
	 * referenced GUI model.
	 */
	def distinct(List<GUISIB> guis)
	{
		var result = new LinkedList<GUISIB>();
		var uuids = new LinkedList<String>();
		for(g:guis)
		{
			if(!uuids.contains( (g as GUISIB).libraryComponentUID))
			{
				result.add(g);
				uuids.add((g as GUISIB).libraryComponentUID);
			}
		}
		return result;
	}
	
	def declaration(GUISIB sib)
	'''List<String> hasBeen«sib.id.escapeDart»Opened = new List();'''
	
	def restart(GUISIB sib)
	'''hasBeen«sib.id.escapeDart»Opened = new List();'''
}
