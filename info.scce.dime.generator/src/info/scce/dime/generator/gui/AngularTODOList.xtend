package info.scce.dime.generator.gui

import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.TODOList

/**
 * Template for the HTML Angular code for todo List component
 */
class AngularTODOList extends GUIGenerator {
	
	/**
	 * Renders the HTML Angular code for a given todo list component.
	 * If the preview mode is enabled, only plain HTML5 is rendered.
	 */
	def createTemplate(TODOList element)
	'''
	«IF !preview»<login-form *ngIf="hasToSignIn«ConventionHelper.cincoID(element)»" (signedin)="hasSignedIn«ConventionHelper.cincoID(element)»($event)"></login-form>«ENDIF»
	<div «element.printStyle»class="panel panel-default"
	«IF !preview»*ngIf="!hasToSignIn«ConventionHelper.cincoID(element)»"«ENDIF»
	>
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-6">
					<p><strong>«element.caption»</strong></p>
				</div>
				<div class="col-md-6">
					<input class="form-control"
					«IF !preview»(keyUp)="filter«ConventionHelper.cincoID(element)»TODOs($event)"«ENDIF»
					placeholder="Filter..."/>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="list-group">
				«IF! element.emptyValue.nullOrEmpty»
				<a
					style="cursor:default;"
					«IF !preview»*ngIf="get«ConventionHelper.cincoID(element)»TODOs().length<=0"«ENDIF»
					class="list-group-item"
				>
					<div class="row">
						<div class="col-sm-12">
							<div class="media">
						        <div class="media-body" style="vertical-align: middle;">
						            <h4 class="media-heading">«IF preview»TODO Entry«ELSE»{{getTodoList«ConventionHelper.cincoID(element)»EmptyValue()}}«ENDIF»</h4>
						        </div>
							</div>
						</div>
					</div>
					
				</a>
				«ENDIF»
				<a
					«IF !preview»*ngFor="let entry of get«ConventionHelper.cincoID(element)»TODOs()"«ENDIF»
					href
					«IF !preview»(click)="clickTODOList«ConventionHelper.cincoID(element)»Entry(entry,$event)"«ENDIF»
					class="list-group-item"
				>
					<div class="row">
						<div class="col-sm-12">
							<div class="media" style="cursor:pointer;">
								<div class="media-left" style="vertical-align: middle;">
									<div style="display: inline-block; position: relative; width: 100px; height: 64px;">
										«IF !preview»
				                        <img  [src]="getTodoList«ConventionHelper.cincoID(element)»Image(entry)" [alt]="getTodoList«ConventionHelper.cincoID(element)»Title(entry)" height="64" style="position: absolute; top: 0; left: 0;">
				                        <img  [src]="getTodoList«ConventionHelper.cincoID(element)»ImageOverlay(entry)" [alt]="getTodoList«ConventionHelper.cincoID(element)»Title(entry)" height="64" style="position: absolute; top: 0; left: 0;">
										«ENDIF»
				                    </div>
								</div>
						        <div class="media-body" style="vertical-align: middle;">
						            <h4 class="media-heading">«IF preview»Title«ELSE»{{ getTodoList«ConventionHelper.cincoID(element)»Title(entry) }}«ENDIF»</h4>
						            «IF preview»Description«ELSE»{{ getTodoList«ConventionHelper.cincoID(element)»Description(entry) }}«ENDIF»
						        </div>
							</div>
						</div>
					</div>
					
		
				</a>
			</div>
		</div>
	</div>
	'''
	
	
	
}
