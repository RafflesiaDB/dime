package info.scce.dime.generator.gui.utils

import info.scce.dime.generator.gui.IOHelper
import info.scce.dime.generator.gui.data.SelectiveExtension
import info.scce.dime.generator.util.JavaIdentifierUtils
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.html.HTMLCSSTemplate
import info.scce.dime.generator.gui.AngularIFFORTemplate
import info.scce.dime.generator.gui.TemplateHelper
import info.scce.dime.data.helper.DataExtension

abstract class GUIGenerator {
	
	protected GenerationContext genctx
	
	protected extension GUIExtension _guiExtension
	protected extension DataExtension _dataExtension
	protected extension SelectiveExtension _selectiveExtension
	protected extension TemplateHelper _templateHelper
	protected extension IOHelper _ioHelper
	protected extension AngularIFFORTemplate _iffor
	protected extension HTMLCSSTemplate _htmlcssTemplate = new HTMLCSSTemplate
	
	
	new() {
		setGenerationContext(GenerationContext.instance)
	}
	
	def setGenerationContext(GenerationContext genctx) {
		this.genctx = genctx
		this._guiExtension = genctx.guiExtension
		this._dataExtension = _guiExtension.dataExtension
		this._selectiveExtension = new SelectiveExtension(genctx)
		this._templateHelper = new TemplateHelper(genctx)
		this._ioHelper = new IOHelper(genctx)
		this._iffor = new AngularIFFORTemplate(genctx)
	}
	
	static def escapeDart(CharSequence s) {
		JavaIdentifierUtils.escapeDart(s)
	}
	
	static def escapeString(CharSequence s) {
		JavaIdentifierUtils.escapeString(s.toString)
	}
	
	static def escapeJava(CharSequence s) {
		JavaIdentifierUtils.escapeJava(s)
	}
	
	static def preview() {
		false
	}
}