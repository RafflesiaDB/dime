package info.scce.dime.generator.gui.utils

import graphmodel.GraphModel
import info.scce.dime.dad.dad.DAD
import info.scce.dime.gui.gui.GUI
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.Set
import org.apache.commons.io.IOUtils
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.ResourcesPlugin

class FileUtils {
	static def String toAbsolutPath(String wsPath,GUI gui)
	{
		if(wsPath.startsWith("http")){
			return wsPath;
		}
		var f = toFile(wsPath,gui);
		return f.absolutePath;
	}
	
	static def toFile(String wsPath, GraphModel model) {
		val platformString = model.eResource.URI.toPlatformString(true)
		val iRes= ResourcesPlugin.workspace.root.findMember(platformString)
		toFile(wsPath, iRes.project)
	}
	
	static def toFile(String wsPath, IProject project) {
		new File(project.location.append(wsPath).toString)
	}
	
	static def void joinFiles(String destination, Set<String> paths,DAD dad)
            throws IOException {
        var OutputStream output = null;
        val destFile = new File(destination)
        new File(destFile.parent).mkdirs
        if(!destFile.createNewFile){
        		destFile.delete
        		destFile.createNewFile
        }
        try {
            output = createAppendableStream(destFile);
            for (path : paths) {
            		val sourceFile = FileUtils.toFile(path, dad);
                appendFile(output, sourceFile);
            }
        } finally {
            IOUtils.closeQuietly(output);
        }
    }

    private static def BufferedOutputStream createAppendableStream(File destination)
            throws FileNotFoundException {
        return new BufferedOutputStream(new FileOutputStream(destination, true));
    }

    private static def void appendFile(OutputStream output, File source)
            throws IOException {
        var InputStream input = null;
        try {
            input = new BufferedInputStream(new FileInputStream(source));
            IOUtils.copy(input, output);
        } finally {
            IOUtils.closeQuietly(input);
        }
    }
}
