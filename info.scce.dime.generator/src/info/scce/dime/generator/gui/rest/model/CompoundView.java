package info.scce.dime.generator.gui.rest.model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import info.scce.dime.generator.gui.data.SelectiveMap;
import info.scce.dime.gui.gui.ComplexVariable;

public class CompoundView extends Parent {
	
	private Set<TypeView> compounds;
	
	private Set<ComplexVariable> importViews;
	
	private SelectiveMap pairs;
	
	public CompoundView()
	{
		this.compounds = new HashSet<TypeView>();
		this.importViews = new HashSet<ComplexVariable>();
		this.pairs = new SelectiveMap();
	}
	
	public Set<TypeView> getCompounds() {
		return compounds;
		
	}

	public void setCompounds(Set<TypeView> compounds) {
		this.compounds = compounds;
	}

	public Set<ComplexVariable> getImportViews() {
		return importViews;
	}

	public void setImportViews(Set<ComplexVariable> importViews) {
		this.importViews = importViews;
	}

	
	public List<TypeView> getAllTypeViews()
	{
//		return this.pairs.entrySet().stream().map(n->n.getValue()).filter(n->n instanceof TypeView).map(n->(TypeView)n).collect(Collectors.toList());
		List<TypeView> tvs = new LinkedList<TypeView>();
		for(TypeView tv:this.getCompounds())
		{
			tvs.add(tv);
			tvs.addAll(getAllFieldTypeViews(tv));
		}
		return tvs;
	}
	
	
	private List<TypeView> getAllFieldTypeViews(TypeView tv)
	{
		List<TypeView> tvs = new LinkedList<TypeView>();
		if(tv instanceof ComplexTypeView){
			ComplexTypeView ctv = (ComplexTypeView) tv;
			for(ComplexFieldView fv : ctv.getDisplayedFields().stream().filter(n->n instanceof ComplexFieldView).map(n->(ComplexFieldView)n).collect(Collectors.toList()))
			{
				tvs.add(fv.getView());
				tvs.addAll(getAllFieldTypeViews(fv.getView()));
			}
		}
		return tvs;
	}

	public SelectiveMap getPairs() {
		return pairs;
	}

	public void setPairs(SelectiveMap pairs) {
		this.pairs = pairs;
	}

	@Override
	public boolean isPrimitive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPrimitiveBoolean() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
}
