package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.data.data.Type;
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator;
import info.scce.dime.gui.gui.GUI;

public class BaseComplexTypeView extends TypeView {
	
	protected Type type;
	
 
	public BaseComplexTypeView(Type type) {
		super();
		this.type = type;
		if(type == null) {
			throw new IllegalStateException("Type not null");
		}
	}

	/**
	 * Copy constructor.
	 *
	 * @param p the copy
	 */
	protected BaseComplexTypeView(BaseComplexTypeView tv) {
		super(tv);
		this.type = tv.type;
		if(type == null) {
			throw new IllegalStateException("Type not null");
		}
	}

	@Override
	public boolean isPrimitive() {
		return false;
	}

	@Override
	public boolean isPrimitiveBoolean() {
		return false;
	}

	public Type getType() {
		return type;
	}
	
	public void setType(Type t) {
		type = t;
	}


	public String getTypeName() {
		return type.getName();
	}

	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BaseComplexTypeView)) {
			return false;
		}
		final BaseComplexTypeView that = (BaseComplexTypeView) obj;
		String thatId = DyWASelectiveDartGenerator.getSelectiveHashCodeJava(that).toString();
		return DyWASelectiveDartGenerator.getSelectiveHashCodeJava(this).toString().equals(thatId);
	}
	
	@Override
    public int hashCode() {
		return DyWASelectiveDartGenerator.getSelectiveHashCodeJava(this).toString().hashCode();
    }

}
