package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.data.data.PrimitiveType;

public class PrimitiveFieldView extends FieldView{

	private PrimitiveType primitiveType;
	
	public PrimitiveFieldView(PrimitiveType pt) {
		primitiveType = pt;
	}

	public PrimitiveType getPrimitiveType() {
		return primitiveType;
	}
	
	public PrimitiveTypeView getPrimitiveTypeView() {
		PrimitiveTypeView ptv = new PrimitiveTypeView();
		ptv.setData(getData());
		ptv.setGModel(getgModel());
		ptv.setId(getId());
		ptv.setList(isList());
		ptv.setName(getName());
		ptv.setPrimitiveType(getPrimitiveType());
		ptv.setScopes(getScopes());
		return ptv;
	}

	public void setPrimitiveType(PrimitiveType primitiveType) {
		this.primitiveType = primitiveType;
	}

	@Override
	public boolean isPrimitive() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isPrimitiveBoolean() {
		// TODO Auto-generated method stub
		return primitiveType==PrimitiveType.BOOLEAN;
	}
	
	
}
