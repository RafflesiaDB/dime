package info.scce.dime.generator.gui.rest

import info.scce.dime.generator.rest.DyWAAbstractGenerator
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.Process
import org.eclipse.core.runtime.IPath

import static extension info.scce.dime.generator.process.BackendProcessGeneratorUtil.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import static extension info.scce.dime.gui.helper.GUIExtension.lastModified2

class InteractionContextDartGenerator {
	
	extension ProcessExtension = new ProcessExtension
	
	def generate(Process interaction, IPath outlet) {
		
		val targetDir = outlet.append("app-presentation/target/generated-sources/app/lib/")
		
		val String content = generate(interaction).toString();
		val String packageName = ".models.";
		val String fileName = "InteractionWrapper" + interaction.modelName.escapeDart + interaction.id.escapeDart +".dart";
		
		DyWAAbstractGenerator.generate(content, packageName, fileName, targetDir, interaction.lastModified2);
	}

	private def generate(Process interaction) '''
		import 'FileReference.dart';
		
		class InteractionWrapper«interaction.modelName.escapeDart»«interaction.id.escapeDart» { 
		
			String id;
			String interactId;
			String guardContainerId;
			String displayName;
			String description;
			String imagePath;
			String overlayImagePath;
			_«interaction.interactionInputsTypeNameDart» interactionInputs;
			
			InteractionWrapper«interaction.modelName.escapeDart»«interaction.id.escapeDart»(json) {
				this.id = json['id'];
				this.interactId = json['interactId'];
				this.guardContainerId = json['guardContainerId'];
				this.displayName = json['displayName'];
				this.description = json['description'];
				this.imagePath = json['imagePath'];
				this.overlayImagePath = json['overlayImagePath'];
				this.interactionInputs = new _«interaction.interactionInputsTypeNameDart»(json['interactionInputs']);
			}
		}
			
		class _«interaction.interactionInputsTypeNameDart» {
			
			«FOR input : interaction.processInputs»
				«input.renderType» «input.name.escapeDart»;
				
				«input.renderType» get«input.name.escapeDart»() {
					return this.«input.name.escapeDart»;
				}
			«ENDFOR»
			
			_«interaction.interactionInputsTypeNameDart»(json) {
				
				«FOR input : interaction.processInputs»
					«IF input.isIsList»
						«input.name.escapeDart» = new DIMEList«IF !input.primitiveBoolean»<«input.renderInnerType»>«ENDIF»();
						«val supplier = '''
						«IF input instanceof ComplexOutputPort»
							(i) => i
						«ELSE»
							«IF input instanceof PrimitiveOutputPort && (input as PrimitiveOutputPort).dataType == PrimitiveType.FILE»
								(i) => new FileReference(jsog:i)
							«ELSEIF (input as PrimitiveOutputPort).dataType == PrimitiveType.TEXT»
								(i) => i.toString()
							«ELSEIF (input as PrimitiveOutputPort).dataType == PrimitiveType.BOOLEAN»
								(i) => i.toString()=='true'
							«ELSE»
								(i) => «input.renderInnerType».parse(i)
							«ENDIF»
						«ENDIF»
						'''»
						«input.name.escapeDart».addAll(json['«input.name.escapeString»'].map(«supplier»));
					«ELSE»
						«input.name.escapeDart» = json['«input.name.escapeString»'];
					«ENDIF»
				«ENDFOR»
			}
		}
	'''
	
	private def static renderType(OutputPort output) '''
«««		TODO: use global utility method?
		«IF output.isIsList»
			DIMEList«IF !output.isPrimitiveBoolean»<«ENDIF»
		«ENDIF»
		«output.renderInnerType»
		«IF output.isIsList && !output.isPrimitiveBoolean»
			>
		«ENDIF»
	'''
	
	private static def isPrimitiveBoolean(OutputPort port){
		if(port instanceof PrimitiveOutputPort){
			return port.dataType==PrimitiveType.BOOLEAN
		}
		return false
	}
	
	private def static renderInnerType(OutputPort output) '''
		«IF output instanceof PrimitiveOutputPort» 
«««			TODO: use global utility method?
			«switch(output.dataType){
				case BOOLEAN: '''«IF !output.isList»bool«ENDIF»'''
				case INTEGER: "int"
				case REAL: "double"
				case TEXT: "String"
				case TIMESTAMP: "DateTime"
				case FILE: "FileReference"
			}»
		«ELSE»
			int
		«ENDIF»
	'''
}