package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.process.process.Process;

import java.util.HashMap;
import java.util.Map;

public class LongRunningProcessCompoundView extends Parent {
	
	private Process process;
	private Map<String, CompoundView> resumePoints;

	public LongRunningProcessCompoundView(Process process) {
		this.process = process;
		this.resumePoints = new HashMap<String, CompoundView>();
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public Map<String, CompoundView> getResumePoints() {
		return resumePoints;
	}

	public void setResumePoints(Map<String, CompoundView> resumePoints) {
		this.resumePoints = resumePoints;
	}

	@Override
	public boolean isPrimitive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPrimitiveBoolean() {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
