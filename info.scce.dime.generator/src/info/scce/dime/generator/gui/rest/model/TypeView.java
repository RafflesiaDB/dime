package info.scce.dime.generator.gui.rest.model;

public abstract class TypeView extends Parent {

	public TypeView() {
		super();
	}

	/**
	 * Copy constructor.
	 *
	 * @param p the copy
	 */
	protected TypeView(TypeView tv) {
		super(tv);
	}

}
