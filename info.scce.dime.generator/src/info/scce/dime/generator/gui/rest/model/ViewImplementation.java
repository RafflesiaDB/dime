package info.scce.dime.generator.gui.rest.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import graphmodel.GraphModel;

public class ViewImplementation {
	
	private GraphModel sourceGraph;
	
	private ComplexTypeView ctv;
	
	public ViewImplementation(GraphModel sourceGraph,ComplexTypeView ctv) {
		if(sourceGraph==null||ctv==null){
			throw new IllegalStateException();
		}
		this.sourceGraph = sourceGraph;
		this.ctv=ctv;
		
	}

	
	public Map<GraphModel,ViewImplementation> getTransitivSubTypeViews(Set<GraphModel> cache){
		if(cache.contains(sourceGraph)){
			return Collections.emptyMap();
		}
		cache.add(sourceGraph);
		Map<GraphModel,ViewImplementation> gs = new HashMap<>();
		gs.put(sourceGraph,this);
		if(ctv!=null){
			// find all ctv reachable and ctv itself by implementation
			ctv.getAllSubTypes().stream().flatMap(n->n.getInterfaces().stream()).forEach(n->gs.putAll(n.getTransitivSubTypeViews(cache)));
		}
		return gs;
	}




	public ComplexTypeView getParent() {
		return ctv;
	}


	public GraphModel getSourceGraph() {
		return sourceGraph;
	}

	
	
	
	
}
