package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.GUI;

/**
 * The GUI compound view represents the root of the tree of selective
 * compound views and holds a reference to the given GUI model.
 * @author zweihoff
 *
 */
public class GUICompoundView extends CompoundView{
	private GUI cgui;
	
	
	public GUICompoundView(GUI gui)
	{
		super();
		this.cgui = gui;
		this.gModel = gui;
	}
	
	public GUI getGui() {	
		return (GUI) cgui;
	}

	public void setGui(GUI gui) {
		this.cgui = gui;
	}
	
	public GUI getGUI()
	{
		return this.cgui;
	}

	public GUI getCgui() {
		return cgui;
	}

	public void setCgui(GUI cgui) {
		this.cgui = cgui;
	}
	
	public String toString(){
		return "gui."+this.cgui.getTitle();
	}

	
	
}
