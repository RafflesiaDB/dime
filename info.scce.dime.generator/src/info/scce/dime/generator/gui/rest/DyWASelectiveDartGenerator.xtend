package info.scce.dime.generator.gui.rest

import graphmodel.GraphModel
import graphmodel.ModelElement
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.FieldView
import info.scce.dime.generator.gui.rest.model.Parent
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.gui.rest.model.TypeViewUtils
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.process.process.ComplexDirectDataFlow
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.PrimitiveInputPort

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import info.scce.dime.process.process.ExtensionAttribute

class DyWASelectiveDartGenerator extends DyWADartGenerator {
	
	

	static def getSelectiveNameJava(BaseComplexTypeView n) '''«n.selectiveNameInternal.escapeJava»«IF n instanceof ComplexTypeView»«n.getgModel?.renderGraphIdSuffix»«ENDIF»'''
	static def getSelectiveHashCodeJava(BaseComplexTypeView n) '''«(n.typeName.toFirstUpper + n.id).escapeJava»«n.getgModel.renderGraphIdSuffix»'''
	
	static def getSelectiveNameDart(ComplexTypeView n) '''«n.getSelectiveNameDartInternal.toString.toFirstUpper.escapeDart»'''
	private static def getSelectiveNameDartInternal(ComplexTypeView n) '''«IF n.compositionToSameOrParentType»«n.compositionView.typeName.toFirstUpper»«n.compositionView.id»«ELSE»«n.typeName.toFirstUpper»«n.id»«ENDIF»'''
	private static def getSelectiveNameInternal(BaseComplexTypeView n) '''«n.typeName.toFirstUpper»Selective«n.id»«IF n instanceof ComplexTypeView»«IF n.compositionToSameOrParentType»_COMP_«n.compositionView.id»«ENDIF»«ENDIF»'''
	
	private static def renderGraphIdSuffix(GraphModel model)
		'''«model?.id?.escapeJava»'''
	
	static def Type dataModelType(ModelElement node)
	{
		if(node instanceof info.scce.dime.gui.gui.ComplexAttribute){
			return node.attribute.dataType;
		}
		if(node instanceof ComplexVariable){
			return node.dataType;
		}
		if(node instanceof info.scce.dime.process.process.ComplexAttribute){
			return node.attribute.dataType;
		}
		if(node instanceof info.scce.dime.process.process.ComplexVariable){
			return node.dataType;
		}
		if(node instanceof ExtensionAttribute){
			if(node.attribute.isComplex) {
				return node.attribute.complexDataType
			}
		}
		if(node instanceof info.scce.dime.gui.gui.ExtensionAttribute){
			if(node.attribute.isComplex) {
				return node.attribute.complexDataType
			}
		}
		if(node instanceof Type){
			return node;
		}
		if(node instanceof ComplexDirectDataFlow) {
			return (node.sourceElement as ComplexOutputPort).dataType
		}
		if(node instanceof ComplexAttribute){
			return (node as ComplexAttribute).dataType;
		}
		if(node instanceof info.scce.dime.data.data.ExtensionAttribute){
			if(node.isComplex) {
				return node.complexDataType
			}
		}
		return null
	}
	

	static def isEncoding(TypeView tv)
	{
		if(tv instanceof ComplexTypeView)return true;
		if(tv instanceof PrimitiveTypeView){
			return tv.primitiveType==PrimitiveType.FILE
		}
		return false;
	}
	
	static def isEncoding(InputPort tv)
	{
		if(tv instanceof ComplexInputPort)return true;
		if(tv instanceof PrimitiveInputPort){
			return tv.dataType==info.scce.dime.process.process.PrimitiveType.FILE
		}
		return false;
	}
	
	static def isTimeStamp(Parent tv)
	{
		if(tv instanceof ComplexTypeView ||tv instanceof ComplexFieldView)return false;
		if(tv instanceof PrimitiveTypeView){
			return tv.primitiveType==PrimitiveType.TIMESTAMP
		}
		if(tv instanceof PrimitiveFieldView){
			return tv.primitiveType==PrimitiveType.TIMESTAMP
		}
		return false;
	}
	
	static def isTimeStamp(FieldView tv)
	{
		if(tv instanceof ComplexFieldView)return false;
		if(tv instanceof PrimitiveFieldView){
			return tv.primitiveType==PrimitiveType.TIMESTAMP
		}
		return false;
	}
	
	static def isEncoding(FieldView tv)
	{
		if(tv instanceof ComplexFieldView)return true;
		if(tv instanceof PrimitiveFieldView){
			return tv.primitiveType==PrimitiveType.FILE
		}
		return false;
	}
	
	static def isEncoding(Parent tv)
	{
		if(tv instanceof FieldView)return isEncoding(tv);
		if(tv instanceof TypeView)return isEncoding(tv);
		return false;
	}
	
	
	
	static def String prefix(Type type)
	{
		return TypeViewUtils.prefix(type)
	}
	
	def static prefix(Data data) {
		return TypeViewUtils.prefix(data)
	}
	
	
	
	static def attributeType(PrimitiveFieldView fieldView,boolean considerList)
	'''
			«IF fieldView.isList && considerList»
				DIMEList«IF !fieldView.primitiveBoolean»<«ENDIF»
			«ENDIF»
			«getLiteral((fieldView).primitiveType,fieldView.list)»
			«IF fieldView.isList && considerList»
				«IF !fieldView.primitiveBoolean»>«ENDIF»
			«ENDIF»
	'''
	
	def String attributeType(Parent parent,boolean considerList,boolean getter,boolean init)
	{
		if(parent instanceof TypeView)return attributeType(parent as TypeView,considerList,getter,init).toString;
		if(parent instanceof PrimitiveFieldView)return attributeType(parent,considerList).toString;
		if(parent instanceof ComplexFieldView)return attributeType((parent as ComplexFieldView).view as ComplexTypeView,considerList,getter,init).toString;
		return "dynamic";
	}
	
	
	
	
}
