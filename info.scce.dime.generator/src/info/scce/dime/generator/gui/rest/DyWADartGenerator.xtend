package info.scce.dime.generator.gui.rest

import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.FieldView
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import org.eclipse.core.runtime.IPath
import info.scce.dime.generator.rest.DyWAAbstractGenerator
import info.scce.dime.generator.gui.rest.model.Parent
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView

class DyWADartGenerator extends DyWAAbstractGenerator {
	
	def generate(IPath targetDir) {
		val String package = ".models.";
		
		val String fileReferenceContent = renderFileReference().toString();
		val String fileReferenceFileName = "FileReference.dart";
		
		DyWAAbstractGenerator.generate(fileReferenceContent, package, fileReferenceFileName, targetDir,1);
	}
	
	
	static def getDefaultPrimitiveValue(Parent pt) {
		if(pt instanceof PrimitiveTypeView)return getDefaultPrimitiveValue(pt.primitiveType)
		if(pt instanceof PrimitiveFieldView)return getDefaultPrimitiveValue(pt.primitiveType)
	}
	
	static def getDefaultPrimitiveValue(PrimitiveType pt) {
		switch pt {
			case BOOLEAN: "false"
			case TEXT: "''"
			case INTEGER: "0"
			case REAL: "0.0"
			case FILE: "new FileReference()"
			case TIMESTAMP: "new DateTime.now()"
		}
	}
	
	static def getDefaultPrimitiveValue(info.scce.dime.process.process.PrimitiveType pt) {
		switch pt {
			case BOOLEAN: "false"
			case TEXT: "''"
			case INTEGER: "0"
			case REAL: "0.0"
			case FILE: "new FileReference()"
			case TIMESTAMP: "new DateTime.now()"
		}
	}
	
	
	private def renderFileReference() '''
		import 'dart:convert';
		class FileReference {
		
			int dywa_id;
			String fileName;
			String contentType;
			
			FileReference({jsog}) {
				
				// default constructor
				if (jsog == null) {
					this.dywa_id = -1;
					this.fileName = "";
					this.contentType = "";
				}
				// from jsog
				else {
					this.dywa_id = jsog["dywaId"];
					this.fileName = jsog["fileName"];
					this.contentType = jsog["contentType"];
				}
			}
			
			Map<String,dynamic> toJSOG(Map<String,dynamic> objects)
			{
				return {
					'dywaId' : this.dywa_id,
					'fileName' : this.fileName,
					'contentType' : this.contentType
				};
			}
			String toJSON()
			{
				return jsonEncode(this.toJSOG(new Map<String,dynamic>()));
			}
			
			@override
			String toString()
			{
				return this.fileName;
			}
		}
	'''
	
	
	
	
	protected static def getLiteral(PrimitiveType pType,boolean isList){
		switch(pType){
			case BOOLEAN: '''«IF !isList»bool«ENDIF»'''
			case INTEGER: "int"
			case REAL: "double"
			case TEXT: "String"
			case TIMESTAMP: "DateTime"
			case FILE: "FileReference"
		}
	}
	
	protected static def getLiteral(info.scce.dime.process.process.PrimitiveType pType,boolean isList){
		switch(pType){
			case BOOLEAN: '''«IF !isList»bool«ENDIF»'''
			case INTEGER: "int"
			case REAL: "double"
			case TEXT: "String"
			case TIMESTAMP: "DateTime"
			case FILE: "FileReference"
		}
	}
	
	static def fieldType(FieldView fieldView) '''
		«IF fieldView instanceof ComplexFieldView»
			«evaluateList(fieldView, generateTOName(fieldView).toString)»
		«ELSEIF fieldView instanceof PrimitiveFieldView»
			«evaluateList(fieldView, getLiteral((fieldView.field as PrimitiveAttribute).dataType,fieldView.isList).toString)»
		«ENDIF»
	'''
	
	static def evaluateList(FieldView typeView, String content) '''
		«IF typeView.isList»
			DIMEList«IF! typeView.primitiveBoolean»<«ENDIF»
		«ENDIF»
		«content»
		«IF typeView.isList»
			«IF! typeView.primitiveBoolean»>«ENDIF»
		«ENDIF»
	'''
}