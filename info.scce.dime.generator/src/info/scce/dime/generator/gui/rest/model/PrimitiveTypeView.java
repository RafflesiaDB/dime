package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.data.data.PrimitiveType;

public class PrimitiveTypeView extends TypeView{
	private PrimitiveType primitiveType;

	public PrimitiveTypeView() {}
	
	/**
	 * Copy constructor.
	 * 
	 * @param p the copy
	 */
	public PrimitiveTypeView(PrimitiveTypeView ptv) {
		super(ptv);
		this.primitiveType = ptv.primitiveType; 
	}
	
	public PrimitiveType getPrimitiveType() {
		return primitiveType;
	}
	public void setPrimitiveType(PrimitiveType primitiveType) {
		this.primitiveType = primitiveType;
	}
	@Override
	public boolean isPrimitive() {
		return true;
	}
	@Override
	public boolean isPrimitiveBoolean() {
		return primitiveType == PrimitiveType.BOOLEAN;
	}
	
}
