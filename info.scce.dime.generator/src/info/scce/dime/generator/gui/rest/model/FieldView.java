package info.scce.dime.generator.gui.rest.model;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ExtensionAttribute;

public abstract class FieldView extends Parent {
	
	private Attribute field;
	
	
	public Attribute getField() {
		return field;
	}

	public void setField(Attribute field) {
		this.field = field;
	}
	
	public boolean isExtensionAttribute() {
		return field instanceof ExtensionAttribute;
	}
	

}
