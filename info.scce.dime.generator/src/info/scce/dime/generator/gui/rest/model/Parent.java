package info.scce.dime.generator.gui.rest.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.Variable;

public abstract class Parent {

	private String id;
	private String name;
	private ModelElement data;
	private List<MovableContainer> scopes;
	private boolean isList; 
	protected GraphModel gModel;

	/**
	 * Copy constructor.
	 *
	 * @param p the copy
	 */
	public Parent(Parent p) {
		this.id = p.id;
		this.name = p.name;
		this.data = p.data;
		this.scopes = new ArrayList<>(p.scopes);
		this.isList = p.isList;
		this.gModel = p.gModel;
	}
	
	public Parent(){
		scopes = new LinkedList<MovableContainer>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@java.lang.Override
	public boolean equals(final java.lang.Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Parent)) {
			return false;
		}
		final Parent that = (Parent) obj;
		return this.getId() == that.getId();
	}
	
    public GraphModel getgModel() {
		return gModel;
	}
    
    public void setGModel(GraphModel g) {
		gModel = g;
	}
    
    public boolean isInput() {
    	if(data instanceof Variable) {
    		return ((Variable) data).isIsInput();
    	}
    	return false;
    }


	@Override
    public int hashCode() {
        return id.hashCode();
    }
    
	public ModelElement getData() {
		return data;
	}

	public void setData(ModelElement data) {
		this.data = data;
	}

	public boolean isList() {
		return isList;
	}

	public void setList(boolean isList) {
		this.isList = isList;
	}

	public List<MovableContainer> getScopes() {
		return scopes;
	}

	public void setScopes(List<MovableContainer> scopes) {
		this.scopes = scopes;
	}

	public abstract boolean isPrimitive();
	
	public abstract boolean isPrimitiveBoolean();
	
}
