package info.scce.dime.generator.gui.rest.model;

import graphmodel.GraphModel;
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator;

public class ComplexFieldView extends FieldView{
	private TypeView view;
	
	private String typeName; 
	
	private boolean isInherited;
	
	public ComplexFieldView(GraphModel g)
	{
		this.gModel = g;
	}
	
	public TypeView getView() {
		return view;
	}

	public void setView(TypeView view) {
		this.view = view;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public boolean isIn() {
		return isInherited;
	}

	public void setInherited(boolean isInherited) {
		this.isInherited = isInherited;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ComplexFieldView)) {
			return false;
		}
		if(this.view == null) return false;
		final ComplexTypeView that = (ComplexTypeView) ((ComplexFieldView)obj).getView();
		
		String thatId = DyWASelectiveDartGenerator.getSelectiveNameJava(that).toString();
		return DyWASelectiveDartGenerator.getSelectiveNameJava((ComplexTypeView)this.view).toString().equals(thatId);
	}
	
	@Override
    public int hashCode() {
		return DyWASelectiveDartGenerator.getSelectiveNameJava((ComplexTypeView)this.view).toString().hashCode();
    }

	@Override
	public boolean isPrimitive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPrimitiveBoolean() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
