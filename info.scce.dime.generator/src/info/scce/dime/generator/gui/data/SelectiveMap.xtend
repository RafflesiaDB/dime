package info.scce.dime.generator.gui.data

import graphmodel.ModelElement
import info.scce.dime.generator.gui.rest.model.Parent
import java.util.HashMap
import java.util.LinkedList

class SelectiveMap {
	
	private HashMap<String, Parent> map = newHashMap
	private HashMap<String, ModelElement> elements = newHashMap
	
	
	def containsKey(ModelElement elm) {
		map.containsKey(elm.id)
	}
	
	def get(ModelElement elm) {
		map.get(elm.id)
	}
	
	def getElements() {
		new LinkedList(elements.values)
	}
	
	def keySet(){
		map.keySet
	}
	
	def entrySet(){
		map.entrySet
	}
	
	def put(ModelElement elm, Parent value)
	{
		val id = elm.id
		if(this.containsKey(elm)){
			val preValue = map.get(id)
			if (preValue.id != value.id){
				println("Adding already known key: "+id+"\npre:"+preValue+"\n replacedby: "+value+"\n---")
			}
			if (map.remove(id) == null){
				throw new IllegalStateException("Could not remove contained key "+id)
			}
			map.put(id,value)
			elements.put(id,elm)
			return preValue
		}
		elements.put(id,elm)
		return map.put(id,value)
	}
	
}