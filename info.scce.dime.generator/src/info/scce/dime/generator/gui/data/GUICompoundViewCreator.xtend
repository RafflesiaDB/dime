package info.scce.dime.generator.gui.data

import graphmodel.Edge
import graphmodel.GraphModel
import graphmodel.ModelElement
import graphmodel.Node
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.FieldView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.rest.model.Parent
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.gui.rest.model.ViewImplementation
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.ChoiceData
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexListAttribute
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeName
import info.scce.dime.gui.gui.ComplexRead
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DataTarget
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.FormLoadSubmit
import info.scce.dime.gui.gui.FormSubmit
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.Iteration
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.ProcessSIB
import info.scce.dime.gui.gui.Select
import info.scce.dime.gui.gui.Table
import info.scce.dime.gui.gui.TableChoice
import info.scce.dime.gui.gui.TableLoad
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.gui.Write
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.Process
import java.util.Collections
import java.util.HashSet
import java.util.LinkedList
import java.util.List
import java.util.Map
import java.util.Map.Entry
import java.util.Set
import java.util.stream.Collectors
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.helper.GUIBranchPort
import info.scce.dime.gui.helper.ComplexGUIBranchPort
import info.scce.dime.gui.gui.ExtensionAttribute

/** 
 * The compound view creator is used to build up a structure of
 * selective views. This Dart class structure represents all variables and attributes
 * of the given GUI model which has actually been used in the given GUI.
 * @author zweihoff
 */
class GUICompoundViewCreator {
	
	protected extension DataExtension = DataExtension.instance
	protected extension GUIExtension guiExtension
	protected extension SelectiveExtension selectiveExtension 
	
	GenerationContext genctx
	
	ExpressionEqualizer ee
	GUI gui
	GUICompoundView guiCV
	
	Set<Variable> variablesInUse = newHashSet
	Map<Node, ComplexTypeView> iterationVariables = newHashMap
	SelectiveMap pairs = new SelectiveMap
	Set<Attribute> attributesInUse = newHashSet
	Set<ComplexVariable> importVariables = newHashSet
	
	static def createCompoundView(GenerationContext genctx, GUI gui) {
		new GUICompoundViewCreator(gui, genctx).collect(newHashMap)
	}
	
	/** 
	 * Prepares the internal structures for the given GUI compound view.
	 * Since GUI SIBs and interaction process SIBs can be present in the GUI,
	 * the sets of already build up compound views are given
	 * as well to use this previous calculated results
	 * @param gui
	 * @param guis
	 * @param processes
	 */
	 new(GUI gui, GenerationContext genctx) {
	 	this.genctx = genctx
		this.gui = gui
		this.guiExtension = genctx.guiExtension
		this.selectiveExtension = new SelectiveExtension(genctx)
		this.ee = new ExpressionEqualizer(gui.find(MovableContainer))
	}
	
	/** 
	 * Prepares the internal structures for the given GUI compound view.
	 * Since GUI SIBs and interaction process SIBs can be present in the GUI
	 * referenced by the given GUI SIB,
	 * the sets of already build up compound views are given
	 * as well to use this previous calculated results
	 * @param gui
	 * @param guis
	 * @param processes
	 */
	 new(GUISIB guiSib, Map<String, Set<GuardContainer>> guardContainers, GenerationContext genctx) {
		this(guiSib.gui, genctx)
	}
	
	/** 
	 * Returns the name of a compound view Dart file
	 * for a given compound view
	 * @param g
	 * @return
	 */
	def static String getGUIViewName(GUICompoundView g) {
		return '''GUI_«g.getGui().getTitle()»''' 
	}
	/** 
	 * Returns the name of a compound view Dart file
	 * for a given GUI model
	 * @param g
	 * @return
	 */
	def static String getGUIViewName(GUI g) {
		return '''GUI_«g.getTitle()»''' 
	}
	/** 
	 * Returns the GUI which is the base of this compound view
	 * @return
	 */
	def GUI getGUI() {
		return gui 
	}
	
	/**
	 * Determines if a complex type view is extended by itself,
	 * so that this is a recursive usage. The given complex type is
	 * marked as in recursive usage
	 */
	static def public void addRecursiveTypeView(ComplexTypeView ctv,GraphModel graphModel) {
		ctv.interfaces.forEach[vi|{
				//CHECK RECURSION IN DATA USAGE
				val transVis = vi.getTransitivSubTypeViews(new HashSet())
				if(vi.parent.getgModel.equals(graphModel)||transVis.containsKey(graphModel)){
					var viewImpls = transVis.get(graphModel)
					if(viewImpls==null&&vi.parent.getgModel.equals(graphModel)){
						viewImpls = vi
					}

					if(!ctv.equals(viewImpls.parent)) {
						if(graphModel instanceof GUI){
							println('''[INFO] -> «ctv.name» of «(ctv.getgModel as GUI).title» is same as «viewImpls.parent.name» from «(viewImpls.parent.getgModel as GUI).title»''')
						}
						if(graphModel instanceof Process) {
							println('''[INFO] -> «ctv.name» of «(ctv.getgModel as Process).modelName» is same as «viewImpls.parent.name» from «(viewImpls.parent.getgModel as Process).modelName»''')
						}
						ctv.toSameType = viewImpls.parent;
					}
				}

		}]
	}
	
	/** 
	 * Add Fields to the ComplexTypeView ctv which are inherited from the ViewImplemetation
	 * @param ctv The ComplexTypeView which will be extended with additional Fields
	 * @param vi The Implementing ComplexVariable
	 */
	def private void addInheritetFields(String id, ComplexTypeView ctv, ViewImplementation vi) {
		if (vi === null || vi.getParent() === null) return;
		if (vi.parent.getgModel instanceof GUI) {
			addInheritetFields(ctv,vi.parent) 
		}
	}
	
	
	
	/** 
	 * Add The Fields of the Super-Selective internalCTV to the implementing Selective Complex Type View 
	 * @param ctv Complex type view to be extended
	 * @param internalCTV The Complex Type View which will be implemented (is extending)
	 */
	def private void addInheritetFields(ComplexTypeView ctv, ComplexTypeView internalCTV) {
		
		
		debug("Add inherited fields")
		debug(" > ComplexTypeView: " + ctv)
		debug(" > Internal ComplexTypeView: " + internalCTV)
		
		/**
		 * Check if the internal CTV of a sub component is under recursion (composition)
		 */
		rebuildComposition(ctv,internalCTV,this.guiCV)
		
		//Modify inherited fields that ARE already contained, but has to implement the fields of the Super-Selective internalCTV
		ctv.getDisplayedFields().filter(ComplexFieldView).filter[n | (internalCTV.getDisplayedFields().stream().filter([e | e.getName().equals(n.getName())]).findAny().isPresent())].forEach[n | {
			var ComplexFieldView inheritedCFV=(internalCTV.getDisplayedFields().stream().filter([e | e.getName().equals(n.getName())]).findFirst().get() as ComplexFieldView) 
			addInheritedFieldViewCopy(n, inheritedCFV) //OK
		}]
		// Add inherited field that are NOT already contained in the Selective ctv
		var List<FieldView> inheritedFields=internalCTV.getDisplayedFields().stream().filter([n | (!ctv.getDisplayedFields().stream().filter([e | e.getName().equals(n.getName())]).findAny().isPresent())]).collect(Collectors.toList()) 
		var int index=1 + ctv.getDisplayedFields().size() 
		for (FieldView fv : inheritedFields) {
			val newFieldView = getFieldViewCopy(fv, fv.getScopes(), index, ctv)
			if(newFieldView instanceof ComplexFieldView) {
				GUICompoundViewCreator.addRecursiveTypeView(newFieldView.view as ComplexTypeView,this.gui)				
			}
			ctv.getDisplayedFields().add(newFieldView) 				
			index++ 
		}

	}

	
	/** 
	 * Add The Fields of the Super-Selective internalCTV to the implementing Selective ctv
	 * @param ctv The complex type view to be extended
	 * @param internalCTV The complex type view from a sub component which will extend ctv
	 */
	def private void addInheritetFields(String id, ComplexTypeView ctv, ComplexTypeView internalCTV) {
		
		/**
		 * Check if the internal CTV of a sub component is under recursion (composition)
		 */
		rebuildComposition(ctv,internalCTV,this.guiCV)
		
		//Modify inherited fields that ARE already contained, but has to implement the fields of the Super-Selective internalCTV
		ctv.getDisplayedFields().filter(ComplexFieldView).filter[n | internalCTV.getDisplayedFields().exists[e | e.getName().equals(n.getName())]].forEach[n | {
			var ComplexFieldView inheritedCFV=(internalCTV.getDisplayedFields().findFirst[e | e.getName().equals(n.getName())] as ComplexFieldView) 
			addInheritedFieldViewCopy(id, n, inheritedCFV) 
		}]
		// Add inherited field that are NOT already contained in the Selective ctv
		var List<FieldView> inheritedFields=internalCTV.getDisplayedFields().stream().filter([n | (!ctv.getDisplayedFields().stream().filter([e | e.getName().equals(n.getName())]).findAny().isPresent())]).collect(Collectors.toList()) 
		var int index=1 + ctv.getDisplayedFields().size() 
		for (FieldView fv : inheritedFields) {
			val newFieldView = getFieldViewCopy(id, fv, fv.getScopes(), index, ctv)
			if(newFieldView instanceof ComplexFieldView){
				GUICompoundViewCreator.addRecursiveTypeView(newFieldView.view as ComplexTypeView,this.gui)
			}
			ctv.getDisplayedFields().add(newFieldView) 
			index++ 
		}
	}
	
	/** 
	 * Modifies all FieldViews which are inherited and already known in this GUICompoundView
	 * @param currentCFV The modified FieldView
	 * @param inheritedCFV The inherited FieldView
	 */
	def private void addInheritedFieldViewCopy(ComplexFieldView currentCFV, ComplexFieldView inheritedCFV) {
		//Get TypeViews under the Field Views
		var ComplexTypeView currentCTV=(currentCFV.getView() as ComplexTypeView) 
		var ComplexTypeView inheritedCTV=(inheritedCFV.getView() as ComplexTypeView) 
		
		
		if (inheritedCTV.getData() === null) {
			println('''[ERROR] No Data Set in ComplexTypeView «inheritedCTV.getName()» in «inheritedCTV.getgModel»''') 
			return;
		}
		
		//Create ViewImplementation for the currentCTV which implements the inheritedCTV
		var ViewImplementation vi=new ViewImplementation(inheritedCFV.getgModel,inheritedCTV) 
		currentCTV.getInterfaces().add(vi) 
		addInheritetFields(currentCTV, inheritedCTV) 
	}
	
	/** 
	 * Modifies all FieldViews which are inherited and already known in this GUICompoundView
	 * @param g The inherited GUI
	 * @param currentCFV The modified FieldView
	 * @param inheritedCFV The inherited FieldView from a sub component
	 */
	def private void addInheritedFieldViewCopy(String id, ComplexFieldView currentCFV, ComplexFieldView inheritedCFV) {
		//Get TypeViews under the Field Views
		var ComplexTypeView currentCTV=(currentCFV.getView() as ComplexTypeView) 
		var ComplexTypeView inheritedCTV=(inheritedCFV.getView() as ComplexTypeView) 
		if (inheritedCTV.getData() === null) {
			println('''[ERRRO] No Data Set in ComplexTypeView «inheritedCTV.getName()» in «inheritedCTV.getgModel()»''') 
			return;
		}
		
		
		//Create ViewImplementation for the currentCTV which implements the inheritedCTV
		var ViewImplementation vi=new ViewImplementation(inheritedCFV.getgModel,inheritedCTV) 
		currentCTV.getInterfaces().add(vi) 
		addInheritetFields(id, currentCTV, inheritedCTV) 
	}
	
	
	def private FieldView getFieldViewCopy(FieldView fv, List<MovableContainer> scopes, int i, TypeView parent) {
		if (fv instanceof ComplexFieldView) {
			var ComplexFieldView cfv=(fv as ComplexFieldView) 
			var ComplexTypeView sourceCTV=(cfv.getView() as ComplexTypeView) 
			var ComplexFieldView c=new ComplexFieldView(this.gui) 
			c.setData(cfv.getData()) 
			c.setField(cfv.getField()) 
			c.setScopes(scopes) 
			c.setId('''«parent.getId()»x«i»''') 
			c.setInherited(true)
			c.setList(cfv.isList()) 
			c.setName(cfv.getName()) 
			c.setTypeName(sourceCTV.getTypeName()) 
			var ComplexTypeView ctv=new ComplexTypeView(sourceCTV.getType(),Collections.EMPTY_SET,this.gui) 
			var ViewImplementation vi=new ViewImplementation(cfv.getgModel,sourceCTV) 
			//TODO add only distinct, if multiple equal GUISIBs are used which uses the same data
			ctv.getInterfaces().add(vi) 
			ctv.setData(sourceCTV.getData()) 
			ctv.setId('''«parent.getId()»x«i»''') 
			ctv.setList(sourceCTV.isList()) 
			ctv.setName(sourceCTV.getName()) 
			//----->
			//ctv.interfaces.addAll(sourceCTV.interfaces)
			//<------
			addInheritetFields(ctv, sourceCTV) 
			c.setView(ctv) 
			return c 
		}
		return fv 
	}
	
	def private FieldView getFieldViewCopy(String id, FieldView fv, List<MovableContainer> scopes,  int i, TypeView parent) {
		if (fv instanceof ComplexFieldView) {
			var ComplexFieldView cfv=(fv as ComplexFieldView) 
			var ComplexTypeView sourceCTV=(cfv.getView() as ComplexTypeView) 
			var ComplexFieldView c=new ComplexFieldView(this.gui) 
			c.setData(cfv.getData()) 
			c.setField(cfv.getField()) 
			c.setScopes(scopes) 
			c.setId('''«parent.getId()»x«i»''') 
			c.setInherited(true) 
			c.setList(cfv.isList()) 
			c.setName(cfv.getName()) 
			c.setTypeName(sourceCTV.getTypeName()) 
			val ComplexTypeView ctv=new ComplexTypeView(sourceCTV.getType(),Collections.EMPTY_SET,this.gui) 
			val ViewImplementation vi=new ViewImplementation(fv.getgModel,sourceCTV) 
			//TODO add only distinct, if multiple equal GUISIBs are used which uses the same data
			ctv.getInterfaces().add(vi) 
			ctv.setData(sourceCTV.getData()) 
			ctv.setId('''«parent.getId()»x«i»''') 
			ctv.setList(sourceCTV.isList()) 
			ctv.setName(sourceCTV.getName()) 
			addInheritetFields(id, ctv, sourceCTV) 
			c.setView(ctv) 
			return c 
		}
		return fv 
	}

	
	/** 
	 * Returns the constructed GUI compound view which holds all seletcive type views
	 * for the current GUI model.
	 * @return
	 */
	def GUICompoundView collect(Map<ModelElement,ComplexTypeView> typeViewsToExtend) {
		
		if (guiCV !== null) {
			return guiCV 
		}
		// root for the tree of the selective type views
		val GUICompoundView view=new GUICompoundView(gui) 
		
		debug("Create for GUI: " + gui?.title)
		
		//this.guiCV=view 
		view.setName(getGUIViewName(view)) 
		view.setId('''«0»''')
		this.guiCV = view
		genctx.addGuiCV(view)
		//collect top level variables
		debug("Collect top level variables for GUI: " + gui?.title)
		
		val variables = gui.find(Variable)
			.filter[getIncoming(ComplexAttributeConnector).isEmpty && getIncoming(ComplexListAttributeConnector).isEmpty]
			.toList
		
		for (i : 0 ..< variables.size) {
			debug("Create TypeView for top level variable: " + variables.get(i)?.name)
			val tv = createTypeView(null, variables.get(i), "", newLinkedList, i+1, null, typeViewsToExtend) 
			if (tv !== null) {
				view.compounds.add(tv)
			}
		}
		
		view.collectRawExpressions
		
		debug("Set Import Variables")
		debug(" > " + importVariables.map[name].join("\n[Selective]  > "))
		view.setImportViews(importVariables) 
		
		view.setPairs(pairs)
		//extend views dependent on sub-component views with interfaces
		extendViewsForEmbeddedSubComponent(view.compounds.filter(ComplexTypeView))
		
		
		//extend the views with additional fields dependent on the interfaces
		view.getAllTypeViews().filter(ComplexTypeView).forEach[n |
			debug("TypeView: " + n)
				n.getInterfaces().forEach[e | this.addInheritetFields(n.getData().getId(), n, e)
			]
		]
		//GUICompoundViewCreator.extendViewsForRecursiveTypeViews(view)
	
		//Add Select-Choice Data implementations
		addSelectChoiceDataFields() 
		//Add Table-Choice Data implementations
		addTableChoiceDataFields(view)
		
		
		
//		if(this.guis.findFirst[getGui.id.equals(view.gui.id)]==null) {
//			this.guis.add(view)			
//		}
		return view 
	}
	
	static def void extendViewsForRecursiveTypeViews(GUICompoundView gcv) {
		//extend all complex typeviews to determine recursive data usage
		gcv.getAllTypeViews().filter(ComplexTypeView).forEach[addRecursiveTypeView(gcv.cgui)]
	}
	
	def void extendViewsForEmbeddedSubComponent(Iterable<ComplexTypeView> views) {
		views.forEach[t|{
			if(t.data==null) {
				throw new IllegalStateException
			}
			if(t.data instanceof ComplexVariable){
				addEmbeddedGUIImports(t.data as ComplexVariable,t)
				//debug(t.interfaces.size)
			}
			if(t.data instanceof ComplexAttribute){
				addEmbeddedGUIImports(t.data as ComplexAttribute,t)
				//debug(t.interfaces.size)
			}
			if(t.data instanceof ExtensionAttribute){
				addEmbeddedGUIImports(t.data as ExtensionAttribute,t)
			}
			t.displayedFields.filter(ComplexFieldView).map[view].filter(ComplexTypeView).extendViewsForEmbeddedSubComponent
		}]
	}
	
	def private void collectRawExpressions(GUICompoundView cv) {
		debug("Collect raw expressions for GUI: " + cv.gui?.title)
		for (entry : this.ee.expressions.entrySet.filter[!it.value]) {
			extendCompoundView(cv, entry.getKey()) 
		}
	}
	
	def private void extendCompoundView(GUICompoundView gcv, Expression xp) {
		debug(" > Extend CompoundView for expression: " + xp?.expression)
		val varName = getVarName(xp.expression)
		debug(" > varName: " + varName)
		if (varName === null) return;
		//Extend TypeViews in the global scope
		for (TypeView t : gcv.compounds) {
			if (t.name == varName && t.scopes.isVariableInSameScope(xp.scope)) {
				debug(" > TypeView for varName: " + t)
				if (t instanceof ComplexTypeView) {
					debug(" > extend ComplexTypeView")
					val ctv = t as ComplexTypeView
					if (xp.expression != varName) {
						extendFieldView(ctv, xp.expression.substring(varName.length + 1), xp.scope) 
					}
				}
				return;
			}
		}
		//Extend TypeViews in Iteration scopes
		//For ComplexVariables
		debug(" > Extend TypeViews in Iteration scopes for ComplexVariables and expression: " + xp?.expression)
		val complexItVarEntries = this.iterationVariables.entrySet
			.filter[key instanceof ComplexVariable]
			.filter[(key as ComplexVariable).name == varName || isListAttribute(varName)]
			.filter[value.scopes.isVariableInSameScope(xp.scope)]
		
		var varNameFound = false;
		for (complexItVarEntry : complexItVarEntries) {
			debug(" > Complex variable: " + (complexItVarEntry.key as ComplexVariable).name)
			if (!varNameFound || (complexItVarEntry.key as ComplexVariable).name == varName) {
				val ctv = gcv.allTypeViews.findFirst[it == complexItVarEntry.value]
				debug(" > ComplexTypeView for this variable: " + ctv)
				if (xp.expression != varName) {
					extendFieldView(ctv as ComplexTypeView, xp.expression.substring(varName.length + 1), xp.scope)
				}
				varNameFound = true
			}
		}
//		this.iterationVariables.entrySet().stream().filter([n | n.getKey() instanceof ComplexVariable]).filter([n | (((n.getKey() as ComplexVariable)).getName().equals(varName) || isListAttribute(varName)) && isVariableInSameScope(n.getValue().getScopes(), xp.getScope())]).forEach([n | {
//			extendFieldView(n.getValue(), xp.getExpression().substring(varName.length() + 1), xp.getScope()) 
//			return;
//		}]) 
		//ComplexListAttributes
		//Find List Variable
		debug(" > Extend TypeViews for List Variables and expression: " + xp?.expression)
		val complexListVars = this.gui.dataContexts.flatMap[variables].filter(ComplexVariable).filter[isList] 
		for (complexListVar : complexListVars) {
			debug(" > Complex list variable: " + complexListVar?.name)
			//Get ComplexType View for List Variable
			val ctv = this.pairs.get(complexListVar) as ComplexTypeView
			//Check Scope
			if (ctv === null) {
				debug("[WARN] Type View is null for variable " + complexListVar?.name + " (id: " + complexListVar?.id + ") in " + gcv)
			}
			if (ctv !== null && ctv.scopes.isVariableInSameScope(xp.scope)) {
				//Check matching ComplexListAttribute
				for (complexListAttr : complexListVar.attributes.filter(ComplexListAttribute)) {
					debug(" > Complex list attribute: " + complexListAttr.attributeName)
					val varNameMatches = switch complexListAttr.attributeName {
						case CURRENT: varName == "current"
						case FIRST: varName == "first"
						case LAST: varName== "last"
						default: false
					}
					debug(" > Matches varName: " + varNameMatches)
					if (varNameMatches) {
						extendFieldView(ctv, xp.expression.substring(varName.length + 1), xp.scope) 
					}
				}
			}
		}
	}
	def private boolean isListAttribute(String varName) {
		return varName.equals("current") || varName.equals("last") || varName.equals("first") 
	}
	def private boolean isVariableInSameScope(List<MovableContainer> scopes, MovableContainer scope) {
		if (scopes === null) {//			System.out.println("Global scope variable");
			
		}
		//The TypeView is in the global scope and is in every scope
		if (scopes.isEmpty()) return true 
		//The TypeView is in an inner scope
		return scopes.stream().filter([n | isInTheSameScope((n as MovableContainer), scope, false)]).findAny().isPresent() 
	}
	def private boolean isInTheSameScope(MovableContainer current, MovableContainer scope, boolean hasInnerScope_finalParam_) {
		var  hasInnerScope=hasInnerScope_finalParam_ 
		//Is in the same scope
		//Is in the global scope, if has not been in any inner scope
		if (scope !== null) {
			if (current.getId().equals(scope.getId())) return true 
		}
		if (current.getContainer() !== null) {
			if (current.getContainer() instanceof MovableContainer) {
				//Is in any scope?
				if (current instanceof Table || (!current.getIncoming(Iteration).isEmpty())) {
					return isInTheSameScope((current.getContainer() as MovableContainer), scope, true) 
				}
				//Scope not determined at the moment
				return isInTheSameScope((current.getContainer() as MovableContainer), scope, false) 
			}
		}
		return !hasInnerScope 
	}
	def private void extendFieldView(ComplexTypeView tv, String s, MovableContainer scope) {
		if (s === null) return;
		
		debug(" > Extend FieldView for expression: " + s)
		
		val varName = getVarName(s)
		debug(" > varName: " + varName)
		if (varName === null) return;
		
		val suffix = switch it:varName {
			case it == s: s
			case it.length >= s.length: null
			default: s.substring(varName.length + 1)
		}
		debug(" > suffix: " + suffix)
		
		for (FieldView t : tv.displayedFields) {
			if (t.name == varName && t.scopes.isVariableInSameScope(scope)) {
				debug(" > FieldView for varName: " + t)
				if (t instanceof ComplexFieldView) {
					debug(" > extend ComplexFieldView")
					extendFieldView(t.view as ComplexTypeView, suffix, scope) 
				}
				return;
			}
		}
		if (varName == "last" || varName == "first") {
			extendFieldView(tv, suffix, scope) 
			return;
		}
		//Variable not found -> Continue Search in the Datamodel
		debug(" > Variable not found, search in Data model: " + varName)
		if (tv instanceof ComplexTypeView) {
			val ctv = tv as ComplexTypeView
			val type = ctv.type
			val index = ctv.displayedFields.size + 1
			for (info.scce.dime.data.data.Attribute attr : type.inheritedAttributes) {
				if (attr.name == varName && tv.scopes.isVariableInSameScope(scope)) {
					debug(" > Attribute for varName: " + attr)
					//ensure that ctv does not already contain a field for attr
					if (!ctv.displayedFields.exists[name == attr.name]) {
						debug(" > Create FieldView for attribute: " + attr.name)
						ctv.displayedFields.add(createFieldView(attr, suffix, ctv.scopes, index, ctv))						
					}
					return;
				}
			}
		}
		println('''[WARN] Attribute {{«varName»}} not found in Type:«tv.getName()» in GUI:«this.gui.getTitle()»''')
	}
	
	def private String getVarName(String s) {
		if (s.nullOrEmpty)
			null
		else switch index : s.indexOf(".") {
			case index > -1: s.substring(0, index)
			default: s
		}
	}
	
	def private PrimitiveType getPrimitveType(info.scce.dime.gui.gui.PrimitiveType pt) {
		switch pt {
			case BOOLEAN: PrimitiveType.BOOLEAN
			case FILE: PrimitiveType.FILE
			case INTEGER: PrimitiveType.INTEGER
			case REAL: PrimitiveType.REAL
			case TIMESTAMP: PrimitiveType.TIMESTAMP
			default: PrimitiveType.TEXT
		}
	}
	
	def private addSelectChoiceDataFields() {
		//Collect Complex Variables with Form Load
		val choiceTargets = pairs.elements.filter(Node)
			.filter[getOutgoing(FormSubmit).exists[targetElement instanceof Select]]
		+ pairs.elements.filter(Node)
			.filter[getOutgoing(FormLoadSubmit).exists[targetElement instanceof Select]]
		
		for (Node node : choiceTargets) {
			val cs = node.outgoing
				.filter[it instanceof FormLoadSubmit || it instanceof FormSubmit]
				.map[targetElement].head
				//no static choices provided
				val sourceListVariable = cs.getIncoming(ChoiceData).get(0).sourceElement
				val sourceChoiceCTV =
					if (sourceListVariable instanceof ComplexVariable) {
						pairs.get(sourceListVariable.getOutgoing(ComplexListAttributeConnector).head.targetElement) 
					} else {
						pairs.get(sourceListVariable) 
					}
				if (sourceListVariable != node) {
					addInheritedFields(node, sourceChoiceCTV) 
				}
		}
	}
	/** 
	 * @param choiceTargetCTV The CTV which will be implemented by choiceSourceCTV
	 * @param choiceSourceCTV The CTV which will implement choiceTargetCTV
	 * @param vi 
	 * @param gui
	 */
	def private void addInheritedFields(String id, ComplexTypeView choiceTargetCTV, ComplexTypeView choiceSourceCTV, ViewImplementation vi, GUICompoundView guiCV) {
		//Do not implement the same twice
//		if (!choiceSourceCTV.getInterfaces().stream().filter([n | n.getParent().getData().getId().equals(vi.getParent().getData().getId())]).findAny().isPresent()) {
//			choiceSourceCTV.getInterfaces().add(vi) 
			//Add FieldViews that ARE already contained and has to be extended
			choiceSourceCTV.getDisplayedFields().stream().filter([n | n instanceof ComplexFieldView]).forEach([n | {
				choiceTargetCTV.getDisplayedFields().stream().filter([e | e.getName().equals(n.getName())]).forEach([e | extendInheritedField(id, e, n, guiCV)]) 
			}]) 
			//Add FieldViews that are not already contained
			var List<FieldView> notContained = choiceTargetCTV.displayedFields.filter[n|choiceSourceCTV.displayedFields.map[name].findFirst[equals(n.name)]==null].toList
			var int index=1 + choiceSourceCTV.getDisplayedFields().size() 
			for (FieldView fv : notContained) {
				choiceSourceCTV.getDisplayedFields().add(getFieldViewCopy(fv, choiceTargetCTV.getScopes(), index, choiceSourceCTV)) 
				index++ 
			}
//		}
	}
	/** 
	 * @param targetFV will be implemented by sourceFV
	 * @param sourceFV will implement taregtFV
	 */
	def private void extendInheritedField(String id, FieldView targetFV, FieldView sourceFV, GUICompoundView guiCV) {
		if (targetFV instanceof ComplexFieldView && sourceFV instanceof ComplexFieldView) {
			var ViewImplementation vi=new ViewImplementation(targetFV.getgModel,(((targetFV as ComplexFieldView)).getView() as ComplexTypeView)) 
			addInheritedFields(id, (((targetFV as ComplexFieldView)).getView() as ComplexTypeView), (((sourceFV as ComplexFieldView)).getView() as ComplexTypeView), vi, guiCV) 
		}
	}
	/** 
	 * @param target the variable which will be implememtning
	 * @param p The typeview which has to be extended
	 */
	def private void addInheritedFields(Node choiceTargetNode, Parent choiceSourceCTV) {
		val Parent unrealChoiceTargetCTV=this.pairs.get(choiceTargetNode)
		var realyChoiceTargetCTV = unrealChoiceTargetCTV
		if(unrealChoiceTargetCTV instanceof ComplexFieldView){
			realyChoiceTargetCTV = unrealChoiceTargetCTV.view
		}
//		var realyChoiceTargetCTV = this.guiCV.allTypeViews.findFirst[equals(unrealChoiceTargetCTV)]
//		if(unrealChoiceTargetCTV instanceof ComplexFieldView){
//			realyChoiceTargetCTV = this.guiCV.allTypeViews.findFirst[equals(unrealChoiceTargetCTV.view)]
//		}
		//Add Interface
//		if (realyChoiceTargetCTV instanceof ComplexTypeView && choiceSourceCTV instanceof ComplexTypeView) {
//			vi.setParent((realyChoiceTargetCTV as ComplexTypeView)) 
//			addInheritedFields(choiceTargetNode.getId(), (realyChoiceTargetCTV as ComplexTypeView), (choiceSourceCTV as ComplexTypeView), vi, this.guiCV) 
//		}
		if (choiceSourceCTV instanceof ComplexTypeView) {
			val ViewImplementation vi=new ViewImplementation(this.gui,realyChoiceTargetCTV as ComplexTypeView) 
			addInheritedFields(choiceTargetNode.getId(),realyChoiceTargetCTV as ComplexTypeView, (choiceSourceCTV as ComplexTypeView), vi, this.guiCV) 
		}
		if (choiceSourceCTV instanceof ComplexFieldView) {
			val ViewImplementation vi=new ViewImplementation(this.gui,realyChoiceTargetCTV as ComplexTypeView) 
			addInheritedFields(choiceTargetNode.getId(), realyChoiceTargetCTV as ComplexTypeView, (((choiceSourceCTV as ComplexFieldView)).getView() as ComplexTypeView), vi, this.guiCV) 
		}
//		if (realyChoiceTargetCTV instanceof ComplexTypeView && choiceSourceCTV instanceof ComplexFieldView) {
//			vi.setParent((realyChoiceTargetCTV as ComplexTypeView)) 
//			addInheritedFields(choiceTargetNode.getId(), (realyChoiceTargetCTV as ComplexTypeView), (((choiceSourceCTV as ComplexFieldView)).getView() as ComplexTypeView), vi, this.guiCV) 
//		}
	}
	
	
	def private void addTableChoiceDataFields(GUICompoundView gcv) {
		//collect all elements which are table source for a table which has a data target for selected entries
		val choicePairsthis = pairs.elements.filter(ComplexVariable)
			.filter[isList]
			.filter[!getOutgoing(TableLoad).isEmpty]
			.filter[getOutgoing(TableLoad).head.targetElement.choices != TableChoice.NONE]
		+ pairs.elements.filter(ComplexAttribute)
			.filter[pairs.get(it).isList]
			.filter[!getOutgoing(TableLoad).isEmpty]
			.filter[getOutgoing(TableLoad).head.targetElement.choices != TableChoice.NONE]
			 
		for (tableSourceVar : choicePairsthis) {
			//source variable for a table
			for (Table cs : tableSourceVar.getSuccessors(Table)) {
				//the connected table
				var Node dataTarget=cs.getIncoming(DataTarget).get(0).getSourceElement() 
				//ensure that the dataTarget is not the data source
				if (!dataTarget.id.equals(tableSourceVar.id)) {
					//fetch the type view for the data source
					val dataSourceTypeView = this.pairs.get(tableSourceVar)
					val realTypeView = gcv.allTypeViews.findFirst[equals(dataSourceTypeView)]
					dataTarget.addInheritedFields(realTypeView) 
				}
			}
		}
	}
	
	
	
	def private List<ViewImplementation> getEmbeddedVariable(ComplexInputPort ccip,ComplexTypeView typeViewToExtend) {
		val sib = ccip.container
		if (!(sib instanceof GUISIB || sib instanceof ProcessSIB || sib instanceof EventListener)) {
			return Collections.emptyList 
		}
		val List<ViewImplementation> vis=new LinkedList<ViewImplementation>() 
		if (sib instanceof GUISIB) {
			val GUI g=sib.gui
			vis.addAll(ccip.name.viewImplementationForGUIInput(g,typeViewToExtend))
			if(sib instanceof DispatchedGUISIB){
				for(subGui : g.subGUIs) {
					vis.addAll(ccip.name.viewImplementationForGUIInput(subGui,typeViewToExtend, /*checkType*/ true))
				}
			}
			return vis
			
			
		} else if (sib instanceof EventListener) {
			if(sib.container instanceof GUISIB) {
				val guiSib = sib.container as GUISIB
				vis.addAll(ccip.name.viewImplementationForEventInput(guiSib.gui,typeViewToExtend))
				if(guiSib instanceof DispatchedGUISIB){
					for(subGui : guiSib.gui.subGUIs) {
						vis.addAll(ccip.name.viewImplementationForEventInput(subGui,typeViewToExtend))
					}
				}
				
			}
			return vis 
		}
		return vis 
	}

	def private void addEmbeddedGUIImports(ComplexVariable ccv, ComplexTypeView tv) {
		ccv.getOutgoing(ComplexRead).map[targetElement]
		.filter[n | (n.getContainer() instanceof GUISIB || n.getContainer() instanceof ProcessSIB || n.getContainer() instanceof EventListener)]
		.forEach[n | {
			val interfaces = getEmbeddedVariable(n,tv)
			tv.getInterfaces().addAll(interfaces)
			if(!interfaces.empty)
			{
				GUICompoundViewCreator.addRecursiveTypeView(tv,this.gui)
			}
		}]
		//check if data is extracted list variable with current, first or last element
		if(ccv.isIsList) {
			//add view implementations for all list attributes present to type view for the list
			ccv.getOutgoing(ComplexListAttributeConnector).map[targetElement].forEach[cv|addEmbeddedGUIImports(cv,tv)]
		}
	}
	
	def private void addEmbeddedGUIImports(ComplexAttribute ccv, ComplexTypeView tv) {
		ccv.getOutgoing(ComplexRead).stream().map([n | n.getTargetElement()]).forEach([n |{
			val interfaces = getEmbeddedVariable(n,tv)
			tv.getInterfaces().addAll(interfaces)
			if(!interfaces.empty){
				GUICompoundViewCreator.addRecursiveTypeView(tv,this.gui)
			}
		}])
	}
	
	def private void addEmbeddedGUIImports(ExtensionAttribute ccv, ComplexTypeView tv) {
		ccv.getOutgoing(ComplexRead).stream().map([n | n.getTargetElement()]).forEach([n |{
			val interfaces = getEmbeddedVariable(n,tv)
			tv.getInterfaces().addAll(interfaces)
			if(!interfaces.empty){
				GUICompoundViewCreator.addRecursiveTypeView(tv,this.gui)
			}
		}])
	}
	
	
	def private TypeView createTypeView(ComplexTypeView listTypeView, Variable v, String access, List<MovableContainer> scope, int i, FieldView parent,Map<ModelElement,ComplexTypeView> typeViewsToExtend) {
		var String ac=if (access.isEmpty()) v.getName() else '''«access».«v.getName()»'''  
		val isUsed = isVariableUsed(v, if (access.isEmpty()) ac else access )
		debug("Create TypeView for: " + ac + " isUsed: " + isUsed)
		if (!isUsed)
			return null
		
		if (v instanceof ComplexVariable) {
			val cv = v as ComplexVariable 
			var tv = listTypeView
			
			//if the current variable v is a list attribute variable (current, first, last)
			//the typeview already exists and has to be updated
			if (listTypeView === null) {
				debug("Create TypeView for ComplexVariable with data type: " + cv.dataType.originalType)
				tv = new ComplexTypeView(cv.dataType.originalType, Collections.EMPTY_SET, this.gui) => [
					id = if (parent !== null) '''«parent.id»x«i»''' else '''x«i»'''
					name = cv.name
					list = cv.isList
					data = cv
					scopes = <MovableContainer> newLinkedList => [
						//Is in a new scope?
						addAll(cv.getSuccessors(Table))
						addAll(cv.getOutgoing(Iteration).map[targetElement].toList)
						if (isEmpty) addAll(scope)
					]
				]
			}
			
			/*
			 * Connect the input variable TypeView on the upper layer at the beginning
			 */
			if (typeViewsToExtend.containsKey(v)) {
				typeViewsToExtend.get(v).interfaces.add(new ViewImplementation(cv.rootElement,tv))
			}
			
			addDisplayedFields(tv, cv, ac, tv.scopes, i,typeViewsToExtend)
//				System.out.println('''«cv.getRootElement().getTitle()»: «cv.getName()»''') 
//				addEmbeddedGUIImports(ccv, tv)
			//FIXME Don't sure if this had be done here -> is done for all CTVs in collect
			//tv.interfaces.forEach[n|this.addInheritetFields(data.getId(), finalTv, n)]
			extendParis(cv, tv)
			//Is list?
			if (cv.isList) {
				if (!cv.getOutgoing(ComplexListAttributeConnector).isEmpty) {
					for (ComplexListAttributeConnector clac : cv.getOutgoing(ComplexListAttributeConnector)) {
						var ComplexVariable listIteratorVar =clac.getTargetElement() 
						var String itListAttrAccess="" 
						if (clac.getAttributeName() !== ComplexListAttributeName.CURRENT) itListAttrAccess=ac 
						var ComplexTypeView itCtv=(tv.createTypeView(listIteratorVar, itListAttrAccess, tv.scopes, i, parent,typeViewsToExtend) as ComplexTypeView) 
						if (itCtv != null) {
							//merge list variable type view with list type view
							//tv.merge(itCtv)
							
							//addInheritetFields(tv, itCtv)
							iterationVariables.put(listIteratorVar, itCtv) 
						}
					}
					
					
//						var ComplexVariable target=ccv.getOutgoing(ComplexListAttributeConnector).get(0).getTargetElement() 
//						var String listAttrAccess="" 
//						if (ccv.getOutgoing(ComplexListAttributeConnector).get(0).getAttributeName() !== ComplexListAttributeName.CURRENT) listAttrAccess=ac 
//						var ComplexTypeView ctv=(createTypeView(target, listAttrAccess, currentScopes, i, parent) as ComplexTypeView) 
//						var iterationCTV = new ComplexTypeView(ctv.guardContainers,ctv.getgModel)
//						if (ctv !== null) {
//							iterationCTV.setName(ccv.getName()) 
//							iterationCTV.setTypeName(this.gui.getTitle() + cv.getDataType().getName()) 
//							iterationCTV.setList(true) 
//							iterationCTV.displayedFields = ctv.displayedFields
//							iterationCTV.data = ctv.data
//							iterationCTV.id = ctv.id
//							iterationCTV.typeName = ctv.typeName
//							iterationCTV.scopes = ctv.scopes
//							iterationCTV.list = true
//							iterationCTV.type = ctv.type
//							//extendParis(target as Node, ctv) 
//							addEmbeddedGUIImports(ccv, iterationCTV) 
//							iterationVariables.put(target, iterationCTV) 
//						}
//						for (ComplexListAttributeConnector clac : ccv.getOutgoing(ComplexListAttributeConnector).subList(0, ccv.getOutgoing(ComplexListAttributeConnector).size())) {
//							var ComplexVariable itTarget=clac.getTargetElement() 
//							var String itListAttrAccess="" 
//							if (clac.getAttributeName() !== ComplexListAttributeName.CURRENT) itListAttrAccess=ac 
//							var ComplexTypeView itCtv=(createTypeView(itTarget, itListAttrAccess, currentScopes, i, parent) as ComplexTypeView) 
//							if (itCtv != null) {
//								itCtv.setList(true) 
//								extendParis((itTarget as Node), iterationCTV) 
//								addEmbeddedGUIImports(itTarget, iterationCTV) 
//								iterationVariables.put(itTarget, iterationCTV) 
//								
//							}
//						}
//						if(ctv==null) {
//							return tv;
//						}
//						return iterationCTV 
				}
				iterationVariables.put(cv, tv) 
			}
			return tv 
		} else {
			var PrimitiveVariable cpv=(v as PrimitiveVariable) 
			var PrimitiveTypeView ptv=new PrimitiveTypeView() 
			if (parent !== null) {
				ptv.setId('''«parent.getId()»x«i»''') 
			} else {
				ptv.setId('''x«i»''') 
			}
			ptv.setList(v.isIsList()) 
			ptv.setName(v.getName()) 
			extendParis((cpv as Node), ptv) 
			//Is in a new scope?
			var List<MovableContainer> currentScopes=new LinkedList<MovableContainer>() 
			currentScopes.addAll(cpv.getSuccessors(Table)) 
			currentScopes.addAll(cpv.getOutgoing(Iteration).stream().map([n | (n.getTargetElement() as MovableContainer)]).collect(Collectors.toList())) 
			if (currentScopes.isEmpty()) {
				currentScopes.addAll(scope) 
			}
			ptv.setScopes(currentScopes) 
			ptv.setData((cpv as Node)) 
			ptv.setPrimitiveType(getPrimitveType(cpv.getDataType())) 
			return ptv 
		}
	}
	
	/**
	 * Merge the complex type view listVariableTypeView into listTypeView.
	 * Used for iterator variables as well as last and first list variables.
	 */
	def void merge(ComplexTypeView tv1, ComplexTypeView tv2) {
		val unknownFields = tv1.displayedFields.filter[ field |
			tv2.displayedFields.exists[it.name == field.name]
		]
		val tv2Fields = tv2.displayedFields => [ addAll(unknownFields.toList) ]
		
		//merge contained displayed fields
		for (field : tv2Fields.filter(ComplexFieldView)) {
			for (knownField : tv1.displayedFields.filter(ComplexFieldView).filter[it.name == field.name]) {
				//merge type views
				merge(field.view as ComplexTypeView, knownField.view as ComplexTypeView)
			}
		}
	}
	
	def private ComplexTypeView addDisplayedFields(ComplexTypeView tv, ComplexVariable ccv, String ac, List<MovableContainer> scopes, int i_finalParam_,Map<ModelElement,ComplexTypeView> typeViewsToExtend) {
		debug("Add displayed fields: " + ccv.name)
		var i = i_finalParam_ 
		//Inner Attributes
		for (attr : ccv.attributes) {
			debug("Add Field View for inner attribute: " + attr)
			tv.addDisplayedField(
				createFieldView(attr, ac, scopes, i++, tv,typeViewsToExtend)
			)
		}
		//External Attributes
		for (ComplexAttributeConnector cac : ccv.getOutgoing(ComplexAttributeConnector)) {
			debug("Add Field View for outgoing attribute connector of " + ccv.name)
			tv.addDisplayedField(
				createFieldView(cac, ac, scopes, i++, tv, typeViewsToExtend)
			)
		}
		return tv 
	}
	
	def private addDisplayedField(ComplexTypeView tv, FieldView fv) {
		if (fv !== null) {
			val alreadyKnownFieldView = tv.displayedFields.findFirst[name == fv.name]
			debug("Field View " + fv.name + " is already known? " + (alreadyKnownFieldView !== null))
			if (alreadyKnownFieldView !== null) {
				if(fv instanceof ComplexFieldView) {
					debug(" => Merge type views")
					//the type views under the field view have to be merged
					merge((alreadyKnownFieldView as ComplexFieldView).view as ComplexTypeView, fv.view as ComplexTypeView)
				}
			} else {
				debug(" => Add as displayed field: " + fv.name)
				tv.displayedFields.add(fv)
			}
		}
	}
	
	def private FieldView createFieldView(ComplexAttributeConnector a, String access, List<MovableContainer> scopes, int i, TypeView parent, Map<ModelElement,ComplexTypeView> typeViewsToExtend) {
		var String ac='''«access».«a.getAssociationName()»''' 
		var ComplexVariable ccv=a.getTargetElement() 
		if (isVariableUsed(ccv, ac)) {
			var ComplexFieldView fv=new ComplexFieldView(this.gui) 
			fv.setId('''«parent.getId()»x«i»''') 
			//Search for Attribute in Datamodel
			var ComplexVariable source=a.getSourceElement() 
			var ComplexVariable cv=(source as ComplexVariable) 
			var Type type=cv.getDataType() 
			val attr=DataExtension.getInstance.getInheritedAttributes(type).filter[name == a.associationName].head 

			//Is in a new scope?
			var List<MovableContainer> currentScopes=new LinkedList<MovableContainer>() 
			currentScopes.addAll(ccv.getSuccessors(Table)) 
			currentScopes.addAll(ccv.getOutgoing(Iteration).stream().map([n | (n.getTargetElement() as MovableContainer)]).collect(Collectors.toList())) 
			if (currentScopes.isEmpty()) {
				currentScopes.addAll(scopes) 
			}
			fv.setScopes(currentScopes) 
			fv.setField(attr) 
			fv.setList(ccv.isIsList()) 
			fv.setName(ccv.getName()) 
			fv.setData((ccv as Node)) 
			//extendParis((Node)ccv, fv);
			var String typeName = null
			if(attr instanceof info.scce.dime.data.data.ComplexAttribute) {
				typeName = attr.dataType.name
			}
			if(attr instanceof info.scce.dime.data.data.ExtensionAttribute) {
				typeName = attr.dataType
			}
			fv.setTypeName(this.gui.getTitle() + typeName) 
			var TypeView tv=createTypeView(null,ccv, access, currentScopes, i, fv,typeViewsToExtend) 
			if (tv === null) return null 
			fv.setView(tv) 
			return fv 
		}
		return null 
	}
	def private FieldView createFieldView(info.scce.dime.data.data.Attribute a, String s, List<MovableContainer> scopes, int i, TypeView parent) {
		if (a instanceof info.scce.dime.data.data.PrimitiveAttribute) {
			var PrimitiveFieldView fv=new PrimitiveFieldView(((a as info.scce.dime.data.data.PrimitiveAttribute)).getDataType()) 
			fv.setId('''«parent.getId()»x«i»''') 
			fv.setField(a) 
			fv.setList(a.isIsList()) 
			fv.setScopes(scopes) 
			fv.setData(a) 
			fv.setName(a.getName()) 
			fv.setPrimitiveType(((a as info.scce.dime.data.data.PrimitiveAttribute)).getDataType()) 
			return fv 
		} else if (a instanceof info.scce.dime.data.data.ComplexAttribute) {
			var ComplexFieldView fv=new ComplexFieldView(this.gui) 
			fv.setId('''«parent.getId()»x«i»''') 
			fv.setField(a) 
			fv.setData(a) 
			fv.setScopes(scopes) 
			fv.setList(a.isIsList()) 
			fv.setName(a.getName()) 
			val ca=a
			fv.setTypeName(this.gui.getTitle() + ca.getDataType().getName()) 
			var TypeView tv=createTypeView(ca, ca.getDataType(), s, a.isIsList(), scopes, i, fv) 
			if (tv === null) return null 
			fv.setView(tv) 
			return fv 
		} else if (a instanceof info.scce.dime.data.data.ExtensionAttribute) {
			val complexType = _dataExtension.getComplexExtensionAttributeType(a)
			if(complexType !== null) {
				var ComplexFieldView fv=new ComplexFieldView(this.gui) 
				fv.setId('''«parent.getId()»x«i»''') 
				fv.setField(a) 
				fv.setData(a) 
				fv.setScopes(scopes) 
				//extendParis(a, fv);
				fv.setList(a.isIsList()) 
				fv.setName(a.getName()) 
				val ca=a
				fv.setTypeName(this.gui.getTitle() + complexType.getName()) 
				var TypeView tv=createTypeView(ca, complexType, s, a.isIsList(), scopes, i, fv) 
				if (tv === null) return null 
				fv.setView(tv) 
				return fv 
			} else {
				val primitiveType = _dataExtension.getPrimitiveExtensionAttributeType(a)
				var PrimitiveFieldView fv=new PrimitiveFieldView(primitiveType) 
				fv.setId('''«parent.getId()»x«i»''') 
				fv.setField(a) 
				fv.setList(a.isIsList()) 
				fv.setScopes(scopes) 
				fv.setData(a) 
				fv.setName(a.getName()) 
				fv.setPrimitiveType(primitiveType) 
				return fv 
			}
			
			
		}
		return null 
	}
	def private ComplexTypeView createTypeView(info.scce.dime.data.data.ComplexAttribute ca, Type t, String s, boolean isList, List<MovableContainer> scopes, int i, FieldView parent) {
		if (s === null) return null 
		val String varName=getVarName(s) 
		var String suffix=null 
		if (s.equals(varName)) {
			suffix=s 
		} else {
			suffix=if (varName.length() >= s.length()) null else s.substring(varName.length() + 1)  
		}
		var String finalSuffix=suffix 
		var ComplexTypeView ctv=new ComplexTypeView(t,Collections.EMPTY_SET,this.gui) 
		ctv.setId('''«parent.getId()»x«i»''') 
		ctv.setList(isList) 
		ctv.setName(t.getName()) 
		ctv.setData(t) 
		ctv.setScopes(scopes) 
		val attrs=t.getAttributes().stream().filter([n | n.getName().equals(varName)]).collect(Collectors.toList()) 
		var int index=i 
		for (attr : attrs) {
			var FieldView fv=createFieldView(attr, finalSuffix, scopes, index, ctv) 
			if (fv !== null) {
				ctv.getDisplayedFields().add(fv) 
			}
			index++ 
		}
		return ctv 
	}
	def private ComplexTypeView createTypeView(info.scce.dime.data.data.ExtensionAttribute ca, Type t, String s, boolean isList, List<MovableContainer> scopes, int i, FieldView parent) {
		if (s === null) return null 
		val String varName=getVarName(s) 
		var String suffix=null 
		if (s.equals(varName)) {
			suffix=s 
		} else {
			suffix=if (varName.length() >= s.length()) null else s.substring(varName.length() + 1)  
		}
		var String finalSuffix=suffix 
		var ComplexTypeView ctv=new ComplexTypeView(t,Collections.EMPTY_SET,this.gui) 
		ctv.setId('''«parent.getId()»x«i»''') 
		ctv.setList(isList) 
		ctv.setName(t.getName()) 
		ctv.setData(t) 
		ctv.setScopes(scopes) 
		val attrs=t.getAttributes().stream().filter([n | n.getName().equals(varName)]).collect(Collectors.toList()) 
		var int index=i 
		for (attr : attrs) {
			var FieldView fv=createFieldView(attr, finalSuffix, scopes, index, ctv) 
			if (fv !== null) {
				ctv.getDisplayedFields().add(fv) 
			}
			index++ 
		}
		return ctv 
	}
	def private void extendParis(Node n, Parent p) {
		this.pairs.put(n, p) 			
	}
	def private FieldView createFieldView(Attribute a, String access, List<MovableContainer> scopes, int i, TypeView parent,Map<ModelElement,ComplexTypeView> typeViewsToExtend) {
		var String ac='''«access».''' 
		if (a instanceof PrimitiveAttribute) {
			var PrimitiveAttribute pa=(a as PrimitiveAttribute) 
			var dpa=pa.getAttribute() 
			ac+=dpa.getName() 
			if (isAttributeUsed(a, ac)) {
				var PrimitiveFieldView fv=new PrimitiveFieldView(dpa.getDataType()) 
				fv.setId('''«parent.getId()»x«i»''') 
				fv.setField(dpa) 
				//Is in a new scope?
				var List<MovableContainer> currentScopes=new LinkedList<MovableContainer>() 
				currentScopes.addAll(a.getSuccessors(Table)) 
				currentScopes.addAll(a.getOutgoing(Iteration).stream().map([n | (n.getTargetElement() as MovableContainer)]).collect(Collectors.toList())) 
				if (currentScopes.isEmpty()) {
					currentScopes.addAll(scopes) 
				}
				fv.setScopes(currentScopes) 
				fv.setData(pa) 
				fv.setList(dpa.isIsList()) 
				fv.setName(dpa.getName()) 
				fv.setPrimitiveType(dpa.getDataType()) 
				extendParis(pa, fv) 
				return fv 
			}
			return null 
		}
		if (a instanceof ComplexAttribute) {
			var ComplexAttribute ca=(a as ComplexAttribute) 
			val dca=ca.getAttribute() 
			ac+=dca.getName() 
			if (isAttributeUsed(a, ac)) {
				var ComplexFieldView fv=new ComplexFieldView(this.gui) 
				fv.setId('''«parent.getId()»x«i»''') 
				fv.setField(dca) 
				fv.setData(ca) 
				//Is in a new scope?
				var List<MovableContainer> currentScopes=new LinkedList<MovableContainer>() 
				currentScopes.addAll(a.getSuccessors(Table)) 
				currentScopes.addAll(a.getOutgoing(Iteration).stream().map([n | (n.getTargetElement() as MovableContainer)]).collect(Collectors.toList())) 
				if (currentScopes.isEmpty()) {
					currentScopes.addAll(scopes) 
				}
				fv.setScopes(currentScopes) 
				extendParis(ca, fv) 
				fv.setTypeName(this.getGUI().getTitle() + dca.getDataType().getName()) 
				fv.setList(dca.isIsList()) 
				fv.setName(dca.getName()) 
				var ComplexTypeView ctv=new ComplexTypeView(dca.getDataType(),Collections.EMPTY_SET,this.gui) 
				
				if(typeViewsToExtend.containsKey(a)) {
					typeViewsToExtend.get(a).interfaces.add(new ViewImplementation(this.gui,ctv))
				}
				
				ctv.setData((a as Node)) 
				ctv.setId('''«fv.getId()»x«i»''') 
				ctv.setName(dca.getName()) 
				ctv.setList(dca.isIsList()) 
				ctv.setScopes(currentScopes) 
//				addEmbeddedGUIImports(a, ctv) 
				fv.setView(ctv) 
				return fv 
			}
		}
		if (a instanceof ExtensionAttribute) {
			var ExtensionAttribute ca=(a as ExtensionAttribute) 
			val dca=ca.getAttribute() 
			ac+=dca.getName() 
			if (isAttributeUsed(a, ac)) {
				val complexType = _dataExtension.getComplexExtensionAttributeType(dca)
				if(complexType !== null) {
					var ComplexFieldView fv=new ComplexFieldView(this.gui) 
					fv.setId('''«parent.getId()»x«i»''') 
					fv.setField(dca) 
					fv.setData(ca) 
					//Is in a new scope?
					var List<MovableContainer> currentScopes=new LinkedList<MovableContainer>() 
					currentScopes.addAll(a.getSuccessors(Table)) 
					currentScopes.addAll(a.getOutgoing(Iteration).stream().map([n | (n.getTargetElement() as MovableContainer)]).collect(Collectors.toList())) 
					if (currentScopes.isEmpty()) {
						currentScopes.addAll(scopes) 
					}
					fv.setScopes(currentScopes) 
					extendParis(ca, fv) 
					fv.setTypeName(this.getGUI().getTitle() + complexType.getName()) 
					fv.setList(dca.isIsList()) 
					fv.setName(dca.getName()) 
					var ComplexTypeView ctv=new ComplexTypeView(complexType,Collections.EMPTY_SET,this.gui) 
					
					if(typeViewsToExtend.containsKey(a)) {
						typeViewsToExtend.get(a).interfaces.add(new ViewImplementation(this.gui,ctv))
					}
					
					ctv.setData((a as Node)) 
					ctv.setId('''«fv.getId()»x«i»''') 
					ctv.setName(dca.getName()) 
					ctv.setList(dca.isIsList()) 
					ctv.setScopes(currentScopes) 
					fv.setView(ctv) 
					return fv 
				} else {
					val primitiveType = _dataExtension.getPrimitiveExtensionAttributeType(dca)
					ac+=dca.getName() 
					if (isAttributeUsed(a, ac)) {
						var PrimitiveFieldView fv=new PrimitiveFieldView(primitiveType) 
						fv.setId('''«parent.getId()»x«i»''') 
						fv.setField(dca) 
						//Is in a new scope?
						var List<MovableContainer> currentScopes=new LinkedList<MovableContainer>() 
						currentScopes.addAll(a.getSuccessors(Table)) 
						currentScopes.addAll(a.getOutgoing(Iteration).stream().map([n | (n.getTargetElement() as MovableContainer)]).collect(Collectors.toList())) 
						if (currentScopes.isEmpty()) {
							currentScopes.addAll(scopes) 
						}
						fv.setScopes(currentScopes) 
						fv.setData(dca) 
						fv.setList(dca.isIsList()) 
						fv.setName(dca.getName()) 
						fv.setPrimitiveType(primitiveType) 
						extendParis(dca, fv) 
						return fv 
					}
				}
			}
		}
		return null 
	}
	def private boolean isVariableUsed(Variable cv, String s) {
		var String access='''«s».«cv.getName()»''' 
		//Variable used in Expression
		if (this.ee.isUsed(s)) {
			this.variablesInUse.add(cv) 
			return true 
		}
		//Variable is known
		if (this.variablesInUse.contains(cv)) return true 
		//Variable is directly used
		var List<Edge> edges=cv.getOutgoing().stream().filter([n | !(n instanceof ComplexAttributeConnector || n instanceof ComplexListAttributeConnector)]).collect(Collectors.toList()) 
		if (!edges.isEmpty()) {
			this.variablesInUse.add(cv) 
			return true 
		}
		if (!cv.getIncoming(Write).isEmpty()) {
			this.variablesInUse.add(cv) 
			return true 
		}
		//ComplexVariable
		if (cv instanceof ComplexVariable) {
			//Variable is extended
			var ComplexVariable ccv=(cv as ComplexVariable) 
			var List<Edge> attributeConnector=cv.getOutgoing().stream().filter([n | (n instanceof ComplexAttributeConnector || n instanceof ComplexListAttributeConnector)]).collect(Collectors.toList()) 
			if (!attributeConnector.isEmpty()) {
				for (Edge edge : attributeConnector) {
					var Variable target=(edge.getTargetElement() as Variable) 
					if (isVariableUsed(target, access)) {
						this.variablesInUse.add(cv) 
						return true 
					}
				}
			}
			//Attributes are used
			for (attr : ccv.getAttributes()) {
				if (isAttributeUsed(attr, access)) {
					this.variablesInUse.add(cv) 
					return true 
				}
			}
		}
		return false 
	}
	def private boolean isAttributeUsed(Attribute ca, String s) {
		if (ca instanceof Attribute) {
			var String access='''«s».''' 
			if (ca instanceof PrimitiveAttribute) {
				var PrimitiveAttribute pa=(ca as PrimitiveAttribute) 
				var info.scce.dime.data.data.PrimitiveAttribute dpa=pa.getAttribute() 
				access+=dpa.getName() 
			} else if (ca instanceof ComplexAttribute) {
				var ComplexAttribute coa=(ca as ComplexAttribute) 
				val dca=coa.getAttribute() 
				access+=dca.getName() 
			} else if (ca instanceof ExtensionAttribute) {
				var ExtensionAttribute coa=(ca as ExtensionAttribute) 
				val dca=coa.getAttribute() 
				access+=dca.getName() 
			}
			// Expression check
			if (this.ee.isUsed(access)) {
				this.attributesInUse.add(ca) 
				return true 
			}
		}
		if (this.attributesInUse.contains(ca)) {
			return true 
		}
		//Variable is directly used
		if (!ca.getOutgoing().isEmpty()) {
			this.attributesInUse.add(ca) 
			return true 
		}
		return false 
	}
	
	private def debug(String msg) {
		if (gui.title == "ResultBoolean") println('''[«class.simpleName»] «msg»''')
	}
	
	static def ComplexTypeView findCorrespondingSelectiveForData(ComplexTypeView currentTypeView, ComplexVariable start, Node target) {
		if(start.equals(target)) {
			return currentTypeView
		}
		val ca = start.attributes.findFirst[equals(target)]
		if(ca !== null){
			//find corresponding selective
			if(ca instanceof ComplexAttribute) {
				// for complex attributes there is only the possibility, that they are the searched target
				val matchingFieldView = currentTypeView.displayedFields.filter(ComplexFieldView).findFirst[name.equals(ca.attribute.name)]
				if(matchingFieldView !== null) {
					return matchingFieldView.view as ComplexTypeView
				}
			}
			if(ca instanceof ExtensionAttribute) {
				// for complex attributes there is only the possibility, that they are the searched target
				val matchingFieldView = currentTypeView.displayedFields.filter(ComplexFieldView).findFirst[name.equals(ca.attribute.name)]
				if(matchingFieldView !== null) {
					return matchingFieldView.view as ComplexTypeView
				}
			}
			if(ca instanceof ComplexListAttribute) {
				//if the target data element is a complex list attribute, the current type view is the searched one
				return currentTypeView
			}
		}
		// walk threw extended complex attribute variables
		for(cac:start.outgoingComplexAttributeConnectors) {
			val matchingFieldView = currentTypeView.displayedFields.filter(ComplexFieldView).findFirst[name.equals(cac.associationName)]
			if(matchingFieldView !== null) {
				val foundCorresponding = (matchingFieldView.view as ComplexTypeView).findCorrespondingSelectiveForData(cac.targetElement,target)
				if(foundCorresponding !== null) {
					return foundCorresponding
				}
			}
		}
		
		//walk threw extended list variables
		for(clac:start.outgoingComplexListAttributeConnectors) {
			val foundCorresponding = currentTypeView.findCorrespondingSelectiveForData(clac.targetElement,target)
			if(foundCorresponding !== null) {
				return foundCorresponding
			}
		}
		
		null
	}
	
	static def Iterable<ComplexTypeView> findBranchPortData(GUIBranch branch, ComplexGUIBranchPort branchPort, GUI currentGUI, GenerationContext genctx) {
		val guiCompoundView = genctx.getCompoundView(currentGUI)
		guiCompoundView.compounds.filter(ComplexTypeView).map[branch.findBranchPortData(
			branchPort,
			currentGUI,
			it.name,
			it,
			genctx
		)].flatten
	}
	
	static def Iterable<ComplexTypeView> findBranchPortData(GUIBranch branch, ComplexGUIBranchPort branchPort, GUI currentGUI, String inputName, ComplexTypeView currentTypeView, GenerationContext genctx) {
		//find all buttons placed in this GUI
//		println('''Look for Button «branch.name» and Port «branchPort.name», starting with «inputName» in «currentGUI.title»''')
		val containedButtons = genctx.guiExtension.find(currentGUI,Button).filter[GUIBranchPort.isBranchable(it)]
		
		//find corresponding input variable
		val inputVariable = genctx.guiExtension.topLevelVariables(currentGUI).filter(ComplexVariable).findFirst[name.equals(inputName)]
		
		if(inputVariable === null) {
			throw new IllegalStateException('''Corresponding Input «inputName» not found in GUI «currentGUI.title»''')
		}
		
		//if branch cannot be found in list of buttons
		val foundButtons = containedButtons.filter[n|GUIBranch.getBranchName(n).equals(branch.name)]
		
		val matchingSelectives = new LinkedList<ComplexTypeView>()
		
		if(foundButtons.isEmpty) {
//			println('''Found no Button «branch.name» in «currentGUI.title»''')
			//continue search in embedded GUISIBs (by following the data flow to the input port)
			genctx.guiExtension.find(currentGUI,GUISIB).forEach[sib|{
				if(genctx.guiExtension.dispatchedOutputs(sib.gui).exists[it.name.equals(branch.name)]) {
//					println('''Looking for Button «branch.name» in GUISIB «sib.label»''')
					//embedded GUISIB contains relevant branch
					//find a dataflow to the input ports
					sib.complexInputPorts.filter[!incoming.isEmpty].forEach[inputPort|
//						println('''Looking for Button «branch.name» in GUISIB «sib.label» starting with port «inputPort.name»''')
						//data node which is connected to the input port
						val targetDataNode = inputPort.predecessors.get(0)
						//find the corresponding type view
						val inputPortTypeView = currentTypeView.findCorrespondingSelectiveForData(inputVariable,targetDataNode)
						if(inputPortTypeView !== null) {
							//throw new IllegalStateException ('''No selective found for port «inputPort.name» of GUISIB «sib.label» in GUI «currentGUI.title»''')
//							println('''Input Port «inputPort.name» of GUISIB «sib.label» correlated to CTV «inputPortTypeView.name»''')
							//check for each complex input port, whether the input is connected to the button as output port
							matchingSelectives += branch.findBranchPortData(branchPort,sib.gui,inputPort.name,inputPortTypeView,genctx)
						}
						
					]
				}
				
			}]
		} else {			
//			println('''Found Button «branch.name» in «currentGUI.title»''')
			//if branch is found in list of buttons
			//find corresponding selective and return
			foundButtons.forEach[btn|{
				//check if searched branch port is found for button
				val foundPort = GUIBranchPort.getPorts(btn).findFirst[name.equals(branchPort.name)]
			
				if(foundPort !== null) {
//					println('''Found Branch Port «foundPort.name» in «currentGUI.title»''')
					//port is found, look for connected data and get selective
					val portNode = foundPort.portNode
					if(portNode instanceof Edge) {
//						println('''Found Branch Port Node «foundPort.name» is AddToSubmission''')
						// if the complex submission edge is the port representant, use the source for type view calculation
						matchingSelectives += currentTypeView.findCorrespondingSelectiveForData(inputVariable,portNode.sourceElement)
					} else {
//						println('''Found Branch Port Node «foundPort.name» is Button''')
						// if the complex submission edge is NOT the port representant, use the data node for type view calculation
						matchingSelectives += currentTypeView.findCorrespondingSelectiveForData(inputVariable,portNode as Node)
					}
				}
			}]
		}
		
		
		return matchingSelectives.filter[it!==null]
	}
}