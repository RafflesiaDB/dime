package info.scce.dime.generator.gui.data

import graphmodel.GraphModel
import graphmodel.ModelElement
import graphmodel.Node
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.TypeAttribute
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.generator.dad.GenerationContext
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.BaseComplexTypeView
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.CompoundView
import info.scce.dime.generator.gui.rest.model.FieldView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.rest.model.Parent
import info.scce.dime.generator.gui.rest.model.PrimitiveFieldView
import info.scce.dime.generator.gui.rest.model.PrimitiveTypeView
import info.scce.dime.generator.gui.rest.model.TypeView
import info.scce.dime.generator.gui.rest.model.ViewImplementation
import info.scce.dime.gui.gui.AbstractBranch
import info.scce.dime.gui.gui.AddToSubmission
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexListAttribute
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexRead
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.ComplexGUIBranchPort
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.helper.PrimitiveGUIBranchPort
import info.scce.dime.process.process.EndSIB
import java.util.ArrayList
import java.util.Collection
import java.util.Collections
import java.util.HashMap
import java.util.HashSet
import java.util.LinkedList
import java.util.List
import java.util.Map

class SelectiveExtension {
	
	static extension DataExtension = DataExtension.instance
	static extension GUIExtension = new GUIExtension
	
	GenerationContext genctx
	
	new(GenerationContext genctx) {
		this.genctx = genctx
		this._gUIExtension = genctx.guiExtension
	}
	
	def List<ViewImplementation> viewImplementationForGUIInput(String inputName,GUI gui,ComplexTypeView typeViewToExtend){
		viewImplementationForGUIInput(inputName, gui, typeViewToExtend, /*checkType*/ false)
	}
	
	def List<ViewImplementation> viewImplementationForGUIInput(String inputName,GUI gui,ComplexTypeView typeViewToExtend, boolean checkType){
		gui.inputVariables
			.filter(ComplexVariable)
			.filter[!checkType || dataType.isTypeOf(typeViewToExtend.type)]
			.findFirst[name == inputName]
			?.viewImplementationForVariable(gui,typeViewToExtend)
			?: newLinkedList
	}
	
	def List<ViewImplementation> viewImplementationForVariable(ComplexVariable cv,GUI gui,ComplexTypeView typeViewToExtend){
		if(cv==null)return Collections.EMPTY_LIST
		
		val Map<ModelElement,ComplexTypeView> inputTypeViewsToExtend = new HashMap
		
		if (cv.isIsList() && !cv.getOutgoing(ComplexListAttributeConnector).isEmpty()) {
			var ComplexVariable target=cv.getOutgoing(ComplexListAttributeConnector).get(0).getTargetElement()
			inputTypeViewsToExtend.put(target,typeViewToExtend)
		} else {
			inputTypeViewsToExtend.put(cv,typeViewToExtend)
		}
		
		//Get GUICV
		var GUICompoundView guiCV=null 
		var guiCVCOpt=genctx.getCompoundView(gui)
		if (guiCVCOpt!=null) {
			guiCV=guiCVCOpt
		} else {
			guiCV=new GUICompoundViewCreator(gui, genctx).collect(inputTypeViewsToExtend) as GUICompoundView 
		}
		
		val vis = new LinkedList
		
		if (cv.isIsList() && !cv.getOutgoing(ComplexListAttributeConnector).isEmpty()) {
			var ComplexVariable target=cv.getOutgoing(ComplexListAttributeConnector).get(0).getTargetElement() 
			var ComplexTypeView ctv=(guiCV.getPairs().get(target) as ComplexTypeView) 
			var ViewImplementation vi=new ViewImplementation(gui,ctv) 
			vis.add(vi) 
		}
		var ComplexTypeView ctv=(guiCV.getPairs().get(cv) as ComplexTypeView) 
		if(ctv!=null){
			var ViewImplementation vi=new ViewImplementation(gui,ctv) 
			vis.add(vi) 			
		}
		return vis
	}
	
	def getToTypeView(Parent p) {
		if(p instanceof ComplexFieldView){
			return p.view
		}
		return p
	}
	
	/**
	 * Find the complex type view b1 which extends the composition CTV b2 of a2
	 * b1 --> b2<--
	 * |     |    |
	 * V     V    |
	 * a1 -->a2 ---
	 * Find b1 from a1 and a2
	 */
	def rebuildComposition(ComplexTypeView a1,ComplexTypeView a2,CompoundView cv) {
		if(a2.compositionView!=null) {
			val b2 = a2.compositionView
			val b1s = cv.allTypeViews.filter(ComplexTypeView).filter[!identically(a1)].filter[A1|A1.interfaces.exists[n|n.parent.equals(b2)]].filter[hasWayToComplexTypeView(a1)]
			if(b1s.empty) {
				println('''[WARN] No recursion (composition) partner for «a1.name» to  «a2.name»''')
				return
			}
			
			if(b1s.size>1){
				println('''[WARN] Multiple recursion (composition) partners for «a1.name» to «a2.name» {«b1s.map['''«name»«id»'''].join(",")»}''')
				return
			}
			if(a1.getgModel instanceof GUI){
				println('''[INFO] -> «a1.name» of «(a1.getgModel as GUI).title» is same as «b1s.get(0).name»''')				
			}
			a1.toSameType = b1s.get(0)
		}	
	}
	
	def hasWayToComplexTypeView(ComplexTypeView b1,ComplexTypeView a1) {
		b1.allSubTypes.exists[c|c.identically(a1)]
	}
	
	def List<ViewImplementation> viewImplementationForEventInput(String inputName,GUI gui,ComplexTypeView typeViewToExtend){
		//find the corresponding event
		var evtPort =gui.events.flatMap[outputPorts].findFirst[name == inputName]
		if(evtPort === null){
			return new LinkedList
		}
		var complexVariables=evtPort.outgoing.map[targetElement].filter(ComplexVariable)
		if(complexVariables.empty){
			return new LinkedList
		}
		return complexVariables.map[viewImplementationForVariable(gui,typeViewToExtend)].flatten.toList
	}
	
	def List<ViewImplementation> viewImplementationForGUIBranch(GUI gui,String branchName,String portName,ComplexTypeView typeViewToExtend)
	{
		val vis = new LinkedList
		//search for all GUI branches
//		val guiBranches = gc.guiextension.branches(gui)
//		val branchesByName = guiBranches.get(branchName)
//		val complexPorts = branchesByName.filter[gc.guiextension.guiSIBbranchPortIsComplex(it)]
//		val findName = complexPorts.findFirst[gc.guiextension.guiSIBbranchPortName(it).equals(portName)]
		val portRepresentant = gui.GUIBranchesMerged.findFirst[it.name == branchName].ports.filter(ComplexGUIBranchPort).findFirst[it.name == portName]
		if (portRepresentant !== null){
			// get selective representant
			val dataNode = portRepresentant
			var GraphModel graphModel = null
			var Parent selectiveRep = null
			switch dataNode {
				AddToSubmission:{
					selectiveRep = dataNode.sourceElement.findGUISelective(typeViewToExtend)
					graphModel = dataNode.rootElement
				}
				ComplexVariable:{
					selectiveRep = dataNode.findGUISelective(typeViewToExtend)
					graphModel = dataNode.rootElement
				}
				ComplexAttribute:{
					selectiveRep = dataNode.findGUISelective(typeViewToExtend)
					graphModel = dataNode.rootElement
				}
				ComplexListAttribute:{
					selectiveRep = dataNode.findGUISelective(typeViewToExtend)
					graphModel = dataNode.rootElement
				}
			}
			if(selectiveRep !== null) {
				var ViewImplementation vi = new ViewImplementation(graphModel,selectiveRep as ComplexTypeView)
				vis.add(vi)
			}
			
			// Variable is present in GUI
			if (dataNode instanceof Variable) {
				if (((dataNode as Variable)).isIsList() &&
					!dataNode.getOutgoing(ComplexListAttributeConnector).empty) {
					var ComplexVariable target = dataNode.getOutgoing(
						typeof(ComplexListAttributeConnector)).get(0).getTargetElement()
					var ComplexTypeView ctv = (target.findGUISelective(typeViewToExtend) as ComplexTypeView)
					if (ctv !== null) {
						var ViewImplementation iteratorVi = new ViewImplementation(target.rootElement,ctv)
						vis.add(iteratorVi)
					}
				}
			}
			//GUI PLugin
			if(dataNode instanceof ComplexParameter) {
				val outputPortRep = dataNode.toComplexType
				val guiPlugin =  gui.findDeepInGUIs(GUIPlugin).findFirst[ it.pluginOutputs.exists[name == branchName]]
				val equallyTypedInputPorts = guiPlugin.inputPorts.filter(ComplexInputPort).filter[dataType.name.equals(outputPortRep.name)]
				val selectives = equallyTypedInputPorts.map[getIncoming(ComplexRead)].flatten.map[sourceElement].map[findGUISelective(typeViewToExtend)]
				selectives.filter(ComplexTypeView).forEach[n|{
					var ViewImplementation vi = new ViewImplementation(gui,n as ComplexTypeView)
					vis.add(vi)
				}]
			}
			if(dataNode instanceof ComplexOutputPort) {
				val outputPortRep = dataNode.toComplexType
				if(dataNode.container instanceof AbstractBranch){
					val branch = dataNode.container as AbstractBranch
					if(branch.container instanceof GUIPlugin){
						val guiPlugin =  branch.container as GUIPlugin
						val equallyTypedInputPorts = guiPlugin.inputPorts.filter(ComplexInputPort).filter[dataType.name.equals(outputPortRep.name)]
						val selectives = equallyTypedInputPorts.map[getIncoming(ComplexRead)].flatten.map[sourceElement].map[findGUISelective(typeViewToExtend)]
						selectives.filter(ComplexTypeView).forEach[n|{
							var ViewImplementation vi = new ViewImplementation(gui,n)
							vis.add(vi)
						}]
						if(vis.empty){
							//no matching input port
							val n = new ComplexTypeView(dataNode.toComplexType, new HashSet, dataNode.rootElement)
							n.data = dataNode
							n.GModel = dataNode.rootElement
							n.id = dataNode.id
							n.list = dataNode.isIsList
							n.name = dataNode.name
							var ViewImplementation vi = new ViewImplementation(dataNode.rootElement,n)
							vis.add(vi)
						}
					}
				}
				
			}
		} else {
			System.err.println("Missing port")
		}
		
		
		return vis
	}
	
	
	def findGUISelective(Node n,ComplexTypeView typeViewToExtend){
		val gui = n.rootElement as GUI
		var GUICompoundView guiCV=null 
		var guiCVCOpt=genctx.getCompoundView(gui)
		if (guiCVCOpt!=null) {
			guiCV=guiCVCOpt
		} else {
			val Map<ModelElement,ComplexTypeView> typeViewsToExtend = new HashMap
			typeViewsToExtend.put(n,typeViewToExtend)
			guiCV=new GUICompoundViewCreator(gui, genctx).collect(typeViewsToExtend) as GUICompoundView
		}
		val pair = guiCV.pairs.get(n)
		if(pair!=null){
			return pair
		}
		
	}
	
	def primitiveDartType(PrimitiveType pt,boolean list) {
		'''«IF list»DIMEList<«ENDIF»«{
			switch(pt) {
				case BOOLEAN: '''bool'''
				case INTEGER: '''int'''
				case FILE: '''FileReference'''
				case REAL: '''double'''
				case TIMESTAMP: '''DateTime'''
				case TEXT: '''String'''
			}
			}»«IF list»>«ENDIF»'''
	}
	
	def dispatch selectiveClassName(PrimitiveTypeView v,boolean list){
		v.primitiveType.primitiveDartType(list&&v.list)
	}
	
	def dispatch selectiveClassName(PrimitiveFieldView v,boolean list){
		v.primitiveType.primitiveDartType(list&&v.list)
	}
	
	def dispatch selectiveClassName(ComplexTypeView v,boolean list)
	'''
	«val dataType = v.type»
	«IF list»DIMEList<«ENDIF»«DyWASelectiveDartGenerator.prefix(dataType)».«dataType.name»«IF list»>«ENDIF»
	'''
	
	def dispatch CharSequence selectiveClassName(ComplexFieldView v,boolean list)
		'''«(v.view as ComplexTypeView).selectiveClassName(list)»'''
	
		def static computeBranchTypeViews(GenerationContext genctx, GUI gui, Collection<Type> typeCollector, Collection<BaseComplexTypeView> complexTypeViewColelctor) {
		val branches = gui.getGUIBranches(true)
		if (!branches.isEmpty) {
			val guiBranchElements = branches.filter[it.hasGUIOrigin]

			val Collection<Type> types = typeCollector
			val Collection<BaseComplexTypeView> complexTypeViews = complexTypeViewColelctor

			/*
			 * Calculate the Branch Name-> (Port Name -> TypeView) Map for GUI Buttons, GUIPlugin Branches found in the given GUI
			 */
			//map (branch name to -> (port name -> TypeView))
			val Map<GUIBranch,Map<String,List<TypeView>>> guiBranchTypeViews = new HashMap
			guiBranchElements.forEach[branch|{
				//check that element is unique (avoids to equal buttons)
				if(!guiBranchTypeViews.keySet.exists[element.equals(branch.element)]) {

					guiBranchTypeViews.put(branch,new HashMap)
					branch.ports.forEach[port|{ 
						if(port instanceof ComplexGUIBranchPort) {
							//add complex type to type-set

							//add complex ports
							val portSelectiveRepresentant = port.typeViewData
							if(portSelectiveRepresentant instanceof ComplexParameter || portSelectiveRepresentant === null) {
								//FIXME portRepresentant === null appears if the portRepresentant cannot be found 
								// This can happen when a branch is overloaded by multiple e.g. buttons that differ in amount of ports
								// As a result of this, the relation between a port and its representative is not distinct
								types.add(port.type);
								//special case, port results of GUIPlugin. BaseSelective is used
								val tv = new BaseComplexTypeView(port.type)
								tv.data = null;
								tv.GModel = gui
								tv.list = port.isList
								complexTypeViews.add(tv)
								guiBranchTypeViews.get(branch).put(port.name, #[tv])
							} else {
								// complex type view found, get corresponding GUICompoundSelective by element root element
								val branchPortSelectves = GUICompoundViewCreator.findBranchPortData(branch,port,gui,genctx);

								var List<ComplexTypeView> resultSelectives = new ArrayList<ComplexTypeView>() 
								if(branchPortSelectves.empty) {
									val portRepresentantModelElement = portSelectiveRepresentant as ModelElement
									val guiSelectives = genctx.getCompoundView(portRepresentantModelElement.rootElement as GUI)
									if(!guiSelectives.pairs.containsKey(portRepresentantModelElement)) {
										throw new IllegalStateException('''Selectvie not found «guiSelectives» «guiSelectives.name» for port «port.name» on branch «branch»''')
									}
									resultSelectives.add(new ComplexTypeView(guiSelectives.pairs.get(portRepresentantModelElement) as ComplexTypeView))
								} else {
									resultSelectives.addAll(branchPortSelectves.map[new ComplexTypeView(it)])
								}
								resultSelectives.forEach[it.list=port.list]
								complexTypeViews.addAll(resultSelectives)
								//find and put the corresponding type view
								guiBranchTypeViews.get(branch).put(port.name, resultSelectives.filter(TypeView).toList)
							}
						} else if (port instanceof PrimitiveGUIBranchPort){
							//add primitive port
							val tv = new PrimitiveTypeView
							tv.id = port.type.literal
							tv.primitiveType = port.type
							tv.name = port.name
							tv.list = port.isList
							guiBranchTypeViews.get(branch).put(port.name, #[tv])
						}
					}]
				}
			}];

			return guiBranchTypeViews
		}

		return Collections.emptyMap
	}
	
	def static computeProcessTypeViews(GUI gui, Collection<Type> typeCollector) {
		val types = typeCollector
		val processBranches = gui.getProcessBranches
		val Map<EndSIB,Map<String,List<TypeView>>> processBranchTypeViews = new HashMap
		processBranches.forEach[branch|{
			processBranchTypeViews.put(branch.element as EndSIB, new HashMap)
			branch.ports.forEach[port|{
				if (port instanceof ComplexGUIBranchPort) {
					//complex port
					//add complex type to type-set
					types.add(port.type);
					val tv = new BaseComplexTypeView(port.type)
					tv.data = port.portNode as ModelElement;
					tv.GModel = gui
					tv.list = port.isList
					processBranchTypeViews.get(branch.element).put(port.name, #[tv])
					
				} else if (port instanceof PrimitiveGUIBranchPort) {
					//primitive port
					val tv = new PrimitiveTypeView
					tv.data = port.portNode as ModelElement;
					tv.primitiveType = port.type
					tv.list = port.isList
					processBranchTypeViews.get(branch.element).put(port.name, #[tv])
				}
			}]
		}];
		return processBranchTypeViews
	}
	
	def static collectComplexTypeViews(GenerationContext genctx) {
		val cache = new ArrayList<ComplexTypeView>();
		
		genctx.getCompoundViews.forEach[n | n.compounds.filter(ComplexTypeView).forEach[e | fillRecursively(e, cache)]];
		
		return cache;
	}

	private static def void fillRecursively(ComplexTypeView ctv, List<ComplexTypeView> cache)	{
		cache.add(ctv);
		ctv.displayedFields.filter(ComplexFieldView).map[view].forEach[n | fillRecursively(n as ComplexTypeView, cache)]
	}
	
	static def buildBlankTypeView(Type t) {
		buildBlankTypeView(t, new HashMap)
	}
	
	static def ComplexTypeView buildBlankTypeView(Type t, Map<Type, ComplexTypeView> cache) {
		val result = t.buildNonRecursiveTypeView
		val fields = new ArrayList<FieldView>(t.attributes.size);

		cache.put(t, result)

		for (Attribute a : t.inheritedAttributes.map[originalAttribute].filter(TypeAttribute)) {
			var FieldView fv 
			if(a.isPrimitive) {
				fv = new PrimitiveFieldView(a.primitiveDataType)
			} else  {
				val view = new ComplexFieldView(null)
				view.typeName = t.name
				view.view = a.complexDataType.buildNonRecursiveTypeView
				fv = view
				
//				val view = new ComplexFieldView(null)
//				view.typeName = t.name
//				
//				val dataType = a.complexDataType
//				val cachedView = cache.get(dataType)
//				view.view = if (cachedView !== null) {cachedView} else {buildBlankTypeView(dataType, cache)}
//				fv = view
			}

			fv.field = a;
			fv.name = a.name
			fv.list = a.isList

			fields.add(fv)
		}

		result.displayedFields = fields
		result
	}

	static def buildNonRecursiveTypeView(Type t) {
		val result = new ComplexTypeView(t, Collections.emptySet, null)
		result.name = t.name
		result.data = t.originalType
		result
	}
}