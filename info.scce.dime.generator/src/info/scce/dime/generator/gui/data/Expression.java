package info.scce.dime.generator.gui.data;

import info.scce.dime.gui.gui.MovableContainer;
/**
 * The expression is used to combine a given (partial) Angular expression
 * and the component which is represents the scope of the variable 
 * mentioned in the expression
 * @author zweihoff
 *
 */
public class Expression {
	private String expression;
	private MovableContainer scope;

	
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public MovableContainer getScope() {
		return scope;
	}
	public void setScope(MovableContainer scope) {
		this.scope = scope;
	}
	
}
