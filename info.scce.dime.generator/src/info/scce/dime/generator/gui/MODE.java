package info.scce.dime.generator.gui;

/**
 * The MODE defines for a variable access, which kind of data access should be done.
 * SET_INIT: the variable should be set and if necessary is initialized before setting. After operation, the variable is not null.
 * GET_INIT: the variable is initialized if necessary and returned. Never null.s
 * GET: default get operation, could be null
 * @author zweihoff
 *
 */
public enum MODE {
	SET_INIT, GET_INIT, GET
}
