package info.scce.dime.generator.search;

import info.scce.dime.generator.util.EclipseUtils;
import info.scce.dime.search.search.Search;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.xtext.util.StringInputStream;

public class SearchGenerator {
	
	private final String pathPrefix = "dywa-app/app-persistence/target/generated-sources/";
	
	private IProject project = null;
	private IProgressMonitor monitor = null;
	
	SearchControllerHelper helper = new SearchControllerHelper();
	private String contentsInterface = "";
	private String contentsImpl = "";
	
	public SearchGenerator(IProject project, IProgressMonitor monitor) {
		super();
		this.project = project;
		this.monitor = monitor;
		
		this.contentsImpl += helper.generateImplHead();
		this.contentsInterface += helper.generateInterfaceHead();
	}

	public void generateForModel(Search model, IPath outlet,
			IProgressMonitor monitor) {

		System.out.println("Generating Search: " + model.getModelName());
		monitor.subTask("Generating Search: " + model.getModelName());

		this.contentsImpl += helper.generateMethod(model);
		this.contentsInterface += helper.generateMethodDefinition(model);
		
	}
	
	public void finishFiles() {
		contentsImpl += helper.generateImplFooter();
		contentsInterface += helper.generateInterfaceFooter();
		writeFile("SearchControllerImpl.java", helper.packageName, contentsImpl);
		writeFile("SearchController.java", helper.packageName, contentsInterface);
	}
	
	private void writeFile(String fileName, String packageName, String contents) {
		if (contents != null) {
			final IFolder outputFolder = project.getFolder(pathPrefix + packageName.replace(".", File.separator));
			
			final IFile output = outputFolder.getFile(fileName);
			try {
				EclipseUtils.mkdirs(outputFolder);

				if (!output.exists()) {
					output.create(new StringInputStream(contents.toString()),
							true, monitor);
				} else {
					output.setContents(
							new StringInputStream(contents.toString()), true,
							true, monitor);
					;
				}
			} catch (CoreException e) {
				throw (new RuntimeException(e));
			}
		}
	}

}
