package info.scce.dime.generator.search;

import info.scce.dime.search.search.util.SearchSwitch
import info.scce.dime.search.search.Search
import info.scce.dime.search.search.SearchInterface
import info.scce.dime.search.search.ComplexOutputParameter
import info.scce.dime.data.data.Type
import info.scce.dime.search.search.SearchNode
import info.scce.dime.search.search.PrimitiveType
import info.scce.dime.search.search.SearchConditional
import info.scce.dime.search.search.AndOperator
import info.scce.dime.search.search.OrOperator
import info.scce.dime.search.search.IntegerCompareOperator
import info.scce.dime.data.data.Attribute
import info.scce.dime.search.search.CompareOperator
import info.scce.dime.search.search.RealCompareOperator
import info.scce.dime.search.search.TextCompareOperator
import info.scce.dime.search.search.BooleanCompareOperator
import info.scce.dime.search.search.TimestampCompareOperator
import info.scce.dime.search.search.DataEdge
import info.scce.dime.search.search.InputParameter
import info.scce.dime.search.search.Variable
import info.scce.dime.search.search.PrimitiveInputParameter

public class SearchControllerHelper extends SearchSwitch<CharSequence> {

	public final String packageName = "de.ls5.dywa.generated.search.controller"; 
	public final String packageEntity = "de.ls5.dywa.generated.entity";
	public final String packageEntityWrapper = "de.ls5.dywa.generated.search.entity";
	
	def generateImplHead() '''
		package «this.packageName»;
		
		import java.util.List;
		import java.util.Map;
		import java.util.HashMap;
		import javax.enterprise.context.RequestScoped;
		import javax.inject.Inject;
		import javax.inject.Named;
		import de.ls5.dywa.generated.search.api.*;
		
		@Named
		@RequestScoped
		public class SearchControllerImpl implements SearchController {
		
			@Inject
			private SearchBuilder searchBuilder;
	'''

	def generateImplFooter() '''
		}
	'''

	def generateInterfaceHead() '''
		package «this.packageName»;
		
		import java.util.List;
		
		public interface SearchController {
	'''

	def generateInterfaceFooter() '''
		}
	'''

	def generateMethodDefinition(Search search) '''
		
			// Generated Search for «search.modelName»
			public List<«getOutPutClass(search.searchInterfaces.get(0))»> executeSearch«search.methodName»(«FOR ip : search.searchInterfaces.get(0).inputParameters SEPARATOR ','»«ip.inputParameters»«ENDFOR»);
			'''

	def generateMethod(Search search) '''
		
			// Generated Search for «search.modelName»
			public List<«getOutPutClass(search.searchInterfaces.get(0))»> executeSearch«search.methodName»(«FOR ip : search.searchInterfaces.get(0).inputParameters SEPARATOR ','»«ip.inputParameters»«ENDFOR») {
				
				DyWAQuery<«getOutPutClass(search.searchInterfaces.get(0))»> _query_ = this.searchBuilder.createQuery(«getOutPutClass(
			search.searchInterfaces.get(0))».class);
			_query_.distinct(true);
				From<«getOutPutClass(search.searchInterfaces.get(0))»> _root_ = _query_.from(«getOutPutClass(
			search.searchInterfaces.get(0))».class);
				
				PathConstraintBuilder<«getOutPutClass(search.searchInterfaces.get(0))»> _pathConstraintBuilder_ = _root_.newPathConstraintBuilder();
				
				// build constraint
				«IF search.searchInterfaces.get(0).getPredecessors(SearchNode).size > 0»
				_pathConstraintBuilder_.addConstraints(«doSwitch(search.searchInterfaces.get(0).getPredecessors(SearchNode).get(0))»);
				«ENDIF»
				
				PathConstraint _pathConstraint_ = _pathConstraintBuilder_.buildPathConstraint();
				_query_.where(_pathConstraint_);
				
«««				// create parameterMap
«««				Map<String, Object> _parameterMap_ = new HashMap<>();
«««				«FOR ip : search.searchInterfaces.get(0).inputParameters»
«««				«getPutInParameterMap(ip)»
«««				«ENDFOR»
				return this.searchBuilder.executeQuery(_query_);
			}
			
	'''

	def getMethodName(Search search) {
		return search.id.replace('-', '');
	}

	def getOutPutClass(SearchInterface si) {
		return doSwitch(si.outputParameters.get(0))
	}
	
	def getInputParameters(InputParameter ip) {
		if (ip instanceof PrimitiveInputParameter)
			return getPrimitiveType(ip.dataType) + " " + ip.name
		return "UNDEF"
	}
	
	def getPutInParameterMap(InputParameter ip) {
		if (ip instanceof PrimitiveInputParameter)
			return "_parameterMap_.put(\"" + ip.name + "\"," + ip.name + "); "
		return "UNDEF"
	}

	def getPrimitiveType(PrimitiveType pType) {
		switch (pType) {
			case BOOLEAN: return "Boolean"
			case INTEGER: return "Long"
			case REAL: return "Double"
			case TEXT: return "String"
			case TIMESTAMP: return "java.util.Date"
		}
		return "UNDEF"
	}

	def getOperatorMethodName(String operator) {
		switch (operator) {
			case "less_than": return "lt"
			case "less_or_equal": return "leq"
			case "greater_than": return "gt"
			case "greater_or_equal": return "geq"
			case "equal": return "equal"
			case "not_equal": return "not equal"
			case "like": return "like"
			case "before": return "before"
			case "after": return "after"
		}
		return "UNDEF"
	}

	override caseComplexOutputParameter(ComplexOutputParameter cop) '''«this.packageEntity».«cop.dataType.name»'''

	override caseAndOperator(AndOperator op) '''_pathConstraintBuilder_.and(«FOR sn : op.getPredecessors(SearchNode) SEPARATOR ','»«doSwitch(
		sn)»«ENDFOR»)'''

	override caseOrOperator(OrOperator op) '''_pathConstraintBuilder_.or(«FOR sn : op.getPredecessors(SearchNode) SEPARATOR ','»«doSwitch(
		sn)»«ENDFOR»)'''

	override caseSearchConditional(SearchConditional sc) '''_pathConstraintBuilder_.«getCompareOperator(
		sc.compareOperators.get(0))»(«sc.compareAttribute»,«sc.compareVariable»)'''

	def getCompareAttribute(SearchConditional sc) '''«this.packageEntityWrapper».«getEntityWrapper(sc.attribute)»_.«sc.
		attribute.name»'''

	def getCompareVariable(SearchConditional sc) {
		var varNode = sc.compareOperators.get(0).getIncoming(DataEdge).get(0).sourceElement
		if (varNode instanceof InputParameter)
			return (varNode).name
		if (varNode instanceof Variable)
			return varNode.inputParameterPredecessors.get(0).name
		return 'UNDEF'
			
	}

	def getEntityWrapper(Attribute attr) {
		if (attr.container instanceof Type)
			return (attr.container as Type).name
		return "UNDEF"
	}

	def getCompareOperator(CompareOperator co) {
		switch (co) {
			case co instanceof IntegerCompareOperator:
				return getOperatorMethodName((co as IntegerCompareOperator).operator.getName())
			case co instanceof RealCompareOperator:
				return getOperatorMethodName((co as RealCompareOperator).operator.getName())
			case co instanceof TextCompareOperator:
				return getOperatorMethodName((co as TextCompareOperator).operator.getName())
			case co instanceof BooleanCompareOperator:
				return getOperatorMethodName((co as BooleanCompareOperator).operator.getName())
			case co instanceof TimestampCompareOperator:
				return getOperatorMethodName((co as TimestampCompareOperator).operator.getName())
		}
		return "UNDEF"
	}
}
