package info.scce.dime.generator.livevariable

import info.scce.dime.dad.dad.GenerationMode
import info.scce.dime.generator.gui.DataAccessType
import info.scce.dime.generator.gui.MODE
import info.scce.dime.generator.gui.dart.DataDartHelper
import info.scce.dime.generator.gui.rest.model.ComplexFieldView
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.gui.utils.GUIGenerator
import info.scce.dime.gui.gui.ComplexVariable
import java.util.Set
import java.util.stream.Collectors

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

/**
 * Template providing methods for live variables 
 * e.g. generating WebSocket variables etc. for 
 * angular parts.
 */
class LiveVariableDartTemplate extends GUIGenerator {
	
	private LiveVariableManager variableManager;
	private GUICompoundView guiCompoundView;

	new(GUICompoundView gcv) {
		super()
		guiCompoundView = gcv;
		variableManager = new LiveVariableManager(gcv);
	}
	
	/**
	 * Will return true, if there is at least one
	 * live variable for the given GUI Compound View.
	 */
	public def boolean hasLiveVariables() {
		variableManager.hasLiveVariables;
	}
	
	/**
	 * Generates imports for WebSockets.
	 */
	public def generateImports() '''
	'''

	/**
	 * Generates the declaration of WebSockets.
	 */
	public def generateVariablesDeclaration() '''
«FOR variable : variableManager.candidateVariables»
html.WebSocket «variable.websocketName»;	// WebSocket for «variable.name»
«ENDFOR»
bool isDestroyed = true;
	'''
	
	/**
	 * Generates the instantiation for WebSocket 
	 * variables, excluding the current user.
	 */
	public def generateOpenWebsockets() {
		val candidates = variableManager.candidateVariables.stream.filter[e | !e.name.equalsIgnoreCase("currentUser")].collect(Collectors.toSet)
	'''
	void openWebsockets() {
		«FOR variable : candidates»
		this._open«variable.websocketName»();
		«ENDFOR»	
	}
	
	«candidates.generateWebsocketMethods»
	'''
	
	}
	
//	/**
//	 * Generates the instantiation for WebSocket 
//	 * for the current user.
//	 */
//	public def generateOpenWebsocketCurrentUser() '''
//«FOR variable : variableManager.candidateVariables.stream.filter[e | e.name.equalsIgnoreCase("currentUser")].collect(Collectors.toSet)»
//«variable.generateOpenWebsocket»
//«ENDFOR»	
//	'''
	
	/**
	 * Generates close of WebSockets.
	 */
	public def generateOnDestroy() '''
	«FOR variable : variableManager.candidateVariables»
	«variable.closeWebsocket»
	«ENDFOR»
	'''
	
	def closeWebsocket(ComplexVariable variable)
	'''
	if(this.«variable.websocketName» != null && this.«variable.websocketName».readyState == html.WebSocket.OPEN) {
		html.window.console.debug("Closing Websocket «variable.websocketName»");
		this.«variable.websocketName».close();
		this.«variable.websocketName» = null;
	}
	'''
	
	/**
	 * Generates close of WebSockets.
	 */
	public def generateOnInit() '''
this.isDestroyed = false;
	'''
	
	/**
	 * Generates the opening for a WebSocket.
	 */
	private def generateOpenWebsocket(ComplexVariable variable,Set<ComplexVariable> candidates) {
		val protocol = if (isModeProduction) "wss" else "ws"
		val port = LiveVariableGenerator.getPort(variable.rootElement)
		'''
		if (this.«variable.pathFromRoot» != null && this.«variable.websocketName» == null) {
			try {
			this.«variable.websocketName» = new html.WebSocket('«protocol»://${html.window.location.hostname}:«port»/«variable.id»/${«variable.pathFromRoot».dywa_id}');
			
			// Callbacks for «variable.name.escapeDart»
			«generateOnOpenCallback(variable)»
			«generateOnMessageCallback(variable,candidates)»
			«generateOnCloseCallback(variable)»
			«generateOnErrorCallback(variable)»
			} catch(e) {
				print("Websocket for «variable.name.escapeDart» could not be created");
			}
		}
		'''
	}
	
	private def generateWebsocketMethods(Set<ComplexVariable> candidates)
	'''
	«FOR variable:candidates»
	void _open«variable.websocketName»({bool close:false}) {
		if(close) {
			«variable.closeWebsocket»
		}
		«generateOpenWebsocket(variable,candidates)»
	}
	«ENDFOR»
	'''
	
	/**
	 * Generates the onOpen callback for a WebSocket.
	 */
	private def generateOnOpenCallback(ComplexVariable variable)
	'''
	this.«variable.websocketName».onOpen.listen((e) {
		html.window.console.debug("onOpen Websocket «variable.websocketName»");
	});
	'''
	
	/**
	 * Generates the onMessage callback for a WebSocket.
	 * Includes logic for transforming the send object
	 * from JSON.
	 */
	private def generateOnMessageCallback(ComplexVariable variable,Set<ComplexVariable> candidates)
	{
		val ctv = (guiCompoundView.pairs.get(variable) as ComplexTypeView)
		val complexAttributes = ctv.displayedFields.filter(ComplexFieldView).map[view].filter(ComplexTypeView).map[data]
		'''
		this.«variable.websocketName».onMessage.listen((html.MessageEvent e) {
			html.window.console.debug("onMessage Websocket «variable.websocketName»");
			if(!this.isDestroyed && e.data != null) {
				«FOR liveAttribute:candidates.filter[!isIsList].filter[n|complexAttributes.exists[id.equals(n.id)]]»
				var tmp_«liveAttribute.name.escapeDart»_id = «variable.pathFromRoot»?.«liveAttribute.name.escapeDart»?.dywa_id;
				«ENDFOR»
				«variable.pathFromRoot» = «variable.rootElement.id.escapeDart».«ctv.selectiveClassName(false)».fromJSON(e.data);
				«FOR liveAttribute:candidates.filter[!isIsList].filter[n|complexAttributes.exists[id.equals(n.id)]]»
				if(tmp_«liveAttribute.name.escapeDart»_id!=null&&«variable.pathFromRoot»?.«liveAttribute.name.escapeDart»!=null) {
					if(tmp_«liveAttribute.name.escapeDart»_id!=«variable.pathFromRoot».«liveAttribute.name.escapeDart».dywa_id) {
						this._open«liveAttribute.websocketName»(close:true);
					}
				}
				«ENDFOR»
			}
		});
		'''
	}
	/**
	 * Generates the onClose callback for a WebSocket.
	 */
	private def generateOnCloseCallback(ComplexVariable variable)
	'''
	this.«variable.websocketName».onClose.listen((html.CloseEvent e) {
		html.window.console.debug("onClose Websocket «variable.websocketName»: ${e.reason} (Code ${e.code}).");
		if(!this.isDestroyed && e.code == «LiveVariableWebsocketTemplate.DELETED_CLOSE_CODE») {
			html.window.console.debug("«variable.name» deleted.");
			«variable.pathFromRoot» = null;
		}
	});
	'''
	
	/**
	 * Generates the onError callback for a WebSocket.
	 */
	private def generateOnErrorCallback(ComplexVariable variable)
	'''
	this.«variable.websocketName».onError.listen((e) {
		html.window.console.debug("Error on Websocket «variable.websocketName»: ${e.toString()}");
	});
	'''
	
	/**
	 * Generates the name of a WebSocket for a given variable.
	 */
	private def getWebsocketName(ComplexVariable variable) '''webSocket«variable.name.escapeDart.toFirstUpper»'''
	
	/**
	 * Generates the path from the root to a given variable.
	 * Example: this.currentUser.todoList for the root 'currentUser'
	 * 			and the candidate 'todoList' as 'todoList' is an attribute
	 * 			of the current user.
	 */
	//private def getPathFromRoot(ComplexVariable candidate)'''this.«variableManager.getRoot(candidate).name.escapeDart»«FOR hop : variableManager.getPathFromRootToCandidate(candidate)».«hop.name.escapeDart»«ENDFOR»'''
	private def getPathFromRoot(ComplexVariable variable)
	{
		val gui = variable.rootElement
		DataDartHelper.getVariableDataName(variable, gui, DataAccessType.^IF, MODE.GET)
	}
	
	private def isModeProduction() {
		genctx.dad.mode == GenerationMode.PRODUCTION
	}
}