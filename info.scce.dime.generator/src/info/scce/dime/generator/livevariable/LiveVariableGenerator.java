package info.scce.dime.generator.livevariable;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;
import info.scce.dime.dad.dad.DAD;
import info.scce.dime.generator.gui.rest.model.GUICompoundView;
import info.scce.dime.gui.gui.GUI;

public class LiveVariableGenerator implements IGenerator<DAD> {

	public static String pkg = "info.scce.dime";
	
	private static Map<String, Integer> portMap = new HashMap<>();
	private static Integer nextPort = 50000;
	
	private Set<GUICompoundView> guiCompundViews;
	
	public LiveVariableGenerator(Set<GUICompoundView> guiCompundViews) {
		this.guiCompundViews = guiCompundViews;
	}
	
	@Override
	public void generate(DAD dad, IPath path, IProgressMonitor progressMonitor) {
		System.out.println("[INFO] Generating live variable components.");
		LiveVariableManager variableManager = new LiveVariableManager(guiCompundViews);
		for(LiveVariableContainer container : variableManager.getLiveVariableContainers()) {
			System.out.println("[INFO] " + container);
		}
		// Disable generation since LiveVariable Featuer is not working with native schema 				
//		EventHandlerTemplate eventHandlerTemplate = new EventHandlerTemplate(guiCompundViews);
//		EventHandlerRegistryBeanTemplate eventHandlerRegistryBeanTemplate = new EventHandlerRegistryBeanTemplate();
//		
//		// Generate EventHandler.
//		writeToFile(
//			EventHandlerTemplate.getClassName() + ".java",
//			path, 
//			eventHandlerTemplate.generate(pkg, variableManager)
//		);
//		// Generate RegistryBean
//		writeToFile(
//			EventHandlerRegistryBeanTemplate.getClassName() + ".java",
//			path,
//			eventHandlerRegistryBeanTemplate.generate(pkg)
//		);
		
		// Generate WebSocket Class for each GUI.
		for(GUI gui : variableManager.getGUIs()) {
			writeToFile(
				getWebsocketType(gui) + ".java", 
				path, 
				LiveVariableWebsocketTemplate.generate(pkg, getWebsocketType(gui), gui, variableManager)
			);
		}
		System.out.println("[INFO] Live variable components generated.");
	}
	
	private void writeToFile(String fileName, IPath rootPath, CharSequence contents) {
		if(contents != null) {
			IPath outputFolder = rootPath
				.append("app-business")
				.append("target")
				.append("generated-sources");
			
			for(String pkgPart : pkg.split("\\.")) {
				outputFolder = outputFolder.append(pkgPart);
			}
			
			try {
				File file = new File(outputFolder + "/" + fileName);
				file.getParentFile().mkdirs(); 
				file.createNewFile();
				FileUtils.writeStringToFile(file, contents.toString());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			
		}
	}

	public static Integer getPort(GUI gui) {
		if(!portMap.containsKey(gui.getTitle())) {
			portMap.put(gui.getTitle(), nextPort++);
		}
		
		return portMap.get(gui.getTitle());
	}
	
	public static String getWebsocketType(GUI gui) {
		return String.format("%sWebSocket", gui.getTitle());
	}
}
