package info.scce.dime.generator.livevariable

import info.scce.dime.data.data.Type
import info.scce.dime.generator.gui.rest.DyWASelectiveDartGenerator
import info.scce.dime.generator.gui.rest.model.ComplexTypeView
import info.scce.dime.generator.gui.rest.model.GUICompoundView
import info.scce.dime.generator.util.DyWAExtension
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUI
import java.util.HashSet
import java.util.Set

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

/**
 * Template generating the DyWAEventHandler and
 * parts of live variables e.g. sending updated
 * variables.
 */
class EventHandlerTemplate
{
	val extension DyWAExtension = new DyWAExtension
	private Set<GUICompoundView> guiCompundViews;

	new(Set<GUICompoundView> guiCompundViews) {
		this.guiCompundViews = guiCompundViews;
	}
	
	public static def getClassName() '''LiveVariableEventHandler'''
	
	/**
	 * Generates the EventHandler, which distributes live variables
	 * to the connected clients.
	 */
	public def generate(String pkg, LiveVariableManager variableManager) '''

package «pkg»;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.ls5.dywa.adapter.events.EventHandler;
import de.ls5.dywa.entities.events.field.PostPersistFieldEvent;
import de.ls5.dywa.entities.events.field.PostRemoveFieldEvent;
import de.ls5.dywa.entities.events.field.PostUpdateFieldEvent;
import de.ls5.dywa.entities.events.field.PrePersistFieldEvent;
import de.ls5.dywa.entities.events.field.PreRemoveFieldEvent;
import de.ls5.dywa.entities.events.field.PreUpdateFieldEvent;
import de.ls5.dywa.entities.events.object.PostPersistObjectEvent;
import de.ls5.dywa.entities.events.object.PostRemoveObjectEvent;
import de.ls5.dywa.entities.events.object.PostUpdateObjectEvent;
import de.ls5.dywa.entities.events.object.PrePersistObjectEvent;
import de.ls5.dywa.entities.events.object.PreRemoveObjectEvent;
import de.ls5.dywa.entities.events.object.PreUpdateObjectEvent;
import de.ls5.dywa.entities.events.property.PostPersistPropertyEvent;
import de.ls5.dywa.entities.events.property.PostRemovePropertyEvent;
import de.ls5.dywa.entities.events.property.PostUpdatePropertyEvent;
import de.ls5.dywa.entities.events.property.PrePersistPropertyEvent;
import de.ls5.dywa.entities.events.property.PreRemovePropertyEvent;
import de.ls5.dywa.entities.events.property.PreUpdatePropertyEvent;
import de.ls5.dywa.entities.events.type.PostPersistTypeEvent;
import de.ls5.dywa.entities.events.type.PostRemoveTypeEvent;
import de.ls5.dywa.entities.events.type.PostUpdateTypeEvent;
import de.ls5.dywa.entities.events.type.PrePersistTypeEvent;
import de.ls5.dywa.entities.events.type.PreRemoveTypeEvent;
import de.ls5.dywa.entities.events.type.PreUpdateTypeEvent;
import de.ls5.dywa.entities.object.DBObject;
import de.ls5.dywa.annotations.IdRef;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import info.scce.dime.livevariable.RequestObserver;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.servlet.ServletRequest;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Optional;

«FOR type : variableManager.allTypes»
import «type.dyWATypeName»;
«ENDFOR»
«FOR type : variableManager.candidateVariableTypes»
import de.ls5.dywa.generated.rest.controller.«type.name»REST;
import «type.getControllerTypeName»;
«ENDFOR»
import info.scce.dime.rest.DIMESelectiveRestFilter;
import info.scce.dime.rest.ObjectCache;

@Singleton
public class «className» implements EventHandler«IF (variableManager.hasLiveVariables)», RequestObserver«ENDIF» {
	«FOR type : variableManager.candidateVariableTypes»
	/**
	 * REST Controller for sending updated «type.name» objects.
	 */
	@Inject
	private «type.name»REST «type.generateTypeRestControllerName»;
	
	/**
	 * Controller for reading updated «type.name» objects.
	 */
	@Inject
	private «type.name»Controller «type.generateTypeControllerName»;
	
	/**
	 * Queue for collecting «type.name»s while 
	 * pre updates and sending on post updates.
	 */
	private Set<Long> «type.generateQueueName» = new HashSet<>();
	
	/**
	 * Queue for collecting deletions of «type.name» 
	 * while pre updates and sending on post updates.
	 */
	private Set<Long> «type.generateDeletionQueueName» = new HashSet<>();
	«ENDFOR»
	
	«FOR gui : variableManager.getGUIs»
	/**
	 * WebSocket for sending updated objects on GUI «gui.title».
	 */
	@Inject
	private «LiveVariableGenerator.getWebsocketType(gui)» «gui.generateWebSocketName»;
	«ENDFOR»
	
	«IF variableManager.hasLiveVariables»
	/**
	 * Mapper for get JSON of updated objects.
	 */
	ObjectMapper mapper;
	
	
	public «className»()
	{
		mapper = info.scce.dime.rest.JacksonProvider.getPreconfiguredMapper();
	}

	public void afterRequest(@Observes @Destroyed(RequestScoped.class) ServletRequest payload) {
		«FOR container : variableManager.getLiveVariableContainers()»
			«IF container.candidateVariable.complexTypeView != null»
			// Send all «container.candidateVariable.name» to the listeners.
			for(Long id : «container.candidateVariable.dataType.generateQueueName») {
				«container.generateCandidateSendFunctionName»(id);
			}
			«container.candidateVariable.dataType.generateQueueName».clear();
			
			// Close all WebSockets, listening on deleted objects of «container.candidateVariable.name».
			for(Long id : «container.candidateVariable.dataType.generateDeletionQueueName») {
				«container.GUI.generateWebSocketName».«LiveVariableWebsocketTemplate.functionNameCloseAfterDeletion(container.candidateVariable)»(id);
			}
			«container.candidateVariable.dataType.generateDeletionQueueName».clear();
			«ENDIF»
		«ENDFOR»
	}
	«ENDIF»

	@Override
	public void handle(PreRemoveObjectEvent event) {
		«generatePreUpdate(variableManager, 'Object')»
	}

	@Override
	public void handle(PostRemoveObjectEvent event) {
		«generatePostUpdate(variableManager, 'Object')»
	}

	@Override
	public void handle(PreUpdatePropertyEvent event) {
		«generatePreUpdate(variableManager, 'Owner')»
	}

	@Override
	public void handle(PostUpdatePropertyEvent event) {
		«generatePostUpdate(variableManager, 'Owner')»
	}

	@Override
	public void handle(PreRemovePropertyEvent event) {
		«generatePreUpdate(variableManager, 'Owner')»
	}

	@Override
	public void handle(PostRemovePropertyEvent event) {
		«generatePostUpdate(variableManager, 'Owner')»
	}
	
	«FOR container : variableManager.getLiveVariableContainers»
	«val viewType = container.candidateVariable.complexTypeView»
	«IF viewType != null»
	/**
	 * Called, when «container.liveVariable.name» of GUI «container.GUI.title» is updated.
	 * Collects the id of updated object of «container.candidateVariable.name»
	 */
	private void «container.generateUpdateLiveVariableFunctionName»(long liveVariableId) {
		System.out.println("Collecting candidate for live variable «container.liveVariable.name» with id " + liveVariableId + ".");
		«IF container.candidate»
		// Live variable can send directly to listeners for «container.liveVariable.name».
		long «container.genereateCandidateIdName» = liveVariableId;
		«container.candidateVariable.dataType.name» «container.candidateVariable.generateVariableName» = «container.candidateVariable.dataType.generateTypeControllerName».read(«container.genereateCandidateIdName»);
		if(«container.candidateVariable.generateVariableName» == null) {
			System.out.println("Candidate «container.candidateVariable.name»#" + liveVariableId + " is null -> deleted.");
			«container.candidateVariable.dataType.generateDeletionQueueName».add(«container.genereateCandidateIdName»);
			return;
		}
		«ELSE»
		// Need to find id for listeners: «container.liveVariable.name» is updated, «container.candidateVariable.name» will be send.
		long «container.genereateCandidateIdName» = -1;
		
		// Set of all «container.candidateVariable.name» instance ids with listeners.
		Set<Long> «container.genereateCandidateIdName»s = «container.candidateVariable.rootElement.generateWebSocketName».«LiveVariableWebsocketTemplate.functionNameGetDywaIds(container.candidateVariable)»();
		System.out.println("WebSocket «container.candidateVariable.rootElement.generateWebSocketName» has " + «container.genereateCandidateIdName»s.size() + " listeners.");
		
		// Get objects for «container.genereateCandidateIdName»s.
		final Map<Long, «container.candidateVariable.dataType.name»> «container.candidateVariable.generateVariableName»s = new HashMap<>();
		«container.genereateCandidateIdName»s.forEach(id -> «container.candidateVariable.generateVariableName»s.put(id, «container.candidateVariable.dataType.generateTypeControllerName».read(id)));
		
		// Find id of «container.candidateVariable.name» which will be send.
		for(Long tmp«container.genereateCandidateIdName» : «container.candidateVariable.generateVariableName»s.keySet()) {
			«container.candidateVariable.dataType.name» «container.candidateVariable.generateVariableName» = «container.candidateVariable.generateVariableName»s.get(tmp«container.genereateCandidateIdName»);
			
			// Candidate variable is null -> sync deletion.
			if(«container.candidateVariable.generateVariableName» == null) {
				System.out.println("Candidate «container.candidateVariable.name»#" + tmp«container.genereateCandidateIdName» + " for «container.liveVariable.name» with id " + liveVariableId + " is null -> deleted.");
				«container.candidateVariable.dataType.generateDeletionQueueName».add(tmp«container.genereateCandidateIdName»);
			} else {
				System.out.println("Checking candidate «container.candidateVariable.name»#" + «container.candidateVariable.generateVariableName».getDywaId() + " for «container.liveVariable.name» with id " + liveVariableId + ".");
				«generateIdSearch(container.genereateCandidateIdName.toString, container)»
			}
		}
		«ENDIF»
		
		// Couldn't find the right id -> objecs may be deleted. Send all.
		if(«container.genereateCandidateIdName» > -1) {
			System.out.println("Found candidate id " + «container.genereateCandidateIdName» + " -> collecting.");
			«container.candidateVariable.dataType.generateQueueName».add(«container.genereateCandidateIdName»);
		} else if(«container.genereateCandidateIdName» < 0) {
			System.out.println("Candidate «container.candidateVariable.name» of live variable «container.liveVariable.name» not found.");
			«IF container.candidate»
			«container.candidateVariable.dataType.generateQueueName».add(«container.genereateCandidateIdName»);
			«ELSE»
			«container.candidateVariable.dataType.generateQueueName».addAll(«container.genereateCandidateIdName»s);
			«ENDIF»
		}
	}
	«ENDIF»
	«ENDFOR»
	
	«val createdSendFunctions = new HashSet<String>()»
	«FOR container : variableManager.liveVariableContainers»
	«val viewType = container.candidateVariable.complexTypeView»
	«IF viewType != null && !createdSendFunctions.contains(container.generateCandidateSendFunctionName.toString)»
	«{ createdSendFunctions.add(container.generateCandidateSendFunctionName.toString); "" }»
	/**
	 * Sends «container.candidateVariable.name» to all listeners of GUI «container.GUI.title».
	 * @param id	Id of the «container.candidateVariable.name» object.
	 */
	public void «container.generateCandidateSendFunctionName»(long id)
	{
		try {
			System.out.println("Sending «container.candidateVariable.name» with id " + id + " to listeners of GUI «container.GUI.title».");
			«container.candidateVariable.dataType.name.escapeJava» obj = «container.candidateVariable.dataType.name.toLowerCase.escapeJava»Controller.read(id);
			de.ls5.dywa.generated.rest.types.«container.candidateVariable.dataType.name.escapeJava» rest = de.ls5.dywa.generated.rest.types.«container.candidateVariable.dataType.name.escapeJava»
					.fromDywaEntity(obj, new ObjectCache());
			de.ls5.dywa.generated.rest.types.«DyWASelectiveDartGenerator.getSelectiveNameJava(viewType)»
					.copy(obj, rest, new ObjectCache());
			«container.GUI.generateWebSocketName».«LiveVariableWebsocketTemplate.functionNameSendToPerspectiveCustomer(container.candidateVariable)»(
				id,
				mapper.writeValueAsString(rest)
			);
		} catch(JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	«ENDIF»
	«ENDFOR»
}
	'''	
	
	/**
	 * Generates the content for trigger updates.
	 * 
	 * @param variableManager
	 * @param eventAttribute	Name of the events object attribute (e.g. 'Owner' or 'Object').
	 */
	private def generatePreUpdate(LiveVariableManager variableManager, String eventAttribute) '''
«IF variableManager.hasLiveVariables»
	if(event != null && event.get«eventAttribute.toFirstUpper»() != null) {
		DBObject dbObject = event.get«eventAttribute.toFirstUpper»();
		«FOR type : variableManager.liveVariableTypes SEPARATOR " else "»
		if(dbObject.getType().getId() == «type.name».class.getAnnotation(IdRef.class).id()){
			«FOR container : variableManager.getLiveVariableContainersByLiveVariableType(type)»
				«IF container.candidateVariable.complexTypeView != null»
				// Collect candidates for live variable «container.liveVariable.name»
				«container.generateUpdateLiveVariableFunctionName»(dbObject.getId());
				«ENDIF»
			«ENDFOR»
		}
		«ENDFOR»
	}
«ENDIF»
	'''
	
	/**
	 * Generates the content for trigger updates.
	 * 
	 * @param variableManager
	 * @param eventAttribute	Name of the events object attribute (e.g. 'Owner' or 'Object').
	 */
	private def generatePostUpdate(LiveVariableManager variableManager, String eventAttribute) '''
«««		«IF variableManager.hasLiveVariables»
«««			if(event != null && event.get«eventAttribute.toFirstUpper»() != null) {
«««				DBObject dbObject = event.get«eventAttribute.toFirstUpper»();
«««				«FOR type : variableManager.liveVariableTypes SEPARATOR " else "»
«««				if(dbObject.getType().getId() == «type.name».class.getAnnotation(IdRef.class).id()){
«««					«FOR container : variableManager.getLiveVariableContainersByLiveVariableType(type)»
«««					«IF container.candidateVariable.complexTypeView != null»
«««					// Send all «container.candidateVariable.name» to the listeners.
«««					for(Long id : «container.candidateVariable.dataType.generateQueueName») {
«««						«container.generateCandidateSendFunctionName»(id);
«««					}
«««					«container.candidateVariable.dataType.generateQueueName».clear();
«««					
«««					// Close all WebSockets, listening on deleted objects of «container.candidateVariable.name».
«««					for(Long id : «container.candidateVariable.dataType.generateDeletionQueueName») {
«««						«container.GUI.generateWebSocketName».«LiveVariableWebsocketTemplate.functionNameCloseAfterDeletion(container.candidateVariable)»(id);
«««					}
«««					«container.candidateVariable.dataType.generateDeletionQueueName».clear();
«««					«ENDIF»
«««					«ENDFOR»
«««				}
«««				«ENDFOR»
«««			}
«««		«ENDIF»
	'''
	
	/**
	 * Generates the name of the WebSocket variable for a given GUI.
	 */
	private def generateWebSocketName(GUI gui) '''«gui.title.toFirstLower»WebSocket'''
	
	/**
	 * Generates the name of the REST controller for a given type.
	 */
	private def generateTypeRestControllerName(Type type) '''«type.name.toLowerCase»REST'''
	
	/**
	 * Generates the controller name for a given type.
	 */
	private def generateTypeControllerName(Type type) '''«type.name.toLowerCase»Controller'''
	
	/**
	 * Generates the queue name for a given type.
	 */
	private def generateQueueName(Type type) '''«type.name.toLowerCase»IdQueue'''
	
	/**
	 * Generates the deletions queue name for a given type.
	 */
	private def generateDeletionQueueName(Type type) '''«type.name.toLowerCase»IdDeletionQueue'''
	
	/**
	 * Generates the name for a given variable, including the variable name and 
	 * the name of the data type.
	 */
	private def generateVariableName(ComplexVariable variable) '''«variable.name.toFirstLower»«variable.dataType.name.toFirstUpper»'''
	
	/**
	 * Generates the name for the function, which will
	 * send the updated candidate of a live variable.
	 */
	private def generateUpdateLiveVariableFunctionName(LiveVariableContainer container) '''collect«container.GUI.title.toFirstUpper»«container.liveVariable.id.escapeJava»«container.liveVariable.name.toFirstUpper»'''
	
	/**
	 * Generates the name for the candidate id, which is set at the id lookup.
	 */
	private def genereateCandidateIdName(LiveVariableContainer container) '''«container.candidateVariable.name.toFirstLower»Id'''
	
	/**
	 * Generates the name of the sending function for a live variable container.
	 */
	private def generateCandidateSendFunctionName(LiveVariableContainer container) '''send«container.GUI.title.toFirstUpper»«container.candidateVariable.name.toFirstUpper»'''
	
	/**
	 * Generates the id lookup to find the right instance of the candidate variable
	 * for the given id of the updated object (which is not the candidate, but a 
	 * successor the that).
	 * 
	 * @param idVariableName	Name of the candidate id variable.
	 * @param container			The container which holds candidate and live
	 * 							variables and the path from candidate to the live.
	 */
	private def generateIdSearch(String idVariableName, LiveVariableContainer container)
	'''
	«var last = container.candidateVariable»
	«var isStreamed = false»
«IF container.hasListInPath»Optional<Long>«ELSE»long«ENDIF» foundId = «container.candidateVariable.generateVariableName»
«FOR next : container.pathFromCandidateToLive»
«IF next.isIsList»
	«IF isStreamed == false»
«««		.get«next.name»()
		.get«next.name»_«next.dataType.name»()
		.stream()
		«{ isStreamed = true; "" }»
	«ELSE»
		.flatMap(«last.name» -> «last.name».get«next.name»_«next.dataType.name»().stream())
«««		.flatMap(«last.name» -> «last.name».get«next.name»().stream())
	«ENDIF»
«ELSE»
	«IF isStreamed == false»
		.get«next.name»_«next.dataType.name»()
	«ELSE»
«««		.map(«last.name» -> «last.name».get«next.name»_«next.dataType.name»())
		.map(«last.name» -> «last.name».get«next.name»())
	«ENDIF»
«ENDIF»
«{ last = next; "" }»
«ENDFOR»
«IF container.hasListInPath == false»
		.getDywaId();
	
	if(foundId == liveVariableId) {
		System.out.println("Testing: «container.liveVariable.name»#" + foundId + " -> true");
		«idVariableName» = «container.candidateVariable.generateVariableName».getDywaId();
		break;
	}
	System.out.println("Testing: «container.liveVariable.name»#" + foundId + " -> false");
	«ELSE»
		.map(«last.name» -> «last.name».getDywaId())
		.filter(dywaId -> {
			System.out.println("Testing List: «container.liveVariable.name»#" + dywaId + " -> " + (dywaId == liveVariableId ? "true" : "false"));
			return dywaId == liveVariableId;
		})
		.findAny();

	if(foundId.isPresent()) {
		«idVariableName» = «container.candidateVariable.generateVariableName».getDywaId();
		break;
	}
«ENDIF»
	'''
	
	/**
	 * @return 	The complex type view of a given variable
	 */
	private def ComplexTypeView getComplexTypeView(ComplexVariable variable) {
		val gcv = guiCompundViews.stream.filter[c | c.cgui.equals(variable.rootElement)].findAny.get;
		if(gcv.pairs.containsKey(variable) && gcv.pairs.get(variable) instanceof ComplexTypeView) {
			return gcv.pairs.get(variable) as ComplexTypeView;			
		}

		return null;	
	}
	
	/**
	 * @return	True, if there is at least one list at the containers path from candidate to live variable.
	 */
	private def boolean hasListInPath(LiveVariableContainer container) {
		return container.pathFromCandidateToLive.stream.filter[isIsList].count > 0;
	}
}