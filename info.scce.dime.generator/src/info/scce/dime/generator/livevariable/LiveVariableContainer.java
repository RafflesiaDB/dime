package info.scce.dime.generator.livevariable;

import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.GUI;

import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 *	Container for live variables,
 *	including the variable itself,
 *	the live candidate and the path
 *	from the root variable, ending at 
 *	the candidate.
 */
public class LiveVariableContainer 
{
	private ComplexVariable liveVariable;
	private ComplexVariable candidateVariable;
	private LinkedList<ComplexVariable> pathFromCandidateToLive;
	
	public LiveVariableContainer(ComplexVariable liveVariable, ComplexVariable candidateVariable, LinkedList<ComplexVariable> pathFromCandidateToLive)
	{
		this.liveVariable = liveVariable;
		this.candidateVariable = candidateVariable;
		this.pathFromCandidateToLive = pathFromCandidateToLive;
	}

	/**
	 * @return	Variable, which is marked as live.
	 */
	public ComplexVariable getLiveVariable() 
	{
		return liveVariable;
	}

	/**
	 * @return	Parent of the highest list in the
	 * 			Tree upwards the live variable.
	 * 			If there is no list upwards, 
	 * 			the candidate is the live variable himself.
	 */
	public ComplexVariable getCandidateVariable()
	{
		return candidateVariable;
	}
	
	/**
	 * @return	Path from the candidate variable to the live variable.
	 * 			Empty, if the live variable is the candidate.
	 */
	public LinkedList<ComplexVariable> getPathFromCandidateToLive()
	{
		return pathFromCandidateToLive;
	}
	
	/**
	 * Checks the live variable is the candidate.
	 * 
	 * @return	True, if the live variable is the candidate.
	 */
	public boolean isCandidate()
	{
		return pathFromCandidateToLive == null || pathFromCandidateToLive.isEmpty();
	}
	
	/**
	 * @return	True, if this container has a candidate.
	 */
	public boolean hasCandidate()
	{
		return candidateVariable != null;
	}
	
	/**
	 * @return	GUI of the live variable.
	 */
	public GUI getGUI()
	{
		return liveVariable.getRootElement();
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null || !(other instanceof LiveVariableContainer)) {
			return false;
		}
		
		return ((LiveVariableContainer) other).getLiveVariable().equals(this);
	}
	
	@Override
	public String toString()
	{
		// [root -> b -> c]
		String pathFromCandidate = "[" + String.join(" -> ", pathFromCandidateToLive.stream().map(v -> v.getName()).collect(Collectors.toList())) + "]";
		
		return String.format(
			"GUI: %s \tLive: %s \tCandidate: %s \tCandidate -> Live: %s", 
			getGUI().getTitle(), 
			liveVariable.getName(),
			candidateVariable.getName(),
			pathFromCandidate
		);
	}
}
