package info.scce.dime.generator.livevariable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import info.scce.dime.data.data.Type;
import info.scce.dime.generator.gui.rest.model.GUICompoundView;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.PrimitiveAttribute;

/**
 *	Manages live variables:
 *	  - Extracts them from GUI Compound Views.
 *	  - Provides methods to collect all candidate variables, types,
 *		live variables and root variables, GUIs with live variables.
 */
public class LiveVariableManager 
{
	private Set<LiveVariableContainer> liveVariableContainers;
	
	public LiveVariableManager(GUICompoundView guiCompoundView)
	{
		liveVariableContainers = createLiveContainers(guiCompoundView);
	}
	
	public LiveVariableManager(Set<GUICompoundView> guiCompoundViews)
	{
		liveVariableContainers = createLiveContainers(guiCompoundViews);
	}
	
	/**
	 * @return	All Containers for live variables (including the live variable,
	 * 			her candidate and the path from the candidate to the live variable).
	 */
	public Set<LiveVariableContainer> getLiveVariableContainers()
	{
		return liveVariableContainers;
	}
	
	/**
	 * @return	All GUIs, which have live variables.
	 */
	public Set<GUI> getGUIs()
	{
		Map<String,GUI> guis = new HashMap<String,GUI>();
		liveVariableContainers
		.stream()
		.map(c -> c.getLiveVariable().getRootElement()).forEach((n)->{
			if(!guis.containsKey(n.getId())){
				guis.put(n.getId(), n);
			}
		});
		return guis.values().stream().collect(Collectors.toSet());
	}
	
	/**
	 * @return	Types of all candidate variables.
	 */
	public Set<Type> getCandidateVariableTypes()
	{
		Map<String,Type> types = new HashMap<String,Type>();
		liveVariableContainers
		.stream()
		.map(c -> c.getCandidateVariable().getDataType()).forEach((n)->{
			if(!types.containsKey(n.getId())){
				types.put(n.getId(), n);
			}
		});
		return types.values().stream().collect(Collectors.toSet());
	}
	
	/**
	 * @return	Types of all live variables.
	 */
	public Set<Type> getLiveVariableTypes()
	{
		Map<String,Type> types = new HashMap<String,Type>();
		liveVariableContainers
		.stream()
		.map(c -> c.getLiveVariable().getDataType()).forEach((n)->{
			if(!types.containsKey(n.getId())){
				types.put(n.getId(), n);
			}
		});
		return types.values().stream().collect(Collectors.toSet());
	}
	
	/**
	 * @return	Types of live and candidate variables.
	 */
	public Set<Type> getAllTypes()
	{
		Set<Type> types = new HashSet<Type>();
		types.addAll(getLiveVariableTypes());
		types.addAll(getCandidateVariableTypes());
		
		return types;
	}
	
	/**
	 * @return	True, if there is at least one live variable.
	 */
	public boolean hasLiveVariables()
	{
		return !liveVariableContainers.isEmpty();
	}
	
	/**
	 * Collects all containers which includes a live variable
	 * with the given type.
	 * 
	 * @param type
	 * @return
	 */
	public Set<LiveVariableContainer> getLiveVariableContainersByLiveVariableType(Type type)
	{
		return liveVariableContainers
		.stream()
		.filter(c -> c.getLiveVariable().getDataType().equals(type))
		.collect(Collectors.toSet());
	}
	
	
	/**
	 * @return	All candidate variables.
	 */
	public Set<ComplexVariable> getCandidateVariables()
	{
		Map<String,ComplexVariable> variables = new HashMap<String,ComplexVariable>();
		liveVariableContainers
		.stream()
		.map(c -> c.getCandidateVariable()).forEach((n)->{
			if(!variables.containsKey(n.getId())){
				variables.put(n.getId(), n);
			}
		});
		return variables.values().stream().collect(Collectors.toSet());

	}
	
	/**
	 * @param	GUI.
	 * @return	All candidate variables for the given GUI.
	 */
	public Set<ComplexVariable> getCandidateVariables(GUI gui)
	{
		Map<String,ComplexVariable> variables = new HashMap<String,ComplexVariable>();
		liveVariableContainers
		.stream()
		.filter(c -> c.getLiveVariable().getRootElement().getTitle().equals(gui.getTitle()))
		.map(c -> c.getCandidateVariable()).forEach((n)->{
			if(!variables.containsKey(n.getId())){
				variables.put(n.getId(), n);
			}
		});
		return variables.values().stream().collect(Collectors.toSet());
	}
	
	/**
	 * Creates a path, starting at the root variable of the given candidate
	 * and ends at the parent of the given candidate variable.
	 * 
	 * @param candidateVariable
	 * @return
	 */
	public LinkedList<ComplexVariable> getPathFromRootToCandidate(ComplexVariable candidateVariable)
	{
		LinkedList<ComplexVariable> path = new LinkedList<>();
		if(candidateVariable == null) {
			return path;
		}
		
		ComplexVariable current = candidateVariable;
		while((current = getParent(current)) != null) {
			if(!isListAttribute(current)) {
				path.offerFirst(current);
			}
		}
		
		return path;
	}
	
	/**
	 * Checks the variable as a root variable.
	 * 
	 * @param candidateVariable
	 * @return	True, of the variable is a root variable.
	 */
	public boolean isRoot(ComplexVariable candidateVariable)
	{
		return !hasParent(candidateVariable);
	}
	
	/**
	 * Finds the root of a given variable.
	 * 
	 * @param candidateVariable
	 * @return
	 */
	public ComplexVariable getRoot(ComplexVariable candidateVariable)
	{
		ComplexVariable current = candidateVariable;
		
		while(hasParent(current)) {
			current = getParent(current);
		}
		
		return current;
	}
	
	/**
	 * Creates a set of successors of a variable, including the successors 
	 * of the successors, and so on.
	 * 
	 * @param variable
	 * @return			Set of successors (and their successors, and...) of the given variable.
	 */
	public Set<ComplexVariable> getComplexSuccessors(ComplexVariable variable)
	{
		Set<ComplexVariable> successors = new HashSet<>();
		
		int added = 0;
		successors.addAll(variable.getComplexVariableSuccessors());
		do {
			Set<ComplexVariable> nextSuccessors = successors
				.stream()
				.flatMap(s -> s.getComplexVariableSuccessors().stream())
				.filter(s -> !successors.contains(s))
				.collect(Collectors.toSet());
			added = nextSuccessors.size();
			successors.addAll(nextSuccessors);
		} while(added > 0);
		
		return successors
			.stream()
			.filter(s -> !isListAttribute(s))
			.collect(Collectors.toSet());
	}
	
	/**
	 * Creates the live containers from the given GUI compound views by the following steps:
	 * 	- Find all complex variables which are marked as "live", excluding list attributes.
	 * 	- Find all complex lists, which have at least one attribute that is marked as "live".
	 * 	- Find all complex attributes, which are marked as "live"
	 * 	- Find all complex variables, which have at least one primitive attribute that is marked as "live".
	 * 	- Find all successors (and their successors, and...) of all previously found complex variables.
	 * 
	 * For each complex variable, that accomplish these steps:
	 * 	- Find the candidate variable (parent of the highest list upwards the live variable).
	 * 	- Find the path from the candidate to the live variable.
	 * 
	 * @param guiCompoundViews
	 * @return
	 */
	private Set<LiveVariableContainer> createLiveContainers(Set<GUICompoundView> gcvs)
	{
		Set<String> guiIds = new HashSet<String>();
		Set<GUICompoundView> guiCompoundViews = new HashSet<GUICompoundView>();
		gcvs.forEach((n)->{
			if(!guiIds.contains(n.getGui().getId())){
				guiCompoundViews.add(n);
				guiIds.add(n.getGui().getId());
			}
		});
		
		Set<ComplexVariable> liveVariables = new HashSet<>();
		
		// Find all complex live variables.
		liveVariables.addAll(findComplexLiveVariables(guiCompoundViews));

		// Find all list attributes.
		liveVariables.addAll(findLiveListAttributes(guiCompoundViews));
		
		// Find all complex variables from live complex attributes.
		liveVariables.addAll(findComplexVariableFromComplexAttribute(guiCompoundViews));
		
		// Find all primitive attributes.
		liveVariables.addAll(findComplexVariablesWithLivePrimitiveAttribute(guiCompoundViews));
		
		// Add all successors of all live variables.
		final Set<ComplexVariable> successors = new HashSet<>();
		liveVariables.forEach(c -> successors.addAll(getComplexSuccessors(c)));
		liveVariables.addAll(successors);
		
		return liveVariables
			.stream()
			.filter(v -> v != null)
			.map(v -> new LiveVariableContainer(v, findCandidate(v), createPathFromCandidateToLive(v)))
			.filter(c -> c.hasCandidate())
			.collect(Collectors.toSet());
	}
	
	/**
	 * Finds all complex variables with isLive = true in
	 * the given GUI compound views.
	 * 
	 * @param guiCompoundViews
	 * @return
	 */
	private Set<ComplexVariable> findComplexLiveVariables(Set<GUICompoundView> guiCompoundViews)
	{
		return guiCompoundViews
			.stream()
			.flatMap(gcv -> gcv.getPairs().getElements().stream())
			.filter(me -> me instanceof ComplexVariable)
			.map(me -> (ComplexVariable) me)
			.filter(v -> v.isIsLive() && !isListAttribute(v))
			.collect(Collectors.toSet());
	}
	
	/**
	 * Finds all live list attributes (first, current, last)
	 * and maps them to their list.
	 * 
	 * @param guiCompoundViews
	 * @return
	 */
	private Set<ComplexVariable> findLiveListAttributes(Set<GUICompoundView> guiCompoundViews)
	{
		return guiCompoundViews
			.stream()
			.flatMap(gcv -> gcv.getPairs().getElements().stream())
			.filter(me -> me instanceof ComplexVariable)
			.map(me -> (ComplexVariable) me)
			.filter(v -> v.isIsLive() && isListAttribute(v))
			.map(v -> getParent(v))
			.collect(Collectors.toSet());
	}
	
	/**
	 * Finds all complex variables having at least 
	 * one primitive attribute
	 * which is marked as live.
	 * 
	 * @param guiCompoundViews
	 * @return
	 */
	private Set<ComplexVariable> findComplexVariablesWithLivePrimitiveAttribute(Set<GUICompoundView> guiCompoundViews)
	{
		return guiCompoundViews
			.stream()
			.flatMap(gcv -> gcv.getPairs().getElements().stream())
			.filter(me -> me instanceof ComplexVariable)
			.map(me -> (ComplexVariable) me)
			.filter(v -> 
				v.getAttributes() != null 
				&& v.getAttributes()
					.stream()
					.filter(a -> a instanceof PrimitiveAttribute && a.isIsLive())
					.count() > 0
			)
			.collect(Collectors.toSet());
	}
	
	/**
	 * Finds all complex attributes, that are live
	 * and transforms them to their complex variable.
	 * 
	 * @param guiCompoundViews
	 * @return
	 */
	private Set<ComplexVariable> findComplexVariableFromComplexAttribute(Set<GUICompoundView> guiCompoundViews)
	{
		return guiCompoundViews
			.stream()
			.flatMap(gcv -> gcv.getPairs().getElements().stream())
			.filter(me -> me instanceof ComplexVariable)
			.map(me -> (ComplexVariable) me)
			.flatMap(v -> v.getAttributes().stream().filter(a -> a instanceof ComplexAttribute && a.isIsLive()))
			.map(a -> (ComplexVariable) ((ComplexAttribute) a).getContainer())
			.map(v -> isListAttribute(v) ? getParent(v) : v)
			.collect(Collectors.toSet());
	}
	
	/**
	 * Find the live candidate of a given complex variable.
	 * The live candidate is the highest complex variable
	 * in the variable tree, upwards the given variable,
	 * that is not a list and has no list upwards.
	 * 
	 * @param variable
	 * @return
	 */
	private ComplexVariable findCandidate(ComplexVariable variable)
	{
		ComplexVariable highestList = null;
		ComplexVariable current = variable;
		while(current != null) {
			if(current.isIsList()) {
				highestList = current;
			}
			
			current = getParent(current);
		}
		
		// No list upwards this variable -> Can use it directly.
		if(highestList == null) {
			return variable;
		}
		
		if(!hasParent(highestList)) {
			System.out.println(String.format("[WARN] %s is a list without parent -> no live updates possible", variable.getName()));
			return null;
		}
		
		return getParent(highestList);
	}
	
	/**
	 * Creates the path, starting at the candidate variable
	 * and ending at the live variable.
	 * 
	 * @param live variable
	 * @return
	 */
	private LinkedList<ComplexVariable> createPathFromCandidateToLive(ComplexVariable liveVariable)
	{
		LinkedList<ComplexVariable> path = new LinkedList<>();
		if(liveVariable == null) {
			return path;
		}
		
		ComplexVariable candidate = findCandidate(liveVariable);
		ComplexVariable current = liveVariable;
		while(current != null && !current.equals(candidate)) {
			if(!isListAttribute(current)) {
				path.offerFirst(current);
			}
			
			current = getParent(current);
		}
		
		return path;
	}
	
	/**
	 * @param variable
	 * @return	Parent complex variable of the given.
	 */
	private ComplexVariable getParent(ComplexVariable variable) 
	{
		if(!hasParent(variable)) {
			return null;
		}
		
		return variable.getComplexVariablePredecessors().get(0);
	}
	
	/**
	 * Checks the parent of the given variable.
	 * 
	 * @param variable
	 * @return	True, if the variable has a parent.
	 */
	private boolean hasParent(ComplexVariable variable)
	{
		return 	variable != null
				&& variable.getComplexVariablePredecessors() != null 
				&& variable.getComplexVariablePredecessors().size() == 1;
	}
	
	/**
	 * Checks the variable is list attribute.
	 * @param variable
	 * @return	True, if the variable is a list attribute.
	 */
	private boolean isListAttribute(ComplexVariable variable)
	{
		return hasParent(variable) && getParent(variable).isIsList();
	}
	
	
	private Set<LiveVariableContainer> createLiveContainers(GUICompoundView guiCompoundView)
	{
		Set<GUICompoundView> guiCompoundViews = new HashSet<>();
		guiCompoundViews.add(guiCompoundView);
		
		return createLiveContainers(guiCompoundViews);
	}
}
