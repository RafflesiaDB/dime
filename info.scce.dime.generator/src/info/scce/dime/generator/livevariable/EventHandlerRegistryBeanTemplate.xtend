package info.scce.dime.generator.livevariable

class EventHandlerRegistryBeanTemplate {
	
	public static def getClassName() '''LiveVariableRegistryBean'''
	
	public def generate(String pkg) '''
		package «pkg»;
		
		import de.ls5.dywa.adapter.events.EventHandler;
		import de.ls5.dywa.api.event.EventDispatcher;
		
		import javax.annotation.PostConstruct;
		import javax.annotation.PreDestroy;
		import javax.ejb.Singleton;
		import javax.ejb.Startup;
		import javax.inject.Inject;
		import java.io.IOException;
		import java.io.InputStream;
		import java.util.Properties;
		
		@Singleton
		@Startup
		public class «className» {
		
			private final static String ARTIFACT_NAME;
		
			static {
				final Properties config = new Properties();
				final InputStream inputStream = «className».class.getResourceAsStream("/META-INF/config.properties");
		
				try {
					config.load(inputStream);
				}
				catch (IOException ioe) {
					throw new RuntimeException(ioe);
				}
		
				ARTIFACT_NAME = config.getProperty("finalName");
			}
		
			private static final String EVENTLISTENER_JNDI = "java:global/" +
					ARTIFACT_NAME +
					'/' +
					«EventHandlerTemplate.className».class.getSimpleName() +
					'!' +
					EventHandler.class.getName();
		
			@Inject
			private EventDispatcher eventDispatcher;
		
			@PostConstruct
			public void registerListener() {
				try {
					eventDispatcher.registerEventListener(EVENTLISTENER_JNDI);
				} catch (Exception e) {
					this.unregisterListener();
					throw new RuntimeException(e);
				}
			}
		
			@PreDestroy
			public void unregisterListener() {
				try {
					eventDispatcher.unregisterEventListener(EVENTLISTENER_JNDI);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	'''
}