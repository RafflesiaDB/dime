package info.scce.dime.generator.migration

import info.scce.dime.data.data.Data
import info.scce.dime.data.data.EnumType
import info.scce.dime.generator.rest.TOGenerator
import java.io.File
import java.util.List
import org.apache.commons.io.FileUtils
import org.eclipse.core.runtime.IPath

import info.scce.dime.generator.rest.DyWAAbstractGenerator
import info.scce.dime.generator.util.DyWAExtension

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class EnumMigrator {
	
	static protected extension DyWAExtension = new DyWAExtension
	
	val packageName = "de.ls5.dywa.generated.migration"
	
	def generate(List<Data> models, IPath outlet) {
		val enums = TOGenerator.extractTypes(models).filter(EnumType)
		generateMigrator(outlet, enums)
	}
	
	private def generateMigrator(IPath outlet, Iterable<EnumType> enumTypes) {
		val targetDir = outlet.append("app-persistence-impl/target/generated-sources/")
		DyWAAbstractGenerator.generate(renderMigrator(enumTypes).toString, '''«packageName».''', "EnumMigrator.java", targetDir, 0);
	}
		
	private def renderMigrator(Iterable<EnumType> enumTypes) '''
		package «packageName»;
		
		import java.util.List;

		import javax.annotation.PostConstruct;
		import javax.ejb.Singleton;
		import javax.ejb.Startup;
		import javax.persistence.EntityManager;
		import javax.persistence.PersistenceContext;
		import javax.persistence.TypedQuery;

		import de.ls5.dywa.generated.util.EnumMapping;

		@Singleton
		@Startup
		public class EnumMigrator {

		    @PersistenceContext
		    private EntityManager em;

		    @PostConstruct
		    public void migrate() {

		        for (EnumMapping mapping : em.createQuery("SELECT mapping FROM de.ls5.dywa.generated.util.EnumMapping mapping", EnumMapping.class).getResultList()) {
		            em.remove(mapping);
		        }
		        em.flush();

    			«FOR type : enumTypes»
    			setup«type.dyWATypeSimpleName»(«type.enumLiterals.join(", ")['''«type.getDyWATypeName».«it.name.escapeJava»''']»);
    			«ENDFOR»
		    }

			«FOR type : enumTypes»
			    private void setup«type.dyWATypeSimpleName»(«type.getDyWATypeName»... values) {

			        for («type.getDyWATypeName» value : values) {
			            final TypedQuery<«type.getDyWATypeName»Entity> query = em.createQuery(
			                    "SELECT o FROM «type.getDyWATypeName»Entity o WHERE o.name_ = :name", «type.getDyWATypeName»Entity.class);
			            query.setParameter("name", value.name());

			            final List<«type.getDyWATypeName»Entity> res = query.getResultList();
			            final «type.getDyWATypeName»Entity entity;

			            switch (res.size()) {
			                case 0:
			                    entity = new «type.getDyWATypeName»Entity();
			                    entity.setDywaName(value.name());
			                    em.persist(entity);
			                    break;
			                case 1:
			                    entity = res.get(0);
			                    break;
			                default:
			                    throw new IllegalStateException("There must not exist multiple enum-entities with the same name");
			            }

			            em.persist(new EnumMapping(value.getDywaEnumId(), entity.getId_()));
			        }
			    }
			«ENDFOR»
		}
	'''
}