package info.scce.dime.generator.scheme

import de.ls5.dywa.entities.object.DBType
import java.util.List

class UtilsGenerator {
	val String packageName
	val extension ModelExtensions modelExtensions;
	val extension RenderExtensions renderExtensions;

	new(String packageName) {
		this(packageName, new ModelExtensions(packageName, null));
	}

	new(String packageName, ModelExtensions modelExtensions) {
		this.packageName = packageName;
		this.modelExtensions = modelExtensions
		this.renderExtensions = new RenderExtensions(packageName)
	}

	def String generateTypeInfo(List<DBType> types) '''
		package «packageName».util;
		
		public class TypeInfo {
			private static java.util.Map<Long, java.lang.Class<?>> idToClassMap = new java.util.HashMap<>();
			private static java.util.Map<Long, java.util.Map<Long, String>> typeFieldNamesMap = new java.util.HashMap<>();
			private static java.util.Map<Long, String> typeNamesMap = new java.util.HashMap<>();
			private static java.util.Set<Long> enumTypes = new java.util.HashSet<>();
			private static java.util.Set<Long> allTypes = new java.util.HashSet<>();
			
			static {
				«FOR type : types»
					«IF !type.isDeleted»
						/* DBType «type.cincoId» */
						typeNamesMap.put(«type.getId»L, "«type.getName»");
						idToClassMap.put(«type.getId()»L, «type.renderFullCanonicalClassName("entity")».class);
						«IF type.isEnumerable»enumTypes.add(«type.getId()»L);«ENDIF»
						allTypes.add(«type.getId»L);
						java.util.Map<Long, String> map«type.getId» = new java.util.HashMap<Long, String>();
						«FOR field : type.getFields»
							map«type.getId».put(«field.getId»L, "«field.getName»");
						«ENDFOR»
						typeFieldNamesMap.put(«type.getId»L, map«type.getId»);
					«ENDIF»
				«ENDFOR»
			}
			
			public static String getGeneratedClassName(Long typeId) {
				java.lang.Class<?> clazz =  idToClassMap.get(typeId);
				return clazz != null ? clazz.getName() : null;
			}
			
			public static java.lang.Class<?> getGeneratedClass(Long typeId) {
				return idToClassMap.get(typeId);
			}
			
			public static java.util.Collection<Class<?>> getAllGeneratedClasses() {
				return idToClassMap.values();
			}
			
			public static java.util.List<String> getAllGeneratedClassNames() {
				return idToClassMap.values().stream().map(Class::getName).collect(java.util.stream.Collectors.toList());
			}
			
			public static boolean isEnumerable(Long id) {
				return enumTypes.contains(id);
			}
			
			public static boolean typeInUse(Long typeId) {
				return allTypes.contains(typeId);
			}
			
			public static java.util.Map<Long, String> getTypeFieldNames(Long typeId) {
				return typeFieldNamesMap.get(typeId);
			}
			
			public static String getTypeName(Long id) {
				return typeNamesMap.get(id);
			}
		}
	'''

	def String generateDelegationAnnotation() '''
		package «packageName».util;
		
		import java.lang.annotation.ElementType;
		import java.lang.annotation.Retention;
		import java.lang.annotation.RetentionPolicy;
		import java.lang.annotation.Target;
		 
		@Target(ElementType.METHOD)
		@Retention(RetentionPolicy.RUNTIME)
		public @interface Delegation {
		    String attributeName();
		    java.lang.Class<?> attributeClass();
		}
	'''

	def String generateDomainFile() '''
		package «packageName».util;
		
		import java.util.Date;
		
		import javax.persistence.Basic;
		import javax.persistence.Column;
		import javax.persistence.Entity;
		import javax.persistence.GeneratedValue;
		import javax.persistence.Id;
		import javax.persistence.Table;
		import javax.persistence.Temporal;
		import javax.persistence.TemporalType;
		
		@Entity
		@Table(name="domain_file")
		public class DomainFile {
			@Id
			@GeneratedValue
			@Column(name="meta_id")
			private long id_;
			
			@Column(name="meta_dywa_id", unique = true)
			private Long dywaId_;
			
			@Basic(optional = false)
			private String fileName;
		
			@Basic(optional = false)
			private String contentType;
		
			@Basic(optional = false)
			@Temporal(TemporalType.TIMESTAMP)
			private Date createdAt;
		
			public DomainFile() { }
		
			public DomainFile(final String fileName, final String contentType) {
				this.fileName = fileName;
				this.contentType = contentType;
				this.createdAt = new Date();
			}
		
			public Long getId() {
			return id_;
			}
		
			public void setId(Long id) {
				this.id_ = id;
			}
		
			// return existing id on runtime
			public long getDywaId() {
					return this.id_;
			}
		
			public String getFileName() {
				return this.fileName;
			}
		
			
			public String getContentType() {
				return this.contentType;
			}
		
			public void setContentType(final String contentType) {
				this.contentType = contentType;
			}
		
			public void setFileName(final String fileName) {
				this.fileName = fileName;
			}
		
			public Date getCreatedAt() {
			return createdAt;
			}
		
			public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
			}
		
			@Override
			public String toString() {
				return "DomainFile{" + "id_=" + id_ + ", fileName='" + fileName + '\'' + ", contentType='" + contentType + '\'' + '}';
			}
		}
		
	'''

	def String generateFileReference() '''
		package «packageName».util;
		
		import java.util.Date;
		import «packageName».util.DomainFile;
		
		public class FileReference {
		
			private final DomainFile delegate;
		
			public FileReference(final DomainFile delegate) {
				if (delegate == null) {
					throw new IllegalArgumentException(
							"Not allowed to wrap null, use null directly");
				}
				this.delegate = delegate;
			}
		
			public DomainFile getDelegate() {
				return this.delegate;
			}
		
			public long getDywaId() {
				return this.delegate.getDywaId();
			}
		
			public String getContentType() {
				return this.delegate.getContentType();
			}
		
			public String getFileName() {
				return this.delegate.getFileName();
			}
		
			public Date getCreatedAt() {
			return this.delegate.getCreatedAt();
			}
		}
		
	'''

	def String generateDomainFileControllerInterface() '''
		package «packageName».util;
		
		public interface DomainFileController {
		
			public FileReference getFileReference(final long id);
		
			public java.io.InputStream loadFile(final FileReference identifier);
		
			public FileReference storeFile(final String fileName, final java.io.InputStream data);
		
			public void deleteFile(final FileReference identifier);
		}
	'''

	def String generateDomainFileController() '''
		package «packageName».util;
		
		import java.io.InputStream;
		import java.io.File;
		
		import javax.inject.Inject;
		import javax.inject.Named;
		import javax.enterprise.context.RequestScoped;
		
		import «packageName».util.DomainFile;
		import «packageName».util.StorageManager;
		
		@Named
		@RequestScoped
		public class DomainFileControllerImpl implements DomainFileController {
		
			@javax.persistence.PersistenceContext
			   private javax.persistence.EntityManager entityManager;
		
			@Inject
			private StorageManager storageManager;
			
			@Override
			public FileReference getFileReference(final long id) {
				final DomainFile file = entityManager.find(DomainFile.class, id);
				if (file == null) {
					return null;
				}
		
				return new FileReference(file);
			}
		
			@Override
			public InputStream loadFile(final FileReference identifier) {
				if (identifier == null) {
					return null;
				}
				File fsFile = getFileForDomainFile(identifier.getDelegate());
				return storageManager.getFile(fsFile);
			}
			
			@Override
			public FileReference storeFile(final String fileName, final InputStream dataStream) {
				final InputStream dataInput = org.apache.tika.io.TikaInputStream.get(dataStream);
				
				final DomainFile result = new DomainFile(fileName, storageManager.getContentType(dataInput));
				entityManager.persist(result);
		
				final File file = getFileForDomainFile(result);
				try {
					storageManager.createFile(file);
				}
				catch (org.xadisk.filesystem.exceptions.FileAlreadyExistsException e) {
					throw new IllegalStateException("file " + file.toString() + " existed, overwriting...");
				}
		
				org.xadisk.additional.XAFileOutputStreamWrapper fsFileOutput = new org.xadisk.additional.XAFileOutputStreamWrapper(storageManager.createXAFileOutputStream(file));
		
				try {
					storageManager.copyStreams(dataInput, fsFileOutput);
				} finally {
					org.apache.tika.io.IOUtils.closeQuietly(fsFileOutput);
					org.apache.tika.io.IOUtils.closeQuietly(dataInput);
				}
				return new FileReference(result);
			}
		
			@Override
			public void deleteFile(final FileReference identifier) {
				File fsFile = getFileForDomainFile(identifier.getDelegate());
				storageManager.deleteFile(fsFile);
				entityManager.remove(identifier.getDelegate());
			}
			
			private File getFileForDomainFile(final DomainFile domainFile) {
				return new File(StorageManager.getNativeDomainStorageRoot(), Long.toString(domainFile.getId()));
			}
		}
	'''

	def String generateEmProvider() '''
		package «packageName».util;
		
		@javax.enterprise.context.ApplicationScoped
		public class EntityManagerProvider {
			@javax.persistence.PersistenceContext 
			private javax.persistence.EntityManager entityManager;
			
			@javax.enterprise.inject.Produces
			public javax.persistence.EntityManager getEntityManager() {
				return this.entityManager;
			}
		}
	'''

	def String generatePersistenceXML(List<String> generatedEntities) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<persistence version="2.0"
			xmlns="http://java.sun.com/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="
		      http://java.sun.com/xml/ns/persistence
		      http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd">
			<persistence-unit name="pu">
				<jta-data-source>java:/DefaultDS</jta-data-source>
		
				<class>«packageName».util.DomainFile</class>
				<class>«packageName».util.EnumMapping</class>
				<!-- generated entities -->
		        «FOR clazz : generatedEntities»
		        <class>«clazz»</class>
		        «ENDFOR»
		
				<shared-cache-mode>ALL</shared-cache-mode>
				<properties>
					<property name="hibernate.hbm2ddl.auto" value="update" />
		
					<property name="hibernate.jdbc.batch_size" value="64" />
					<property name="hibernate.default_batch_fetch_size" value="64" />
		
					<property name="hibernate.generate_statistics" value="false" />
					<property name="hibernate.cache.use_second_level_cache" value="true" />
					<property name="hibernate.cache.use_query_cache" value="true" />
		
					<property name="hibernate.show_sql" value="false" />
					<property name="hibernate.format_sql" value="false" />
					<property name="hibernate.use_sql_comments" value="false" />
					<property name="hibernate.archive.autodetection" value="class, hbm" />
		
		
					<property name="eclipselink.ddl-generation" value="create-tables"/>
					<property name="eclipselink.ddl-generation.output-mode" value="database" />
		
					<property name="eclipselink.cache.coordination.protocol" value="jms"/>
					<property name="eclipselink.cache.coordination.jms.topic" value="java:/jms/topic/DyWACacheTopic"/>
					<property name="eclipselink.cache.coordination.jms.factory" value="java:/ConnectionFactory"/>
		
					<property name="eclipselink.target-server" value="JBoss"/>
					<property name="eclipselink.deploy-on-startup" value="True" />
		
					<property name="eclipselink.weaving" value="static"/>
					<property name="eclipselink.weaving.internal" value="false"/>
					<property name="eclipselink.id-validation" value="NEGATIVE"/>
		
					<property name="eclipselink.logging.parameters" value="true"/>
				</properties>
			</persistence-unit>
		</persistence>
	'''

	def String generateStorageManager() '''
		package «packageName».util;
		
		import java.io.File;
		import java.io.IOException;
		import java.io.InputStream;
		import java.io.OutputStream;
		import java.nio.file.Files;
		import java.nio.file.Path;
		import java.util.InvalidPropertiesFormatException;
		import java.util.Properties;
		
		import javax.annotation.Resource;
		import javax.enterprise.context.RequestScoped;
		import javax.persistence.OptimisticLockException;
		import javax.resource.ResourceException;
		
		import org.apache.tika.detect.DefaultDetector;
		import org.apache.tika.detect.Detector;
		import org.apache.tika.io.IOUtils;
		import org.apache.tika.io.TikaInputStream;
		import org.apache.tika.metadata.Metadata;
		import org.apache.tika.mime.MediaType;
		import org.slf4j.Logger;
		import org.slf4j.LoggerFactory;
		import org.xadisk.additional.XAFileInputStreamWrapper;
		import org.xadisk.bridge.proxies.interfaces.Session;
		import org.xadisk.bridge.proxies.interfaces.XAFileInputStream;
		import org.xadisk.bridge.proxies.interfaces.XAFileOutputStream;
		import org.xadisk.connector.outbound.XADiskConnection;
		import org.xadisk.connector.outbound.XADiskConnectionFactory;
		import org.xadisk.filesystem.exceptions.DirectoryNotEmptyException;
		import org.xadisk.filesystem.exceptions.FileAlreadyExistsException;
		import org.xadisk.filesystem.exceptions.FileNotExistsException;
		import org.xadisk.filesystem.exceptions.FileUnderUseException;
		import org.xadisk.filesystem.exceptions.InsufficientPermissionOnFileException;
		import org.xadisk.filesystem.exceptions.LockingFailedException;
		import org.xadisk.filesystem.exceptions.NoTransactionAssociatedException;
		
		/**
		 * Responsible for data storage.
		 */
		@RequestScoped
		public class StorageManager {
		
			private final static Logger LOGGER = LoggerFactory.getLogger(StorageManager.class);
		
			/**
			 * The XADisk file system accessor factory.
			 */
			@Resource(mappedName = "java:jboss/eis/XADiskConnection")
			private XADiskConnectionFactory xaDiskConnectionFactory;
		
			/**
			 * The shared MIME type detector. This is stateless and thread-safe and will therefore be shared between all
			 * instances of this bean.
			 */
			private static final Detector MIME_DETECTOR = new DefaultDetector();
		
			/**
			 * The root path for file storage. All files are saved in this directory; named by the associated {@link DBFile}'s
			 * ID.
			 */
			private static final File STORAGE_ROOT;
		
			private static final File NATIVE_DOMAIN_STORAGE_ROOT;
		
			static {
				final Properties fileStorageProperties = new Properties();
				try {
					fileStorageProperties
							.loadFromXML(StorageManager.class.getClassLoader().getResourceAsStream("/META-INF/properties.xml"));
				}
				catch (final InvalidPropertiesFormatException e) {
					throw new IllegalStateException(e);
				}
				catch (final IOException e) {
					throw new IllegalStateException(e);
				}
		
				// Property is set in standalone.xml of Wildfly
				final String JBOSS_HOME_ENV = "de.ls5.dywa.JBOSS_HOME";
				final String JBOSS_HOME = System.getProperty(JBOSS_HOME_ENV);
		
				if (JBOSS_HOME == null || JBOSS_HOME.isEmpty()) {
					// store files in tmp dir
					try {
						final File tempDir = Files.createTempDirectory(null).toFile();
		
						LOGGER.warn("'" + JBOSS_HOME_ENV + "' not specified. Using '" + tempDir.getPath()
								+ "' to TEMPORARILY store files");
		
						STORAGE_ROOT = new File(tempDir, fileStorageProperties.getProperty("file-storage"));
						NATIVE_DOMAIN_STORAGE_ROOT =
								new File(tempDir, fileStorageProperties.getProperty("native-domain-storage"));
					}
					catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
				else {
					STORAGE_ROOT =
							new File(JBOSS_HOME + File.separatorChar + fileStorageProperties.getProperty("file-storage"));
					NATIVE_DOMAIN_STORAGE_ROOT = new File(
							JBOSS_HOME + File.separatorChar + fileStorageProperties.getProperty("native-domain-storage"));
				}
		
				if (!STORAGE_ROOT.isDirectory()) {
					if (!STORAGE_ROOT.mkdirs()) {
						throw new IllegalStateException("could not create storage directory");
					}
				}
		
				if (!NATIVE_DOMAIN_STORAGE_ROOT.isDirectory()) {
					if (!NATIVE_DOMAIN_STORAGE_ROOT.mkdirs()) {
						throw new IllegalStateException("could not create app storage directory");
					}
				}
			}
		
			public XADiskConnectionFactory getXaDiskConnectionFactory() {
				return xaDiskConnectionFactory;
			}
		
			/**
			 * Returns the MIME detector
			 *
			 * @return
			 */
			public static Detector getMimeDetector() {
				return MIME_DETECTOR;
			}
		
			/**
			 * Returns the root of DyWA storage
			 *
			 * @return
			 */
			public static File getStorageRoot() {
				return STORAGE_ROOT;
			}
		
			/**
			 * Returns the root of the generated application storage
			 *
			 * @return
			 */
			public static File getNativeDomainStorageRoot() {
				return NATIVE_DOMAIN_STORAGE_ROOT;
			}
		
			/**
			 * Identify the content (MIME) type of an {@link InputStream}'s contents and store it in the {@link DBFile}'s
			 * content type field. {@link IOException}s are rethrown as {@link IllegalStateException}s since no graceful
			 * in-app-server handling is possible.
			 *
			 * @param dataInput the {@link InputStream} to identify, should be an {@link TikaInputStream}.
			 */
			public String getContentType(final InputStream dataInput) {
				MediaType mimeType;
				try {
					mimeType = MIME_DETECTOR.detect(dataInput, new Metadata());
				}
				catch (IOException e) {
					throw new IllegalStateException(e);
				}
		
				return mimeType.toString();
			}
		
			/**
			 * Call {@link IOUtils#copy(InputStream, OutputStream)}, wrapping any {@link IOException}s as
			 * {@link IllegalStateException}s.
			 *
			 * @param input  the stream to read from.
			 * @param output the stream to copy into.
			 * @see IOUtils#copy(InputStream, OutputStream)
			 */
			public void copyStreams(InputStream input, OutputStream output) {
				try {
					IOUtils.copy(input, output);
				}
				catch (IOException e) {
					throw new IllegalStateException(e);
				}
			}
		
			public InputStream getFile(File fsFile) {
				final InputStream fsFileInput;
		
				try {
					if (Files.isSymbolicLink(fsFile.toPath())) {
						fsFile = Files.readSymbolicLink(fsFile.toPath()).toFile(); // extract real path
					}
					fsFileInput = new XAFileInputStreamWrapper(this.createXAFileInputStream(fsFile));
				}
				// FIXME if file not found an InsufficientPermissionOnFileException is thrown instead of FileNotExistsException.
				// So here we catch Exception :(.
				catch (final Exception e) {
					throw new RuntimeException(e);
				}
		
				return fsFileInput;
			}
		
			/**
			 * Connect to the XADisk instance, refusing to handle any exceptions.
			 *
			 * @return a {@link XADiskConnection}, caller is responsible to {@link XADiskConnection#close()}.
			 */
			private XADiskConnection getConnection() {
				try {
					return getXaDiskConnectionFactory().getConnection();
				}
				catch (final ResourceException e) {
					throw new RuntimeException(e);
				}
			}
		
			/**
			 * Wraps the {@link Session#createFile(File, boolean)} method that declares several unlikely or impossible checked
			 * exception. This method handles cleanup of these exceptions. This method only creates files, not directories.
			 *
			 * @param file the file to create.
			 * @throws FileAlreadyExistsException if the file was already created.
			 */
			public void createFile(final File file) throws FileAlreadyExistsException {
				final XADiskConnection fileSystem = this.getConnection();
		
				try {
					fileSystem.createFile(file, false);
		
				}
				catch (final FileNotExistsException e) {
					// if the directory does not exist, config error
					throw new IllegalStateException(e);
		
				}
				catch (final InsufficientPermissionOnFileException e) {
					// if the directory has bad permissions, config error
					throw new IllegalStateException(e);
		
				}
				catch (final LockingFailedException e) {
					// wrap it as a db-style exception
					throw new OptimisticLockException(e);
		
				}
				catch (final NoTransactionAssociatedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				catch (final InterruptedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				finally {
					fileSystem.close();
				}
			}
		
			/**
			 * Creates a symbolic link
			 *
			 * @param from
			 * @param to
			 * @return
			 */
			public static boolean makeSymLink(Path src, Path target) {
				try {
					Files.deleteIfExists(src);
					Files.createSymbolicLink(src, target);
					return true;
				}
				catch (IOException | UnsupportedOperationException e) {
					e.printStackTrace();
					return false;
				}
			}
		
			/**
			 * Wraps the {@link Session#createXAFileInputStream(File, boolean)} method that declares several unlikely or
			 * impossible checked exception. This method handles cleanup of these exceptions.
			 *
			 * @param file the file to read from.
			 * @return the {@link XAFileInputStream} linked to the file's contents.
			 * @throws FileNotExistsException if the requested file does not exist.
			 */
			private XAFileInputStream createXAFileInputStream(final File file) throws FileNotExistsException {
				final XADiskConnection fileSystem = this.getConnection();
		
				try {
					return fileSystem.createXAFileInputStream(file, false);
		
				}
				catch (final InsufficientPermissionOnFileException e) {
					// caller must ensure file permissions are good
					throw new IllegalArgumentException(e);
		
				}
				catch (final LockingFailedException e) {
					// wrap it as a db-style exception
					throw new OptimisticLockException(e);
		
				}
				catch (final NoTransactionAssociatedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				catch (final InterruptedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				finally {
					fileSystem.close();
				}
			}
		
			/**
			 * Wraps the {@link Session#createXAFileOutputStream(File, boolean)} method that declares several unlikely or
			 * impossible checked exception. This method handles cleanup of these exceptions.
			 *
			 * @param file the file to write to.
			 * @return the {@link XAFileOutputStream} linked to the file's contents.
			 */
			public XAFileOutputStream createXAFileOutputStream(final File file) {
				final XADiskConnection fileSystem = this.getConnection();
		
				try {
					return fileSystem.createXAFileOutputStream(file, true);
		
				}
				catch (final FileNotExistsException e) {
					// caller must ensure file exists
					throw new IllegalArgumentException(e);
		
				}
				catch (final FileUnderUseException e) {
					// wrap it as a db-style exception
					throw new OptimisticLockException(e);
		
				}
				catch (final InsufficientPermissionOnFileException e) {
					// caller must ensure file permissions are good
					throw new IllegalArgumentException(e);
		
				}
				catch (final LockingFailedException e) {
					// wrap it as a db-style exception
					throw new OptimisticLockException(e);
		
				}
				catch (final NoTransactionAssociatedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				catch (final InterruptedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				finally {
					fileSystem.close();
				}
			}
		
			/**
			 * Wraps the {@link Session#deleteFile(File)} method that declares several unlikely or impossible checked exception.
			 * This method handles cleanup of these exceptions.
			 *
			 * @param file the file to create.
			 */
			public void deleteFile(final File file) {
				final XADiskConnection fileSystem = this.getConnection();
		
				try {
					if (Files.isSymbolicLink(file.toPath())) { // symlink: safe delete
						fileSystem.deleteFile(file);
					}
					else {
						boolean symLinkFound = false;
						if (file.getParentFile()
								.equals(STORAGE_ROOT)) { // dywa file: check if domain file links to it and eventually replace it with the original
							try {
								for (File symLink : NATIVE_DOMAIN_STORAGE_ROOT
										.listFiles(f -> Files.isSymbolicLink(f.toPath()))) { // domain symlinks
									if (Files.readSymbolicLink(symLink.toPath()).equals(file.toPath())) {
										symLinkFound = true;
										String newName = symLink.getName(); // get name
										fileSystem.deleteFile(symLink); // delete symlink before moving
										fileSystem.moveFile(file,
												new File(NATIVE_DOMAIN_STORAGE_ROOT, newName)); // replace it with the new one
										break;
									}
								}
							}
							catch (FileAlreadyExistsException | IOException e) {
								throw new IllegalStateException(e);
							}
						}
						else if (file.getParentFile()
								.equals(NATIVE_DOMAIN_STORAGE_ROOT)) { // domain file: check if DyWA file links to it and eventually replace it with the original
							try {
								for (File symLink : STORAGE_ROOT
										.listFiles(f -> Files.isSymbolicLink(f.toPath()))) { // DyWA symlinks
									if (Files.readSymbolicLink(symLink.toPath()).equals(file.toPath())) {
										symLinkFound = true;
										String newName = symLink.getName(); // get name
										fileSystem.deleteFile(symLink); // delete symlink before moving
										fileSystem
												.moveFile(file, new File(STORAGE_ROOT, newName)); // replace it with the new one
										break;
									}
								}
							}
							catch (FileAlreadyExistsException | IOException e) {
								throw new IllegalStateException(e);
							}
						}
						else
							throw new RuntimeException("Unknown parent directory of file.");
		
						if (!symLinkFound) {
							fileSystem.deleteFile(file); // no symlink found: delete normally
						}
					}
		
				}
				catch (final DirectoryNotEmptyException e) {
					// caller must ensure to call with real file
					throw new IllegalStateException(e);
		
				}
				catch (final FileNotExistsException e) {
					// if the directory does not exist, config error
					throw new IllegalStateException(e);
		
				}
				catch (final FileUnderUseException e) {
					// wrap it as a db-style exception
					throw new OptimisticLockException(e);
		
				}
				catch (final InsufficientPermissionOnFileException e) {
					// if the directory has bad permissions, config error
					throw new IllegalStateException(e);
		
				}
				catch (final LockingFailedException e) {
					// wrap it as a db-style exception
					throw new OptimisticLockException(e);
		
				}
				catch (final NoTransactionAssociatedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				catch (final InterruptedException e) {
					// can not happen, app server malfunction
					throw new IllegalStateException(e);
		
				}
				finally {
					fileSystem.close();
				}
			}
		}
	'''

	def generateAALControllerInterface() '''
		package «packageName».util;
		
		public interface AALController {
		
			public void reset();
		
		}
	'''

	def generateAALControllerImpl(Iterable<DBType> nonAbstractTypes) '''
		package «packageName».util;
		
		import javax.inject.Inject;
		import javax.enterprise.context.RequestScoped;
		import javax.persistence.EntityManager;
		import javax.persistence.FlushModeType;
		import javax.persistence.PersistenceContext;
		
		@RequestScoped
		public class AALControllerImpl implements AALController {
		
			@PersistenceContext
			private EntityManager entityManager;
		
			«FOR t : nonAbstractTypes»
			@Inject
			private «packageName».controller«RenderExtensions.renderTypePackageSuffix(t)».«RenderExtensions.renderTypeName(t)»Controller «RenderExtensions.renderTypeName(t)»Controller;
			«ENDFOR»
		
			@Override
			public void reset() {
		
			final FlushModeType oldFlushMode = this.entityManager.getFlushMode();
			this.entityManager.flush();
			this.entityManager.setFlushMode(FlushModeType.COMMIT);
		
			«FOR t : nonAbstractTypes»
				for (final «t.renderFQTypeName» o : «RenderExtensions.renderTypeName(t)»Controller.fetch()) {
				«IF t.isEnumerable»
					o.setDywaName(o.toString());
					«FOR f : t.getFullActiveFields»
						o.set«f.renderPropertyName»(null);
					«ENDFOR»
				«ELSE»
					«RenderExtensions.renderTypeName(t)»Controller.deleteWithIncomingReferences(o);
				«ENDIF»
				}
			«ENDFOR»
		
				this.entityManager.setFlushMode(oldFlushMode);
			}
		}
	'''

	def generateEnumMappingEntity() '''
		package «packageName».util;
		
		import javax.persistence.Entity;
		import javax.persistence.Id;
		
		@Entity
		public class EnumMapping {
		
		    @Id
		    private long enumId;
		
		    private long objectId;
		
		    public EnumMapping() {}
		
		    public EnumMapping(long enumId, long objectId) {
		    this.enumId = enumId;
		    this.objectId = objectId;
		    }
		
		    public long getEnumId() {
		    return enumId;
		    }
		
		    public void setEnumId(long enumId) {
		    this.enumId = enumId;
		    }
		
		    public long getObjectId() {
		    return objectId;
		    }
		
		    public void setObjectId(long objectId) {
		    this.objectId = objectId;
		    }
		}
	'''
}
