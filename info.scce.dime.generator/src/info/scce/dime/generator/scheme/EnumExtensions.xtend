package info.scce.dime.generator.scheme

import de.ls5.dywa.entities.object.DBField

class EnumExtensions {
	val extension ModelExtensions modelExtensions;

	new(String packageName) {
		this(packageName, new ModelExtensions(packageName, null));
	}

	new(String packageName, ModelExtensions modelExtensions) {
		this.modelExtensions = modelExtensions;
	}

	def renderEnumEntityGetterAndSetter(DBField field) '''
		public «field.renderMethodPropertyName(false)» get«field.renderMethodSuffix»() {
			loadOrRefresh();
			return this.internalDelegate != null ? this.internalDelegate.get«field.renderMethodSuffix»() : null;
		}
		
		public void set«field.renderMethodSuffix»(«field.renderMethodPropertyName(false)» newValue) {
			loadOrRefresh();
			if (this.internalDelegate != null) {
				this.internalDelegate.set«field.renderMethodSuffix»(newValue);
			}
		}
	'''
}
