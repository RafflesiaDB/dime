package info.scce.dime.generator.scheme

import de.ls5.dywa.entities.object.DBField
import de.ls5.dywa.entities.object.DBObject
import de.ls5.dywa.entities.object.DBType
import java.util.Collection
import java.util.List
import java.util.Map

import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*

class EnumGenerator {
	val String packageName
	val extension ModelExtensions modelExtensions;
	val extension EnumExtensions enumExtensions;
	val Map<DBType, List<DBType>> inheritance
	
	new(String packageName, Map<DBType, List<DBType>> inheritance) {
		this(packageName,inheritance,new ModelExtensions(packageName, inheritance))
	}

	new(String packageName, Map<DBType, List<DBType>> inheritance, ModelExtensions modelExtensions) {
    		this.packageName = packageName;
    		this.modelExtensions = modelExtensions;
    		this.enumExtensions = new EnumExtensions(packageName, modelExtensions);
    		this.inheritance = inheritance;
    	}
	
	def String generateInterface(DBType type) '''
		/* generated by «this.getClass().getName()» */
		package «type.renderFullPackageName("entity")»;
				
		@de.ls5.dywa.annotations.OriginalName(name = "«type.getName()»")
		@de.ls5.dywa.annotations.ShortDescription(description = "«type.getShortDescription()»")
		@de.ls5.dywa.annotations.LongDescription(description = "«type.getLongDescription()»")
		public interface «type.renderClassName»Interface extends «type.renderInterfaceExtensions» {
			«FOR field : type.getFields»
			«IF !field.isDeleted»
			«field.renderGetterAndSetterSignature»
			
			«ENDIF»
			«ENDFOR»
		}
	'''
	
	def String generateEnum(DBType type, Collection<DBObject> instances) '''
		/* generated by «this.getClass().getName()» */
		package «type.renderFullPackageName("entity")»;
		
		@de.ls5.dywa.annotations.IdRef(id = «type.getId()»L)
		@de.ls5.dywa.annotations.OriginalName(name = "«type.getName()»")
		@de.ls5.dywa.annotations.ShortDescription(description = "«type.getShortDescription()»")
		@de.ls5.dywa.annotations.LongDescription(description = "«type.getLongDescription()»")
		public enum «type.renderClassName» implements «packageName».util.Identifiable, «type.renderFullPackageName("entity")».«type.renderClassName»Interface {
			«instances.map[ o | '''«o.name.escapeJava»(«o.id»L)'''].join(', ')»;
			
			private final long id;
			private «type.renderClassName»Interface internalDelegate;
			private static «packageName».util.EntityManagerProvider emp;
			
			«type.renderClassName»(long id) {
				this.id = id;
			}

			public long getDywaEnumId() {
				return this.id;
			}
		
			public long getDywaId() {
				loadOrRefresh();
				return this.internalDelegate.getDywaId();
			}
		
			public java.lang.String getDywaName() {
				loadOrRefresh();
				return this.internalDelegate.getDywaName();
			}
		
			public void setDywaName(java.lang.String name) {
				loadOrRefresh();
				this.internalDelegate.setDywaName(name);
			}
			
			public long getDywaVersion() {
				loadOrRefresh();
				return this.internalDelegate.getDywaVersion();
			}
			
			public void setDywaVersion(final long version) {
				loadOrRefresh();
				this.internalDelegate.setDywaVersion(version);
			}
			
			«FOR field : type.getFullActiveFields»
				«field.renderEnumEntityGetterAndSetter»

			«ENDFOR»
			
			private void loadOrRefresh() {
				javax.persistence.EntityManager em = getEntityManager();
				if (em != null) {
					javax.persistence.TypedQuery<«type.renderCanonicalClassName("entity")»Interface> query = em.createQuery(
						"SELECT ent " +
						"FROM de.ls5.dywa.generated.util.EnumMapping map " +
							"JOIN «type.renderCanonicalClassName("entity")»Entity ent " +
							"ON (map.objectId = ent.id_) " +
						"WHERE map.enumId = :id",
						«type.renderCanonicalClassName("entity")»Interface.class);
					query.setParameter("id", this.id);
					try {
						this.internalDelegate = query.getSingleResult();
					} catch (java.lang.Exception e) {
						this.internalDelegate = null;
					}
				}
			}
			
			private javax.persistence.EntityManager getEntityManager() {
				if (emp == null) {
					javax.enterprise.inject.spi.BeanManager beanManager = javax.enterprise.inject.spi.CDI.current().getBeanManager();
					javax.enterprise.inject.spi.Bean bean = (javax.enterprise.inject.spi.Bean) beanManager.getBeans(«packageName».util.EntityManagerProvider.class).iterator().next();
					javax.enterprise.context.spi.CreationalContext<«packageName».util.EntityManagerProvider> cctx = beanManager.createCreationalContext(bean);
					emp = («packageName».util.EntityManagerProvider) beanManager.getReference(bean, «packageName».util.EntityManagerProvider.class, cctx);
				}
				return emp.getEntityManager();
			}
			
			<T extends «type.renderClassName»Interface> T getEntityAs(Class<T> clazz) {
				loadOrRefresh();
				return (T) this.internalDelegate;
			}
			
			public static «type.renderClassName» forId(long id) {
				«FOR instance : instances SEPARATOR ' else '»
				if (id == «instance.getId») {
					return «type.renderClassName».«instance.name.escapeJava»;
				}
				«ENDFOR»
				return null;
			}
		}
		
	'''
	
	def String generateEntityClass(DBType type, Collection<DBField> implicitFields, Collection<DBType> additionalTypes) '''
		/* generated by «this.getClass().getName()» */
		package «type.renderFullPackageName("entity")»;
		
		@de.ls5.dywa.annotations.IdRef(id = «type.getId()»L)
		@de.ls5.dywa.annotations.OriginalName(name = "«type.getName()»")
		@de.ls5.dywa.annotations.ShortDescription(description = "«type.getShortDescription()»")
		@de.ls5.dywa.annotations.LongDescription(description = "«type.getLongDescription()»")
		@javax.persistence.Entity
		@javax.persistence.Table(name = "«type.renderTableName»")
		public class «type.renderClassName»Entity implements «type.renderClassName»Interface {
			@javax.persistence.Id
			@javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.SEQUENCE)
			@javax.persistence.Column(name = "meta_id")
			private long id_;
			
			/* DYWA METADATA START */
			@javax.persistence.Column(name = "meta_name")
			private java.lang.String name_;
			
			@javax.persistence.Column(name = "meta_version")
			private long version_;
			
			@javax.persistence.Column(name = "meta_inheritance")
			private boolean inheritance_ = false;

			«IF !type.fields.filter[isBidirectional].isEmpty»
			@javax.persistence.Transient
			private boolean bidirectionalDirtyFlag;
			«ENDIF»
			/* DYWA METADATA END */
			
			/* MAIN ATTRIBUTES START */
			«FOR field : type.getFields»
				«IF !field.isDeleted»
				«field.renderAttribute(type)»
				
				«ENDIF»
			«ENDFOR»
			/* MAIN ATTRIBUTES END */
			
			/* IMPLICIT ATTRIBUTES START */
			«FOR field : implicitFields»
			«field.renderAttribute»
			
			«ENDFOR»
			/* IMPLICIT ATTRIBUTES END */
			
			/* INHERITED MODELS START */
			«FOR superType : inheritance.get(type).filter[!abstractType]»
				@javax.persistence.OneToOne(optional = true, cascade = javax.persistence.CascadeType.ALL)
				@javax.persistence.JoinColumn(name = "«NameGenerator.prepareIdentifierName("inherited",superType.cincoId,superType.name)»")
				private «superType.renderFullCanonicalClassName("entity")» inherited«superType.renderClassName»_;
			«ENDFOR»
			/* INHERITED MODELS END */
			
			/* ADDITIONAL INHERITED MODELS START */
			«FOR superType : additionalTypes»
			@javax.persistence.OneToOne(optional = true, cascade = javax.persistence.CascadeType.ALL)
			@javax.persistence.JoinColumn(name = "«NameGenerator.prepareIdentifierName("inherited",superType.cincoId,superType.name)»")
			private «superType.renderFullCanonicalClassName("entity")» inherited«superType.renderClassName»_;
			
			«ENDFOR»
			/* ADDITIONAL INHERITED MODELS END */
			
			// Constructors
			public «type.renderFullClassName»() { }
			
			public «type.renderFullClassName»(long id) {
				this.id_ = id;
			}
			
			public «type.renderFullClassName»(boolean inheritance) {
				this.inheritance_ = inheritance;
			}
			
			// Methods
			public long getId_() {
				return this.id_;
			}
			
			public void setId_(long id) {
				this.id_ = id;
			}
			
			@java.lang.Override
			public long getDywaId() {
				return this.id_;
			}
			
			public java.lang.String getDywaName() {
				return this.name_;
			}
			
			public void setDywaName(java.lang.String name) {
				this.name_ = name;
			}
			
			public long getDywaVersion() {
				return this.version_;
			}
			
			public void setDywaVersion(final long version) {
				this.version_ = version;
			}
			
			«FOR field : type.getFields»
				«IF !field.isDeleted»
				«field.renderGetterAndSetter(type)»
				
				«ENDIF»									
			«ENDFOR»
			«FOR field : implicitFields»
			«field.renderGetterAndSetter(type)»
			
			«ENDFOR»
			«FOR field : type.getActiveSuperFields(false)»
				«IF inheritance.get(type).stream.anyMatch[t | !t.abstractType && t.getFullActiveFields.contains(field)]»
				«field.renderInheritedGetterAndSetter(type)»
				
				«ELSEIF additionalTypes.stream.anyMatch[t | t.getFullActiveFields.contains(field)]»
				«field.renderAdditionalGetterAndSetter(additionalTypes.stream.filter[t | t.getFullActiveFields.contains(field)].findAny.get)» // additional
				
				«ELSEIF type.getFields.stream.noneMatch[f | f.id === field.id] && implicitFields.stream.noneMatch[f | f.id === field.id]»
				«field.renderGetterAndSetter(type)»
				
				«ENDIF»
			«ENDFOR»
		}
		
	'''
}