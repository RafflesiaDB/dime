import 'dart:async';

import 'package:angular/angular.dart';

import '../core/dime_process_service.dart';

@Component(
    selector: 'dime-modal',
    directives: [coreDirectives],
    templateUrl: 'Modal.html')
class Modal implements OnInit {

  @Input()
  bool isLink;

  @Input()
  bool isDisabled = false;

  @Input()
  String label;

  @Input()
  String title;

  @Input()
  String modalClass;

  @Input()
  bool reload = false;

  @Input()
  bool hasBeforeIcon = false;

  @Input()
  String beforeIconClass;

  @Input()
  bool hasAfterIcon = false;

  @Input()
  String afterIconClass;

  @Output('opened')
  Stream<dynamic> get evt_opened => openedctrl.stream;
  StreamController<dynamic> openedctrl = StreamController<dynamic>();
  
  final DIMEProcessService _processService;

  bool isShown = false;

  bool isLoaded = false;
  
  Modal(this._processService);

  @override
  void ngOnInit() {
    isShown = false;
  }

  void showModal(dynamic e) {
    e.preventDefault();
    isShown = true;
    openedctrl.add(hashCode);
    if (!reload) {
      isLoaded = true;
    }
  }

  void close() {
    isShown = false;
  }

  void closeModal(dynamic e) {
    e.preventDefault();
    close();
  }
  
  bool get disabled => isDisabled || _processService.isAnyProcessActive;
}
