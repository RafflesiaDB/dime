package info.scce.dime.process;

import info.scce.dime.exception.GUIEncounteredSignal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

public class GUIResumer {

    protected void checkAuthentication(ProcessCallFrame processCallFrame, String id) {
        if (processCallFrame.isAuthenticationRequired()) {
            final Subject shiroSubj = SecurityUtils.getSubject();
            if (!shiroSubj.isAuthenticated()) {
                GUIEncounteredSignal sig = new GUIEncounteredSignal(processCallFrame, id);
                sig.setStatus(401);
                throw sig;
            }
        }
    }
}
