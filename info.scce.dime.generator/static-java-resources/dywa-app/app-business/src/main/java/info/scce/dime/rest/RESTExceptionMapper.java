package info.scce.dime.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.UUID;

@Provider
public class RESTExceptionMapper implements ExceptionMapper<Exception> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RESTExceptionMapper.class);

    @Override
    public Response toResponse(Exception exception) {
        final String uuid = UUID.randomUUID().toString();
        LOGGER.error(uuid, exception);
        return Response.serverError().entity(uuid).build();
    }
}
