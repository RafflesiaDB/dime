package info.scce.dime.dad.checks

import graphmodel.IdentifiableElement
import graphmodel.Node
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.mcam.modules.checks.DADCheck
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.data.Type
import info.scce.dime.util.DataID
import info.scce.dime.dad.dad.DataComponent

class DataIDsCheck extends DADCheck {
	
	DAD dad
	
	override check(DAD model) {
		this.dad = model
		val dataModels = model.find(DataComponent).map[it.model]
		
		val types = 
			dataModels.flatMap[
				find(Type)
					.drop(ReferencedType)
					.drop(ReferencedUserType)
					.drop(ReferencedEnumType)
			]
		
		validateDataIds(types)
		
		val attrs = 
			dataModels.flatMap[
				find(Attribute)
					.filter[it.findParents(ReferencedType).isEmpty]
					.filter[it.findParents(ReferencedUserType).isEmpty]
					.filter[it.findParents(ReferencedEnumType).isEmpty]
			]
		
		validateDataIds(attrs)
	}
	
	def validateDataIds(Iterable<? extends Node> nodes) {
		val id16_on_node = <String,Node> newHashMap
		for (node : nodes) {
			val dataId = DataID.from(node)
			val id16 = dataId.escapedLowerCase16
			if (!id16_on_node.containsKey(id16)) {
				id16_on_node.put(id16, node)
			} else {
				val other = id16_on_node.get(id16)
				dad.find(DataComponent).findFirst[
					it.model == node.rootElement
				].addError(
					'''ID «node.id» of «node.displayName» conflicts with ID «other.id» of «other.displayName» (both map on 16 chars «id16»)'''
				)
			}
		}
	}
	
	def String displayName(IdentifiableElement elm) {
		switch elm {
			Data: "Model '" + elm.modelName + "'"
			Type: "Type '" + elm.name + "' in " + elm.rootElement.displayName
			Attribute: "Attribute '" + elm.name + "' of " + elm.findFirstParent(Type).displayName
			default: elm.class.simpleName
		}
	}
}