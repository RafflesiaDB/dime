package info.scce.dime.dad.checks;


import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.ProcessEntryPointComponent;
import info.scce.dime.dad.dad.StartupProcessPointer;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.dad.mcam.adapter.DADAdapter;
import info.scce.dime.dad.mcam.adapter.DADId;
import info.scce.dime.data.data.AbstractType;

public class NotAllowedNamesCheck extends AbstractCheck<DADId, DADAdapter> {

	
	@Override
	public void doExecute(DADAdapter adapter) {
		for (DADId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof URLProcess && ((URLProcess) obj).getIncoming(StartupProcessPointer.class).isEmpty()) {
				URLProcess processCmponent = (URLProcess) obj;
				if(processCmponent.getUrl()==null||processCmponent.getUrl().isEmpty()) {
					addError(id, "Value " + processCmponent.getUrl() + "for attribute 'url' must not be empty");
					continue;
				}
				if(processCmponent.getUrl().contains("/")) {
					addError(id, "Value " + processCmponent.getUrl() + "for attribute 'url' should not contain '/'");
					continue;
				}
				if(processCmponent.getUrl().contains(" ")) {
					addError(id, "Value " + processCmponent.getUrl() + "for attribute 'url' should not contain ' '");
					continue;
				}
				if (processCmponent.getUrl().equals("app")) {
					addError(id, "Value for attribute 'url' must not be 'app' (reserved entry point)");
					continue;
				}
				//check port type not abstract
				if(processCmponent instanceof ProcessComponent) {
					ProcessComponent pc = (ProcessComponent) processCmponent;
					pc.getModel()
					.getStartSIBs()
					.get(0)
					.getComplexOutputPorts()
					.stream()
					.filter((n)->n.getDataType() instanceof AbstractType)
					.forEach((n)->addError(id, "No Abstract ports allowed for route parameters"));
				}
				if(processCmponent instanceof ProcessEntryPointComponent) {
					ProcessEntryPointComponent pc = (ProcessEntryPointComponent) processCmponent;
					pc.getEntryPoint()
					.getComplexInputPorts()
					.stream()
					.filter((n)->n.getDataType() instanceof AbstractType)
					.forEach((n)->addError(id, "No Abstract ports allowed for route parameters"));
				}
			}
		}
		
	}
	
	@Override
	public void init() {}

}
