package info.scce.dime.dad.checks;

import info.scce.dime.checks.AbstractModelIntegrityCheck;
import info.scce.dime.dad.mcam.adapter.DADAdapter;
import info.scce.dime.dad.mcam.adapter.DADId;

public class ModelIntegrityCheck
	   extends AbstractModelIntegrityCheck<DADId, DADAdapter> {
}
