package info.scce.dime.dad.checks;

import java.util.HashSet;
import java.util.Set;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.dad.dad.StartupProcessPointer;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.dad.mcam.adapter.DADAdapter;
import info.scce.dime.dad.mcam.adapter.DADId;

public class UniqueNamesCheck extends AbstractCheck<DADId, DADAdapter> {

	@Override
	public void doExecute(DADAdapter adapter) {
		Set<String> urls = new HashSet<>();
		for (DADId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof URLProcess && ((URLProcess) obj).getIncoming(StartupProcessPointer.class).isEmpty()) {
				URLProcess processCmponent = (URLProcess) obj;
				if (!urls.add(processCmponent.getUrl()))
					addError(id, processCmponent.getUrl() + " not unique");
			}
		}
	}
	
	@Override
	public void init() {}

}