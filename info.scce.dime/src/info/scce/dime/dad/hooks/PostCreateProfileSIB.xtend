package info.scce.dime.dad.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.profile.profile.Profile
import info.scce.dime.dad.layout.Layouter

class PostCreateProfileSIB extends DIMEPostCreateHook<ProfileSIB> {
	
	extension Layouter = Layouter.instance
		
	override postCreate(ProfileSIB sib) {
		sib => [
			cacheNames
			newProfileIconButton(0,0)
			container.layout
		]
	}
	
	def boolean cacheNames(ProfileSIB sib) {
		val obj = sib.referencedProfile as Profile
		sib.cacheName(obj?.name)
		return obj !== null
	}
	
	def void cacheName(ProfileSIB sib, String name) {
		if (!name.nullOrEmpty
				&& sib.cachedReferencedProfileName != name) {
			sib.cachedReferencedProfileName = name
		}
	}
}