package info.scce.dime.dad.hooks

import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlight
import de.jabc.cinco.meta.runtime.hook.CincoPostSelectHook

import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.dad.dad.ProfileIconButton

import static org.eclipse.graphiti.util.IColorConstant.*

class ActivateButtonOnClick extends CincoPostSelectHook<ProfileIconButton> {
	
	override postSelect(ProfileIconButton button) {
		button => [
			animateClick
			diagram.clearSelection
			onClick
		]
	}
	
	def onClick(ProfileIconButton button) {
		switch it:button?.container {
			ProfileSIB: active = !isActive
		}
	}
	
	def animateClick(ProfileIconButton button) {
		var fgColor = GRAY
		var bgColor = WHITE
		if (button.container instanceof ProfileSIB) {
			val profileSIB = button.container as ProfileSIB
			fgColor = if (profileSIB.isActive) RED else GREEN
			bgColor = if (profileSIB.isActive) RED else GREEN
		}
		new Highlight(fgColor, bgColor).add(button).flash(0.5)
	}
}