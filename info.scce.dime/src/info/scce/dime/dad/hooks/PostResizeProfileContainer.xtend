package info.scce.dime.dad.hooks

import info.scce.dime.api.DIMEPostResizeHook
import info.scce.dime.dad.dad.ProfileContainer
import info.scce.dime.dad.layout.Layouter

class PostResizeProfileContainer extends DIMEPostResizeHook<ProfileContainer> {
	
	extension Layouter = Layouter.instance
	
	override postResize(ProfileContainer container, int direction, int width, int height) {
		container.layout
	}
	
}