package info.scce.dime.dad.hooks

import info.scce.dime.api.DIMEPostResizeHook
import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.dad.layout.Layouter

class PostResizeProfileSIB extends DIMEPostResizeHook<ProfileSIB> {
	
	extension Layouter = Layouter.instance
	
	override postResize(ProfileSIB sib, int direction, int width, int height) {
		sib.container.layout
	}
	
}