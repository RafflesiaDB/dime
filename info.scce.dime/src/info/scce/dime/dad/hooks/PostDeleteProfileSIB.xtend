package info.scce.dime.dad.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.dad.layout.Layouter

class PostDeleteProfileSIB extends CincoPostDeleteHook<ProfileSIB> {
	
	extension Layouter = Layouter.instance
	
	override getPostDeleteFunction(ProfileSIB sib) {
		val container = sib.container
		return [container.layout]
	}
	
}