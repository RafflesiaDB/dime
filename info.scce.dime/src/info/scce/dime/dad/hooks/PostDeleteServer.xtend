package info.scce.dime.dad.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import info.scce.dime.dad.dad.Server
import info.scce.dime.dad.layout.ServersLayouter

class PostDeleteServer extends CincoPostDeleteHook<Server> {
	
	extension ServersLayouter = ServersLayouter.instance
	
	override getPostDeleteFunction(Server server) {
		val container = server.container
		return [container.layout]
	}
	
}