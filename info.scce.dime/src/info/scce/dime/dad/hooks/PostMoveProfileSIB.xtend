package info.scce.dime.dad.hooks

import graphmodel.ModelElementContainer
import info.scce.dime.api.DIMEPostMoveHook
import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.dad.layout.Layouter

class PostMoveProfileSIB extends DIMEPostMoveHook<ProfileSIB> {
	
	extension Layouter = Layouter.instance
	
	override postMove(ProfileSIB sib, ModelElementContainer source, ModelElementContainer target, int x, int y, int deltaX, int deltaY) {
		sib.container.layout
	}
	
}