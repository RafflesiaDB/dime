package info.scce.dime.dad.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.dad.dad.Server
import info.scce.dime.dad.layout.ServersLayouter

class PostCreateServer extends DIMEPostCreateHook<Server> {
	
	extension ServersLayouter = ServersLayouter.instance
		
	override postCreate(Server server) {
		server.container.layout
	}
}