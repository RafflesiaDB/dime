package info.scce.dime.dad.hooks

import graphmodel.ModelElementContainer
import info.scce.dime.api.DIMEPostMoveHook
import info.scce.dime.dad.dad.Server
import info.scce.dime.dad.layout.ServersLayouter

class PostMoveServer extends DIMEPostMoveHook<Server> {
	
	extension ServersLayouter = ServersLayouter.instance
	
	override postMove(Server server, ModelElementContainer source, ModelElementContainer target, int x, int y, int deltaX, int deltaY) {
		server.container.layout
	}
	
}