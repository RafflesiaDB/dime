package info.scce.dime.dad.actions

import graphmodel.Node
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.dad.dad.ProfileSIB

class OpenModel extends DIMECustomAction<Node> {
	
	override execute(Node it) {
		println("Opening submodel for " + eClass.name)
		switch it {
			ProfileSIB: referencedProfile
		}?.openEditor
	}

	override hasDoneChanges() {
		false
	}
	
}