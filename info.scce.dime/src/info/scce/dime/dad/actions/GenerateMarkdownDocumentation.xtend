package info.scce.dime.dad.actions

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.dad.dad.DAD
import info.scce.dime.dad.dad.RootInteractionPointer
import info.scce.dime.dad.dad.StartupProcessPointer
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.Process
import java.io.File
import java.io.IOException
import org.apache.commons.io.FileUtils
import org.eclipse.emf.ecore.EObject

class GenerateMarkdownDocumentation<T extends DAD> extends DIMECustomAction<T> {
	
	override getName() {
		"Create Documentation"
	}

	override canExecute(DAD d) {
		true
	}
	
	override hasDoneChanges() {
		false
	}

	override void execute(DAD d) {
		val filePath = d.file.project.location.append('''../documentation/«d.appName».md'''.toString).toString
		createFile(createDoc(d).toString,filePath)
		d.dataComponents.map[model].forEach[new info.scce.dime.data.actions.GenerateMarkdownDocumentation().execute(it)]
		lookup(GUI).forEach[new info.scce.dime.gui.actions.GenerateMarkdownDocumentation().execute(it)]
		lookup(Process).forEach[new info.scce.dime.process.actions.GenerateMarkdownDocumentation().execute(it)]
		
	}
	
	/**
	 * Helper method to create a file with the given content on the given path.
	 * @param content
	 * @param path
	 * @throws IOException
	 */
	def void createFile(String content,String path) throws IOException
	{
		val File f = new File(path);
		f.getParentFile().mkdirs(); 
		f.createNewFile();
		
		FileUtils.writeStringToFile(f,content);
	}
	
	def createDoc(DAD d)
	'''
	# «d.appName»
	 
«««	«d.documentation.doc»
	 
	## Data model
	 
	«d.dataComponents.map['''* [«model.modelName»](./data/«model.modelName».md)'''].join("\n")»
	 
	## Root Interaction Process
		 
	«d.starts.map[getOutgoing(RootInteractionPointer)].flatten.map[targetElement].map['''* [«model.modelName»](./process/«model.modelName».md)'''].join("\n")»
	 
	## Startup process
	 
	«d.starts.map[getOutgoing(StartupProcessPointer)].flatten.map[targetElement].map['''* [«model.modelName»](./process/«model.modelName».md)'''].join("\n")»
	 
	## System User Type
		 
	«d.systemUsers.map['''* [«systemUser.name»](./data/«systemUser.rootElement.modelName»/«systemUser.name».md)'''].join("\n")»
	
	'''
	
	def <T extends EObject> lookup(Class<T> clazz) {
		ReferenceRegistry.instance.lookup(clazz)
	}
}
