package info.scce.dime.dad.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.dad.dad.LoginComponent

class OpenGUI extends DIMECustomAction<LoginComponent> {
	
	override void execute(LoginComponent s) {
		s.model.openEditor
	}
	
	override hasDoneChanges() {
		false
	}
}
