package info.scce.dime.dad.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.dad.dad.DataComponent

class OpenData extends DIMECustomAction<DataComponent> {
	
	override void execute(DataComponent s) {
		s.model.openEditor
	}

	override hasDoneChanges() {
		false
	}
}
