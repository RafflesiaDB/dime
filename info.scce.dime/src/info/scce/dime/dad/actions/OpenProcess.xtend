package info.scce.dime.dad.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.dad.dad.ProcessComponent
import graphmodel.Node
import info.scce.dime.dad.dad.FindLoginUserComponent
import info.scce.dime.dad.dad.ProcessEntryPointComponent

class OpenProcess extends DIMECustomAction<Node> {
	
	override void execute(Node s) {
		switch s {
			ProcessComponent: s.model.openEditor
			FindLoginUserComponent: s.model.openEditor
			ProcessEntryPointComponent: s.entryPoint.rootElement.openEditor
		}
	}
	
	override hasDoneChanges() {
		false
	}
}
