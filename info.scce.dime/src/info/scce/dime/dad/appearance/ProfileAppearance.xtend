package info.scce.dime.dad.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.dad.dad.ProfileSIB
import style.StyleFactory
import style.LineStyle

class ProfileAppearance implements StyleAppearanceProvider<ProfileSIB> {
	
	static val factory = StyleFactory.eINSTANCE
	
	static val black = factory.createAppearance => [ foreground = factory.createColor => [r=0 g=0 b=0] ]
	static val darkGray = factory.createAppearance => [ foreground = factory.createColor => [r=80 g=80 b=80] ]
	static val lightGray = factory.createAppearance => [ foreground = factory.createColor => [r=180 g=180 b=180] ]
	static val invalid = factory.createAppearance => [foreground = factory.createColor => [r=255] lineStyle = LineStyle.DASH ]
	
	override getAppearance(ProfileSIB sib, String shapeId) {
		if (shapeId == "rootShape") {
			if (sib.referencedProfile === null) {
				return invalid
			}
			return if (sib.isActive) black else darkGray
		}
		else if (shapeId == "label") {
			return if (sib.isActive) black else lightGray
		}
	}
	
}