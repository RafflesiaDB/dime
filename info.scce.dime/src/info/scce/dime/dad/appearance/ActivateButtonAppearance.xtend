package info.scce.dime.dad.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.dad.dad.ProfileIconButton
import style.StyleFactory
import info.scce.dime.dad.dad.ProfileSIB

class ActivateButtonAppearance implements StyleAppearanceProvider<ProfileIconButton> {
	
	static val factory = StyleFactory.eINSTANCE
	static val transparent = factory.createAppearance => [ transparency = 1.0 ]
	static val visible = factory.createAppearance => [ transparency = 0.0]
	
	override getAppearance(ProfileIconButton button, String shapeId) {
		if (button.container instanceof ProfileSIB) {
			val profileSIB = button.container as ProfileSIB
			if (shapeId == "imageActive") {
				return if (profileSIB.isActive) visible else transparent
			}
			if (shapeId == "imageInactive") {
				return if (profileSIB.isActive) transparent else visible
			}
		}
	}
	
	
}
