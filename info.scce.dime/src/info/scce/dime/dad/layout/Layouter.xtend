package info.scce.dime.dad.layout

import de.jabc.cinco.meta.runtime.layout.ListLayouter
import graphmodel.Node
import info.scce.dime.dad.dad.ProfileContainer
import info.scce.dime.dad.dad.ProfileIconButton
import info.scce.dime.dad.dad.ProfileSIB
import java.util.List

import static de.jabc.cinco.meta.runtime.layout.LayoutConfiguration.*

class Layouter extends ListLayouter {
	
	static Layouter _instance
	static def getInstance() {
		_instance ?: (_instance = new Layouter)
	}
	
	dispatch def getLayoutConfiguration(ProfileContainer container) {#{
		PADDING_TOP -> 30,
		PADDING_BOTTOM -> 10,
		PADDING_Y -> 5,
		MIN_HEIGHT -> 100,
		MIN_WIDTH -> 150
	}}
	
	dispatch def getLayoutConfiguration(ProfileSIB sib) {#{
		PADDING_TOP -> 0
	}}
	
	dispatch def List<? extends Class<? extends Node>> getIgnoredChildren(ProfileSIB container) {#[
		ProfileIconButton
	]}
}
