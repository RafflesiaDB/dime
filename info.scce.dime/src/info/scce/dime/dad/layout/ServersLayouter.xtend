package info.scce.dime.dad.layout

import de.jabc.cinco.meta.runtime.layout.ListLayouter
import graphmodel.Node
import info.scce.dime.dad.dad.ProfileIconButton
import info.scce.dime.dad.dad.ProfileSIB
import info.scce.dime.dad.dad.Servers
import java.util.List

import static de.jabc.cinco.meta.runtime.layout.LayoutConfiguration.*

class ServersLayouter extends ListLayouter {
	
	static ServersLayouter _instance
	static def getInstance() {
		_instance ?: (_instance = new ServersLayouter)
	}
	
	dispatch def getLayoutConfiguration(Servers container) {#{
		PADDING_TOP -> 30,
		PADDING_BOTTOM -> 10,
		PADDING_Y -> 5,
		MIN_HEIGHT -> 100,
		MIN_WIDTH -> 150
	}}
	
	dispatch def List<? extends Class<? extends Node>> getIgnoredChildren(ProfileSIB container) {#[
		ProfileIconButton
	]}
}
