package info.scce.dime.process.aps

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.process.process.AtomicSIB
import style.StyleFactory

class AtomicSIBAppearance implements StyleAppearanceProvider<AtomicSIB> {

	extension StyleFactory = StyleFactory.eINSTANCE

	override getAppearance(AtomicSIB sib, String elementName) {
		val iconPath = (sib.sib as info.scce.dime.siblibrary.SIB).iconPath
		if (elementName == "icon" && !iconPath.nullOrEmpty) {
			createAppearance => [
				imagePath = iconPath
			]
		}
	}
}
