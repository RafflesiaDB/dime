package info.scce.dime.process.aps

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter
import info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider
import info.scce.dime.process.process.GenericSIB
import org.eclipse.emf.ecore.EObject
import style.Appearance
import style.StyleFactory

import static info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider.*

class GenericSIBAppearance implements StyleAppearanceProvider<GenericSIB> {
	
	extension StyleFactory = StyleFactory.eINSTANCE
	
	override Appearance getAppearance(GenericSIB genericSIB, String attribute) {
		if (genericSIB.referencedObject !== null) {
			var IModeltrafoSupporter<EObject> trafoSupporter = getSupportedModelElement(genericSIB.referencedObject)
			if (trafoSupporter !== null) {
				val iconPath = trafoSupporter.iconPath
				if (!iconPath.nullOrEmpty) {				
					if (attribute.equals("icon")) {
						createAppearance => [
							imagePath = iconPath
						]
					}
				} else {
					if (GenericSIBGenerationProvider.isGUI(genericSIB.referencedObject)) {
						createAppearance => [
							imagePath = "icons/guiSIB.png"
						]
					}
				}
			}
		}
	}
}
