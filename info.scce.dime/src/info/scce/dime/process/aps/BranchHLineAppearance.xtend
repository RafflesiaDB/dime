package info.scce.dime.process.aps

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.dime.process.process.AbstractBranch
import style.StyleFactory

class BranchHLineAppearance implements StyleAppearanceProvider<AbstractBranch> {

	extension StyleFactory = StyleFactory.eINSTANCE

	val visible = createColor => [r=0 g=0 b=0]
	val invisible = createColor => [r=245 g=245 b=245]

	override getAppearance(AbstractBranch branch, String elementName) {
		if (elementName == "hline") {
			createAppearance => [
				foreground =
					if (!branch.rootElement.dataFlowView || branch.outputPorts.isEmpty) 
						invisible
					else
						HLineColor
			]
		}
	}

	def getHLineColor() {
		visible
	}
}
