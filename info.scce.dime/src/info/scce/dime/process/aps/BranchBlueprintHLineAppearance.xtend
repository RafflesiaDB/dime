package info.scce.dime.process.aps

import style.StyleFactory

class BranchBlueprintHLineAppearance extends BranchHLineAppearance {

	val visible = StyleFactory.eINSTANCE.createColor => [r=42 g=40 b=180]
	
	override getHLineColor() {
		visible
	}

}
