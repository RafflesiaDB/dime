package info.scce.dime.process.vp

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import graphmodel.internal.InternalNode
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.helper.GUIExtensionProvider
import info.scce.dime.process.process.IgnoreBranch
import info.scce.dime.process.process.SIB
import java.util.Map

import static java.util.Comparator.comparing

class IgnoreBranchPossibleValuesProvider extends CincoValuesProvider<IgnoreBranch, String> {
	
	extension GUIExtension = GUIExtensionProvider.guiextension
	
	override Map<String, String> getPossibleValues(IgnoreBranch ib) {
		val sib = ((ib.eContainer as InternalNode).element as SIB)
		val ignorableBranches = <String,String> newTreeMap(comparing[it?.toLowerCase])
		sib.getIgnorableBranches(ignorableBranches)
		
		val ignoredBranches = sib.ignoredBranch.map[name].filterNull
		val usedBranches = sib.branchSuccessors.map[name].filterNull
		for (branch : ignoredBranches + usedBranches) {
			if (branch != ib.name) {
				ignorableBranches.remove(branch)
			}
		}
		
		return ignorableBranches
	}
}