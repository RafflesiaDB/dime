package info.scce.dime.process.vp

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.GenericSIB
import info.scce.dime.process.process.GUISIBOptions
import info.scce.dime.process.process.SIB
import java.util.Map
import graphmodel.internal.InternalNode

class GenericMajorPageDefaultContentPossibleValuesProvider extends CincoValuesProvider<GUISIBOptions, SIB>{
	extension ProcessExtension = new ProcessExtension
	
	override Map<SIB, String> getPossibleValues(GUISIBOptions options) {
		val GenericSIB genericSIB = (options.eContainer as InternalNode).element as GenericSIB
		if (!options.isMajorPage)
			#{}
		else genericSIB.container.getModelElements(SIB).filter[isMinor].toMap([it],[label])
	}
}