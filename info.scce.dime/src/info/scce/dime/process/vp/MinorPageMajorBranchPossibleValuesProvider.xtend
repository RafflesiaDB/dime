package info.scce.dime.process.vp

import info.scce.dime.api.DIMEValuesProvider
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.SIB
import java.util.Map

class MinorPageMajorBranchPossibleValuesProvider extends DIMEValuesProvider<SIB, AbstractBranch> {
	
	extension ProcessExtension = new ProcessExtension
	
	override Map<AbstractBranch, String> getPossibleValues(SIB sib) {
		if (!sib.isMinor)
			#{} 
		else sib.rootElement.find(AbstractBranch)
			.filter[ switch it:SIBPredecessors.head {
				GUISIB case isMajorPage && defaultContent !== null: true
				GUIBlueprintSIB case isMajorPage && defaultContent !== null: true
				default: false
			}].toSet.toMap(
				[it], ['''«it.SIBPredecessors.head.label»: «it.name»'''.toString]
			)
	}
}