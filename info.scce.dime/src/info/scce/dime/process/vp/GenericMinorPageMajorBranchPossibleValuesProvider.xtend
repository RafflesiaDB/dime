package info.scce.dime.process.vp

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.GenericSIB
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.GUISIBOptions
import java.util.Map
import graphmodel.internal.InternalNode

class GenericMinorPageMajorBranchPossibleValuesProvider extends CincoValuesProvider<GUISIBOptions, AbstractBranch>{
	
	override Map<AbstractBranch, String> getPossibleValues(GUISIBOptions options) {
		val GenericSIB genericSIB = (options.eContainer as InternalNode).element as GenericSIB
		if (options.isMajorPage)
			#{} 
		else genericSIB.rootElement.find(AbstractBranch)
			.filter[ switch it:SIBPredecessors.head {
				GUISIB case isMajorPage && defaultContent !== null: true
				GUIBlueprintSIB case isMajorPage && defaultContent !== null: true
				GenericSIB case guiOptions !== null && guiOptions.isMajorPage && guiOptions.defaultContent !== null: true
				default: false
			}].toSet.toMap(
				[it], ['''«it.SIBPredecessors.head.label»: «it.name»'''.toString]
			)
	}
}