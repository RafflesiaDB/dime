package info.scce.dime.process.vp;

import java.util.HashMap;
import java.util.Map;

import info.scce.dime.api.DIMEValuesProvider;
import info.scce.dime.process.process.BlueprintSIB;
import info.scce.dime.process.process.BranchBlueprint;

public class BranchBlueprintDefaultBranchValuesProvider extends DIMEValuesProvider<BlueprintSIB, BranchBlueprint>{

	@Override
	public Map<BranchBlueprint,String> getPossibleValues(BlueprintSIB sib) {
		Map<BranchBlueprint,String> collect = new HashMap<BranchBlueprint, String>();
		
		sib.getSuccessors(BranchBlueprint.class).forEach(n->collect.put(n, n.getName()));
		
		return collect;
	}

}
