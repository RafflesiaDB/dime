package info.scce.dime.process.vp

import info.scce.dime.api.DIMEValuesProvider
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.SIB
import java.util.Map

class MajorPageDefaultContentPossibleValuesProvider extends DIMEValuesProvider<SIB, SIB> {
	
	extension ProcessExtension = new ProcessExtension
	
	override Map<SIB, String> getPossibleValues(SIB sib) {
		if (sib.isMinor)
			#{}
		else sib.container.getModelElements(SIB).filter[isMinor].toMap([it],[label])
	}
}