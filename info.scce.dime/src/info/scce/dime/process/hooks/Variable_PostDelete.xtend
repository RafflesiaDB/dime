package info.scce.dime.process.hooks

import info.scce.dime.process.process.Variable
import info.scce.dime.process.process.DataContext
import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook

class Variable_PostDelete extends CincoPostDeleteHook<Variable>{
		val OFFSET = 30
	
	def layout(DataContext dataContext) {
		var maxHeight = maxHeight(dataContext);
		dataContext.setHeight(maxHeight + OFFSET);
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
	override getPostDeleteFunction(Variable node) {
		if(node.container instanceof DataContext){
			val dataContext  = node.container as DataContext
			return [dataContext.layout]
		}
	}
	
}