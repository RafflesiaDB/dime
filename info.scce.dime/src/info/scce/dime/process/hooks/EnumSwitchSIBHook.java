package info.scce.dime.process.hooks;

import info.scce.dime.data.data.EnumType;
import info.scce.dime.process.build.EnumSwitchSIBBuild;
import info.scce.dime.process.process.EnumSwitchSIB;

public class EnumSwitchSIBHook extends AbstractPostCreateSIBHook<EnumSwitchSIB>{

	@Override
	boolean initialize(EnumSwitchSIB sib) {

		EnumType switchedType = sib.getSwitchedType();

		setLabel("Switch " + switchedType.getName());

		return EnumSwitchSIBBuild.initialize(sib);
	}

}
