package info.scce.dime.process.hooks;

import info.scce.dime.gui.gui.GUI;
import info.scce.dime.process.build.GuiSIBBuild;
import info.scce.dime.process.process.GUISIB;

public class GUISIBHook extends AbstractPostCreateSIBHook<GUISIB> {

	@Override
	boolean initialize(GUISIB sib) {

		GUI primeGUI = sib.getGui();
		
		setLabel(primeGUI.getTitle());
		sib.setName(primeGUI.getTitle());
		GuiSIBBuild gBuilder = new GuiSIBBuild(sib);
		gBuilder.createEventListeners(sib);
		
		
		return GuiSIBBuild.initialize(sib);
	}

}
