package info.scce.dime.process.hooks;

import info.scce.dime.process.process.PutToContextSIB;

public class PutToContextSIBHook extends AbstractPostCreateSIBHook<PutToContextSIB>{

	@Override
	boolean initialize(PutToContextSIB sib) {
		
		setLabel("PutToContext");
    		addBranch("success");
    		
    		return true;
	}

}
