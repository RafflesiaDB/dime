package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.PortUtils.calcName;

import graphmodel.Node;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.process.helper.SIBLayoutUtils;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.OutputPort;

public class OutputPortHook extends DIMEPostCreateHook<OutputPort>{

	final static int OFFSET = 30;
	
	@Override
	public void postCreate(OutputPort port) {
		try {
			port.setName(calcName(port));
			SIBLayoutUtils.resizeAndLayout(port.getContainer());
     		if (port.getContainer() instanceof DataFlowSource) {
     			DataFlowSource sib = (DataFlowSource) port.getContainer();
     			layout(sib);
     		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void layout(DataFlowSource sib) {
		int newY = sib.getY() + sib.getHeight() + OFFSET;
		for (Node node : sib.getSuccessors()) {
			if(
					node.getY()<=(sib.getY()+sib.getHeight()) &&
					node.getY()>=sib.getY() &&
					node.getX()<=(sib.getX()+sib.getWidth()) &&
					node.getX()>=sib.getX()
			) {
				//branch is grown into the successor
				node.moveTo(sib.getRootElement(), node.getX(), newY);
			}
		}
	}
}
