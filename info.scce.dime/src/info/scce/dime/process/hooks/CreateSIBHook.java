package info.scce.dime.process.hooks;

import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.process.process.CreateSIB;
import info.scce.dime.process.process.PrimitiveType;

public class CreateSIBHook extends AbstractPostCreateSIBHook<CreateSIB>{

	@Override
	boolean initialize(CreateSIB sib) {

		ConcreteType createdType = sib.getCreatedType();

		setLabel("Create " + createdType.getName());
		addPrimitiveInputPort("internalName", PrimitiveType.TEXT, false);
		
		addBranch("success");
		addComplexOutputPort("created", createdType, false);
		
		return true;
	}
		

}
