package info.scce.dime.process.hooks;

import java.util.Optional;

import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.UserAssociation;
import info.scce.dime.process.process.RetrieveCurrentUserSIB;

public class RetrieveCurrentUserSIBHook extends AbstractPostCreateSIBHook<RetrieveCurrentUserSIB>{

	@Override
	boolean initialize(RetrieveCurrentUserSIB sib) {
		
		setLabel("Retrieve Current User");
		
		addBranch("success");
		
		Optional<UserAssociation> userAssoc = 
				sib.getCurrentUser().getOutgoing(UserAssociation.class).stream().findFirst();
		if (userAssoc.isPresent() && userAssoc
				.map(assoc -> assoc.getTargetElement() instanceof ConcreteType
						&& ((ConcreteType) assoc.getTargetElement()).getIncoming(Inheritance.class).isEmpty())
				.orElse(false)) {
			
			addComplexOutputPort("currentUser", ((ConcreteType) userAssoc.get().getTargetElement()), false);
		}

		addBranch("not authenticated");
		
		return true;
	}

}
