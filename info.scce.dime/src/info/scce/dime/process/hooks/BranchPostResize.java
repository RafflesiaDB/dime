package info.scce.dime.process.hooks;

import info.scce.dime.process.helper.LayoutConstants;
import info.scce.dime.process.process.AbstractBranch;
import info.scce.dime.process.process.Output;

public class BranchPostResize extends ProcessNodePostResize<AbstractBranch>{

	@Override
	public void postResize(AbstractBranch cModelElement, int direction, int width, int height) {
		super.postResize(cModelElement, direction, width, height);
		
		for (Output o : cModelElement.getOutputs()) {
			o.resize(width - LayoutConstants.PORT_X*2, o.getHeight());
		}
	}

}
