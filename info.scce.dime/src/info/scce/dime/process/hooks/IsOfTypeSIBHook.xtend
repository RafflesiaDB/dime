package info.scce.dime.process.hooks

import info.scce.dime.process.process.IsOfTypeSIB
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension

class IsOfTypeSIBHook extends AbstractPostCreateSIBHook<IsOfTypeSIB> {
	
	extension WorkbenchExtension = new WorkbenchExtension
	
	override package boolean initialize(IsOfTypeSIB sib) {
				
		val checkedType = sib.checkedType
		
		setLabel('''Is a «checkedType.name»''')

		val rootType = if(checkedType.rootTypes.size == 1) {
			checkedType.rootTypes.head
		}
		else {
			val rootTypes = checkedType.rootTypes.toList
			rootTypes.get(
				showCustomQuestionDialog(
					"Please Choose", 
					"Choose a root type please...", 
					rootTypes.map[name]
				)
			)
		}
		
		addComplexInputPort("instance", rootType, false)

		addBranch("yes")
		addComplexOutputPort("casted", checkedType, false)
		
		addBranch("no")
		
		true
	}
}
