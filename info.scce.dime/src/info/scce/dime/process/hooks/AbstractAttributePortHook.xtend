package info.scce.dime.process.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.data.data.InheritorType
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.process.process.CreateSIB
import info.scce.dime.process.process.CreateUserSIB
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.RetrieveOfTypeSIB
import info.scce.dime.process.process.TransientCreateSIB
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.dialogs.MessageDialog

import static info.scce.dime.process.process.PrimitiveType.*
import info.scce.dime.process.process.SIB
import info.scce.dime.data.data.Inheritance
import java.util.Set

abstract class AbstractAttributePortHook<T extends EObject> extends DIMEPostCreateHook<T> {
	def getInputs(SIB sib) { (sib as DataFlowTarget).inputs }
	
	def getType(SIB sib) {
		switch(it: sib) {
			CreateSIB: createdType
			TransientCreateSIB: createdType
			CreateUserSIB: createdType 
			RetrieveOfTypeSIB: retrievedType
		}
	}
	
	def getTypeHierarchy(SIB sib) {
		val type = sib.type
		if (type != null)
			#[type] + type.superTypes
	}
	
	def Iterable<Type> getSuperTypes(Type type, Set<Type> collection) {
		switch it:type {
			InheritorType: findTargetsOf(Inheritance).filter(Type).map[#[it] + superTypes].flatten
			default: #[]
		}
	}
	
	def isTypeSIB(SIB sib) {
		switch(it: sib) {
			CreateSIB,
			TransientCreateSIB,
			CreateUserSIB, 
			RetrieveOfTypeSIB: true
			default: false
		}
	}
	
	def toProcessPrimitive(PrimitiveType type) {
		switch (type) {
			case BOOLEAN: BOOLEAN
			case INTEGER: INTEGER
			case REAL: REAL
			case TEXT: TEXT
			case TIMESTAMP: TIMESTAMP
			case FILE: FILE
			default: throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.")
		}
	}
	
	def showError(String title, String body) {
		MessageDialog.openError(null, title, body)
	}
}