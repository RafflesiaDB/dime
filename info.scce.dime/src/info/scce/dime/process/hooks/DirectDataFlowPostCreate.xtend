package info.scce.dime.process.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.process.helper.EdgeLayoutUtils.Location
import info.scce.dime.process.process.DirectDataFlow

import static info.scce.dime.process.helper.EdgeLayoutUtils.getManhattanPoints

class DirectDataFlowPostCreate extends DIMEPostCreateHook<DirectDataFlow> {
	
	override postCreate(DirectDataFlow edge) {
		for (Location p : getManhattanPoints(edge.sourceElement, edge.targetElement)) {
			edge.addBendpoint(p.x, p.y)
		}
	}
}
