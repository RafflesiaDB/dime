package info.scce.dime.process.hooks;

import info.scce.dime.data.data.Type;
import info.scce.dime.process.process.PutComplexToContextSIB;

public class PutComplexToContextSIBHook extends AbstractPostCreateSIBHook<PutComplexToContextSIB>{

	@Override
	boolean initialize(PutComplexToContextSIB sib) {

		Type putType = sib.getPutType();
		
		setLabel("Put " + putType.getName());
		addComplexInputPort("in", putType, false);
		
		addBranch("success");
		addComplexOutputPort("out", putType, false);
		
		return true;
	}
	
}
