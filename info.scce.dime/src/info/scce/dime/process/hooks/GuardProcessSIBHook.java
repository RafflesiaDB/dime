package info.scce.dime.process.hooks;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserAttribute;
import info.scce.dime.process.build.GuardProcessSIBBuild;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.GuardContainer;
import info.scce.dime.process.process.GuardProcessSIB;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;
import info.scce.dime.process.process.SIB;
import info.scce.dime.process.process.StartSIB;

public class GuardProcessSIBHook extends AbstractPostCreateSIBHook<SIB>{
	
	@Override
	void preProcess(SIB s) throws StopHookExecution {
		super.preProcess(s);
		if(s instanceof GuardProcessSIB){
			GuardProcessSIB sib = (GuardProcessSIB)s;
			Process primeModel = sib.getSecurityProcess();
			
			if (ProcessType.SECURITY != primeModel.getProcessType()) {
				sib.delete();
				stop("Only Security processes can be guards.");
			}
			
			if (primeModel.getStartSIBs().isEmpty()) {
				sib.delete();
				stop("Referenced process model does not contain a Start SIB");
			} else {
				if (primeModel.getStartSIBs().size() > 1) {
					showError("Referenced process model contains multiple Start SIBs");
				}
				
				StartSIB startSIB = primeModel.getStartSIBs().get(0);
				
				List<Output> currentUsers = startSIB.getOutputs().stream()
						.filter(o -> "currentUser".equals(o.getName()))
						.collect(Collectors.toList());
				
				if (currentUsers.size() < 1) {
					sib.delete();
					stop("Invalid Security Process. Must have one input 'currentUser'.");
				}
				
				if (currentUsers.size() > 1) {
					sib.delete();
					stop("Invalid Security Process. Must not have more than one input 'currentUser'.");
				}
				
				if (currentUsers.get(0) instanceof ComplexOutputPort) {
					Type currentUserType = ((ComplexOutputPort) currentUsers.get(0)).getDataType();
					if (getUserAttribute(currentUserType) == null) {
						sib.delete();
						stop(String.format("Invalid Security Process. "
								+ "'currentUser' type '%s' is not a valid user type",
								currentUserType.getName()));
					}
				} else {
					sib.delete();
					stop("Invalid Security Process. 'currentUser' type is not a a valid user type");
				}
			}
		}
	}
	
	@Override
	boolean initialize(SIB s) {
		if(s instanceof GuardProcessSIB)
		{
			GuardProcessSIB sib = (GuardProcessSIB)s;
			
			Process primeModel = sib.getSecurityProcess();
			
			setLabel(primeModel.getModelName());
			
			if(GuardProcessSIBBuild.initialize(sib)){
				if (sib.getRootElement().getProcessType() == ProcessType.BASIC) {
					GuardContainer cgc = (GuardContainer) sib.getContainer();
					if (!cgc.getAbstractBranchSuccessors().stream()
							.filter(n -> n.getName().equals("denied")).findAny()
							.isPresent()) {
						this.sib = cgc;
						addBranch("denied");
					}
				}
				return true;
			}
			return false;
			
		}
		return true;
	}
	
	private UserAttribute getUserAttribute(Type t) {
		EList<UserAttribute> userAttributes = t.getUserAttributes();
		if (userAttributes.size() != 0) {
			return userAttributes.get(0);
		}
		else for (Inheritance isAEdge : t.getOutgoing(Inheritance.class)) {
			UserAttribute parentUserAttribute = getUserAttribute(((Type)isAEdge.getTargetElement()));
			if (parentUserAttribute != null) {
				return parentUserAttribute;
			}
		}
		return null;
	}
}
