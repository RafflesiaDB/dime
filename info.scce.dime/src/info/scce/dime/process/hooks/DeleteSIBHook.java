package info.scce.dime.process.hooks;

import info.scce.dime.process.process.DeleteSIB;

public class DeleteSIBHook extends AbstractPostCreateSIBHook<DeleteSIB>{

	@Override
	boolean initialize(DeleteSIB sib) {
		setLabel("Delete");
		addBranch("deleted");
		
		return true;
	}
}
