package info.scce.dime.process.hooks;

import static info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider.getSupportedModelElement;
import static info.scce.dime.process.helper.PortUtils.toPrimitiveType;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;

import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericComplexPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericJavaNativePort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericOutputBranch;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPrimitivePort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericSIBBuild;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.modeltrafo.extensionpoint.transformation.GenericSIBGenerationProvider;
import info.scce.dime.process.build.BranchCreator;
import info.scce.dime.process.build.InitializationBranchCreator;
import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.GenericSIB;
import info.scce.dime.process.process.JavaNativeInputPort;
import info.scce.dime.process.process.JavaNativeOutputPort;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.ProcessFactory;

public class GenericSIBHook extends AbstractPostCreateSIBHook<GenericSIB> {

	@Override
	boolean initialize(GenericSIB sib) {
		EObject sibReference = sib.getReferencedObject();
		IModeltrafoSupporter<EObject> modelTrafoSupporter = getSupportedModelElement(sibReference);
		if (modelTrafoSupporter != null) {
			IGenericSIBBuilder<EObject> modellingProvider = modelTrafoSupporter.getModellingProvider();
			GenericSIBBuild<EObject> sibBuild = modellingProvider.getSIBBuild(sibReference);
			List<GenericPort> genericInputPorts = sibBuild.getInputPorts();
			createInputPorts(genericInputPorts, sib);
			List<GenericOutputBranch> genericOutputBranches = sibBuild.getOutputBranches();
			createOutputBranches(genericOutputBranches, sib);
			sib.setName(modellingProvider.getName(sibReference));
			sib.setLabel(modellingProvider.getLabel(sibReference));
			if (GenericSIBGenerationProvider.isGUI(sibReference)) {
				sib.setGuiOptions(ProcessFactory.eINSTANCE.createGUISIBOptions());
			}
		} else {
			MessageDialog.openError(null, "No Modeltrafo Provider found",
					"No Modeltrafo Provider found for " + sibReference.eClass().getName() + "!");
			sib.delete();
		}
		return true;
	}

	public static void createInputPorts(List<GenericPort> genericInputPorts, GenericSIB sib) {
		for (GenericPort genericPort : genericInputPorts) {
			if (genericPort instanceof GenericPrimitivePort) {
				createPrimitiveInputPort(genericPort, sib);
			} else if (genericPort instanceof GenericComplexPort) {
				createComplexInputPort(genericPort, sib);
			} else if (genericPort instanceof GenericJavaNativePort) {
				createJavaNativeInputPort(genericPort, sib);
			}
		}
	}
	
	public static void createPrimitiveInputPort(GenericPort genericInputPort,  GenericSIB sib) {
		PrimitiveInputPort port = sib.newPrimitiveInputPort(0, 0);
		port.setName(genericInputPort.getName());
		port.setDataType(toPrimitiveType(((GenericPrimitivePort) genericInputPort).getType()));
		port.setIsList(genericInputPort.isList());
	}

	public static void createComplexInputPort(GenericPort genericInputPort, GenericSIB sib) {
		GenericComplexPort complexPort = (GenericComplexPort) genericInputPort;
		ComplexInputPort port = sib.newComplexInputPort(complexPort.getType(), 0, 0);
		port.setName(complexPort.getName());
		port.setIsList(genericInputPort.isList());
	}
	
	public static void createJavaNativeInputPort(GenericPort genericInputPort, GenericSIB sib) {
		GenericJavaNativePort complexPort = (GenericJavaNativePort) genericInputPort;
		JavaNativeInputPort port = sib.newJavaNativeInputPort(complexPort.getType(), 0, 0);
		port.setName(complexPort.getName());
		port.setIsList(genericInputPort.isList());
	}
	
	public void createOutputBranches(List<GenericOutputBranch> genericOutputBranches, GenericSIB sib) {
		BranchCreator branchCreator = new InitializationBranchCreator(sib);
		for (GenericOutputBranch genericOutputBranch : genericOutputBranches) {
			Branch outputBranch = branchCreator.getNewBranch();
			outputBranch.setName(genericOutputBranch.getName());
			createOutputPorts(genericOutputBranch.getOutputPorts(), outputBranch);
		}
	}
	
	public static void createOutputPorts(List<GenericPort> genericOutputPorts, Branch outputBranch) {
		for (GenericPort genericPort : genericOutputPorts) {
			if (genericPort instanceof GenericPrimitivePort) {
				createPrimitveOutputPort(genericPort, outputBranch);
			} else if (genericPort instanceof GenericComplexPort) {
				createComplexOutputPort(genericPort, outputBranch);
			} else if (genericPort instanceof GenericJavaNativePort) {
				createComplexOutputPort(genericPort, outputBranch);
			}
		}
	}
	
	public static void createPrimitveOutputPort(GenericPort genericOutputPort, Branch outputBranch) {
		PrimitiveOutputPort port = outputBranch.newPrimitiveOutputPort(0, 0);
		port.setName(genericOutputPort.getName());
		port.setDataType(toPrimitiveType(((GenericPrimitivePort) genericOutputPort).getType()));
		port.setIsList(genericOutputPort.isList());
	}
	
	public static void createComplexOutputPort(GenericPort genericOutputPort, Branch outputBranch) {
		GenericComplexPort complexPort = (GenericComplexPort) genericOutputPort;
		ComplexOutputPort port = outputBranch.newComplexOutputPort(complexPort.getType(), 0, 0);
		port.setName(complexPort.getName());
		port.setIsList(genericOutputPort.isList());
	}
	
	public static void createJavaNativeOutputPort(GenericPort genericOutputPort, Branch outputBranch) {
		GenericJavaNativePort complexPort = (GenericJavaNativePort) genericOutputPort;
		JavaNativeOutputPort port = outputBranch.newJavaNativeOutputPort(complexPort.getType(), 0, 0);
		port.setName(complexPort.getName());
		port.setIsList(genericOutputPort.isList());
	}

}
