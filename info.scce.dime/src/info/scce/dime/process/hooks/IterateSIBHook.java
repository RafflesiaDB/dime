package info.scce.dime.process.hooks;

import info.scce.dime.data.data.Type;
import info.scce.dime.process.process.IterateSIB;

public class IterateSIBHook extends AbstractPostCreateSIBHook<IterateSIB>{

	@Override
	boolean initialize(IterateSIB sib) {

   		Type iteratedType = sib.getIteratedType();
   		
   		setLabel("Iterate " + iteratedType.getName() + "s");
   		
   		addComplexInputPort("list", iteratedType, true);
   		
   		addBranch("next");
   		addComplexOutputPort("element", iteratedType, false);
   		
   		addBranch("exit");
   		
   		return true;
	}

}
