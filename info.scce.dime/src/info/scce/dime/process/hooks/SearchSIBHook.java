package info.scce.dime.process.hooks;

import java.util.Arrays;

import info.scce.dime.process.process.SearchSIB;
import info.scce.dime.search.search.Search;
import info.scce.dime.search.search.SearchInterface;

public class SearchSIBHook extends AbstractPostCreateSIBHook<SearchSIB> {

	@Override
	boolean initialize(SearchSIB sib) {

		Search lcSearch = sib.getSearch();
		setLabel(lcSearch.getModelName());

		SearchInterface searchInterface = lcSearch.getSearchInterfaces().get(0);
		
		addInputPortsForReferences(searchInterface.getInputParameters());
		
		addBranch("found");
		addOutputPortsForReferences(Arrays.asList(searchInterface.getOutputParameters().get(0)));

		addBranch("not found");
		
		return true;
	}

}
