package info.scce.dime.process.hooks;

import info.scce.dime.api.DIMEPostResizeHook;
import info.scce.dime.process.helper.LayoutConstants;
import info.scce.dime.process.process.Attribute;
import info.scce.dime.process.process.ComplexVariable;

public class ComplexVariablePostResize extends DIMEPostResizeHook<ComplexVariable>{

	@Override
	public void postResize(ComplexVariable cVar, int direction, int width, int height) {
		for (Attribute a : cVar.getAttributes()) {
			a.resize(width - LayoutConstants.VAR_ATTR_X*2, a.getHeight());
		}
	}

}
