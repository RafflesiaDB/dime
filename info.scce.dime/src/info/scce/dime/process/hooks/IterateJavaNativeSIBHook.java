package info.scce.dime.process.hooks;

import info.scce.dime.process.process.IterateJavaNativeSIB;
import info.scce.dime.siblibrary.Type;

public class IterateJavaNativeSIBHook extends AbstractPostCreateSIBHook<IterateJavaNativeSIB>{

	@Override
	boolean initialize(IterateJavaNativeSIB sib) {

   		Type iteratedType = (Type) sib.getIteratedType();
   		
   		setLabel("Iterate " + iteratedType.getName());
   		
   		addJavaNativeInputPort("list", iteratedType, true);
   		
   		addBranch("next");
   		addJavaNativeOutputPort("element", iteratedType, false);
   		
   		addBranch("exit");
   		
   		return true;
	}

}
