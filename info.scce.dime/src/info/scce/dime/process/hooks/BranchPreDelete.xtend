package info.scce.dime.process.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPreDeleteHook
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.EnumSwitchSIB
import info.scce.dime.process.build.EnumSwitchSIBBuild

class BranchPreDelete extends CincoPreDeleteHook<Branch> {

	override preDelete(Branch branch) {
		/**
		 * After deleting a branch an 'else' branch should be created.
		 */
		var enumSib = branch.predecessors.filter(EnumSwitchSIB).head
		if (enumSib != null) {
			if (!enumSib.branchSuccessors.exists[name == "else"]) { // no else branch exists
			// create new else branch
				val enumSwitch = new EnumSwitchSIBBuild(enumSib)
				enumSwitch.addElseBranch(enumSib)
			}
		}
	}

}
