package info.scce.dime.process.hooks;

import java.util.Optional;

import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.UserAssociation;
import info.scce.dime.process.process.RetrieveCurrentUserSIB;
import info.scce.dime.process.process.SwitchToOriginalUserSIB;

public class SwitchToOriginalUserSIBHook extends AbstractPostCreateSIBHook<SwitchToOriginalUserSIB>{

	@Override
	boolean initialize(SwitchToOriginalUserSIB sib) {
		
		setLabel("Switch To Original User");
		
		addBranch("success");
		
		Optional<UserAssociation> userAssoc = 
				sib.getCurrentUser().getOutgoing(UserAssociation.class).stream().findFirst();
		if (userAssoc.isPresent() && userAssoc
				.map(assoc -> assoc.getTargetElement() instanceof ConcreteType
						&& ((ConcreteType) assoc.getTargetElement()).getIncoming(Inheritance.class).isEmpty())
				.orElse(false)) {
			
			addComplexOutputPort("originalUser", ((ConcreteType) userAssoc.get().getTargetElement()), false);
		}

		
		return true;
	}

}
