package info.scce.dime.process.hooks

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.dime.process.process.EnumSwitchSIB
import info.scce.dime.process.build.EnumSwitchSIBBuild
import java.util.ArrayList

class EnumSwitchResetBranches extends CincoCustomAction<EnumSwitchSIB>{
	
	
	override execute(EnumSwitchSIB sib) {
		sib.branchSuccessors.filter[name == "else"].head.delete
		val eBuilder = new EnumSwitchSIBBuild(sib)
		var missingBranches = findMissingBranches(sib)
		missingBranches.forEach[eBuilder.addBranches(it)]
	}
	
	def findMissingBranches(EnumSwitchSIB sib) {
		var missingBranches = new ArrayList
		for(literal : sib.switchedType.enumLiterals){
			if(!sib.branchSuccessors.exists[name == literal.name]){
				missingBranches.add(literal.name)
			}
		}
		return missingBranches
	}
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Reset branches"
	}
	
	
}