package info.scce.dime.process.hooks;

import info.scce.dime.data.data.Type;
import info.scce.dime.process.process.RetrieveOfTypeSIB;

public class RetrieveOfTypeSIBHook extends AbstractPostCreateSIBHook<RetrieveOfTypeSIB> {

	@Override
	boolean initialize(RetrieveOfTypeSIB sib) {

		Type retrievedType = sib.getRetrievedType();

		setLabel("Retrieve from " + retrievedType.getName());

		addBranch("success");
		addComplexOutputPort("retrieved", retrievedType, true);

		addBranch("none found");
		
		return true;
	}

}
