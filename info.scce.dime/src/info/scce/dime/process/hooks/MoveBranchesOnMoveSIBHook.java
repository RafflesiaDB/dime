package info.scce.dime.process.hooks;

import graphmodel.ModelElementContainer;
import info.scce.dime.api.DIMEPostMoveHook;
import info.scce.dime.process.process.AbstractBranch;
import info.scce.dime.process.process.SIB;

public class MoveBranchesOnMoveSIBHook extends DIMEPostMoveHook<SIB> {

	@Override
	public void postMove(SIB sib, ModelElementContainer sourceContainer, ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		
		for (AbstractBranch branch : sib.getAbstractBranchSuccessors()) {
			
			// TODO document what happens here
		}
		
	}

}
