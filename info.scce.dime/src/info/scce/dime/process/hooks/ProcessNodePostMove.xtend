package info.scce.dime.process.hooks

import graphmodel.ModelElement
import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.dime.api.DIMEPostMoveHook
import info.scce.dime.process.process.DataContext

import static java.lang.Math.abs
import static java.lang.Math.min
import static java.lang.Math.max
import graphmodel.internal.InternalModelElement
import org.eclipse.swt.widgets.Display

abstract package class ProcessNodePostMove<E extends ModelElement> extends DIMEPostMoveHook<E> {
	
	override postMove(E node, ModelElementContainer sourceContainer,
			ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		if (node instanceof Node && targetContainer == node.rootElement) {
			updateDataContext(node as Node, deltaY)
		}
	}
	
	def updateDataContext(Node movedNode, int deltaY) {
		if (Display.current === null) {
			return // only resize if triggered by changes in the UI editor
		}
		val model = movedNode.rootElement
		val nodes = model.allNodes.drop(DataContext)
		if (!nodes.nullOrEmpty) {
			val topNode = nodes.sortBy[y].head
			val movedNodeOldY = movedNode.y - deltaY
			val scndTopNode = nodes.filter[it != movedNode].sortBy[y].head ?: topNode
			val oldMin = min(movedNodeOldY, scndTopNode.y)
			val botNode = nodes.sortBy[y + height].reverse.head
			val scndBotNode = nodes.filter[it != movedNode].sortBy[y + height].reverse.head ?: botNode
			val oldMax = max(movedNodeOldY + movedNode.height, scndBotNode.y + scndBotNode.height)
			val selection = model.selectedModelElements
			for (dataContext : model.getNodes(DataContext)) {
				val selected = selection.contains(dataContext)
				if (!selected) {
					val oldDataContextBot = dataContext.y + dataContext.height
					if (abs(oldMin - dataContext.y) < 2) {
						// data context has been aligned to the y of the top node => resize
						val dY = topNode.y - dataContext.y
						if (dY != 0) {
							println('''Resize data context y/height from «dataContext.y»/«dataContext.height» to «topNode.y»/«(dataContext.height - dY)» by dY=«dY»''')
							dataContext.y = topNode.y
							for (innerNode : dataContext.allNodes) {
								innerNode.move(innerNode.x, max(20, innerNode.y - dY))
							}
							dataContext.height = dataContext.height - dY
						}
					}
					val minHeight = (dataContext.allNodes.map[y + height].sort.reverse.head ?: 0) + 17
					if (abs(oldMax - oldDataContextBot) < 2 || dataContext.height == minHeight) {
						// data context has been aligned to the bottom of the bottom node => resize
						val dY2 = botNode.y + botNode.height - dataContext.y - dataContext.height
						if (dY2 != 0) {
							println('''Resize data context height from «dataContext.height» to «botNode.y + botNode.height - dataContext.y» by dY=«dY2»''')
							dataContext.height = max(minHeight, dataContext.height + dY2)
						}
					}
				}
			}
		}
	}
}
