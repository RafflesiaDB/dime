package info.scce.dime.process.hooks;

import info.scce.dime.process.build.AtomicSIBBuild;
import info.scce.dime.process.process.AtomicSIB;
import info.scce.dime.siblibrary.SIB;

public class AtomicSIBHook extends AbstractPostCreateSIBHook<AtomicSIB>{

	@Override
	boolean initialize(AtomicSIB sib) {
		
		SIB lcAtomicSIB = (SIB) sib.getSib();
		
		setLabel(lcAtomicSIB.getName());
		
		return AtomicSIBBuild.initialize(sib);
	}

}
