package info.scce.dime.process.hooks;

import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.TransientCreateSIB;

public class TransientCreateSIBHook extends AbstractPostCreateSIBHook<TransientCreateSIB> {

	@Override
	boolean initialize(TransientCreateSIB sib) {

		ConcreteType createdType = sib.getCreatedType();

		setLabel("Transient Create " + createdType.getName());
		addPrimitiveInputPort("internalName", PrimitiveType.TEXT, false);

		addBranch("success");
		addComplexOutputPort("created", createdType, false);
		
		return true;
	}

}
