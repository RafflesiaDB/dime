package info.scce.dime.process.hooks;

import info.scce.dime.process.process.ContainsJavaNativeSIB;
import info.scce.dime.siblibrary.Type;

public class ContainsJavaNativeSIBHook extends AbstractPostCreateSIBHook<ContainsJavaNativeSIB>{

	@Override
	boolean initialize(ContainsJavaNativeSIB sib) {
		
   		Type listType = (Type) sib.getListType();
   		
   		setLabel(listType.getName() + " List Contains");
   		
   		addJavaNativeInputPort("list", listType, true);
   		addJavaNativeInputPort("element", listType, false);
   		
   		addBranch("yes");
   		addBranch("no");
   		return true;
	}

}
