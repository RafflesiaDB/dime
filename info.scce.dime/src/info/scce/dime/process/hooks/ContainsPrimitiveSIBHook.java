package info.scce.dime.process.hooks;

import info.scce.dime.process.process.ContainsPrimitiveSIB;
import info.scce.dime.process.process.PrimitiveType;

public class ContainsPrimitiveSIBHook extends AbstractPostCreateSIBHook<ContainsPrimitiveSIB>{

	@Override
	boolean initialize(ContainsPrimitiveSIB sib) {

   		setLabel("List Contains");
   		
   		addPrimitiveInputPort("list", PrimitiveType.TEXT, true);
   		addPrimitiveInputPort("element", PrimitiveType.TEXT, false);
   		
   		addBranch("yes");
   		addBranch("no");
   		return true;
	}

}
