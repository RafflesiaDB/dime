package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.EdgeLayoutUtils.getAbsoluteLocation;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getBendpoints;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getConnection;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getDistance;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getMovableEdges;
import static info.scce.dime.process.helper.EdgeLayoutUtils.moveBendpoint;

import java.util.List;

import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement;
import graphmodel.Edge;
import graphmodel.ModelElementContainer;
import info.scce.dime.api.DIMEPostMoveHook;
import info.scce.dime.process.helper.EdgeLayoutUtils.Location;
import info.scce.dime.process.process.EventListener;
import info.scce.dime.process.process.Input;

public class EventListenerDataFlowSourcePostMove extends DIMEPostMoveHook<EventListener> {

	@Override
	public void postMove(EventListener dfTarget, ModelElementContainer sourceContainer,
			ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		
		postProcessEdges(dfTarget, deltaX, deltaY);
	}
	
	
	void postProcessEdges(EventListener dfTarget, int deltaX, int deltaY) {
		// find out which edges have already been moved completely
		List<FreeFormConnection> movables = getMovableEdges((CModelElement)dfTarget);
		
		for (Input input : dfTarget.getInputs()) {
			Location absLoc = getAbsoluteLocation(input);
			Location oldLoc = new Location(absLoc.x + 6 - deltaX, absLoc.y + 6 - deltaY);
			for (Edge edge : input.getIncoming()) {
				if (!movables.contains(getConnection(edge))) {
					List<Point> points = getBendpoints(edge);
					for (int i = 0; i < points.size(); i++) {
						Point point = points.get(i);
						if (getDistance(oldLoc, Location.from(point)) < 60) {
							moveBendpoint(edge, i, point.getX() + deltaX, point.getY() + deltaY);
						}
					}
				}
			}
		}
	}

}
