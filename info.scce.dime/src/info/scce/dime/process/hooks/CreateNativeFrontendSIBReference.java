package info.scce.dime.process.hooks;

import info.scce.dime.process.build.NativeFrontendSIBReferenceBuild;
import info.scce.dime.process.process.NativeFrontendSIBReference;

public class CreateNativeFrontendSIBReference extends AbstractPostCreateSIBHook<NativeFrontendSIBReference>{

	@Override
	public boolean initialize(NativeFrontendSIBReference sib) {
		
		setLabel(sib.getReferencedSib().getLabel());
		return NativeFrontendSIBReferenceBuild.initialize(sib);

	}
		

}
