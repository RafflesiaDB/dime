package info.scce.dime.process.hooks;

import java.util.Optional;

import info.scce.dime.data.data.ConcreteType;
import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.UserAssociation;
import info.scce.dime.process.process.SwitchToUserSIB;

public class SwitchToUserSIBHook extends AbstractPostCreateSIBHook<SwitchToUserSIB>{

	@Override
	boolean initialize(SwitchToUserSIB sib) {
		
		setLabel("Switch To User");
		
		addComplexInputPort("user", sib.getCurrentUser(), false);
		
		addBranch("success");
		
		Optional<UserAssociation> userAssoc = 
				sib.getCurrentUser().getOutgoing(UserAssociation.class).stream().findFirst();
		if (userAssoc.isPresent() && userAssoc
				.map(assoc -> assoc.getTargetElement() instanceof ConcreteType
						&& ((ConcreteType) assoc.getTargetElement()).getIncoming(Inheritance.class).isEmpty())
				.orElse(false)) {
			
			addComplexOutputPort("switchUser", ((ConcreteType) userAssoc.get().getTargetElement()), false);
		}

		
		return true;
	}

}
