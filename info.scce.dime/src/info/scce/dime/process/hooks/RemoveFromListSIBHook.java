package info.scce.dime.process.hooks;

import info.scce.dime.data.data.Type;
import info.scce.dime.process.process.RemoveFromListSIB;

public class RemoveFromListSIBHook extends AbstractPostCreateSIBHook<RemoveFromListSIB>{

	@Override
	boolean initialize(RemoveFromListSIB sib) {

		Type listType = sib.getListType();

		setLabel("RemoveFromList");
		addComplexInputPort("list", listType, true);
		addComplexInputPort("element", listType, false);

		addBranch("removed");
		addBranch("not found");
		
		return true;
	}
	
}
