package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_X;
import static info.scce.dime.process.helper.LayoutConstants.SIB_FIRST_PORT_Y;
import static info.scce.dime.process.helper.PortUtils.calcName;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.process.helper.NodeLayout;
import info.scce.dime.process.helper.SIBLayoutUtils;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.DeleteSIB;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.EventListener;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PutToContextSIB;
import info.scce.dime.process.process.SIB;

public class InputHook extends DIMEPostCreateHook<Input>{

	@Override
	public void postCreate(Input port) {
		try {
			port.setName(calcName(port));
			
     		if (port.getContainer() instanceof EndSIB) {
     			
     			EndSIB cEndSIB = (EndSIB)port.getContainer();
     			
     			// SIB's height is resized so that all input ports fit exactly into it
     			int inputPortAmount = cEndSIB.getInputs().size();
     			cEndSIB.resize(cEndSIB.getWidth(), NodeLayout.getSIBHeight(inputPortAmount));
     		
     			int y = SIB_FIRST_PORT_Y + PORT_SPACE * (inputPortAmount - 1);
     			port.moveTo(cEndSIB, PORT_X, y);
     		}
     		else if (port.getContainer() instanceof DeleteSIB) {
     			SIBLayoutUtils.resizeAndLayout(port.getContainer());
     		}
     		else if (port.getContainer() instanceof SIB) {
     			if(port.getContainer() instanceof PutToContextSIB) {
     				//add fitting output ports as well
     				PutToContextSIB ptc = (PutToContextSIB)port.getContainer();
     				if (port instanceof PrimitiveInputPort) {
     					PrimitiveInputPort pip = (PrimitiveInputPort) port;
     					ptc.getBranchSuccessors().forEach(n -> {
     						PrimitiveOutputPort pop = n.newPrimitiveOutputPort(0, 0);
     						pop.setName(port.getName());
     						pop.setDataType(pip.getDataType());
     					});     					
     				}
     				if (port instanceof ComplexInputPort) {
     					ComplexInputPort cip = (ComplexInputPort) port;
     					ptc.getBranchSuccessors().forEach(n -> {
     						ComplexOutputPort pop = n.newComplexOutputPort(cip.getDataType(),0, 0);
     						pop.setName(port.getName());
     					});     					
     				}
     			}
     			SIBLayoutUtils.resizeAndLayout(port.getContainer());
     		}
     		else if (port.getContainer() instanceof EventListener) {
     			SIBLayoutUtils.resizeAndLayout(port.getContainer());
     		}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
