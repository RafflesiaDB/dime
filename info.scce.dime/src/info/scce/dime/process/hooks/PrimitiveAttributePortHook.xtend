package info.scce.dime.process.hooks

import info.scce.dime.data.data.Type
import info.scce.dime.process.process.PrimitiveAttributePort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.SIB
import graphmodel.internal.InternalModelElement

class PrimitiveAttributePortHook extends AbstractAttributePortHook<PrimitiveAttributePort> {
	
	/**
	 * Replaces the {@link PrimitiveAttributePort} with a {@link PrimitiveInputPort}. 
	 */
	override postCreate(PrimitiveAttributePort port) {
		val attr = port.attribute
		val type = attr.container as Type
		val it = port.container as SIB
		
		val ime = port.internalElement as InternalModelElement
		port.delete
		// FIXME re-setting the ime is a workaround for not causing an exception when deleting model elements in post-create hooks 
		port.internalElement = ime
		
		if (!isTypeSIB) {
			showError("Operation not Possible",
				'''You may drop an attribute only on data SIBs (like create, retrieve, ...) and not on a sib of type '«eClass.name»'.''')
			return
		}
		
		if (!typeHierarchy.exists[it == type]) {
			showError("Operation not Possible", 
				'''Inserting an attribute of type '«type?.name ?: "null"»' into a sib for type '«type.name»' is not allowed.''')
			return
		}
		
		if (inputs.map[name].contains(attr.name)) {
			showError("Operation not Possible", 
				'''The sib already contains an attribute with name '«attr.name»'.''')
			return
		}
		
		newPrimitiveInputPort(0, 0) => [
			name = attr.name
			dataType = attr.dataType.toProcessPrimitive
			isList = attr.isList
		]
	}
	
}