package info.scce.dime.process.hooks

import info.scce.dime.process.process.EnumSwitchSIB
import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook

class EnumSwitchPostDelete extends CincoPostDeleteHook<EnumSwitchSIB>{
	
	override getPostDeleteFunction(EnumSwitchSIB sib) {
		val elseSib = sib.branchSuccessors.filter[name=="else"].head
		return [elseSib?.delete]	}
	
	
}