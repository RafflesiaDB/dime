package info.scce.dime.process.hooks;

import info.scce.dime.api.DIMEPostResizeHook;
import info.scce.dime.process.helper.LayoutConstants;
import info.scce.dime.process.process.EventListener;
import info.scce.dime.process.process.Input;

public class EventListenerPostResize extends DIMEPostResizeHook<EventListener>{

	@Override
	public void postResize(EventListener cModelElement, int direction, int width, int height) {
		for (Input o : cModelElement.getInputs()) {
			o.resize(width - LayoutConstants.PORT_X*2, o.getHeight());
		}
	}

}
