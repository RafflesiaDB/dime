package info.scce.dime.process.hooks;

import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.ProcessEntryPointComponent;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.process.build.LinkProcessSIBBuild;
import info.scce.dime.process.process.LinkProcessSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

public class LinkProcessSIBHook extends AbstractPostCreateSIBHook<LinkProcessSIB>{

	@Override
	void preProcess(LinkProcessSIB sib) {
		super.preProcess(sib);
		Process process = null;
		if(sib.getProMod() instanceof ProcessComponent) {
			process = ((ProcessComponent)sib.getProMod()).getModel();
		}
		if(sib.getProMod() instanceof ProcessEntryPointComponent) {
			process = ((ProcessEntryPointComponent)sib.getProMod()).getEntryPoint().getProMod();
		}
		if (process.getProcessType() != ProcessType.BASIC) {
			sib.delete();
			stop("Only Basic processes can be linked.");
		}
	}
	
	@Override
	public boolean initialize(LinkProcessSIB sib) {
		
		setLabel(((URLProcess)sib.getProMod()).getUrl());
		
		return LinkProcessSIBBuild.initialize(sib);
	}

}
