package info.scce.dime.process.hooks;

import info.scce.dime.data.data.UserType;
import info.scce.dime.process.process.CreateUserSIB;
import info.scce.dime.process.process.PrimitiveType;

public class CreateUserSIBHook extends AbstractPostCreateSIBHook<CreateUserSIB>{

	@Override
	boolean initialize(CreateUserSIB sib) {
		
		UserType createdType = sib.getCreatedType();
		
		setLabel("Create " + createdType.getName());
		addPrimitiveInputPort("internalName", PrimitiveType.TEXT, false);
		
		addBranch("success");
		addComplexOutputPort("created", createdType, false);
		
		return true;
	}
}
