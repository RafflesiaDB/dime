package info.scce.dime.process.hooks;

import java.util.List;

import graphmodel.Node;
import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.process.process.AbstractBranch;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.DataContext;
import info.scce.dime.process.process.GUISIB;
import info.scce.dime.process.process.InputPort;
import info.scce.dime.process.process.JavaNativeVariable;
import info.scce.dime.process.process.PrimitiveVariable;
import info.scce.dime.process.process.SIB;

public class DeleteBranchesOnSIBDelete extends DIMEPreDeleteHook<SIB> {

	@Override
	public void preDelete(SIB sib) {
		List<AbstractBranch> branches = sib.getAbstractBranchSuccessors();
		branches.forEach(branch -> branch.delete());
		if(sib instanceof GUISIB){
			((GUISIB) sib).getEventListenerSuccessors().forEach(n->n.delete());
		}
		
	}

}
