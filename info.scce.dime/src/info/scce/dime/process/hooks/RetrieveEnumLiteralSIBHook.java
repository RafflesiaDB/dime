package info.scce.dime.process.hooks;

import info.scce.dime.data.data.EnumLiteral;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.process.process.RetrieveEnumLiteralSIB;

public class RetrieveEnumLiteralSIBHook extends AbstractPostCreateSIBHook<RetrieveEnumLiteralSIB>{

	@Override
	boolean initialize(RetrieveEnumLiteralSIB sib) {

		EnumLiteral literal = sib.getRetrievedLiteral();
		EnumType enumType = (EnumType) literal.getContainer();

		setLabel("Get " + enumType.getName());

		addBranch(literal.getName());
		addComplexOutputPort("literal", enumType, false);
		
		return true;
	}

}
