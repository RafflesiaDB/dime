package info.scce.dime.process.hooks

import graphmodel.Node
import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.process.process.ComplexVariable

import static java.lang.Math.min
import static java.lang.Math.max
import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_X
import info.scce.dime.process.process.DataContext

class ComplexVariablePostCreate extends DIMEPostCreateHook<ComplexVariable> {
	val OFFSET = 30
	override postCreate(ComplexVariable it) {
		val containerWidth = (container as Node).width
		
		// width should not be smaller than 10 
		width = max(containerWidth - 2 * VAR_ATTR_X, 10)
		
		// offset should not move it outside the container
		x = min(VAR_ATTR_X, max((containerWidth - width) / 2, 0))
		
		// name needs to be set after resize to have the label resized, too
		name = dataType.name.toLowerCase
		
		if(it.container instanceof DataContext){
			var dataContext = it.container as DataContext
			var maxHeight = maxHeight(dataContext);
			dataContext.setHeight( maxHeight + OFFSET);
			
		}
	}
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
}
