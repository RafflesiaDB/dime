package info.scce.dime.process.hooks;

import info.scce.dime.process.process.TriggerEventSIB;

public class TriggerEventSIBHook extends AbstractPostCreateSIBHook<TriggerEventSIB>{

	@Override
	boolean initialize(TriggerEventSIB sib) {
		
		setLabel("TriggerEvent");
    		addBranch("success");
    		
    		return true;
	}

}
