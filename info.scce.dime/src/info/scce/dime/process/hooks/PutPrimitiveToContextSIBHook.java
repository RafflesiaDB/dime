package info.scce.dime.process.hooks;

import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.PutPrimitiveToContextSIB;

public class PutPrimitiveToContextSIBHook extends AbstractPostCreateSIBHook<PutPrimitiveToContextSIB>{

	@Override
	boolean initialize(PutPrimitiveToContextSIB sib) {

		setLabel("Put Primitive");
		addPrimitiveInputPort("in", PrimitiveType.TEXT, false);
		
		addBranch("success");
		addPrimitiveOutputPort("out", PrimitiveType.TEXT, false);
		
		return true;
	}
	
}
