package info.scce.dime.process.hooks;

import info.scce.dime.data.data.Type;
import info.scce.dime.process.process.ContainsSIB;

public class ContainsSIBHook extends AbstractPostCreateSIBHook<ContainsSIB>{

	@Override
	boolean initialize(ContainsSIB sib) {
		
		Type listType = sib.getListType();
		
		setLabel(listType.getName() + " List Contains");
		
		addComplexInputPort("list", listType, true);
   		addComplexInputPort("element", listType, false);
   		
   		addBranch("yes");
   		addBranch("no");
   		return true;
	}

}
