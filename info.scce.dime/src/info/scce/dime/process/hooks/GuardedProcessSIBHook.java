package info.scce.dime.process.hooks;

import info.scce.dime.process.build.GuardedProcessSIBBuild;
import info.scce.dime.process.process.GuardedProcessSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

public class GuardedProcessSIBHook extends AbstractPostCreateSIBHook<GuardedProcessSIB>{

	@Override
	void preProcess(GuardedProcessSIB sib) {
		super.preProcess(sib);
		
		if (sib.getProMod().getProcessType() != ProcessType.BASIC) {
			sib.delete();
			stop("Only Basic processes can be guarded.");
		}
	}
	
	@Override
	public boolean initialize(GuardedProcessSIB sib) {
		
		Process primeModel = sib.getProMod();
		
		setLabel(primeModel.getModelName());
		
		return GuardedProcessSIBBuild.initialize(sib);
	}

}
