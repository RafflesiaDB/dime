package info.scce.dime.process.hooks;

import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.TypeAttribute;
import info.scce.dime.process.process.SetAttributeValueSIB;

public class SetAttributeValueSIBHook extends AbstractPostCreateSIBHook<SetAttributeValueSIB>{

	@Override
	boolean initialize(SetAttributeValueSIB sib) {
     	TypeAttribute attribute = sib.getAttribute();
     	setLabel("Set " + attribute.getName());
     	Type type = (Type) attribute.getContainer();
     	addComplexInputPort(type.getName().toLowerCase(), type, false);
     	if (attribute instanceof ComplexAttribute) {
     		addComplexInputPort(
     			attribute.getName().toLowerCase(),
     			((ComplexAttribute) attribute).getDataType(),
     			attribute.isIsList());
     	}
     	else if (attribute instanceof PrimitiveAttribute) {
     		addPrimitiveInputPort(
     			attribute.getName().toLowerCase(),
     			((PrimitiveAttribute) attribute).getDataType(),
     			attribute.isIsList());
     	}
     	addBranch("success");
     	
     	return true;
	}

}
