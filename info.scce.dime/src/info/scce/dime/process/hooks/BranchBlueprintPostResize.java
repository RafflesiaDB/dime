package info.scce.dime.process.hooks;

import info.scce.dime.api.DIMEPostResizeHook;
import info.scce.dime.process.helper.LayoutConstants;
import info.scce.dime.process.process.BranchBlueprint;
import info.scce.dime.process.process.Output;

public class BranchBlueprintPostResize extends DIMEPostResizeHook<BranchBlueprint>{

	@Override
	public void postResize(BranchBlueprint cModelElement, int direction, int width, int height) {
		for (Output o : cModelElement.getOutputs()) {
			o.resize(width - LayoutConstants.PORT_X*2, o.getHeight());
		}
	}

}
