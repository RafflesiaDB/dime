package info.scce.dime.process.hooks;

import info.scce.dime.process.helper.LayoutConstants;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.StartSIB;

public class StartSIBPostResize extends ProcessNodePostResize<StartSIB>{

	@Override
	public void postResize(StartSIB cStartSIB, int direction, int width, int height) {
		super.postResize(cStartSIB, direction, width, height);
		
		for (Output o : cStartSIB.getOutputs()) {
			o.resize(width - LayoutConstants.PORT_X * 2, o.getHeight());
		}
		
	}

}
