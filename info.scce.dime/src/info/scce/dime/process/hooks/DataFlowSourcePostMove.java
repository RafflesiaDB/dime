package info.scce.dime.process.hooks;

import static info.scce.dime.process.helper.EdgeLayoutUtils.getAbsoluteLocation;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getBendpoints;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getConnection;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getDistance;
import static info.scce.dime.process.helper.EdgeLayoutUtils.getMovableEdges;
import static info.scce.dime.process.helper.EdgeLayoutUtils.moveBendpoint;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.swt.widgets.Display;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CNode;
import graphmodel.Edge;
import graphmodel.ModelElementContainer;
import info.scce.dime.process.helper.EdgeLayoutUtils.Location;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.Output;

public class DataFlowSourcePostMove extends ProcessNodePostMove<DataFlowSource> {

	@Override
	public void postMove(DataFlowSource dfSource, ModelElementContainer sourceContainer,
			ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		
		super.postMove(dfSource, sourceContainer, targetContainer, x, y, deltaX, deltaY);
		
		// find out which edges have already been moved completely
		if (dfSource instanceof CNode) {
			List<FreeFormConnection> movables = new ArrayList<>();
			if (Display.getCurrent() != null) {
				// seems to be an UI-related change,
				// find out which edges have already been moved completely
				movables = getMovableEdges(dfSource);
			}
			for (Output output : dfSource.getOutputs()) {
				Location absLoc = getAbsoluteLocation(output);
				Location oldLoc = new Location(absLoc.x + 6 - deltaX, absLoc.y + 6 - deltaY);
				for (Edge edge : output.getOutgoing()) {
					FreeFormConnection connection = getConnection(edge);
					if (connection != null && !movables.contains(getConnection(edge))) {
						List<Point> points = getBendpoints(edge);
						for (int i = 0; i < points.size(); i++) {
							Point point = points.get(i);
							if (getDistance(oldLoc, Location.from(point)) < 60) {
								moveBendpoint(edge, i, point.getX() + deltaX, point.getY() + deltaY);
							}
						}
					}
				}
			}
		}
	}
}
