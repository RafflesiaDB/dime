package info.scce.dime.process.hooks;

import graphmodel.ModelElementContainer;
import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.process.helper.SIBLayoutUtils;
import info.scce.dime.process.process.IO;

public class CIOPreDelete extends DIMEPreDeleteHook<IO> {

	@Override
	public void preDelete(IO port) {
		
		ModelElementContainer container = port.getContainer();
		SIBLayoutUtils.resizeAndLayoutBeforeDelete(container, port);
		
	}

}
