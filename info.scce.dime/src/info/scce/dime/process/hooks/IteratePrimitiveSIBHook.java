package info.scce.dime.process.hooks;

import info.scce.dime.process.process.IteratePrimitiveSIB;
import info.scce.dime.process.process.PrimitiveType;

public class IteratePrimitiveSIBHook extends AbstractPostCreateSIBHook<IteratePrimitiveSIB>{

	@Override
	boolean initialize(IteratePrimitiveSIB sib) {

   		setLabel("Iterate");
   		
   		addPrimitiveInputPort("list", PrimitiveType.TEXT, true);
   		
   		addBranch("next");
   		addPrimitiveOutputPort("element", PrimitiveType.TEXT, false);
   		
   		addBranch("exit");
   		
   		return true;
	}

}
