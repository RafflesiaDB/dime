package info.scce.dime.process.hooks;

import info.scce.dime.process.helper.LayoutConstants;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.SIB;

public class SIBPostResize extends ProcessNodePostResize<SIB>{

	@Override
	public void postResize(SIB cModelElement, int direction, int width, int height) {
		super.postResize(cModelElement, direction, width, height);
		
		for (Input i : cModelElement.getInputs()) {
			i.resize(width - LayoutConstants.PORT_X * 2, i.getHeight());
		}
		
	}

}
