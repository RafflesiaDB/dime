package info.scce.dime.process.hooks

import graphmodel.ModelElement
import graphmodel.Node
import info.scce.dime.api.DIMEPostResizeHook
import info.scce.dime.process.process.DataContext
import org.eclipse.swt.widgets.Display

import static java.lang.Math.abs
import static java.lang.Math.max

class ProcessNodePostResize<E extends ModelElement> extends DIMEPostResizeHook<E> {
	
	override postResize(E node, int direction, int deltaWidth, int deltaHeight) {
		if (node instanceof Node) {
			updateDataContext(node as Node, deltaHeight)
		}
	}
	
	def updateDataContext(Node resizedNode, int deltaHeight) {
		if (Display.current === null) {
			return // only resize if triggered by changes in the UI editor
		}
		val model = resizedNode.rootElement
		val nodes = model.allNodes.drop(DataContext)
		if (!nodes.nullOrEmpty) {
			val botNode = nodes.sortBy[y + height].reverse.head
			val scndBotNode = nodes.filter[it != resizedNode].sortBy[y + height].reverse.head ?: botNode
			val oldMax = max(resizedNode.y + resizedNode.height - deltaHeight, scndBotNode.y + scndBotNode.height)
			for (dataContext : model.getNodes(DataContext)) {
				val oldDataContextBot = dataContext.y + dataContext.height
				val minHeight = (dataContext.allNodes.map[y + height].sort.reverse.head ?: 0) + 17
				if (abs(oldMax - oldDataContextBot) < 2 || dataContext.height == minHeight) {
					// data context has been aligned to the bottom of the bottom node => resize
					val dY = botNode.y + botNode.height - dataContext.y - dataContext.height
					if (dY != 0) {
						println('''Resize data context height from «dataContext.height» to «botNode.y + botNode.height - dataContext.y» by dY=«dY»''')
						dataContext.height = max(minHeight, dataContext.height + dY)
					}
				}
			}
		}
	}
}