package info.scce.dime.process.hooks;

import org.eclipse.emf.common.util.URI;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.process.actions.InitializeProcessType;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

public class ProcessInit extends DIMEPostCreateHook<Process>{

	@Override
	public void postCreate(Process processModel) {
		try {
			
			URI uri = processModel.eResource().getURI();
			String fileName = uri.lastSegment();
			String fileExtension = uri.fileExtension();
			String modelName = fileName.replace("." + fileExtension, "");
			
			
			processModel.setModelName(modelName);
			processModel.setDescription(modelName);
			
			String fullPath = uri.toPlatformString(true);
			System.out.println(fullPath);
			
			ProcessType chosenType = ProcessType.UNSPECIFIED;
			
			if (fullPath.contains("basic")) {
				chosenType = ProcessType.BASIC;
			}
			else if (fullPath.contains("interactable")) {
				chosenType = ProcessType.BASIC;
			}
			else if (fullPath.contains("security")) {
				chosenType = ProcessType.SECURITY;
			}
			else if (fullPath.contains("file_download_security")) {
				chosenType = ProcessType.FILE_DOWNLOAD_SECURITY;
			}
			else if (fullPath.contains("interaction")) {
				chosenType = ProcessType.BASIC;
			}
			else if (fullPath.contains("longrunning")) {
				chosenType = ProcessType.LONG_RUNNING;
			} 
			else if (fullPath.contains("async")) {
				chosenType = ProcessType.ASYNCHRONOUS;
			}
			else if (fullPath.contains("nfsl")) {
				chosenType = ProcessType.NATIVE_FRONTEND_SIB_LIBRARY;
			}
			else {
				chosenType = InitializeProcessType.chooseProcessTypeDialog();
			}
			
			InitializeProcessType.initialize(processModel,chosenType);
     		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
