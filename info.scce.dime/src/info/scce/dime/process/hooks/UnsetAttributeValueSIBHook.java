package info.scce.dime.process.hooks;

import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.TypeAttribute;
import info.scce.dime.process.process.UnsetAttributeValueSIB;

public class UnsetAttributeValueSIBHook extends AbstractPostCreateSIBHook<UnsetAttributeValueSIB> {

	@Override
	boolean initialize(UnsetAttributeValueSIB sib) {
		TypeAttribute attribute = sib.getAttribute();
		setLabel("Unset " + attribute.getName());
     	Type type = (Type) attribute.getContainer();
     	addComplexInputPort(type.getName().toLowerCase(), type, false);
     	addBranch("success");
     	
     	return true;
	}

}
