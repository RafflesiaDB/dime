package info.scce.dime.process.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.process.process.ProcessInputStatic
import info.scce.dime.process.process.ProcessSIB

class ProcessInputStaticHook extends DIMEPostCreateHook<ProcessInputStatic> {
	
	override postCreate(ProcessInputStatic input) {
		if (input.container instanceof ProcessInputStatic === false)
			return;
			
		val cParentInput = input.container as ProcessInputStatic
		val cSIB = cParentInput.container as ProcessSIB
		input => [
			name = cParentInput.name
			moveTo(cSIB, cParentInput.x, cParentInput.y)
		]
		
		cParentInput.delete
	}
}
