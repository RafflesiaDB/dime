package info.scce.dime.process.build


import static info.scce.dime.process.helper.LayoutConstants.BRANCH_H_SPACE
import static info.scce.dime.process.helper.LayoutConstants.BRANCH_THRESHOLD
import static info.scce.dime.process.helper.LayoutConstants.BRANCH_V_DISTANCE
import static info.scce.dime.process.helper.LayoutConstants.BRANCH_WIDTH
import static info.scce.dime.process.helper.LayoutConstants.SIB_WIDTH
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.SIB

interface BranchCreator {
	
	def Branch getNewBranch()
}

class UpdateBranchCreator implements BranchCreator {

	SIB sib
	info.scce.dime.process.process.Process model

	protected int marginY = 10
	protected int branchOffsetX = 20
	protected int branchOffsetY = -20

	new(SIB sib) {
		this.sib = sib
		model = sib.rootElement
	}

	override getNewBranch() {
		val branch = model.newBranch(
			sib.x + sib.width + branchOffsetX,
			sib.y + branchOffsetY
		)
		sib.newBranchConnector(branch)
		branchOffsetY += branch.height
		return branch
	}

}

class InitializationBranchCreator implements BranchCreator {

	SIB sib
	info.scce.dime.process.process.Process model

	private int branchX

	new(SIB sib) {
		this.sib = sib
		model = sib.rootElement
		branchX = calcBranchX
	}
	
	override getNewBranch() {
		val branches = sib.branchSuccessors
		val numBranches = branches.size 
		
		if (numBranches < BRANCH_THRESHOLD) {
			for (branch : branches) {
				branch.internalBranch.x = (branch.x - BRANCH_WIDTH / 2 - BRANCH_H_SPACE / 2);
			}
		}
		
		val branch = model.newBranch(branchX, calcBranchY)
		
		branch.resize(BRANCH_WIDTH, branch.height)

	    	val connector = sib.newBranchConnector(branch)
	    	if (numBranches >= BRANCH_THRESHOLD) {
	    		connector.addBendpoint(
					branch.x + branch.width / 2,
					sib.y + sib.height / 2);
	    	}
	    	
	    	if ((numBranches + 1) < BRANCH_THRESHOLD) {
			branchX += BRANCH_WIDTH / 2 + BRANCH_H_SPACE / 2;
		}
		else {
	    		branchX += BRANCH_WIDTH + BRANCH_H_SPACE;
	    	}
	    	
	    	return branch
	}

	def calcBranchX() {
		sib.x + SIB_WIDTH / 2 - BRANCH_WIDTH / 2;
	}
	
	def calcBranchY() {
		sib.y + sib.height + BRANCH_V_DISTANCE;
	}
}

