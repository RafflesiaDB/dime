package info.scce.dime.process.build

import graphmodel.ModelElement
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.GUIBranchPort
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.mcam.cli.GUIExecution
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.ComplexAttributeConnector
import info.scce.dime.process.process.ComplexListAttributeConnector
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import java.util.List
import org.eclipse.jface.dialogs.MessageDialog

class GuiSIBBuild extends PrimeSIBBuild<
		      GUISIB, /* SIB type */
		ModelElement, /* SIB port reference */
		     GUIBranch, /* Branch reference */
		     GUIBranchPort  /* Branch port reference */ > {
	
	protected extension GUIExtension = new GUIExtension
	protected extension DataExtension = DataExtension.instance
	
	static def initialize(GUISIB sib) {
		new GuiSIBBuild(sib).initialize
	}
	
	static def update(GUISIB sib) {
		new GuiSIBBuild(sib).update
	}
	
	new(GUISIB sib) {
		super(sib)
	}
	
	override getSIBReference(GUISIB sib) {
		sib.gui
	}
	
	override isSIBReferenceValid(GUISIB sib) {
		!sib.gui.hasErrors
	}
	
	override getSIBPortReferences(GUISIB sib) {
		val List<ModelElement> refs = newArrayList
		refs.addAll(
			sib.gui.dataContexts
				.map[variables]
				.flatten
				.filter[isIsInput]
				.filter[getIncoming(ComplexAttributeConnector).isEmpty]
				.filter[getIncoming(ComplexListAttributeConnector).isEmpty])
		
		
		return refs
	}
	
	override isSIBPortReference(Input port, ModelElement portRef) {
		port.name == switch portRef {
			Input: portRef.name
			Output: portRef.name
			Variable: portRef.name
			PrimitiveAttribute: portRef.attribute.name
		}
	}
	
	override getBranchReferences(GUISIB sib) {
		val gbm = sib.gui.GUIBranchesMerged
		return gbm.filter[b|sib.ignoredBranch.map[name].filter[it!==null].findFirst[equals(b.name)]===null]
	}
	
	override isBranchReference(Branch branch, GUIBranch branchRef) {
		branch.name == branchRef.name
	}
	
	override getBranchName(GUIBranch branchRef) {
		branchRef.name
	}
	
	override getBranchPortReferences(GUIBranch branchRef) {
		branchRef.ports.notEquallyNamedPorts
	}
	
	override isBranchPortReference(Output branchPort, GUIBranchPort branchPortRef) {
		branchPort.name == branchPortRef.name
	}
			
	def boolean hasErrors(GUI model) {
		val file = model.eResource.file.rawLocation.toFile
		val fe = new GUIExecution
		val adapter = fe.initApiAdapter(file)
		val check = fe.executeCheckPhase(adapter)
		if (check.hasErrors || check.hasWarnings)
			println(check)
		return check.hasErrors
	}
	
	def void createEventListeners(GUISIB guiSib){
	var gui = guiSib.gui;
		if (gui === null) {
			showDialog("Reference is null", "Reference is null!")
			return;
		}
		var events = gui.listenerContexts.map[events].flatten
		var x = guiSib.x + guiSib.width + 20;
		var y = guiSib.y - 50;
		for(evt:events)
		{
			//is the listener not already present?
			if(guiSib.eventListenerSuccessors.filter[name.equals(evt.name)].empty)
			{
				//create event listener
				var el = guiSib.rootElement.newEventListener(x,y);
				el.name = evt.name;
				//add ports
				for(port:evt.outputPorts){
					if(port instanceof ComplexOutputPort) {
						var p = el.newComplexInputPort(port.dataType,0,0);
						p.isList = port.isList
						p.name = port.name;	
					}
					if(port instanceof PrimitiveOutputPort) {
						var p = el.newPrimitiveInputPort(0,0);
						p.name = port.name;
						p.isList = port.isList
						p.dataType = port.dataType.toData.toProcess;
					}
				}
				x+=140;
				guiSib.newEventConnector(el);
			}
		}
	}
	
	 def private boolean showDialog(String title, String msg) {
		return MessageDialog::openConfirm(null, title, msg)
	}
}
