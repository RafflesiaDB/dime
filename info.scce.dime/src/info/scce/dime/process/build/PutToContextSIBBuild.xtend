package info.scce.dime.process.build

import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PutToContextSIB

class PutToContextSIBBuild extends PrimeSIBBuild<
		PutToContextSIB, /* SIB type */
		          Input, /* SIB port reference */
		         String, /* Branch reference */
		          Input  /* Branch port reference */ > {
	
	static def initialize(PutToContextSIB sib) {
		new PutToContextSIBBuild(sib).initialize
	}
	
	static def update(PutToContextSIB sib) {
		new PutToContextSIBBuild(sib).update
	}
	
	new(PutToContextSIB sib) {
		super(sib)
	}
	
	override getSIBReference(PutToContextSIB sib) {
		sib
	}
	
	override isSIBReferenceValid(PutToContextSIB sib) {
		!sib.inputs.isEmpty
	}
	
	override getSIBPortReferences(PutToContextSIB sib) {
		sib.inputs // just keep the existing ones
	}
	
	override isSIBPortReference(Input port, Input portRef) {
		port == portRef
	}
	
	override getBranchReferences(PutToContextSIB sib) {
		newArrayList("success") // single static branch
	}
	
	override isBranchReference(Branch branch, String staticBranchName) {
		branch.name == staticBranchName
	}
	
	override getBranchName(String staticBranchName) {
		staticBranchName
	}
	
	override getBranchPortReferences(String branchRef) {
		sib.inputs
	}
	
	override isBranchPortReference(Output branchPort, Input sibPort) {
		branchPort.name == sibPort.name
	}
	
}