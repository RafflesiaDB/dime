package info.scce.dime.process.build

import info.scce.dime.data.data.EnumType
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.EnumSwitchSIB
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.SIB
import org.eclipse.emf.ecore.EObject

class EnumSwitchSIBBuild extends PrimeSIBBuild<
		EnumSwitchSIB, /* SIB type */
		     EnumType, /* SIB port reference */
		  String, /* Branch reference */
		      EObject  /* Branch port reference */ > {
	
	String portName
	
	static def initialize(EnumSwitchSIB sib) {
		new EnumSwitchSIBBuild(sib).initialize
	}
	
	static def update(EnumSwitchSIB sib) {
		new EnumSwitchSIBBuild(sib).update
	}
	
	new(EnumSwitchSIB sib) {
		super(sib)
		portName = sib.inputs?.head?.name ?: "enum"
	}
	
	override getSIBReference(EnumSwitchSIB sib) {
		sib.switchedType
	}
	
	override isSIBReferenceValid(EnumSwitchSIB sib) {
		true
	}
	
	override getSIBPortReferences(EnumSwitchSIB sib) {
		newArrayList(sib.switchedType)
	}
	
	override isSIBPortReference(Input port, EnumType portRef) {
		true
	}
	
	override postProcessNewSIBPort(IO port, Object ref) {
		super.postProcessNewSIBPort(port, ref)
		(port as Input).name = portName
	}
	
	override postProcessUpdateSIBPort(IO port, Object ref) {
		super.postProcessUpdateSIBPort(port, ref)
		(port as Input).name = portName
	}
	
	override getBranchReferences(EnumSwitchSIB sib) {
		sib.switchedType.enumLiterals.map[name] + #["else"]
	}
	
	override isBranchReference(Branch branch, String branchRef) {
		branch.name == branchRef
	}
	
	override getBranchName(String branchRef) {
		branchRef
	}
	
	override getBranchPortReferences(String branchRef) {
		/* none */
	}
	
	override isBranchPortReference(Output branchPort, EObject portRef) {
		false // there are no branch ports
	}
	
	def addElseBranch(SIB sib){
		super.branchCreator = new InitializationBranchCreator(sib)
		super.addBranch("else")
	}
	
	def addBranches(String branchName){
		super.branchCreator = new InitializationBranchCreator(sib)
		super.addBranch(branchName)
		
	}
	
}