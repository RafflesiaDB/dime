package info.scce.dime.process.helper;

import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_X;
import static info.scce.dime.process.helper.LayoutConstants.VAR_FIRST_ATTR_Y;

import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.EnumLiteral;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.ReferencedBidirectionalAttribute;
import info.scce.dime.data.data.ReferencedComplexAttribute;
import info.scce.dime.data.data.ReferencedExtensionAttribute;
import info.scce.dime.data.data.ReferencedPrimitiveAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.DataExtension;
import info.scce.dime.process.process.ComplexAttributeConnector;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexListAttribute;
import info.scce.dime.process.process.ComplexListAttributeConnector;
import info.scce.dime.process.process.ComplexListAttributeName;
import info.scce.dime.process.process.ComplexVariable;
import info.scce.dime.process.process.PrimitiveListAttribute;
import info.scce.dime.process.process.PrimitiveListAttributeName;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.Process;

public class AttributeExpandUtils {

	/**
	 * Resizes cVar's height so that all contained {@link CAttribute}s fit,
	 * positions them within cVar and resizes their width according to cVar's width.
	 * 
	 * @param cVar
	 */
	public static void resizeAndLayout(ComplexVariable cVar) {

		int x = VAR_ATTR_X;
		int y = VAR_FIRST_ATTR_Y;

		cVar.resize(cVar.getWidth(), NodeLayout.getVariableHeight(cVar.getAttributes().size()));
		
		for (info.scce.dime.process.process.Attribute cAttribute : cVar.getAttributes()) {
			cAttribute.moveTo(cVar, x, y);
			cAttribute.resize(cVar.getWidth() - 2 * VAR_ATTR_X, cAttribute.getHeight());
			y += VAR_ATTR_SPACE;
		}

	}

	/**
	 * GUI version of {@link #resizeAndLayout(CComplexVariable)}
	 */
	public static void resizeAndLayout(info.scce.dime.gui.gui.ComplexVariable cVar) {
		int x = VAR_ATTR_X;
		int y = VAR_FIRST_ATTR_Y;

		cVar.resize(cVar.getWidth(), NodeLayout.getVariableHeight(cVar.getAttributes().size()));
		
		for (info.scce.dime.gui.gui.Attribute cAttribute : cVar.getAttributes()) {
			cAttribute.moveTo(cVar, x, y);
			cAttribute.resize(cVar.getWidth() - 2 * VAR_ATTR_X, cAttribute.getHeight());
			y += VAR_ATTR_SPACE;
		}
	}

	private static void expandList(ComplexVariable cVar) {
		ComplexVariable var = cVar;
		Type varType = var.getDataType();

		// Get the fields (enum elements) that are connected via Pseudo Attribute
		// Connector
		List<ComplexListAttributeName> connectedListAttributeNames = cVar
				.getOutgoing(ComplexListAttributeConnector.class).stream()
				.map(connector -> connector.getAttributeName()).collect(Collectors.toList());
		
		// Invert the set of connected
		Set<ComplexListAttributeName> unconnectedListAttributeNames = EnumSet.allOf(ComplexListAttributeName.class)
				.stream().filter(field -> !connectedListAttributeNames.contains(field)).collect(Collectors.toSet());

		for (ComplexListAttributeName attrName : unconnectedListAttributeNames) {
			if (!cVar.getComplexListAttributes().stream().filter((n) -> n.getAttributeName() == attrName).findAny()
					.isPresent()) {
				ComplexListAttribute complexListAttribute = cVar.newComplexListAttribute(varType, 1, 1);
				complexListAttribute.setAttributeName(attrName);
			}
		}
		if (cVar.getPrimitiveListAttributes().isEmpty()) {
			PrimitiveListAttribute size = cVar.newPrimitiveListAttribute(varType, 1, 1);
			size.setPrimitiveType(PrimitiveType.INTEGER);
			size.setAttributeName(PrimitiveListAttributeName.SIZE);
		}


		resizeAndLayout(cVar);
		sort(cVar);

	}

	/**
	 * GUI version of {@link #expandList(CComplexVariable)}
	 */
	private static void expandList(info.scce.dime.gui.gui.ComplexVariable cVar) {
		// TODO: this should work on the C-API. See #15472
		info.scce.dime.gui.gui.ComplexVariable var = cVar;
		Type varType = var.getDataType();


		// Get the fields (enum elements) that are connected via Pseudo Attribute
		// Connector
		List<info.scce.dime.gui.gui.ComplexListAttributeName> connectedListAttributeNames = cVar
				.getOutgoing(info.scce.dime.gui.gui.ComplexListAttributeConnector.class).stream()
				.map(connector -> connector.getAttributeName()).collect(Collectors.toList());
		// Invert the set of connected
		Set<info.scce.dime.gui.gui.ComplexListAttributeName> unconnectedListAttributeNames = EnumSet
				.allOf(info.scce.dime.gui.gui.ComplexListAttributeName.class).stream()
				.filter(field -> !connectedListAttributeNames.contains(field)).collect(Collectors.toSet());

		for (info.scce.dime.gui.gui.ComplexListAttributeName attrName : unconnectedListAttributeNames) {
			info.scce.dime.gui.gui.ComplexListAttribute complexListAttribute = cVar.newComplexListAttribute(varType, 1,
					1);
			complexListAttribute.setAttributeName(attrName);
		}
		info.scce.dime.gui.gui.PrimitiveListAttribute size = cVar.newPrimitiveListAttribute(varType, 1, 1);
		size.setPrimitiveType(info.scce.dime.gui.gui.PrimitiveType.INTEGER);
		size.setAttributeName(info.scce.dime.gui.gui.PrimitiveListAttributeName.SIZE);


		resizeAndLayout(cVar);
		sort(cVar);

	}

	

	private static void expandSingle(ComplexVariable cVar) {
		// TODO: this should work on the C-API. See #15472
		ComplexVariable var = (ComplexVariable) cVar;
		Type varType = var.getDataType();

		List<String> connectedAttributeNames = cVar.getComplexVariableSuccessors().stream()
				.map(successor -> successor.getName()).collect(Collectors.toList());

		List<String> containedComplexAttributeNames = var.getComplexAttributes().stream()
				.filter(a -> a.getAttribute() != null).map(a -> a.getAttribute().getName())
				.collect(Collectors.toList());

		List<String> containedPrimitiveAttributeNames = var.getPrimitiveAttributes().stream()
				.filter(a -> a.getAttribute() != null).map(a -> a.getAttribute().getName())
				.collect(Collectors.toList());

		List<Attribute> attributes = DataExtension.getInstance().getInheritedAttributes(varType);
		for (Attribute a : attributes) {
			System.out.println(a.getClass());
			if (a instanceof PrimitiveAttribute) {
				if (!containedPrimitiveAttributeNames.contains(a.getName())) {
					PrimitiveAttribute realAttr = getUnreferencedPrimitiveAttribute((PrimitiveAttribute) a);
					cVar.newPrimitiveAttribute(realAttr, 1, 1);
				}
			} else if (a instanceof ComplexAttribute) {
				if (!connectedAttributeNames.contains(a.getName())
						&& !containedComplexAttributeNames.contains(a.getName())) {
					ComplexAttribute realAttr = getUnreferencedComplexAttribute((ComplexAttribute) a);
					cVar.newComplexAttribute(realAttr, 1, 1);
				}
			}
			else if (a instanceof ExtensionAttribute) {
				if (!connectedAttributeNames.contains(a.getName())
						&& !containedComplexAttributeNames.contains(a.getName())) {
					ExtensionAttribute realAttr = getUnreferencedComplexAttribute((ExtensionAttribute) a);
					Process process = (Process) realAttr.getProcess();
					if(process.getEndSIBs().get(0).getInputs().get(0) instanceof ComplexInputPort) {
						cVar.newComplexExtensionAttribute(realAttr, 1, 1);						
					} else {
						cVar.newPrimitiveExtensionAttribute(realAttr, 1, 1);
					}
				}
			
			} else if (a instanceof EnumLiteral) {
				// enum literals are not expanded
			} else {
				throw new IllegalStateException(
						"Reached else/default case in exhaustive if/switch. Please fix the code.");
			}
		}
		resizeAndLayout(cVar);
		sort(cVar);
	}

	/**
	 * GUI version of {@link #expandSingle(CComplexVariable)}
	 */
	private static void expandSingle(info.scce.dime.gui.gui.ComplexVariable cVar) {
		// TODO: this should work on the C-API. See #15472
		info.scce.dime.gui.gui.ComplexVariable var = (info.scce.dime.gui.gui.ComplexVariable) cVar;
		Type varType = var.getDataType();

		List<String> connectedAttributeNames = cVar.getComplexVariableSuccessors().stream()
				.map(successor -> successor.getName()).collect(Collectors.toList());

		List<String> containedComplexAttributeNames = var.getComplexAttributes().stream()
				.filter(a -> a.getAttribute() != null).map(a -> (a.getAttribute()).getName())
				.collect(Collectors.toList());

		List<String> containedPrimitiveAttributeNames = var.getPrimitiveAttributes().stream()
				.filter(a -> a.getAttribute() != null).map(a -> a.getAttribute().getName())
				.collect(Collectors.toList());

		List<Attribute> attributes = DataExtension.getInstance().getInheritedAttributes(varType);
		for (Attribute a : attributes) {
			if (a instanceof PrimitiveAttribute) {
				if (!containedPrimitiveAttributeNames.contains(a.getName())) {
					PrimitiveAttribute realAttr = getUnreferencedPrimitiveAttribute((PrimitiveAttribute) a);
					cVar.newPrimitiveAttribute(realAttr, 1, 1);
				}
			} else if (a instanceof ComplexAttribute) {
				if (!connectedAttributeNames.contains(a.getName())
						&& !containedComplexAttributeNames.contains(a.getName())) {
					ComplexAttribute realAttr = getUnreferencedComplexAttribute((ComplexAttribute) a);
					cVar.newComplexAttribute(realAttr, 1, 1);
				}
			} else if (a instanceof ExtensionAttribute) {
				if (!connectedAttributeNames.contains(a.getName())
						&& !containedComplexAttributeNames.contains(a.getName())) {
					ExtensionAttribute realAttr = getUnreferencedComplexAttribute((ExtensionAttribute) a);
					Process process = (Process) realAttr.getProcess();
					if(process.getEndSIBs().get(0).getInputs().get(0) instanceof ComplexInputPort) {
						cVar.newComplexExtensionAttribute(realAttr, 1, 1);						
					} else {
						cVar.newPrimitiveExtensionAttribute(realAttr, 1, 1);
					}
				}
			
			} else if (a instanceof EnumLiteral) {
				// enum literals are not expanded
			} else {
				throw new IllegalStateException(
						"Reached else/default case in exhaustive if/switch. Please fix the code.");
			}
		}
		resizeAndLayout(cVar);
		sort(cVar);
	}

	private static PrimitiveAttribute getUnreferencedPrimitiveAttribute(PrimitiveAttribute a) {
		if (a instanceof ReferencedPrimitiveAttribute) {
			return getUnreferencedPrimitiveAttribute(((ReferencedPrimitiveAttribute) a).getReferencedAttribute());
		} else {
			return a;
		}
	}

	private static ComplexAttribute getUnreferencedComplexAttribute(ComplexAttribute a) {
		if (a instanceof ReferencedComplexAttribute) {
			return getUnreferencedComplexAttribute(((ReferencedComplexAttribute) a).getReferencedAttribute());
		} else if (a instanceof ReferencedBidirectionalAttribute) {
			return getUnreferencedComplexAttribute(((ReferencedBidirectionalAttribute) a).getReferencedAttribute());
		} else {
			return a;
		}
	}
	
	private static ExtensionAttribute getUnreferencedComplexAttribute(ExtensionAttribute a) {
		if (a instanceof ReferencedExtensionAttribute) {
			return getUnreferencedComplexAttribute(((ReferencedExtensionAttribute) a).getReferencedAttribute());
		} else {
			return a;
		}
	}

	/**
	 * Expands the complex variable.
	 * 
	 * If it is a list of complex elements:
	 * 
	 * creates pseudo attributes: integer size listType first listType last listType
	 * append (== last+1)
	 * 
	 * If it is a single complex element:
	 * 
	 * for every {@link ComplexAttribute} or {@link PrimitiveAttribute} a subnode is
	 * created.
	 * 
	 * Ignores those attributes that already have a corresponding adjacent Variable
	 * connected with a {@link ComplexAttributeConnector}.
	 * 
	 * @param cVar
	 */
	public static void expand(ComplexVariable cVar) {
		if (cVar.isIsList()) {
			expandList(cVar);
		} else {
			expandSingle(cVar);
		}
		cVar.setExpanded(true);
	}

	/**
	 * GUI version of {@link #expand(CComplexVariable)}
	 */
	public static void expand(info.scce.dime.gui.gui.ComplexVariable cVar) {
		if (cVar.isIsList()) {
			expandList(cVar);
		} else {
			expandSingle(cVar);
		}
		cVar.setExpanded(true);
	}

	/**
	 * deletes all unconnected contained attributes and resizes to default height
	 * 
	 * @param cVar
	 */
	public static void collapse(ComplexVariable cVar) {

		List<info.scce.dime.process.process.Attribute> unconnectedAttributes = cVar.getAttributes().stream()
				.filter(a -> (a.getIncoming().size() == 0) && (a.getOutgoing().size() == 0))
				.collect(Collectors.toList());
		List<info.scce.dime.process.process.Attribute> connectedAttributes = cVar.getAttributes().stream()
				.filter(a -> !unconnectedAttributes.contains(a)).collect(Collectors.toList());
		for (info.scce.dime.process.process.Attribute cAttribute : unconnectedAttributes) {
			cAttribute.delete();
		}

		// TODO: should only use
		// resizeAndLayout(cVar);
		// here, but does not work currently with the C-API. See #15473

		int x = VAR_ATTR_X;
		int y = VAR_FIRST_ATTR_Y;

		cVar.resize(cVar.getWidth(), NodeLayout.getVariableHeight(connectedAttributes.size()));
		for (info.scce.dime.process.process.Attribute cAttribute : connectedAttributes) {
			cAttribute.moveTo(cVar, x, y);
			cAttribute.resize(cVar.getWidth() - 2 * VAR_ATTR_X, cAttribute.getHeight());
			y += VAR_ATTR_SPACE;
		}

		cVar.setExpanded(false);
		sort(cVar);

	}

	/**
	 * GUI version of {@link #collapse(CComplexVariable)}
	 */
	public static void collapse(info.scce.dime.gui.gui.ComplexVariable cVar) {

		List<info.scce.dime.gui.gui.Attribute> unconnectedAttributes = cVar.getAttributes().stream()
				.filter(a -> (a.getIncoming().size() == 0) && (a.getOutgoing().size() == 0))
				.collect(Collectors.toList());
		List<info.scce.dime.gui.gui.Attribute> connectedAttributes = cVar.getAttributes().stream()
				.filter(a -> !unconnectedAttributes.contains(a)).collect(Collectors.toList());
		for (info.scce.dime.gui.gui.Attribute cAttribute : unconnectedAttributes) {
			cAttribute.delete();
		}

		// TODO: should only use
		// resizeAndLayout(cVar);
		// here, but does not work currently with the C-API. See #15473

		int x = VAR_ATTR_X;
		int y = VAR_FIRST_ATTR_Y;

		cVar.resize(cVar.getWidth(), NodeLayout.getVariableHeight(connectedAttributes.size()));
		for (info.scce.dime.gui.gui.Attribute cAttribute : connectedAttributes) {
			cAttribute.moveTo(cVar, x, y);
			cAttribute.resize(cVar.getWidth() - 2 * VAR_ATTR_X, cAttribute.getHeight());
			y += VAR_ATTR_SPACE;
		}

		cVar.setExpanded(false);
		sort(cVar);

	}
	
	//Help methods
	
	private static void sort(info.scce.dime.gui.gui.ComplexVariable cVar) {
		 EList<info.scce.dime.gui.gui.Attribute> allAttributes = cVar.getAttributes();
				LinkedList<info.scce.dime.gui.gui.Attribute> myAttr = new LinkedList<>();
				myAttr.addAll(allAttributes);
				myAttr.sort(new Comparator<info.scce.dime.gui.gui.Attribute>() {

						@Override
						public int compare(info.scce.dime.gui.gui.Attribute o1, info.scce.dime.gui.gui.Attribute o2) {
							String name1 = getName(o1);
							String name2 = getName(o2);
							return name1.compareTo(name2);

						}
					});
				//new psotions
				int offset = VAR_FIRST_ATTR_Y;
				for(info.scce.dime.gui.gui.Attribute attr : myAttr){
					attr.moveTo(attr.getContainer(), VAR_ATTR_X, offset);
					offset += VAR_ATTR_SPACE;
				}
				
		
	}
	
	private static void sort(ComplexVariable cVar) {
		 EList<info.scce.dime.process.process.Attribute> allAttributes = cVar.getAttributes();
				LinkedList<info.scce.dime.process.process.Attribute> myAttr = new LinkedList<>();
				myAttr.addAll(allAttributes);
				myAttr.sort(new Comparator<info.scce.dime.process.process.Attribute>() {

						@Override
						public int compare(info.scce.dime.process.process.Attribute o1, info.scce.dime.process.process.Attribute o2) {
							String name1 = getName(o1);
							String name2 = getName(o2);
							return name1.compareTo(name2);

						}
					});
				//new psotions
				int offset = VAR_FIRST_ATTR_Y;
				for(info.scce.dime.process.process.Attribute attr : myAttr){
					attr.moveTo(attr.getContainer(), VAR_ATTR_X, offset);
					offset += VAR_ATTR_SPACE;
				}
				
		
	}

	private static String getName(info.scce.dime.gui.gui.Attribute o1) {
		if (o1 instanceof info.scce.dime.gui.gui.PrimitiveAttribute) {
			PrimitiveAttribute primAttr = ((info.scce.dime.gui.gui.PrimitiveAttribute) o1).getAttribute();
			return primAttr.getName();
		}
		if (o1 instanceof info.scce.dime.gui.gui.PrimitiveListAttribute) {
			return ((info.scce.dime.gui.gui.PrimitiveListAttribute) o1).getAttributeName().getName();
		}
		if (o1 instanceof info.scce.dime.gui.gui.ComplexAttribute) {
			ComplexAttribute primAttr = ((info.scce.dime.gui.gui.ComplexAttribute) o1).getAttribute();
			return primAttr.getName();
		}
		if (o1 instanceof info.scce.dime.gui.gui.ComplexListAttribute) {
			return ((info.scce.dime.gui.gui.ComplexListAttribute) o1).getAttributeName().getName();
		}
		return "";
	}

	private static String getName(info.scce.dime.process.process.Attribute o1) {
		if (o1 instanceof info.scce.dime.process.process.PrimitiveAttribute) {
			PrimitiveAttribute primAttr = ((info.scce.dime.process.process.PrimitiveAttribute) o1).getAttribute();
			return primAttr.getName();
		}
		if (o1 instanceof info.scce.dime.process.process.PrimitiveListAttribute) {
			return ((info.scce.dime.process.process.PrimitiveListAttribute) o1).getAttributeName().getName();
		}
		if (o1 instanceof info.scce.dime.process.process.ComplexAttribute) {
			ComplexAttribute primAttr = ((info.scce.dime.process.process.ComplexAttribute) o1).getAttribute();
			return primAttr.getName();
		}
		if (o1 instanceof info.scce.dime.process.process.ComplexListAttribute) {
			return ((info.scce.dime.process.process.ComplexListAttribute) o1).getAttributeName().getName();
		}
		return "";
	}
}
