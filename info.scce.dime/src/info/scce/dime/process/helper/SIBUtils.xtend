package info.scce.dime.process.helper

import info.scce.dime.process.process.AtomicSIB
import info.scce.dime.process.process.ContainsSIB
import info.scce.dime.process.process.CreateSIB
import info.scce.dime.process.process.CreateUserSIB
import info.scce.dime.process.process.EnumSwitchSIB
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.GuardProcessSIB
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.IterateSIB
import info.scce.dime.process.process.NativeFrontendSIBReference
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.PutComplexToContextSIB
import info.scce.dime.process.process.RetrieveEnumLiteralSIB
import info.scce.dime.process.process.RetrieveOfTypeSIB
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.SetAttributeValueSIB
import info.scce.dime.process.process.TransientCreateSIB
import info.scce.dime.process.process.UnsetAttributeValueSIB
import info.scce.dime.process.process.EntryPointProcessSIB
import info.scce.dime.process.process.LinkProcessSIB
import info.scce.dime.dad.dad.URLProcess
import info.scce.dime.dad.dad.ProcessComponent
import info.scce.dime.dad.dad.ProcessEntryPointComponent
import info.scce.dime.process.process.GenericSIB

class SIBUtils {
	
	static def model(URLProcess it) {
		switch it {
			ProcessComponent: model
			ProcessEntryPointComponent: entryPoint.rootElement
		}
	}
	
	static def getReferencedComponent(SIB it) {
		switch it {
			AtomicSIB: sib
			EntryPointProcessSIB: proMod
			LinkProcessSIB: (proMod as URLProcess).model
			ProcessSIB: proMod
			GuardedProcessSIB: proMod
			GuardProcessSIB: securityProcess
			CreateSIB: createdType
			TransientCreateSIB: createdType
			CreateUserSIB: createdType
			SetAttributeValueSIB: attribute
			UnsetAttributeValueSIB: attribute
			ContainsSIB: listType
			IterateSIB: iteratedType
			PutComplexToContextSIB: putType
			RetrieveOfTypeSIB: retrievedType
			EnumSwitchSIB: switchedType
			RetrieveEnumLiteralSIB: retrievedLiteral
			NativeFrontendSIBReference : referencedSib
			GUISIB: gui
			GenericSIB: referencedObject
		}
	}
}