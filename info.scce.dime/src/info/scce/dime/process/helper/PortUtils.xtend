package info.scce.dime.process.helper

import graphmodel.ModelElement
import graphmodel.ModelElementContainer
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gUIPlugin.ComplexInputParameter
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.gUIPlugin.PrimitiveParameter
import info.scce.dime.gui.gui.AddComplexToSubmission
import info.scce.dime.gui.gui.AddPrimitiveToSubmission
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.ComplexGUIBranchPort
import info.scce.dime.gui.helper.GUIBranchPort
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.helper.PrimitiveGUIBranchPort
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DataFlowSource
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.EventListener
import info.scce.dime.process.process.IO
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.ProcessFactory
import info.scce.dime.process.process.ProcessInputStatic
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.search.search.ComplexOutputParameter
import info.scce.dime.search.search.PrimitiveInputParameter
import info.scce.dime.siblibrary.DataTypePort
import info.scce.dime.siblibrary.JavaType
import info.scce.dime.siblibrary.JavaTypePort
import info.scce.dime.siblibrary.Port
import info.scce.dime.siblibrary.PrimitivePort
import java.util.ArrayList
import java.util.HashMap
import org.eclipse.emf.common.util.Enumerator

import static extension java.lang.Character.isUpperCase

class PortUtils {
	
	extension GUIExtension _guix = new GUIExtension
	extension DataExtension _datax = DataExtension.instance
	
	static final HashMap<?,PrimitiveType> PRIMITIVE_TYPE = newHashMap(
		
		BooleanInputStatic   -> PrimitiveType::BOOLEAN,
		TextInputStatic      -> PrimitiveType::TEXT,
		IntegerInputStatic   -> PrimitiveType::INTEGER,
		RealInputStatic      -> PrimitiveType::REAL,
		TimestampInputStatic -> PrimitiveType::TIMESTAMP,
		
		info.scce.dime.siblibrary.PrimitiveType::BOOLEAN   -> PrimitiveType::BOOLEAN,
		info.scce.dime.siblibrary.PrimitiveType::TEXT      -> PrimitiveType::TEXT,
		info.scce.dime.siblibrary.PrimitiveType::INTEGER   -> PrimitiveType::INTEGER,
		info.scce.dime.siblibrary.PrimitiveType::REAL      -> PrimitiveType::REAL,
		info.scce.dime.siblibrary.PrimitiveType::TIMESTAMP -> PrimitiveType::TIMESTAMP,
		info.scce.dime.siblibrary.PrimitiveType::FILE 	  -> PrimitiveType::FILE,
		
		info.scce.dime.gui.gui.PrimitiveType::BOOLEAN   -> PrimitiveType::BOOLEAN,
		info.scce.dime.gui.gui.PrimitiveType::TEXT      -> PrimitiveType::TEXT,
		info.scce.dime.gui.gui.PrimitiveType::INTEGER   -> PrimitiveType::INTEGER,
		info.scce.dime.gui.gui.PrimitiveType::REAL      -> PrimitiveType::REAL,
		info.scce.dime.gui.gui.PrimitiveType::TIMESTAMP -> PrimitiveType::TIMESTAMP,
		info.scce.dime.gui.gui.PrimitiveType::FILE      -> PrimitiveType::FILE,
		
		info.scce.dime.data.data.PrimitiveType::BOOLEAN   -> PrimitiveType::BOOLEAN,
		info.scce.dime.data.data.PrimitiveType::TEXT      -> PrimitiveType::TEXT,
		info.scce.dime.data.data.PrimitiveType::INTEGER   -> PrimitiveType::INTEGER,
		info.scce.dime.data.data.PrimitiveType::REAL      -> PrimitiveType::REAL,
		info.scce.dime.data.data.PrimitiveType::TIMESTAMP -> PrimitiveType::TIMESTAMP,
		info.scce.dime.data.data.PrimitiveType::FILE      -> PrimitiveType::FILE,
		
		PrimitiveType::BOOLEAN   -> PrimitiveType::BOOLEAN,
		PrimitiveType::TEXT      -> PrimitiveType::TEXT,
		PrimitiveType::INTEGER   -> PrimitiveType::INTEGER,
		PrimitiveType::REAL      -> PrimitiveType::REAL,
		PrimitiveType::TIMESTAMP -> PrimitiveType::TIMESTAMP,
		PrimitiveType::FILE      -> PrimitiveType::FILE,
		
		info.scce.dime.search.search.PrimitiveType::BOOLEAN   -> PrimitiveType::BOOLEAN,
		info.scce.dime.search.search.PrimitiveType::TEXT      -> PrimitiveType::TEXT,
		info.scce.dime.search.search.PrimitiveType::INTEGER   -> PrimitiveType::INTEGER,
		info.scce.dime.search.search.PrimitiveType::REAL      -> PrimitiveType::REAL,
		info.scce.dime.search.search.PrimitiveType::TIMESTAMP -> PrimitiveType::TIMESTAMP,
//		info.scce.dime.search.search.PrimitiveType::FILE      -> PrimitiveType::FILE,
		
		info.scce.dime.gUIPlugin.PrimitiveType::BOOLEAN   -> PrimitiveType::BOOLEAN,
		info.scce.dime.gUIPlugin.PrimitiveType::TEXT      -> PrimitiveType::TEXT,
		info.scce.dime.gUIPlugin.PrimitiveType::INTEGER   -> PrimitiveType::INTEGER,
		info.scce.dime.gUIPlugin.PrimitiveType::REAL      -> PrimitiveType::REAL,
		info.scce.dime.gUIPlugin.PrimitiveType::TIMESTAMP -> PrimitiveType::TIMESTAMP
	)
	
	static final HashMap<PrimitiveType,Class<? extends InputStatic>> STATIC_INPUT_TYPE = newHashMap(
		
		PrimitiveType::BOOLEAN   -> BooleanInputStatic,
		PrimitiveType::TEXT      -> TextInputStatic,
		PrimitiveType::INTEGER   -> IntegerInputStatic,
		PrimitiveType::REAL      -> RealInputStatic,
		PrimitiveType::TIMESTAMP -> TimestampInputStatic
	)
	
//	static def toPrimitiveType(IO cType) {
//		cType.toPrimitiveType
//	}
	
	static def toPrimitiveType(IO type) {
		val key = PRIMITIVE_TYPE.keySet.filter(Class).findFirst[clazz | clazz.isInstance(type)]
		PRIMITIVE_TYPE.get(key ?: type.class)
	}
	
	static def toPrimitiveType(Enumerator type) {
		PRIMITIVE_TYPE.get(type)
	}
	
	static def toStaticInputType(PrimitiveType type) {
		STATIC_INPUT_TYPE.get(type)
	}
	
	static def dispatch IO addInput(DataFlowTarget dft, Object portReference) {
		switch portReference {
			
			ComplexOutputPort: dft.newComplexInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			PrimitiveOutputPort: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.dataType
			]
			
			JavaNativeOutputPort: dft.newJavaNativeInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			ComplexInputPort: dft.newComplexInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			PrimitiveInputPort: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.dataType
			]
			
			InputStatic: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = false
				dataType = portReference.toPrimitiveType
			]
			
			JavaNativeInputPort: dft.newJavaNativeInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			ComplexVariable: dft.newComplexInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			PrimitiveVariable: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.dataType.toPrimitiveType
			] 
			
			Type: dft.newComplexInputPort(portReference, 0, 0) => [
				name = portReference.name?.replace(" ", "").trim.toFirstLower ?: ""
				isList = false
			]
			
			PrimitiveType: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.getName?.replace(" ", "").trim.toFirstLower ?: ""
				isList = false
				dataType = portReference
			]
			
			PrimitivePort: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.type.toPrimitiveType
			]
			
			JavaTypePort: dft.newJavaNativeInputPort(portReference.type, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			DataTypePort: dft.newComplexInputPort(portReference.type, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			PrimitiveInputParameter: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.getName
				isList = false
				dataType = portReference.dataType.toPrimitiveType
			]
			
			ProcessPlaceholderSIB: 
				dft.newProcessInputStatic(portReference.proMod, 0, 0) => [
					name = portReference.label
			]
			
			ComplexAttribute: dft.newComplexInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isIsList
			]
			
			info.scce.dime.data.data.PrimitiveAttribute: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isIsList
				dataType = portReference.dataType.toPrimitiveType
			]

			default: throw new SwitchException
		}
	}
	
	static def dispatch IO addInput(EventListener dft, Object portReference) {
		switch portReference {
			
			ComplexOutputPort: dft.newComplexInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			PrimitiveOutputPort: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.dataType
			]

			
			JavaNativeOutputPort: dft.newJavaNativeInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			ComplexInputPort: dft.newComplexInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			PrimitiveInputPort: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.dataType
			]
			
			InputStatic: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = false
				dataType = portReference.toPrimitiveType
			]
			
			JavaNativeInputPort: dft.newJavaNativeInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			ComplexVariable: dft.newComplexInputPort(portReference.dataType, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			PrimitiveVariable: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.dataType.toPrimitiveType
			] 
			
			Type: dft.newComplexInputPort(portReference, 0, 0) => [
				name = portReference.name?.replace(" ", "").trim.toFirstLower ?: ""
				isList = false
			]
			
			PrimitiveType: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.getName?.replace(" ", "").trim.toFirstLower ?: ""
				isList = false
				dataType = portReference
			]
			
			PrimitivePort: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.name
				isList = portReference.isList
				dataType = portReference.type.toPrimitiveType
			]
			
			JavaTypePort: dft.newJavaNativeInputPort(portReference.type, 0, 0) => [
				name = portReference.name
				isList = portReference.isList
			]
			
			
			PrimitiveInputParameter: dft.newPrimitiveInputPort(0, 0) => [
				name = portReference.getName
				isList = false
				dataType = portReference.dataType.toPrimitiveType
			]
			
			ModelElement: dft.addInput(portReference)
			
			default: throw new SwitchException
		}
	}
	
//	static def clonePort(CInput port) {
//		clonePort(port.container as CContainer, port)
//	}
	
	static def dispatch clonePort(DataFlowTarget dft, Input port) {
		switch port {
			
			PrimitiveInputPort: dft.newPrimitiveInputPort(port.x, port.y) => [
				name = port.name
				isList = port.isIsList
				dataType = port.dataType
			]
			
			ComplexInputPort: dft.newComplexInputPort(
				(port as ComplexInputPort).dataType, port.x, port.y) => [
					name = port.name
					isList = port.isIsList
				]
			
			JavaNativeInputPort: dft.newJavaNativeInputPort(
				(port as JavaNativeInputPort).dataType, port.x, port.y) => [
					name = port.name
					isList = port.isList
				]
				
			BooleanInputStatic: dft.newBooleanInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			IntegerInputStatic: dft.newIntegerInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			RealInputStatic: dft.newRealInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			TextInputStatic: dft.newTextInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			TimestampInputStatic: dft.newTimestampInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			default: throw new SwitchException
		}
	}
	
	static def dispatch clonePort(EventListener dft, Input port) {
		switch port {
			
			PrimitiveInputPort: dft.newPrimitiveInputPort(port.x, port.y) => [
				name = port.name
				isList = port.isIsList
				dataType = port.dataType
			]
			
			ComplexInputPort: dft.newComplexInputPort(
				(port as ComplexInputPort).dataType, port.x, port.y) => [
					name = port.name
					isList = port.isIsList
				]
			
			JavaNativeInputPort: dft.newJavaNativeInputPort(
				(port as JavaNativeInputPort).dataType, port.x, port.y) => [
					name = port.name
					isList = port.isList
				]
				
			BooleanInputStatic: dft.newBooleanInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			IntegerInputStatic: dft.newIntegerInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			RealInputStatic: dft.newRealInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			TextInputStatic: dft.newTextInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			TimestampInputStatic: dft.newTimestampInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			default: throw new SwitchException
		}
	}
	
	static def dispatch addStaticPort(DataFlowTarget dft, PrimitiveInputPort archetype) {
		switch archetype.dataType {
			
			case BOOLEAN: dft.newBooleanInputStatic(archetype.x, archetype.y)
			
			case INTEGER: dft.newIntegerInputStatic(archetype.x, archetype.y)
			
			case REAL: dft.newRealInputStatic(archetype.x, archetype.y)
			
			case TEXT: dft.newTextInputStatic(archetype.x, archetype.y)
			
			case TIMESTAMP: dft.newTimestampInputStatic(archetype.x, archetype.y)
			
			default: throw new SwitchException
			
		} => [
			name = archetype.name
		]
	}
	
	static def dispatch addStaticPort(DataFlowTarget dft, InputStatic archetype) {
		switch archetype {
			
			BooleanInputStatic: dft.newBooleanInputStatic(archetype.x, archetype.y) => [
				value = archetype.value
			]
			
			IntegerInputStatic: dft.newIntegerInputStatic(archetype.x, archetype.y) => [
				value = archetype.value
			]
			
			RealInputStatic: dft.newRealInputStatic(archetype.x, archetype.y) => [
				value = archetype.value
			]
			
			TextInputStatic: dft.newTextInputStatic(archetype.x, archetype.y) => [
				value = archetype.value
			]
			
			TimestampInputStatic: dft.newTimestampInputStatic(archetype.x, archetype.y) => [
				value = archetype.value
			]
			
			default: throw new SwitchException
			
		} => [
			name = archetype.name
		]
	}
	
	static def dispatch addStaticPort(EventListener dft, PrimitiveInputPort archetype) {
		switch archetype.dataType {
			
			case BOOLEAN: dft.newBooleanInputStatic(archetype.x, archetype.y)
			
			case INTEGER: dft.newIntegerInputStatic(archetype.x, archetype.y)
			
			case REAL: dft.newRealInputStatic(archetype.x, archetype.y)
			
			case TEXT: dft.newTextInputStatic(archetype.x, archetype.y)
			
			case TIMESTAMP: dft.newTimestampInputStatic(archetype.x, archetype.y)
			
			default: throw new SwitchException
			
		} => [
			name = archetype.name
		]
	}
	
	static def cloneStaticPort(InputStatic port) {
		cloneStaticPort(port.container as DataFlowTarget, port)
	}
	
	static def cloneStaticPort(DataFlowTarget dft, InputStatic port) {
		switch port {
			
			BooleanInputStatic: dft.newBooleanInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			IntegerInputStatic: dft.newIntegerInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			RealInputStatic: dft.newRealInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			TextInputStatic: dft.newTextInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			TimestampInputStatic: dft.newTimestampInputStatic(port.x, port.y) => [
				name = port.name
				value = port.value
			]
			
			ProcessInputStatic: dft.newProcessInputStatic((port as ProcessInputStatic).value, port.x, port.y) => [
				name = port.name
			]
			
			default: throw new SwitchException
 		}
	}
	
	def addOutput(DataFlowSource branch, Object ref) {
		switch ref {
			
			ComplexInputPort: branch.newComplexOutputPort(ref.dataType, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			PrimitiveInputPort: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.isList
				dataType = ref.dataType
			]
			
			JavaNativeInputPort: branch.newJavaNativeOutputPort(ref.dataType, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			PrimitiveOutputPort: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.isList
				dataType = ref.dataType
			]
			
			JavaNativeOutputPort: branch.newJavaNativeOutputPort(ref.dataType, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			info.scce.dime.gui.gui.PrimitiveOutputPort: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.isList
				dataType = ref.dataType.toData.toProcess
			]
			
			InputStatic: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = false
				dataType = ref.toPrimitiveType
			]
			
			Type: branch.newComplexOutputPort(ref, 0, 0) => [
				name = ref.name?.replace(" ", "").trim.toFirstLower ?: ""
				isList = false
			]
			
			PrimitiveType: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.getName?.replace(" ", "").trim.toFirstLower ?: ""
				isList = false
				dataType = ref
			]
			
			ComplexVariable: branch.newComplexOutputPort(ref.dataType, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			PrimitiveVariable: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.isList
				dataType = ref.dataType.toPrimitiveType
			]
			
			PrimitiveAttribute: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.attribute.name
				isList = ref.attribute.isList
				dataType = ref.attribute.dataType.toPrimitiveType
			]
			
			info.scce.dime.gUIPlugin.PrimitiveInputParameter: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.isList
				dataType = ref.parameter.type.toPrimitiveType.toData.toProcess
			]
			AddPrimitiveToSubmission: branch.newPrimitiveOutputPort(0, 0) => [
				name = GUIBranchPort.getPortName(ref)
				isList = GUIBranchPort.isList(ref)
				dataType = GUIBranchPort.getPrimitiveType(ref).toProcess
			]
			
			AddComplexToSubmission: branch.newComplexOutputPort(GUIBranchPort.getComplexType(ref), 0, 0) => [
				name = GUIBranchPort.getPortName(ref)
				isList = GUIBranchPort.isList(ref)
			]
			
			PrimitiveParameter: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.isList
				dataType = ref.type.toPrimitiveType.toData.toProcess
			]
			
			ComplexParameter: branch.newComplexOutputPort(ref.toComplexType,0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			ComplexInputParameter: branch.newComplexOutputPort(ref.parameter.toComplexType, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			PrimitivePort: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.isList
				dataType = ref.type.toPrimitiveType
			]
			
			JavaTypePort: branch.newJavaNativeOutputPort(ref.type, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			ComplexOutputParameter: branch.newComplexOutputPort(ref.dataType, 0, 0) => [
				name = ref.name
				isList = true
			]
			DataTypePort: branch.newComplexOutputPort(ref.type, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			ComplexOutputPort: branch.newComplexOutputPort(ref.dataType, 0, 0) => [
				name = ref.name
				isList = ref.isIsList
			]
			
			info.scce.dime.gui.gui.ComplexOutputPort: branch.newComplexOutputPort(ref.dataType, 0, 0) => [
				name = ref.name
				isList = ref.isList
			]
			
			ComplexGUIBranchPort: branch.newComplexOutputPort(ref.type, 0, 0) => [
				name = ref.name
				isList = ref.list
			]
			
			PrimitiveGUIBranchPort: branch.newPrimitiveOutputPort(0, 0) => [
				name = ref.name
				isList = ref.list
				dataType = ref.type.toPrimitiveType
			]
			
			default: throw new SwitchException(ref)
		}
	}
	
	static def reconnect(DataFlow edge, Output source, Input target) {
		edge.reconnectSource(source)
		edge.reconnectTarget(target)
//		reconnectSource(edge, source)
//		reconnectTarget(edge, target)
	}
	
//	static def reconnectSource(DataFlow edge, Output source) {
//		switch edge {
//			
//			CComplexDirectDataFlow
//				case source instanceof CComplexOutputPort:
//					edge.reconnectSource(source as CComplexOutputPort)
//			
//			CPrimitiveDirectDataFlow
//				case source instanceof CPrimitiveOutputPort:
//					edge.reconnectSource(source as CPrimitiveOutputPort)
//			
//			CJavaNativeDirectDataFlow
//				case source instanceof CJavaNativeOutputPort:
//					edge.reconnectSource(source as CJavaNativeOutputPort)
//			
//			CComplexUpdate
//				case source instanceof CComplexOutputPort:
//					edge.reconnectSource(source as CComplexOutputPort)
//			
//			CPrimitiveUpdate
//				case source instanceof CPrimitiveOutputPort:
//					edge.reconnectSource(source as CPrimitiveOutputPort)
//			
//			CJavaNativeUpdate
//				case source instanceof CJavaNativeOutputPort:
//					edge.reconnectSource(source as CJavaNativeOutputPort)
//			
//			default: throw new SwitchException
//		}
//	}
//	
//	static def reconnectTarget(CDataFlow edge, CIO target) {
//		switch edge {
//			
//			CComplexDirectDataFlow
//				case target instanceof CComplexInputPort:
//					edge.reconnectTarget(target as CComplexInputPort)
//			
//			CPrimitiveDirectDataFlow
//				case target instanceof CPrimitiveInputPort:
//					edge.reconnectTarget(target as CPrimitiveInputPort)
//			
//			CJavaNativeDirectDataFlow
//				case target instanceof CJavaNativeInputPort:
//					edge.reconnectTarget(target as CJavaNativeInputPort)
//			
//			CComplexRead
//				case target instanceof CComplexInputPort:
//					edge.reconnectTarget(target as CComplexInputPort)
//			
//			CPrimitiveRead
//				case target instanceof CPrimitiveInputPort:
//					edge.reconnectTarget(target as CPrimitiveInputPort)
//			
//			CJavaNativeRead
//				case target instanceof CJavaNativeInputPort:
//					edge.reconnectTarget(target as CJavaNativeInputPort)
//		}
//	}
//	
	static def reconnectIncoming(Input oldTarget, Input newTarget) {
		for (edge : new ArrayList(oldTarget.incoming)) {
			(edge as DataFlow).reconnectTarget(newTarget)
		}
	}
	
	static def reconnectOutgoing(Output oldSource, Output newSource) {
		for (edge : new ArrayList(oldSource.outgoing)) {
			(edge as DataFlow).reconnectSource(newSource)
		}
	}
	
	static def toProcessInputPort(Port port) {
		switch port {
			
			PrimitivePort:
				ProcessFactory.eINSTANCE.createPrimitiveInputPort => [
					name = port.name
					isList = port.isIsList
					dataType = port.type.toPrimitiveType
				]
			
			JavaTypePort:
				ProcessFactory.eINSTANCE.createJavaNativeInputPort => [
					name = port.name
					isList = port.isIsList
					libraryComponentUID = (port.type as JavaType)?.fqn
				]
		}
	}
	
	static def toProcessOutputPort(Port port) {
		switch port {
			
			PrimitivePort:
				ProcessFactory.eINSTANCE.createPrimitiveOutputPort => [
					name = port.name
					isList = port.isIsList
					dataType = port.type.toPrimitiveType
				]
			
			JavaTypePort:
				ProcessFactory.eINSTANCE.createJavaNativeOutputPort => [
					name = port.name
					isList = port.isIsList
					libraryComponentUID = (port.type as JavaType)?.fqn
				]
		}
	}
	
	static def hasPrimitiveType(ModelElement it) {
		getPrimitiveType !== null
	}
	
	static def getPrimitiveType(IO it) {
		 switch it {
			PrimitiveInputPort: dataType
			PrimitiveOutputPort: dataType
			InputStatic: toPrimitiveType
		}
	}
	
	static def getPrimitiveType(Variable it) {
		 switch it {
			PrimitiveVariable: dataType.toPrimitiveType
		}
	}
	
	static def getPrimitiveType(ModelElement it) {
		 switch it {
			IO: primitiveType
			Variable: primitiveType
		}
	}
	
	static def getPrimitiveType(Port it) {
		switch it {
			PrimitivePort: type.toPrimitiveType
		}
	}
	
	static def calcName(Input port) {
		val typeName = switch port {
			PrimitiveInputPort: port.dataType.getName()
			ComplexInputPort: port.dataType.getName()
			JavaNativeInputPort: (port.dataType as info.scce.dime.siblibrary.Type).getName()
			default: "input"
		}
		var name = typeName.substring(Math.max(0, typeName.lastUpperCaseLetterIndex)).toLowerCase
		val sib = port.container
		if (!sib.inputWithNameExists(name)) {
			return name
		} else {
			for (num : 2 ..< Integer.MAX_VALUE) {
				if (!sib.inputWithNameExists(name + num)) {
					return (name + num)
				}
			}
		}
		
		return "input"
	}
	
	static def calcName(Output port) {
		val typeName = switch port {
			PrimitiveOutputPort: port.dataType.getName()
			ComplexOutputPort: port.dataType.getName()
			JavaNativeOutputPort: (port.dataType as info.scce.dime.siblibrary.Type).getName()
			default: "output"
		}
		var name = typeName.substring(Math.max(0, typeName.lastUpperCaseLetterIndex)).toLowerCase
		val branch = port.container
		if (!branch.outputWithNameExists(name)) {
			return name
		} else {
			for (num : 2 ..< Integer.MAX_VALUE) {
				if (!branch.outputWithNameExists(name + num)) {
					return (name + num)
				}
			}
		}
		
		return "output"
	}
	
	static def getLastUpperCaseLetterIndex(String str) {
		var index = -1
		val size = str.length
		for (i : 0 ..< size) {
			if (str.charAt(i).isUpperCase) {
				index = i
			}
		}
		return index
	}
	
	static def inputWithNameExists(ModelElementContainer sib, String name) {
		sib.nodes.filter(Input).exists[it.name == name]
	}
	
	static def outputWithNameExists(DataFlowSource branch, String name) {
		branch.outputs.exists[it.name == name]
	}
	
	static class Sync<Item,Reference> {
		public (Item,Reference) => boolean equals = [ false ]
		public (Reference) => void add = [ ]
		public (Item,Reference) => void update = [ ]
		public (Item) => void delete = [ ]
		
		def apply(Iterable<Item> items, Iterable<Reference> refs) {
			val _items = items.toNewList
			val _refs = refs.toNewList
			
			// update consistent items, add non-existent ones
			for (ref : _refs) {
				val item = _items.findFirst[equals.apply(it, ref)]
				if (item !== null) {
					_items.remove(item)
					update.apply(item, ref)
				} else add.apply(ref)
			}
			
			// remove unnecessary items
			_items.forEach(delete)
		}
		
		def <X> toNewList(Iterable<X> list) {
			val newList = newArrayList
			if (list !== null)
				newList.addAll(list)
			return newList
		}
	}
	
	static class SwitchException extends IllegalStateException {
		
		new() {
			super("Default case in exhaustive switch; implementation broken?")
		}
		
		new(Object obj) {
			super("Default case in exhaustive switch for " + obj?.class?.simpleName + "; implementation broken?")
		}
	}
	
}