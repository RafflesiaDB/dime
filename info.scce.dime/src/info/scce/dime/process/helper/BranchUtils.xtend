package info.scce.dime.process.helper

import static extension info.scce.dime.process.build.PrimeSIBBuild.getBuild

import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.SIB
import org.eclipse.emf.ecore.EObject
import java.util.Map.Entry
import java.util.List

class BranchUtils {
	
	static def getReferencedComponent(Branch branch) {
		val sib = branch.getPredecessors(SIB)?.head
		val build = sib.build
		if (build != null)
			((build.getBranchReferences(sib)
				.filter[build.isBranchReference(branch,it)]
				.head as Entry)?.value as List)?.head as EObject
		// TODO cast to EObject should not be necessary, fix the PrimeSIBBuild
	}
	
	static def getBranchReference(SIB sib, Branch branch) {
		val build = sib.build
		if (build != null)
			build.getBranchReferences(sib)
				.filter[build.isBranchReference(branch,it)].head
	}
}