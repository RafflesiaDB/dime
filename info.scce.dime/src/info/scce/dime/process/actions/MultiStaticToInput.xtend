package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.InputStatic

/** 
 * Context menu action, which converts all static ports to
 * input ports
 * @author zweihoff
 */
class MultiStaticToInput<T extends DataFlowTarget> extends DIMECustomAction<DataFlowTarget> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Convert to input ports"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(DataFlowTarget guiSib) {
		//at least one primitive input port without incoming edge
		!guiSib.inputStatics.empty
	}

	override void execute(DataFlowTarget guiSib) {
		
		// replacing directly will result in concurrent modification problem (without exception though)
		val allToBeReplaced = guiSib.inputs.filter(InputStatic).map[id].toList
		
		val staticToPrimitiveConverter = new StaticToPrimitivePort
		for (toBeReplaced : allToBeReplaced) {
			guiSib.inputs.filter(InputStatic).filter[id == toBeReplaced].forEach[n|				
				staticToPrimitiveConverter.execute(n)
		]
		
		}
	}
}