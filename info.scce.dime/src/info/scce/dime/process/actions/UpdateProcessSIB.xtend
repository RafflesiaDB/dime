package info.scce.dime.process.actions

import info.scce.dime.process.build.ProcessSIBBuild
import info.scce.dime.process.process.ProcessSIB

class UpdateProcessSIB<T extends ProcessSIB> extends UpdateSIBAction<ProcessSIB> {
	
	override update(ProcessSIB sib) {
		ProcessSIBBuild.update(sib)
	}
	
}