package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ControlFlow
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.SIB
import java.util.HashMap
import java.util.Map
import org.eclipse.jface.dialogs.MessageDialog

//TODO beautify after auto conversion to Xtend class
class PublishBasic extends DIMECustomAction<GuardedProcessSIB> {
	
	override getName() {
		"Publish Process"
	}

	override boolean canExecute(GuardedProcessSIB processSib) throws ClassCastException {
		if (processSib.getRootElement().getProcessType() !== ProcessType::BASIC) {
			return false
		}
		if (!(processSib.getContainer() instanceof GuardContainer)) {
			return false
		}
		val Process eObj = processSib.getProMod()
		if(eObj === null) return false
		if(eObj.getProcessType() !== ProcessType::BASIC) return false
		return true
	}

	override void execute(GuardedProcessSIB processSib) {
		val Process eObj = processSib.getProMod()
		if (eObj === null) {
			showDialog("Reference is null", "Secure failed. Reference is null!")
			return;
		}
		val Process root = processSib.getRootElement()
		// Find GuardContainer
		val GuardContainer cgc = (processSib.getContainer() as GuardContainer)
		// Remove denied branch
		// Create ProcessSIB
		val ProcessSIB newCProcess = root.newProcessSIB(processSib.getProMod(), cgc.getX() + 5, cgc.getY() + 5)
		// Move all Inputs
		processSib.getInputs().forEach([n|
			// FIXME C-API currently has no `moveTo()` method for containers...
			n.moveTo(newCProcess, n.getX(), n.getY())
		])
		// Reconnect Incomming Controllflow
		cgc.getIncoming(typeof(ControlFlow)).forEach([n|n.reconnectTarget(newCProcess)])
		// Reconnect Outgoing BranchConnectors
		cgc.getOutgoing(typeof(BranchConnector)).forEach([n|n.reconnectSource(newCProcess)])
		cgc.delete()
		removeDublicatedBranches(newCProcess)
		newCProcess.getAbstractBranchSuccessors().stream().filter([n|n.getName().equals("denied")]).forEach([n |
			n.delete()
		])
	}

	def private boolean showDialog(String title, String msg) {
		return MessageDialog::openConfirm(null, title, msg)
	}

	def static void removeDublicatedBranches(SIB sib) {
		// Remove Duplicated Branches
		val Map<String, AbstractBranch> knownBranchNames = new HashMap<String, AbstractBranch>()
		// Find already used branches
		sib.getAbstractBranchSuccessors().forEach([n |
			{
				if (!n.getOutgoing(typeof(ControlFlow)).isEmpty()) {
					knownBranchNames.put(n.getName(), n)
				} else if (n.getOutputs().stream().filter([e|!(e.getOutgoing().isEmpty())]).findAny().isPresent()) {
					knownBranchNames.put(n.getName(), n)
				}
			}
		])
		sib.getAbstractBranchSuccessors().forEach([n |
			{
				if (knownBranchNames.containsKey(n.getName())) {
					if (!knownBranchNames.get(n.getName()).equals(n)) {
						n.delete()
					}
				} else {
					knownBranchNames.put(n.getName(), n)
				}
			}
		])
	}
}
