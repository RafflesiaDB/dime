package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.EventListener
import info.scce.dime.process.process.PrimitiveInputPort

/** 
 * Context menu action, which converts all primitive
 * input ports, which are not connected by data flow, to a static input
 * @author zweihoff
 */
class EventMultiInputToStatic extends DIMECustomAction<EventListener> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Convert to static ports"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(EventListener guiSib) {
		//at least one primitive input port without incoming edge
		!guiSib.inputs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].empty
	}

	override void execute(EventListener guiSib) {
		guiSib.inputs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].forEach[n|
			val p = new PrimitivePortToStatic
			p.execute(n)
		]
	}
}
