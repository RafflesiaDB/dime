package info.scce.dime.process.actions

import info.scce.dime.process.build.GuiSIBBuild
import info.scce.dime.process.process.GUISIB

class UpdateGuiSIB extends UpdateSIBAction<GUISIB> {
	
	override update(GUISIB sib) {
		GuiSIBBuild.update(sib)
	}
}