package info.scce.dime.process.actions

import graphmodel.Edge
import graphmodel.Node
import graphmodel.internal.InternalEdge
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.helper.PortUtils
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.BlueprintSIB
import info.scce.dime.process.process.BranchBlueprint
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.SIB

import static extension info.scce.dime.process.helper.PortUtils.*

/** 
 * Action that converts a SIB to a Blueprint SIB.
 */
abstract class ConvertToBlueprintSIB<T extends SIB> extends DIMECustomAction<T> {

	extension PortUtils = new PortUtils

	override getName() {
		"Convert to Blueprint"
	}
	
	override void execute(T sib) {
		val newSib = sib.convert
		sib.branches.forEach[convert(it, newSib)]
		
		val branches = newSib.branchBlueprintSuccessors
		if (branches.size == 1) {
			newSib.defaultBranch = branches.head
		}
		
		sib.delete
	}
	
	def convert(T sib) {
		
		val defaultContentSIBs
			= sib.rootElement.find(SIB)
				.filter[hasDefaultContent(sib)]
				.toSet
				
		val newSib = createBlueprintSIB(sib)
		newSib.overtakeIncomingEdgesOf(sib)
		sib.inputs.forEach[convert(it, newSib)]
		
		defaultContentSIBs.forEach[defaultContent = newSib]
		
		return newSib
	}
	
	def BlueprintSIB createBlueprintSIB(T sib)
	
	def convert(Input port, BlueprintSIB newSib) {
		val newPort = createInput(port, newSib)
		newPort.overtakeIncomingEdgesOf(port)
		return newPort
	}
	
	def createInput(Input port, BlueprintSIB newSib) {
		switch port {
			InputStatic: newSib.addStaticPort(port)
			default: newSib.addInput(port)
		}
	}
	
	def convert(AbstractBranch branch, BlueprintSIB newSib) {
		val majorBranchSIBs
			= branch.rootElement.find(SIB)
				.filter[hasMajorBranch(branch)]
				.toSet
		
		val newBranch = createBranch(branch, newSib)
		val newBranchConnector = newSib.newBranchConnector(newBranch)
		newBranchConnector.overtakeBendpointsOf(branch.incomingBranchConnectors.head)
		branch.outputPorts.forEach[convert(it, newBranch)]
		newBranch.overtakeOutgoingEdgesOf(branch)
		
		majorBranchSIBs.forEach[majorBranch = newBranch]
		
		return newBranch
	}
	
	def createBranch(AbstractBranch branch, BlueprintSIB newSib) {
		newSib.rootElement.newBranchBlueprint(branch.x, branch.y, branch.width, branch.height) => [
			name = branch.name
		]
	}
	
	def convert(Output port, BranchBlueprint newBranch) {
		val newPort = createOutput(port, newBranch)
		newPort.overtakeOutgoingEdgesOf(port)
		return newPort
	}
	
	def createOutput(Output port, BranchBlueprint newBranch) {
		newBranch.addOutput(port)
	}
	
	def overtakeIncomingEdgesOf(Node newNode, Node node) {
		node.incoming.forEach[reconnectTarget(newNode)]
	}
	
	def overtakeOutgoingEdgesOf(Node newNode, Node node) {
		node.outgoing.forEach[reconnectSource(newNode)]
	}
	
	def overtakeBendpointsOf(Edge newEdge, Edge edge) {
		for (bp : (edge.internalElement as InternalEdge).bendpoints) {
			newEdge.addBendpoint(bp.x, bp.y)
		}
	}
}
