package info.scce.dime.process.actions;

import org.eclipse.jface.dialogs.MessageDialog;

import info.scce.dime.api.DIMECustomAction;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.NativeFrontendSIBLibrary;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

//TODO convert to Xtend to use extension providers more elegantly
public class InitializeProcessType extends DIMECustomAction<Process> {

	@Override
	public String getName() {
		return "Initialize Process Type";
	}

	@Override
	public boolean canExecute(Process processModel) {
		return ProcessType.UNSPECIFIED.equals(processModel.getProcessType());
	}

	public static ProcessType chooseProcessTypeDialog() {

		ProcessType chosenType = null;

		while (chosenType == null) {

			MessageDialog dialog = new MessageDialog(null,
					"Choose Process Type", null,
					"Which type of process do you want to initialize?",
					MessageDialog.QUESTION,
					new String[] { "Basic",
							"Security", "Long-Running", "File-Download Security", "Native Frontend SIBLibrary" }, -1);
			int result = dialog.open();

			switch (result) {
			case 0:
				chosenType = ProcessType.BASIC;
				break;
			case 1:
				chosenType = ProcessType.SECURITY;
				break;
			case 2:
				chosenType = ProcessType.LONG_RUNNING;
				break;
			case 3:
				chosenType = ProcessType.FILE_DOWNLOAD_SECURITY;
				break;
			case 4:
				chosenType = ProcessType.NATIVE_FRONTEND_SIB_LIBRARY;
				break;
			default:
				chosenType = ProcessType.UNSPECIFIED;
			}
		}
		return chosenType;
	}

	@Override
	public void execute(Process processModel) {
		ProcessType chosenType = chooseProcessTypeDialog();
		initialize(processModel, chosenType);
	}

	public static void initialize(Process process, ProcessType t) {
		process.setProcessType(t);
		if (t != ProcessType.NATIVE_FRONTEND_SIB_LIBRARY)
			process.newDataContext(20, 20, 210, 545);
		switch (t) {
		case BASIC:
		case ASYNCHRONOUS:
			process.newStartSIB(330, 20);
			process.newEndSIB(330, 500).setBranchName("success");
			break;
		case LONG_RUNNING:
			process.newStartSIB(330, 20);
			process.newEndSIB(330, 500).setBranchName("finished");
			process.newEndSIB(500, 500).setBranchName("suspended");
			break;
		case SECURITY:
			process.newStartSIB(330, 20);
			process.newEndSIB(330, 500).setBranchName("granted");
			process.newEndSIB(500, 500).setBranchName("denied");
			process.newEndSIB(650, 500, 150, 65).setBranchName("permanently denied");
			break;
		case FILE_DOWNLOAD_SECURITY: {
			process.newStartSIB(330, 20);
			EndSIB endSIB = process.newEndSIB(330, 500);
			endSIB.setBranchName("result");
			PrimitiveInputPort port = endSIB.newPrimitiveInputPort(1, 1);
			port.setHeight(18);
			port.setDataType(PrimitiveType.FILE);
			port.setName("file");
			process.newEndSIB(500, 500).setBranchName("not found");
		}
		case UNSPECIFIED:
			break;
		case NATIVE_FRONTEND_SIB_LIBRARY:
			NativeFrontendSIBLibrary c= process.newNativeFrontendSIBLibrary(0, 0);
			c.setHeight(800);
			c.setWidth(1280);
			c.setClassName("dart-classes/"+process.getModelName()+".dart");
			break;
		default:
			break;

		}

	}

}
