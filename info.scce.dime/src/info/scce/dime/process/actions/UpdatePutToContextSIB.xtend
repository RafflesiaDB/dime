package info.scce.dime.process.actions

import info.scce.dime.process.build.PutToContextSIBBuild
import info.scce.dime.process.process.PutToContextSIB

class UpdatePutToContextSIB extends UpdateSIBAction<PutToContextSIB> {
	
	override update(PutToContextSIB sib) {
		PutToContextSIBBuild.update(sib)
	}
	
}