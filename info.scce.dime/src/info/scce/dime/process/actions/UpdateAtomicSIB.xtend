package info.scce.dime.process.actions

import info.scce.dime.process.build.AtomicSIBBuild
import info.scce.dime.process.process.AtomicSIB

class UpdateAtomicSIB extends UpdateSIBAction<AtomicSIB> {
	
	override update(AtomicSIB sib) {
		AtomicSIBBuild.update(sib)
	}
	
}