package info.scce.dime.process.actions

import info.scce.dime.process.build.EnumSwitchSIBBuild
import info.scce.dime.process.process.EnumSwitchSIB

class UpdateEnumSwitchSIB extends UpdateSIBAction<EnumSwitchSIB> {
	
	override update(EnumSwitchSIB sib) {
		EnumSwitchSIBBuild.update(sib)
	}
	
}