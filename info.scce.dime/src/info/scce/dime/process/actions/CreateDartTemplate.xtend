package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.NativeFrontendSIBLibrary
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TextInputStatic
import java.io.File
import java.util.HashSet
import java.util.Set
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.Path
import org.eclipse.jface.dialogs.MessageDialog

import static extension info.scce.dime.data.helper.JavaIdentifierUtils.*

class CreateDartTemplate extends DIMECustomAction<NativeFrontendSIBLibrary> {
	NativeFrontendSIBLibrary sib
	Set<String> imports = new HashSet<String>

	override String getName() {
		return "Create Dart template from Model"
	}

	override boolean canExecute(NativeFrontendSIBLibrary s) {
		return s.getModelElements().size() > 0
	}

	def private void init(NativeFrontendSIBLibrary nfsl) {
		this.sib = nfsl
	}

	override void execute(NativeFrontendSIBLibrary nfsl) {
		init(nfsl)
		val IFile sibFile = sib.file
		var boolean overwriteAndContinue = true
		if (sibFile.project.exists(new Path(sib.className))) {
			overwriteAndContinue = showDialog("Target already exitst",
				"The file for the target source code already exists. Do you want overwrite it?")
		}
		if(!overwriteAndContinue) return;
		val className = new File(sib.className).name
		val template = '''
			class «className.replace(".dart","").escapeDart» {
			«FOR it : sib.nativeFrontendSIBs»
				«switch(sibType){
                               case BOOL: "bool"
                               case VOID: "void"
                               case COMPLEX:{
                                       abstractBranchSuccessors.findFirst[name.compareToIgnoreCase("success")==0].outputs.head.getVariableTypeFromOutput(imports)
                                       
                                       }
                               }» «methodName.escapeDart»(«FOR input: it.inputs SEPARATOR ',' AFTER ''»«input.getParameterType(imports)» «input.name»«ENDFOR»){
				                // implement here
				}
			«ENDFOR»
			
			        }
		'''
		val importText = '''
		import 'package:angular2/core.dart';
		«FOR imp : imports SEPARATOR '\n'»import 'package:app/models/«imp.escapeDart».dart';«ENDFOR»'''
		sibFile.project.createFile(sib.className, importText + "\n" + template, true)

	}

	public static def String getVariableTypeFromOutput(Output output, Set<String> imports) {
		var String type = "var"
		if (output instanceof PrimitiveOutputPort) {
			switch (output.getDataType()) {
				case BOOLEAN:
					type = "bool"
				case INTEGER:
					type = "int"
				case REAL:
					type = "double"
				case TEXT:
					type = "String"
				case TIMESTAMP,
				case FILE,
				default: {
					throw new IllegalArgumentException("currently not supported type");
				}
			}
		} else if (output instanceof ComplexOutputPort) {
			val dataType = output.dataType
			imports.add(dataType.rootElement.modelName)
			type = dataType.name.escapeDart
		}
		if (output instanceof OutputPort) {
			if (output.isIsList()) {
				type = '''List<«type»>'''
			}
		}
		return type
	}

	public static def String getParameterType(Input input, Set<String> imports) {
		var String type = "ERROR"
		if (input instanceof PrimitiveInputPort) {
			switch (input.getDataType()) {
				case BOOLEAN:
					type = "bool"
				case INTEGER:
					type = "int"
				case REAL:
					type = "double"
				case TEXT:
					type = "String"
				case TIMESTAMP,
				case FILE,
				default: {
					throw new IllegalArgumentException("currently not supported type");
				}
			}
		} else if (input instanceof BooleanInputStatic) {
			type = "bool"
		} else if (input instanceof IntegerInputStatic) {
			type = "int"
		} else if (input instanceof RealInputStatic) {
			type = "double"
		} else if (input instanceof TextInputStatic) {
			type = "String"
		} else if (input instanceof ComplexInputPort) {
			val dataType = input.dataType
			imports.add(dataType.rootElement.modelName)
			type = dataType.name.escapeDart
		}
		if (input instanceof InputPort) {
			if (input.isIsList()) {
				type = '''List<«type»>'''
			}
		}
		return type
	}

	def private boolean showDialog(String title, String msg) {
		return MessageDialog.openConfirm(null, title, msg)
	}
}
