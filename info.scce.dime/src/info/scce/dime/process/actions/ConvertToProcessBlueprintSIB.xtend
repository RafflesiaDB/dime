package info.scce.dime.process.actions

import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType

/** 
 * Action that converts a Process SIB to a Process Blueprint SIB.
 */
class ConvertToProcessBlueprintSIB extends ConvertToBlueprintSIB<ProcessSIB> {
	
	override createBlueprintSIB(ProcessSIB sib) {
		sib.rootElement.newProcessBlueprintSIB(sib.x, sib.y, sib.width, sib.height) => [
			processType = sib.proMod?.processType ?: ProcessType.UNSPECIFIED
			label = sib.label
			name = sib.name
		]
	}
	
}
