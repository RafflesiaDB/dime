package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.StartSIB

class StartSIBNewPrimitiveInput extends DIMECustomAction<StartSIB> {
	val OFFSET = 30
	override getName() {
		"New Primitive Input"
	}

	//TODO beautify after auto conversion to Xtend class

	override execute(StartSIB sib) {
		
		try {
			sib.newPrimitiveOutputPort(1, 1)
			layout(sib)
		} catch (Exception e) {
			e.printStackTrace()
		}

	}
	
	def layout(StartSIB sib) {
		var newY = sib.y + sib.height + OFFSET
		for (branch : sib.successors) {
			branch.moveTo(sib.rootElement, branch.x, newY)
		}
	}
}
