package info.scce.dime.process.actions

import graphmodel.Container
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic

import static extension info.scce.dime.process.helper.PortUtils.addInput
import static extension info.scce.dime.process.helper.PortUtils.reconnectIncoming

class StaticToPrimitivePort<T extends InputStatic> extends DIMECustomAction<InputStatic> {
	
	override getName() {
		"Convert Static Value to Input"
	}

	override execute(InputStatic inputStatic) {
		try {
			val container = inputStatic.container as Container
			// re-add all ports to preserve their order
			container.getNodes(Input).forEach[oldPort |
				if (oldPort === inputStatic) {
					container.addInput(inputStatic)
				} else {
					oldPort.reconnectIncoming(
						oldPort.clone(oldPort.container)
					)
				}
				oldPort.delete
			]
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
}
