package info.scce.dime.process.actions

import info.scce.dime.process.process.ProcessPlaceholderSIB

/** 
 * Context menu action, which converts a process sib to 
 * a process placeholder sib.
 * 
 * @author neubauer
 */
class ConvertToProcessSIB extends ConvertSIBCustomAction<ProcessPlaceholderSIB> {

	/** 
	 * Returns the name of the context menu entry.
	 */
	override getName() {
		"Convert to process"
	}
	
	/** 
	 * Guarantees that the Process SIB is not already a placeholder.
	 */
	override canExecute(ProcessPlaceholderSIB sib) {
//		!(sib.container instanceof ExtensionContext)
		true
	}

	override void execute(ProcessPlaceholderSIB sib) {
		val cProcess = sib.rootElement
		sib.convertSIB[ cProcess.newProcessSIB(sib.proMod, sib.x, sib.y) ]
	}
}
