package info.scce.dime.process.actions

import info.scce.dime.process.build.GuardProcessSIBBuild
import info.scce.dime.process.process.GuardProcessSIB

class UpdateGuardProcessSIB extends UpdateSIBAction<GuardProcessSIB> {
	
	override update(GuardProcessSIB sib) {
		GuardProcessSIBBuild.update(sib)
	}
	
}