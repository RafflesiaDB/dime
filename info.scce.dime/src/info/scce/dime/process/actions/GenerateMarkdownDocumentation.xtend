package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import java.io.File
import java.io.IOException
import org.apache.commons.io.FileUtils

class GenerateMarkdownDocumentation<T extends Process> extends DIMECustomAction<T> {
	
	override getName() {
		"Create Documentation"
	}

	override canExecute(Process d) {
		true
	}
	
	override hasDoneChanges() {
		false
	}

	override void execute(Process d) {
		val filePath = d.file.project.location.append('''../documentation/process/«d.modelName».md'''.toString).toString
		createFile(createDoc(d).toString,filePath)
		
	}
	
	/**
	 * Helper method to create a file with the given content on the given path.
	 * @param content
	 * @param path
	 * @throws IOException
	 */
	def void createFile(String content,String path) throws IOException
	{
		val File f = new File(path);
		f.getParentFile().mkdirs(); 
		f.createNewFile();
		
		FileUtils.writeStringToFile(f,content);
	}
	
	def createDoc(Process d){
		val start = d.startSIB
		val endsibs = d.endSIBs
		val containedGuiSibs = d.GUISIBs
		val containedBasicSibs = d.processSIBs.filter[proMod.processType==ProcessType.BASIC]
		val containedSecuritySibs = d.processSIBs.filter[proMod.processType==ProcessType.SECURITY]
		val containedFileDownloadSibs = d.processSIBs.filter[proMod.processType==ProcessType.FILE_DOWNLOAD_SECURITY]
	'''
	# «d.modelName» *«d.processType.literal»*
	 
	«d.documentation.doc»
	 
	### Notes
	 
	«d.documentations.map['''* «it.content»'''].join("\n")»
	 
	### Input Ports
	 
	«start.outputPorts.map['''* *«name»* : «IF isIsList»List<«ENDIF»«typeName»«IF isIsList»>«ENDIF»'''].join("\n")»
	 
	### End SIBs
	 
	«endsibs.map[branch].join("\n")»
	 
	### Used GUI SIBs
	 
	«containedGuiSibs.map['''* [«it.gui.title»](../gui/«it.gui.title».md)'''].join("\n")»
	 
	### Used Guarded Processes
	 
	«d.guardContainers.filter[!guardedProcessSIBs.empty].map['''* [«it.guardedProcessSIBs.get(0).proMod.modelName»](./«it.guardedProcessSIBs.get(0).proMod.modelName».md)«IF !it.guardProcessSIBs.empty» guarded by «it.guardProcessSIBs.map['''[«it.securityProcess.modelName»](./«it.securityProcess.modelName».md)'''].join(", ")»«ENDIF»'''].join("\n")»
	 
	### Used Basic Processes
	 
	«containedBasicSibs.map['''* [«it.proMod.modelName»](./«it.proMod.modelName».md)'''].join("\n")»
	 
	### Used Security Processes
	 
	«containedSecuritySibs.map['''* [«it.proMod.modelName»](./«it.proMod.modelName».md)'''].join("\n")»
	 
	### Used File Download Security Processes
	 
	«containedFileDownloadSibs.map['''* [«name»](./«name».md)'''].join("\n")»
	'''
	}
	
	
	
	def branch(EndSIB e)
	'''
	* *«e.branchName»*
	«FOR p:e.inputPorts»
	«" "»* «p.name»: «IF p.isIsList»List<«ENDIF»«IF p instanceof ComplexInputPort»«p.dataType.name»«ELSE»«(p as PrimitiveInputPort).dataType.literal»«ENDIF»«IF p.isIsList»>«ENDIF»
	«ENDFOR»
	«FOR p:e.inputStatics»
	«" "»* «p.name»: ```«p.staticValue»```
	«ENDFOR»
	'''
	
	
	
	
	
	
	def dispatch typeName(ComplexOutputPort t) {
		'''[«t.dataType.name»](../data/«t.dataType.rootElement.modelName»/«t.dataType.name».md)'''
	}
	
	def dispatch typeName(PrimitiveOutputPort t) {
		t.dataType.literal
	}
	
	
	
	def doc(String s){
		if(!s.nullOrEmpty) {
			return '''
			>>>
			«s»
			<<<
			'''
		}
		""
	}
	
	
}
