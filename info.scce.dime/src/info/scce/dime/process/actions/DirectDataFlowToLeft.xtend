package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.DirectDataFlow

import static extension info.scce.dime.process.helper.EdgeLayoutUtils.getBendpoints
import static extension info.scce.dime.process.helper.EdgeLayoutUtils.moveBendpoint

class DirectDataFlowToLeft<T extends DirectDataFlow> extends DIMECustomAction<DirectDataFlow> {
	
	override getName() {
		"Move left"
	}
	
	override canExecute(DirectDataFlow edge) {
		!edge.bendpoints.isEmpty
	}

	override execute(DirectDataFlow edge) {
		val bps = edge.bendpoints
		for (i: 0 ..< bps.size) {
			val bp = bps.get(i)
			edge.moveBendpoint(i, bp.x - 10, bp.y)
		}
	}
}
