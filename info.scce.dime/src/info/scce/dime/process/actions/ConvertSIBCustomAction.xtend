package info.scce.dime.process.actions

import com.google.common.collect.Lists
import graphmodel.IdentifiableElement
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.build.ProcessSIBBuild
import info.scce.dime.process.hooks.DeleteBranchesOnSIBDelete
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.SIB
import java.util.ArrayList
import static extension info.scce.dime.process.helper.PortUtils.*
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ControlFlow

/**
 * Offers basic conversion between sibs. Call the <code>convertSIB()</code> in your implementation of the <code>execute()</code> method. You will get back 
 * an instance of your target sib type from the c-api and can add some specific synchronization/copying code.  
 */
abstract class ConvertSIBCustomAction<T extends IdentifiableElement> extends DIMECustomAction<T> {
	
	/**
	 * Converts a sib to another sib type. It reuses the inputs (including static inputs with their values) 
	 * branches, their outputs, and the connected data flow edges to and from inputs and outputs.
	 * 
	 * @param sib the sib to convert.
	 * @param createNewSIB a closure for instantiating the new sib.
	 */
	def <U extends SIB> convertSIB(SIB sib, () => U createNewSIB) {
		val cSIB = sib
		val cNewSIB = {
			val newProcessSIB = createNewSIB.apply
			// FIXME this rewrapping makes postcreate hooks in the new process sib take place!!!
			newProcessSIB as U
		} 
		
		// delete the via post create hook created branches of the new sib...
		new DeleteBranchesOnSIBDelete().preDelete(cNewSIB)
		
		// reconnect the branches of the old sib to the new sib instead
		Lists.newArrayList(cSIB.outgoing).filter(BranchConnector).forEach [ reconnectSource(cNewSIB) ]
		
		// reconnect incoming edges of the old sib to the new sib
		Lists.newArrayList(cSIB.incoming).filter(ControlFlow).forEach [ reconnectTarget(cNewSIB) ]
		
		// delete the via post create hook created inputs of the new sib...
		Lists.newArrayList(cNewSIB.inputs).forEach [ delete ]
		
		// move the inputs of the old sib to the new sib instead
		new ArrayList(cSIB.inputs).forEach [
			// FIXME C-API removed `moveTo`-method for container...
			moveTo(cNewSIB, x, y)
		]
		
		// copy primitive values
		cNewSIB.label = cSIB.label
		cNewSIB.name = cSIB.name
		
		// do the layouting stuff
		ProcessSIBBuild.update(cNewSIB as ProcessSIB)
		
		// delete the old sib
		cSIB.delete
		
		return cNewSIB
	}
}