package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.EventListener

/** 
 * Context menu action, which converts all static ports to
 * input ports
 * @author zweihoff
 */
class EventMultiStaticToInput extends DIMECustomAction<EventListener> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Convert to input ports"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(EventListener guiSib) {
		//at least one primitive input port without incoming edge
		!guiSib.inputStatics.empty
	}

	override void execute(EventListener guiSib) {
		guiSib.inputStatics.forEach[n|
			val p = new StaticToPrimitivePort
			p.execute(n)
		]
	}
}
