package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.DirectDataFlow

import static info.scce.dime.process.helper.EdgeLayoutUtils.getManhattanPoints

import static extension info.scce.dime.process.helper.EdgeLayoutUtils.*

class SimpleTwoPointRoute<T extends DirectDataFlow> extends DIMECustomAction<DirectDataFlow> {
	
	override getName() {
		"Convert to Two-Point Route"
	}
	
	override execute(DirectDataFlow edge) {
//		val model = wrapGraphModel(edge.rootElement, edge.diagram)
//		val cEdge = model.findCDirectDataFlow(edge)
		val source = edge.sourceElement
		val target = edge.targetElement
		
		for (i: 0 ..< edge.bendpoints.size)
			edge.removeBendpoint(0)
			
		for (point : getManhattanPoints(source, target)) {
			edge.addBendpoint(point.x, point.y)
		}
	}
	 
}