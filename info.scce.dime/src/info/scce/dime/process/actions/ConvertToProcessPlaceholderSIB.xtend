package info.scce.dime.process.actions

import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType

/** 
 * Context menu action, which converts a process sib to 
 * a process placeholder sib.
 * 
 * @author neubauer
 */
class ConvertToProcessPlaceholderSIB extends ConvertSIBCustomAction<ProcessSIB> {

	/** 
	 * Returns the name of the context menu entry.
	 */
	override getName() {
		"Convert to placeholder"
	}
	
	/** 
	 * Guarantees that the Process SIB is not already a placeholder.
	 */
	override canExecute(ProcessSIB sib) {
		!(sib.rootElement.processType == ProcessType.NATIVE_FRONTEND_SIB_LIBRARY) && 
		!(sib instanceof ProcessPlaceholderSIB)
	}

	override void execute(ProcessSIB sib) {
		val cProcess = sib.rootElement
		sib.convertSIB[ cProcess.newProcessPlaceholderSIB(sib.proMod, sib.x, sib.y) ]
	}
}
