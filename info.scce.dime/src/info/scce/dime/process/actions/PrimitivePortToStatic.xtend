package info.scce.dime.process.actions

import graphmodel.Container
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveType

import static extension info.scce.dime.process.helper.PortUtils.addStaticPort
import static extension info.scce.dime.process.helper.PortUtils.reconnectIncoming

class PrimitivePortToStatic extends DIMECustomAction<PrimitiveInputPort> {
	
	override getName() {
		"Convert Input to Static Value"
	}

	override canExecute(PrimitiveInputPort port) {
		PrimitiveType::FILE !== port.dataType
	}

	override execute(PrimitiveInputPort port) {
		try {
			val container = port.container as Container
			val oldPorts = newArrayList
			// re-add all ports to preserve their order
			container.allNodes.filter(Input).forEach[oldPort |
				if (oldPort === port) {
					container.addStaticPort(port)
				} else {
					var oldClone = oldPort.clone(container)
					oldPort.reconnectIncoming(oldClone)
				}
				oldPorts.add(oldPort)
			]
			oldPorts.forEach[delete]
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
}
