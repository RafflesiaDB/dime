package info.scce.dime.process.actions;

import static info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider.getSupportedModelElement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;

import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericSIBBuild;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.UpdateGenericSIBBuild;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.process.build.BranchCreator;
import info.scce.dime.process.build.UpdateBranchCreator;
import info.scce.dime.process.process.GenericSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.SIB;

public class UpdateGenericSIB extends UpdateSIBAction<GenericSIB> {

	protected Process cModel;
	protected BranchCreator branchCreator;
	protected GenericSIBBuild<EObject> sibBuild;

	@Override
	public void update(GenericSIB sib) {
		cModel = sib.getRootElement();
		SIB cSib = sib;
		sibBuild = getGenericSIBBuild(sib);
		if (sib.getReferencedObject() == null) {
			letUserKnow("Reference is null", "Update failed. Reference not found!");
			sib.delete();
			return;
		}
		branchCreator = new UpdateBranchCreator(cSib);
		UpdateGenericSIBBuild updateGenericSIBBuild = new UpdateGenericSIBBuild(sibBuild, sib, branchCreator);
		updateGenericSIBBuild.update();
	}

	
	private GenericSIBBuild<EObject> getGenericSIBBuild(GenericSIB sib) {
		EObject sibReference = sib.getReferencedObject();
		IModeltrafoSupporter<EObject> modelTrafoSupporter = getSupportedModelElement(sibReference);
		if (modelTrafoSupporter != null) {
			IGenericSIBBuilder<EObject> modellingProvider = modelTrafoSupporter.getModellingProvider();
			GenericSIBBuild<EObject> sibBuild = modellingProvider.getSIBBuild(sibReference);
			return sibBuild;
		} 
		return null;
	}
	
	private void letUserKnow(String title, String msg) {
		MessageDialog.openInformation(null, title, msg);
	}

}
