package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.EndSIB

class EndSIBNewPrimitiveOutput extends DIMECustomAction<EndSIB> {
	
	override getName() {
		"New Primitive Output"
	}

	//TODO beautify after auto conversion to Xtend class

	override execute(EndSIB sib) {
		try {
			// layout and resize is then automatically done by OutputPortHook
			sib.newPrimitiveInputPort(1, 1)
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace()
		}

	}
}
