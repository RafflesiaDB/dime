package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.PrimitiveInputPort

/** 
 * Context menu action, which converts all primitive
 * input ports, which are not connected by data flow, to a static input
 * @author zweihoff
 */
class MultiInputToStatic<T extends DataFlowTarget> extends DIMECustomAction<T> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Convert to static inputs"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(DataFlowTarget guiSib) {
		//at least one primitive input port without incoming edge
		!guiSib.inputs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].empty
	}

	override void execute(DataFlowTarget guiSib) {
		// replacing directly will result in concurrent modification problem (without exception though)
		val allToBeReplaced = guiSib.inputs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].map[id].toList
		
		val primitiveToStaticConverter = new PrimitivePortToStatic
		for (toBeReplaced : allToBeReplaced) {
			guiSib.inputs.filter(PrimitiveInputPort).filter[id == toBeReplaced].forEach[n|
				primitiveToStaticConverter.execute(n)
		]
		
		}
	}
}
