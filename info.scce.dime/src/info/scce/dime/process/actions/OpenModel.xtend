package info.scce.dime.process.actions

import graphmodel.Node
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.SIB

import static extension info.scce.dime.process.helper.BranchUtils.getReferencedComponent
import static extension info.scce.dime.process.helper.SIBUtils.getReferencedComponent
import info.scce.dime.process.process.ProcessInputStatic

class OpenModel extends DIMECustomAction<Node> {
	
	override execute(Node it) {
		println("Opening submodel for " + eClass.name)
		switch it {
			SIB: referencedComponent
			Branch: referencedComponent
			ProcessInputStatic: value
		}.openEditor
	}

	override hasDoneChanges() {
		false
	}
	
}