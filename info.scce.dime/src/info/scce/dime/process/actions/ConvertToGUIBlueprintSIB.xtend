package info.scce.dime.process.actions

import info.scce.dime.process.process.GUISIB

/** 
 * Action that converts a GUI SIB to a GUI Blueprint SIB.
 */
class ConvertToGUIBlueprintSIB extends ConvertToBlueprintSIB<GUISIB> {

	override createBlueprintSIB(GUISIB sib) {
		sib.rootElement.newGUIBlueprintSIB(sib.x, sib.y, sib.width, sib.height) => [
			label = sib.label
			name = sib.name
			majorBranch = sib.majorBranch
			majorPage = sib.majorPage
			defaultContent = sib.defaultContent
		]
	}
	
}
