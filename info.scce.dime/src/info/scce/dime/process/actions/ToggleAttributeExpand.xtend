package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.helper.AttributeExpandUtils
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.DataContext

class ToggleAttributeExpand extends DIMECustomAction<ComplexVariable> {
	val OFFSET = 30
	
	override getName() {
		"Toggle Attribute Expand"
	}

	override void execute(ComplexVariable variable) {
		if (!variable.isExpanded()) {
			AttributeExpandUtils::expand(variable)
			layout(variable)
		} else {
			AttributeExpandUtils::collapse(variable)
			layout(variable)
		}
	}
	
	def layout(ComplexVariable node) {
		if(node.container instanceof DataContext){
			var dataContext = node.container as DataContext
			var dataCtxLowerBound = dataContext.height -OFFSET
			var nodeLowerBoundX = node.y + node.height
			if(dataCtxLowerBound < nodeLowerBoundX){
				var diff = nodeLowerBoundX - dataCtxLowerBound
				dataContext.height = dataContext.height + diff
			}
			var maxHeight = maxHeight(dataContext)
			dataContext.height = maxHeight + OFFSET
		}
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
	
}
