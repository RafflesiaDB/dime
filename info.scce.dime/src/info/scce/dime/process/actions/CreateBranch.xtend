package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.TriggerEventSIB

class CreateBranch extends DIMECustomAction<TriggerEventSIB> {

	override getName() {
		"Create Branches"
	}

	override boolean canExecute(TriggerEventSIB sib) {
		sib.branchSuccessors.empty 
	}

	override void execute(TriggerEventSIB sib) {
		if(sib.branchSuccessors.empty) {
			val cProcess = sib.getRootElement() as info.scce.dime.process.process.Process
			val branch = cProcess.newBranch(sib.x+80,sib.y+80)
			branch.name = "success";
			sib.newBranchConnector(branch)
		}
		
	}
}
