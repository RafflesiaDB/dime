package info.scce.dime.process.actions

import info.scce.dime.process.build.GuardedProcessSIBBuild
import info.scce.dime.process.process.GuardedProcessSIB

class UpdateGuardedProcessSIB extends UpdateSIBAction<GuardedProcessSIB> {
	
	override update(GuardedProcessSIB sib) {
		GuardedProcessSIBBuild.update(sib) 
	}
	
}