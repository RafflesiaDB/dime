package info.scce.dime.process.actions

import info.scce.dime.process.build.NativeFrontendSIBReferenceBuild
import info.scce.dime.process.process.NativeFrontendSIBReference
import org.eclipse.graphiti.features.IFeatureProvider

class UpdateNativeFrontendSIBReference extends UpdateSIBAction<NativeFrontendSIBReference> {
	
	override update(NativeFrontendSIBReference sib) {
		NativeFrontendSIBReferenceBuild.update(sib)
	}
	
}