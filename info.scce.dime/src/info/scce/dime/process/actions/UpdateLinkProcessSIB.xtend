package info.scce.dime.process.actions

import info.scce.dime.process.build.LinkProcessSIBBuild
import info.scce.dime.process.process.LinkProcessSIB

class UpdateLinkProcessSIB extends UpdateSIBAction<LinkProcessSIB> {
	
	override update(LinkProcessSIB sib) {
		LinkProcessSIBBuild.update(sib) 
	}
	
}