package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.helper.NodeLayout
import info.scce.dime.process.process.Process
import graphmodel.Container
import info.scce.dime.process.process.GuardContainer
import info.scce.dime.process.process.GuardProcessSIB
import info.scce.dime.process.process.GuardedProcessSIB
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.DataFlowSource
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.DataContext
import info.scce.dime.process.process.Variable
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.Attribute

class ToggleDataFlowView extends DIMECustomAction<Process> {
	
	override getName() {
		"Toggle DataFlow View"
	}

	//TODO beautify after auto conversion to Xtend class

	override execute(Process processModel) {
		try {
			var boolean df = !processModel.isDataFlowView()
			processModel.setDataFlowView(df)
			for (Container container : processModel.getAllContainers()) {
				if (container instanceof GuardContainer) {
					var GuardContainer guardContainer = (container as GuardContainer)
					for (GuardProcessSIB guardProcess : guardContainer.getGuardProcessSIBs()) {
						guardProcess.resize(guardProcess.getWidth(),
							NodeLayout::getSIBHeight(if(df) guardProcess.getInputs().size() else 0))
					}
					for (GuardedProcessSIB guardedProcess : guardContainer.getGuardedProcessSIBs()) {
						guardedProcess.resize(guardedProcess.getWidth(),
							NodeLayout::getSIBHeight(if(df) guardedProcess.getInputs().size() else 0))
					}
				} else if (container instanceof DataFlowTarget) {
					var DataFlowTarget dft = (container as DataFlowTarget)
					dft.resize(dft.getWidth(), NodeLayout::getSIBHeight(if(df) dft.getInputs().size() else 0))
				} else if (container instanceof DataFlowSource) {
					var DataFlowSource dfs = (container as DataFlowSource)
					if (container instanceof AbstractBranch) {
						dfs.resize(dfs.getWidth(), NodeLayout::getBranchHeight(if(df) dfs.getOutputs().size() else 0))
					} else {
						dfs.resize(dfs.getWidth(), NodeLayout::getSIBHeight(if(df) dfs.getOutputs().size() else 0))
					}
				}
			}
			//TODO create/use extension methods for this stuff
			// hide df edges
			// TODO: wrappedProcessModel should have getCDataFlows() and getCReads() and getCWrites()
			
			// FIXME: Changed during api overhaul: Removed the for loops below and added the loop for all DataFlow edges
			for (DataFlow edge :processModel.allNodes.filter(DataFlow)){
				edge.dfViewWorkaround = !edge.dfViewWorkaround
			}
//			for (DataFlow edge : processModel.getPrimitiveReads()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getPrimitiveUpdates()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getComplexReads()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getComplexUpdates()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getJavaNativeReads()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getJavaNativeUpdates()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getPrimitiveDirectDataFlows()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getComplexDirectDataFlows()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
//			for (DataFlow edge : processModel.getJavaNativeDirectDataFlows()) {
//				edge.setDfViewWorkaround(!edge.isDfViewWorkaround())
//			}
			// hide df container and data
			for (DataContext context : processModel.getDataContexts()) {
				context.setDfViewWorkaround(!context.isDfViewWorkaround())
				for (Variable variable : context.getVariables()) {
					variable.setDfViewWorkaround(!variable.isDfViewWorkaround())
					if (variable instanceof ComplexVariable) {
						var ComplexVariable complexVariable = (variable as ComplexVariable)
						for (Attribute attribute : complexVariable.getAttributes()) {
							attribute.setDfViewWorkaround(!attribute.isDfViewWorkaround())
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace()
		}

	}
}
