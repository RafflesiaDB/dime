package info.scce.dime.process.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.SIB


abstract class UpdateSIBAction<SIBType extends SIB> extends DIMECustomAction<SIBType>{
	
	override getName() {
		"Update SIB"
	}
	
	override execute(SIBType sib) {
		update(sib)
	}
	
	def void update(SIBType sib)
	
}