package info.scce.dime.process.actions

import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.List
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.CommonPlugin
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.jface.dialogs.MessageDialog
import graphmodel.Edge
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.AbstractBranch
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.BranchBlueprint
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ControlFlow
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.JavaNativeInputPort
import info.scce.dime.process.process.JavaNativeOutputPort
import info.scce.dime.process.process.Output
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.ProcessBlueprintSIB
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import graphmodel.ModelElementContainer
import info.scce.dime.process.process.StartSIB
import info.scce.dime.process.process.EndSIB

//TODO beautify after auto conversion to Xtend class
class CreateSubmodel extends DIMECustomAction<ProcessBlueprintSIB> {
	
	info.scce.dime.process.process.Process process
	File newFile
	info.scce.dime.process.process.Process newProcess
	ProcessSIB newProcessSib
	ProcessBlueprintSIB sib
	ProcessType cSibProcessType
	int actualEndSibX = 350

	override getName() {
		"Create SubModel from BluePrint"
	}

	def private void init(ProcessBlueprintSIB bluePrintSib) {
		this.sib = bluePrintSib
		process = sib.getRootElement() 
//		cSib = process.findCProcessBlueprintSIB(sib)
//		cSibProcessType = cSib.getProcessType()
	}

	override void execute(ProcessBlueprintSIB bluePrintSib) {
		init(bluePrintSib)
		createFilePathForNewModel(sib)
		var boolean overwriteAndContinue = true
		if(newFile !== null && newFile.exists()) overwriteAndContinue = showDialog("Target model already exitst",
			"The file for the target model already exists. Do you want overwrite it?")
		if(!overwriteAndContinue) return;
		createSubModel(sib.getLabel(), cSibProcessType)
		addStartSib()
		addEndSibs()
		if (newProcess !== null)
			try {
				newProcess.eResource().save(null)
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace()
			}
		else {
			throw new IllegalStateException("could not create new process")
		}
		createProcessSib()
		if (newProcessSib !== null) {
			copyProcessSibPorts()
			createBranches()
			deleteBluePrint()
		} else {
			throw new IllegalStateException("could not create ProcessSIB")
		}
	}

	def private void deleteBluePrint() {
		for (Branch branch : sib.getSuccessors(typeof(Branch))) {
			branch.delete()
		}
		sib.delete()
	}

	//TODO create/use extension methods for this stuff
	def private void copyProcessSibBranchPorts(AbstractBranch branch, Branch cBranch) {
		for (Output output : branch.getOutputs()) {
			var Output cOutput = addOutput(cBranch, output)
			var List<Edge> outEdges = new ArrayList(output.getOutgoing())
			for (Edge edge : outEdges) {
//				var Edge cEdge = process.findCEdge(edge)
//				reconnect(cEdge, cOutput, null)
				edge.reconnectSource(cOutput)
			}
		}
	}

	//TODO create/use extension methods for this stuff
	def private Output addOutput(Branch cBranch, Output output) {
		if (output instanceof ComplexOutputPort) {
			var ComplexOutputPort outputPort = (output as ComplexOutputPort)
			var ComplexOutputPort port = cBranch.newComplexOutputPort(outputPort.getDataType(), 0, 0)
			port.setName(outputPort.getName())
			port.setIsList(outputPort.isIsList())
			return port
		} else if (output instanceof PrimitiveOutputPort) {
			var PrimitiveOutputPort outputPort = (output as PrimitiveOutputPort)
			var PrimitiveOutputPort port = cBranch.newPrimitiveOutputPort(0, 0)
			port.setDataType(outputPort.getDataType())
			port.setName(outputPort.getName())
			port.setIsList(outputPort.isIsList())
			return port
		} else if (output instanceof JavaNativeOutputPort) {
			var JavaNativeOutputPort outputPort = (output as JavaNativeOutputPort)
			var JavaNativeOutputPort port = cBranch.newJavaNativeOutputPort(outputPort.getDataType(), 0, 0)
			port.setName(outputPort.getName())
			port.setIsList(outputPort.isIsList())
			return port
		} else {
			throw new IllegalStateException("unknown port type of output")
		}
	}

	//TODO create/use extension methods for this stuff
	def private void createBranches() {
		for (AbstractBranch branch : sib.getSuccessors(typeof(AbstractBranch))) {
			var ModelElementContainer mec = branch.getContainer()
			if (mec instanceof info.scce.dime.process.process.Process) {
				var info.scce.dime.process.process.Process container = (mec as info.scce.dime.process.process.Process)
				var Branch newbranch = container.newBranch(branch.getX(), branch.getY())
				newbranch.setName(branch.getName())
				newProcessSib.newBranchConnector(newbranch)
				copyProcessSibBranchPorts(branch, newbranch)
				for (ControlFlow cf : branch.getOutgoing(typeof(ControlFlow))) {
					cf.reconnectSource(newbranch)
				}
			}
		}
	}
	
	//TODO create/use extension methods for this stuff
	def private void copyProcessSibPorts() {
		for (Input input : sib.getInputs()) {
			var Input cInput = addInput(input)
			var List<Edge> inEdges = new ArrayList(input.getIncoming())
			for (Edge edge : inEdges) {
				edge.reconnectTarget(cInput)
			}
		}
	}

	//TODO create/use extension methods for this stuff
	def private Input addInput(Input input) {
		if (input instanceof ComplexInputPort) {
			var ComplexInputPort inputPort = (input as ComplexInputPort)
			var ComplexInputPort port = newProcessSib.newComplexInputPort(inputPort.getDataType(), 0, 0)
//			port.setId(inputPort.getId())
			port.setName(inputPort.getName())
			port.setIsList(inputPort.isIsList())
			return port
		} else if (input instanceof PrimitiveInputPort) {
			var PrimitiveInputPort inputPort = (input as PrimitiveInputPort)
			var PrimitiveInputPort port = newProcessSib.newPrimitiveInputPort(0, 0)
//			port.setId(inputPort.getId())
			port.setDataType(inputPort.getDataType())
			port.setName(inputPort.getName())
			port.setIsList(inputPort.isIsList())
			return port
		} else if (input instanceof InputStatic) {
			if (input instanceof IntegerInputStatic) {
				var IntegerInputStatic inputPort = (input as IntegerInputStatic)
				var IntegerInputStatic port = newProcessSib.newIntegerInputStatic(0, 0)
//				port.setId(inputPort.getId())
				port.setName(inputPort.getName())
				port.setValue(inputPort.getValue())
				return port
			} else if (input instanceof BooleanInputStatic) {
				var BooleanInputStatic inputPort = (input as BooleanInputStatic)
				var BooleanInputStatic port = newProcessSib.newBooleanInputStatic(0, 0)
//				port.setId(inputPort.getId())
				port.setName(inputPort.getName())
				port.setValue(inputPort.isValue())
				return port
			} else if (input instanceof TextInputStatic) {
				var TextInputStatic inputPort = (input as TextInputStatic)
				var TextInputStatic port = newProcessSib.newTextInputStatic(0, 0)
//				port.setId(inputPort.getId())
				port.setName(inputPort.getName())
				port.setValue(inputPort.getValue())
				return port
			} else if (input instanceof RealInputStatic) {
				var RealInputStatic inputPort = (input as RealInputStatic)
				var RealInputStatic port = newProcessSib.newRealInputStatic(0, 0)
//				port.setId(inputPort.getId())
				port.setName(inputPort.getName())
				port.setValue(inputPort.getValue())
				return port
			} else if (input instanceof TimestampInputStatic) {
				var TimestampInputStatic inputPort = (input as TimestampInputStatic)
				var TimestampInputStatic port = newProcessSib.newTimestampInputStatic(0, 0)
//				port.setId(inputPort.getId())
				port.setName(inputPort.getName())
				port.setValue(inputPort.getValue())
				return port
			}
		} else if (input instanceof JavaNativeInputPort) {
			var JavaNativeInputPort inputPort = (input as JavaNativeInputPort)
			var JavaNativeInputPort port = newProcessSib.newJavaNativeInputPort(inputPort.getDataType(), 0, 0)
//			port.setId(inputPort.getId())
			port.setName(inputPort.getName())
			port.setIsList(inputPort.isIsList())
			return port
		}
		throw new IllegalStateException("unknown port type of input")
	}

	def private void createProcessSib() {
		var ModelElementContainer cMEC = sib.getContainer()
		if (cMEC instanceof info.scce.dime.process.process.Process) {
			var info.scce.dime.process.process.Process cContainer = (cMEC as info.scce.dime.process.process.Process)
			newProcessSib = cContainer.newProcessSIB(newProcess, sib.getX(), sib.getY())
			newProcessSib.setLabel(sib.getLabel())
			init(sib)
			for (Input cInput : newProcessSib.getInputs()) {
				cInput.delete()
			}
			for (Branch cBranch : newProcessSib.getSuccessors(typeof(Branch))) {
				cBranch.delete()
			}
			for (ControlFlow cf : sib.getIncoming(typeof(ControlFlow))) {
				cf.reconnectTarget(newProcessSib)
			}
		}
	}

	//TODO create/use extension methods for this stuff
	def private void addEndSibs() {
		for (BranchBlueprint branch : sib.getSuccessors(typeof(BranchBlueprint))) {
			var EndSIB cEnd = newProcess.newEndSIB(actualEndSibX, 700)
			cEnd.setBranchName(branch.getName())
			actualEndSibX += 150
			for (Output output : branch.getOutputs()) {
				if (output instanceof ComplexOutputPort) {
					var ComplexOutputPort outputPort = (output as ComplexOutputPort)
					var ComplexInputPort inputPort = cEnd.newComplexInputPort(outputPort.getDataType(), 0, 0)
					inputPort.setName(outputPort.getName())
					inputPort.setIsList(outputPort.isIsList())
				} else if (output instanceof PrimitiveOutputPort) {
					var PrimitiveOutputPort outputPort = (output as PrimitiveOutputPort)
					var PrimitiveInputPort inputPort = cEnd.newPrimitiveInputPort(0, 0)
					inputPort.setDataType(outputPort.getDataType())
					inputPort.setName(outputPort.getName())
					inputPort.setIsList(outputPort.isIsList())
				} else if (output instanceof JavaNativeOutputPort) {
					var JavaNativeOutputPort outputPort = (output as JavaNativeOutputPort)
					var JavaNativeInputPort inputPort = cEnd.newJavaNativeInputPort(outputPort.getDataType(), 0, 0)
					inputPort.setName(outputPort.getName())
					inputPort.setIsList(outputPort.isIsList())
				} else {
					throw new IllegalStateException("unknown port type of output")
				}
			}
		}
	}

	//TODO create/use extension methods for this stuff
	def private void addStartSib() {
		var StartSIB start = newProcess.newStartSIB(350, 50)
		for (Input input : sib.getInputs()) {
			if (input instanceof ComplexInputPort) {
				var ComplexInputPort inputPort = (input as ComplexInputPort)
				var ComplexOutputPort outputPort = start.newComplexOutputPort(inputPort.getDataType(), 0, 0)
				outputPort.setName(inputPort.getName())
				outputPort.setIsList(inputPort.isIsList())
			} else if (input instanceof PrimitiveInputPort) {
				var PrimitiveInputPort inputPort = (input as PrimitiveInputPort)
				var PrimitiveOutputPort outputPort = start.newPrimitiveOutputPort(0, 0)
				outputPort.setName(inputPort.getName())
				outputPort.setIsList(inputPort.isIsList())
				outputPort.setDataType(inputPort.getDataType())
			} else if (input instanceof InputStatic) {
				var InputStatic inputPort = (input as InputStatic)
				var PrimitiveOutputPort outputPort = start.newPrimitiveOutputPort(0, 0)
				outputPort.setName(inputPort.getName())
				outputPort.setIsList(false)
				if(inputPort instanceof IntegerInputStatic) outputPort.setDataType(
					PrimitiveType::INTEGER) else if(inputPort instanceof BooleanInputStatic) outputPort.setDataType(
					PrimitiveType::BOOLEAN) else if(inputPort instanceof TextInputStatic) outputPort.setDataType(
					PrimitiveType::TEXT) else if(inputPort instanceof RealInputStatic) outputPort.setDataType(
					PrimitiveType::REAL) else if(inputPort instanceof TimestampInputStatic) outputPort.setDataType(
					PrimitiveType::TIMESTAMP)
			} else if (input instanceof JavaNativeInputPort) {
				var JavaNativeInputPort inputPort = (input as JavaNativeInputPort)
				var JavaNativeOutputPort outputPort = start.newJavaNativeOutputPort(inputPort.getDataType(), 0, 0)
				outputPort.setName(inputPort.getName())
				outputPort.setIsList(inputPort.isIsList())
			} else {
				throw new IllegalStateException("unknown port type of input")
			}
		}
	}

	def private void createFilePathForNewModel(ProcessBlueprintSIB sib) {
		var URI resolvedFile = CommonPlugin::resolve(EcoreUtil::getURI(sib.getRootElement()))
		var IFile iFile = ResourcesPlugin::getWorkspace().getRoot().getFileForLocation(
			new Path(resolvedFile.toFileString()))
		var IPath newPath = iFile.getParent().getFullPath()
		newPath = newPath.append(sib.getLabel()).addFileExtension("process")
		this.newFile = new File(ResourcesPlugin::getWorkspace().getRoot().getLocation().append(newPath).toString())
	}

	def private void createSubModel(String name, ProcessType type) {
		var URI resolvedFile = CommonPlugin::resolve(EcoreUtil::getURI(sib.getRootElement()))
		var IFile iFile = ResourcesPlugin::getWorkspace().getRoot().getFileForLocation(
			new Path(resolvedFile.toFileString()))
//		try {
//			newProcess = ProcessWrapper::newProcess(iFile.getParent().getFullPath(), sib.getLabel())
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace()
//		}

		// postcreate might initialize start/end/context if process type folder name is recognized
		newProcess.getStartSIBs().forEach([s|s.delete()])
		newProcess.getEndSIBs().forEach([e|e.delete()])
		newProcess.getDataContexts().forEach([c|c.delete()])
		newProcess.setProcessType(type)
		newProcess.newDataContext(50, 50, 180, 800)
	}

	def private boolean showDialog(String title, String msg) {
		return MessageDialog::openConfirm(null, title, msg)
	}
}
