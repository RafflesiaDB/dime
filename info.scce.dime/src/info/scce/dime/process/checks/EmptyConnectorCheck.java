package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.ComplexAttributeConnector;
import info.scce.dime.process.process.ComplexListAttributeConnector;

public class EmptyConnectorCheck extends AbstractCheck<ProcessId, ProcessAdapter> {
	
	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof ComplexAttributeConnector) {
				ComplexAttributeConnector con = (ComplexAttributeConnector) obj;
				checkComplexAttributeConnector(id, con);
			}
			if (obj instanceof ComplexListAttributeConnector) {
				ComplexListAttributeConnector con = (ComplexListAttributeConnector) obj;
				checkComplexListAttributeConnector(id, con);
			}
		}
	}
	
	@Override
	public void init() {}
	
	private void checkComplexAttributeConnector(ProcessId id, ComplexAttributeConnector con) {
		if (con.getAttributeName().equals(""))
			addError(id, "AssociationName is empty. Please correct!");
	}
	
	private void checkComplexListAttributeConnector(ProcessId id, ComplexListAttributeConnector con) {
		if (con.getAttributeName().equals(""))
			addError(id, "AttributeName is empty. Please correct!");
	}
}
