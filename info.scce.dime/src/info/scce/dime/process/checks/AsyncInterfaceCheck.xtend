package info.scce.dime.process.checks

import info.scce.dime.process.mcam.modules.checks.ProcessCheck
import static info.scce.dime.process.process.ProcessType.*
import info.scce.dime.process.process.Process

/**
 * The async interface check tests, whether it is a valid asynchronous process 
 * (i.e., has no complex inputs, at most one end sib with branchname "success", and no outputs). 
 */
class AsyncInterfaceCheck extends ProcessCheck {
	
	override check(Process it) {
		if(processType != ASYNCHRONOUS) return;
		
		startSIBs.map[complexOutputPorts].flatten.forEach [
			addError('''Async processes are not allowed to have complex types in their interface.''')
		]
		
		if (endSIBs.size > 1)
			addError('''Async processes are allowed to have at most one end sib.''')
			
		endSIBs.filter[branchName != "success"].forEach[
			addError('''Async processes' branch has to be named "success". Found: «branchName»''')
		]
		
		endSIBs.map[inputPorts].flatten.forEach [
			addError('''Async processes are not allowed to have outputs''')
		]
	}
	
}
									