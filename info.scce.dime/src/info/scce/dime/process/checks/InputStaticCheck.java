package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.TextInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;

public class InputStaticCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof TextInputStatic) {
				checkText(id, (TextInputStatic) obj);	
			}
			if (obj instanceof TimestampInputStatic) {
				checkTimeStamp(id, (TimestampInputStatic) obj);	
			}
		}
	}
	
	@Override
	public void init() {}
	
	private void checkTimeStamp(ProcessId id, TimestampInputStatic obj) {
		if(String.valueOf(obj.getValue()).length() > 10){
			addError(id, "Only 10 digits allowed");
		}
		if(obj.getValue()<0){
			addError(id, "Timestamp has to be positive");
		}
		
	}
	
	private void checkText(ProcessId id, TextInputStatic te) {
		String value = te.getValue();
		
		if (value != null) {
			//check escapable characters
			for(String i:info.scce.dime.gui.checks.InputStaticCheck.escapable){
				String subString = value;
				while(subString.indexOf(i)>-1){
					int indexOf = subString.indexOf(i);
					if(indexOf > 0){
						if(subString.charAt(indexOf-1)!='\\'){
							addError(id, "value char: "+i+" has to be escaped");
						}
					}else{
						addError(id, "value char: "+i+" has to be escaped");
					}
					subString = subString.substring(indexOf+1);
				}
			}

		}
	}

}
