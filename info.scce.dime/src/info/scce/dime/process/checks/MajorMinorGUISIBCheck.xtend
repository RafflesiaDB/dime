package info.scce.dime.process.checks

import info.scce.dime.checks.ProcessCheck
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.GUIBlueprintSIB
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.GenericSIB

class MajorMinorGUISIBCheck extends ProcessCheck {
	
	override check(Process model) {
		model.find(SIB).forEach[switch it {
			GUISIB: if (isMajorPage) checkMajorGUI else checkMinorGUI
			GUIBlueprintSIB: if (isMajorPage) checkMajorGUI else checkMinorGUI
			GenericSIB: if (guiOptions !== null && guiOptions.isMajorPage) checkMajorGUI else if(guiOptions !== null) checkMinorGUI
		}]
	}
	
	def checkMajorGUI(SIB it) {
		if (majorBranch !== null) {
			addWarning("major branch has no effect on major GUISIBs")
		}
		
		val defaultContent = defaultContent
		if (defaultContent !== null) switch defaultContent {
			GUISIB: {
				check[
					! defaultContent.isMajor
				].elseError("default content cannot be major GUISIB")
				checkPlaceholder
			}
			
			GUIBlueprintSIB: {
				check[
					! defaultContent.isMajor
				].elseError("default content cannot be major GUISIB")
				checkPlaceholder
			}
			
			default: addError("default content must be minor Interaction SIB or minor GUISIB")
		}
	}
	
	def getDefaultContent(SIB it) {
		switch it {
			GUISIB: defaultContent
			GUIBlueprintSIB: defaultContent
			GenericSIB: guiOptions.defaultContent
		}
	}
	
	def checkMinorGUI(SIB it) {
		checkPlaceholder
		check[
			defaultContent === null
		].elseWarning("default content has no effect on minor GUISIBs")
		
		if(majorBranch === null) {
			addError('''Major branch not set''')
		}
	}
	
	def checkPlaceholder(SIB it) {
		if (majorBranch === null)
			return
		val major = majorBranch?.findSourceOf(BranchConnector)
		if (major instanceof GUISIB) {
			switch size: major.gui.find(Placeholder).size {
				case size < 1: addError("no placeholder present")
				case size > 1: addError("multiple placeholders present")
			}
		} else if(major instanceof GenericSIB) {
			// do nothing
		} else if (!(major instanceof GUIBlueprintSIB)) {
			addError("only branches of GUISIBs and GUIBlueprintSIBs can be major branches")
		}
	}
	
}
