package info.scce.dime.process.checks;

import graphmodel.Node;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.ControlFlow;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.IterateSIB;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.StartSIB;

import java.util.ArrayList;
import java.util.HashMap;

public class LoopCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	private ProcessAdapter adapter;

	private ArrayList<Node> knownElements = new ArrayList<>();
	private HashMap<Node, Integer> loopLevel = new HashMap<Node, Integer>();

	@Override
	public void doExecute(ProcessAdapter adapter) {
		this.adapter = adapter;
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Process) {
				calculateLoopAssociation((Process) obj);
//				printAssocs();
				analyzeLoopAssociation((Process) obj);
			}
		}
		processResults();
	}
	
	@Override
	public void init() {
		knownElements.clear();
		loopLevel.clear();
	}

	private void analyzeLoopAssociation(Process process) {
//		System.out.println("Process: " + process.getModelName());
		
		for (Node node : loopLevel.keySet()) {
			if (node instanceof DataFlowTarget == false)
				continue;

			for (ControlFlow edge : node.getIncoming(ControlFlow.class)) {
				Node preNode = edge.getSourceElement();
				
//				System.out.println("Node: " + node + " - " + loopLevel.get(node));
//				System.out.println("PreNode: " + preNode + " - " + loopLevel.get(preNode));
				
				if (loopLevel.get(node) == null || loopLevel.get(preNode) == null)
					continue;
				
				if (loopLevel.get(node) < loopLevel.get(preNode)) {
					addWarning(adapter.getIdByString(preNode.getId()),
							"Eary loop exit detected");
				}
			}
		}
	}

	private void calculateLoopAssociation(Process process) {
		for (StartSIB startSib : process.getStartSIBs()) {
			loopLevel.put(startSib, 0);
			for (DataFlowTarget dft : startSib
					.getSuccessors(DataFlowTarget.class)) {
				traverseAssociation(dft, loopLevel.get(startSib));
			}
		}
	}

	private void traverseAssociation(DataFlowTarget dft, Integer level) {
		// already visited ?
		if (knownElements.contains(dft))
			return;
		knownElements.add(dft);

		// set loop information
		loopLevel.put(dft, level);
		if (dft instanceof IterateSIB)
			loopLevel.put(dft, level + 1);

		for (DataFlowSource dfs : dft.getSuccessors(DataFlowSource.class)) {
			loopLevel.put(dfs, loopLevel.get(dft));
			if (dft instanceof IterateSIB && dfs instanceof Branch) {
				Branch branch = (Branch) dfs;
				if (!branch.getName().equals("next"))
					loopLevel.put(dfs, loopLevel.get(dft) - 1);
			}

			for (DataFlowTarget dftSucc : dfs
					.getSuccessors(DataFlowTarget.class)) {
				traverseAssociation(dftSucc, loopLevel.get(dfs));
			}
		}
	}
}
