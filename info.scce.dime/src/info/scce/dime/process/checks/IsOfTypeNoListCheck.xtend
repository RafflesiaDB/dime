package info.scce.dime.process.checks

import info.scce.dime.process.mcam.modules.checks.ProcessCheck
import info.scce.dime.process.process.Process

/**
 * The no list check for the is of type sib tests, whether the input parameter and output parameter are both or none lists. 
 */
class IsOfTypeNoListCheck extends ProcessCheck {
	
	override check(Process model) {
		model.isOfTypeSIBs.forEach[
			if(inputPorts.head.isIsList != branchSuccessors.exists[outputPorts.exists[isList]]) 
				addError("Either both input and output have to be of list type or none.")
		]
	}
	
}
									