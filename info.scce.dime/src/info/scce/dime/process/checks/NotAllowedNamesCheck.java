package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.Variable;

import java.util.Arrays;

public class NotAllowedNamesCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	private String[] notAllowedVariableNames = {"var"};
	
	private String[] notAllowedEndSibPortNames = {"var", "iteratorElement"};
	
	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Variable) {
				Variable var = (Variable) obj;
				if (Arrays.asList(notAllowedVariableNames).contains(var.getName()))
					addError(id, var.getName()
							+ " not allowed");
			}
			if (obj instanceof EndSIB) {
				EndSIB endSib = (EndSIB) obj;
				for (Input input : endSib.getInputs()) {
					if (Arrays.asList(notAllowedEndSibPortNames).contains(input.getName()))
						addError(id, input.getName()
								+ " not allowed");
				}
			}
		}
	}
	
	@Override
	public void init() {}

}
