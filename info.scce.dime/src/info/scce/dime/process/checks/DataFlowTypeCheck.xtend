package info.scce.dime.process.checks

import graphmodel.Edge
import info.scce.dime.checks.ProcessCheck
import info.scce.dime.process.process.ComplexDirectDataFlow
import info.scce.dime.process.process.ComplexRead
import info.scce.dime.process.process.ComplexUpdate
import info.scce.dime.process.process.DataFlow
import info.scce.dime.process.process.JavaNativeDirectDataFlow
import info.scce.dime.process.process.JavaNativeRead
import info.scce.dime.process.process.JavaNativeUpdate
import info.scce.dime.process.process.PrimitiveDirectDataFlow
import info.scce.dime.process.process.PrimitiveRead
import info.scce.dime.process.process.PrimitiveUpdate
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.Variable
import info.scce.dime.process.process.Attribute

class DataFlowTypeCheck extends ProcessCheck {

	override void check(Process model) {
		model.find(DataFlow).forEach[switch it {
			PrimitiveRead, 
			  PrimitiveUpdate, 
			  PrimitiveDirectDataFlow: checkPrimitive
			ComplexRead,
			  ComplexUpdate,
			  ComplexDirectDataFlow: checkComplex
			JavaNativeRead,
			  JavaNativeUpdate,
			  JavaNativeDirectDataFlow: checkNative
			default: throw new SwitchException
		}]
	}
	
	def checkPrimitive(Edge it) {
		val srcType = sourceElement.primitiveType
		val tgtType = targetElement.primitiveType
		check[
			srcType == tgtType
		].elseError('''«srcType» != «tgtType»''')
		checkListState
	}
	
	def checkComplex(Edge it) {
		val srcType = sourceElement.complexType
		val tgtType = targetElement.complexType
		if(srcType !== null && tgtType !==null){
			check[
				srcType.isTypeOf(tgtType)
			].elseError('''«srcType.name» != «tgtType.name»''')
		}
		checkListState
	}
	
	def checkNative(Edge it) {
		val srcType = sourceElement.nativeType
		val tgtType = targetElement.nativeType
		check[
			srcType.name == tgtType.name
		].elseError('''«srcType» != «tgtType»''')
		checkListState
	}
	
	def private void checkListState(Edge it) {
		val source = sourceElement
		val target = targetElement
		if (target instanceof Variable || target instanceof Attribute) {
			check[
				target.isListType || !source.isListType
			].elseError(
				if (source.isListType) '''target should be list'''
				else '''target should not be list'''
			)
		}
		else check[
			sourceElement.isListType == targetElement.isListType
		].elseError(
			"source and target should be lists, or none of them"
		)
	}
}
									