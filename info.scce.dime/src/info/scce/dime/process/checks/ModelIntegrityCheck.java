package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractModelIntegrityCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;

public class ModelIntegrityCheck
	   extends AbstractModelIntegrityCheck<ProcessId, ProcessAdapter> {
}
