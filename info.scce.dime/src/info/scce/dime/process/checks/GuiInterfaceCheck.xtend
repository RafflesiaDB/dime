package info.scce.dime.process.checks

import graphmodel.Container
import graphmodel.ModelElementContainer
import info.scce.dime.checks.AbstractCheck
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gUIPlugin.AbstractParameter
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.Output
import info.scce.dime.gui.gui.AddToSubmission
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttribute
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DataContext
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.OutputPort
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveListAttribute
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.ComplexGUIBranchPort
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.GUIBranchPort
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.helper.GUIExtensionProvider
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.mcam.adapter.ProcessAdapter
import info.scce.dime.process.mcam.adapter.ProcessId
import info.scce.dime.process.process.BooleanInputStatic
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.Input
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import java.util.ArrayList
import java.util.Collections
import java.util.HashSet
import java.util.List
import java.util.Set
import java.util.stream.Collectors
import org.eclipse.emf.ecore.EObject
import java.util.LinkedList

class GuiInterfaceCheck extends AbstractCheck<ProcessId, ProcessAdapter> {
	
	extension DataExtension = DataExtension.instance
	extension ProcessExtension = new ProcessExtension
	extension GUIExtension guiex
	
	ProcessAdapter adapter
	GUISIB sib
	GUI refGui
	GUI cGui
	override void doExecute(ProcessAdapter adapter) {
		this.adapter=adapter 
		for (ProcessId id : adapter.getEntityIds()) {
			var Object obj=id.getElement() 
			if (obj instanceof GUISIB) {
				sib=obj as GUISIB 
				refGui=sib.getGui() 
				cGui = refGui 
				checkInputs(id, sib) 
				checkEvents(id,sib)
				checkBranches(id, sib) 
				checkBranchPorts() 
			}
		}
		processResults() 
	}
	
	override init() {
		guiex = GUIExtensionProvider.guiextension
	}
	
	def checkEvents(ProcessId id, GUISIB guisib) {
		val events = guisib.eventListenerSuccessors
		val listener = refGui.listenerContexts.map[n|n.events].flatten
		//check not present listeners
		events.filter[n|listener.filter[name.equals(n.name)].empty].forEach[n|addError(id, '''event '«n.getName()»' not found in GUI''') ]
		//check ports for present events
		val presentEvents = events.filter[n|!listener.filter[name.equals(n.name)].empty]
		for(pEvent:presentEvents)
		{
			//find corresponding listener
			val coListener = listener.findFirst[name.equals(pEvent.name)]
			
			//check missing ports
			coListener.outputPorts.filter[!pEvent.inputs.map[name].contains(name)].forEach[n|addError(id, '''missing port «n.name»'''.toString)]
			//check not existing ports
			pEvent.inputs.filter[!coListener.outputPorts.map[name].contains(name)].forEach[n|addError(id, '''unkown port «n.getName()»'''.toString)]
			//check port type
			for(portTupel:pEvent.inputs.groupBy[n|coListener.outputPorts.findFirst[name.equals(n.name)]].entrySet)
			{
				// GUI event parameter
				val guiListnerPort = portTupel.key
				val guiEventPort = portTupel.value.get(0)
				//wrong list status
				if(guiEventPort instanceof InputPort) {
					if(guiListnerPort.isList!=guiEventPort.isList){
						addError(id, '''wrong list status «guiEventPort.getName()»'''.toString)
					}	
				}
				if(guiListnerPort instanceof info.scce.dime.gui.gui.ComplexOutputPort){
					//wrong complex type
					if(guiEventPort instanceof ComplexInputPort) {
						val superTypesAndMe = #[(guiEventPort as ComplexInputPort).dataType] + (guiEventPort as ComplexInputPort).dataType.superTypes
						if(superTypesAndMe.map[name].findFirst[n|n.equals((guiListnerPort as info.scce.dime.gui.gui.ComplexOutputPort).dataType.name)]==null) {
							addError(id, '''incompatible complex types «guiEventPort.getName()»'''.toString)
						}
					}
					else{
						addError(id, '''«guiEventPort.getName()» has to be complex'''.toString)
					}
				}
				if(guiListnerPort instanceof info.scce.dime.gui.gui.PrimitiveOutputPort) {
					//wrong primitive type
					if(guiEventPort instanceof PrimitiveInputPort) {
						if((guiListnerPort as info.scce.dime.gui.gui.PrimitiveOutputPort).dataType.toData!=(guiEventPort as PrimitiveInputPort).dataType.toData) {
							addError(id, '''incompatible primitive types «guiEventPort.getName()»'''.toString)
						}
					}
					else if(guiEventPort instanceof InputStatic) {
						val pType = guiListnerPort.dataType.toData
						val isWrong = switch guiEventPort {
							TextInputStatic case pType==info.scce.dime.data.data.PrimitiveType.TEXT:false
							IntegerInputStatic case pType==info.scce.dime.data.data.PrimitiveType.INTEGER:false
							RealInputStatic case pType==info.scce.dime.data.data.PrimitiveType.REAL:false
							TimestampInputStatic case pType==info.scce.dime.data.data.PrimitiveType.TIMESTAMP:false
							BooleanInputStatic case pType==info.scce.dime.data.data.PrimitiveType.BOOLEAN:false
						}
						if(isWrong)addError(id,'''«guiEventPort.getName()» primitive type incompatible'''.toString)
					}
					else{
						addError(id, '''«guiEventPort.getName()» has to be primitive'''.toString)
					}
				}
				
			}
		}
		
	}
	

	def private void checkInputs(ProcessId sibId, GUISIB sib) {
		var List<EObject> inputs=new ArrayList(sib.getInputs()) 
		var List<EObject> inputOrigins=collectInputs() 
		// delete ports
		for (EObject input : inputs) {
			var EObject eObj=getRelatedPort(inputOrigins, input) 
			if (eObj === null && input instanceof Input) {
				var ProcessId inputId=adapter.getIdByString(((input as Input)).getId()) 
				addError(inputId, '''input '«»«((input as Input)).getName()»' not found in GUI''') 
			}
		}
		// update ports
		for (Input input : sib.getInputs()) {
			var EObject eObj=getRelatedPort(inputOrigins, input) 
			if (eObj != null){
				var ProcessId inputId=adapter.getIdByString(((input as Input)).getId()) 
				checkInput(inputId, input, eObj) 
			}
		}
		// add ports
		for (EObject eObj : inputOrigins) {
			var EObject inputEObj=getRelatedPort(inputs, eObj) 
			if (inputEObj === null) {
				var missingName = (eObj as Variable).name
				addError(sibId, "input '" + missingName + "' is missing") 
			}
		}
	}
	
	def private void checkInput(ProcessId inputId, Input input, EObject eObj) {
		// get Input information
		var boolean inputIsList=false 
		var String inputPType=null 
		var Type inputType=null 
		if (input instanceof InputStatic) {
			if (input instanceof BooleanInputStatic) {
				inputPType=PrimitiveType.BOOLEAN.getLiteral() 
			} else if (input instanceof IntegerInputStatic) {
				inputPType=PrimitiveType.INTEGER.getLiteral() 
			} else if (input instanceof RealInputStatic) {
				inputPType=PrimitiveType.REAL.getLiteral() 
			} else if (input instanceof TextInputStatic) {
				inputPType=PrimitiveType.TEXT.getLiteral() 
			} else if (input instanceof TimestampInputStatic) {
				inputPType=PrimitiveType.TIMESTAMP.getLiteral() 
			} else throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation");
		} else if (input instanceof PrimitiveInputPort) {
			inputPType=((input as PrimitiveInputPort)).getDataType().getLiteral() 
			inputIsList=((input as PrimitiveInputPort)).isIsList() 
		} else if (input instanceof ComplexInputPort) {
			inputType=((input as ComplexInputPort)).getDataType() 
			inputIsList=((input as ComplexInputPort)).isIsList() 
		} else throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation");
		// get origin information
		var boolean originIsList=false 
		var String originPType=null 
		var Type originType=null 
		if (eObj instanceof Variable) {
			if (eObj instanceof PrimitiveVariable) {
				originPType=((eObj as PrimitiveVariable)).getDataType().getLiteral() 
				originIsList=((eObj as PrimitiveVariable)).isIsList() 
			} else if (eObj instanceof ComplexVariable) {
				originType=((eObj as ComplexVariable)).getDataType() 
				originIsList=((eObj as ComplexVariable)).isIsList() 
			} else throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation");
		} else if (eObj instanceof Attribute) {
			if (eObj instanceof PrimitiveAttribute) {
				originPType=((eObj as PrimitiveAttribute)).getAttribute().getDataType().getLiteral() 
				originIsList=((eObj as PrimitiveVariable)).isIsList() 
			}
			if (eObj instanceof PrimitiveListAttribute) {
				originPType="integer" 
				originIsList=((eObj as PrimitiveVariable)).isIsList() 
			}
			if (eObj instanceof ComplexAttribute) {
				originType=((eObj as ComplexAttribute)).getAttribute().getDataType() 
				originIsList=((eObj as ComplexAttribute)).getAttribute().isIsList() 
			}
			if (eObj instanceof ComplexListAttribute) {
				originType=((eObj as ComplexListAttribute)).getListType() 
				originIsList=false 
			}
		} else if (eObj instanceof PrimitiveOutputPort) {
			originPType=((eObj as PrimitiveOutputPort)).getDataType().getLiteral() 
			originIsList=((eObj as PrimitiveOutputPort)).isIsList() 
		} else if (eObj instanceof ComplexOutputPort) {
			originType=((eObj as ComplexOutputPort)).getDataType() 
			originIsList=((eObj as ComplexOutputPort)).isIsList() 
		} else throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation");
		// check results
		if (inputIsList !== originIsList) addError(inputId, '''isList property mismatch. should be «originIsList»''') 
		if (inputPType !== originPType && originPType !== null) addError(inputId, '''primitive type mismatch. should be «originPType»''') 
		if (originType !== null && (inputType === null || (inputType.getId() !== originType.getId()))) addError(inputId, '''type mismatch. should be «originType.getName()»''') 
	}
	def private List<EObject> collectInputs() {
		var ArrayList<EObject> list=new ArrayList()
		for (DataContext dataContext : sib.getGui().getDataContexts()) {
			// collect inputs
			var List<Variable> inputs=dataContext.getVariables().stream().filter([^var | ^var.isIsInput()]).collect(Collectors.toList()) 
			for (Variable ^var : new ArrayList<Variable>(inputs)) {
				if (^var.getIncoming(ComplexAttributeConnector).size() > 0) inputs.remove(^var) 
				if (^var.getIncoming(ComplexListAttributeConnector).size() > 0) inputs.remove(^var) 
			}
			list.addAll(inputs) 
		}
		return list 
	}
	
	def private void checkBranches(ProcessId sibId, GUISIB sib) {
		var List<Branch> branches=sib.getSuccessors(Branch) 
		var Set<String> branchesOrigins=sib.gui.getGUIBranches(true).map[it.name].toSet
		// delete branches
		for (Branch branch : branches) {
			if(!branchesOrigins.contains(branch.name)){
				addError(adapter.getIdByString(branch.getId()), '''branch '«»«branch.getName()»' not found in GUI''') 
			}
		}
		// add branches
		for (String branchName : branchesOrigins) {
			if (!branches.exists[name == branchName] && !sib.isIgnoredBranch(branchName)) {
				addError(sibId, '''Branch '«branchName»' is missing''') 
			}
		}
	}
	def private void checkBranchPorts() {
		for (Branch branch : sib.getSuccessors(Branch)) {
			var ProcessId branchId=adapter.getIdByString(branch.getId()) 
			val branchRepresentant = sib.gui.GUIBranchesMerged.findFirst[it.name == branch.name]
			if (branchRepresentant!==null) {
				checkOutputs(branchId,branch,branchRepresentant) 
			}
		}
	}
	def private void checkOutputs(ProcessId branchId, Branch branch, GUIBranch branchRep) {
		val List<info.scce.dime.process.process.Output> outputs = new ArrayList(branch.getOutputs())
		val List<String> names = new LinkedList(branchRep.ports.map[name])

		for (info.scce.dime.process.process.Output output : outputs) {
			if (!names.contains(output.getName())) {
				// delete ports
				addError(adapter.getIdByString(output.getId()), '''output '«»«output.getName()»' not found in GUI''')
			} else {
				// check ports
				val portObj = branchRep.ports.findFirst[name.equals(output.name)]
				val ProcessId outputId = adapter.getIdByString(output.getId())
				checkOutput(outputId, output, portObj)
				names.remove(output.getName())

			}

		}
		// add ports
		for (String name : names) {
			addError(branchId, '''output '«»«name»' is missing''')

		}
	}
	
	def private Set<Button> findButtons(ModelElementContainer container) {
		if (container instanceof Button) {
			if (((container as Button)).getOptions() !== null) {
				if (((container as Button)).getOptions().getStaticURL() !== null) {
					if (!((container as Button)).getOptions().getStaticURL().isEmpty()) {
						var Set<Button> empty=Collections.emptySet() 
						return empty 
					}
				}
			}
			//			if (ElementCollector.isEmbeddedInTab((Container) container)) {
			//				Set<Button> empty = Collections.emptySet();
			//				return empty;
			//			}
			//			if (ElementCollector.isActionButton((Button) container)) {
			//				Set<Button> empty = Collections.emptySet();
			//				return empty;
			//			}
			if (((container as Button)).isDisabled()) return Collections.emptySet() 
			return Collections.singleton((container as Button)) 
		} else {
			var Set<Button> contained=new HashSet<Button>() 
			for (Container c : container.getAllContainers()) {
				contained.addAll(findButtons(c)) 
			}
			return contained 
		}
	}
	def private Set<Output> findGUIPlugins(ModelElementContainer container) {
		if (container instanceof GUIPlugin) {
			var GUIPlugin gp=(container as GUIPlugin) 
			var Function fun=(gp.getFunction() as Function) 
			return fun.getOutputs().stream().collect(Collectors.toSet()) 
		} else {
			var Set<Output> contained=new HashSet<Output>() 
			for (Container c : container.getAllContainers()) {
				contained.addAll(findGUIPlugins(c)) 
			}
			return contained 
		}
	}
	
	
	def private void checkOutput(ProcessId outputId, info.scce.dime.process.process.Output output, GUIBranchPort guiBranchPort) {
		// get Input information
		val eObj = guiBranchPort.portNode
		//check list type
		if(GUIBranchPort.isPrimitive(eObj)) {
			//check port is primitive
			if(output instanceof PrimitiveOutputPort) {
				//check list
				if(guiBranchPort.list!=output.isIsList) {
					addError(outputId, '''is List not matching. should be «guiBranchPort.list»''') 
				}
				//check type
				if(GUIBranchPort.getPrimitiveType(eObj)!=GUIBranchPort.toData(output.dataType)) {
					addError(outputId, '''primitive type not matching. should be «GUIBranchPort.getPrimitiveType(eObj).toString»''') 
				}
			} else {
				addError(outputId, '''should be primitive''') 
			}
		}
		else {
			val complexGUIBranchPort = guiBranchPort as ComplexGUIBranchPort
			//check port is complex
			if(output instanceof ComplexOutputPort) {
				//check list
				if(guiBranchPort.list!=output.isIsList) {
					addError(outputId, '''is List not matching. should be «guiBranchPort.list»''') 
				}
				//check type
				if(!complexGUIBranchPort.type.originalType.equals(output.dataType.originalType)) {
					addError(outputId, '''complex type not matching. should be «complexGUIBranchPort.type.originalType.name»''') 
				}
			} else {
				addError(outputId, '''should be complex''') 
			}
		}
		
	}
	
	def private EObject getRelatedPort(Iterable<EObject> col, Object item) {
		var String itemName=null 
		if (item instanceof OutputPort) itemName=((item as OutputPort)).getName() 
		if (item instanceof Input) itemName=((item as Input)).getName() 
		if (item instanceof Variable) itemName=((item as Variable)).getName()  else if (item instanceof Attribute) {
			if (item instanceof PrimitiveAttribute) {
				itemName=((item as PrimitiveAttribute)).getAttribute().getName() 
			}
			if (item instanceof ComplexAttribute) {
				itemName=((item as ComplexAttribute)).getAttribute().getName() 
			}
			if (item instanceof ComplexListAttribute) {
				itemName=((item as ComplexListAttribute)).getAttributeName().getLiteral() 
			}
			if (item instanceof PrimitiveListAttribute) {
				itemName=((item as PrimitiveListAttribute)).getAttributeName().getLiteral() 
			}
		}
		if (item instanceof Output) itemName=((item as Output)).getOutputName() 
		if (item instanceof AbstractParameter) itemName=((item as AbstractParameter)).getName() 
		if (item instanceof String) itemName=item as String 
		if (itemName === null) throw new IllegalStateException('''unknown port type of item: «item»''');
		for (EObject eObj : col) {
			var String colItemName=null 
			if (eObj instanceof OutputPort) colItemName=((eObj as OutputPort)).getName() 
			if (eObj instanceof Input) colItemName=((eObj as Input)).getName() 
			if (eObj instanceof Variable) colItemName=((eObj as Variable)).getName() 
			if (eObj instanceof Attribute) {
				if (eObj instanceof PrimitiveAttribute) {
					colItemName=((eObj as PrimitiveAttribute)).getAttribute().getName() 
				}
				if (eObj instanceof ComplexAttribute) {
					colItemName=((eObj as ComplexAttribute)).getAttribute().getName() 
				}
				if (eObj instanceof ComplexListAttribute) {
					colItemName=((eObj as ComplexListAttribute)).getAttributeName().getLiteral() 
				}
				if (eObj instanceof PrimitiveListAttribute) {
					colItemName=((eObj as PrimitiveListAttribute)).getAttributeName().getLiteral() 
				}
			}
			if (eObj instanceof AddToSubmission) colItemName=((eObj as AddToSubmission)).outputName
			if (eObj instanceof AbstractParameter) colItemName=((eObj as AbstractParameter)).getName() 
			if (eObj instanceof Output) colItemName=((eObj as Output)).getOutputName() 
			if (colItemName === null) throw new IllegalStateException('''unknown port type of colItem: «eObj»''');
			if (itemName.equals(colItemName)) return eObj 
		}
		return null 
	}
}