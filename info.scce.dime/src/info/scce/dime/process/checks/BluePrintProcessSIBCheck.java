package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.ProcessBlueprintSIB;

public class BluePrintProcessSIBCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof ProcessBlueprintSIB) {
				ProcessBlueprintSIB sib = (ProcessBlueprintSIB) obj;
				checkDefault(id, sib);
			}
		}
	}
	
	@Override
	public void init() {}

	private void checkDefault(ProcessId id, ProcessBlueprintSIB sib) {
		boolean hasBranches = !sib.getBranchBlueprintSuccessors().isEmpty();
		if (hasBranches && sib.getDefaultBranch() == null)
			addError(id, "default branch, should be set of " + sib.getLabel()
					+ " SIB");
	}
}
