package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.DataFlow;
import info.scce.dime.process.process.DataFlowTarget;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.Process;

public class InputNotSetCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof DataFlowTarget) {
				if (!(((DataFlowTarget) obj).getContainer() instanceof Process))
					continue;
				for (Input input : ((DataFlowTarget) obj).getInputs()) {
					if (input instanceof InputStatic == false && input.getIncoming(DataFlow.class).size() <= 0)
						addWarning(adapter.getIdByString(input.getId()), "not set");
				}
					
			}
		}
	}
	
	@Override
	public void init() {}

}
