package info.scce.dime.process.checks

import de.jabc.cinco.meta.runtime.xapi.CodingExtension
import graphmodel.Container
import graphmodel.IdentifiableElement
import graphmodel.Node
import info.scce.dime.process.mcam.modules.checks.ProcessCheck
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessPlaceholderSIB
import info.scce.dime.process.process.ProcessSIB
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.Variable

import static extension org.jooq.lambda.Seq.*

/**
 * The extension check is used to validate the signature
 * of the current process model, if it extends another process model.
 */
class ExtensionCheck extends ProcessCheck {
	
	extension CodingExtension = new CodingExtension
	
	override check(Process it) {
		
		// check whether a process is conform to the processes it extends
//		extensionContexts.head?.with [
//			if(rootElement.processType == ProcessType.NATIVE_FRONTEND_SIB_LIBRARY)
//				addError('''Frontend processes do not support extension contexts.''')
//		]?.processSIBs?.head?.with [
//			if(!proMod.extensionContexts.empty)
//				addError('''The extension process itself is an extension.''')
//			checkSignature
//			checkNoConnections
//			checkNoSelfExtension
//			checkContainer
//		]
		
		processSIBs.seq.peek [ p |
			p.checkContainer
			// check whether the process type of the process static input is an extension 
			// of the process represented by the process placeholder sib in the sub process 'p'
			p.processInputStatics
				.associateWithKey [ it ]
				.mapValue [ v | 
					p.proMod.processPlaceholderSIBs.findFirst [ label == v.name ].proMod
				]
				.filter [
					if(v1.value == v2) false
				].map [ v1 ]
				.forEach [
					addError('''The chosen process is not an extension of the placeholder process.''')
				]
		]
		.filter(ProcessPlaceholderSIB).seq
		.peek [
			if(rootElement.processType != ProcessType.BASIC ) 
				addError('''Only basic processes support process placeholders.''')
		]
		.peek [
			if(inputs.size > 16) addError('''Placeholder SIBs may have at most 16 inputs. Found: «inputs.size»''')
		]
		// all with same label
		.grouped [ label ].map [ v2 ]
		// look at labels that are used on multiple placeholders, only
		.filter [size > 1 ]
		// look at placeholders with different base processes, only
		.filter [ map [ proMod ].toSet.size > 1 ].flatten.forEach [
			// check failed, that placeholders with same label refer to the same base process, as they are conflated in one `ProcessInputStatic`.
			addError('''Different base process for same placeholder label «label».''')
		]
	}
	
	private def void checkNoConnections(Node it) {
		if (!noEdges) addError('''No edges allowed on extension''')
		if (it instanceof Container) modelElements.filter(Node).forEach[ checkNoConnections ]
		if (it instanceof SIB) abstractBranchSuccessors.forEach[ checkNoConnections ] 
	}
	
	private def checkNoSelfExtension(ProcessSIB it) {
		// TODO JN: Perhaps check "no circle extension" instead?
		if (proMod == rootElement) addError('''No self extension allowed''')
	}
	
	private def boolean getNoEdges(Node it) { 
		(incoming.empty || incoming.forall[ it instanceof BranchConnector ]) && 
		(outgoing.empty || outgoing.forall[ it instanceof BranchConnector ])
	}
	
	private def checkSignature(ProcessSIB parentProcessSIB) {
		val processModel = parentProcessSIB.rootElement
		val parentProcess = parentProcessSIB.proMod
		// Collect required process inputs (since they are formal parameters, they are outputs of the start sib). 
		val requiredInputs = parentProcess.startSIBs.head?.outputPorts.associateWithKey[name].toMap
		val presentInputs = processModel.startSIBs.head?.outputPorts
		
		// Compare inputs with same name and check whether the present input 
		// is of same type as the required inputs type.
		// Inputs that are defined on the interface, but not on the implementation are ignored. 
		// It is ok not to define some inputs in the implementation, but when they are implemented, they 
		// have to be the same.
		// It is not ok to define inputs on the implementation that are not defined in the interface, 
		// because the placeholder sib cannot provide them (would always be null).
		presentInputs.associateWithKey [
			requiredInputs.get(name)
		]
		.filter[
			if(v1 == null) {
				v2.addError('''Input "«v2.name»" is not present in the implemented interface.''')
				false
			}
			else true
		]
		.filter [
			parentProcessSIB.checkListStateEq(v1, v2)
		]
		.filter [
			parentProcessSIB.checkType(v1, v2)
		].toList
		
		// check branches...
		val requiredOutputs = parentProcess.endSIBs.associateWithKey [ branchName ].mapValue[ inputPorts.associateWithKey [ name ].toMap ].toMap
		
		// Each branch defined in the implementation has to be implemented in the interface,
		// so that the corresponding control-flow can be handled by a corresponding placeholder 
		// sib's branches. Branches defined in the interface, but not in the implementation are 
		// ignored, since they just do never happen, which does not harm the placeholder sib. 
		// The same holds for the outputs.
		processModel.endSIBs.associateWithKey [
			if(requiredOutputs.containsKey(branchName)) branchName else null
		]
		.filter[
			if(v1 == null) {
				v2.addError('''Branch "«v2.branchName»" is not present in the implemented interface.''')
				false
			}
			else true
		]
		.map [ v2.inputPorts ].flatten.associateWithKey[
			requiredOutputs.get((container as EndSIB).branchName)?.get(name)
		]
		.filter [
			if(v1 == null) {
				v2.addError('''Output "«v2.name»" is not present in the implemented interface.''')
				false
			}
			else true
		]
		.filter [
			parentProcessSIB.checkListStateEq(v1, v2)
		]
		.filter [
			parentProcessSIB.checkType(v1, v2)
		].toList
	}
	
	private def checkContainer(ProcessSIB parentProcessSIB) {
		parentProcessSIB.abstractBranchSuccessors.filter[it.container != parentProcessSIB.container].forEach [
			addError('''Container of branch «name» is different from its SIBs container''')
		]
	}
	
	private dispatch def checkType(IdentifiableElement it, Variable v1, Variable v2) {
		false
	}
	
	private dispatch def checkType(IdentifiableElement it, ComplexOutputPort v1, ComplexOutputPort v2) {
		if (v2.dataType == v1.dataType) true
		else { 
			addError('''The type of overridden input "«v1.name» is not a sub type of the overriden input's type."''')
			false
		}
	}
	
	private dispatch def checkType(IdentifiableElement it, ComplexOutputPort v1, OutputPort v2) {
		addError('''Overridden input "«v1.name»" represents a complex type, but overriding input not.''')
		false
	}
	
	private dispatch def checkType(IdentifiableElement it, OutputPort v1, ComplexOutputPort v2) {
		addError('''Overriding input "«v2.name»" represents a complex type, but overridden input not.''')
		false
	}
	
	private dispatch def checkType(IdentifiableElement it, PrimitiveOutputPort v1, PrimitiveOutputPort v2) {
		if(v1.dataType != v2.dataType) {
			addError('''Overridden input "«v1.name»" has not same primitive type as overriding input.''')
		}
		false
	}
	
	private dispatch def checkType(IdentifiableElement it, PrimitiveOutputPort v1, OutputPort v2) {
		addError('''Overridden input "«v1.name»" represents a primitive type, but overriding input not.''')
		false
	}
	
	private dispatch def checkType(IdentifiableElement it, OutputPort v1, PrimitiveOutputPort v2) {
		addError('''Overriding input "«v2.name»" represents a primitive type, but overridden input not.''')
		false
	}
	
	
	private dispatch def checkListStateEq(IdentifiableElement it, InputPort v1, InputPort v2) {
		if (v1.isList != v2.isList) { 
			addError('''List type status for overriding input "«v2.name»" and its overridden input differ.''')
			return false
		}
		return true
	}
	
	private dispatch def checkListStateEq(IdentifiableElement it, OutputPort v1, OutputPort v2) {
		if (v1.isList != v2.isList) { 
			addError('''List type status for overriding input "«v2.name»" and its overridden input differ.''')
			return false
		}
		return true
	}
}