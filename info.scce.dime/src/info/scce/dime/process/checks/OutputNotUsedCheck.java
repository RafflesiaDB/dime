package info.scce.dime.process.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.process.mcam.adapter.ProcessAdapter;
import info.scce.dime.process.mcam.adapter.ProcessId;
import info.scce.dime.process.process.DataFlow;
import info.scce.dime.process.process.DataFlowSource;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.Process;

public class OutputNotUsedCheck extends AbstractCheck<ProcessId, ProcessAdapter> {

	@Override
	public void doExecute(ProcessAdapter adapter) {
		for (ProcessId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof DataFlowSource) {
				if (!(((DataFlowSource) obj).getContainer() instanceof Process))
					continue;
				for (Output output : ((DataFlowSource) obj).getOutputs()) {
					if (output.getOutgoing(DataFlow.class).size() <= 0)
						addWarning(adapter.getIdByString(output.getId()), "not used");
				}
					
			}
		}
	}
	
	@Override
	public void init() {}
}
