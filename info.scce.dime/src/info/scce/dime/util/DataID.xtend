package info.scce.dime.util

import graphmodel.IdentifiableElement
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtend.lib.annotations.Accessors

/**
 * Represents the ID of a data object to be used in the context
 * of the native domain generator. The representation there is
 * max 16 Bytes long and does not contain any special character
 * other than the underscore '_'
 */
class DataID {
	
	@Accessors val String cincoId
	var String _escapedLowerCase
	var String _escapedLowerCase16
	
	static def from(IdentifiableElement element) {
		new DataID(element.id)
	}
	
	static def from(String cincoId) {
		new DataID(cincoId)
	}
	
	new(String cincoId) {
		this.cincoId = cincoId
	}
	
	/**
	 * Replaces any character other than A-Z or a-z or digits 0-9 with
	 * an underscore '_' and converts the ID to lower case.
	 */
	private def String escapedLowerCase() {
		_escapedLowerCase ?: {
			_escapedLowerCase = cincoId.replaceAll("[^\\dA-Za-z0-9]", "_").toLowerCase
		}
	}
	
	/**
	 * Escapes the given String and cuts its length if it exceeds 16 chars.
	 * The escaped String contains only ASCII characters. Hence String.length() is
	 * equal to String.getBytes().length() and we are safe to cut at 16 chars to
	 * achieve 16 Bytes.
	 */
	def String escapedLowerCase16() {
		_escapedLowerCase16 ?: {
			val str = escapedLowerCase()
			_escapedLowerCase16 = str.substring(0, Math.min(str.length, 16))
		}
	}
	
	/**
	 * Compares this ID to the given one after converting both via escapedLowerCase16()
	 */
	def boolean equalsEscapedLowerCase16(DataID other) {
		this.escapedLowerCase16() == other?.escapedLowerCase16()
	}
	
	/**
	 * Compares this ID to the given one after converting both via escapedLowerCase16()
	 */
	def boolean equalsEscapedLowerCase16(String otherId) {
		equalsEscapedLowerCase16(DataID.from(otherId))
	}
	
	/**
	 * Checks whether the ID of the given element matches any of the IDs of the given
	 * elements after converting them via escapedLowerCase16(). If so, this method tries
	 * to generate a new ID for the given element by EcoreUtil.generateUUID() that does
	 * not match any of the IDs of the given elements after converting them via
	 * escapedLowerCase16().
	 */
	static def assertUniqueDataID(IdentifiableElement element, Iterable<? extends IdentifiableElement> elements) {
		var i = 0
		// try a thousand times to avoid running forever
		while (i++ <= 1000 && elements.exists[
			DataID.from(it).equalsEscapedLowerCase16(element.id)
		]) {
			EcoreUtil.setID(element, EcoreUtil.generateUUID)
		}
		if (i >= 1000) System.err.println(
			'''ERROR Failed to set unique DataID of «element.eClass.name» «element.id»'''
		)
	}
}