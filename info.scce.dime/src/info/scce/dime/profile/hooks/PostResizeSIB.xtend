package info.scce.dime.profile.hooks

import info.scce.dime.api.DIMEPostResizeHook
import info.scce.dime.profile.layout.Layouter
import info.scce.dime.profile.profile.SIB

class PostResizeSIB extends DIMEPostResizeHook<SIB> {
	
	extension Layouter = Layouter.instance
	
	override postResize(SIB sib, int direction, int width, int height) {
		sib.layout
	}
	
}