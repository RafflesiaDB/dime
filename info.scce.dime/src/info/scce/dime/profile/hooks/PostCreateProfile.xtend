package info.scce.dime.profile.hooks

import info.scce.dime.profile.profile.Profile
import info.scce.dime.api.DIMEPostCreateHook

class PostCreateProfile extends DIMEPostCreateHook<Profile> {
	
	override postCreate(Profile model) {
		val uri = model.eResource.URI
		val fileName = uri.lastSegment
		model.name = fileName.replace("." + uri.fileExtension, "")
	}
	
}