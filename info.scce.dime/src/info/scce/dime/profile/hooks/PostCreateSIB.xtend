package info.scce.dime.profile.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.profile.build.ProcessSIBBuild
import info.scce.dime.profile.profile.SIB
import info.scce.dime.profile.util.PrimeNameCacher
import info.scce.dime.profile.profile.ProcessSIB
import info.scce.dime.profile.profile.ProcessBlueprintSIB
import info.scce.dime.profile.build.ProcessBlueprintSIBBuild
import info.scce.dime.profile.profile.GUIBlueprintSIB
import info.scce.dime.profile.build.GUIBlueprintSIBBuild
import info.scce.dime.profile.profile.GUISIB
import info.scce.dime.profile.build.GUISIBBuild

class PostCreateSIB extends DIMEPostCreateHook<SIB> {
	
	extension PrimeNameCacher = new PrimeNameCacher
	
	override postCreate(SIB sib) {
		switch sib {
			GUISIB: new GUISIBBuild(sib)
			ProcessSIB: new ProcessSIBBuild(sib)
			GUIBlueprintSIB: new GUIBlueprintSIBBuild(sib)
			ProcessBlueprintSIB: new ProcessBlueprintSIBBuild(sib)
		}?.initialize
		
		sib.cacheNames
	}
	
}