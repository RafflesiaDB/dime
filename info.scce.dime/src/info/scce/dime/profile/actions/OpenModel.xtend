package info.scce.dime.profile.actions

import graphmodel.Node
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.IO
import info.scce.dime.profile.profile.SIB

class OpenModel extends DIMECustomAction<Node> {
	
	extension ProfileExtension = new ProfileExtension
	
	override execute(Node it) {
		println("Opening submodel for " + eClass.name)
		switch it {
			SIB: referencedObject
			Branch: referencedObject
			IO: referencedObject
		}?.openEditor
	}

	override hasDoneChanges() {
		false
	}
	
}