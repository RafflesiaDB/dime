package info.scce.dime.profile.actions

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.dime.profile.build.GUISIBBuild
import info.scce.dime.profile.build.ProcessBlueprintSIBBuild
import info.scce.dime.profile.build.ProcessSIBBuild
import info.scce.dime.profile.profile.GUISIB
import info.scce.dime.profile.profile.ProcessBlueprintSIB
import info.scce.dime.profile.profile.ProcessSIB
import info.scce.dime.profile.profile.SIB

class UpdateSIB extends CincoCustomAction<SIB> {
	
	override getName() {
		"Update SIB"
	}
	
	override execute(SIB it) {
		switch it {
			GUISIB : GUISIBBuild.update(it)
			ProcessSIB : ProcessSIBBuild.update(it)
			ProcessBlueprintSIB : ProcessBlueprintSIBBuild.update(it)
		}
	}
	
}