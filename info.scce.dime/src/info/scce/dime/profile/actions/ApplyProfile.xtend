package info.scce.dime.profile.actions

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.Profile
import info.scce.dime.profile.util.ReplacementStrategy
import org.eclipse.emf.common.util.URI

class ApplyProfile  extends CincoCustomAction<Profile> {
	
	extension ProfileExtension = new ProfileExtension
	extension ReplacementStrategy = new ReplacementStrategy
	
	val replacementAffectedURIs = <URI> newHashSet
	
	override getName() {
		"Apply Profile"
	}
	
	override execute(Profile profile) {
		profile.blueprintSIBs
			.partition[isReplacable]
			.matching[
				forEach[replace]
			]
			.nonMatching[
				forEach[println(
					"Skipping replacement of Blueprint SIB " + it?.name
					+ " in Profile " + profile.name
					+ " (replacement conditions not fulfilled)"
				)]
			]
	}
	
	def isReplacable(BlueprintSIB it) {
		replacementConditionsFulfilled
	}
	
	def replace(BlueprintSIB blueprintSIB) {
		println("[INFO] Replacing " + blueprintSIB.cachedReferencedBlueprintSIBLabel)
		val refSib = blueprintSIB.getReferencedObject
		val uri = refSib?.eResource?.URI
		if (uri !== null) {
			replacementAffectedURIs.add(uri)
		}
		new Replace().performReplacement(blueprintSIB)
	}
	
	def cleanupRegistry() {
		replacementAffectedURIs.forEach[ uri |
			println("[INFO] Cleanup replacements of " + uri)
			val ids = ReferenceRegistry.getInstance().unregister(uri)
			ids?.forEach[
				ReferenceRegistry.getInstance().unregister(it)
			]
			ReferenceRegistry.getInstance().enqueue(uri)
		]
	}
}