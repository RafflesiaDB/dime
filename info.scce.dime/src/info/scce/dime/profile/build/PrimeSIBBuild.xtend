package info.scce.dime.profile.build

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.Container
import graphmodel.GraphModel
import graphmodel.Node
import info.scce.dime.process.helper.PortUtils.Sync
import info.scce.dime.process.mcam.cli.ProcessExecution
import info.scce.dime.profile.profile.SIB
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.jface.dialogs.MessageDialog

abstract class PrimeSIBBuild<
			SIBType extends SIB,
			SIBPortType extends Node,
			SIBPortRef,
			BranchType extends Node,
			BranchRef,
			BranchPortType extends Node,
			BranchPortRef
		> extends CincoRuntimeBaseClass {
	
	protected SIBType sib
	
	new(SIBType sib) {
		this.sib = sib
	}
	
	def EObject getSIBReference(SIBType sib)
	
	def Iterable<SIBPortType> getSIBPorts(SIBType sib)
	
	def Iterable<SIBPortRef> getSIBPortReferences(SIBType sib)
	
	def boolean isSIBPortReference(SIBPortType port, SIBPortRef portRef)
	
	def SIBPortType addSIBPort(SIBPortRef ref)
	
	def Iterable<BranchType> getBranches(SIBType sib)
	
	def Iterable<BranchRef> getBranchReferences(SIBType sib)
	
	def boolean isBranchReference(BranchType branch, BranchRef branchRef)
	
	def BranchType addBranch(BranchRef ref)
	
	def Iterable<BranchPortType> getBranchPorts(BranchType sib)
	
	def Iterable<BranchPortRef> getBranchPortReferences(BranchRef branchRef)
	
	def boolean isBranchPortReference(BranchPortType branchPort, BranchPortRef portRef)
	
	def BranchPortType addBranchPort(BranchType branch, BranchPortRef ref)
	
	def void layout(SIBType sib) {
		// do nothing
	}
	
	def void layout(BranchType branch) {
		// do nothing
	}
	
	def initialize() {
		if (initializeInternal) {
			initializeSIBInternal
			initializeBranchesInternal
			sib.layout
			return true	
		}
		false
	}
	
	def initializeSIB() {
		if (initializeInternal) {
			initializeSIBInternal
			sib.layout
			return true	
		}
		false
	}
	
	private def initializeInternal() {
		val error = isValid
		if (error !== null) {
			val ok = letUserConfirm("Initialization failed.", (error ?: "") +  "Continue anyway?")
			if (!ok) {
				sib.delete
				return false
			}
		}
		return true
	}
	
	def String isValid() {
		if (sib.SIBReference === null) {
			return "Referenced element is null."
		}
	}
	
	def update() {
		val error = isValid
		if (error !== null) {
			val ok = letUserConfirm("Initialization failed.", (error ?: "") +  "Continue anyway?")
			if (!ok) return
		}
		updateSIB
		updateBranches
		sib.layout
	}
	
	def validate() {
		val errors = <String> newArrayList
		val error = isValid
		if (error !== null) {
			errors.add(error)
		} else {
			errors.addAll(validateSIBPorts)
			errors.addAll(validateBranches)
		}
		return errors
	}
	
	private def initializeSIBInternal() {
		val sync = new Sync<SIBPortType,SIBPortRef> => [
			add    = [ ref | addSIBPort(ref) ]
		]
		sync.apply(getSIBPorts(sib), getSIBPortReferences(sib))
	}
	
	def updateSIB() {
		val sync = new Sync<SIBPortType,SIBPortRef> => [
			equals = [ input,ref | input.isSIBPortReference(ref) ]
			add    = [       ref | addSIBPort(ref) ]
			update = [ input,ref | input.updateInPort(ref) ]
			delete = [ input     | input.delete ]
		]
		sync.apply(getSIBPorts(sib), getSIBPortReferences(sib))
	}
	
	def validateSIBPorts() {
		val errors = <String> newArrayList
		val sync = new Sync<SIBPortType,SIBPortRef> => [
			equals = [ input,ref | input.isSIBPortReference(ref) ]
			add    = [       ref | errors.add('''SIB port missing for referenced element «ref»''') ]
			delete = [ input     | errors.add('''SIB port «input» is obsolete''') ]
		]
		sync.apply(getSIBPorts(sib), getSIBPortReferences(sib))
		return errors
	}
	
	def getSIBPortSync() {
		new Sync<SIBPortType,SIBPortRef> => [
			equals = [ input,ref | input.isSIBPortReference(ref) ]
		]
	}
	
	def void updateInPort(SIBPortType sibPort, SIBPortRef ref) {
		val cInput = sibPort
		val cInputNew = addSIBPort(ref)
		for (cEdge : cInput.incoming.toNewList) {
			cEdge.reconnectTarget(cInputNew)
		}
		cInput.delete
		EcoreUtil.setID(cInputNew, sibPort.id)
	}
	
	def void postProcessNewSIBPort(SIBPortType port, Object ref) {
		/* default: do nothing */
	}
	
	package def initializeBranchesInternal() {
		val sync = new Sync<BranchType,BranchRef> => [
			add    = [        ref | addAndUpdateBranch(ref) ]
		]
		sync.apply(sib.branches, getBranchReferences(sib))
	}
	
	def updateBranches() {
		val sync = new Sync<BranchType,BranchRef> => [
			equals = [ branch,ref | branch.isBranchReference(ref) ]
			add    = [        ref | addAndUpdateBranch(ref) ]
			update = [ branch,ref | branch.updateBranch(ref) ]
			delete = [ branch     | branch.delete ]
		]
		sync.apply(getBranches(sib), getBranchReferences(sib))
	}
	
	def getBranchSync() {
		new Sync<BranchType,BranchRef> => [
			equals = [ branch,ref | branch.isBranchReference(ref) ]
		]
	}
	
	def validateBranches() {
		val errors = <String> newArrayList
		val sync = new Sync<BranchType,BranchRef> => [
			equals = [ branch,ref | branch.isBranchReference(ref) ]
			add    = [        ref | errors.add('''Branch missing for referenced element «ref»''') ]
			update = [ branch,ref | errors.addAll(branch.validateBranchPorts(ref)) ]
			delete = [ branch     | errors.add('''Branch «branch» is obsolete''') ]
		]
		sync.apply(getBranches(sib), getBranchReferences(sib))
		return errors
	}
	
	def addAndUpdateBranch(BranchRef ref) {
		val newBranch = addBranch(ref)
		println("New branch: " + newBranch)
		newBranch.updateBranch(ref)
	}
	
	def updateBranch(BranchType branch, BranchRef branchRef) {
		val sync = new Sync<BranchPortType,BranchPortRef> => [
			equals = [ port,ref | port.isBranchPortReference(ref) ]
			add    = [      ref | branch.addBranchPort(ref) ]
			update = [ port,ref | port.update(ref, branch) ]
			delete = [ port     | port.delete ]
		]
		sync.apply(getBranchPorts(branch), getBranchPortReferences(branchRef))
		if (branch instanceof Container) {
			branch.layout
		}
	}
	
	def validateBranchPorts(BranchType branch, BranchRef branchRef) {
		val errors = <String> newArrayList
		val sync = new Sync<BranchPortType,BranchPortRef> => [
			equals = [ port,ref | port.isBranchPortReference(ref) ]
			add    = [      ref | errors.add('''Branch port missing for referenced element «ref»''') ]
			delete = [ port     | errors.add('''Branch port «port» is obsolete''') ]
		]
		sync.apply(getBranchPorts(branch), getBranchPortReferences(branchRef))
		return errors
	}
	
	def getBranchPortSync() {
		new Sync<BranchPortType,BranchPortRef> => [
			equals = [ port,ref | port.isBranchPortReference(ref) ]
		]
	}
	
	def update(BranchPortType branchPort, BranchPortRef portRef, BranchType branch) {
		// do nothing
	}
	
	def boolean hasErrors(GraphModel model) {
		val file = model.eResource.file.rawLocation.toFile
		val fe = new ProcessExecution
		val adapter = fe.initApiAdapter(file)
		val check = fe.executeCheckPhase(adapter)
		if (check.hasErrors || check.hasWarnings)
			println(check)
		return check.hasErrors
	}
	
	def void letUserKnow(String title, String msg) {
		MessageDialog.openInformation(null, title, msg);
	}
	
	def boolean letUserConfirm(String title, String msg) {
		return MessageDialog.openConfirm(null, title, msg);
	}
	
	def <X> toNewList(Iterable<X> list) {
		val newList = newArrayList
		if (list !== null)
			newList.addAll(list)
		return newList
	}
	
	def showError(String message) {
		showErrorDialog("Operation canceled", message);
	}
}
