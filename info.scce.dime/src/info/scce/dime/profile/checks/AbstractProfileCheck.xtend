package info.scce.dime.profile.checks

import graphmodel.Node
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.mcam.modules.checks.ProfileCheck
import info.scce.dime.profile.profile.Profile
import info.scce.dime.profile.profile.SIB

abstract class AbstractProfileCheck extends ProfileCheck {
	
	protected extension ProfileExtension = new ProfileExtension
	
	override check(Profile model) {
		model.find(SIB).forEach[
			if (checkIntegrity) checkReplacement
		]
	}
	
	dispatch def boolean checkIntegrity(Node node) {
		true // add dispatch methods in sub-classes
	}
	
	dispatch def boolean checkReplacement(Node node) {
		true // add dispatch methods in sub-classes
	}
}