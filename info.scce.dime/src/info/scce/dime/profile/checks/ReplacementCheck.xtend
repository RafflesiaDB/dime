package info.scce.dime.profile.checks

import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.profile.OutputPort
import info.scce.dime.profile.util.ReplacementStrategy
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.ReplacementSIB
import info.scce.dime.profile.profile.DoNotReplace
import info.scce.dime.profile.profile.SIB
import info.scce.dime.profile.profile.ReplacementCondition
import graphmodel.Node

class ReplacementCheck extends AbstractProfileCheck {
	
	extension ReplacementStrategy = new ReplacementStrategy
	
	dispatch def boolean checkIntegrity(ReplacementSIB sib) {
		// referencedObject == null is perfectly fine for Replacement SIBs
		if (sib.referencedObject !== null) {
			val errors = sib.compound?.validate
			if (!errors.nullOrEmpty) {
				sib.addError('''SIB is out of sync with its referenced element, please apply an update''')
				return false
			}
		}
		sib.find(InputPort, Branch).filter(Node).map[checkIntegrity].forall[it === true]
		return true
	}
	
	dispatch def boolean checkIntegrity(BlueprintSIB sib) {
		if (sib.hasInvalidPrimeReference
			&& sib.findSuccessorsVia(ReplacementCondition).filter(SIB).exists[isInferredReplacement]) {
				
			sib.addError(
				'''Blueprint SIB '«sib.cachedReferencedBlueprintSIBLabel»' does not exist in model '«sib.cachedReferencedBlueprintSIBsModelName»' '''
			)
			return false
		}
		return sib.find(InputPort, Branch).filter(Node).map[checkIntegrity].forall[it === true]
	}
	
	dispatch def boolean checkReplacement(BlueprintSIB sib) {
		if (sib.replacement === null) {
			sib.addError(
				'''No replacement defined for SIB '«sib.name»' '''
			)
			return false
		}
		
		for (replSib : sib.replacementPath) {
			val cycle = replSib.findCycleInReplacementConditions
			if (!cycle.nullOrEmpty) {
				replSib.addError(
					'''Cycle in replacement conditions: «cycle.filter(SIB).map[it.name].join(" -> ")»'''
				)
				return false
			}
		}
		
		val inferredReplacement = sib.getInferredReplacement
		if (inferredReplacement === null) {
			sib.addError(
				'''No replacement available for BlueprintSIB '«sib.name»' '''
			)
			return false
		}
		
		sib => [
			if (!replacementConditionsFulfilled) {
				addInfo(
					'''Blueprint SIB '«sib.name»' is not going to be replaced (conditions not fulfilled). '''
				)
			}
			else if (inferredReplacement instanceof ReplacementSIB) {
				addInfo(
					'''Inferred replacement for BlueprintSIB '«sib.name»' is «inferredReplacement.eClass.name» '«inferredReplacement.name»' '''
				)
			}
			else if (inferredReplacement instanceof DoNotReplace) {
				addInfo(
					'''Blueprint SIB '«sib.name»' is not going to be replaced. '''
				)
			}
		]
		return sib.find(InputPort, Branch).filter(Node).map[checkReplacement].forall[it === true]
	}
	
	dispatch def boolean checkIntegrity(InputPort port) {
		println("Check Integrity of InputPort : " + port)
		if (port.hasInvalidPrimeReference && !port.container.hasInvalidPrimeReference) {
			port.addError(
				'''Input port '«port.cachedReferencedObjectName»' does not exist. SIB out of date? '''
			)
			return false
		}
		return true
	}
	
	dispatch def boolean checkReplacement(InputPort port) {
		val replacement = port.replacement
		if (replacement === null) {
			port.addError(
				'''Replacement for input port '«port.name»' of SIB '«port.sib.name»' cannot be inferred'''
			)
			return false
		}
		
		if (port.dataType === null) return false
		
		(port.targetedPorts + #[replacement]).toSet
			.filter[dataType !== null]
			.forEach[target|
				port.check[
					port.isTypeOf(target)
				].elseError(
					'''Type of input port '«port.name»' of SIB '«port.sib.name»' is incompatible to the type of the target port '«target?.name»' of SIB '«target?.sib?.name»' '''
				)
			]
		return true
	}
	
	dispatch def boolean checkIntegrity(Branch branch) {
		println("Branch " + branch.sib.name + "." + branch.name + " hasInvalidPrimeReference: " + branch.hasInvalidPrimeReference)
		if (branch.hasInvalidPrimeReference && !branch.container.hasInvalidPrimeReference) {
			branch.addError(
				'''Branch '«branch.cachedReferencedObjectName»' does not exist. SIB out of date? '''
			)
			return false
		}
		return branch.find(OutputPort).map[checkIntegrity].forall[it === true]
	}
	
	dispatch def boolean checkReplacement(Branch branch) {
		println("Branch " + branch.sib.name + "." + branch.name + " replacement: " + branch.replacement)
		if (branch.replacement === null) {
			branch.addError(
				'''Replacement for branch '«branch.name»' of SIB '«branch.sib.name»' cannot be inferred'''
			)
			return false
		}
		
		branch => [
			if (sib instanceof BlueprintSIB) sib.addInfo(
				'''Inferred replacement for branch '«branch.name»' is branch '«branch.inferredReplacement?.name»' of SIB '«branch.inferredReplacement?.sib?.name»' '''
			)
		]
		return branch.find(OutputPort).map[checkReplacement].forall[it === true]
	}
	
	dispatch def boolean checkIntegrity(OutputPort port) {
		if (port.hasInvalidPrimeReference && !port.container.hasInvalidPrimeReference) {
			port.addError(
				'''Output port '«port.cachedReferencedObjectName»' does not exist. SIB out of date? '''
			)
			return false
		}
		return true
	}
	
	dispatch def boolean checkReplacement(OutputPort port) {
		val replacement = port.replacement
		if (replacement === null) {
			port.addError(
				'''Replacement for output port '«port.name»' of branch '«port.branch.name»' of SIB '«port.branch.sib.name»' cannot be inferred'''
			)
			return false
		}
		
		if (port.dataType === null) return false
		
		(port.targetedPorts + #[replacement]).toSet
			.filter[dataType !== null]
			.forEach[target| 
				port.check[
					port.isTypeOf(target)
				].elseError(
					'''Type of output port '«port.name»' of branch '«port.branch.name»' of SIB '«port.branch.sib.name»' is incompatible to the type of the target port '«target?.name»' of branch '«target?.branch?.name»' of SIB '«target?.branch?.sib?.name»' '''
				)
			]
		return true
	}
}