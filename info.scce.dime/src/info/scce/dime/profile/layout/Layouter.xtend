package info.scce.dime.profile.layout

import de.jabc.cinco.meta.runtime.layout.ListLayouter
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.profile.OutputPort
import info.scce.dime.profile.profile.SIB

import static de.jabc.cinco.meta.runtime.layout.LayoutConfiguration.*

class Layouter extends ListLayouter {
	
	static Layouter _instance
	static def getInstance() {
		_instance ?: (_instance = new Layouter)
	}

	dispatch def getLayoutConfiguration(SIB sib) {#{
		PADDING_TOP -> 30
	}}
	
	dispatch def getLayoutConfiguration(Branch container) {#{
		MIN_WIDTH -> 100,
		MIN_HEIGHT -> 20,
		PADDING_TOP -> 24
	}}
	
	dispatch def getLayoutConfiguration(InputPort container) {#{
		MIN_HEIGHT -> 18,
		PADDING_TOP -> 24,
		PADDING_BOTTOM -> 5
	}}
	
	dispatch def getLayoutConfiguration(OutputPort container) {#{
		MIN_HEIGHT -> 18,
		PADDING_TOP -> 24
	}}
	
	dispatch def getLayoutConfiguration(BlueprintSIB sib) {#{
		MIN_HEIGHT -> 34,
		PADDING_TOP -> 38
	}}
	
	dispatch def getInnerOrder(SIB sib) {
		#[InputPort, Branch]
	}
}
