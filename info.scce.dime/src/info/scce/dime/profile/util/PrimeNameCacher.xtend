package info.scce.dime.profile.util

import graphmodel.Node
import info.scce.dime.gUIPlugin.Output
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.GUI
import info.scce.dime.process.process.BranchBlueprint
import info.scce.dime.process.process.EndSIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.SIB
import info.scce.dime.profile.api.ProfileExtension
import info.scce.dime.profile.profile.BlueprintBranch
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.BranchReferencingBranch
import info.scce.dime.profile.profile.ButtonReferencingBranch
import info.scce.dime.profile.profile.EndSIBReferencingBranch
import info.scce.dime.profile.profile.GUIBlueprintSIB
import info.scce.dime.profile.profile.GUISIB
import info.scce.dime.profile.profile.IO
import info.scce.dime.profile.profile.OutputReferencingBranch
import info.scce.dime.profile.profile.ProcessBlueprintSIB
import info.scce.dime.profile.profile.ProcessSIB
import info.scce.dime.profile.profile.ReplacementSIB

class PrimeNameCacher {
	
	extension ProfileExtension = new ProfileExtension
	
	dispatch def boolean cacheNames(Node node) {
		// do nothing
		return true
	}
	
	// ++++++++++++++++++
	//
	//       SIBs
	//
	// ++++++++++++++++++
	
	dispatch def boolean cacheNames(GUISIB sib) {
		val obj = sib.referencedGUI as GUI
		sib.cacheName(obj?.title)
		return obj !== null
	}
	
	dispatch def boolean cacheNames(ProcessSIB sib) {
		val obj = sib.referencedProcess as Process
		sib.cacheName(obj?.modelName)
		return obj !== null
	}
	
	dispatch def boolean cacheNames(ProcessBlueprintSIB sib) {
		val obj = sib.referencedProcessBlueprintSIB as SIB
		sib.cacheNames(obj)
		return obj !== null
	}
	
	dispatch def boolean cacheNames(GUIBlueprintSIB sib) {
		val obj = sib.referencedGUIBlueprintSIB as SIB
		sib.cacheNames(obj)
		return obj !== null
	}
	
	def cacheNames(BlueprintSIB sib, SIB refSib) {
		if (refSib !== null) {
			val refModelName = refSib.rootElement?.modelName
			if (!refModelName.nullOrEmpty
					&& sib.cachedReferencedBlueprintSIBsModelName != refModelName) {
				sib.cachedReferencedBlueprintSIBsModelName = refModelName
			}
			val refSibLabel = refSib.label
			if (!refSibLabel.nullOrEmpty
					&& sib.cachedReferencedBlueprintSIBLabel != refSibLabel) {
				sib.cachedReferencedBlueprintSIBLabel = refSibLabel
			}
		}
	}
	
	def void cacheName(ReplacementSIB sib, String name) {
		if (!name.nullOrEmpty
				&& sib.cachedReferencedObjectName != name) {
			sib.cachedReferencedObjectName = name
		}
	}
	
	// ++++++++++++++++++
	//
	//      Branches
	//
	// ++++++++++++++++++
	
	dispatch def boolean cacheNames(BranchReferencingBranch branch) {
		val obj = branch.referencedBranch as info.scce.dime.gui.gui.Branch
		branch.cacheName(obj?.name)
		return obj !== null
	}
	
	dispatch def boolean cacheNames(ButtonReferencingBranch branch) {
		val obj = branch.referencedButton as Button
		branch.cacheName(obj?.label)
		return obj !== null
	}
	
	dispatch def boolean cacheNames(EndSIBReferencingBranch branch) {
		val obj = branch.referencedEndSIB as EndSIB
		branch.cacheName(obj?.branchName)
		return obj !== null
	}
	
	dispatch def boolean cacheNames(OutputReferencingBranch branch) {
		val obj = branch.referencedOutput as Output
		branch.cacheName(obj?.outputName)
		return obj !== null
	}
	
	dispatch def boolean cacheNames(BlueprintBranch branch) {
		val obj = branch.referencedBranch as BranchBlueprint
		branch.cacheName(obj?.name)
		return obj !== null
	}
	
	def cacheName(Branch branch, String name) {
		if (!name.nullOrEmpty
				&& branch.cachedReferencedObjectName != name) {
			branch.cachedReferencedObjectName = name
		}
	}
	
	// ++++++++++++++++++
	//
	//         IO
	//
	// ++++++++++++++++++
	
	dispatch def boolean cacheNames(IO port) {
		val refPort = port.referencedObject
		if (refPort !== null) {
			val refPortName = port.referencedObjectName
			if (!refPortName.nullOrEmpty
					&& port.cachedReferencedObjectName != refPortName) {
				port.cachedReferencedObjectName = refPortName
			}
			var refPortDataTypeName = port.referencedObjectDataTypeName
			if (refPortDataTypeName !== null) {
				if (port.isReferencedObjectList) {
					refPortDataTypeName = "[" + refPortDataTypeName + "]"
				}
				if (port.cachedReferencedObjectDataTypeName != refPortDataTypeName) {
					port.cachedReferencedObjectDataTypeName = refPortDataTypeName
				}
			}
		}
		return refPort !== null
	}
}