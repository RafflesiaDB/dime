package info.scce.dime.profile.util

import info.scce.dime.profile.api.ProfileRuntimeBaseClass
import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.BranchReplacement
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.profile.OutputPort
import info.scce.dime.profile.profile.SIB
import info.scce.dime.profile.profile.SIBReplacement
import info.scce.dime.profile.profile.ReplacementBranch
import info.scce.dime.profile.profile.ReplacementSIB
import graphmodel.Node
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.ReplacementCondition
import java.util.Set

class ReplacementStrategy extends ProfileRuntimeBaseClass {
	
	// ++++++++++++++++++
	//
	//        SIB
	//
	// ++++++++++++++++++
	
	def getInferredReplacement(SIB sib) {
		sib.replacementPath.findFirst[replacementConditionsFulfilled]
//		=> [println("Inferred replacement for " + sib?.name + " => " + it?.referencedProcessName)]
	}
	
	def isInferredReplacement(Node sib) {
		sib.findPredecessorsVia(SIBReplacement).filter(BlueprintSIB).exists[ blueSib |
			blueSib.replacementConditionsFulfilled
			&& blueSib.getInferredReplacement == sib
		]
	}
	
	def getReplacement(Node node) {
		node.findTargetOf(SIBReplacement)
//		=> [println("Replacement for " + sib?.name + " => " + it?.referencedProcessName)]
	}
	
	def getReplacementPath(SIB sib) {
		val path = newArrayList
		var replSib = sib.replacement
		while (replSib !== null) {
			if (path.contains(replSib)) {
				return path
			} else {
				path.add(replSib)
			}
			replSib = replSib.replacement
		}
		return path
	}
	
	dispatch def boolean getReplacementConditionsFulfilled(Node node) {
		true
	}
	
	dispatch def boolean getReplacementConditionsFulfilled(SIB sib) {
		!sib.hasInvalidPrimeReference
		&& !sib.hasCycleInReplacementConditions
		&& sib.outgoingReplacementConditions.forall[
				if (isNegate) !targetElement.isInferredReplacement
				else targetElement.isInferredReplacement
			]
		=> [println("Replacement conditions for " + sib?.name + " fulfilled? " + it)]
	}
	
	def hasCycleInReplacementConditions(SIB sib) {
		sib.findCycleInReplacementConditions !== null
	}
	
	def findCycleInReplacementConditions(Node sib) {
		sib.findCycleInReplacementConditions_recurse(newLinkedHashSet)
	}
	
	private def Set<Node> findCycleInReplacementConditions_recurse(Node node, Set<Node> visited) {
		if (!visited.add(node)) {
			return visited
		}
		node.getOutgoing(ReplacementCondition)
			.map[targetElement.findCycleInReplacementConditions_recurse(newLinkedHashSet => [addAll(visited)])]
			.findFirst[!nullOrEmpty]
			?: null
	}
	
	// ++++++++++++++++++
	//
	//      SIB Port
	//
	// ++++++++++++++++++
	
	def InputPort getInferredReplacement(InputPort port) {
		val sibReplacement = port.sib.getInferredReplacement
		if (sibReplacement instanceof ReplacementSIB) {
			port.getInferredReplacement(sibReplacement)
			=> [
				println("Inferred replacement for " + port?.name + " of (" + port.sib.name + " -> " + sibReplacement?.name + ") => " + it?.cachedReferencedObjectName)
			]
		}
	}
	
	def InputPort getInferredReplacement(InputPort port, ReplacementSIB replSib) {
		port.getReplacementByEdge(replSib)
		?: port.getReplacementByPath(replSib)
		=> [
			println("Inferred replacement for " + port?.name + " of (" + port.sib.name + " -> " + replSib?.name + ") => " + it?.cachedReferencedObjectName)
		]
	}
	
	def InputPort getReplacement(InputPort port) {
		val replSib = port.sib.replacement
		if (replSib instanceof ReplacementSIB) {
			port.getReplacementByEdge(replSib)
			?: port.getReplacementByName(replSib)
		}
	}
	
	def getReplacementByEdge(InputPort port, ReplacementSIB replSib) {
		port.targetedPorts.filter[it.sib == replSib].head as InputPort
	}
	
	def getReplacementByName(InputPort port, ReplacementSIB replSib) {
		replSib.find(InputPort).filter[it.name == port.name].head as InputPort
	}
	
	def getReplacementByPath(InputPort port, ReplacementSIB replSib) {
		val path = port.sib.replacementPath
		if (path.contains(replSib)) {
			var currPort = port
			for (i : 0 ..< path.size) {
				val currSib = path.get(i) as ReplacementSIB
				currPort = currPort.getReplacementByEdge(currSib) ?: currPort.getReplacementByName(currSib)
				if (currSib == replSib || currPort === null) {
					return currPort as InputPort
				}
			}
		}
	}
	
	// ++++++++++++++++++
	//
	//      Branch
	//
	// ++++++++++++++++++
	
	def ReplacementBranch getInferredReplacement(Branch branch) {
		val sibReplacement = branch.sib.getInferredReplacement
		if (sibReplacement instanceof ReplacementSIB) {
			branch.getInferredReplacement(sibReplacement)
//			=> [println("Inferred replacement for " + branch?.name + " => " + it?.referencedEndSIBName)]
		}
	}
	
	def ReplacementBranch getInferredReplacement(Branch branch, ReplacementSIB replSib) {
		branch.getReplacementByEdge(replSib)
		?: branch.getReplacementByPath(replSib)
	}
	
	def ReplacementBranch getReplacement(Branch branch) {
		val replSib = branch.sib.replacement
		if (replSib instanceof ReplacementSIB) {
			branch.getReplacementByEdge(replSib)
			?: branch.getReplacementByName(replSib)
		}
	}
	
	def getReplacementByEdge(Branch branch, ReplacementSIB replSib) {
		branch.getOutgoing(BranchReplacement).map[targetElement].filter[it.sib == replSib].head as ReplacementBranch
	}
	
	def getReplacementByName(Branch branch, ReplacementSIB replSib) {
		replSib.find(Branch).filter[it.name == branch.name].head as ReplacementBranch
	}
	
	def getReplacementByPath(Branch branch, ReplacementSIB replSib) {
		val path = branch.sib.replacementPath
		if (path.contains(replSib)) {
			var currBranch = branch
			for (i : 0 ..< path.size) {
				val currSib = path.get(i) as ReplacementSIB
				currBranch = currBranch.getReplacementByEdge(currSib) ?: currBranch.getReplacementByName(currSib)
				if (currSib == replSib || currBranch === null) {
					return currBranch as ReplacementBranch
				}
			}
		}
	}
	
	def getReplacements(Iterable<OutputPort> it) {
		map[targetedPorts].flatten
	}
	
	// ++++++++++++++++++
	//
	//    Branch Port
	//
	// ++++++++++++++++++
	
	def OutputPort getInferredReplacement(OutputPort port) {
		val sibReplacement = port.branch.sib.getInferredReplacement
		if (sibReplacement instanceof ReplacementSIB) {
			port.getInferredReplacement(sibReplacement)
//			=> [
//				println("Inferred replacement for " + port?.name + " of (" + port.branch.sib.name + " -> " + port.sib.inferredReplacement?.name + ") => " + it?.referencedInputPortName)
//			]
		}
	}
	
	def OutputPort getInferredReplacement(OutputPort port, ReplacementSIB replSib) {
		port.getReplacementByEdge(replSib)
		?: port.getReplacementByPath(replSib)
		=> [
			println("Inferred replacement for " + port.name + " of branch " + port.branch.name + " of (" + port.branch.sib.name + " -> " + replSib?.name + ") => " + it?.cachedReferencedObjectName)
		]
	}
	
	def OutputPort getReplacement(OutputPort port) {
		val replSib = port.branch.sib.replacement
		if (replSib instanceof ReplacementSIB) {
			port.getReplacementByEdge(replSib)
			?: port.getReplacementByName(replSib)
		}
	}
	
	def getReplacementByEdge(OutputPort port, ReplacementSIB replSib) {
		port.targetedPorts.filter[it.branch.sib == replSib].head as OutputPort
	}
	
	def getReplacementByName(OutputPort port, ReplacementSIB replSib) {
		port.branch.getInferredReplacement(replSib).find(OutputPort).filter[it.name == port.name].head as OutputPort
	}
	
	def getReplacementByPath(OutputPort port, ReplacementSIB replSib) {
		val path = port.branch.sib.replacementPath
		if (path.contains(replSib)) {
			var currPort = port
			for (i : 0 ..< path.size) {
				val currSib = path.get(i) as ReplacementSIB
				currPort = currPort.getReplacementByEdge(currSib) ?: currPort.getReplacementByName(currSib)
				if (currSib == replSib || currPort === null) {
					return currPort as OutputPort
				}
			}
		}
	}
	
	
}