package info.scce.dime.profile.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import graphmodel.Node
import info.scce.dime.profile.api.ProfileRuntimeBaseClass
import info.scce.dime.profile.util.PrimeNameCacher
import style.Appearance
import style.LineStyle
import style.StyleFactory

class SIBAppearance<T extends Node> extends ProfileRuntimeBaseClass implements StyleAppearanceProvider<T> {
	
	extension PrimeNameCacher = new PrimeNameCacher
	
	static val factory = StyleFactory.eINSTANCE
	static val appearances = <Node,Appearance> newHashMap
	
	override getAppearance(T node, String shapeId) {
		if (shapeId == "rootShape") { // compute only once for each style
			val isValid = cacheNames(node)
			if (isValid) {
				// TODO maybe somehow mark replacements with green border color?
			} else if (!node.container.hasInvalidPrimeReference) {
				appearances.put(node, factory.createAppearance => [
					foreground = factory.createColor => [r=255]
					lineStyle = LineStyle.DASH
				])
			} else appearances.remove(node)
		}
		return appearances.get(node)
	}
	
}