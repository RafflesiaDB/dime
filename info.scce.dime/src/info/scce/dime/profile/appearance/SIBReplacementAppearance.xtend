package info.scce.dime.profile.appearance

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import graphmodel.Edge
import info.scce.dime.profile.api.ProfileRuntimeBaseClass
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.SIBReplacement
import info.scce.dime.profile.util.ReplacementStrategy
import style.StyleFactory

class SIBReplacementAppearance<T extends Edge> extends ProfileRuntimeBaseClass implements StyleAppearanceProvider<T> {
	
	static val factory = StyleFactory.eINSTANCE
	static val thickLineAppearance = factory.createAppearance => [ lineWidth = 3 ]
	
	extension ReplacementStrategy = new ReplacementStrategy
	
	override getAppearance(T edge, String shapeId) {
		val targetNode = edge.targetElement
		val blueprintSIB = targetNode.findPredecessorsVia(SIBReplacement).filter(BlueprintSIB).head
		if (blueprintSIB !== null) {
			val replacementPath = blueprintSIB.replacementPath
			if (!replacementPath.exists[!hasInvalidPrimeReference]) {
				return null
			}
			for (node : replacementPath) {
				if (node == targetNode) {
					return thickLineAppearance
				}
				if (!node.hasInvalidPrimeReference) {
					return null
				}
			}
		}
	}
	
}