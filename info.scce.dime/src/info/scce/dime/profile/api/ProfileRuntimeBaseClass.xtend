package info.scce.dime.profile.api

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass

class ProfileRuntimeBaseClass extends CincoRuntimeBaseClass {
	
	protected extension ProfileExtension = new ProfileExtension
}