package info.scce.dime.profile.api

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.IdentifiableElement
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.profile.profile.AbstractParameterReferencingOutputPort
import info.scce.dime.profile.profile.AddToSubmissionReferencingOutputPort
import info.scce.dime.profile.profile.BlueprintSIB
import info.scce.dime.profile.profile.Branch
import info.scce.dime.profile.profile.BranchReferencingBranch
import info.scce.dime.profile.profile.ButtonReferencingBranch
import info.scce.dime.profile.profile.ComplexAttributeReferencingOutputPort
import info.scce.dime.profile.profile.ComplexInputPortReferencingInputPort
import info.scce.dime.profile.profile.ComplexInputPortReferencingOutputPort
import info.scce.dime.profile.profile.ComplexOutputPortReferencingInputPort
import info.scce.dime.profile.profile.ComplexOutputPortReferencingOutputPort
import info.scce.dime.profile.profile.ComplexVariableReferencingInputPort
import info.scce.dime.profile.profile.EndSIBReferencingBranch
import info.scce.dime.profile.profile.GUIComplexInputPortReferencingOutputPort
import info.scce.dime.profile.profile.GUIComplexOutputPortReferencingOutputPort
import info.scce.dime.profile.profile.GUIPrimitiveInputPortReferencingOutputPort
import info.scce.dime.profile.profile.GUIPrimitiveOutputPortReferencingOutputPort
import info.scce.dime.profile.profile.GUISIB
import info.scce.dime.profile.profile.InputPort
import info.scce.dime.profile.profile.InputPortMapping
import info.scce.dime.profile.profile.OutputGenericReferencingOutputPort
import info.scce.dime.profile.profile.OutputPort
import info.scce.dime.profile.profile.OutputPortMapping
import info.scce.dime.profile.profile.OutputReferencingBranch
import info.scce.dime.profile.profile.PrimitiveAttributeReferencingOutputPort
import info.scce.dime.profile.profile.PrimitiveInputPortReferencingInputPort
import info.scce.dime.profile.profile.PrimitiveInputPortReferencingOutputPort
import info.scce.dime.profile.profile.PrimitiveOutputPortReferencingInputPort
import info.scce.dime.profile.profile.PrimitiveOutputPortReferencingOutputPort
import info.scce.dime.profile.profile.PrimitiveVariableReferencingInputPort
import info.scce.dime.profile.profile.ProcessSIB
import info.scce.dime.profile.profile.ReplacementSIB
import info.scce.dime.profile.profile.SIB
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.profile.profile.IO
import info.scce.dime.profile.profile.ProcessBlueprintSIB
import info.scce.dime.profile.profile.GUIBlueprintSIB
import info.scce.dime.profile.profile.PrimitiveVariableReferencingOutputPort
import info.scce.dime.profile.profile.ComplexVariableReferencingOutputPort
import info.scce.dime.profile.build.ProcessSIBBuild
import info.scce.dime.profile.build.GUISIBBuild
import info.scce.dime.profile.build.ProcessBlueprintSIBBuild
import info.scce.dime.profile.build.GUIBlueprintSIBBuild
import info.scce.dime.profile.profile.BlueprintBranch

import static extension info.scce.dime.gui.helper.GUIBranchPort.*

class ProfileExtension extends CincoRuntimeBaseClass {
	
	extension DataExtension = DataExtension.instance
	extension GUIExtension = new GUIExtension
	
	// ++++++++++++++++++
	//
	//        SIB
	//
	// ++++++++++++++++++
	
	def getName(SIB it) {
		switch it {
			BlueprintSIB: cachedReferencedBlueprintSIBLabel
			ReplacementSIB: cachedReferencedObjectName
		}
	}
	
	def getCompound(SIB sib) {
		switch sib {
			ProcessSIB: new ProcessSIBBuild(sib)
			ProcessBlueprintSIB: new ProcessBlueprintSIBBuild(sib)
			GUISIB: new GUISIBBuild(sib)
			GUIBlueprintSIB: new GUIBlueprintSIBBuild(sib)
		}
	}
	
	// ++++++++++++++++++
	//
	//      Branch
	//
	// ++++++++++++++++++
	
	def getName(Branch it) {
		cachedReferencedObjectName
	}
	
	def getSib(Branch it) {
		findFirstParent(SIB)
	}
	
	// ++++++++++++++++++
	//
	//         IO
	//
	// ++++++++++++++++++
	
	def getName(IO it) {
		cachedReferencedObjectName
	}
	
	def getDataType(IO it) {
		switch it {
			PrimitiveInputPortReferencingInputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.PrimitiveInputPort)?.dataType
			ComplexInputPortReferencingInputPort: (referencedComplexInputPort as info.scce.dime.process.process.ComplexInputPort)?.dataType
			PrimitiveOutputPortReferencingInputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.PrimitiveOutputPort)?.dataType
			ComplexOutputPortReferencingInputPort: (referencedComplexOutputPort as info.scce.dime.process.process.ComplexOutputPort)?.dataType
			PrimitiveOutputPortReferencingOutputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.PrimitiveOutputPort)?.dataType
			ComplexOutputPortReferencingOutputPort: (referencedComplexOutputPort as info.scce.dime.process.process.ComplexOutputPort)?.dataType
			PrimitiveInputPortReferencingOutputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.PrimitiveInputPort)?.dataType
			ComplexInputPortReferencingOutputPort: (referencedComplexInputPort as info.scce.dime.process.process.ComplexInputPort)?.dataType
		}
	}
	
	def isTypeOf(IO port, IO other) {
		switch dataType : port.dataType {
			info.scce.dime.process.process.PrimitiveType: dataType == other?.dataType
			info.scce.dime.data.data.Type: switch otherType:other?.dataType {
				Type: dataType.isTypeOf(otherType)
				default: false
			}
			default: false
		}
	}
	
	// ++++++++++++++++++
	//
	//     Input Port
	//
	// ++++++++++++++++++
	
	def getSib(InputPort it) {
		findFirstParent(SIB)
	}
	
	def getTargetedPorts(InputPort it) {
		getOutgoing(InputPortMapping)?.map[targetElement]
	}
	
	// ++++++++++++++++++
	//
	//     Output Port
	//
	// ++++++++++++++++++
	
	def getBranch(OutputPort it) {
		findFirstParent(Branch)
	}
	
	def getTargetedPorts(OutputPort it) {
		getOutgoing(OutputPortMapping)?.map[targetElement]
	}
	
	// ++++++++++++++++++
	//
	//  Prime References
	//
	// ++++++++++++++++++
	
	def getReferencedObject(SIB it) {
		switch it {
			ProcessSIB: referencedProcess
			GUISIB: referencedGUI
			ProcessBlueprintSIB: referencedProcessBlueprintSIB
			GUIBlueprintSIB: referencedGUIBlueprintSIB
		}
	}
	
	def getReferencedObject(Branch it) {
		switch it {
			BlueprintBranch: referencedBranch
			BranchReferencingBranch: referencedBranch
			ButtonReferencingBranch: referencedButton
			EndSIBReferencingBranch: referencedEndSIB
			OutputReferencingBranch: referencedOutput
		}
	}
	
	def getReferencedObject(IO it) {
		switch it {
			// InputPort => GUI model elements
			PrimitiveVariableReferencingInputPort: referencedPrimitiveVariable
			ComplexVariableReferencingInputPort: referencedComplexVariable
			// InputPort => Process model elements
			PrimitiveInputPortReferencingInputPort: referencedPrimitiveInputPort
			ComplexInputPortReferencingInputPort: referencedComplexInputPort
			PrimitiveOutputPortReferencingInputPort: referencedPrimitiveOutputPort
			ComplexOutputPortReferencingInputPort: referencedComplexOutputPort
			// OutputPort => GUI model elements
			AbstractParameterReferencingOutputPort: referencedAbstractParameter
			AddToSubmissionReferencingOutputPort: referencedAddToSubmission
			PrimitiveAttributeReferencingOutputPort: referencedPrimitiveAttribute
			ComplexAttributeReferencingOutputPort: referencedComplexAttribute
			GUIPrimitiveInputPortReferencingOutputPort: referencedPrimitiveInputPort
			GUIComplexInputPortReferencingOutputPort: referencedComplexInputPort
			GUIPrimitiveOutputPortReferencingOutputPort: referencedPrimitiveOutputPort
			GUIComplexOutputPortReferencingOutputPort: referencedComplexOutputPort
			OutputGenericReferencingOutputPort: referencedOutputGeneric
			PrimitiveVariableReferencingOutputPort: referencedPrimitiveVariable
			ComplexVariableReferencingOutputPort: referencedComplexVariable
			// OutputPort => Process model elements
			PrimitiveInputPortReferencingOutputPort: referencedPrimitiveInputPort
			ComplexInputPortReferencingOutputPort: referencedComplexInputPort
			PrimitiveOutputPortReferencingOutputPort: referencedPrimitiveOutputPort
			ComplexOutputPortReferencingOutputPort: referencedComplexOutputPort
		}
	}
	
	def isReferencedObjectList(IO it) {
		switch it {
			// InputPort => GUI model elements
			PrimitiveVariableReferencingInputPort: (referencedPrimitiveVariable as info.scce.dime.gui.gui.Variable)?.isList
			ComplexVariableReferencingInputPort: (referencedComplexVariable as info.scce.dime.gui.gui.Variable)?.isList
			// InputPort => Process model elements
			PrimitiveInputPortReferencingInputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.IO).isList
			ComplexInputPortReferencingInputPort: (referencedComplexInputPort as info.scce.dime.process.process.IO).isList
			PrimitiveOutputPortReferencingInputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.IO).isList
			ComplexOutputPortReferencingInputPort: (referencedComplexOutputPort as info.scce.dime.process.process.IO).isList
			// OutputPort => GUI model elements

			AbstractParameterReferencingOutputPort: referencedAbstractParameter.isList
			AddToSubmissionReferencingOutputPort: referencedAddToSubmission.isList
			PrimitiveAttributeReferencingOutputPort: referencedPrimitiveAttribute.isList
			ComplexAttributeReferencingOutputPort: referencedComplexAttribute.isList
			GUIPrimitiveInputPortReferencingOutputPort: referencedPrimitiveInputPort.isList
			GUIComplexInputPortReferencingOutputPort: referencedComplexInputPort.isList
			GUIPrimitiveOutputPortReferencingOutputPort: referencedPrimitiveOutputPort.isList
			GUIComplexOutputPortReferencingOutputPort: referencedComplexOutputPort.isList
			OutputGenericReferencingOutputPort: referencedOutputGeneric.isList
			PrimitiveVariableReferencingOutputPort: referencedPrimitiveVariable.isList
			ComplexVariableReferencingOutputPort: referencedComplexVariable.isList

			// OutputPort => Process model elements
			PrimitiveInputPortReferencingOutputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.IO).isList
			ComplexInputPortReferencingOutputPort: (referencedComplexInputPort as info.scce.dime.process.process.IO).isList
			PrimitiveOutputPortReferencingOutputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.IO).isList
			ComplexOutputPortReferencingOutputPort: (referencedComplexOutputPort as info.scce.dime.process.process.IO).isList
		}
	}
	
	def getReferencedObjectName(IO it) {
		switch it {
			// InputPort => GUI model elements
			PrimitiveVariableReferencingInputPort: (referencedPrimitiveVariable as info.scce.dime.gui.gui.Variable)?.name
			ComplexVariableReferencingInputPort: (referencedComplexVariable as info.scce.dime.gui.gui.Variable)?.name
			// InputPort => Process model elements
			PrimitiveInputPortReferencingInputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.InputPort)?.name
			ComplexInputPortReferencingInputPort: (referencedComplexInputPort as info.scce.dime.process.process.InputPort)?.name
			PrimitiveOutputPortReferencingInputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.OutputPort)?.name
			ComplexOutputPortReferencingInputPort: (referencedComplexOutputPort as info.scce.dime.process.process.OutputPort)?.name
			// OutputPort => GUI model elements
			AbstractParameterReferencingOutputPort: referencedAbstractParameter.portName
			AddToSubmissionReferencingOutputPort: referencedAddToSubmission.portName
			PrimitiveAttributeReferencingOutputPort: referencedPrimitiveAttribute.portName
			ComplexAttributeReferencingOutputPort: referencedComplexAttribute.portName
			GUIPrimitiveInputPortReferencingOutputPort: referencedPrimitiveInputPort.portName
			GUIComplexInputPortReferencingOutputPort: referencedComplexInputPort.portName
			GUIPrimitiveOutputPortReferencingOutputPort: referencedPrimitiveOutputPort.portName
			GUIComplexOutputPortReferencingOutputPort: referencedComplexOutputPort.portName
			OutputGenericReferencingOutputPort: referencedOutputGeneric.portName
			PrimitiveVariableReferencingOutputPort: referencedPrimitiveVariable.portName
			ComplexVariableReferencingOutputPort: referencedComplexVariable.portName
 			// OutputPort => Process model elements
			PrimitiveInputPortReferencingOutputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.IO).portName
			ComplexInputPortReferencingOutputPort: (referencedComplexInputPort as info.scce.dime.process.process.IO).portName
			PrimitiveOutputPortReferencingOutputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.IO).portName
			ComplexOutputPortReferencingOutputPort: (referencedComplexOutputPort as info.scce.dime.process.process.IO).portName
		}
	}
	
	def getReferencedObjectDataTypeName(IO it) {
		switch it {
			// InputPort => GUI model elements
			PrimitiveVariableReferencingInputPort: (referencedPrimitiveVariable as info.scce.dime.gui.gui.PrimitiveVariable)?.dataType?.toString
			ComplexVariableReferencingInputPort: (referencedComplexVariable as info.scce.dime.gui.gui.ComplexVariable)?.dataType?.name
			// InputPort => Process model elements
			PrimitiveInputPortReferencingInputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.IO)?.dataTypeName
			ComplexInputPortReferencingInputPort: (referencedComplexInputPort as info.scce.dime.process.process.IO)?.dataTypeName
			PrimitiveOutputPortReferencingInputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.IO)?.dataTypeName
			ComplexOutputPortReferencingInputPort: (referencedComplexOutputPort as info.scce.dime.process.process.IO)?.dataTypeName
			// OutputPort => GUI model elements
			AbstractParameterReferencingOutputPort: {
				switch it:(referencedAbstractParameter as info.scce.dime.gUIPlugin.AbstractParameter) {
					info.scce.dime.gUIPlugin.ComplexParameter: (type as info.scce.dime.data.data.Type)?.name
					info.scce.dime.gUIPlugin.PrimitiveParameter: type?.toString
					info.scce.dime.gUIPlugin.GenericParameter: typeParameterName
				}
			}
			PrimitiveAttributeReferencingOutputPort: (referencedPrimitiveAttribute as info.scce.dime.gui.gui.Attribute)?.dataTypeName
			ComplexAttributeReferencingOutputPort: (referencedComplexAttribute as info.scce.dime.gui.gui.Attribute)?.dataTypeName
			GUIPrimitiveInputPortReferencingOutputPort: (referencedPrimitiveInputPort as info.scce.dime.gui.gui.InputPort)?.dataTypeName
			GUIComplexInputPortReferencingOutputPort: (referencedComplexInputPort as info.scce.dime.gui.gui.InputPort)?.dataTypeName
			GUIPrimitiveOutputPortReferencingOutputPort: (referencedPrimitiveOutputPort as info.scce.dime.gui.gui.OutputPort)?.dataTypeName
			GUIComplexOutputPortReferencingOutputPort: (referencedComplexOutputPort as info.scce.dime.gui.gui.OutputPort)?.dataTypeName
			OutputGenericReferencingOutputPort: (referencedOutputGeneric as info.scce.dime.gui.gui.OutputPort).dataTypeName
			PrimitiveVariableReferencingOutputPort: (referencedPrimitiveVariable as info.scce.dime.gui.gui.Variable).dataTypeName
			ComplexVariableReferencingOutputPort: (referencedComplexVariable as info.scce.dime.gui.gui.Variable).dataTypeName
			// OutputPort => Process model elements
			PrimitiveInputPortReferencingOutputPort: (referencedPrimitiveInputPort as info.scce.dime.process.process.IO).portName
			ComplexInputPortReferencingOutputPort: (referencedComplexInputPort as info.scce.dime.process.process.IO).portName
			PrimitiveOutputPortReferencingOutputPort: (referencedPrimitiveOutputPort as info.scce.dime.process.process.IO).portName
			ComplexOutputPortReferencingOutputPort: (referencedComplexOutputPort as info.scce.dime.process.process.IO).portName
		}
	}
	
	def getPortName(info.scce.dime.process.process.IO it) {
		switch it {
			info.scce.dime.process.process.InputPort: name
			info.scce.dime.process.process.OutputPort: name
		}
	}
	
	def getDataTypeName(info.scce.dime.process.process.IO it) {
		switch it {
			info.scce.dime.process.process.PrimitiveInputPort: dataType?.toString
			info.scce.dime.process.process.ComplexInputPort: dataType?.name
			info.scce.dime.process.process.PrimitiveOutputPort: dataType?.toString
			info.scce.dime.process.process.ComplexOutputPort: dataType?.name
		}
	}
	
	def getDataTypeName(info.scce.dime.gui.gui.InputPort it) {
		switch it {
			info.scce.dime.gui.gui.PrimitiveInputPort: dataType?.toString
			info.scce.dime.gui.gui.ComplexInputPort: dataType?.name
		}
	}
	
	def getDataTypeName(info.scce.dime.gui.gui.OutputPort it) {
		switch it {
			info.scce.dime.gui.gui.PrimitiveOutputPort: dataType?.toString
			info.scce.dime.gui.gui.ComplexOutputPort: dataType?.name
			info.scce.dime.gui.gui.OutputGeneric: typeParameter
		}
	}
	
	def getDataTypeName(info.scce.dime.gui.gui.Variable it) {
		switch it {
			info.scce.dime.gui.gui.PrimitiveVariable: dataType?.toString
			info.scce.dime.gui.gui.ComplexVariable:  dataType?.name
		}
	}
	
	def getDataTypeName(info.scce.dime.gui.gui.Attribute it) {
		switch it {
			info.scce.dime.gui.gui.PrimitiveAttribute: attribute?.dataType?.toString
			info.scce.dime.gui.gui.ComplexAttribute: attribute?.dataType?.name
		}
	}
	
	def isList(info.scce.dime.process.process.IO it) {
		switch it {
			info.scce.dime.process.process.PrimitiveInputPort: isList
			info.scce.dime.process.process.ComplexInputPort: isList
			info.scce.dime.process.process.PrimitiveOutputPort: isList
			info.scce.dime.process.process.ComplexOutputPort: isList
		}
	}
	
	dispatch def hasInvalidPrimeReference(IdentifiableElement it) {
		false
	}
	
	dispatch def hasInvalidPrimeReference(SIB sib) {
		sib.referencedObject === null
	}
	
	dispatch def hasInvalidPrimeReference(Branch branch) {
		branch.referencedObject === null
	}
	
	dispatch def hasInvalidPrimeReference(IO port) {
		port.referencedObject === null
	}
}