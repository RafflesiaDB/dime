package info.scce.dime.gui.checks;

import info.scce.dime.checks.AbstractModelIntegrityCheck;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * Check if the given GUI model is valid
 * @author zweihoff
 *
 */
public class ModelIntegrityCheck
	   extends AbstractModelIntegrityCheck<GUIId, GUIAdapter> {
}
