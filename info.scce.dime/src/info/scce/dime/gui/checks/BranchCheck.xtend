package info.scce.dime.gui.checks

import graphmodel.Node
import info.scce.dime.checks.GUICheck
import info.scce.dime.gui.gui.AddToSubmission
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.ComplexAttributeConnector
import info.scce.dime.gui.gui.ComplexListAttributeConnector
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.FormLoadSubmit
import info.scce.dime.gui.gui.FormSubmit
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Read

class BranchCheck extends GUICheck {
	
	override check(GUI model) {
		
		// check that branch ports are not connected
		model.find(Branch).flatMap[outputPorts].forEach[
			check[outgoing.isEmpty].elseError('''No outgoing edges allowed for port «it.name»''')
		]
		
		// check that modified variables can be persisted
		model.allEdges
			.filter[it instanceof FormSubmit || it instanceof FormLoadSubmit]
			.map[sourceElement]
			.forEach[
				check[isReadOrSubmitted].elseError("Modified variable is not persisted")
			]
	}
	
	def boolean isReadOrSubmitted(Node it) {
		switch it {
			Attribute: (container as ComplexVariable).isReadOrSubmitted
			default:
				! findSuccessorsVia(Read, AddToSubmission).isEmpty
				|| findPredecessorsVia(
						ComplexAttributeConnector, ComplexListAttributeConnector
				   ).exists[isReadOrSubmitted]
		}
	}
}
