package info.scce.dime.gui.checks;

import graphmodel.Edge;
import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ChoiceData;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttribute;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeName;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.FOR;
import info.scce.dime.gui.gui.InputPort;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.PrimitiveFOR;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Read;
import info.scce.dime.gui.gui.SIB;
import info.scce.dime.gui.gui.TableColumnLoad;
import info.scce.dime.gui.gui.TableLoad;
import info.scce.dime.gui.gui.Template;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The iteration scope check is used to verify that no invalid
 * data assignments are specified depended on the iteration scopes
 * provided by iteration edges
 * @author zweihoff
 *
 */
public class IterationScopeCheck extends AbstractCheck<GUIId, GUIAdapter> {

	
	/**
	 * Checks, if any components in a given GUI model
	 * are part of an iteration scope and access variables
	 * which are only accessible in other scopes
	 */
	@Override
	public void doExecute(GUIAdapter adapter) {

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof MovableContainer) {
				checkEdges(id, (MovableContainer) obj);
			}
		}
	}
	
	@Override
	public void init() {}
	
	/**
	 * Checks if the given component is connected to a variable or
	 * attribute and validates that they are all in the same scope
	 * @param id
	 * @param mc
	 */
	private void checkEdges(GUIId id, MovableContainer mc) {
		List<MovableContainer> parentContainers = getContainerHierarchy(mc);
		for (Edge edge : getIncomingEdges(mc)) {

			Node current = findCurrent(edge.getSourceElement());
			if (current != null) {
				Variable listVar = (Variable) getParent(current);
				List<MovableContainer> targets = getIterationTargetContainers(listVar);
				if (targets.isEmpty()) {
					addError(id, "Iteration scope missing");
					break;
				}
				else if (Collections.disjoint(targets, parentContainers)) {
					addError(id, "Outside of iteration scope");
					break;
				}
			}
		}
	}
	
	/**
	 * Returns a list of all parent components of the given component
	 * @param mc
	 * @return
	 */
	private List<MovableContainer> getContainerHierarchy(MovableContainer mc) {
		List<MovableContainer> hierarchy = new ArrayList<>();
		hierarchy.add(mc);
		ModelElementContainer parent = mc.getContainer();
		if (parent == null || parent instanceof Template)
			return hierarchy;
		if (parent instanceof MovableContainer)
			hierarchy.addAll(getContainerHierarchy((MovableContainer) parent));
		return hierarchy;
	}
	/**
	 * Returns a list of all data binding edges of the given component
	 * @param mc
	 * @return
	 */
	private List<Edge> getIncomingEdges(MovableContainer mc) {
		// default edges
		List<Edge> edges = new LinkedList<>(mc.getIncoming().stream().filter(n->!(n instanceof TableColumnLoad)).collect(Collectors.toList()));
		// edges for GUISibs and GUIPlugins
		if (mc instanceof SIB){
			edges.addAll(
				((SIB) mc).getIOs().stream()
					.filter(InputPort.class::isInstance)
					.flatMap(n -> n.getIncoming(Read.class).stream())
					.collect(Collectors.toList()));
		}
		return edges;
	}
	
	/**
	 * Returns the first found iteration scope creating
	 * parent variable
	 * @param node
	 * @return
	 */
	private Node findCurrent(Node node) {
		if (isCurrent(node))
			return node;
		Node parent = getParent(node);
		return (parent != null)
			? findCurrent(parent)
			: null;
	}
	
	/**
	 * Returns the parent variable for the given attribute or variable
	 * @param node
	 * @return
	 */
	private Node getParent(Node node) {
		if (node instanceof Attribute)
			return (Node) node.getContainer();
		List<? extends Edge> incoming = node.getIncoming(ComplexAttributeConnector.class);
		if (!incoming.isEmpty())
			return incoming.get(0).getSourceElement();
		incoming = node.getIncoming(ComplexListAttributeConnector.class);
		if (!incoming.isEmpty())
			return incoming.get(0).getSourceElement();
		return null;
	}
	
	/**
	 * Checks, if the given attribute or variable creates a new iteration scope.
	 * An iteration scope is recognized by at least one outgoing iteration edge
	 * @param node
	 * @return
	 */
	private boolean isCurrent(Node node) {
		if (node instanceof ComplexListAttribute) {
			return ((ComplexListAttribute) node).getAttributeName() == ComplexListAttributeName.CURRENT;
		}
		if (node instanceof ComplexVariable) {
			List<ComplexListAttributeConnector> incoming = ((ComplexVariable) node).getIncoming(ComplexListAttributeConnector.class);
			if (!incoming.isEmpty()) {
				return incoming.get(0).getAttributeName() == ComplexListAttributeName.CURRENT;
			}
		}
		return false;
	}
	
	/**
	 * Returns the inner components which are placed in the iteration scope component
	 * connected to the given variable by an iteration edge
	 * @param v
	 * @return
	 */
	private List<MovableContainer> getIterationTargetContainers(Variable v) {
		List<MovableContainer> targets = new ArrayList<>();
		if (v instanceof PrimitiveVariable && ((PrimitiveVariable) v).isIsList())
			targets.addAll(getTargetContainers(v, PrimitiveFOR.class));
		if (v instanceof ComplexVariable && ((ComplexVariable) v).isIsList()) {
			targets.addAll(getTargetContainers(v, FOR.class));
			targets.addAll(getTargetContainers(v, TableLoad.class));
			targets.addAll(getTargetContainers(v, ChoiceData.class));
		}
		return targets;
	}
	
	/**
	 * Returns the list of targeted components
	 * @param node
	 * @param cls
	 * @return
	 */
	private List<MovableContainer> getTargetContainers(Node node, Class<? extends Edge> cls) {
		return node.getOutgoing(cls).stream()
				.map(e -> (MovableContainer) e.getTargetElement())
				.collect(Collectors.toList());
	}
}
