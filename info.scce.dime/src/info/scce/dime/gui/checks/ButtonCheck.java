package info.scce.dime.gui.checks;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import graphmodel.Node;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.DataExtension;
import info.scce.dime.gui.gui.AddComplexToSubmission;
import info.scce.dime.gui.gui.AddPrimitiveToSubmission;
import info.scce.dime.gui.gui.Button;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.SIB;
import info.scce.dime.gui.helper.ComplexGUIBranchPort;
import info.scce.dime.gui.helper.GUIBranch;
import info.scce.dime.gui.helper.GUIBranchPort;
import info.scce.dime.gui.helper.GUIExtension;
import info.scce.dime.gui.helper.PrimitiveGUIBranchPort;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * The button check is used to validate the label
 * of the button components of a GUI model
 * @author zweihoff
 *
 */
public class ButtonCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;
	
	private GUIExtension guiex = new GUIExtension();
	private DataExtension dataex = DataExtension.getInstance();
	/**
	 * Validates all buttons placed in a given GUI model
	 */
	@Override 
	public void doExecute(GUIAdapter adapter) {
		this.adapter = adapter;
			
		//collect branched of embedded models (Interaction GUIPlugin GUISIB)
		Set<GUIBranch> embeddedBranches = new HashSet<>();
		for (GUIId id : adapter.getEntityIds()) {
			if (id.getElement() instanceof SIB) {
				embeddedBranches.addAll(guiex.getGUIBranchesMerged((SIB)id.getElement()));
			}
			
		}
		Map<String,Set<Button>> buttonLabelMap = new HashMap<String,Set<Button>>();
		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Button) {
				Button button = (Button) obj;
				checkButton(button);
				if(buttonLabelMap.containsKey(button.getLabel())){
					buttonLabelMap.get(button.getLabel()).add(button);
				}
				else{
					
					buttonLabelMap.put(button.getLabel(), new HashSet<Button>(Arrays.asList(button)));
				}
				checkEmbeddedBranchSignatur(id,button,embeddedBranches);
			}
		}
		checkButtonSignatur(buttonLabelMap);
		
	}
	
	private void checkEmbeddedBranchSignatur(GUIId id,Button button,Set<GUIBranch> embeddedBranches)
	{
		String branchName = GUIBranch.getBranchName(button);
		Optional<GUIBranch> branch = embeddedBranches.stream().filter((b)->b.getName().equals(branchName)).findAny();
		if(!branch.isPresent()) {
			return;
		}
		GUIBranch embeddedBranch = branch.get();
		//check each button in the GUI has same ports as embedded model branches
		List<EObject> ports = GUIBranchPort.getPorts(button).stream().map(GUIBranchPort::getPortNode).collect(Collectors.toList());
		ports.forEach(port->{
			String portName = GUIBranchPort.getPortName(port);
			//check port is present in embedded
			Optional<GUIBranchPort> portMatch = embeddedBranch.getPorts().stream().filter(p->p.getName().equals(portName)).findAny();
			if(portMatch.isPresent()){
				GUIBranchPort embeddedPort = portMatch.get();
				//check list status
				if(embeddedPort.isList() != GUIBranchPort.isList(port)) {
					addError(id, "port "+portName+" overrides incorrect by different list status");
				}
				//check port equal type
				if (embeddedPort instanceof ComplexGUIBranchPort && GUIBranchPort.isComplex(port)) {
					//both are complex
					
					Type portType = GUIBranchPort.getComplexType(port);
					Type embeddedPortType = ((ComplexGUIBranchPort)embeddedPort).getType();
					//types are equal
					if(!embeddedPortType.equals(portType)){
						//gui port type is a sub type of embedded port type
						if(!dataex.getSuperTypes(portType).contains(embeddedPortType)) {
							addError(id, "port "+portName+" overrides incorrect by different complextype");							
						}
					}
				}
				else if(embeddedPort instanceof PrimitiveGUIBranchPort && GUIBranchPort.isPrimitive(port)) {
					//both are primitive
					if(!((PrimitiveGUIBranchPort)embeddedPort).getType().equals(GUIBranchPort.getPrimitiveType(port))){
						addError(id, "port "+portName+" overrides incorrect by different primitive type");
					}
				} else {
					addError(id, "port "+portName+" overrides incorrect by different type");
				}
			} else {
				addError(id, "port "+portName+" is odd, overrides incorrect");
			}
		});
		//check missing ports
		embeddedBranch.getPorts().forEach(embeddedPort->{
			String embeddedPortName = embeddedPort.getName();
			Optional<EObject> portMatch = ports.stream().filter(n->GUIBranchPort.getPortName(n).equals(embeddedPortName)).findAny();
			if(!portMatch.isPresent()){
				addError(id, "port "+embeddedPortName+" is missing");
			}
		});
		
	}

	/**
	 * Checks, if buttons with the same label has the same signature.
	 * This means that all buttons have the same submission data binded to it.
	 * @param buttonLabelMap
	 */
	private void checkButtonSignatur(Map<String, Set<Button>> buttonLabelMap) {
		for(Entry<String, Set<Button>> entry:buttonLabelMap.entrySet().stream().filter(n->n.getValue().size()>1).collect(Collectors.toList()))
		{
			Button b = entry.getValue().stream().findFirst().get();
			//Check for primitive variables and attributes
			for(AddPrimitiveToSubmission edge:b.getIncoming(AddPrimitiveToSubmission.class))
			{
				String name = null;
				Node node = edge.getSourceElement();
				if(edge.getOutputName()!=null){
					if(!edge.getOutputName().isEmpty()){
						name = edge.getOutputName();
					}
				}
				String type = null;
				boolean isList = false;
				if(node instanceof PrimitiveVariable){
					if(name == null){
						name = ((PrimitiveVariable) node).getName();						
					}
					type = ((PrimitiveVariable) node).getDataType().getLiteral();
					isList = ((PrimitiveVariable) node).isIsList();
				}
				else if(node instanceof PrimitiveAttribute){
					if(name == null){
						name = ((PrimitiveAttribute) node).getAttribute().getName();						
					}
					type = ((PrimitiveAttribute) node).getAttribute().getDataType().getLiteral();
					isList = ((PrimitiveAttribute) node).getAttribute().isIsList();
				}
				for(Button otherButtons:entry.getValue()){
					boolean isMatching = false;
					for(AddPrimitiveToSubmission dataEdge:otherButtons.getIncoming(AddPrimitiveToSubmission.class))
					{
						Node data = dataEdge.getSourceElement();
						String compareName = null;
						if(dataEdge.getOutputName()!=null){
							if(!dataEdge.getOutputName().isEmpty()){
								compareName = edge.getOutputName();
							}
						}
						String compareType = null;
						boolean compareIsList = false;
						if(data instanceof PrimitiveVariable){
							if(compareName == null){
								compareName = ((PrimitiveVariable) data).getName();								
							}
							compareType = ((PrimitiveVariable) data).getDataType().getLiteral();
							compareIsList = ((PrimitiveVariable) data).isIsList();
						}
						else if(data instanceof PrimitiveAttribute){
							if(compareName == null){
								compareName = ((PrimitiveAttribute) data).getAttribute().getName();								
							}
							compareType = ((PrimitiveAttribute) data).getAttribute().getDataType().getLiteral();
							compareIsList = ((PrimitiveAttribute) data).getAttribute().isIsList();
						}
						//Test
						if(name.equals(compareName) && type.equals(compareType) && isList == compareIsList){
							isMatching = true;
							break;
						}
					}
					// no matching submission is found
					if(!isMatching) {
						addError(adapter.getIdByString(otherButtons.getId()), "Not the same signatur. "+name+" missmatch");
					}
				}
			}
			
			//Check for complex variables and attributes
			for(AddComplexToSubmission edge:b.getIncoming(AddComplexToSubmission.class))
			{
				Node node = edge.getSourceElement();
				String name = null;
				if(edge.getOutputName()!=null){
					if(!edge.getOutputName().isEmpty()){
						name = edge.getOutputName();
					}
				}
				Type type = null;
				boolean isList = false;
				if(node instanceof ComplexVariable){
					if(name == null){
						name = ((ComplexVariable) node).getName();						
					}
					type = ((ComplexVariable) node).getDataType();
					isList = ((ComplexVariable) node).isIsList();
				}
				else if(node instanceof ComplexAttribute){
					if(name == null){
						name = ((ComplexAttribute) node).getAttribute().getName();						
					}
					type = ((ComplexAttribute) node).getAttribute().getDataType();
					isList = ((ComplexAttribute) node).getAttribute().isIsList();
				}
				for(Button otherButtons:entry.getValue()){
					boolean isMatching = false;
					for(AddComplexToSubmission dataEdge:otherButtons.getIncoming(AddComplexToSubmission.class))
					{
						Node data = dataEdge.getSourceElement();
						String compareName = null;
						if(dataEdge.getOutputName()!=null){
							if(!dataEdge.getOutputName().isEmpty()){
								compareName = edge.getOutputName();
							}
						}
						Type compareType = null;
						boolean compareIsList = false;
						if(data instanceof ComplexVariable){
							if(compareName==null){
								compareName = ((ComplexVariable) data).getName();								
							}
							compareType = ((ComplexVariable) data).getDataType();
							compareIsList = ((ComplexVariable) data).isIsList();
						}
						else if(data instanceof ComplexAttribute){
							if(compareName == null){
								compareName = ((ComplexAttribute) data).getAttribute().getName();								
							}
							compareType = ((ComplexAttribute) data).getAttribute().getDataType();
							compareIsList = ((ComplexAttribute) data).getAttribute().isIsList();
						}
						//Test if compare type is type of any super type of type
						Type ct = dataex.getOriginalType(compareType);
						Optional<Type> hasSharedSuperType = dataex.getSuperTypes(type).stream().filter(n->dataex.isTypeOf(ct, n)).findAny();
						
						if(name.equals(compareName) && (type.equals(ct) || hasSharedSuperType.isPresent()) && isList == compareIsList){
							isMatching = true;
							break;
						}
					}
					// no matching submission is found
					if(!isMatching) {
						addError(adapter.getIdByString(otherButtons.getId()), "Not the same signatur. "+name+" missmatch");
					}
				}
			}
		}
		
	}
	

	@Override
	public void init() {}
	
	/**
	 * The label attribute of a button component has to be specified.
	 * It is checked whether it is not null nor empty.
	 * In addition to this, the label does not contain any Angular expressions.
	 * For any violation, errors are thrown
	 * @param button
	 */
	private void checkButton(Button button) {
		if (button.getLabel() == null || button.getLabel() == ""){
			addError(adapter.getIdByString(button.getId()), "Label undefined");			
		}
		else {
			if(button.getLabel().contains("{{")&&button.getLabel().contains("}}")){
				addError(adapter.getIdByString(button.getId()), "no expression in Label allowed, use displayLabel instead");	
			}
		}
			
	}
}
