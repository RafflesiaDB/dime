package info.scce.dime.gui.checks;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * The variable check is used to validate the input status of all varibales
 * in all data contexts of a GUI model
 * @author zweihoff
 *
 */
public class VariableCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if the input status of all varibales in the given GUI model
	 * is valid.
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof Variable)
				checkInputVariable(id, (Variable) obj);
		}
	}
	
	@Override
	public void init() {}

	/**
	 * Checks, if a given variable is an input variable and 
	 * than also top level variable
	 * @param id
	 * @param te
	 */
	private void checkInputVariable(GUIId id, Variable te) {
		//Check data Edges
		if(te.isIsInput()){
			if(!te.getIncoming(ComplexAttributeConnector.class).isEmpty()){
				addError(id, "cannot be an input variable");
			}
			if(!te.getIncoming(ComplexListAttributeConnector.class).isEmpty()){
				addError(id, "cannot be an input variable");
			}
		}
	}
}
