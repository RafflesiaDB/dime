package info.scce.dime.gui.checks;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.checks.AbstractCheck;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * The library component reference check is used to
 * validate all prime references present on any model element.
 * @author zweihoff
 *
 */
public class LibCompReferenceCheck extends AbstractCheck<GUIId, GUIAdapter> {

	/**
	 * Checks, if the library component references is valid
	 * for all model elements present in the given GUI model
	 */
	@Override
	public void doExecute(GUIAdapter adapter) {

		EObject eObj = adapter.getModel();

		if (eObj instanceof GraphModel == false)
			return;

		GraphModel gModel = (GraphModel) eObj;

		TreeIterator<EObject> it = gModel.eAllContents();
		while (it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof ModelElement == false)
				continue;
			ModelElement me = (ModelElement) obj;
			EStructuralFeature libCompFeature = me.eClass()
					.getEStructuralFeature("libraryComponentUID");
			if (libCompFeature != null) {
				Object val = me.eGet(libCompFeature);
				if (val == null) {
					addError(adapter.getIdByString(me.getId()),
							"Reference is NULL");
				}
			}
		}
	}
	
	@Override
	public void init() {}
}