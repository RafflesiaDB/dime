package info.scce.dime.gui.checks;

import java.util.ArrayList;
import java.util.Optional;

import info.scce.dime.api.DIMECheck;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserAttribute;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.process.mcam.adapter.ProcessId;

/**
 * The data model check, validates that all complex variables
 * and complex attributes which reference a type from a data model
 * are still consistent. 
 * @author zweihoff
 *
 */
public class DataModelCheck extends DIMECheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;

	private ArrayList<Attribute> brokenAttributes = new ArrayList<Attribute>();

	/**
	 * Collects all complex variables of all data contexts
	 * present in a given GUI adapter which are no lists.
	 */
	@Override
	public void doExecute(GUIAdapter adapter) {
		this.adapter = adapter;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof ComplexVariable) {
				ComplexVariable cv = (ComplexVariable) obj;
				if (!isComplexListVariable(cv)) {
					checkComplexVariable(id, cv);
				}
				checkNoUpdateOnExtensionAttributes(id, cv);
			}
		}
	}
	
	/**
	 * Prepares a list for all broken attributes
	 */
	@Override
	public void beforeCheck() {		
		brokenAttributes = new ArrayList<Attribute>();
	}

	private void checkNoUpdateOnExtensionAttributes(GUIId id,ComplexVariable cv) {
		//is attribute of complex type and is update
		if(!cv.getIncomingComplexAttributeConnectors().isEmpty() && !cv.getIncomingComplexWrites().isEmpty()) {
			//get root type
			ComplexVariable rootVar = cv.getIncomingComplexAttributeConnectors().get(0).getSourceElement();
			Optional<info.scce.dime.data.data.Attribute> optExt = _dataExtension.getInheritedAttributes(rootVar.getDataType()).stream().filter(n->n instanceof ExtensionAttribute).filter(n->n.getName().equals(cv.getName())).findFirst();
			if(optExt.isPresent()) {
				addError(id, "No updates allowed on Extension Attribute "+cv.getName());
			}
		}
	}
	
	/**
	 * Checks if the reference of the given complex variable is
	 * valid and than checks its attributes
	 * @param id
	 * @param cv
	 */
	private void checkComplexVariable(GUIId id, ComplexVariable cv) {
		Type rType = cv.getDataType();
		if (rType == null) {
			addError(id, "Type reference broken");
		} else {
			brokenAttributes = getBrokenAttributes(cv);
			checkAttributeReferences(id, cv, rType);
			checkCurrentUserVar(id, cv, rType);
		}
	}

	/**
	 * Checks, if a current user variable which is recognized by its name "currentUser"
	 * references a user type in a data model.
	 * @param id
	 * @param cv
	 * @param rType
	 */
	private void checkCurrentUserVar(GUIId id, ComplexVariable cv, Type rType) {
		if (!cv.getName().equals("currentUser"))
			return;
		
		boolean isUserVar = false;
		for (info.scce.dime.data.data.Attribute attribute : _dataExtension.getInheritedAttributes(rType)) {
			if (attribute instanceof UserAttribute)
				isUserVar = true;
		}
		if (!isUserVar)
			addError(id, "variable 'currentUser' has to be a user variable");
		
	}

	/**
	 * Checks the attributes of a given complex variables.
	 * All references are validated and it is checked whether no attribute
	 * names are present twice.
	 * @param id
	 * @param cv
	 * @param rType
	 */
	private void checkAttributeReferences(GUIId id, ComplexVariable cv, Type rType) {
		ArrayList<String> cvAttributesNames = getAttributeNames(cv);

		for (info.scce.dime.data.data.Attribute rAttr : _dataExtension.getInheritedAttributes(rType)) {
			int count = 0;
			String oAttrName = null;
			for (Attribute attr : cv.getAttributes()) {
				if (brokenAttributes.contains(attr))
					continue;

				if (attr instanceof PrimitiveAttribute) {
					info.scce.dime.data.data.PrimitiveAttribute refAttr = ((PrimitiveAttribute) attr)
							.getAttribute();
					if (_dataExtension.getOriginalAttribute(refAttr).getId()
							.equals(rAttr.getId())) {
						count++;
						oAttrName = refAttr.getName();
					}
				}
				if (attr instanceof ComplexAttribute) {
					info.scce.dime.data.data.ComplexAttribute refAttr = ((ComplexAttribute) attr)
							.getAttribute();
					if (_dataExtension.getOriginalAttribute(refAttr).getId()
							.equals(rAttr.getId())) {
						count++;
						oAttrName = refAttr.getName();
					}
				}
			}
			for (ComplexAttributeConnector cac : cv
					.getOutgoing(ComplexAttributeConnector.class)) {
				if (cac.getAssociationName().equals(rAttr.getName())) {
					count++;
					oAttrName = cac.getAssociationName();
				}
			}

			if (count > 1) {
				addError(id, "Attribute '" + rAttr.getName() + "' exists " + count
						+ " times");
			} else if (count <= 0) {
				// cvMsg.add("Attribute '" + rAttr.getName() + "' not found");
			} else {
				cvAttributesNames.remove(oAttrName);
			}
		}

		for (String attrName : cvAttributesNames) {
			addError(id, "Reference for '" + attrName + "' not found");
		}

		for (Attribute attr : brokenAttributes) {
			addError(adapter.getIdByString(attr.getId()), "Reference not found");
		}
	}

	/**
	 * Checks if the given complex variable is an expended complex 
	 * attribute of another complex variable. If the variable is an expanded
	 * attribute, it is compared to the referenced type in the data model.
	 * @param cv
	 * @return
	 */
	private boolean isComplexListVariable(ComplexVariable cv) {
		if (cv.getIncoming(ComplexAttributeConnector.class).size() > 0) {
			ComplexAttributeConnector cac = cv.getIncoming(
					ComplexAttributeConnector.class).get(0);
			String attrName = cac.getAssociationName();
			Type parentType = ((ComplexVariable)cac.getSourceElement()).getDataType();
			for (info.scce.dime.data.data.Attribute attribute : parentType
					.getAttributes()) {
				if (attribute.getName().equals(attrName)
						&& attribute.isIsList())
					return true;
			}
		}
		return false;
	}

	/**
	 * Returns the attributes of the given variable, which references to the attributes of
	 * a data model are invalid or broken
	 * @param cv
	 * @return
	 */
	private ArrayList<Attribute> getBrokenAttributes(ComplexVariable cv) {
		ArrayList<Attribute> brokenAttributes = new ArrayList<>();
		for (Attribute attr : cv.getAttributes()) {
			if (attr instanceof PrimitiveAttribute) {
				info.scce.dime.data.data.PrimitiveAttribute refAttr = ((PrimitiveAttribute) attr)
						.getAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
			if (attr instanceof ComplexAttribute) {
				info.scce.dime.data.data.ComplexAttribute refAttr = ((ComplexAttribute) attr)
						.getAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
		}
		return brokenAttributes;
	}

	/**
	 * Returns a list of the names of all attributes placed in the
	 * given complex variable and the expanded attributes recognized by
	 * complex attribute connector edges
	 */
	private ArrayList<String> getAttributeNames(ComplexVariable cv) {
		ArrayList<String> names = new ArrayList<>();
		for (Attribute cvAttr : cv.getAttributes()) {
			if (brokenAttributes.contains(cvAttr))
				continue;
			if (cvAttr instanceof PrimitiveAttribute) {
				info.scce.dime.data.data.PrimitiveAttribute refAttr = ((PrimitiveAttribute) cvAttr)
						.getAttribute();
				names.add(refAttr.getName());
			}
			if (cvAttr instanceof ComplexAttribute) {
				info.scce.dime.data.data.ComplexAttribute refAttr = ((ComplexAttribute) cvAttr)
						.getAttribute();
				names.add(refAttr.getName());
			}
		}
		for (ComplexAttributeConnector cac : cv
				.getOutgoing(ComplexAttributeConnector.class)) {
			names.add(cac.getAssociationName());
		}
		return names;
	}

}
