package info.scce.dime.gui.checks;

import java.util.Arrays;

import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.checks.AbstractCheck;

/**
 * The not allowed names check validates the names of all variables
 * 
 * @author zweihoff
 *
 */
public class NotAllowedNamesCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private String[] notAllowedVariableNames = {"iteratorElement"};
	
	/**
	 * Checks, if any variable has a not allowed name
	 */
	@Override
	public void doExecute(GUIAdapter adapter) {
		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Variable) {
				Variable var = (Variable) obj;
				if (Arrays.asList(notAllowedVariableNames).contains(var.getName()))
					addError(id, var.getName()
							+ " not allowed");
			}
		}
	}
	
	@Override
	public void init() {}
}
