package info.scce.dime.gui.checks;


import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * The image check is used to validate image components
 * @author zweihoff
 *
 */
public class FileCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if all images in the given GUI model are valid
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof File)
				check(id, (File) obj);
		}
	}
	
	@Override
	public void init() {}

	/**
	 * Check, if a given file component is valid.
	 * If a display edge is connected to the file the value of the edge source variable or attribute
	 * has to be a file.
	 * If no display edge is present a file guard process has to be present.
	 * @param id
	 * @param te
	 */
	private void check(GUIId id, File te) {
		//Check file guards
		if(te.getGuardSIBs().isEmpty()){
			addError(id, "Unguarded file access");
		}
		else{
			//guard availbale
			if(te.getGuardSIBs().size()>1){
				addError(id, "only one file guard is allowed");
			}
		}
		
	}
	
}
