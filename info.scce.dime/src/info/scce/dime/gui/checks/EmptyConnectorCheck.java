package info.scce.dime.gui.checks;

import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.checks.AbstractCheck;

/**
 * The empty connector check is used to validate the complex (list) attribute
 * connector edges, drawn between complex variables.
 * @author zweihoff
 *
 */
public class EmptyConnectorCheck extends AbstractCheck<GUIId, GUIAdapter> {
	
	/**
	 * Checks, if the association name attribute of all complex attribute connector edges
	 * is not empty and if the attribute name attribute of all complex list attribute connector edges
	 * is not empty
	 */
	@Override
	public void doExecute(GUIAdapter adapter) {
		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof ComplexAttributeConnector) {
				ComplexAttributeConnector con = (ComplexAttributeConnector) obj;
				checkComplexAttributeConnector(id, con);
			}
			if (obj instanceof ComplexListAttributeConnector) {
				ComplexListAttributeConnector con = (ComplexListAttributeConnector) obj;
				checkComplexListAttributeConnector(id, con);
			}
		}

	}
	
	@Override
	public void init() {}
	
	/**
	 * Checks, if the association name attribute of all complex attribute connector edges
	 * is not empty 
	 */
	private void checkComplexAttributeConnector(GUIId id, ComplexAttributeConnector con) {
		if (con.getAssociationName().equals(""))
			addError(id, "AssociationName is empty. Please correct!");
	}
	
	/**
	 * Checks, if the attribute name attribute of all complex list attribute connector edges
	 * is not empty
	 */
	private void checkComplexListAttributeConnector(GUIId id, ComplexListAttributeConnector con) {
		if (con.getAttributeName().equals(""))
			addError(id, "AttributeName is empty. Please correct!");
	}
	
}
