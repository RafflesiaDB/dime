package info.scce.dime.gui.checks;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import graphmodel.Node;
import info.scce.dime.api.DIMECheck;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.data.data.Type;
import info.scce.dime.gui.gui.Button;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexInputPort;
import info.scce.dime.gui.gui.ComplexListAttribute;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.ComplexListAttributeName;
import info.scce.dime.gui.gui.ComplexRead;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataContext;
import info.scce.dime.gui.gui.Descriptionentry;
import info.scce.dime.gui.gui.Dropdown;
import info.scce.dime.gui.gui.Embedded;
import info.scce.dime.gui.gui.FOR;
import info.scce.dime.gui.gui.FORSIB;
import info.scce.dime.gui.gui.Field;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.FormEntry;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.Headline;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.gui.Input;
import info.scce.dime.gui.gui.Iteration;
import info.scce.dime.gui.gui.Listentry;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.Panel;
import info.scce.dime.gui.gui.PrimitiveFOR;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Select;
import info.scce.dime.gui.gui.StaticContent;
import info.scce.dime.gui.gui.TODOList;
import info.scce.dime.gui.gui.Tab;
import info.scce.dime.gui.gui.Table;
import info.scce.dime.gui.gui.TableEntry;
import info.scce.dime.gui.gui.TableLoad;
import info.scce.dime.gui.gui.Text;
import info.scce.dime.gui.gui.TextInputStatic;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.process.checks.ProcessUtils;
import info.scce.dime.process.process.ComplexAttributeConnector;

/**
 * The expression check is used to validate all Angular expressions
 * present in the textual attributes of all components.
 * The Angular expression correspond to the variables in the data contexts.
 * The attribute access is signed by a point junction.
 * @author zweihoff
 *
 */
public class ExpressionCheck extends DIMECheck<GUIId, GUIAdapter> {
	private GUIAdapter adapter;

	/**
	 * Attribute names that are implicitly accessible
	 */
	private String[] basicNames = { "dywa_id", "dywa_name", "dywa_version", "size", "last", "first"};

	private ProcessUtils processUtils = new ProcessUtils();

	/**
	 * Checks all textual attributes of all components
	 * that has to be considered, because they can be extended by expressions
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof MovableContainer)
				checkMovableContainer(id, (MovableContainer) obj);
			if (obj instanceof Text)
				checkText(id, (Text) obj);
			if (obj instanceof Headline)
				checkHeadline(id, (Headline) obj);
			if (obj instanceof Listentry)
				checkListentry(id, (Listentry) obj);
			if (obj instanceof Descriptionentry)
				checkDescriptionentry(id, (Descriptionentry) obj);
			if (obj instanceof Form)
				checkForm(id, (Form) obj);
			if (obj instanceof Panel)
				checkPanel(id, (Panel) obj);
			if (obj instanceof Button)
				checkButton(id, (Button) obj);
			if (obj instanceof TableEntry)
				checkTableEntry(id, (TableEntry) obj);
			if (obj instanceof TODOList)
				checkTODOList(id, (TODOList) obj);
			if (obj instanceof Dropdown)
				checkDropdown(id, (Dropdown) obj);
			if (obj instanceof Image)
				checkImage(id, (Image) obj);
			if (obj instanceof FormEntry)
				checkFormEntry(id, (FormEntry) obj);
			if (obj instanceof Input)
				checkInput(id, (Input) obj);
			if (obj instanceof Embedded)
				checkEmbedded(id, (Embedded) obj);
			if (obj instanceof Tab)
				checkTab(id, (Tab) obj);
			if(obj instanceof TextInputStatic) {
				checkInput(id, (TextInputStatic) obj);
			}

		}
	}
	
	/**
	 * Checks the source expressions of the embedded component
	 * @param id
	 * @param obj
	 */
	private void checkEmbedded(GUIId id, Embedded obj) {
		if(obj.getSource() != null){
			checkExpressionString(id, obj, obj.getSource(),
					obj.getRootElement());
		}
		
	}
	
	/**
	 * Checks the label expressions of the tab component
	 * @param id
	 * @param obj
	 */
	private void checkTab(GUIId id, Tab obj) {
		if(obj.getLabel() != null){
			checkExpressionString(id, obj, obj.getLabel(),
					obj.getRootElement());
		}
		
	}
	
	/**
	 * Checks the label expressions of the text input static
	 * @param id
	 * @param obj
	 */
	private void checkInput(GUIId id, TextInputStatic obj) {
		if(obj.getValue() != null){
			checkExpressionString(id, obj, obj.getValue(),
					obj.getRootElement());
		}
		
	}

	/**
	 * Checks the label expressions of the dropdown component
	 * @param id
	 * @param obj
	 */
	private void checkDropdown(GUIId id, Dropdown obj) {
		if(obj.getLabel() != null){
			checkExpressionString(id, obj, obj.getLabel(),
					obj.getRootElement());
		}
		
	}	

	/**
	 * Checks the tooltip and general style expressions of any movable component
	 * @param id
	 * @param obj
	 */
	private void checkMovableContainer(GUIId id, MovableContainer obj) {
		if(obj.getGeneralStyle() != null){
			checkExpressionString(id, obj, obj.getGeneralStyle().getRawContent(),
					obj.getRootElement());
			checkStyle(id,obj,obj.getGeneralStyle().getRawContent());
			if (obj.getGeneralStyle().getTooltip() != null)
				
				for (StaticContent sc : obj.getGeneralStyle().getTooltip().getContent()) {
					checkExpressionString(id, obj, sc.getRawContent(),
							obj.getRootElement());
				}
			
		}
	}
	
	private void checkStyle(GUIId id, MovableContainer obj, String rawContent) {
		//split in single statements
		if(rawContent!=null){
			if(!rawContent.trim().isEmpty()){
				String[] statements = rawContent.split(";");
				for(int i=0;i<statements.length;i++)
				{
					if(!statements[i].trim().isEmpty()){
						String[] parts = statements[i].trim().split(":");
						if(parts.length!=2){
							addError(id, "Invalid styling: "+statements[i]);
						}
					}
				}
				
			}
			
		}
		
	}

	/**
	 * Checks the error message expressions of the form component
	 * @param id
	 * @param obj
	 */
	private void checkForm(GUIId id, Form obj) {
		if (obj.getErrorMessage() != null)
		{
				checkExpressionString(id, obj, obj.getErrorMessage(),
						obj.getRootElement());
		}
	}
	
	/**
	 * Checks the display label expressions of the button component
	 * @param id
	 * @param obj
	 */
	private void checkButton(GUIId id, Button obj) {
		if(obj.getDisplayLabel() != null){
			checkExpressionString(id, obj, obj.getDisplayLabel(),
					obj.getRootElement());
		}
		if(obj.getOptions()!=null){
			checkExpressionString(id, obj, obj.getOptions().getStaticURL(),
					obj.getRootElement());
			
		}
	}
	
	/**
	 * Checks the error text, empty value and help text expressions of the input component
	 * @param id
	 * @param obj
	 */
	private void checkInput(GUIId id, Input obj) {
		if(obj.getValidation() != null){
			if(obj.getValidation().getErrorText() != null){
				checkExpressionString(id, obj, obj.getValidation().getErrorText(),
						obj.getRootElement());
			}
		}
		if(obj.getAdditionalOptions() != null) {
			if(obj.getAdditionalOptions().getEmptyValue() != null){
				checkExpressionString(id, obj, obj.getAdditionalOptions().getEmptyValue(),
						obj.getRootElement());
			}
			
			if(obj.getAdditionalOptions().getHelpText() != null){
				checkExpressionString(id, obj, obj.getAdditionalOptions().getHelpText(),
						obj.getRootElement());
			}
		}
	}
	
	/**
	 * Checks the label expressions of the form entry component (file, field and select)
	 * @param id
	 * @param obj
	 */
	private void checkFormEntry(GUIId id, FormEntry obj) {
		if(obj instanceof File){
			if(((File) obj).getLabel() != null){
				checkExpressionString(id, obj, ((File) obj).getLabel(),
						obj.getRootElement());
			}
		}
		if(obj instanceof Field){
			if(((Field) obj).getLabel() != null){
				checkExpressionString(id, obj, ((Field) obj).getLabel(),
						obj.getRootElement());
			}
		}
		if(obj instanceof Select){
			
			if(((Select) obj).getLabel() != null){
				checkExpressionString(id, obj, ((Select) obj).getLabel(),
						obj.getRootElement());
			}
		}
	}
	
	/**
	 * Checks the path expressions of the image component
	 * @param id
	 * @param obj
	 */
	private void checkImage(GUIId id, Image obj) {
		if(obj.getPath() != null){
			checkExpressionString(id, obj, obj.getPath(),
					obj.getRootElement());
			if(obj.getPath().contains("{{") && obj.getPath().contains("}}")){
				if(!(obj.getPath().startsWith("{{") && obj.getPath().endsWith("}}"))){
					addError(id, "Invalid Expression");
				}
			}
		}
	}
	
	/**
	 * Checks the label expressions of the table component
	 * @param id
	 * @param obj
	 */
	private void checkTableEntry(GUIId id, TableEntry obj) {
		if(obj.getLabel() != null){
			checkExpressionString(id, obj, obj.getLabel(),
					obj.getRootElement());
		}
	}
	
	/**
	 * Checks the empty value and caption expressions of the todo list component
	 * @param id
	 * @param obj
	 */
	private void checkTODOList(GUIId id, TODOList obj) {
		if(obj.getEmptyValue() != null){
			checkExpressionString(id, obj, obj.getEmptyValue(),
					obj.getRootElement());
		}
		if(obj.getCaption() != null){
			checkExpressionString(id, obj, obj.getCaption(),
					obj.getRootElement());
		}
	}

	/**
	 * Checks the heading and footer expressions of the panel component
	 * @param id
	 * @param obj
	 */
	private void checkPanel(GUIId id, Panel obj) {
		checkStaticContent(id, obj.getHeading(), obj);
		checkStaticContent(id, obj.getFooter(), obj);
	}


	/**
	 * Checks the content and description expressions of the description entry component
	 * @param id
	 * @param obj
	 */
	private void checkDescriptionentry(GUIId id, Descriptionentry obj) {
		checkStaticContent(id, obj.getContent(), obj);
		checkStaticContent(id, obj.getDescription(), obj);
	}

	/**
	 * Checks the content expressions of the list entry component
	 * @param id
	 * @param obj
	 */
	private void checkListentry(GUIId id, Listentry obj) {
		checkStaticContent(id, obj.getContent(), obj);
	}

	/**
	 * Checks the content expressions of the headline component
	 * @param id
	 * @param obj
	 */
	private void checkHeadline(GUIId id, Headline obj) {
		checkStaticContent(id, obj.getContent(), obj);
	}

	/**
	 * Checks the text expressions of the text component
	 * @param id
	 * @param obj
	 */
	private void checkText(GUIId id, Text obj) {
		checkStaticContent(id, obj.getContent(), obj);
	}
	
	/**
	 * Checks the raw content expressions of any movable component
	 * @param id
	 * @param obj
	 */
	private void checkStaticContent(GUIId id,List<StaticContent> contents,MovableContainer mc){
		for (StaticContent sc : contents) {
			/**
			 * If the HTML attribute is enabled, only one expression is allowed and nothing else.
			 * The value behind the expression is injected in the DOM without escaping
			 */
			if(sc.isHtml()){
				int countOpenMustages = sc.getRawContent().length() - sc.getRawContent().replace("}}", "").length();
				int countCloseMustages = sc.getRawContent().length() 	- sc.getRawContent().replace("{{", "").length();
				if(countOpenMustages!=2&&countCloseMustages!=2){
					addError(id, "static content should only consist of one expression");
				}
				if(!(sc.getRawContent().startsWith("{{")&&sc.getRawContent().endsWith("}}"))){
					addError(id, "static content should only consist of one expression");
				}
				
			}
			checkExpressionString(id, mc, sc.getRawContent(),
					mc.getRootElement());
		}
	}

	/**
	 * Validates if a given variable and expression matches
	 * @param id
	 * @param variable
	 * @param expression
	 */
	private void checkVariable(GUIId id, Variable variable, String expression) {
		if (variable instanceof PrimitiveVariable && !expression.isEmpty())
			if(variable.isIsList()){
				if(!(expression.equals("first")||expression.equals("last")||(expression.equals("size")))){
					addError(id, "expression {{"+expression+"}} does not match to primitive list attribute '"
							+ variable.getName() + "'");		
				}
			}
			else{
				addError(id, "expression does not match to primitive variable '"
						+ variable.getName() + "'");				
			}
		else if (variable instanceof ComplexVariable && expression.isEmpty()) {
			if(!(((ComplexVariable)variable).getDataType() instanceof EnumType)) {
					addError(id,
							"empty expression does not match to complex variable '"
									+ variable.getName() + "'");				
			}
		}
		else if (variable instanceof ComplexVariable && !expression.isEmpty())
			checkExpression(id, expression,
					((ComplexVariable) variable).getDataType());
	}
	
	/**
	 * Validates if a complex attribute and expression matches
	 * @param id
	 * @param variable
	 * @param expression
	 */
	private void checkListAttribute(GUIId id, ComplexListAttribute variable, String expression) {
		if (expression.isEmpty())
			addError(id,
					"empty expression does not match to complex list attribute '"
							+ "current" + "'");
		else if (!expression.isEmpty())
			checkExpression(id, expression,
					variable.getListType());
	}

	/**
	 * Validates if a given node and expression matches.
	 * @param id
	 * @param variable
	 * @param expression
	 */
	private void checkExpressionString(GUIId id, Node node, String content,
			GUI gui) {
		
		if (content == null) {
			return;
		}

		Pattern pattern = processUtils.getExpressionStringPattern();
		Matcher matcher = pattern.matcher(content);
		
		while (matcher.find()) {
			// System.out.println("g0: " + matcher.group(0));
			// System.out.println("g1: " + matcher.group(1));
			// System.out.println("g2: " + matcher.group(2));

			String expressionString = matcher.group(2);
			if(expressionString.contains("|")){
				int index = expressionString.indexOf("|");
				expressionString = expressionString.substring(0, index);
			}
			int firstDotPos = expressionString.indexOf(".");

			String varName = "";
			String expression = "";
			if (firstDotPos > 0) {
				varName = expressionString.substring(0, firstDotPos);
				expression = expressionString.substring(firstDotPos + 1);
			} else {
				varName = expressionString;
			}
			boolean found = false;
			
			if (varName.contains("[") && varName.contains("]"))
				varName = varName.substring(0, varName.indexOf("["));

			varName = varName.trim();
			
//			System.out.println("==============================");
//			System.out.println("Id: " + id);
//			System.out.println("VarName: " + varName);
//			System.out.println("exp: " + expression);
			
			
			

			// check FOR hierarchy
			Node parentNode = node;
			while (parentNode != null) {
				for (Iteration iterationEdge : parentNode
						.getIncoming(Iteration.class)) {
					if (iterationEdge instanceof PrimitiveFOR) {
						PrimitiveFOR pFor = (PrimitiveFOR) iterationEdge;
						if (varName.equals(pFor.getIterator())){
							found = true;
						}
						if (varName.equals(pFor.getIndex())){
							found = true;
						}
					} else if (iterationEdge instanceof FOR) {
						Node sourceNode = iterationEdge.getSourceElement();
						if (sourceNode instanceof ComplexVariable) {
							ComplexVariable cVar = (ComplexVariable) sourceNode;
							if (!cVar.isIsList())
								continue;
							if (!varName.equals(iterationEdge.getIndex())) {
								if (!cVar.getOutgoing(ComplexListAttributeConnector.class).isEmpty()) {
									ComplexVariable cListVar = cVar
											.getOutgoing(
													ComplexListAttributeConnector.class)
											.get(0).getTargetElement();
									if (!varName.equals(cListVar.getName()))
										continue;
								} else {
									if(!varName.equals("current")){
										continue;
									}
								}
							}
							

							found = true;
							checkExpression(id, expression, cVar.getDataType());
						}
					}
				}
				if(parentNode instanceof FORSIB){
					FORSIB forSib = (FORSIB) parentNode;
					if(!(forSib.getComplexInputPorts().isEmpty())){
						ComplexInputPort ccip = forSib.getComplexInputPorts().get(0);
						for (ComplexRead iterationEdge : ccip.getIncoming(ComplexRead.class)) {
								Node sourceNode = iterationEdge.getSourceElement();
								if (sourceNode instanceof ComplexVariable) {
									ComplexVariable cVar = (ComplexVariable) sourceNode;
									if (!cVar.isIsList()){
										continue;										
									}
									if (!cVar.getOutgoing(ComplexListAttributeConnector.class).isEmpty()) {
										ComplexVariable cListVar = cVar
												.getOutgoing(
														ComplexListAttributeConnector.class)
												.get(0).getTargetElement();
										if (!varName.equals(cListVar.getName()))
											continue;
									}else{
										if(varName.equals("current")){
											continue;
										}
									}
									
									
									found = true;
									checkExpression(id, expression, cVar.getDataType());
								}
								if (sourceNode instanceof ComplexAttribute) {
									ComplexAttribute cAttr = (ComplexAttribute) sourceNode;
									if (!cAttr.getAttribute().isIsList()){
										continue;
									}
									ComplexVariable cListVar = (ComplexVariable) cAttr.getContainer();
									if (!varName.equals(cListVar.getName())){
										continue;
									}
									found = true;
									checkExpression(id, expression, cAttr.getAttribute().getDataType());
								}
						}
					}
				}

				if (found)
					break;

				if (parentNode.getContainer() instanceof Node) {
					parentNode = (Node) parentNode.getContainer();
				} else {
					parentNode = null;
				}
			}
			
			// check TableLoad hierarchy
			parentNode = node;
			while (parentNode != null) {
				if (parentNode instanceof Table) {
					for (TableLoad tl : parentNode.getIncoming(TableLoad.class)) {
						Node sourceNode = tl.getSourceElement();
						if (sourceNode instanceof Variable) {
							Variable var = (Variable) sourceNode;
							if (!var.isIsList()) {
								if (var.getName().equals(varName)) {
									found = true;
									checkVariable(id, var, expression);
								}
									
							} else {
								if (var instanceof ComplexVariable) {
									if(!var.getOutgoing(ComplexListAttributeConnector.class).isEmpty()) {
										for(ComplexListAttributeConnector clac : var.getOutgoing(ComplexListAttributeConnector.class)){
											ComplexVariable currentVar = (ComplexVariable) clac.getTargetElement();
											if (currentVar.getName().equals(varName)) {
												found = true;
												checkVariable(id, currentVar, expression);
											}										
											
										}
									
										
									}
									//Check for ComplexListAttributes
									for(info.scce.dime.gui.gui.Attribute attr:((ComplexVariable) var).getAttributes()){
										if(attr instanceof ComplexListAttribute){
											if(((ComplexListAttribute) attr).getAttributeName() == ComplexListAttributeName.CURRENT && varName.equals("current")){
												found = true;
												checkListAttribute(id, (ComplexListAttribute) attr, expression);													
											}
										}
									}
								}
							}
						}
					}
				}
				
				if (found)
					break;

				if (parentNode.getContainer() instanceof Node) {
					parentNode = (Node) parentNode.getContainer();
				} else {
					parentNode = null;
				}
			}

			// check primary variables in data contexts
			for (DataContext dataContext : gui.getDataContexts()) {
				for (Variable variable : dataContext.getVariables()) {
					if (!variable.getIncoming(info.scce.dime.gui.gui.ComplexAttributeConnector.class)
							.isEmpty())
						continue;
					if (!variable.getIncoming(
							ComplexListAttributeConnector.class).isEmpty())
						continue;

					if (!varName.equals(variable.getName()))
						continue;

					found = true;
					checkVariable(id, variable, expression);
				}
			}
			
			//check top level variable access
			if(!found){
				List<Variable> topLevelVars = gui.
						getDataContexts().
						stream().
						flatMap(n->n.
								getVariables().
								stream().
								filter(e->(e.getIncoming(ComplexAttributeConnector.class).isEmpty() && e.getIncoming(ComplexListAttributeConnector.class).isEmpty()) )
								).collect(Collectors.toList());
				
				List<String> topLevelVarNames = topLevelVars.stream().map(n->n.getName()).collect(Collectors.toList());
				if(!topLevelVarNames.contains(varName))
				{
					found = false;
				}				
			}

			if (!found)
				addError(id, "no variable found for '" + varName + "'");
		}
	}

	/**
	 * Checks if a given expression can be parsed depended on the 
	 * given data type. In addition pipes are validated.
	 * @param id
	 * @param expression
	 * @param dataType
	 */
	private void checkExpression(GUIId id, String expression, Type dataType) {
		Type actualType = dataType;
		//Check No Spaces
		if(expression.contains(" ")){
			//addError(id, "No spaces allowed in expressions");
		}
		
		//Check Pipes
		if(expression.contains("|")){
			int index = expression.indexOf("|");
			String pipe = expression.substring(index).replaceAll(" ", "");
			if(pipe.startsWith("|date:'")){
				if(!pipe.endsWith("'")){
					addError(id, "Invalid date pipe");
				}
			}
			else if(pipe.startsWith("|number:'")){
				if(!pipe.endsWith("'")){
					addError(id, "Invalid number pipe");
				}
			}
			else if(pipe.startsWith("|percent:'")){
				if(!pipe.endsWith("'")){
					addError(id, "Invalid percent pipe");
				}
			}
			else if(pipe.startsWith("|replace:'")){
				if(!pipe.endsWith("'")){
					addError(id, "Invalid replace pipe");
				}
			}
			else if(pipe.startsWith("|uppercase")){
				if(pipe.contains(":")||pipe.contains("'")){
					addError(id, "Invalid uppercase pipe");
				}
			}
			else if(pipe.startsWith("|seconds")){
				if(pipe.contains(":")||pipe.contains("'")){
					addError(id, "Invalid seconds pipe");
				}
			}
			else if(pipe.startsWith("|lowercase")){
				if(pipe.contains(":")||pipe.contains("'")){
					addError(id, "Invalid lowercase pipe");
				}
			}

			else{
				addError(id, "Allowed pipes are: date, uppercase, uppercase, lowercase, number, replace, percent, seconds");
			}
			expression=expression.substring(0, index).trim();
		}

		Pattern pattern = processUtils.getExpressionPattern();
		Matcher matcher = pattern.matcher(expression);
		Attribute foundAttr = null;
		while (matcher.find()) {
			boolean found = false;
			String varName = matcher.group(1).trim();
			boolean isList = (matcher.group(3) != null) ? true : false;

			List<Attribute> attributes = _dataExtension.getInheritedAttributes(actualType);
			for (Attribute attribute : attributes) {

				if (!attribute.getName().equals(varName)) {
					if ("fileName".equals(varName)
							&& foundAttr != null
							&& foundAttr instanceof PrimitiveAttribute
							&& ((PrimitiveAttribute)foundAttr).getDataType() == PrimitiveType.FILE) {
						// do nothing here, just don't continue
					}
					else continue;
				}

				found = true;
				foundAttr = attribute;
				if (!attribute.isIsList() && isList)
					addError(id,
							"list property differs. " + attribute.getName()
									+ " isList = " + attribute.isIsList());

				if (attribute instanceof info.scce.dime.data.data.ComplexAttribute) {
					info.scce.dime.data.data.ComplexAttribute cAttr = (info.scce.dime.data.data.ComplexAttribute) attribute;
					actualType = cAttr.getDataType();
				}
				if (attribute instanceof info.scce.dime.data.data.ExtensionAttribute) {
					Type t = _dataExtension.getComplexExtensionAttributeType((ExtensionAttribute) attribute);
					if(t!=null) {
						actualType = t;						
					}
				}
			}
			if (!found && !Arrays.asList(basicNames).contains(varName))
				addError(
						id,
						varName + " not found in hierarchy of "
								+ dataType.getName());
		}
	}
}
