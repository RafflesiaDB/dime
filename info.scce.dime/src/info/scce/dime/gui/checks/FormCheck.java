package info.scce.dime.gui.checks;

import graphmodel.Node;
import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.gui.gui.Checkbox;
import info.scce.dime.gui.gui.ChoiceData;
import info.scce.dime.gui.gui.Combobox;
import info.scce.dime.gui.gui.ComplexAttribute;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexChoiceData;
import info.scce.dime.gui.gui.ComplexListAttributeConnector;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataBinding;
import info.scce.dime.gui.gui.Display;
import info.scce.dime.gui.gui.Field;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.FormEntry;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.gui.PrimitiveChoiceData;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Radio;
import info.scce.dime.gui.gui.Select;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * The form check is used to validate a form component and all the form field components
 * placed anywhere in the form.
 * @author zweihoff
 *
 */
public class FormCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;

	/**
	 * Checks, if the select, field or choice data components are valid
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Select) {
				checkSelect(id, (Select) obj);
				checkRootForm(id, (Select) obj);				
			}
			if (obj instanceof Field) {
				checkField(id, (Field) obj);
				checkRootForm(id, (Field) obj);				
			}
			if (obj instanceof ChoiceData)
				checkChoiceData(id, (ChoiceData) obj);
		}
	}
	
	@Override
	public void init() {}
	
	/**
	 * Checks, if a given form entry is surrounded by at least one form component
	 * @param id
	 * @param formEntry
	 */
	private void checkRootForm(GUIId id, FormEntry formEntry) {
		MovableContainer mc = formEntry;
		boolean rootFormFound = false;
		while(mc.getContainer() instanceof MovableContainer){
			mc = (MovableContainer) mc.getContainer();
			if(mc instanceof Form){
				rootFormFound = true;
				break;
			}
		}
		if(!rootFormFound){
			addError(id, "no root form found for input");
		}
	}

	/**
	 * Checks, if the source variable or attribute if the given
	 * choice data edge matches the target select form field component.
	 * This includes the list status and data type
	 * @param id
	 * @param choiceData
	 */
	private void checkChoiceData(GUIId id, ChoiceData choiceData) {
		Node source = choiceData.getSourceElement();

		if (choiceData instanceof PrimitiveChoiceData) {
			if (source instanceof PrimitiveVariable) {
				if (!((PrimitiveVariable) source).isIsList())
					addError(id, "source has to be a list");
			} else if (source instanceof PrimitiveAttribute) {
				if (!((PrimitiveAttribute) source).getAttribute().isIsList())
					addError(id, "source has to be a list");
			} else {
				addError(id, "wrong type of source node");
			}

		} else if (choiceData instanceof ComplexChoiceData) {

			if (source instanceof ComplexVariable) {
				if (!((ComplexVariable) source).isIsList())
					addError(id, "source has to be a list");

			} else if (source instanceof ComplexAttribute) {
				if (!((ComplexAttribute) source).getAttribute().isIsList())
					addError(id, "source has to be a list");

			} else {
				addError(id, "wrong type of source node");
			}

		}
	}

	/**
	 * Checks if a given form field component input type
	 * matches the binded variable or attribute, so that
	 * a conversion between the inserted textual value and the data type
	 * can be computed.
	 * @param id
	 * @param field
	 */
	private void checkField(GUIId id, Field field) {
		if (field.getIncoming(DataBinding.class).size() < 1) {
			if (field.isDisabled()) {
				if (field.getAdditionalOptions() == null
						|| field.getAdditionalOptions().getEmptyValue() == null
						|| field.getAdditionalOptions().getEmptyValue().isEmpty()) {
					addError(id, "missing empty value or data binding edge");
				}
			} else {
				addError(id, "missing data binding edge");
			}
		} else if (field.getIncoming(DataBinding.class).size() > 1) {
			addError(id, "too many data binding edges");
		} else {
			Node node = field.getIncoming(DataBinding.class).get(0)
					.getSourceElement();
			String pTypeName = null;
			if (node instanceof PrimitiveAttribute) {
				pTypeName = ((PrimitiveAttribute) node).getAttribute()
						.getDataType().getLiteral().toUpperCase();
			} else if (node instanceof PrimitiveVariable) {
				pTypeName = ((PrimitiveVariable) node).getDataType()
						.getLiteral().toUpperCase();
			} else {
				addError(id, "field is not connected to a primitive type");
			}

			if (pTypeName != null) {
				boolean validType = true;
				switch (field.getInputType()) {
				case CHECKBOX:
					if (pTypeName.equals("BOOLEAN"))
						break;
					validType = false;
					break;
				case COLOR:
					if (pTypeName.equals("TEXT"))
						break;
					validType = false;
					break;
				case DATE:
					if (pTypeName.equals("TIMESTAMP"))
						break;
					validType = false;
					break;
				case DATETIME:
					if (pTypeName.equals("TIMESTAMP"))
						break;
					validType = false;
					break;
				case SIMPLE_FILE: {
					if(!field.getIncomingFormLoadSubmits().isEmpty()) {
						//not allowed because of browser security
						addError(id, "forml load submit not allowed on file input");
					}
					if (pTypeName.equals("FILE"))
						break;
					validType = false;

					break;
				}
					
				case ADVANCED_FILE: {
					if(!field.getIncomingFormLoadSubmits().isEmpty()) {
						//not allowed because of browser security
						addError(id, "forml load submit not allowed on file input");
					}
					if (pTypeName.equals("FILE"))
						break;
					validType = false;
					break;
				}
					
					
				case MONTH:
					if (pTypeName.equals("TIMESTAMP"))
						break;
					validType = false;
					break;
				case NUMBER:
					if (pTypeName.equals("REAL"))
						break;
					if (pTypeName.equals("INTEGER"))
						break;
					validType = false;
					break;
				case PASSWORD:
					if (pTypeName.equals("TEXT"))
						break;
					validType = false;
					break;
				case TEL:
					if (pTypeName.equals("TEXT"))
						break;
					validType = false;
					break;
				case TEXT:
					if (pTypeName.equals("TEXT"))
						break;
					validType = false;
					break;
				case TIME:
					if (pTypeName.equals("TIMESTAMP"))
						break;
					validType = false;
					break;
				case URL:
					if (pTypeName.equals("TEXT"))
						break;
					validType = false;
					break;
				case WEEK:
					if (pTypeName.equals("TIMESTAMP"))
						break;
					validType = false;
					break;
				case EMAIL:
					if (pTypeName.equals("TEXT"))
						break;
					validType = false;
					break;
				case TEXT_AREA:
					if (pTypeName.equals("TEXT"))
						break;
					validType = false;
					break;
				default:
					throw new IllegalStateException("unknown type of input in "
							+ field.getLabel());
				}

				if (!validType)
					addError(id, pTypeName + " not valid for field '"
							+ field.getLabel() + "' with input type "
							+ field.getInputType().getLiteral());
			}
		}
	}

	/**
	 * Checks, if a given select form component is valid.
	 * This includes, if a data binding edge and choice data edge are present
	 * and if the select component options and specialized type like e.g. radio,
	 * checkbox,.. match the selected variable or attribute list status.
	 * In addition a display edge has to be provided the set the displayed the
	 * entries.
	 * @param id
	 * @param select
	 */
	private void checkSelect(GUIId id, Select select) {
		if (select.getIncoming(DataBinding.class).size() < 1) {
			addError(id, "missing data binding");
			return;	
		} else if(select.getIncoming(Display.class).isEmpty()){
			if(select.getIncoming(PrimitiveChoiceData.class).isEmpty()){
				addError(id, "missing display edge");				
				return;
			}
		} else {
			// check multiplicity
			for (DataBinding bind : select.getIncoming(DataBinding.class)) {
				Node sourceNode = bind.getSourceElement();
				boolean targetIsList = false;
				if (sourceNode instanceof Variable) {
					targetIsList = ((Variable) sourceNode).isIsList();
				} else if (sourceNode instanceof PrimitiveAttribute) {
					targetIsList = ((PrimitiveAttribute) sourceNode)
							.getAttribute().isIsList();
				} else if (sourceNode instanceof ComplexAttribute) {
					targetIsList = ((ComplexAttribute) sourceNode)
							.getAttribute().isIsList();
				}
				if(select instanceof Checkbox){
					if(!targetIsList){
						addError(id, "Checkbox option mismatch. target is not a list");
					}
				}
				if(select instanceof Combobox){
					if(targetIsList!=((Combobox)select).isMultiple()){
						addError(id, "Combobox multiple option mismatch.");
					}
				}
				if(select instanceof Radio){
					if (targetIsList)
						addError(id, "Radio target is a list");					
				}
			}
			//check display is on the choice data access path
			for(Display display:select.getIncoming(Display.class))
			{
				Variable var = null;
				boolean isOnAccessPath = false;
				if(display.getSourceElement() instanceof Variable){
					var = (Variable) display.getSourceElement();
				}
				else{
					var = (Variable) display.getSourceElement().getContainer();
				}
				while(var != null)
				{
					if(var.getOutgoing(ChoiceData.class).stream().filter(n->n.getTargetElement().getId().equals(select.getId())).findAny().isPresent())
					{
						isOnAccessPath = true;
						break;
					}
					if(var instanceof ComplexVariable) {
						for(info.scce.dime.gui.gui.Attribute atrr:((ComplexVariable) var).getAttributes()){
							if(atrr.getOutgoing(ChoiceData.class).stream().filter(n->n.getTargetElement().getId().equals(select.getId())).findAny().isPresent())
							{
								isOnAccessPath = true;
								break;
							}
						}
						
					}
					
					if(!var.getIncoming(ComplexAttributeConnector.class).isEmpty()){
						var = (Variable) var.getIncoming(ComplexAttributeConnector.class).get(0).getSourceElement();
					}
					else if(!var.getIncoming(ComplexListAttributeConnector.class).isEmpty()) {
						var = (Variable) var.getIncoming(ComplexListAttributeConnector.class).get(0).getSourceElement();
					}
					else{
						var = null;
					}
					
				}
				if(!isOnAccessPath){
					addError(id, "display edge is not on choice data");
				}
			}
		}

		// check choice data
		if (select.getIncoming(ChoiceData.class).size() < 1) {

			addError(id, "missing choice data");
		}

		// check type compability
		checkSelectTypeCompapility(id, select);
	}

	/**
	 * Checks, if a given select form component is valid.
	 * This includes, if a data binding edge and choice data edge are 
	 * of the same data type.
	 * entries.
	 * @param id
	 * @param select
	 */
	private void checkSelectTypeCompapility(GUIId id, Select select) {
		if (select.getIncoming(ChoiceData.class).size() > 0) {
			Node choiceNode = select.getIncoming(ChoiceData.class).get(0)
					.getSourceElement();
			Node targetNode = select.getIncoming(DataBinding.class).get(0)
					.getSourceElement();

			String choiceType = getNodeTypeString(choiceNode);
			String targetType = getNodeTypeString(targetNode);

			if (!choiceType.equals(targetType))
				addError(id, "choice/target type mismatch");
		}
	}

	/**
	 * Returns the data type of a given node, which can be a variable or an attribute
	 * @param node
	 * @return
	 */
	private String getNodeTypeString(Node node) {
		if (node instanceof ComplexVariable)
			return ((ComplexVariable) node).getDataType().getId();
		else if (node instanceof ComplexAttribute)
			return ((ComplexAttribute) node).getAttribute().getDataType()
					.getId();
		else if (node instanceof PrimitiveVariable)
			return ((PrimitiveVariable) node).getDataType().getLiteral();
		else if (node instanceof PrimitiveAttribute)
			return ((PrimitiveAttribute) node).getAttribute().getDataType()
					.getLiteral();
		else
			throw new IllegalStateException("unknown type of node");
	}

	
}
