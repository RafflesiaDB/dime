package info.scce.dime.gui.checks;

import java.util.stream.Collectors;

import org.eclipse.xtext.xbase.lib.Pair;

import graphmodel.Node;
import info.scce.dime.api.DIMECheck;
import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.ProcessEntryPointComponent;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.gui.gui.BooleanInputStatic;
import info.scce.dime.gui.gui.ComplexInputPort;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.InputPort;
import info.scce.dime.gui.gui.IntegerInputStatic;
import info.scce.dime.gui.gui.LinkProcessSIB;
import info.scce.dime.gui.gui.PrimitiveInputPort;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Read;
import info.scce.dime.gui.gui.RealInputStatic;
import info.scce.dime.gui.gui.TextInputStatic;
import info.scce.dime.gui.gui.TimestampInputStatic;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.helper.GUIExtensionProvider;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.OutputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

/**
 * The link SIB check is used to validate process SIBs
 * placed in a GUI model
 * @author zweihoff
 *
 */
public class LinkProcessSIBCheck extends DIMECheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if all SIBs in the given
	 * GUI model are valid
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof LinkProcessSIB)
				checkLinkProcessSIB(id, (LinkProcessSIB) obj);
		}
	}
	
	/**
	 * Checks if the input ports of the given link SIB
	 * are still in sync with the referenced process model start SIB.
	 * @param id
	 * @param te
	 */
	private void checkLinkProcessSIB(GUIId id, LinkProcessSIB te) {
		//Check process type
		URLProcess url = (URLProcess) te.getProMod();
		Process process = null;
		if(url instanceof ProcessComponent) {
			process = ((ProcessComponent) url).getModel();
		} else if(url instanceof ProcessEntryPointComponent) {
			process = ((ProcessEntryPointComponent) url).getEntryPoint().getProMod();
		}
		if(process.getProcessType()!=ProcessType.BASIC){
			addError(id, "Invalid process type. Required BASIC");
		}
		
		//Check data Edges
		for(InputPort ip:te.getInputPorts())
		{
			if(ip.getIncoming().isEmpty()){
				addError(id, "missing port "+ip.getName()+" data write");
			}
		}
		//Check Signatur
		if(url.getUrl()!=null&&!url.getUrl().equals(te.getLabel())){
			addError(id, "link process sib name is outdated");
		}
		
		for(info.scce.dime.gui.gui.InputStatic ip:te.getInputStatics())
		{
			boolean isPortFound = false;
			for(OutputPort port : process.getStartSIBs().stream().flatMap(n->n.getOutputs().stream().map(e->(OutputPort)e)).collect(Collectors.toList()))
			{
				if(port.getName().equals(ip.getName())){	
					isPortFound = true;
					if( port.isIsList()){
						addError(id, "sib input port "+ip.getName()+" cannot be static");
						continue;
					}
					if( port instanceof ComplexOutputPort){
						addError(id, "sib input port "+ip.getName()+" cannot be static");
						continue;
					}
					if(port instanceof PrimitiveOutputPort){
						//check type
						PrimitiveOutputPort pop = (PrimitiveOutputPort) port;
						if(ip instanceof TextInputStatic) {
							if(pop.getDataType()!=PrimitiveType.TEXT)addError(id, "sib input port "+ip.getName()+" has to be "+pop.getDataType().getLiteral());
						}
						if(ip instanceof IntegerInputStatic) {
							if(pop.getDataType()!=PrimitiveType.INTEGER)addError(id, "sib input port "+ip.getName()+" has to be "+pop.getDataType().getLiteral());
						}
						if(ip instanceof RealInputStatic) {
							if(pop.getDataType()!=PrimitiveType.REAL)addError(id, "sib input port "+ip.getName()+" has to be "+pop.getDataType().getLiteral());
						}
						if(ip instanceof BooleanInputStatic) {
							if(pop.getDataType()!=PrimitiveType.BOOLEAN)addError(id, "sib input port "+ip.getName()+" has to be "+pop.getDataType().getLiteral());
						}
						if(ip instanceof TimestampInputStatic) {
							if(pop.getDataType()!=PrimitiveType.TIMESTAMP)addError(id, "sib input port "+ip.getName()+" has to be "+pop.getDataType().getLiteral());
						}
						
					}
				}
			}
			if(!isPortFound) {
				addError(id, "sib port "+ip.getName()+" is outdated");
			}
			
		}
		
		for(InputPort ip:te.getInputPorts())
		{
			boolean isPortFound = false;
			for(OutputPort port : process.getStartSIBs().stream().flatMap(n->n.getOutputs().stream().map(e->(OutputPort)e)).collect(Collectors.toList()))
			{
				if(port.getName().equals(ip.getName())){	
					isPortFound = true;
					if( port.isIsList()!=ip.isIsList()){
						addError(id, "sib input port "+ip.getName()+" list state is outdated");
					}
					if(port instanceof ComplexOutputPort){
						if(!(ip instanceof ComplexInputPort)){
							addError(id, "sib iput port "+ip.getName()+" type is outdated");						
						}
						else{
							ComplexOutputPort cv = (ComplexOutputPort) port;
							ComplexInputPort cip = (ComplexInputPort) ip;
							if(!cv.getDataType().equals(cip.getDataType())){	
								addError(id, "sib iput port "+ip.getName()+" data type is outdated");
							}
						}
					}
					if(port instanceof PrimitiveOutputPort){
						if(!(ip instanceof PrimitiveInputPort)){
							addError(id, "sib input port "+ip.getName()+" type is outdated");						
						}
						else{
							PrimitiveOutputPort cv = (PrimitiveOutputPort) port;
							PrimitiveInputPort cip = (PrimitiveInputPort) ip;
							if(!cv.getDataType().getLiteral().equals(cip.getDataType().getLiteral())){	
								addError(id, "sib iput port "+ip.getName()+" data type is outdated");
							}
						}
					}
				}
				
			}
			//Check Port connection
			
				for(Node node:ip.getIncoming(Read.class).stream().map(n->n.getSourceElement()).collect(Collectors.toList())){
					if(node instanceof Variable){
						Variable var = (Variable) node;
						
						if(var.isIsList()!=ip.isIsList()){
							addError(id, "sib port "+ip.getName()+" is not compatible to variable on list status");
						}
						if(var instanceof PrimitiveVariable){
							if(ip instanceof PrimitiveInputPort){
								if(((PrimitiveVariable) var).getDataType()!=((PrimitiveInputPort) ip).getDataType()){
									addError(id, "sib port "+ip.getName()+" primitive type is not compatible to variable");
								}
							}
							else{
								addError(id, "complex sib port "+ip.getName()+" is not compatible to primitive variable");
							}
						}
						else{
							if(ip instanceof ComplexInputPort){
								if (!GUIExtensionProvider.guiextension().isSubTypeWithSmartCast(
										Pair.of(var, ((ComplexVariable)var).getDataType()),
										Pair.of(ip, ((ComplexInputPort)ip).getDataType()))) {	
									addError(id, "sib port "+ip.getName()+" type is not compatible to variable");
								}
							}
							else{
								addError(id, "primitive sib port "+ip.getName()+" is not compatible to complex variable");
							}
						}
					}
				
			}
			
			if(!isPortFound){
				addError(id, "sib port "+ip.getName()+" is outdated");
			}
		}
		
		//Check if ports are missing
		int portSize = te.getInputPorts().size() + te.getInputStatics().size();
		long inputVars = process.getStartSIBs().stream().flatMap(n->n.getOutputs().stream()).count();
		if(portSize<inputVars){
			addError(id, "sib input ports are missing");
		}
	}
	
}
