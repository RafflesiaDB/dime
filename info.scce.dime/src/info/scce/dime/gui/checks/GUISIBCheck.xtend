package info.scce.dime.gui.checks

import graphmodel.Edge
import graphmodel.IdentifiableElement
import graphmodel.Node
import info.scce.dime.data.data.PrimitiveType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gUIPlugin.AbstractParameter
import info.scce.dime.gUIPlugin.ComplexInputParameter
import info.scce.dime.gUIPlugin.ComplexParameter
import info.scce.dime.gUIPlugin.Function
import info.scce.dime.gUIPlugin.GenericInputParameter
import info.scce.dime.gUIPlugin.GenericParameter
import info.scce.dime.gUIPlugin.Plugin
import info.scce.dime.gUIPlugin.PrimitiveInputParameter
import info.scce.dime.gUIPlugin.PrimitiveParameter
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.PrimitiveListAttribute
import info.scce.dime.gui.gui.PrimitiveListAttributeName
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Read
import info.scce.dime.gui.gui.Registration
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.mcam.modules.checks.GUICheck
import java.util.LinkedList
import java.util.List
import java.util.stream.Collectors
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import info.scce.dime.gui.helper.GUIExtensionProvider
import graphmodel.Edge
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import info.scce.dime.gui.gui.DataContext
import info.scce.dime.gui.gui.PrimitiveExtensionAttribute
import info.scce.dime.gui.gui.ComplexExtensionAttribute

/** 
 * 
 * The GUI SIB check is used to validate GUI SIBs and GUI plug in SIBs
 * placed in a GUI model
 * 
 * @author zweihoff
 * @author neubauer
 */
class GUISIBCheck extends GUICheck {
	protected extension GUIExtension guiex
	protected extension DataExtension = DataExtension.instance
	
	override init() {
		guiex = GUIExtensionProvider.guiextension
	}
	
	private dispatch def void doCheck(EObject it) {
		// nothing to check
	}
	
	
	
	private def ambiguousMsg(List<GUI> matchingGUIs) 
		'''Dispatching might be ambiguous for the following sub GUIs: «
			FOR it : matchingGUIs SEPARATOR ', '
				»«title»«
			ENDFOR
		».'''
	
	// TODO JN: move to generated GUICheck!!!
	private def addError(IdentifiableElement element, CharSequence msg) {
		element.addError(msg.toString)
	}

	/** 
	 * Checks, if all GUI SIBs and GUI plug in SIBs in the given
	 * GUI model are valid
	 */
	override check(GUI model) {
		adapter.entityIds.map[it.element].forEach[doCheck]
	}
	
	private def void checkListener(List<EventListener> listener, IdentifiableElement te) {
		// TODO replace with .drop[getIncoming(Registration).isEmpty] using 'drop' from CollectionExtension
		listener.filter[!getIncoming(Registration).empty].forEach[checkPortsConnected(te)]
		// TODO replace with .drop[IOs.map[incoming].flatten.isEmpty] using 'drop' from CollectionExtension
		listener.filter[getIncoming(Registration).empty].filter[IOs.exists[!incoming.isEmpty]].forEach[n|te.addWarning('''data is never send''')]
	}
	
	private def checkPortsConnected(EventListener el, IdentifiableElement te) {
		//el.inputPorts.filter[incoming.filter(Read).empty].forEach[n|te.addError('''missing port «n.getName()» data write'''.toString)]
	}
	
	private def checkGUIPluginListeners(EList<EventListener> listeners, EList<info.scce.dime.gUIPlugin.EventListener> events, GUIPlugin te) {
		//check missing event listeners
		events.filter[!listeners.map[name].contains(name)].forEach[n|te.addError( '''missing event listener «n.getName()»'''.toString)]
		//check not existing events
		listeners.filter[!events.map[name].contains(name)].forEach[n|te.addError( '''unkown event listener «n.getName()»'''.toString)]
		//check ports
		for(tuple:listeners.groupBy[n|events.findFirst[name.equals(n.name)]].entrySet)
		{
			val evt = tuple.key
			val listener = tuple.value.get(0)
			//check missing ports
			evt.parameters.filter[!listener.IOs.map[name].contains(name)].forEach[n|te.addError( '''missing port «n.name»'''.toString)]
			//check not existing ports
			listener.IOs.filter[!evt.parameters.map[name].contains(name)].forEach[n|te.addError( '''unkown port «n.getName()»'''.toString)]
			//check port type
			for(portTupel:listener.IOs.groupBy[n|evt.parameters.findFirst[name.equals(n.name)]].entrySet.filter[key!=null])
			{
				// GUI plug in parameter
				val evtPort = portTupel.key
				val listPort = portTupel.value.get(0)
				//wrong list status
				if(listPort instanceof InputPort) {
					if(evtPort.isList!=listPort.isList){
						te.addError( '''wrong list status «listPort.getName()»'''.toString)
					}	
				}
				if(listPort instanceof ComplexInputPort){
					//wrong complex type
					if(evtPort instanceof ComplexParameter) {
						val typeName = ((evtPort as ComplexParameter).type as Type)?.name
						val dataType = (listPort as ComplexInputPort).dataType
						if (dataType.name != typeName && !dataType.superTypes.map[name].contains(typeName)) {
							te.addError('''incompatible complex types «listPort.getName()»'''.toString)
						}
					}
					else if(evtPort instanceof GenericParameter){}
					else{
						te.addError( '''«listPort.getName()» has to be complex'''.toString)
					}
				}
				//wrong primitive type
				if(listPort instanceof PrimitiveInputPort && !(listPort instanceof InputStatic)) {
					if(evtPort instanceof PrimitiveParameter) {
						if(evtPort.type.toData!=(listPort as PrimitiveInputPort).dataType.toData) {
							te.addError( '''incompatible primitive types «listPort.getName()»'''.toString)
						}
					}
					else{
						te.addError( '''«listPort.getName()» has to be primitive'''.toString)
					}
				}
				
				
			}
		}
	}
	
	def checkGUISIBListeners(List<EventListener> listeners,List<Event> events, GUISIB te) {
		//check missing event listeners
		events.filter[!listeners.map[name].contains(name)].forEach[n|te.addError( '''missing event listener «n.getName()»'''.toString)]
		//check not existing events
		listeners.filter[!events.map[name].contains(name)].forEach[n|te.addError( '''unkown event listener «n.getName()»'''.toString)]
		//check ports
		for(tuple:listeners.groupBy[n|events.findFirst[name.equals(n.name)]].entrySet.filter[key!=null])
		{
			val evt = tuple.key
			val listener = tuple.value.get(0)
			//check missing ports
			evt.outputPorts.filter[!listener.IOs.map[name].contains(name)].forEach[n|te.addError( '''missing port «n.getName()»'''.toString)]
			//check not existing ports
			listener.IOs.filter[!evt.outputPorts.map[name].contains(name)].forEach[n|te.addError( '''unkown port «n.getName()»'''.toString)]
			//check port type
			for(portTupel:listener.IOs.groupBy[n|evt.outputPorts.findFirst[name.equals(n.name)]].entrySet)
			{
				val evtPort = portTupel.key
				val listPort = portTupel.value.get(0)
				//wrong list status
				if(listPort instanceof InputPort) {
					if(evtPort.isList!=listPort.isList){
						te.addError( '''wrong list status «listPort.getName()»'''.toString)
					}	
				}
				if(evtPort instanceof ComplexOutputPort){
					//wrong complex type
					if(listPort instanceof ComplexInputPort) {
						if(!(listPort as ComplexInputPort).dataType.superTypes.contains((evtPort as ComplexOutputPort).dataType) && !((listPort as ComplexInputPort).dataType.equals((evtPort as ComplexOutputPort).dataType))) {
							te.addError( '''incompatible complex types «listPort.getName()»'''.toString)
						}
					}
					else if(listPort instanceof GenericInputParameter){}
					else{
						te.addError( '''«listPort.getName()» has to be complex'''.toString)
					}
				}
				if(evtPort instanceof PrimitiveOutputPort && !(listPort instanceof InputStatic)) {
					//wrong primitive type
					if(listPort instanceof PrimitiveInputPort) {
						if((evtPort as PrimitiveOutputPort).dataType!=(listPort as PrimitiveInputPort).dataType) {
							te.addError( '''incompatible primitive types «listPort.getName()»'''.toString)
						}
					}
					else{
						te.addError( '''«listPort.getName()» has to be primitive'''.toString)
					}
				}
			}
		}
		
	}

	/** 
	 * TODO JN: why not use post create hook to create a new GUI SIB and check whether result is "the same" as current SIB?
	 * 
	 * Checks if the input ports, branches and output ports of
	 * the given GUI SIB are still in sync with the referenced GUI model
	 * @param id
	 * @param te
	 */
	private dispatch def void doCheck(GUISIB te) {
		// Check data Edges
		if (!(te.container instanceof ExtensionContext)) {
//			for (InputPort ip : te.getInputPorts()) {
//				if (ip.getIncoming().isEmpty()) {
//					te.addError('''missing port «ip.getName()» data write'''.toString)
//				}
//			}

		}
		
		var GUI gui = te.getGui()
		
		if(te instanceof DispatchedGUISIB) {
			gui.sortedDispatchedGUIs.forEach [ score, matchingGUIs |
			if (matchingGUIs.size > 1) {
				te.addWarning(matchingGUIs.ambiguousMsg.toString)
			}
		]
		}
		
		
		// Check event listener signature
		te.eventListeners.checkGUISIBListeners(gui.listenerContexts.map[events].flatten.toList, te)
		// Check data edges for event listeners
		te.eventListeners.checkListener(te)
		
		// Check Argumnents
		val availablePlaceholder = gui.find(Placeholder).map[name].toList
		val providedArguments = te.arguments.map[blockName]
		availablePlaceholder.filter[n|!providedArguments.contains(n)].forEach[n|te.addError('''Argument for placeholder «n» missing''')]
		providedArguments.filter[n|!availablePlaceholder.contains(n)].forEach[n|te.addError('''No placeholder for «n» available''')]

		// Check Signatur
		if (!gui.getTitle().equals(te.label)) {
			te.addError( "gui sib name is outdated")
		}
		for (InputPort ip : te.getInputPorts()) {
			var boolean isPortFound = false
			for (Variable ^var : gui.getDataContexts().stream().flatMap([n|n.getVariables().stream()]).filter([ n |
				n.isIsInput()
			]).collect(Collectors::toList())) {
				if (^var.getName().equals(ip.getName())) {
					isPortFound = true
					if (^var.isIsList() !== ip.isIsList()) {
						te.addError( '''gui sib input port «ip.getName()» list state is outdated'''.toString)
					}
					if (^var instanceof ComplexVariable) {
						if (!(ip instanceof ComplexInputPort)) {
							te.addError( '''gui sib input port «ip.getName()» type is outdated'''.toString)
						} else {
							var ComplexVariable cv = (^var as ComplexVariable)
							var ComplexInputPort cip = (ip as ComplexInputPort)
							if (!cv.getDataType().equals(cip.getDataType())) {
								te.addError( '''gui sib input port «ip.getName()» data type is outdated'''.toString)
							}
						}
					}
					if (^var instanceof PrimitiveVariable) {
						if (!(ip instanceof PrimitiveInputPort)) {
							te.addError( '''gui sib input port «ip.getName()» type is outdated'''.toString)
						} else {
							var PrimitiveVariable cv = (^var as PrimitiveVariable)
							var PrimitiveInputPort cip = (ip as PrimitiveInputPort)
							if (!cv.getDataType().equals(cip.getDataType())) {
								te.addError( '''gui sib input port «ip.getName()» data type is outdated'''.toString)
							}
						}
					}
				}
			}
			// not necessary for extension GUISIB
			if (te.container instanceof ExtensionContext) {
				return
			}
			// Check Port connection
			if (ip.getIncoming().isEmpty()) {
				te.addWarning( '''gui sib port «ip.getName()» is not connected to variable'''.toString)
			} else {
				for (Node node : ip.getIncoming(typeof(Read)).stream().map([n|n.getSourceElement()]).collect(
						Collectors::toList())) {
					if (node instanceof Variable) {
						var Variable ^var = (node as Variable)
						if (^var.isIsList() !== ip.isIsList()) {
							te.addError(
								'''gui sib port «ip.getName()» is not compatible to variable on list status'''.toString)
						}
						if (^var instanceof PrimitiveVariable) {
							if (ip instanceof PrimitiveInputPort) {
								if (((^var as PrimitiveVariable)).getDataType() !==
										((ip as PrimitiveInputPort)).getDataType()) {
									te.addError(
										'''gui sib port «ip.getName()» primitive type is not compatible to variable'''
									)
								}
							}
							else {
								te.addError(
									'''complex gui sib port «ip.getName()» is not compatible to primitive variable'''
								)
							}
						} 
						else {
							if (ip instanceof ComplexInputPort) {
								if (!isSubTypeWithSmartCast(
										new Pair(^var, ((^var as ComplexVariable)).getDataType()),
										new Pair(ip, ((ip as ComplexInputPort)).getDataType()))) {
									te.addError( 
										'''gui sib port «ip.getName()» type is not compatible to variable'''.toString)
								}
							}
							else {
								te.addError(
									'''primitive gui sib port «ip.getName()» is not compatible to complex variable'''.
										toString)
							}
						}
					}
					else if (node instanceof Attribute) {
						val Attribute attr = (node as Attribute)
						if (attr instanceof PrimitiveAttribute) {
							if (((attr as PrimitiveAttribute)).getAttribute().isIsList() !== ip.isIsList()) {
								te.addError(
									'''gui sib port «ip.getName()» is not compatible to attribute on list status'''
								)
							}
							if (ip instanceof PrimitiveInputPort) {
								if (((attr as PrimitiveAttribute)).
										getAttribute().getDataType() !==
										toData(((ip as PrimitiveInputPort)).getDataType())) {
									te.addError(
										'''gui sib port «ip.getName()» primitive type is not compatible to attribute'''
									)
								}
							} 
							else {
								te.addError(
									'''complex gui sib port «ip.getName()» is not compatible to primitive attribute'''.
										toString)
							}
						}
						else if (attr instanceof PrimitiveExtensionAttribute) {
							if (((attr as PrimitiveExtensionAttribute)).getAttribute().isIsList() !== ip.isIsList()) {
								te.addError(
									'''gui sib port «ip.getName()» is not compatible to attribute on list status'''
								)
							}
							if (ip instanceof PrimitiveInputPort) {
								if (((attr as PrimitiveExtensionAttribute)).
										getAttribute().primitiveDataType !==
										toData(((ip as PrimitiveInputPort)).getDataType())) {
									te.addError(
										'''gui sib port «ip.getName()» primitive type is not compatible to attribute'''
									)
								}
							} 
							else {
								te.addError(
									'''complex gui sib port «ip.getName()» is not compatible to primitive attribute'''.
										toString)
							}
						}
						else if(attr instanceof ComplexExtensionAttribute){
							if (ip instanceof ComplexInputPort) {
								if (!isSubTypeWithSmartCast(
										attr ->
											((attr as ComplexExtensionAttribute)).
												getAttribute().
												complexDataType,
										ip ->
											((ip as ComplexInputPort)).
												getDataType())) {
									te.addError(
										'''gui sib port «ip.getName()» type is not compatible to attribute'''
									)
								}
							}
							else {
								te.addError(
									'''primitive gui sib port «ip.getName()» is not compatible to complex attribute'''
								)
							}
						}
						else {
							if (ip instanceof ComplexInputPort) {
								if (!isSubTypeWithSmartCast(
										attr ->
											((attr as ComplexAttribute)).
												getAttribute().
												getDataType(),
										ip ->
											((ip as ComplexInputPort)).
												getDataType())) {
									te.addError(
										'''gui sib port «ip.getName()» type is not compatible to attribute'''
									)
								}
							}
							else {
								te.addError(
									'''primitive gui sib port «ip.getName()» is not compatible to complex attribute'''
								)
							}
						}
					}
				}
			}
			if (!isPortFound) {
				te.addError(
					'''gui sib port «ip.getName()» is outdated'''
				)
			}
		}
		//check Inputstaticports
		val guiPrimInputVars = te.gui?.find(PrimitiveVariable).filter[isInput]
		for (inputStatic : te.inputStatics) {
			val primInputVar = guiPrimInputVars?.findFirst[it.name == inputStatic.name]
			if (primInputVar === null){
				te.addError("GUISIB '" + te.label + "': input variable '" + inputStatic.name+  "' not found in GUI")
			} else { // corresponding input variable exists
				var primInputVarTypeName = primInputVar.dataType.getName
				var primInputVarIsList = primInputVar.isList
				
				if(inputStatic instanceof IntegerInputStatic){
					if(primInputVarTypeName != "Integer"){
						te.addError("Type mismatch of IntegerInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' does not have an integer type")
					}
					if(primInputVarIsList){
						te.addError("List type mismatch of IntegerInputStatic in GUISIB '" + te.name + "':  corresponding variable '" + primInputVar.name + "' is a list")
					}
				}
				if(inputStatic instanceof BooleanInputStatic){
					if(primInputVarTypeName != "Boolean"){
						te.addError("Type mismatch of BooleanInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' does not have a boolean type")
					}
					if(primInputVarIsList){
						te.addError("List type mismatch of BooleanInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' is a list")
					}
				}
				if(inputStatic instanceof RealInputStatic){
					if(primInputVarTypeName != "Real"){
						te.addError("Type mismatch of RealInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' does not have a real type")
					}
					if(primInputVarIsList){
						te.addError("List type mismatch of RealInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' is a list")
					}
				}
				if(inputStatic instanceof TextInputStatic){
					if(primInputVarTypeName != "Text"){
						te.addError("Type mismatch of TextInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' does not have a text type")
					}
					if(primInputVarIsList){
						te.addError("List type mismatch of TextInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' is a list")
					}
				}
				if(inputStatic instanceof TimestampInputStatic){
					if(primInputVarTypeName != "Timestamp"){
						te.addError("Type mismatch of TimestampInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' does not have a timestamp type")
					}
					if(primInputVarIsList){
						te.addError("List type mismatch of TimestampInputStatic in GUISIB '" + te.name + "': corresponding variable '" + primInputVar.name + "' is a list")
					}
				}
			}
			
		}
		
		// Check if ports are missing
		var int portSize = te.
			getInputPorts().size() +
			te.getInputStatics().size()
		var long inputVars = gui.
			getDataContexts().stream().
			flatMap([ n |
				n.getVariables().
					stream()
			]).
			filter([n|n.isIsInput()]).
			count()
		if (portSize < inputVars) {
			te.addError(
				"gui sib input ports are missing")
		}
	}

	/** 
	 * Checks if the input ports, branches and output ports of
	 * the given GUI plug in SIB are still in sync with the referenced GUI plug in
	 * @param id
	 * @param te
	 */
	private dispatch def void doCheck(GUIPlugin te) {
		val function = te.function as Function
		// Check generic ports
		if((!te.inputGenerics.empty)||(te.abstractBranchs.filter(Branch).exists[!outputGenerics.empty])||(te.eventListeners.exists[!inputGenerics.empty])){
			te.addError('''generic inputs has to be replaced'''.toString)
		}
		// Check event listener signature
		te.eventListeners.checkGUIPluginListeners(function.events, te)
		// Check data edges for event listeners
		te.eventListeners.checkListener(te)
																							
//		for (InputPort ip : te.getInputPorts().filter[n|!(n instanceof InputGeneric)]) {
//			if (ip.getIncoming().isEmpty()) {
//				te.addError('''missing port «ip.getName()» data write'''.toString)
//			}
//		}
		
		// Check Placeholder
		// Check Argumnents
		
		val plugin = function.eContainer as Plugin
		val availablePlaceholder = new LinkedList
		if(plugin.template?.placholders?.placeholders != null){
			availablePlaceholder+= plugin.template.placholders.placeholders.map[name].toList			
		}
		val providedArguments = te.arguments.map[blockName]
		availablePlaceholder.filter[n|!providedArguments.contains(n)].forEach[n|te.addError('''Argument for placeholder «n» missing''')]
		providedArguments.filter[n|!availablePlaceholder.contains(n)].forEach[n|te.addError('''No placeholder for «n» available''')]
		
		// Check Signature
		var Function fun = (te.getFunction() as Function)
		if (!fun.getFunctionName().equals(te.getLabel())) {
			te.addError("native gui sib name is outdated")
		}
		//check inputs
		for (InputPort ip : te.getInputPorts()) {
			var boolean portFound = false
			for (AbstractParameter par : fun.getParameters()) {
				var ipName = if(par instanceof ComplexInputParameter){
					par.parameter.name
				} else if(par instanceof GenericInputParameter){
					par.parameter.name
				} else {
					(par as PrimitiveInputParameter).parameter.name
				}
				var ipIsList = if(par instanceof ComplexInputParameter){
					par.parameter.isList
				} else if(par instanceof GenericInputParameter){
					par.parameter.isIsList
				} else {
					(par as PrimitiveInputParameter).parameter.isIsList
				}
				
				if (ipName.equals(ip.getName())) {
					portFound = true
					if (ipIsList != ip.isIsList()) {
						te.addError('''native gui sib input port «ip.getName()» list state is outdated'''.toString)
					}
					if (par instanceof ComplexInputParameter && (ip instanceof PrimitiveInputPort)) {
						te.addError('''native gui sib port «ip.getName()» outdated'''.toString)
					}
					if (par instanceof PrimitiveInputParameter && (ip instanceof ComplexInputPort)) {
						te.addError('''native gui sib port «ip.getName()» outdated'''.toString)
					}
					if (par instanceof ComplexInputParameter && ip instanceof ComplexInputPort) {
						if(!(toComplexType(par as ComplexInputParameter).equals((ip as ComplexInputPort).dataType))){
							te.addError('''native gui sib port «ip.getName()» outdated'''.toString)
						}
					}
					if (par instanceof PrimitiveInputParameter && ip instanceof PrimitiveInputPort) {
						if((toPrimitiveType((par as PrimitiveInputParameter).parameter.type)!=((ip as PrimitiveInputPort).dataType))){
							te.addError('''native gui sib port «ip.getName()» outdated'''.toString)
						}
					}
				}
			}
			if (!portFound) {
				te.addError('''native gui sib iput port «ip.getName()» is outdated '''.toString)
			}
			for (Node node : ip.getIncoming(Read).map[sourceElement]) {
				if (node instanceof Variable) {
					var Variable ^var = (node as Variable)
					if (^var.isIsList() !==ip.isIsList()) {
						te.addError('''native gui sib port «ip.getName()» is not compatible to variable on list status'''.toString)
					}
					if (^var instanceof PrimitiveVariable) {
						if (ip instanceof PrimitiveInputPort) {
							if (((^var as PrimitiveVariable)).getDataType() !==((ip as PrimitiveInputPort)).getDataType()) {
								te.addError('''native gui sib port «ip.getName()» primitive type is not compatible to variable'''.toString)
							}
						}
						else {
							te.addError('''complex native gui sib port «ip.getName()» is not compatible to primitive variable'''.toString)
						}
					}
					else {
						if (ip instanceof ComplexInputPort) {
							if (!isSubTypeWithSmartCast(new Pair(^var,((^var as ComplexVariable)).getDataType()),new Pair(ip,((ip as ComplexInputPort)).getDataType()))) {
								te.addError('''native gui sib port «ip.getName()» type is not compatible to variable'''.toString)
							}
						}
						else {
							te.addError('''primitive native gui sib port «ip.getName()» is not compatible to complex variable'''.toString)
						}
					}
				}
				else if (node instanceof Attribute) {
					val Attribute attr = (node as Attribute)
					if (attr instanceof PrimitiveAttribute) {
						if (((attr as PrimitiveAttribute)).getAttribute().isIsList() !==ip.isIsList()) {
							te.addError('''native gui sib port «ip.getName()» is not compatible to attribute on list status'''.toString)
						}
						if (ip instanceof PrimitiveInputPort) {
							if (((attr as PrimitiveAttribute)).getAttribute().getDataType() !==toData(((ip as PrimitiveInputPort)).getDataType())) {
								te.addError('''native gui sib port «ip.getName()» primitive type is not compatible to attribute'''.toString)
							}
						}
						else {
							te.addError('''native complex gui sib port «ip.getName()» is not compatible to primitive attribute'''.toString)
						}
					}
					else if (attr instanceof PrimitiveExtensionAttribute) {
						if (((attr as PrimitiveExtensionAttribute)).getAttribute().isIsList() !==ip.isIsList()) {
							te.addError('''native gui sib port «ip.getName()» is not compatible to attribute on list status'''.toString)
						}
						if (ip instanceof PrimitiveInputPort) {
							if (((attr as PrimitiveExtensionAttribute)).getAttribute().primitiveDataType !==toData(((ip as PrimitiveInputPort)).getDataType())) {
								te.addError('''native gui sib port «ip.getName()» primitive type is not compatible to attribute'''.toString)
							}
						}
						else {
							te.addError('''native complex gui sib port «ip.getName()» is not compatible to primitive attribute'''.toString)
						}
					}
					else if(attr instanceof PrimitiveListAttribute) {
	                    if (ip instanceof PrimitiveInputPort) {
	                        if (((attr as PrimitiveListAttribute)).attributeName == PrimitiveListAttributeName.SIZE && toData(((ip as PrimitiveInputPort)).getDataType()) != PrimitiveType.INTEGER) {
	                                te.addError('''native gui sib port «ip.getName()» primitive type is not compatible to size list attribute'''.toString)
	                        }
	                    }
	                    else {
	                    		te.addError('''native gui sib port «ip.getName()» is not compatible to primitive list attribute'''.toString)
	                    }
	                }
	                else if(attr instanceof ComplexExtensionAttribute){
						if (ip instanceof ComplexInputPort) {
							if (!isSubTypeWithSmartCast(new Pair(attr,((attr as ComplexExtensionAttribute)).getAttribute().complexDataType),new Pair(ip,((ip as ComplexInputPort)).getDataType()))) {
								te.addError('''native gui sib port «ip.getName()» type is not compatible to attribute'''.toString)
							}
						}
						else {
							te.addError('''native primitive gui sib port «ip.getName()» is not compatible to complex attribute'''.toString)
						}
					}
					else {
						if (ip instanceof ComplexInputPort) {
							if (!isSubTypeWithSmartCast(new Pair(attr,((attr as ComplexAttribute)).getAttribute().getDataType()),new Pair(ip,((ip as ComplexInputPort)).getDataType()))) {
								te.addError('''native gui sib port «ip.getName()» type is not compatible to attribute'''.toString)
							}
						}
						else {
							te.addError('''native primitive gui sib port «ip.getName()» is not compatible to complex attribute'''.toString)
						}
					}
				}
			}
		}
		var int portSize = te.getInputPorts().size() +te.getInputStatics().size()
		var long requiredOutputs = fun.getParameters().size()
		if (portSize <requiredOutputs) {
			te.addError("native GUI SIB input ports are missing")
		}
		
		//check outputs
		te.branchs.forEach[b|{
			//check branch is defined
			val output = function.outputs.findFirst[it.outputName.equals(b.name)]
			if(output===null) {
				b.addError('''branch «b.name» does not exist in GUIPlugin «te.label»'''.toString)
			} else {
				// check ports exist
				b.outputPorts.forEach[p|{
					val parameter = output.parameters.findFirst[it.name.equals(p.name)]
					if(parameter === null) {
						b.addError('''port «p.name» does not exist in GUIPlugin «te.label» branch «output.outputName»'''.toString)
					} else {
						if(parameter.isList != p.isIsList) {
							b.addError('''port «p.name» list state is outdated of GUIPlugin «te.label» branch «output.outputName»'''.toString)
						}
						if(parameter instanceof ComplexParameter && p instanceof ComplexOutputPort) {
							if(!(parameter as ComplexParameter).type.equals((p as ComplexOutputPort).dataType)) {
								b.addError('''port «p.name» type is outdated of GUIPlugin «te.label» branch «output.outputName»'''.toString)
							}
						} else if(parameter instanceof PrimitiveParameter && p instanceof PrimitiveOutputPort) {
							if((parameter as PrimitiveParameter).type.toData != (p as PrimitiveOutputPort).dataType.toData) {
								b.addError('''port «p.name» type is outdated of GUIPlugin «te.label» branch «output.outputName»'''.toString)
							}
						} else {
							b.addError('''port «p.name» type is outdated of GUIPlugin «te.label» branch «output.outputName»'''.toString)
						}
					}
				}]
			}
			
		}]
		
	}	
}
																																																																								
																																																																						
