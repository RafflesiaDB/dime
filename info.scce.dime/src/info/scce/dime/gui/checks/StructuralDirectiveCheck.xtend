package info.scce.dime.gui.checks

import info.scce.dime.checks.GUICheck
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.MovableContainer
import info.scce.dime.gui.gui.^FOR
import info.scce.dime.gui.gui.^IF
import info.scce.dime.gui.gui.Iteration
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.IS

class StructuralDirectiveCheck extends GUICheck {
	
	override check(GUI it) {
		find(MovableContainer)
			.filter[!getIncoming(^FOR).isEmpty && !getIncoming(^IF).isEmpty]
			.forEach[addError("incoming IF and FOR at the same time is not supported")]
		find(Iteration)
			.map[sourceElement]
			.filter(Variable)
			.filter[!isIsList]
			.forEach[addError("Iteration can only be performed on a list")]
		find(Iteration)
			.map[sourceElement]
			.filter(ComplexAttribute)
			.filter[!attribute.isIsList]
			.forEach[addError("Iteration can only be performed on a list")]
		find(Iteration)
			.map[sourceElement]
			.filter(PrimitiveAttribute)
			.filter[!attribute.isIsList]
			.forEach[addError("Iteration can only be performed on a list")]
		find(IS)
			.filter[matchingType.nullOrEmpty]
			.forEach[addError("IS edge matching type is not specified")]
	}

}