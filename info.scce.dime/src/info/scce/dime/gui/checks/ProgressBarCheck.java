package info.scce.dime.gui.checks;

import info.scce.dime.gui.gui.Display;
import info.scce.dime.gui.gui.ProgressBar;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.checks.AbstractCheck;

/**
 * The progress bar check is used to validate progress bar components
 * @author zweihoff
 *
 */
public class ProgressBarCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if all progress bar components present in the
	 * given GUI model are connected to exactly one variable or attribute
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof ProgressBar)
				check(id, (ProgressBar) obj);
		}
	}
	
	@Override
	public void init() {}

	/**
	 * Checks, if the given progress bar component is
	 * connected to exactly one variable or attribute
	 * by one display edge
	 */
	private void check(GUIId id, ProgressBar te) {
		//Check display Edges
		if(te.getIncoming(Display.class).isEmpty()){
			addError(id, "display edge missing");
		}
		else{
			if(te.getIncoming(Display.class).size()>1){
				addError(id, "only one display edge allowed");				
			}
		}
		
	}
	
}
