package info.scce.dime.gui.checks;

import java.io.File;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;

import info.scce.dime.checks.AbstractCheck;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;

/**
 * The image check is used to validate image components
 * @author zweihoff
 *
 */
public class ImageCheck extends AbstractCheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if all images in the given GUI model are valid
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof Image)
				check(id, (Image) obj);
		}
	}
	
	/**
	 * Returns the absolute path for a given workspace relative path
	 * @param wsPath
	 * @param gui
	 * @return
	 */
	private String toAbsolutPath(String wsPath,GUI gui)
	{
		if(wsPath.startsWith("http")){
			return wsPath;
		}
		File f = toFile(wsPath,gui);
		if(f == null){
			return null;
		}
		return f.getAbsolutePath();
	}
	
	/**
	 * Returns a file object for a given workspace relative path
	 * @param wsPath
	 * @param gui
	 * @return
	 */
	private File toFile(String wsPath,GUI gui)
	{
		String platformString = gui.eResource().getURI().toPlatformString(true);
		if(platformString==null) {
			return null;
		}
		IResource ir= ResourcesPlugin.getWorkspace().getRoot().findMember(platformString);
		
		return new File(ir.getProject().getLocation().append(wsPath).toString());
	}
	
	@Override
	public void init() {}

	/**
	 * Check, if a given image component is valid.
	 * If a display edge is connected to the image the value of the edge source variable or attribute
	 * has to be a file or a string describing a file location.
	 * If no display edge is present, a image path has to be specified.
	 * This can be done statically or dynamic by an Angular expression.
	 * @param id
	 * @param te
	 */
	private void check(GUIId id, Image te) {
		//Check file guards
		if(te.getGuardSIBs().isEmpty()){
			if(te.getPath() != null) {
				if(te.getPath().isEmpty()){
					addError(id, "Unguarded file access");
				}
				else{
					if(!(te.getPath().startsWith("{{")&&te.getPath().endsWith("}}"))){
						if(!te.getPath().startsWith("http")){
							String absPath = toAbsolutPath(te.getPath(),te.getRootElement());
							if(absPath != null) {
								File file = new File(absPath);
								if(!file.exists()){
									addError(id, "file does not exist");
								}
								
							}
							
						}			
					}
				}
			}else {
				addError(id, "path is null");
			}
		}
		else{
			//guard availbale
			if(te.getGuardSIBs().size()>1){
				addError(id, "only one file guard is allowed");
			}
			if(te.getPath() != null){
				if(!te.getPath().isEmpty()){
					addWarning(id, "path is ignored");
				}
			}
		}
		
	}
	
}
