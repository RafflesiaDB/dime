package info.scce.dime.gui.checks;

import java.util.stream.Collectors;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;
import graphmodel.Node;
import info.scce.dime.api.DIMECheck;
import info.scce.dime.gui.gui.ComplexInputPort;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.gui.GuardSIB;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.gui.InputPort;
import info.scce.dime.gui.gui.PrimitiveInputPort;
import info.scce.dime.gui.gui.PrimitiveVariable;
import info.scce.dime.gui.gui.Read;
import info.scce.dime.gui.gui.Variable;
import info.scce.dime.gui.mcam.adapter.GUIAdapter;
import info.scce.dime.gui.mcam.adapter.GUIId;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.OutputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

/**
 * The Guard SIB check is used to validate Guard SIBs
 * placed in a GUI model
 * @author zweihoff
 *
 */
public class GuardSIBCheck extends DIMECheck<GUIId, GUIAdapter> {

	private GUIAdapter adapter;


	/**
	 * Checks, if all Guard SIBs in the given
	 * GUI model are valid
	 */
	@Override
	public void doExecute(GUIAdapter arg0) {
		this.adapter = arg0;

		for (GUIId id : adapter.getEntityIds()) {
			Object obj = id.getElement();

			if (obj instanceof GuardSIB)
				checkGuardSIB(id, (GuardSIB) obj);
		}
	}	

	/**
	 * Checks if the input ports of the given Guard SIB
	 * are still in sync with the referenced security file process model.
	 * @param id
	 * @param te
	 */
	private void checkGuardSIB(GUIId id, GuardSIB te) {
		//Check process type
//		FIXME: Issue #20589
//		Process p = (Process) te.getProcess();
		Process p = (Process) ReferenceRegistry.getInstance().getEObject(te.getLibraryComponentUID());
		if(p.getProcessType()!=ProcessType.FILE_DOWNLOAD_SECURITY){
			addError(id, "Invalid guard process type");
		}
		//Check process signature
		if(p.getEndSIBs().size()==2){
			if(!p.getEndSIBs().stream().filter(n->n.getBranchName().equals("result")).findAny().isPresent())
			{
				addError(id, "result end sib missing");
			}
			else{
				EndSIB sib = p.getEndSIBs().stream().filter(n->n.getBranchName().equals("result")).findAny().get();
				if(sib.getInputPorts().size()!=1){
					addError(id, "result end sib ports invalid");
				}
				if(!sib.getPrimitiveInputPorts().stream().filter(n->n.getName().equals("file")&&n.getDataType()==PrimitiveType.FILE).findAny().isPresent())
				{
					addError(id, "result end sib port file is missing");
				}
			}
			if(!p.getEndSIBs().stream().filter(n->n.getBranchName().equals("not found")).findAny().isPresent())
			{				
				addError(id, "not found end sib missing");
			}
			else{
				EndSIB sib = p.getEndSIBs().stream().filter(n->n.getBranchName().equals("not found")).findAny().get();
				if(sib.getInputPorts().size()!=0){
					addError(id, "not found end sib ports invalid");
				}
			}
		}
		else {
			addError(id, "Invalid guard process signature: End SIBS 'not found' and 'result' required");
		}
		//Check data Edges
		for(InputPort ip:te.getInputPorts())
		{
			if(ip instanceof ComplexInputPort){
				addError(id, "only primitive inputs allowed");
			}
			if(ip.getIncoming().isEmpty()){
				addError(id, "missing port "+ip.getName()+" data write");
			}
		}
		
		if(te.getContainer() instanceof File || te.getContainer() instanceof Image) {
			te.getIOs().stream().filter(n->n.getName().equals("cache")).forEach(n->addError(id, "'cache' is not allowed as port name "));
		}
		
		//Check Signatur
//		FIXME: Issue #20589
		Process process = (Process) ReferenceRegistry.getInstance().getEObject(te.getLibraryComponentUID());
//		Process process = (Process) te.getProcess();
		if(!process.getModelName().equals(te.getName())){
			addError(id, "guard sib name is outdated");
		}
		
		for(InputPort ip:te.getInputPorts())
		{
			
			boolean isPortFound = false;
			for(OutputPort port : process.getStartSIBs().stream().flatMap(n->n.getOutputs().stream().map(e->(OutputPort)e)).collect(Collectors.toList()))
			{
				if(port.getName().equals(ip.getName())){	
					isPortFound = true;
					if( port.isIsList()!=ip.isIsList()){
						addError(id, "guard sib input port "+ip.getName()+" list state is outdated");
					}
					if(port instanceof ComplexOutputPort){
						if(!(ip instanceof ComplexInputPort)){
							addError(id, "guard sib iput port "+ip.getName()+" type is outdated");						
						}
						else{
							ComplexOutputPort cv = (ComplexOutputPort) port;
							ComplexInputPort cip = (ComplexInputPort) ip;
							if(!cv.getDataType().equals(cip.getDataType())){	
								addError(id, "guard sib iput port "+ip.getName()+" data type is outdated");
							}
						}
					}
					if(port instanceof PrimitiveOutputPort){
						if(!(ip instanceof PrimitiveInputPort)){
							addError(id, "guard sib input port "+ip.getName()+" type is outdated");						
						}
						else{
							PrimitiveOutputPort cv = (PrimitiveOutputPort) port;
							PrimitiveInputPort cip = (PrimitiveInputPort) ip;
							if(!cv.getDataType().getLiteral().equals(cip.getDataType().getLiteral())){	
								addError(id, "guard sib iput port "+ip.getName()+" data type is outdated");
							}
						}
					}
				}
				
			}
			//Check Port connection
			if(ip.getIncoming().isEmpty()){
				addError(id, "guard sib port "+ip.getName()+" is not connected to variable");
			}
			else{
				for(Node node:ip.getIncoming(Read.class).stream().map(n->n.getSourceElement()).collect(Collectors.toList())){
					if(node instanceof Variable){
						Variable var = (Variable) node;
						if(var.isIsList()!=ip.isIsList()){
							addError(id, "guard sib port "+ip.getName()+" is not compatible to variable on list status");
						}
						if(var instanceof PrimitiveVariable){
							if(ip instanceof PrimitiveInputPort){
								if(((PrimitiveVariable) var).getDataType()!=((PrimitiveInputPort) ip).getDataType()){
									addError(id, "guard sib port "+ip.getName()+" primitive type is not compatible to variable");
								}
							}
							else{
								addError(id, "complex guard sib port "+ip.getName()+" is not compatible to primitive variable");
							}
						}
					}
				}
			}
			
			if(!isPortFound){
				addError(id, "guard sib port "+ip.getName()+" is outdated");
			}
		}
		
		//Check if ports are missing
		int portSize = te.getInputPorts().size() + te.getInputStatics().size();
		long inputVars = process.getStartSIBs().stream().flatMap(n->n.getOutputs().stream()).count();
		if(portSize<inputVars){
			addError(id, "gui sib input ports are missing");
		}
	}
	
}
