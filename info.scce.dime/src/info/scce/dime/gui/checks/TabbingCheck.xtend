package info.scce.dime.gui.checks

import info.scce.dime.checks.AbstractCheck
import info.scce.dime.gui.gui.Tabbing
import info.scce.dime.gui.mcam.adapter.GUIAdapter
import info.scce.dime.gui.mcam.adapter.GUIId
import info.scce.dime.gui.gui.Iteration

/** 
 * The event check is used to validate the event listeners
 * of all listener contexts of a GUI model
 * @author zweihoff
 */
class TabbingCheck extends AbstractCheck<GUIId, GUIAdapter> {
	GUIAdapter adapter

	/** 
	 * Checks, if the events in given GUI model
	 * are valid.
	 */
	override void doExecute(GUIAdapter arg0) {
		this.adapter = arg0
		for (GUIId id : adapter.getEntityIds()) {
			var Object obj = id.getElement()
			if(obj instanceof Tabbing){
				obj.tabs.filter[n|!n.placeholders.empty].filter[buttonGroups.empty].forEach[id.addError('''A button group has to be provided''')]
				obj.tabs.filter[n|!n.placeholders.empty].map[buttonGroups].flatten.filter[buttons.empty].forEach[id.addError('''At least one button has to be provided''')]
				if(!obj.tabs.filter[n|!n.placeholders.empty].empty){
					//placehodler
					//no defaults
					obj.tabs.filter[^default].forEach[n|id.addWarning('''Default tab «n.label» is ignored in major/minor tabbing''')]
				} else{
					obj.tabs.filter[^default].filter[!getIncoming(Iteration).empty].forEach[n|id.addError('''Looped tab «n.label» can not be default''')]
				}
			}
		}
	}
	
	override init() {
	}
	
}
