package info.scce.dime.gui.checks;

import graphmodel.Container;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.impl.ComplexVariableImpl;
import info.scce.dime.gui.mcam.modules.checks.GUICheck;

/**
 * Check for live variables:
 * 	- List-Variables without any predecessor can no be live.
 */
public class LiveVariableCheck extends GUICheck {

	@Override
	public String getName() {
		return "LiveVariableCheck";
	}
	
	@Override
	public void check(GUI model) {
		this.adapter
			.getEntityIds()
			.stream()
			.map(id -> id.getElement())
			.filter(e -> e instanceof ComplexVariableImpl && ((ComplexVariableImpl) e).isIsLive())
			.forEach(e -> checkLiveVariable((Container) e));
	}
		
	private void checkLiveVariable(final Container container) {
		ComplexVariableImpl complexVariable = (ComplexVariableImpl) container;
		if(complexVariable.isIsList() 
			&& (complexVariable.getComplexVariablePredecessors() == null || complexVariable.getComplexVariablePredecessors().isEmpty())) {
			this.addError(container, "List without any predecessor can not be live.");
		}
	}

}
