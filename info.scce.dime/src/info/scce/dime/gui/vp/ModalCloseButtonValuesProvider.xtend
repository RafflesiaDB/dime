package info.scce.dime.gui.vp

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import graphmodel.ModelElementContainer
import graphmodel.internal.InternalNode
import info.scce.dime.gui.gui.ModalCloseButton
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.gui.helper.GUIExtensionProvider
import java.util.Map

class ModalCloseButtonValuesProvider extends CincoValuesProvider<ModalCloseButton, String> {
	
	extension GUIExtension = GUIExtensionProvider.guiextension
	
	override Map<String, String> getPossibleValues(ModalCloseButton ib) {
		val container = (ib.eContainer.eContainer.eContainer as InternalNode).element as ModelElementContainer
		container.GUIBranchesMerged.toMap([name],[name])
	}
}