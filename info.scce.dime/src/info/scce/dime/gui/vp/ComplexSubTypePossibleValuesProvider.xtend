package info.scce.dime.gui.vp

import info.scce.dime.api.DIMEValuesProvider
import info.scce.dime.data.data.EnumType
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.IS

class ComplexSubTypePossibleValuesProvider extends DIMEValuesProvider<IS, String> {
	
	override getPossibleValues(IS conditionIs) {
		val dataType = switch it: conditionIs.sourceElement {
			ComplexVariable: dataType.originalType
			ComplexAttribute: attribute.dataType.originalType
		}
		if (dataType instanceof EnumType) {
			dataType.enumLiterals.toMap([id], [name])
		} else {
			dataType.knownSubTypes.toMap([id], [name])
		}
	}
}