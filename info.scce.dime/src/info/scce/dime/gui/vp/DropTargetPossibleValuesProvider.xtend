package info.scce.dime.gui.vp

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import info.scce.dime.gui.gui.DropTarget

class DropTargetPossibleValuesProvider extends CincoValuesProvider<DropTarget, String> {
	
	override getPossibleValues(DropTarget arg0) {
		ReferenceRegistry.instance.lookup(info.scce.dime.gui.gui.Table).toMap([id], [name]);
	}	
}