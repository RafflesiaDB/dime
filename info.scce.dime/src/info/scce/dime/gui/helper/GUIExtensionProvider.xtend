package info.scce.dime.gui.helper

class GUIExtensionProvider {
	private static GUIExtension guiex;
	private static boolean REFRESH = true
	
	public synchronized static def guiextension(){
		if(guiex==null||REFRESH) {
			GUIExtensionProvider.init
		}
		guiex
	}
	
	public synchronized static def init(){
		guiex = new GUIExtension
	}
	
	public synchronized static def void enableRefresh(){
		REFRESH = true
	}
	
	public synchronized static def void disableRefresh(){
		REFRESH = false
	}
	
}