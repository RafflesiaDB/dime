package info.scce.dime.gui.helper

import org.eclipse.xtend.lib.annotations.Accessors
import info.scce.dime.data.data.PrimitiveType
import org.eclipse.emf.ecore.EObject

@Accessors class PrimitiveGUIBranchPort extends GUIBranchPort {
	
	val PrimitiveType type
	
	new(EObject portNode) {
		super(portNode)
		this.type = portNode.primitiveType
	}
	
}