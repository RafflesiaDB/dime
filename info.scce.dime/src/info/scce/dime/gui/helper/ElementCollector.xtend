package info.scce.dime.gui.helper

import graphmodel.Node

/**
 * Utility methods to collect model elements of a certain type
 * respecting their generation order as well as their vertical
 * and horizontal position.
 */
 // TODO Delete as soon as the calling classes have been translated to Xtend classes. It is obsolete, as all necessary extension methods moved to GUIExtension.
class ElementCollector {
	
	/**
	 * Sorts a list of nodes according to their horizontal position.
	 * 
	 * @param elements
	 * @return
	 */
	// TODO Delete because calling 'sortBy[x]' is shorter and more descriptive than calling 'getElementsH'
	static def <T extends Node> getElementsH(Iterable<T> elements) {
		elements.sortBy[x]
	}
	
	/**
	 * Sorts a list of nodes according to their vertical position.
	 * 
	 * @param elements
	 * @return
	 */
	 // TODO Delete because calling 'sortBy[y]' is shorter and more descriptive than calling 'getElementsV'
	static def <T extends Node> getElementsV(Iterable<T> elements) {
		elements.sortBy[y]
	}
	
}