package info.scce.dime.gui.helper

import org.eclipse.xtend.lib.annotations.Accessors
import info.scce.dime.data.data.Type
import org.eclipse.emf.ecore.EObject
import info.scce.dime.gui.gui.AddComplexToSubmission
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.GUIPlugin
import info.scce.dime.gUIPlugin.Function

@Accessors class ComplexGUIBranchPort extends GUIBranchPort {
	
	Type type
	
	new(EObject portNode) {
		super(portNode)
		this.type = portNode.complexType
	}
	
	def getTypeViewData() {
		switch selectiveNode : portSelectiveNode {
			// GUI
			AddComplexToSubmission: selectiveNode.sourceElement
			ComplexAttribute: selectiveNode.attribute
			ComplexOutputPort 
				case selectiveNode.container instanceof Branch
					&& ((selectiveNode.container as Branch).container instanceof GUIPlugin):
				{
				val guiPlugin = (selectiveNode.container as Branch).container as GUIPlugin
				val fun = guiPlugin.function as Function
				val param = fun.outputs.flatMap[parameters].findFirst[it.name == selectiveNode.name]
				if (param === null) {
					throw new IllegalStateException('''port could not be found on plugin «guiPlugin.label» in «guiPlugin.rootElement.title»''')
				}
				else param
			}
			
			default: selectiveNode
		}
	}
}