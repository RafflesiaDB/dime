package info.scce.dime.gui.helper

import info.scce.dime.data.data.Type
import info.scce.dime.gui.gui.AddComplexToSubmission
import info.scce.dime.gui.gui.AddPrimitiveToSubmission
import info.scce.dime.gui.gui.AddToSubmission
import info.scce.dime.gui.gui.Button
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.OutputGeneric
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.PrimitiveType
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.Variable
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors abstract class GUIBranchPort {
	
	val String name
	val EObject portNode
	EObject portSelectiveNode
	val boolean isList
	
	new(EObject portNode) {
		this.name = portNode.portName
		this.portNode = portNode
		this.portSelectiveNode = portNode
		this.isList = portNode.isList
	}
	
	static def toGUIBranchPort(EObject elm) {
		switch it:elm {
			case isComplex: new ComplexGUIBranchPort(elm)
			default: new PrimitiveGUIBranchPort(elm)
		}
	}
	
	static def String getPortName(EObject it) {
		switch it {
			// Nodes
			PrimitiveAttribute: it.attribute.name
			ComplexAttribute: it.attribute.name
			PrimitiveInputPort: it.name
			ComplexInputPort: it.name
			ComplexOutputPort: it.name
			PrimitiveOutputPort: it.name
			OutputGeneric: it.name
			Variable: it.name
			// Edges
			AddToSubmission: if (!outputName.nullOrEmpty) outputName else sourceElement.portName
			// GUI Plugin
			info.scce.dime.gUIPlugin.AbstractParameter: it.name
			// Process
			info.scce.dime.process.process.Input: it.name
		}
	}
	
	static def boolean isList(EObject port) {
		switch it:port {
			// Nodes
			PrimitiveAttribute: it.attribute.isList
			ComplexAttribute: it.attribute.isList
			PrimitiveInputPort: it.isList
			ComplexInputPort: it.isList
			ComplexOutputPort: it.isList
			PrimitiveOutputPort: it.isList
			OutputGeneric: it.isList
			Variable: it.isList
			// Edges
			AddToSubmission: sourceElement.isList
			// GUI Plugin
			info.scce.dime.gUIPlugin.AbstractParameter: it.isList
			// Process
			info.scce.dime.process.process.InputPort: it.isList
			default: false
		}
	}
	
	static def boolean isComplex(EObject port) {
		#{
			// Nodes
			ComplexVariable,
			ComplexAttribute,
			ComplexOutputPort,
			// Edges
			AddComplexToSubmission,
			// GUI Plugin
			info.scce.dime.gUIPlugin.ComplexParameter,
			// Process
			info.scce.dime.process.process.ComplexInputPort
			
		}.exists[isInstance(port)]
	}
	
	static def Type getComplexType(EObject port) {
		switch it:port {
			// Nodes
			ComplexVariable: dataType.originalType
			ComplexAttribute: attribute.dataType.originalType
			ComplexOutputPort: dataType.originalType
			// GUI Plugin
			info.scce.dime.gUIPlugin.ComplexParameter: (type as Type).originalType
			// Edges
			AddComplexToSubmission: sourceElement.complexType
			// Process
			info.scce.dime.process.process.ComplexInputPort: dataType.originalType
		}
	}
	
	static def boolean isPrimitive(EObject port) {
		#{
			// Nodes
			PrimitiveVariable,
			PrimitiveAttribute,
			PrimitiveInputPort,
			PrimitiveOutputPort,
			// Edges
			AddPrimitiveToSubmission,
			// GUI Plugin
			info.scce.dime.gUIPlugin.PrimitiveParameter,
			// Process
			info.scce.dime.process.process.PrimitiveInputPort,
			info.scce.dime.process.process.InputStatic
			
		}.exists[isInstance(port)]
	}
	
	static def info.scce.dime.data.data.PrimitiveType getPrimitiveType(EObject port) {
		switch it:port {
			// Nodes
			PrimitiveVariable: dataType.toData
			PrimitiveAttribute: attribute.dataType
			PrimitiveInputPort: dataType.toData
			PrimitiveOutputPort: dataType.toData
			// GUI Plugin
			info.scce.dime.gUIPlugin.PrimitiveParameter: type.toData
			// Edges
			AddPrimitiveToSubmission: sourceElement.primitiveType
			// Process
			info.scce.dime.process.process.PrimitiveInputPort: dataType.toData
			info.scce.dime.process.process.TextInputStatic: info.scce.dime.data.data.PrimitiveType.TEXT
			info.scce.dime.process.process.IntegerInputStatic: info.scce.dime.data.data.PrimitiveType.INTEGER
			info.scce.dime.process.process.RealInputStatic: info.scce.dime.data.data.PrimitiveType.REAL
			info.scce.dime.process.process.BooleanInputStatic: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			info.scce.dime.process.process.TimestampInputStatic: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}
	
	static def dispatch List<GUIBranchPort> getPorts(info.scce.dime.process.process.EndSIB endSIB) {
		newArrayList => [ addAll(
			endSIB.inputs.map[toGUIBranchPort]
		)]
	}

	static def dispatch List<GUIBranchPort> getPorts(Button button) {
		newArrayList => [
			if (button.isBranchable) addAll(
				button.getIncoming(AddToSubmission)
					.map[switch it {
						case outputName.nullOrEmpty: sourceElement
						default: it
					}]
					.map[toGUIBranchPort]
			)
		]
	}

	static def dispatch List<GUIBranchPort> getPorts(info.scce.dime.gUIPlugin.Output output) {
		newArrayList => [ addAll(
			output.parameters
				.filter[!(it instanceof info.scce.dime.gUIPlugin.GenericParameter)]
				.map[toGUIBranchPort]
		)]
	}
	
	static def dispatch List<GUIBranchPort> getPorts(Branch branch) {
		newArrayList => [ addAll(
			branch.outputPorts.map[toGUIBranchPort]
		)]
	}
	
	static def boolean isBranchable(Button it) {
		!isDisabled && (options === null || options.staticURL.nullOrEmpty)
	}
	
	// TODO create a single static global datatype mapper for this
	static def toData(PrimitiveType it) {
		switch it {
			case BOOLEAN: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			case FILE: info.scce.dime.data.data.PrimitiveType.FILE
			case INTEGER: info.scce.dime.data.data.PrimitiveType.INTEGER
			case REAL: info.scce.dime.data.data.PrimitiveType.REAL
			case TEXT: info.scce.dime.data.data.PrimitiveType.TEXT
			case TIMESTAMP: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}
	
	// TODO create a single static global datatype mapper for this
	static def toData(info.scce.dime.gUIPlugin.PrimitiveType it) {
		switch it {
			case info.scce.dime.gUIPlugin.PrimitiveType.BOOLEAN: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			case info.scce.dime.gUIPlugin.PrimitiveType.INTEGER: info.scce.dime.data.data.PrimitiveType.INTEGER
			case info.scce.dime.gUIPlugin.PrimitiveType.REAL: info.scce.dime.data.data.PrimitiveType.REAL
			case info.scce.dime.gUIPlugin.PrimitiveType.TEXT: info.scce.dime.data.data.PrimitiveType.TEXT
			case info.scce.dime.gUIPlugin.PrimitiveType.TIMESTAMP: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}
	
	// TODO create a single static global datatype mapper for this
	static def toData(info.scce.dime.process.process.PrimitiveType it) {
		switch it {
			case info.scce.dime.process.process.PrimitiveType.BOOLEAN: info.scce.dime.data.data.PrimitiveType.BOOLEAN
			case info.scce.dime.process.process.PrimitiveType.FILE: info.scce.dime.data.data.PrimitiveType.FILE
			case info.scce.dime.process.process.PrimitiveType.INTEGER: info.scce.dime.data.data.PrimitiveType.INTEGER
			case info.scce.dime.process.process.PrimitiveType.REAL: info.scce.dime.data.data.PrimitiveType.REAL
			case info.scce.dime.process.process.PrimitiveType.TEXT: info.scce.dime.data.data.PrimitiveType.TEXT
			case info.scce.dime.process.process.PrimitiveType.TIMESTAMP: info.scce.dime.data.data.PrimitiveType.TIMESTAMP
		}
	}
	
	// TODO create a single static global datatype mapper for this
	static def Type getOriginalType(Type type) {
		switch it : type {
			info.scce.dime.data.data.ReferencedType: referencedType.originalType
			info.scce.dime.data.data.ReferencedUserType: referencedType.originalType
			info.scce.dime.data.data.ReferencedEnumType: referencedType.originalType
			default: it	
		}
		
	}
}