package info.scce.dime.gui.hooks

import com.google.common.collect.Iterables
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.OutputPort
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.SIB
import info.scce.dime.gui.gui.Variable
import java.util.List
import info.scce.dime.gui.gui.Placeholder
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.ComplexGUIBranchPort
import info.scce.dime.gui.helper.PrimitiveGUIBranchPort
import info.scce.dime.gui.gui.ExtensionContext

/** 
 * The GUI SIB hook is used create the corresponding ports and branches
 * for the new GUI SIB
 * @author zweihoff
 */
class GUISIBHook extends AbstractPostCreateSIBHook<GUISIB> {
	val OFFSET = 30
	/** 
	 * Creates the corresponding GUI SIB input ports for every input variable
	 * of the referenced GUI model.
	 * For every button and branch of embedded SIBs, a branch with the corresponding ports is added.
	 */
	override void postCreate(GUISIB sib) {
		var GUI lcGUI = sib.getGui()
		var Iterable<Variable> inputs = lcGUI.inputVariables
		val branches = lcGUI.GUIBranchesMerged
		var Iterable<Event> events = _gUIExtension.events(lcGUI)
		sib.setLabel(lcGUI.getTitle())
		init(sib, lcGUI.getTitle(), Iterables::size(inputs))
		inputs.forEach([v|addGUIInput(v)])
		branches.forEach[addBranch]
		// add events
		events.forEach([e|addEventBranches(e.getName(), e.getOutputPorts())])
		finish()
		
		for (ph : lcGUI.find(Placeholder)) {
			sib.addArgument(ph.name)	
		}
		
		// Trigger Layouter
		layout(sib)
		if(sib.container instanceof ExtensionContext){
			var eContext = sib.container as ExtensionContext
			layoutContext(eContext)
		}
	}
	


	def layoutContext(ExtensionContext node) {
		var maxHeight = maxHeight(node)
		node.setHeight(maxHeight + OFFSET)

	}

	def maxHeight(ExtensionContext context) {
		var maxLowerBound = 50;
		for (node : context.nodes) {
			if (node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height
			}
		}
		return maxLowerBound
	}

	def void layout(SIB sib) {
		var CreateComponentHook cch = new CreateComponentHook()
		cch.postCreate(sib)
	}

	def void addArgument(GUISIB csib,String name) {
//		if (csib.getArguments().isEmpty()) {
			var Argument ca = ((csib as GUISIB)).newArgument(1, csib.getHeight())
			ca.setBlockName(name)
//		}
	}

	def void addGUIInput(Variable ^var) {
		// Input Ports
		if (^var instanceof ComplexVariable) {
			var ComplexVariable cVar = (^var as ComplexVariable)
			addComplexInputPort(cVar.getName(), cVar.getDataType(), cVar.isIsList())
		} else if (^var instanceof PrimitiveVariable) {
			var PrimitiveVariable pVar = (^var as PrimitiveVariable)
			addPrimitiveInputPort(pVar.getName(), pVar.getDataType(), pVar.isIsList())
		} else {
			throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation")
		}
	}

	def void addEventBranches(String name, List<OutputPort> outputPorts) {
		newEventListener(name, outputPorts.size())
		for (OutputPort port : outputPorts) {
			if (port instanceof ComplexOutputPort) {
				addEventComplexInputPort(port.getName(), ((port as ComplexOutputPort)).getDataType(), port.isIsList())
			}
			if (port instanceof PrimitiveOutputPort) {
				var PrimitiveOutputPort pVar = (port as PrimitiveOutputPort)
				addEventPrimitiveInputPort(pVar.getName(), pVar.getDataType(), pVar.isIsList())
			}
		}
	}

	def void addBranch(GUIBranch branch) {
		newBranch(branch.name, branch.ports.size)
		for (branchPort : branch.ports) {
			if (branchPort instanceof ComplexGUIBranchPort) {
				addComplexOutputPort(branchPort.name, branchPort.type, branchPort.isList)
			} else if (branchPort instanceof PrimitiveGUIBranchPort) {
				addPrimitiveOutputPort(branchPort.name, branchPort.type.toGUI, branchPort.isList)
			}
		}
	}
}
