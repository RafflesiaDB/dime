package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.action.CincoPostAttributeChangeHook
import info.scce.dime.gui.gui.ProgressBar
import org.eclipse.emf.ecore.EStructuralFeature

class ShowPercentValueChange extends CincoPostAttributeChangeHook<ProgressBar>{
	
	override canHandleChange(ProgressBar bar, EStructuralFeature arg1) {
		true
	}
	
	override handleChange(ProgressBar bar, EStructuralFeature e) {
		if(e.name == "percent"){
			if(bar.percent){
				bar.setValue("30%")
			}else if(!bar.percent){
				bar.setValue("30/100")
			}
		}
	}
	
}