package info.scce.dime.gui.hooks

import graphmodel.Node
import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.process.helper.LayoutConstants

/** 
 * Adds input ports to a given IF SIB
 * @author zweihoff
 */
class ComplexInputPostCreate extends DIMEPostCreateHook<ComplexInputPort> {
	
	override postCreate(ComplexInputPort node) {
		if(node.container instanceof ControlSIB){
			
			
			val cIf = node.container as ControlSIB
			node.name = '''«node.dataType.name.toFirstLower»«cIf.IOs.size»'''
			node.width = (node.name.length + node.dataType.name.length+2) * 7
			//calculate next position
			val posY = 50+(cIf.IOs.size)*LayoutConstants.PORT_SPACE
			//move port to position
			node.moveTo(cIf,LayoutConstants.PORT_X,posY)
			//resize SIB
			cIf.resize(cIf.width,cIf.height+LayoutConstants.PORT_SPACE)
			//move arguments
			cIf.arguments.forEach[n|{
				n.y= n.y+LayoutConstants.PORT_SPACE
			}]
			//trigger layouter
			var CreateComponentHook cch = new CreateComponentHook()
			//cch.postCreate(node)
			cch.postCreate(cIf)
		}
		else{
			//cCIP.delete
		}
		
	}
	
}
