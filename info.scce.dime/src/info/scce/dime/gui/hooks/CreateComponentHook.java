package info.scce.dime.gui.hooks;

import graphmodel.Container;
import graphmodel.Node;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.gui.factory.GUIFactory;
import info.scce.dime.gui.gui.Alert;
import info.scce.dime.gui.gui.Argument;
import info.scce.dime.gui.gui.Badge;
import info.scce.dime.gui.gui.BaseElement;
import info.scce.dime.gui.gui.Button;
import info.scce.dime.gui.gui.ButtonGroup;
import info.scce.dime.gui.gui.ButtonToolbar;
import info.scce.dime.gui.gui.Col;
import info.scce.dime.gui.gui.ControlSIB;
import info.scce.dime.gui.gui.Description;
import info.scce.dime.gui.gui.Descriptionentry;
import info.scce.dime.gui.gui.Dropdown;
import info.scce.dime.gui.gui.Field;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.FormEntry;
import info.scce.dime.gui.gui.GuiFactory;
import info.scce.dime.gui.gui.Headline;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.gui.Input;
import info.scce.dime.gui.gui.Jumbotron;
import info.scce.dime.gui.gui.LinkProcessSIB;
import info.scce.dime.gui.gui.LinkSIB;
import info.scce.dime.gui.gui.Listentry;
import info.scce.dime.gui.gui.Listing;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.Panel;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.gui.gui.ProgressBar;
import info.scce.dime.gui.gui.Row;
import info.scce.dime.gui.gui.Select;
import info.scce.dime.gui.gui.StaticContent;
import info.scce.dime.gui.gui.Tabbing;
import info.scce.dime.gui.gui.TableEntry;
import info.scce.dime.gui.gui.Template;
import info.scce.dime.gui.gui.Text;
import info.scce.dime.gui.gui.Thumbnail;
import info.scce.dime.gui.gui.TopNavBar;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.ProcessType;

/**
 * The create component hook is used to position the new create GUI model component
 * depended on the parent component type and whether its child components are
 * positioned in horizontal or vertical order.
 * In addition to this, the new component is prepared with default values.
 * @author zweihoff
 *
 */
public class CreateComponentHook extends DIMEPostCreateHook<Node>{

	public static int V_OFFSET_MIN = 30;
	public static int H_OFFSET_MIN = 10;
	public static int H_OFFSET = 10;
	public static int V_OFFSET = 10;
	public static int HEIGHT_MIN = 60;
	public static int WIDTH_MIN = 100;
	
	public static int SIGN_1_X = 2;
	public static int SIGN_2_X = 17;
	public static int SIGN_Y = 2;
	
	/**
	 * Positions the new create GUI model component
	 * depended on the parent component type and whether its child components are
	 * positioned in horizontal or vertical order.
	 * In addition to this, the new component is prepared with default values.
	 */
	@Override
	public void postCreate(Node node) {
		
		if(isDisabledForHook(node)){
			return;
		}
		
		try {
			if(forbiddenNode(node)){
				if(node instanceof BaseElement){
					BaseElement cb = (BaseElement) node;
					cb.delete();
					return;
				}
			}
			
			//Set Position
			if(node.getContainer() !=null){
				if(node.getContainer() instanceof Container){
					//Set the Moving signs depending on the container
					setMovingSigns((Container)node); 
					// Position the new element
					int x = node.getX();
					int y = node.getY();
//					int x=H_OFFSET_MIN,y=LayoutHelper.getVOff((Container) node.getContainer());
					if(LayoutHelper.isHorizontalComponent((Container)node.getContainer())){
						y = V_OFFSET_MIN;
//						x=LayoutHelper.getHorizontalWidth((Container)node.getContainer(),node);
					}
					else{
						x = H_OFFSET_MIN;
//						y=LayoutHelper.getVerticalHeight((Container)node.getContainer(),node);
					}
					node.setX(x);
					node.setY(y);
					//Resize parent Container if necessary
					if(node.getContainer() instanceof Container){
						LayoutHelper.layoutComponent((Container)node.getContainer());
					}
					
					//Add Static Content to depending
					
					//Add Children
					addDependingElements((Container)node);
				}
			}
			addStaticContent((Container)node);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
	}
	
	private boolean isDisabledForHook(Node node) {
		if(node.getContainer() instanceof ControlSIB){
			//return true;
		}
		return false;
	}

	/**
	 * Checks, if the created node is not allowed in GUI models
	 * and deletes it.
	 * @param node
	 * @return
	 */
	private boolean forbiddenNode(Node node) {
		if(node instanceof ProcessSIB){
			ProcessSIB ei = (ProcessSIB) node;
			Process p = (Process) ei.getProMod();
			if(p.getProcessType()!=ProcessType.BASIC)return true;
		}
		return false;
	}

	/**
	 * Adds the moving sign nodes to the given container
	 * depended on the parent container
	 * @param cContainer
	 */
	private void setMovingSigns(Container cContainer) {
		if(cContainer instanceof MovableContainer && !(cContainer instanceof Argument)){
			MovableContainer cMovableContainer = (MovableContainer) cContainer;
			if(cMovableContainer.getMoveSigns().isEmpty()){
				if(LayoutHelper.isHorizontalComponent((Container)cContainer.getContainer())){
					cMovableContainer.newMoveLeft(2, 2);
					cMovableContainer.newMoveRight(10, 2);
				}
				else {
					cMovableContainer.newMoveUp(2, 2);
					cMovableContainer.newMoveDown(2, 10);
				}				
			}
		}
	}
	
	/**
	 * Extends the given container depended on its type
	 * with specific default content
	 * @param cContainer
	 */
	private void addDependingElements(Container cContainer) {
		if(cContainer instanceof Row) {
			Row cRow= (Row) cContainer;
			cRow.newCol(1, 1);
		}
		if(cContainer instanceof Form) {
			Form cForm = (Form) cContainer;
			Button btn = cForm.newButton(1, 1);
			btn.setLabel("Submit");
		}
		if(cContainer instanceof Listing) {
			Listing cList = (Listing) cContainer;
			cList.newListentry(1, 1);
		}
		if(cContainer instanceof Description) {
			Description cDescription = (Description) cContainer;
			cDescription.newDescriptionentry(1, 1);
		}
		if(cContainer instanceof ButtonToolbar) {
			ButtonToolbar cButtonToolbar = (ButtonToolbar) cContainer;
			cButtonToolbar.newButtonGroup(1, 1);
		}
		if(cContainer instanceof ButtonGroup) {
			ButtonGroup cButtonGroup = (ButtonGroup) cContainer;
			cButtonGroup.newButton(1, 1);
		}
		if(cContainer instanceof Dropdown) {
			Dropdown cButtonGroup = (Dropdown) cContainer;
			cButtonGroup.newButton(1, 1);
		}
		if(cContainer instanceof Thumbnail) {
			Thumbnail cThumbnail = (Thumbnail) cContainer;
			cThumbnail.newImage(1, 1);
		}
		if(cContainer instanceof Jumbotron) {
			Jumbotron cJumbotron = (Jumbotron) cContainer;
			cJumbotron.newRow(1, 1);
		}
		if(cContainer instanceof Tabbing) {
			Tabbing cTabbing = (Tabbing) cContainer;
			cTabbing.newTab(1, 1);
		}
		if(cContainer instanceof TableEntry) {
			TableEntry cTabbing = (TableEntry) cContainer;
			cTabbing.setLabel("Column");
		}
		if(cContainer instanceof TopNavBar){
			TopNavBar ct = (TopNavBar)cContainer;
			ct.setIcon(GUIFactory.eINSTANCE.createIcon());
			ct.newLeftNavBarPart(0, 0);
			ct.newRightNavBarPart(50, 0);
		}
		if(cContainer instanceof Alert){
			Alert ct = (Alert)cContainer;
			ct.newText(0, 0);
		}
	}
	
	/**
	 * Adds static content to the container depended on its type
	 * @param cContainer
	 */
	private void addStaticContent(Container cContainer)
	{
		if(cContainer instanceof MovableContainer) {
			MovableContainer t = (MovableContainer) cContainer;
			t.setGeneralStyle(GuiFactory.eINSTANCE.createGeneralStyling());
		}
		if(cContainer instanceof Template) {
			Template t = (Template) cContainer;
			t.setGeneralStyle(GuiFactory.eINSTANCE.createGeneralStyling());
		}
		if(cContainer instanceof File) {
			File t = (File) cContainer;
			t.setLabel("Download");
		}
		if(cContainer instanceof Col) {
			Col t = (Col) cContainer;
			t.setLayout(GuiFactory.eINSTANCE.createLayout());
		}
		if(cContainer instanceof FormEntry) {
			FormEntry t = (FormEntry) cContainer;
			t.setIcon(GuiFactory.eINSTANCE.createIcon());
		}
		if(cContainer instanceof Input) {
			Input t = (Input) cContainer;
			t.setValidation(GuiFactory.eINSTANCE.createFormFieldValidation());
			t.setAdditionalOptions(GuiFactory.eINSTANCE.createFormFieldAdditionalOptions());
		}
		if(cContainer instanceof File) {
			File t = (File) cContainer;
			t.setStyling(GuiFactory.eINSTANCE.createFormFieldStyling());
		}
		if(cContainer instanceof Field) {
			Field t = (Field) cContainer;
			t.setStyling(GuiFactory.eINSTANCE.createFormFieldStyling());
		}
		if(cContainer instanceof Select) {
			Select t = (Select) cContainer;
			t.setStyling(GuiFactory.eINSTANCE.createSelectFormFieldStyling());
		}
		if(cContainer instanceof Button) {
			Button t = (Button) cContainer;
			t.setLabel("Label");
			t.setStyling(GuiFactory.eINSTANCE.createButtonStyling());
			t.setOptions(GuiFactory.eINSTANCE.createButtonOptions());
		}
		if(cContainer instanceof LinkSIB) {
			LinkSIB t = (LinkSIB) cContainer;
			t.setStyling(GuiFactory.eINSTANCE.createButtonStyling());
			t.setOptions(GuiFactory.eINSTANCE.createButtonOptions());
			t.setIcon(GuiFactory.eINSTANCE.createIcon());
		}
		if(cContainer instanceof Dropdown) {
			Dropdown t = (Dropdown) cContainer;
			t.setLabel("Dropdown");
			t.setStyling(GuiFactory.eINSTANCE.createButtonStyling());
		}
		if(cContainer instanceof Image) {
			Image t = (Image) cContainer;
			t.setStyling(GuiFactory.eINSTANCE.createImageStyling());
		}
		if(cContainer instanceof ProgressBar) {
			ProgressBar t = (ProgressBar) cContainer;
			t.setStyling(GuiFactory.eINSTANCE.createProgressBarStyling());
		}
		if(cContainer instanceof Panel) {
			Panel t = (Panel) cContainer;
			t.setColoring(GuiFactory.eINSTANCE.createComponentColoring());
		}
		if(cContainer instanceof Text) {
			Text t = (Text) cContainer;
			t.getContent().add(createStaticContent("Text"));
		}
		if(cContainer instanceof Headline) {
			Headline t = (Headline) cContainer;
			t.getContent().add(createStaticContent("Headline"));
		}
		if(cContainer instanceof Badge) {
			Badge t = (Badge) cContainer;
			t.getContent().add(createStaticContent("Badge"));
			t.setColor(GuiFactory.eINSTANCE.createComponentColoring().getColor());
		}
		if(cContainer instanceof Listentry) {
			Listentry t = (Listentry) cContainer;
			t.getContent().add(createStaticContent("Entry"));
		}
		if(cContainer instanceof Button) {
			Button cb = (Button)cContainer;
			if(cb.getContainer() != null){
				if(!(cb.getContainer() instanceof Form)) {
				}
			}
		}
		if(cContainer instanceof ProcessSIB) {
			ProcessSIB t = (ProcessSIB) cContainer;
			info.scce.dime.process.process.Process p = (info.scce.dime.process.process.Process) t.getProMod();
			t.setLabel(p.getModelName());
		}
		if(cContainer instanceof LinkProcessSIB) {
			LinkProcessSIB t = (LinkProcessSIB) cContainer;
			URLProcess p = (URLProcess) t.getProMod();
			t.setLabel(p.getUrl());
		}
		if(cContainer instanceof Descriptionentry) {
			Descriptionentry t = (Descriptionentry) cContainer;
			t.getDescription().add(createStaticContent("Description"));
			t.getContent().add(createStaticContent("Entry"));
		}
	}
	
	/**
	 * Returns a new static content type object
	 * @param content
	 * @return
	 */
	private StaticContent createStaticContent(String content)
	{
		StaticContent sc = GuiFactory.eINSTANCE.createStaticContent();
		sc.setRawContent(content);
		sc.setStyling(GuiFactory.eINSTANCE.createTextStyling());
		return sc;
	}
	
}
