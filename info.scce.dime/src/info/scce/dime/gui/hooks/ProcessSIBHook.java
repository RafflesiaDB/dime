package info.scce.dime.gui.hooks;

import java.util.List;
import java.util.stream.Collectors;

import info.scce.dime.gui.gui.PrimitiveType;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.process.process.BooleanInputStatic;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.IntegerInputStatic;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.RealInputStatic;
import info.scce.dime.process.process.TimestampInputStatic;

/**
 * The GUI SIB hook is used create the corresponding ports and branches
 * for the new GUI SIB
 * @author zweihoff
 *
 */
public class ProcessSIBHook extends AbstractPostCreateSIBHook<ProcessSIB> {

	/**
	 * Creates the corresponding GUI SIB input ports for every input variable
	 * of the referenced GUI model.
	 * For every button and branch of embedded SIBs, a branch with the corresponding ports is added.
	 * 
	 */
	@Override
	public void postCreate(ProcessSIB sib) {
     		Process lcProcess = (Process) sib.getProMod();
     		List<Output> inputs = lcProcess.getStartSIBs().stream().flatMap(n->n.getOutputs().stream()).collect(Collectors.toList());
     		
     		
     		
     		init(sib, lcProcess.getModelName(), inputs.size());
     		
     		     		
     		//Input Ports
     		
     		for (Output var : inputs) {
     			if (var instanceof ComplexOutputPort) {
     				ComplexOutputPort cVar = (ComplexOutputPort) var;
     				addComplexInputPort(cVar.getName(), cVar.getDataType(), cVar.isIsList());
     			}
     			else if (var instanceof PrimitiveOutputPort) {
     				PrimitiveOutputPort pVar = (PrimitiveOutputPort) var;
     				addPrimitiveInputPort(pVar.getName(), toGUIPT(pVar.getDataType()), pVar.isIsList());
     			}
     			else {
     				throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation");
     			}
     		}
     		
     		//Collecting Branches
     		List<EndSIB> endSibs = lcProcess.getEndSIBs();
     		
     		for (EndSIB endSib : endSibs) {
	    		int outputPorts = endSib.getInputs().size();
		    	newBranch(endSib.getBranchName(), outputPorts);
		    	//collect ports for branches
		    	for (info.scce.dime.process.process.Input input : endSib.getInputs()) {
	    			if(input instanceof ComplexInputPort) {
	    				ComplexInputPort cVar = (ComplexInputPort) input;
	    					addComplexOutputPort(cVar.getName(), cVar.getDataType(), cVar.isIsList());
	    			}
	    			else if(input instanceof PrimitiveInputPort){
    					PrimitiveInputPort pVar = (PrimitiveInputPort) input;
    					addPrimitiveOutputPort(pVar.getName(), toGUIPT(pVar.getDataType()), pVar.isIsList());
	    			}
	    			else if(input instanceof InputStatic){
	    				InputStatic pVar = (InputStatic) input;
	    				PrimitiveType pt = PrimitiveType.TEXT;
	    				if(pVar instanceof IntegerInputStatic) pt = PrimitiveType.INTEGER;
	    				if(pVar instanceof RealInputStatic) pt = PrimitiveType.REAL;
	    				if(pVar instanceof TimestampInputStatic) pt = PrimitiveType.TIMESTAMP;
	    				if(pVar instanceof BooleanInputStatic) pt = PrimitiveType.BOOLEAN;
    					addPrimitiveOutputPort(pVar.getName(), pt, false);
	    			}
		    	}
 		}
     		
 		sib.setLabel(lcProcess.getModelName());
	     		
	    finish();
	    //Trigger Layouter
	    CreateComponentHook cch = new CreateComponentHook();
	    cch.postCreate(sib);
		    
	}

	private PrimitiveType toGUIPT(info.scce.dime.process.process.PrimitiveType dataType) {
		switch(dataType) {
		case BOOLEAN:return PrimitiveType.BOOLEAN;
		case REAL:return PrimitiveType.REAL;
		case INTEGER:return PrimitiveType.INTEGER;
		case TEXT:return PrimitiveType.TEXT;
		case TIMESTAMP:return PrimitiveType.TIMESTAMP;
		default :return PrimitiveType.FILE;
		}
	}
	
	
}
