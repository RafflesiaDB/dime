package info.scce.dime.gui.hooks

import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.OutputPort
import info.scce.dime.gui.gui.Placeholder
import java.util.HashMap
import java.util.HashSet
import java.util.LinkedList
import java.util.List
import info.scce.dime.gui.helper.GUIExtension

/** 
 * The GUI SIB hook is used create the corresponding ports and branches
 * for the new GUI SIB
 * @author zweihoff
 */
class DispatchedGUISIBHook extends GUISIBHook {
	
	
	/** 
	 * Creates the corresponding GUI SIB input ports for every input variable
	 * of the referenced GUI model.
	 * For every button and branch of embedded SIBs, a branch with the corresponding ports is added.
	 */
	override void postCreate(GUISIB sib) {
		val parentGUI = sib.gui
		val allSubs = parentGUI.subGUIs
		//calculate inputs of parent
		
		val dispatchableInputs = new HashSet
		parentGUI.inputVariables.forEach[n|dispatchableInputs.add(n)]
		
		val dispatchableBranches = parentGUI.dispatchedOutputs
		// add branches of parent GUI model
		
		val dispatchableEvents= new HashMap<String,List<OutputPort>>
		// add events of parent GUI model
		parentGUI.events.forEach[e|dispatchableEvents.put(e.name,e.outputPorts)]
		
		val dispatchablePlaceholders = new HashSet<String>
		
		parentGUI.find(Placeholder).forEach[n|dispatchablePlaceholders.add(n.name)]
		allSubs.forEach[n|n.find(Placeholder).forEach[pl|dispatchablePlaceholders.add(pl.name)]]
		
		//calculate dispatched inputs, collected from other sub GUI models
		for(subGUI:allSubs)
		{
			//extend the input list with the inputs of all sub GUIs
			for(input:subGUI.inputVariables) {
				val alreadyKnownInput = dispatchableInputs.findFirst[n|n.name.equals(input.name)]
				if(alreadyKnownInput != null) {
					// input is known by name
					// check if the type has to be abstracted
					if(alreadyKnownInput instanceof ComplexVariable) {
						//check if the input has to be overwritten, because the found equally
						//named input has a more abstract type
						if(alreadyKnownInput.dataType.getSuperTypes.contains((input as ComplexVariable).dataType)) {
							dispatchableInputs.remove(alreadyKnownInput)
							dispatchableInputs.add(input)
						}
					}
				}
				else{
					dispatchableInputs.add(input)
				}
			}
			// merge branches
			for(event:subGUI.events){
				//if branch name is not in map put
				val name = event.name
				if(!dispatchableEvents.containsKey(name)) {
					dispatchableEvents.put(name,event.outputPorts)
				}
				else {
					val alreadyKnownEventPorts = new LinkedList
					alreadyKnownEventPorts.addAll(dispatchableEvents.get(name))
					//then add ports that are not on parent branch
					//and merge the ports that are complex and already contained
					for(port:event.outputPorts)
					{
						val alreadyKnownPort = new LinkedList
						alreadyKnownPort.addAll(alreadyKnownEventPorts.filter[n|n.name.equals(port.name)])
						if(!alreadyKnownPort.empty){
							val knownPort = alreadyKnownPort.get(0)
							if(knownPort instanceof ComplexOutputPort) {
								//port is known and port is complex
								//compare the types
								if(knownPort.dataType.superTypes.contains((port as ComplexOutputPort).dataType))
								{
									//replace the port of the branch
									alreadyKnownEventPorts.remove(knownPort)
									alreadyKnownEventPorts.add(port)
								}
								
							}
						}
						else{
							alreadyKnownEventPorts.add(port)
						}
					}
					dispatchableEvents.put(name,alreadyKnownEventPorts)
					
				}
			}

		}
		
		//val guiSIBHook = new GUISIBHook
		//init the SIB 
		init(sib,sib.gui.title,dispatchableInputs.size)
		//add dispatched inputs
		dispatchableInputs.forEach[addGUIInput]
		//add branches
		dispatchableBranches.forEach[addBranch]
		//add events
		dispatchableEvents.entrySet.forEach[e|addEventBranches(e.key,e.value)]
		//add argument
		dispatchablePlaceholders.forEach[n|addArgument(csib as GUISIB,n)]
		
		finish
		
	}
	
	def notIn(String string, List<String> strings) {
		!strings.contains(string)
	}
	def isIn(String string, List<String> strings) {
		!string.notIn(strings)
	}
}