package info.scce.dime.gui.hooks;

import org.eclipse.graphiti.mm.pictograms.Diagram;

import graphmodel.Node;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataBinding;
import info.scce.dime.gui.gui.Field;
import info.scce.dime.gui.gui.Form;
import info.scce.dime.gui.gui.FormLoadSubmit;
import info.scce.dime.gui.gui.FormSubmit;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.InputType;
import info.scce.dime.gui.gui.PrimitiveAttribute;
import info.scce.dime.gui.gui.PrimitiveType;
import info.scce.dime.gui.gui.PrimitiveVariable;

/**
 * The create data binding edge hook is used to create
 * form fields for all attributes of the source variable of the edge
 * if the edge is draw to a form component
 * @author zweihoff
 *
 */
public class CreateDataBindingEdgeHook extends DIMEPostCreateHook<DataBinding>{

	/**
	 * Creates form fields for all attributes of the source variable of the edge
	 * if the edge is draw to a form component
	 */
	@Override
	public void postCreate(DataBinding object) {
		Node target = object.getTargetElement();
		Node source = object.getSourceElement();
		GUI cGUI = object.getRootElement();
		if(target instanceof Form){
			
			Form form = (Form) target;			
			if(source instanceof PrimitiveVariable) {
				createField(cGUI, form, (PrimitiveVariable) source, object);
			}
			else if(source instanceof ComplexVariable) {
				ComplexVariable cv = (ComplexVariable) source;
				for(Attribute attribute:cv.getAttributes()) {
					if(attribute instanceof PrimitiveAttribute){
						createField(cGUI, form, (PrimitiveAttribute)attribute, object);
					}
				}
			}
			
		}
		
	}
	
	
	
	
	/**
	 * Helper method to create a form field component in the given form binded to the given primitive variable
	 * @param cGUI
	 * @param cform
	 * @param pv
	 * @param object
	 */
	private void createField(GUI cGUI,Form cform,PrimitiveVariable pv,DataBinding object)
	{
		Field field = cform.newField(0, 0);
		prepareField(field, pv.getDataType(), pv.isIsList(), pv.getName());
		
		if(object instanceof FormLoadSubmit) {
			pv.newFormLoadSubmit(field);
		}
		if(object instanceof FormSubmit) {
			pv.newFormSubmit(field);
		}
	}
	
	/**
	 * Helper method to create a form field component in the given form binded to the given primitive attribute
	 * @param cGUI
	 * @param cform
	 * @param pv
	 * @param object
	 */
	private void createField(GUI cGUI,Form cform,PrimitiveAttribute pa,DataBinding object)
	{
		info.scce.dime.data.data.PrimitiveAttribute pAttribute = pa.getAttribute();
		Field field = cform.newField(0, 0);
		
		prepareField(field, pAttribute.getDataType(), pAttribute.isIsList(), pAttribute.getName());
		
		if(object instanceof FormLoadSubmit) {
			pa.newFormLoadSubmit(field);
		}
		else if(object instanceof FormSubmit) {
			pa.newFormSubmit(field);
		}
	}
	
	/**
	 * Sets the corresponding input type of the given field
	 * @param field
	 * @param type
	 * @param isList
	 * @param variableName
	 */
	private void prepareField(Field field,PrimitiveType type,boolean isList,String variableName)
	{
		if(type == PrimitiveType.BOOLEAN)field.setInputType(InputType.CHECKBOX);
		if(type == PrimitiveType.INTEGER)field.setInputType(InputType.NUMBER);
		if(type == PrimitiveType.REAL)field.setInputType(InputType.NUMBER);
		if(type == PrimitiveType.TEXT)field.setInputType(InputType.TEXT);
		if(type == PrimitiveType.TIMESTAMP)field.setInputType(InputType.DATETIME);
		if(type == PrimitiveType.FILE)field.setInputType(InputType.ADVANCED_FILE);
		if(type == PrimitiveType.FILE)field.setInputType(InputType.SIMPLE_FILE);
		field.setLabel(variableName);
	}
	
	/**
	 * Sets the corresponding input type of the given field
	 * @param field
	 * @param type
	 * @param isList
	 * @param variableName
	 */
	private void prepareField(Field field,info.scce.dime.data.data.PrimitiveType type,boolean isList,String variableName)
	{
		if(type == info.scce.dime.data.data.PrimitiveType.BOOLEAN)field.setInputType(InputType.CHECKBOX);
		if(type == info.scce.dime.data.data.PrimitiveType.INTEGER)field.setInputType(InputType.NUMBER);
		if(type == info.scce.dime.data.data.PrimitiveType.REAL)field.setInputType(InputType.NUMBER);
		if(type == info.scce.dime.data.data.PrimitiveType.TEXT)field.setInputType(InputType.TEXT);
		if(type == info.scce.dime.data.data.PrimitiveType.TIMESTAMP)field.setInputType(InputType.DATETIME);
		if(type == info.scce.dime.data.data.PrimitiveType.FILE)field.setInputType(InputType.SIMPLE_FILE);
		if(type == info.scce.dime.data.data.PrimitiveType.FILE)field.setInputType(InputType.ADVANCED_FILE);
		field.setLabel(variableName);
	}

}