package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.ListenerContext

class Event_PostCreate extends CincoPostCreateHook<Event> {
	val OFFSET = 30;

	override postCreate(Event node) {
		if (node.getContainer() instanceof ListenerContext) {
			var listenerContext = node.getContainer() as ListenerContext
			var maxHeight = maxHeight(listenerContext);
			listenerContext.setHeight(maxHeight + OFFSET);
		}

	}

	def maxHeight(ListenerContext context) {
		var maxLowerBound = 50;
		for (node : context.nodes) {
			if (node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height
			}
		}
		return maxLowerBound
	}

}
