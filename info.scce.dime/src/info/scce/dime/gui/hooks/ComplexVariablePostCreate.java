package info.scce.dime.gui.hooks;

import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_X;

import graphmodel.Node;
import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.data.UserType;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataContext;

/**
 * The complex variable post create hook is used to set the name
 * of the variable depended on the data type.
 * @author zweihoff
 *
 */
public class ComplexVariablePostCreate extends DIMEPostCreateHook<ComplexVariable>{
final int  OFFSET = 30;
	/**
	 * Sets the name of the complex variable depended on the data type
	 */
	@Override
	public void postCreate(ComplexVariable var) {

   		
   		//TODO: why not correct return type in C-API?
   		DataContext container = (DataContext) var.getContainer();
   		
   		//Set Name of the variable to the data-type-name first lower
   		Type type = var.getDataType();
   		if(!type.getPredecessors(UserType.class).isEmpty()){
   			var.setName("currentUser");
   			var.setIsInput(false);
   			var.setIsList(false);
   		}
   		else{
   			String dataTypeName = type.getName();   			
   			var.setName(dataTypeName.substring(0,1).toLowerCase()+dataTypeName.substring(1));
   		}
   		int newAttrWidth = container.getWidth() - 2*VAR_ATTR_X;
   		
   		var.resize(newAttrWidth, var.getHeight());
   		//TODO: does not work, C-API broken, see #???? (redmine broken too atm *sigh*)
   		//cVar.moveTo(container, newAttrX, cVar.getY());
   		
   		if(var.getContainer() instanceof DataContext){
			DataContext dataContext = (DataContext)var.getContainer();
			int maxHeight = maxHeight(dataContext);
			dataContext.setHeight( maxHeight + OFFSET);
			
		}
		
	}
	
	private int maxHeight(DataContext context) {
		int maxLowerBound= 50;
		for(Node node : context.getNodes()){
			if(node.getY() + node.getHeight() > maxLowerBound){
				maxLowerBound = node.getY() + node.getHeight();
			}
		}
		return maxLowerBound;
	}

}
