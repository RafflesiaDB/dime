package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostMoveHook
import graphmodel.ModelElementContainer
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.ListenerContext

class Event_PostMove extends CincoPostMoveHook<Event>{
	val OFFSET = 30;

	override postMove(Event node, ModelElementContainer arg1, ModelElementContainer arg2, int arg3, int arg4, int arg5, int arg6) {
		if (node.getContainer() instanceof ListenerContext) {
			var listenerContext = node.getContainer() as ListenerContext
			var maxHeight = maxHeight(listenerContext);
			listenerContext.setHeight(maxHeight + OFFSET);
		}

	}

	def maxHeight(ListenerContext context) {
		var maxLowerBound = 50;
		for (node : context.nodes) {
			if (node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height
			}
		}
		return maxLowerBound
	}
	

	
	
}