package info.scce.dime.gui.hooks;

import graphmodel.Container;
import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.api.DIMEPostMoveHook;
import info.scce.dime.gui.gui.ExtensionContext;
import info.scce.dime.gui.gui.GUISIB;
import info.scce.dime.gui.gui.ListenerContext;
import info.scce.dime.gui.gui.MovableContainer;

/**
 * The move component hook is used to resize and layout the previous and the new parent component
 * of the moved component
 * @author zweihoff
 *
 */
public class MoveComponentHook extends DIMEPostMoveHook<MovableContainer>{
	static final int OFFSET =30;
	/**
	 * Resizes and layouts the previous and the new parent component
	 * of the moved component
	 */
	public void postMove(MovableContainer modelElement,ModelElementContainer sourceContainer,ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		
		if(!(modelElement instanceof Node))return;
		try {
			Node cNode = (Node) modelElement;
			int newx = x;
			int newy = y;
			//Set Position
			if(cNode.getContainer() !=null){
				
				if(LayoutHelper.isHorizontalComponent((Container)cNode.getContainer())){
					newy=LayoutHelper.V_OFFSET_MIN;
				}
				else{
					newx=LayoutHelper.H_OFFSET_MIN;
				}
				cNode.setX(newx);
				cNode.setY(newy);
				//Resize parent Container if necessary
				if(cNode.getContainer() instanceof Container){
					LayoutHelper.layoutComponent((Container)sourceContainer);
					LayoutHelper.layoutComponent((Container)targetContainer);
				}     
			}
			if(cNode instanceof GUISIB) {
				if((GUISIB)cNode instanceof ExtensionContext) {
					ExtensionContext lContext = (ExtensionContext)(GUISIB)cNode;
					layout(lContext); 
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void layout(ExtensionContext node) {
		ExtensionContext listenerContext = (ExtensionContext) node.getContainer();
		int maxHeight = maxHeight(listenerContext);
		listenerContext.setHeight(maxHeight + OFFSET);

	}

	private int maxHeight(ExtensionContext context) {
		int maxLowerBound = 50;
		for (Node node : context.getNodes()) {
			if (node.getY() + node.getHeight() > maxLowerBound) {
				maxLowerBound = node.getY() + node.getHeight();
			}
		}
		return maxLowerBound;
	}

	
}
