package info.scce.dime.gui.hooks;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;
import info.scce.dime.gui.gui.GuardSIB;
import info.scce.dime.gui.gui.PrimitiveType;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.StartSIB;

/**
 * The GUI SIB hook is used create the corresponding ports and branches
 * for the new GUI SIB
 * @author zweihoff
 *
 */
public class GuardSIBHook extends AbstractPostCreateSIBHook<GuardSIB> {

	/**
	 * Creates the corresponding GUI SIB input ports for every input variable
	 * of the referenced GUI model.
	 * For every button and branch of embedded SIBs, a branch with the corresponding ports is added.
	 * 
	 */
	@Override
	public void postCreate(GuardSIB sib) {
			csib = sib;
//			FIXME: See issue #20589
			Process lcProcess = (Process) ReferenceRegistry.getInstance().getEObject(sib.getLibraryComponentUID());
//     		Process lcProcess = (Process) sib.getProcess();
     		List<info.scce.dime.process.process.Output> inputs = new ArrayList<info.scce.dime.process.process.Output>();
     		
     		for(StartSIB start : lcProcess.getStartSIBs()) {
	     		// collect inputs
	     		inputs.addAll(start.
	     				getOutputs().
	     				stream().
	     				collect(Collectors.toList()));
     		}
     		 		
     		init(sib, lcProcess.getModelName(), inputs.size());
     		
     		//Input Ports
     		for (info.scce.dime.process.process.Output output : inputs) {
     			if (output instanceof PrimitiveOutputPort) {
     				PrimitiveOutputPort pVar = (PrimitiveOutputPort) output;
     				addPrimitiveInputPort(pVar.getName(), convert(pVar.getDataType()), pVar.isIsList());
     			}
     			else {
     				throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation");
     			}
     		}
     		
     		sib.setName(lcProcess.getModelName());

		    finish();
		    
		    //Trigger Layouter
		    CreateComponentHook cch = new CreateComponentHook();
		    cch.postCreate(sib);
	}
	
	private PrimitiveType convert(info.scce.dime.process.process.PrimitiveType pt)
	{
		switch(pt)
		{
		case BOOLEAN:return PrimitiveType.BOOLEAN;
		case FILE:return PrimitiveType.FILE;
		case TEXT:return PrimitiveType.TEXT;
		case TIMESTAMP:return PrimitiveType.TIMESTAMP;
		case REAL:return PrimitiveType.REAL;
		default: return PrimitiveType.INTEGER;
		}
	}
	

}
