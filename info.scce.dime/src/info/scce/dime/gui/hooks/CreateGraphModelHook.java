package info.scce.dime.gui.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.gui.gui.DataContext;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.Template;

/**
 * The create graph model hook is used to create the needed elements for the GUI model
 * like the first template and a data context
 * @author zweihoff
 *
 */
public class CreateGraphModelHook extends DIMEPostCreateHook<GUI>{

	private static final int MARGIN = 10;
	
	/**
	 * Creates the needed elements for the GUI model
	 * like the first template and a data context
	 */
	@Override
	public void postCreate(GUI graphModel) {
		try {
			String fileName = graphModel.eResource().getURI().lastSegment();
			String fileExtension = graphModel.eResource().getURI().fileExtension();
			String modelName = fileName.replace("." + fileExtension, "");
			
			graphModel.setTitle(modelName);
			
			DataContext dctx = graphModel.newDataContext(MARGIN, MARGIN);
			
			Template t = graphModel.newTemplate(MARGIN + dctx.getWidth() + 50, MARGIN);
			t.newRow(1, 1);
			
			//cGUI.newCExtensionPoint(10, 10);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
