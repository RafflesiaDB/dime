package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.action.CincoPostAttributeChangeHook
import graphmodel.internal.InternalContainer
import info.scce.dime.gui.gui.StaticContent
import info.scce.dime.gui.gui.internal.InternalBadge
import org.eclipse.emf.ecore.EStructuralFeature

class BadgeContentChange extends CincoPostAttributeChangeHook<StaticContent> {
	val OFFSET = 50
	override canHandleChange(StaticContent arg0, EStructuralFeature arg1) {
		true
	}

	override handleChange(StaticContent con, EStructuralFeature arg1) {
		if(arg1.name == "rawContent"){
			if(con.eContainer instanceof InternalBadge){
				var badge = con.eContainer as InternalBadge
				if (badge.container instanceof InternalContainer) {
					var container = badge.container as InternalContainer
					container.height = maxHeight(container) + OFFSET
				}
			}
		}
	}
	
	def maxHeight(InternalContainer container) {
		var maxHeight = 200
		for(element : container.modelElements){
			if(element instanceof InternalContainer){
				var baseElem = element as InternalContainer
				if(maxHeight < baseElem.height + baseElem.y){
					maxHeight = baseElem.height + baseElem.y
				}
			}
			
		}
		return maxHeight
	}

}
