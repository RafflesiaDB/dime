package info.scce.dime.gui.hooks;

import static info.scce.dime.process.helper.LayoutConstants.BRANCH_FIRST_PORT_Y;
import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_X;
import static info.scce.dime.process.helper.LayoutConstants.SIB_FIRST_PORT_Y;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Type;
import info.scce.dime.gui.gui.Branch;
import info.scce.dime.gui.gui.ComplexInputPort;
import info.scce.dime.gui.gui.ComplexOutputPort;
import info.scce.dime.gui.gui.EventListener;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.GUIPlugin;
import info.scce.dime.gui.gui.GUISIB;
import info.scce.dime.gui.gui.InputGeneric;
import info.scce.dime.gui.gui.OutputGeneric;
import info.scce.dime.gui.gui.PrimitiveInputPort;
import info.scce.dime.gui.gui.PrimitiveOutputPort;
import info.scce.dime.gui.gui.PrimitiveType;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.gui.gui.SIB;
import info.scce.dime.gui.helper.NodeLayout;

/**
 * The abstract post create SIB hook is used for embedded SIBs in GUI models
 * and provides layouting, port and branch creation
 * @author zweihoff
 *
 * @param <T>
 */
public abstract class AbstractPostCreateSIBHook<T extends SIB> extends DIMEPostCreateHook<T>{
	
	private GUI rootModel;
	protected SIB csib;

	private int sibInputPortY;
	
	private int branchY;
	
	private int branchOutputPortY;
	
	private Branch currentBranch;
	
	private EventListener currentEvent;

	/**
	 * finishes the sib creation. currently only sets the SIB width to 150.
	 */
	protected void finish() {
		csib.resize(150, csib.getHeight());
	}
	
	/**
	 * Convenience wrapper for {@link #init(SIB, String, int, int, boolean)} with resize=true
	 */
	protected void init(SIB sib, String label, int inputPortAmount) {
		init(sib, label, inputPortAmount, true);
	}
	
	/**
	 * Initializes the creation/autolayouting of the SIB. This method must be called prior to any other
	 * 
	 * @param sib The SIB that is created
	 * @param label The SIB's label, is actually given to {@link SIB#setLabel(String)}
	 * @param inputPortAmount The amount of input ports the SIB has. Required vor calculating SIB height. No effect if resize=false.
	 * @param branchAmount The amount of branches the SIB has. Required to calculate intial branch x/y position
	 * @param resize automatically resizes the SIB according to inputPortAmount
	 */
	protected void init(SIB sib, String label, int inputPortAmount, boolean resize) {
		try {
			rootModel = sib.getRootElement();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
			csib = sib;	
		try { //FIXME: this should not be necessary... 
			if (resize) {
				csib.resize(csib.getWidth(), NodeLayout.getSIBHeight(inputPortAmount));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			// ignore... it worked... don't worry...
		}
     	csib.setLabel(label);
		
		sibInputPortY = SIB_FIRST_PORT_Y;
	}
	
	/**
	 * Adds a primitive input port to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addPrimitiveInputPort(String name, PrimitiveType type, boolean isList) {
		PrimitiveInputPort inputPort = csib.newPrimitiveInputPort(PORT_X, sibInputPortY);
		inputPort.setName(name);
		inputPort.setIsList(isList);
		inputPort.setDataType(type);
		sibInputPortY += PORT_SPACE;
	}

	/**
	 * Adds a complex input port to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addComplexInputPort(String name, Type type, boolean isList) {
		ComplexInputPort inputPort = csib.newComplexInputPort(type, PORT_X, sibInputPortY);
		inputPort.setName(name);
		inputPort.setIsList(isList);
		sibInputPortY += PORT_SPACE;
	}
	
	/**
	 * Adds a generic input port to the SIB
	 * @param name
	 * @param typeParameterName
	 */
	protected void addGenericInputPort(String name, String typeParameterName,boolean isList) {
		InputGeneric inputPort = csib.newInputGeneric(PORT_X, sibInputPortY);
		inputPort.setName(name);
		inputPort.setIsList(isList);
		inputPort.setTypeParameter(typeParameterName);
		sibInputPortY += PORT_SPACE;
	}
	
	/**
	 * Adds a branch to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void newBranch(String branchName, int outputPortAmount) {
		if(csib instanceof GUISIB){
			currentBranch = ((GUISIB) csib).newBranch(PORT_X, branchY);			
		}
		if(csib instanceof GUIPlugin){
			currentBranch = ((GUIPlugin) csib).newBranch(PORT_X, branchY);	
		}
		if(csib instanceof ProcessSIB){
			currentBranch = ((ProcessSIB) csib).newBranch(PORT_X, branchY);	
		}
		int posX = 5;
		int posY = csib.getHeight();
		int height = NodeLayout.getBranchHeight(outputPortAmount);
		sibInputPortY+= height + PORT_SPACE;
		
	    currentBranch.setName(branchName);
		currentBranch.resize(currentBranch.getWidth(), height);
		currentBranch.setX(posX);
		currentBranch.setY(posY);
		
		csib.resize(csib.getWidth(), csib.getHeight()+height+PORT_SPACE);
		
		branchOutputPortY = BRANCH_FIRST_PORT_Y;
	    
	}
	
	/**
	 * Adds a branch to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void newEventListener(String branchName, int outputPortAmount) {
		if(csib instanceof GUISIB){
			currentEvent = ((GUISIB) csib).newEventListener(PORT_X, branchY);			
		}
		if(csib instanceof GUIPlugin){
			currentEvent = ((GUIPlugin) csib).newEventListener(PORT_X, branchY);	
		}
		int posX = 5;
		int posY = csib.getHeight();
		int height = NodeLayout.getBranchHeight(outputPortAmount);
		sibInputPortY+= height + PORT_SPACE;
		
		currentEvent.setName(branchName);
		currentEvent.resize(currentEvent.getWidth(), height);
		currentEvent.setX(posX);
		currentEvent.setY(posY);
		
		csib.resize(csib.getWidth(), csib.getHeight()+height+PORT_SPACE);
		
		branchOutputPortY = BRANCH_FIRST_PORT_Y;
	    
	}
	
	/**
	 * Adds a primitive output port to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addPrimitiveOutputPort(String name, PrimitiveType type, boolean isList) {
		PrimitiveOutputPort outputPort = currentBranch.newPrimitiveOutputPort(PORT_X, branchOutputPortY);
		outputPort.setDataType(type);
		outputPort.setIsList(isList);
		outputPort.setName(name);
		branchOutputPortY += PORT_SPACE;
	}
	
	/**
	 * Adds a primitive input port to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addEventPrimitiveInputPort(String name, PrimitiveType type, boolean isList) {
		PrimitiveInputPort inputPort = currentEvent.newPrimitiveInputPort(PORT_X, branchOutputPortY);
		inputPort.setDataType(type);
		inputPort.setIsList(isList);
		inputPort.setName(name);
		branchOutputPortY += PORT_SPACE;
	}
	
	/**
	 * Adds a complex output port to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addComplexOutputPort(String name, Type type, boolean isList) {
		ComplexOutputPort outputPort = currentBranch.newComplexOutputPort(type, PORT_X, branchOutputPortY);
		outputPort.setName(name);
		outputPort.setIsList(isList);
		branchOutputPortY += PORT_SPACE;
	}
	
	/**
	 * Adds a generic output port to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addGenericOutputPort(String name, String typeParameter, boolean isList) {
		OutputGeneric outputPort = currentBranch.newOutputGeneric(PORT_X, branchOutputPortY);
		outputPort.setName(name);
		outputPort.setTypeParameter(typeParameter);
		outputPort.setIsList(isList);
		branchOutputPortY += PORT_SPACE;
	}
	
	/**
	 * Adds a complex output ipnut to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addEventComplexInputPort(String name, Type type, boolean isList) {
		ComplexInputPort ipnutPort = currentEvent.newComplexInputPort(type, PORT_X, branchOutputPortY);
		ipnutPort.setName(name);
		ipnutPort.setIsList(isList);
		branchOutputPortY += PORT_SPACE;
	}
	
	/**
	 * Adds a generic output ipnut to the SIB
	 * @param name
	 * @param type
	 * @param isList
	 */
	protected void addEventGenericInputPort(String name, String typeParameter, boolean isList) {
		InputGeneric inputPort = currentEvent.newInputGeneric(PORT_X, branchOutputPortY);
		inputPort.setName(name);
		inputPort.setTypeParameter(typeParameter);
		inputPort.setIsList(isList);
		branchOutputPortY += PORT_SPACE;
	}

}
