package info.scce.dime.gui.hooks;

import static info.scce.dime.process.helper.LayoutConstants.BRANCH_FIRST_PORT_Y;
import static info.scce.dime.process.helper.LayoutConstants.PORT_SPACE;
import static info.scce.dime.process.helper.LayoutConstants.PORT_X;

import graphmodel.Container;
import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.gui.gui.Branch;
import info.scce.dime.gui.gui.Event;
import info.scce.dime.gui.gui.EventListener;
import info.scce.dime.gui.gui.OutputPort;
import info.scce.dime.process.helper.NodeLayout;


public class CIOPreDelete extends DIMEPreDeleteHook<OutputPort> {

	@Override
	public void preDelete(OutputPort port) {
		
		ModelElementContainer container = port.getContainer();
		
		resizeAndLayoutBeforeDelete((Container)container, port, PORT_X, BRANCH_FIRST_PORT_Y, PORT_SPACE, OutputPort.class);
		
	}
	
	private void resizeAndLayoutBeforeDelete(Container container, Node doomedNode, int xMargin, int initialY, int ySpace, Class<? extends Node> ... layoutedTypes) {

		int y = initialY;
		
		int ioAmount = 0;
		for (Class<? extends Node> layoutedType : layoutedTypes) {
			ioAmount += container.getModelElements(layoutedType).size();
		}
		
		ioAmount -= (doomedNode == null ? 0 : 1); 
		
		resize(container, container.getWidth(), NodeLayout.getBranchHeight(ioAmount));
		
		for (Class<? extends Node> layoutedType : layoutedTypes) {
			for (Node node: container.getModelElements(layoutedType)) {
				if (!node.equals(doomedNode)) {
					moveTo(node, container, xMargin, y);
					resize(node, container.getWidth()-2*xMargin, node.getHeight());
					y += ySpace;
				}
			}
		}
	}

	private void moveTo(Node node, Container container, int x, int y) {

		if(node instanceof OutputPort && container instanceof Branch){
			((OutputPort)node).moveTo((Branch)container,x,y);
		}
		if(node instanceof OutputPort && container instanceof Event){
			((OutputPort)node).moveTo((Event)container,x,y);
		}
		
	}

	private void resize(Node container, int width, int height) {
		if(container instanceof Event){
			((Event)container).resize(width, height);
		}
		if(container instanceof EventListener){
			((EventListener)container).resize(width, height);
		}
		if(container instanceof OutputPort){
			((OutputPort)container).resize(width, height);
		}
		
	}

}
