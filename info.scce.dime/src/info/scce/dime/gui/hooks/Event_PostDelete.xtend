package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.ListenerContext

class Event_PostDelete extends CincoPostDeleteHook<Event> {
	val OFFSET = 30

	override getPostDeleteFunction(Event node) {
		switch it : node.container {
			ListenerContext: [layout]
			default: [/* do nothing */]
		}
	}

	def layout(ListenerContext node) {
		var maxHeight = maxHeight(node)
		node.setHeight(maxHeight + OFFSET)

	}

	def maxHeight(ListenerContext context) {
		var maxLowerBound = 50;
		for (node : context.nodes) {
			if (node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height
			}
		}
		return maxLowerBound
	}

}
