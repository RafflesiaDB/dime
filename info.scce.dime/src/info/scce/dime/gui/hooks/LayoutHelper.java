package info.scce.dime.gui.hooks;

import java.util.ArrayList;

import graphmodel.Container;
import graphmodel.Node;
import info.scce.dime.gui.gui.Bar;
import info.scce.dime.gui.gui.Box;
import info.scce.dime.gui.gui.Branch;
import info.scce.dime.gui.gui.ButtonGroup;
import info.scce.dime.gui.gui.ButtonToolbar;
import info.scce.dime.gui.gui.ControlSIB;
import info.scce.dime.gui.gui.Description;
import info.scce.dime.gui.gui.Dropdown;
import info.scce.dime.gui.gui.File;
import info.scce.dime.gui.gui.GUI;
import info.scce.dime.gui.gui.GUIPlugin;
import info.scce.dime.gui.gui.GUISIB;
import info.scce.dime.gui.gui.GuardSIB;
import info.scce.dime.gui.gui.IO;
import info.scce.dime.gui.gui.Image;
import info.scce.dime.gui.gui.LinkProcessSIB;
import info.scce.dime.gui.gui.Listing;
import info.scce.dime.gui.gui.MovableContainer;
import info.scce.dime.gui.gui.MoveSign;
import info.scce.dime.gui.gui.NavBarPart;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.gui.gui.Row;
import info.scce.dime.gui.gui.Tabbing;
import info.scce.dime.gui.gui.Table;
import info.scce.dime.gui.gui.Template;
import info.scce.dime.gui.gui.Thumbnail;
import info.scce.dime.gui.helper.ElementCollector;
/**
 * The layout helper is used to enable the layouting of components
 * of GUI models and especially on the create, delete and move hooks
 * @author zweihoff
 *
 */
public class LayoutHelper {
	
	public static int V_OFFSET_MIN = 30;
	public static int H_OFFSET_MIN = 10;
	public static int H_OFFSET = 10;
	public static int V_OFFSET = 10;
	public static int HEIGHT_MIN = 60;
	public static int WIDTH_MIN = 100;
	
	/**
	 * Checks if the given container is the root template
	 * @param cContainer
	 * @return
	 */
	private static boolean isRootComponent(Container cContainer){
		return !( cContainer.getContainer() instanceof GUI);
	}
	
	/**
	 * Layouts the elements in the given container and all its parent container
	 * until the root template
	 * @param cContainer
	 */
	public static void layoutComponent(Container cContainer)
	{
		if(isHorizontalComponent(cContainer)){
			resize(cContainer,getHorizontalWidth(cContainer)+10,getHorizontalHeight(cContainer)+20);
			repositionContainingElementsHorizontal(cContainer);
		}
		else{
			resize(cContainer,getVerticalWidth(cContainer)+10,getVerticalHeight(cContainer)+20);
			repositionContainingElementsVertical(cContainer);
		}
		if(cContainer.getContainer() != null){
			if( isRootComponent(cContainer) ){
				layoutComponent((Container)cContainer.getContainer());
			}
		}
	}
	
	/**
	 * Returns the component specific height
	 * @param c
	 * @return
	 */
	public static int getVOff(Container c)
	{
		if(c instanceof Dropdown){
			return 55;
		}
		if(c instanceof File){
			return 80;
		}
		if(c instanceof Thumbnail){
			return 55;
		}
		if(c instanceof Listing){
			return 40;
		}
		if(c instanceof Description){
			return 40;
		}
		if(c instanceof Box){
			return 60;
		}
		if(c instanceof Image){
			return 120;
		}
		if(c instanceof GUISIB){
			return 70;
		}
		if(c instanceof GUIPlugin){
			return 70;
		}
		if(c instanceof ProcessSIB){
			return 70;
		}
		if(c instanceof LinkProcessSIB){
			return 70;
		}
		if(c instanceof GuardSIB){
			return 70;
		}
		if(c instanceof ControlSIB){
			return 70;
		}
		return V_OFFSET_MIN;
	}
	
	/**
	 * Returns the summed width of all nodes placed in the given container
	 * which are wider than the given node
	 * @param cc
	 * @param node
	 * @return
	 */
	public static int getHorizontalWidth(Container cc,Node node)
	{
		int x = LayoutHelper.H_OFFSET_MIN;
		for(Node n:ElementCollector.getElementsH(getAllNodes(cc))){
			if(n.getX() < node.getX()) {
				x += n.getWidth()+LayoutHelper.H_OFFSET;
			}
		}
		return x;
	}
	
	/**
	 * Returns the summed height of all nodes placed in the given container
	 * which are taller than the given node
	 * @param cc
	 * @param node
	 * @return
	 */
	public static int getVerticalHeight(Container cc,Node node)
	{
		int y = LayoutHelper.getVOff(cc);
		for(Node n:ElementCollector.getElementsV(getAllNodes(cc))){
			if(n.getY() < node.getY()) {
				y += n.getWidth()+LayoutHelper.V_OFFSET;
			}
		}
		return y;
	}
	
	/**
	 * Checks, if the given container layouts its inner
	 * nodes in horizontal order
	 * @param cContainer
	 * @return
	 */
	public static boolean isHorizontalComponent(Container cContainer)
	{
		if(cContainer instanceof Row)return true;
		if(cContainer instanceof Bar)return true;
		if(cContainer instanceof Tabbing)return true;
		if(cContainer instanceof Table)return true;
		if(cContainer instanceof ButtonGroup)return true;
		if(cContainer instanceof ButtonToolbar)return true;
		if(cContainer instanceof NavBarPart)return true;
		return false;
	}
	
	/**
	 * Returns the maximal width of all inner nodes in the given container
	 * @param cContainer
	 * @return
	 */
	private static int getVerticalWidth(Container cContainer){
		 int width = WIDTH_MIN;
		 for(Node node:getAllNodes(cContainer)){
			 if(node.getWidth() > width){
				 width = node.getWidth();
			 }
		 }
		 return width + H_OFFSET_MIN;
	}
	
	/**
	 * Returns the maximal height of all inner nodes in the given container
	 * @param cContainer
	 * @return
	 */
	private static int getHorizontalHeight(Container cContainer){
		 int height = HEIGHT_MIN;
		 for(Node node:getAllNodes(cContainer)){
			 if(node.getHeight() > height){
				 height = node.getHeight();
			 }
		 }
		 return height + V_OFFSET_MIN;
	}
	
	/**
	 * Returns the summed height of all nodes in the given container
	 * @param cContainer
	 * @return
	 */
	public static int getVerticalHeight(Container cContainer)
	{
		int height = getVOff(cContainer);
		for(Node n:getAllNodes(cContainer)) {
			height+=(n.getHeight()+V_OFFSET);
		}
		return height;
	}
	
	
	/**
	 * Returns the summed width of all nodes in the given container
	 * @param cContainer
	 * @return
	 */
	public static int getHorizontalWidth(Container cContainer)
	{
		int width = H_OFFSET_MIN;
		for(Node n:getAllNodes(cContainer)) {
			width+=(n.getWidth()+H_OFFSET);
		}
		if(width < WIDTH_MIN) width = WIDTH_MIN;
		return width;
	}
	
	/**
	 * Repositions the nodes in the given container horizontal
	 * @param cContainer
	 */
	private static void repositionContainingElementsHorizontal(Container cContainer){
		int x = H_OFFSET_MIN;
		for(Node n:ElementCollector.getElementsH(getAllNodes(cContainer))) {
			n.setX(x);
			x += H_OFFSET;
			x += n.getWidth();
		}
	}
	
	/**
	 * Repositions the nodes in the given container vertical
	 * @param cContainer
	 */
	private static void repositionContainingElementsVertical(Container cContainer){
	
		int y = getVOff(cContainer);
		for(Node n:ElementCollector.getElementsV(getAllNodes(cContainer))) {
			if(!(n instanceof IO) && !(n instanceof Branch)){
				n.setY(y);				
			}
			y += V_OFFSET;
			y += n.getHeight();
		}
	}
	
	/**
	 * Resizes the given node to the defined width and height
	 * @param cNode
	 * @param width
	 * @param height
	 */
	public static void resize(Node cNode,int width,int height){
		if(cNode instanceof MovableContainer){
			((MovableContainer)cNode).resize(width, height);
		}
		if(cNode instanceof Template) {
			((Template)cNode).resize(width, height);
		}
	}
	
	/**
	 * Returns a list of all nodes in the give container
	 * @param cContainer
	 * @return
	 */
	public static ArrayList<Node> getAllNodes(Container cContainer) {
		ArrayList<Node> nodes = new ArrayList<Node>();
		for(Node cNode:cContainer.getAllNodes()){
			if(!(cNode instanceof MoveSign)){
				nodes.add(cNode);
			}
		}
		return nodes;
	}
}
