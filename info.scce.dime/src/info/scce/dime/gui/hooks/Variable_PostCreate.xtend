package info.scce.dime.gui.hooks

import info.scce.dime.gui.gui.Variable
import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook
import info.scce.dime.gui.gui.DataContext

class Variable_PostCreate extends CincoPostCreateHook<Variable>{
	
	val OFFSET = 30
	
	override postCreate(Variable node) {
		if(node.container instanceof DataContext){
			var dataContext = node.container as DataContext
			var maxHeight = maxHeight(dataContext);
			dataContext.setHeight( maxHeight + OFFSET);
			
		}
	}
		def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
}