package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.ExtensionContext

class GUISIB_PostDelete extends CincoPostDeleteHook<GUISIB> {
	
	val OFFSET = 30

	override getPostDeleteFunction(GUISIB node) {
		switch it : node.container {
			ExtensionContext: [layout]
			default: [/* do nothing */]
		}
	}

	def layout(ExtensionContext node) {
		var maxHeight = maxHeight(node)
		node.setHeight(maxHeight + OFFSET)

	}

	def maxHeight(ExtensionContext context) {
		var maxLowerBound = 50;
		for (node : context.nodes) {
			if (node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height
			}
		}
		return maxLowerBound
	}
	
}