package info.scce.dime.gui.hooks;

import java.util.List;
import java.util.stream.Collectors;

import info.scce.dime.dad.dad.ProcessComponent;
import info.scce.dime.dad.dad.ProcessEntryPointComponent;
import info.scce.dime.dad.dad.URLProcess;
import info.scce.dime.gui.gui.LinkProcessSIB;
import info.scce.dime.gui.gui.PrimitiveType;
import info.scce.dime.gui.gui.ProcessSIB;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.EndSIB;
import info.scce.dime.process.process.Output;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.Process;

/**
 * The GUI SIB hook is used create the corresponding ports and branches
 * for the new GUI SIB
 * @author zweihoff
 *
 */
public class LinkProcessSIBHook extends AbstractPostCreateSIBHook<LinkProcessSIB> {

	/**
	 * Creates the corresponding GUI SIB input ports for every input variable
	 * of the referenced GUI model.
	 * For every button and branch of embedded SIBs, a branch with the corresponding ports is added.
	 * 
	 */
	@Override
	public void postCreate(LinkProcessSIB sib) {
			URLProcess url = (URLProcess) sib.getProMod();
			Process lcProcess = null;
			if(url instanceof ProcessComponent) {
				lcProcess = ((ProcessComponent) url).getModel();
			} else if(url instanceof ProcessEntryPointComponent) {
				lcProcess = ((ProcessEntryPointComponent) url).getEntryPoint().getProMod();
			}
     		List<Output> inputs = lcProcess.getStartSIBs().stream().flatMap(n->n.getOutputs().stream()).collect(Collectors.toList());
     		
     		
     		init(sib, url.getUrl(), inputs.size());
     		
     		     		
     		//Input Ports
     		
     		for (Output var : inputs) {
     			if (var instanceof ComplexOutputPort) {
     				ComplexOutputPort cVar = (ComplexOutputPort) var;
     				addComplexInputPort(cVar.getName(), cVar.getDataType(), cVar.isIsList());
     			}
     			else if (var instanceof PrimitiveOutputPort) {
     				PrimitiveOutputPort pVar = (PrimitiveOutputPort) var;
     				addPrimitiveInputPort(pVar.getName(), toGUIPT(pVar.getDataType()), pVar.isIsList());
     			}
     			else {
     				throw new IllegalStateException("else in exhaustive if should not happen; please fix broken implementation");
     			}
     		}
     	
 		
     		
 		sib.setLabel(url.getUrl());
	     		
	    finish();
	    //Trigger Layouter
	    CreateComponentHook cch = new CreateComponentHook();
	    cch.postCreate(sib);
		    
	}

	private PrimitiveType toGUIPT(info.scce.dime.process.process.PrimitiveType dataType) {
		switch(dataType) {
		case BOOLEAN:return PrimitiveType.BOOLEAN;
		case REAL:return PrimitiveType.REAL;
		case INTEGER:return PrimitiveType.INTEGER;
		case TEXT:return PrimitiveType.TEXT;
		case TIMESTAMP:return PrimitiveType.TIMESTAMP;
		default :return PrimitiveType.FILE;
		}
	}
	
	
}
