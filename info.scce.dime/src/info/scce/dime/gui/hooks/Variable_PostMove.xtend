package info.scce.dime.gui.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostMoveHook
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.gui.DataContext
import graphmodel.ModelElementContainer

class Variable_PostMove extends CincoPostMoveHook<Variable>{
	
	val OFFSET = 30
		
	override postMove(Variable node, ModelElementContainer arg1, ModelElementContainer arg2, int arg3, int arg4, int arg5, int arg6) {
		if(node.container instanceof DataContext){
			var dataContext = node.container as DataContext
			var maxHeight = maxHeight(dataContext);
			dataContext.setHeight( maxHeight + OFFSET);
		}
	}
	
	
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
}