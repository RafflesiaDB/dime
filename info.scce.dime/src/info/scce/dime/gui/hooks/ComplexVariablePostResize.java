package info.scce.dime.gui.hooks;

import info.scce.dime.api.DIMEPostResizeHook;
import info.scce.dime.gui.gui.Attribute;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.process.helper.LayoutConstants;

/**
 * The complex variable post resize hook is used to layout
 * the attributes placed in a variable after its resize
 * @author zweihoff
 *
 */
public class ComplexVariablePostResize extends DIMEPostResizeHook<ComplexVariable>{

	/**
	 * The complex variable post resize hook is used to layout
	 * the attributes placed in a variable after its resize
	 * @author zweihoff
	 *
	 */
	@Override
	public void postResize(ComplexVariable cVar, int direction, int width, int height) {
		for (Attribute a : cVar.getAttributes()) {
			a.resize(width - LayoutConstants.VAR_ATTR_X*2, a.getHeight());
		}
	}

}
