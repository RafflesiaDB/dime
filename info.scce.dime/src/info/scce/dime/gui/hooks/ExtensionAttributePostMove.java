package info.scce.dime.gui.hooks;

import static info.scce.dime.process.helper.LayoutConstants.VAR_ATTR_X;

import graphmodel.ModelElementContainer;
import graphmodel.Node;
import info.scce.dime.api.DIMEPostMoveHook;
import info.scce.dime.data.data.Type;
import info.scce.dime.process.helper.AttributeExpandUtils;
import info.scce.dime.gui.gui.ComplexAttributeConnector;
import info.scce.dime.gui.gui.ComplexInputPort;
import info.scce.dime.gui.gui.ComplexVariable;
import info.scce.dime.gui.gui.DataContext;
import info.scce.dime.gui.gui.ExtensionAttribute;
import info.scce.dime.process.process.Input;
import info.scce.dime.process.process.InputStatic;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.Process;

public class ExtensionAttributePostMove extends DIMEPostMoveHook<ExtensionAttribute> {
	final int OFFSET = 30;
	@Override
	public void postMove(ExtensionAttribute attribute, ModelElementContainer sourceContainer, ModelElementContainer targetContainer, int x, int y, int deltaX, int deltaY) {
		
		if (sourceContainer.equals(targetContainer)) {
			// do nothing if Attribute moved within the same ComplexVariable

			// undo the move operation would be nice here, but if manually moved back 
			// to original position, postMove would be triggered again.
			// TODO: file feature request

		}
		else if (targetContainer instanceof DataContext){
			DataContext context = (DataContext) targetContainer;
			ComplexVariable originatingVariable = (ComplexVariable) sourceContainer;
			
			//FIXME: this should not be necessary. See #15472
			info.scce.dime.data.data.ExtensionAttribute referencedAttr = 
					attribute.getAttribute();
			Process p = (Process) referencedAttr.getProcess();
			//check if process has valid signature
			if(p.getEndSIBs().isEmpty() || p.getEndSIBs().get(0).getInputs().isEmpty()) {
				//invalid signature -> move to prior spot
				attribute.moveTo(sourceContainer, x-deltaX, y-deltaY);
				return;
			}
			//check primitive or complex
			Input input = p.getEndSIBs().get(0).getInputs().get(0);
			if(input instanceof PrimitiveInputPort || input instanceof InputStatic) {
				//move for primitive attributes is invalid
				attribute.moveTo(sourceContainer, x-deltaX, y-deltaY);
				return;
			}
			
			info.scce.dime.process.process.ComplexInputPort cip = (info.scce.dime.process.process.ComplexInputPort) input;
			Type dataType = cip.getDataType();
			
			// TODO: correct positioning should be done by postCreate hook, but
			// moveTo methods currently missing in C-API.
			ComplexVariable newVar = context.newComplexVariable(dataType, VAR_ATTR_X, y);
			ComplexAttributeConnector connector = originatingVariable.newComplexAttributeConnector(newVar);
			connector.setAssociationName(referencedAttr.getName());
			
			newVar.setName(referencedAttr.getName());
			newVar.setIsList(referencedAttr.isIsList());
			newVar.setIsInput(false);
			newVar.resize(originatingVariable.getWidth(), originatingVariable.getHeight());
			
			AttributeExpandUtils.expand(newVar);
			attribute.delete();
			AttributeExpandUtils.resizeAndLayout(originatingVariable);
			layout(newVar);
		}
		else {
			throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
		}
		
		
	}

	private void layout(ComplexVariable node) {
		if(node.getContainer() instanceof DataContext){
			DataContext dataContext = (DataContext)node.getContainer();
			int maxHeight = maxHeight(dataContext);
			dataContext.setHeight( maxHeight + OFFSET);
		}
		
	}
	

	private int maxHeight(DataContext context) {
		int maxLowerBound= 50;
		for(Node node : context.getNodes()){
			if(node.getY() + node.getHeight() > maxLowerBound){
				maxLowerBound = node.getY() + node.getHeight(); 
			}
		}
		return maxLowerBound;
	}

}
