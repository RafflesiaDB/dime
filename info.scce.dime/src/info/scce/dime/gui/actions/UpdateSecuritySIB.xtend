package info.scce.dime.gui.actions

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import info.scce.dime.gui.gui.Argument
import info.scce.dime.gui.gui.Box
import info.scce.dime.gui.gui.Col
import info.scce.dime.gui.gui.Form
import info.scce.dime.gui.gui.Jumbotron
import info.scce.dime.gui.gui.Panel
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.gui.Tab
import info.scce.dime.gui.gui.Template
import info.scce.dime.gui.gui.Thumbnail
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessType
import org.eclipse.emf.ecore.EObject

/** 
 * Context menu action, which updates a given GUI SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class UpdateSecuritySIB extends DIMEGUICustomAction<SecuritySIB> {

	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Update security SIB"
	}

	/** 
	 * Checks if the reference to a GUI SIB is present and valid
	 */
	override canExecute(SecuritySIB guiSib) throws ClassCastException {
		switch it : guiSib.proMod {
			Process: processType == ProcessType::SECURITY
		}
	}

	/** 
	 * Updates the given GUI SIB.
	 * The update process creates a new GUI SIB
	 * and uses the semantics of the post create hook to
	 * consider all changes.
	 * In a second step, the data flow edges are reconnected
	 * and the out dated GUI SIB is removed, so that
	 * the new SIB is in the same position
	 */
	//TODO create/use extension method for this stuff
	override void execute(SecuritySIB guiSib) {
		val model = guiSib.proMod
		if (model instanceof Process === false) {
			showDialog("Referenced model is null", "Update failed. Referenced model is null!")
			return
		}
		
		var EObject eObj = ReferenceRegistry.instance.getEObject(guiSib.libraryComponentUID)
		if (eObj === null || eObj instanceof Process === false) {
			showDialog("Reference is null", "Update failed. Reference is null!")
			return;
		}
		//collect static inputs
		
		// Create new GUISIB
		var SecuritySIB newSecuritySIB = null
		var EObject container = guiSib.getContainer()
		if (container instanceof Template) {
			var Template n = (container as Template)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Col) {
			var Col n = (container as Col)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Tab) {
			var Tab n = (container as Tab)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Argument) {
			var Argument n = (container as Argument)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Panel) {
			var Panel n = (container as Panel)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Jumbotron) {
			var Jumbotron n = (container as Jumbotron)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Box) {
			var Box n = (container as Box)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Form) {
			var Form n = (container as Form)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if (container instanceof Thumbnail) {
			var Thumbnail n = (container as Thumbnail)
			newSecuritySIB = n.newSecuritySIB(eObj, guiSib.getX() + 1, guiSib.getY() + 1)
		}
		if(guiSib.generalStyle !=null) {
			newSecuritySIB.generalStyle = guiSib.generalStyle;
		}
		guiSib.rebuildPorts(newSecuritySIB);
		guiSib.delete()
	}

	
}
