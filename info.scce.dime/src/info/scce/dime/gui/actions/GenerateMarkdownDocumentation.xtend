package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ComplexOutputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.Event
import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.PrimitiveOutputPort
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.ProcessSIB
import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.LinkedList
import java.util.List
import org.apache.commons.io.FileUtils
import org.eclipse.emf.common.util.TreeIterator
import org.eclipse.emf.ecore.EObject
import info.scce.dime.gui.helper.GUIBranch
import info.scce.dime.gui.helper.ComplexGUIBranchPort
import info.scce.dime.gui.helper.PrimitiveGUIBranchPort

class GenerateMarkdownDocumentation<T extends GUI> extends DIMECustomAction<T> {
	
	
	
	override getName() {
		"Create Documentation"
	}

	override canExecute(GUI d) {
		true
	}
	
	override hasDoneChanges() {
		false
	}

	override void execute(GUI d) {
		val filePath = d.file.project.location.append('''../documentation/gui/«d.title».md'''.toString).toString
		createFile(createDoc(d).toString,filePath)
		
	}
	
	/**
	 * Helper method to create a file with the given content on the given path.
	 * @param content
	 * @param path
	 * @throws IOException
	 */
	def void createFile(String content,String path) throws IOException
	{
		val File f = new File(path);
		f.getParentFile().mkdirs(); 
		f.createNewFile();
		
		FileUtils.writeStringToFile(f,content);
	}
	
	def createDoc(GUI d){
		val inputs = d.inputVariables
		val branches = d.GUIBranchesMerged
		val events = d.events
		val containedGuiSibs = d.containedGuiSibs
		val containedInteractionSibs = d.containedInteractionSibs
	'''
	# «d.title» *GUI* «IF !d.extensionContexts.isEmpty» extends «d.extensionContexts.map[GUISIBs].flatten.map[name].join(",")»«ENDIF»
	 
«««	«d.documentation.doc»
	 
	### Input Ports
	 
	«inputs.map['''* *«name»* : «IF isIsList»List<«ENDIF»«typeName»«IF isIsList»>«ENDIF»'''].join("\n")»
	 
	### Events
	 
	«events.map[event].join("\n")»
	 
	### Branches
	 
	«branches.map[branch].join("\n")»
	 
	### Used GUI SIBs
	 
	«containedGuiSibs.map['''* [«name»«IF it instanceof DispatchedGUISIB» _dispatched_«ENDIF»](./«name».md)'''].join("\n")»
	 
	### Used Interaction Processes
	 
	«containedInteractionSibs.map['''* [«name»](../process/«name».md)'''].join("\n")»
	'''
	}
	
	def branch(GUIBranch b)
	'''
	* *«b.name»*
	«FOR p:b.ports»
	«" "»* «p.name»: «IF p.isList»List<«ENDIF»«IF p instanceof ComplexGUIBranchPort»«p.type.name»«ELSE»«(p as PrimitiveGUIBranchPort).type.literal»«ENDIF»«IF p.isList»>«ENDIF»
	«ENDFOR»
	'''
	
	def event(Event b)
	'''
	* *«b.name»*
	«FOR p:b.outputPorts»
	«" "»* «p.name»: «IF p.isIsList»List<«ENDIF»«IF p instanceof ComplexOutputPort»«p.dataType.name»«ELSE»«(p as PrimitiveOutputPort).dataType.literal»«ENDIF»«IF p.isList»>«ENDIF»
	«ENDFOR»
	'''
	
	
	
	def dispatch typeName(ComplexVariable t) {
		'''[«t.dataType.name»](../data/«t.dataType.rootElement.modelName»/«t.dataType.name».md)'''
	}
	
	def dispatch typeName(PrimitiveVariable t) {
		t.dataType.literal
	}
	
	
	
	def doc(String s){
		if(!s.nullOrEmpty) {
			return '''
			>>>
			«s»
			<<<
			'''
		}
		""
	}
	
	def private List<GUISIB> getContainedGuiSibs(GUI gui) {
		var ArrayList<GUISIB> list=new ArrayList<GUISIB>() 
		var TreeIterator<EObject> it=gui.eAllContents() 
		while (it.hasNext()) {
			var Object obj=it.next() 
			if (obj instanceof GUISIB){
				if(!(obj.container instanceof ExtensionContext)){
					var GUISIB guiSib=(obj as GUISIB) 
					list.add(guiSib) 
				}
			}
			
		}
		return list 
	}
	
	def private List<ProcessSIB> getContainedInteractionSibs(GUI gui) {
		var List<ProcessSIB> list=new LinkedList<ProcessSIB>() 
		var TreeIterator<EObject> it=gui.eAllContents() 
		while (it.hasNext()) {
			var Object obj=it.next() 
			if (obj instanceof ProcessSIB){
				list.add(obj) 
				
			}
			
		}
		return list 
	}
	
}
