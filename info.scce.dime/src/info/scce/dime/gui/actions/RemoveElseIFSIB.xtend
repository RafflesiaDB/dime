package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.ISSIB
import info.scce.dime.gui.hooks.CreateComponentHook

/** 
 *Removes the Else argument of an IF SIB.
 * @author zweihoff
 */
class RemoveElseIFSIB<T extends ControlSIB> extends DIMECustomAction<ControlSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"remove else"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(ControlSIB node) throws ClassCastException {
		return node.arguments.size>=2
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(ControlSIB node) {
		if(node instanceof IFSIB){
			val argElse = node.arguments.findFirst[blockName.equals("ELSE")]
			val argIf = node.arguments.findFirst[blockName.equals("THEN")]
			if(argElse != null) {
				// reposition arg
				if(argIf.y >= argElse.y){
					//switch
					argIf.y = argElse.y
				}
				//resize sib
				val h = argElse.height
				node.resize(node.width,node.height-h)
				argElse.delete
				var CreateComponentHook cch = new CreateComponentHook()
				cch.postCreate(node)
			}
		}
		if(node instanceof ISSIB){
			val argElse = node.arguments.findFirst[blockName.equals("ELSE")]
			val argIs = node.arguments.findFirst[blockName.equals("THEN")]
			if(argElse != null) {
				// reposition arg
				if(argIs.y >= argElse.y){
					//switch
					argIs.y = argElse.y
				}
				//resize sib
				val h = argElse.height
				node.resize(node.width,node.height-h)
				argElse.delete
				var CreateComponentHook cch = new CreateComponentHook()
				cch.postCreate(node)
			}
		}
	}
}
