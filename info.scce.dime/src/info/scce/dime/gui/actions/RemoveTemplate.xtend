package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.Template

/** 
 * Context menu action to remove a template from a GUI model.
 * The context menu entry is available for templates.
 * @author zweihoff
 */
class RemoveTemplate extends DIMECustomAction<Template> {
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Remove Template"
	}

	/** 
	 * At least one template is required for a GUI model,
	 * so that all templates but one can be removed
	 */
	override canExecute(Template port) {
		!port.rootElement?.templates.nullOrEmpty
	}

	/** 
	 * Removes the given template from its GUI model
	 */
	override execute(Template template) {
		template.delete
	}
}
