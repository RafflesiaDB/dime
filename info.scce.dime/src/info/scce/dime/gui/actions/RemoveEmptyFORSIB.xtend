package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.hooks.CreateComponentHook

/** 
 *Removes the Else argument of an IF SIB.
 * @author zweihoff
 */
class RemoveEmptyFORSIB extends DIMECustomAction<FORSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"remove empty"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(FORSIB node) throws ClassCastException {
		return node.arguments.exists[blockName.equals("EMPTY")]
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(FORSIB node) {
		val argElse = node.arguments.findFirst[blockName.equals("EMPTY")]
		val otherArgs = node.arguments.filter[!blockName.equals("EMPTY")]
		if(argElse != null) {
			for(oA:otherArgs){
				// reposition arg
				if(oA.y >= argElse.y){
					//switch
					oA.y = argElse.y
				}
				
			}
			//resize sib
			val h = argElse.height
			node.resize(node.width,node.height-h)
			argElse.delete
			var CreateComponentHook cch = new CreateComponentHook()
			cch.postCreate(node)
		}
		
		
	}
}
