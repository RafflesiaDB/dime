package info.scce.dime.gui.actions

import graphmodel.Node
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.SecuritySIB
import info.scce.dime.gui.hooks.CreateComponentHook
import org.eclipse.graphiti.mm.pictograms.Diagram

/** 
 * De-negates an IF SIB.
 * @author zweihoff
 */
class AddElseSecuritySIB<T extends SecuritySIB> extends DIMECustomAction<ControlSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"add denied"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(ControlSIB node) throws ClassCastException {
		return node.arguments.size<=1
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(ControlSIB node) {
		if(node instanceof SecuritySIB){
			var arg = node.newArgument(10,0)
			arg.blockName = "DENIED"
			//trigger layouter
			var CreateComponentHook cch = new CreateComponentHook()
			cch.postCreate(arg)
		}
		
		
	}
}
