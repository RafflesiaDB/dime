package info.scce.dime.gui.actions

import info.scce.dime.gui.gui.ExtensionContext
import info.scce.dime.gui.gui.GUISIB
import info.scce.dime.gui.gui.DispatchedGUISIB

/** 
 * Context menu action, which updates a given GUI SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class DispatchGuiSIB extends DIMEGUICustomAction<GUISIB> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Activate Dispatching"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(GUISIB guiSib) {
		!(guiSib instanceof DispatchedGUISIB) && !(guiSib.container instanceof ExtensionContext)
	}

	override void execute(GUISIB guiSib) {
		new UpdateDispatchedGuiSIB().updateSIB(guiSib)
	}
}
