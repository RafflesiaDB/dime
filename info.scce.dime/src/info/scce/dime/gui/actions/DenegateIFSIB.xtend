package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.ISSIB

/** 
 * De-negates an IF SIB.
 * @author zweihoff
 */
class DenegateIFSIB<T extends ControlSIB> extends DIMECustomAction<ControlSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"de-negate"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(ControlSIB node) throws ClassCastException {
		if(node instanceof IFSIB) {
			return node.negate			
		}
		if(node instanceof ISSIB) {
			return node.negate			
		}
		false
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(ControlSIB node) {
		if(node instanceof IFSIB) {
			node.negate = false
		}
		if(node instanceof ISSIB) {
			node.negate = false			
		}
	}
}
