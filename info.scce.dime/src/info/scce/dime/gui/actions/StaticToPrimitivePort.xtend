package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.BooleanInputStatic
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.IntegerInputStatic
import info.scce.dime.gui.gui.PrimitiveType
import info.scce.dime.gui.gui.RealInputStatic
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.TimestampInputStatic
import graphmodel.ModelElementContainer
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.SIB
import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.ControlSIB

/** 
 * Context menu action to convert a static primitive input port of a GUI SIB or GUI plug in SIB
 * to a none static input port of the same data type.
 * The context menu entry is available for static primitive input ports.
 * @author zweihoff
 */
class StaticToPrimitivePort<T extends InputStatic> extends DIMECustomAction<InputStatic> {
	
	override getName() {
		"Convert Static Value to Input"
	}

	/** 
	 * Creates a new primitive input port with the corresponding type
	 * of the static primitive port data type and deleted the static primitive port afterwards.
	 */
	//TODO beautify after auto conversion to Xtend class
	//TODO create/use extension method for this stuff
	override void execute(InputStatic inputStatic) {
		try {
			var int x = inputStatic.getX()
			var int y = inputStatic.getY()
			var String name = inputStatic.getName()
			var ModelElementContainer container = inputStatic.getContainer()
			inputStatic.delete()
			var PrimitiveType pType = null
			if (inputStatic instanceof IntegerInputStatic) {
				pType = PrimitiveType::INTEGER
			} else if (inputStatic instanceof RealInputStatic) {
				pType = PrimitiveType::REAL
			} else if (inputStatic instanceof TextInputStatic) {
				pType = PrimitiveType::TEXT
			} else if (inputStatic instanceof TimestampInputStatic) {
				pType = PrimitiveType::TIMESTAMP
			} else if (inputStatic instanceof BooleanInputStatic) {
				pType = PrimitiveType::BOOLEAN
			} else {
				throw new IllegalStateException("missing implementation for static port type")
			}
			var PrimitiveInputPort cInputPort = null
			if (container instanceof SIB) {
				cInputPort = ((container as SIB)).newPrimitiveInputPort(x, y)
			}
			if (container instanceof EventListener) {
				cInputPort = container.newPrimitiveInputPort(x, y)
			}
			if (container instanceof ControlSIB) {
				cInputPort = container.newPrimitiveInputPort(x, y)
			}
			cInputPort.setName(name)
			cInputPort.setDataType(pType)
			cInputPort.highlight()
		} catch (Exception e) {
			e.printStackTrace()
			return;
		}

	}
}
