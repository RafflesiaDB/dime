package info.scce.dime.gui.actions

import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.EventListener

/** 
 * Context menu action, which converts all primitive
 * input ports, which are not connected by data flow, to a static input
 * @author zweihoff
 */
class EventMultiInputToStatic extends DIMEGUICustomAction<EventListener> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Convert to static ports"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(EventListener guiSib) {
		//at least one primitive input port without incoming edge
		!guiSib.IOs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].empty
	}

	override void execute(EventListener guiSib) {
		guiSib.IOs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].forEach[n|
			val p = new PrimitivePortToStatic
			p.execute(n)
		]
	}
}
