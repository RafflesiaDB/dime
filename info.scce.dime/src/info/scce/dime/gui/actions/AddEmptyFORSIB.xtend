package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.FORSIB
import info.scce.dime.gui.hooks.CreateComponentHook

/** 
 * Adds an Join argument to the FORSIB.
 * @author zweihoff
 */
class AddEmptyFORSIB extends DIMECustomAction<FORSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"add empty"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(FORSIB node) throws ClassCastException {
		return !node.arguments.exists[blockName.equals("EMPTY")]
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(FORSIB node) {
		var arg = node.newArgument(10,0)
		arg.blockName = "EMPTY"
		//trigger layouter
		var CreateComponentHook cch = new CreateComponentHook()
		cch.postCreate(arg)
			
		
		
	}
}
