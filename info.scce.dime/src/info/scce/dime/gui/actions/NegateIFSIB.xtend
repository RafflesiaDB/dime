package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.ISSIB

/** 
 * Context menu action which can only be used on the GUI graphmodel.
 * Adds a footer template to the model, if no footer is already present
 * @author zweihoff
 */
class NegateIFSIB<T extends ControlSIB> extends DIMECustomAction<ControlSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"negate"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(ControlSIB node) throws ClassCastException {
		if(node instanceof IFSIB) {
			return !node.negate			
		}
		if(node instanceof ISSIB) {
			return !node.negate			
		}
		false
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(ControlSIB node) {
		if(node instanceof IFSIB) {
			node.negate = true
		}
		if(node instanceof ISSIB) {
			node.negate = true			
		}
	}
}
