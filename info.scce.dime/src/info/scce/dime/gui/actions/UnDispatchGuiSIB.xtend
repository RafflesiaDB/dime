package info.scce.dime.gui.actions

import info.scce.dime.gui.gui.DispatchedGUISIB
import info.scce.dime.gui.gui.ExtensionContext

/** 
 * Context menu action, which updates a given GUI SIB.
 * The update process includes the input ports and the branches and their output ports.
 * @author zweihoff
 */
class UnDispatchGuiSIB extends DIMEGUICustomAction<DispatchedGUISIB> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Deactivate Dispatching"
	}

	/** 
	 * Checks if the GUI SIB is dispatched
	 */
	override canExecute(DispatchedGUISIB guiSib) {
		!(guiSib.container instanceof ExtensionContext)
	}

	override void execute(DispatchedGUISIB guiSib) {
		new UpdateGuiSIB().updateSIB(guiSib)
	}
}
