package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ControlSIB
import info.scce.dime.gui.gui.IFSIB
import info.scce.dime.gui.gui.ISSIB
import info.scce.dime.gui.hooks.CreateComponentHook

/** 
 * De-negates an IF SIB.
 * @author zweihoff
 */
class AddElseIFSIB<T extends ControlSIB> extends DIMECustomAction<ControlSIB> {
	
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"add else"
	}


	/** 
	 * Checks if a footer template is already present in the given GUI model
	 */
	override canExecute(ControlSIB node) throws ClassCastException {
		return node.arguments.size<=1
	}

	/** 
	 * Creates a new footer template on the given GUI model
	 */
	override execute(ControlSIB node) {
		if(node instanceof IFSIB || node instanceof ISSIB){
			var arg = node.newArgument(10,0)
			arg.blockName = "ELSE"
			//trigger layouter
			var CreateComponentHook cch = new CreateComponentHook()
			cch.postCreate(arg)
			
		}
		
		
	}
}
