package info.scce.dime.gui.actions
 
import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.process.helper.AttributeExpandUtils
import info.scce.dime.gui.gui.DataContext

/** 
 * Context menu action to expand and collapse the attributes of a given complex variable 
 * placed in a GUI data context.
 * @author zweihoff
 */
class ToggleAttributeExpand extends DIMECustomAction<ComplexVariable> {
	val OFFSET = 30
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Toggle Attribute Expand"
	}

	/** 
	 * Collapses a given complex variable if it is already expanded.
	 * Otherwise the complex variable is expanded to display all attributes.
	 */
	 //TODO beautify after auto conversion to Xtend class
	override void execute(ComplexVariable variable) {
		if (!variable.isExpanded()) {
			AttributeExpandUtils::expand(variable)
			layout(variable)
		} else {
			AttributeExpandUtils::collapse(variable)
			layout(variable)
		}
	}
	
	def layout(ComplexVariable node) {
		if(node.container instanceof DataContext){
			var dataContext = node.container as DataContext
			var maxHeight = maxHeight(dataContext)
			dataContext.height = maxHeight + OFFSET
		}
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound= 50;
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound){
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
}
