package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.SecuritySIB

/** 
 * The open model action opens the editor for the model referenced by a given embedded
 * GUI SIBs, Guard SIBs or GUI plug in SIB placed in a GUI model
 * @author zweihoff
 */
class OpenSecuritySIBModel extends DIMECustomAction<SecuritySIB> {
	
	/** 
	 * Preserves that the action does not perform any changes to the GUI model,
	 * so that it has not to be flagged as dirty
	 */
	override hasDoneChanges() {
		false
	}

	/** 
	 * If a GUI SIB, Guard SIB or GUI plug in SIB is double clicked, the corresponding model
	 * is opened in a new editor tab, which is brought to front.
	 */
	override execute(SecuritySIB s) {
		s.proMod?.openEditor
	}
}
