package info.scce.dime.gui.actions

import info.scce.dime.gui.gui.EventListener
import info.scce.dime.gui.gui.InputStatic

/** 
 * Context menu action, which converts all static ports to
 * input ports
 * @author zweihoff
 */
class EventMultiStaticToInput extends DIMEGUICustomAction<EventListener> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		"Convert to input ports"
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(EventListener guiSib) {
		//at least one primitive input port without incoming edge
		!guiSib.IOs.filter(InputStatic).empty
	}

	override void execute(EventListener guiSib) {
		guiSib.IOs.filter(InputStatic).forEach[n|
			val p = new StaticToPrimitivePort
			p.execute(n)
		]
	}
}
