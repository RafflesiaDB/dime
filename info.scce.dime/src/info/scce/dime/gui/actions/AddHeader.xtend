package info.scce.dime.gui.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.gui.gui.GUI
import info.scce.dime.gui.gui.Template

/** 
 * Context menu action which can only be used on the GUI graphmodel.
 * Adds a header template to the model, if no footer is already present
 * @author zweihoff
 */
class AddHeader extends DIMECustomAction<GUI> {
	/** 
	 * Returns the name displayed in the context menu
	 */
	override getName() {
		"Add Header"
	}

	//TODO beautify after auto conversion to Xtend class

	/** 
	 * Checks if a header template is already present in the given GUI model
	 */
	override canExecute(GUI gui) throws ClassCastException {
		return !gui.getAllNodes().stream().filter([n|n instanceof Template]).map([n|((n as Template))]).filter([n |
			n.getBlockName().equals("header")
		]).findAny().isPresent()
	}

	/** 
	 * Creates a new header template on the given GUI model
	 */
	override execute(GUI gui) {
		var Template t = gui.newTemplate(100, 100)
		t.setBlockName("header")
	}
}
