package info.scce.dime.gui.actions

import graphmodel.IdentifiableElement
import info.scce.dime.api.DIMECustomAction

abstract class DIMEGUICustomAction<T extends IdentifiableElement> extends DIMECustomAction<T>{
	protected extension GUIActionExtension = new GUIActionExtension
}