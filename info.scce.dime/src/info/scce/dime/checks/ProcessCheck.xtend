package info.scce.dime.checks

import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.helper.ProcessExtension

abstract class ProcessCheck extends info.scce.dime.process.mcam.modules.checks.ProcessCheck {
	
	protected extension DataExtension = DataExtension.instance
	protected extension ProcessExtension = new ProcessExtension
	
	static class SwitchException extends IllegalStateException {
		new() { super("Default case in exhaustive switch; implementation broken?") }
	}
}