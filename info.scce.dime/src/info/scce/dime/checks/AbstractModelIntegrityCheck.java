package info.scce.dime.checks;

import graphmodel.GraphModel;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter;
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId;

public class AbstractModelIntegrityCheck<E extends _CincoId, M extends _CincoAdapter<E, ? extends GraphModel>> extends AbstractCheck<E,M> {
	
	@Override
	public void doExecute(M adapter) {
		Resource res = null;
		try {
			res = adapter.getModel().eResource();
		} catch(Exception e) {
			fail(adapter, "failed to retrieve resource");
			return;
		}
		if (res == null) {
			fail(adapter, "failed to retrieve resource");
			return;
		}
		if (res instanceof XtextResource) {
			if (((XtextResource) res).getParseResult().hasSyntaxErrors()) {
				fail(adapter, "model has syntax errors");
				return;
			}
		}
	}
	
	private void fail(M adapter, String msg) {
		addError(adapter.getIdByString(adapter.getModel().getId()), msg);
		processResults();
	}
	
	@Override
	public void init() { /* nothing to do */ }


}
