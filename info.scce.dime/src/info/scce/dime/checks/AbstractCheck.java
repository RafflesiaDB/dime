package info.scce.dime.checks;

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter;
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId;
import graphmodel.GraphModel;
import info.scce.mcam.framework.modules.CheckModule;

public abstract class AbstractCheck<E extends _CincoId, M extends _CincoAdapter<E, ? extends GraphModel>> extends CheckModule<E,M> {
	
	@Override
	public final void execute(M adapter) {
		try {
			doExecute(adapter);
		} catch(Exception e) {
			addError(adapter.getIdByString(adapter.getModel().getId()), "check execution failed");
			System.err.println(e);
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	abstract public void doExecute(M adapter);
}
