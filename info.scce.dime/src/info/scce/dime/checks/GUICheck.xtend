package info.scce.dime.checks

import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.gui.helper.GUIExtensionProvider

abstract class GUICheck extends info.scce.dime.gui.mcam.modules.checks.GUICheck {
	
	protected extension DataExtension = DataExtension.instance
	protected extension ProcessExtension = new ProcessExtension
	protected extension GUIExtension guiex = GUIExtensionProvider.guiextension
	
//	override init() {
//		guiex = GUIExtensionProvider.guiextension
//	}
}