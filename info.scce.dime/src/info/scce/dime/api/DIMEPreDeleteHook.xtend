package info.scce.dime.api

import de.jabc.cinco.meta.runtime.hook.CincoPreDeleteHook
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension
import org.eclipse.emf.ecore.EObject

abstract class DIMEPreDeleteHook<E extends EObject> extends CincoPreDeleteHook<E> {
	
	protected extension DataExtension = DataExtension.instance
}