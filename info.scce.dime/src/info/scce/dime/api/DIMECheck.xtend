package info.scce.dime.api

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import graphmodel.GraphModel
import info.scce.dime.checks.AbstractCheck
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension

abstract class DIMECheck<E extends _CincoId, M extends _CincoAdapter<E, ? extends GraphModel>> extends AbstractCheck<E, M> {
	
	protected extension GraphModelExtension = new GraphModelExtension
	protected extension DataExtension = DataExtension.instance
	
	
	/**
	 * Always reinitialize extensions to avoid caching problems. This method is final. If you want to add own
	 * behavior to be executed before every check, implement #beforeCheck 
	 */
	override final init() {
		_graphModelExtension = new GraphModelExtension	
		_dataExtension = DataExtension.instance
		beforeCheck()	
	}
	
	def void beforeCheck() {
		// does nothing by default
	}
	
	
}