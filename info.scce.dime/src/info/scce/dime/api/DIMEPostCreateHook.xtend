package info.scce.dime.api

import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension
import org.eclipse.emf.ecore.EObject
import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook

abstract class DIMEPostCreateHook<E extends EObject> extends CincoPostCreateHook<E> {
	
	protected extension DataExtension = DataExtension.instance
	protected extension GUIExtension = new GUIExtension
}