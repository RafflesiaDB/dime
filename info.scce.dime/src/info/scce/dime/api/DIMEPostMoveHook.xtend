package info.scce.dime.api

import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension
import de.jabc.cinco.meta.runtime.xapi.CollectionExtension
import de.jabc.cinco.meta.runtime.hook.CincoPostMoveHook
import graphmodel.ModelElement

abstract class DIMEPostMoveHook<E extends ModelElement> extends CincoPostMoveHook<E> {
	
	// FIXME push meta extensions to super class as soon as the hook class hierarchy does not require the C-API stuff anymore
	
	protected extension CollectionExtension = new CollectionExtension
    protected extension WorkspaceExtension = new WorkspaceExtension
    protected extension WorkbenchExtension = new WorkbenchExtension
    protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
	protected extension ResourceExtension = new ResourceExtension
    protected extension FileExtension = new FileExtension
	protected extension DataExtension = DataExtension.instance
}