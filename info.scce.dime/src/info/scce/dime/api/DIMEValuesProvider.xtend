package info.scce.dime.api

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import graphmodel.ModelElement
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension

abstract class DIMEValuesProvider<E extends ModelElement, A> extends CincoValuesProvider<E, A> {
	
	protected extension DataExtension = DataExtension.instance
}