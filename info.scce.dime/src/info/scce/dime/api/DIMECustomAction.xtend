package info.scce.dime.api

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import graphmodel.IdentifiableElement
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.helper.GUIExtension
import info.scce.dime.process.helper.ProcessExtension

abstract class DIMECustomAction<T extends IdentifiableElement> extends CincoCustomAction<T> {
	
	protected extension DataExtension = DataExtension.instance
	protected extension ProcessExtension = new ProcessExtension
	protected extension GUIExtension = new GUIExtension
}