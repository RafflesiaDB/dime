package info.scce.dime.api.modelgen

import de.jabc.cinco.meta.core.ge.style.generator.runtime.layout.EdgeLayout
import de.jabc.cinco.meta.core.ge.style.generator.runtime.layout.EdgeLayoutUtils
import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.Container
import graphmodel.Edge
import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.dime.data.data.BidirectionalAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.gui.actions.PrimitivePortToStatic
import info.scce.dime.gui.factory.GUIFactory
import info.scce.dime.gui.gui.Attribute
import info.scce.dime.gui.gui.Branch
import info.scce.dime.gui.gui.ComplexAttribute
import info.scce.dime.gui.gui.ComplexInputPort
import info.scce.dime.gui.gui.ComplexVariable
import info.scce.dime.gui.gui.DataContext
import info.scce.dime.gui.gui.InputPort
import info.scce.dime.gui.gui.InputStatic
import info.scce.dime.gui.gui.OutputPort
import info.scce.dime.gui.gui.PrimitiveAttribute
import info.scce.dime.gui.gui.PrimitiveInputPort
import info.scce.dime.gui.gui.PrimitiveType
import info.scce.dime.gui.gui.PrimitiveVariable
import info.scce.dime.gui.gui.SIB
import info.scce.dime.gui.gui.TextInputStatic
import info.scce.dime.gui.gui.Variable
import info.scce.dime.gui.helper.GUIExtension
import java.util.Set
import org.eclipse.core.runtime.IPath

import static extension info.scce.dime.process.helper.PortUtils.toPrimitiveType

class GUIModelGenerationLanguage extends CincoRuntimeBaseClass {
	
	protected extension DataExtension = DataExtension.instance
	protected extension GUIExtension = new GUIExtension
	extension EdgeLayoutUtils = new EdgeLayoutUtils
	
	static val VARIABLE_DEFAULT_HEIGHT = 25
	static val DATACONTEXT_PADDING_TOP = 23
	static val VARIABLE_MARGIN_X = 10
	static val VARIABLE_MARGIN_Y = 20
	static val VARIABLE_PADDING_TOP = 23
	static val VARIABLE_UNFOLDED_ATTRIBUTE_MARGIN_TOP = 20
	static val ATTRIBUTE_DEFAULT_HEIGHT = 25
	static val ATTRIBUTE_MARGIN_X = 10
	static val ATTRIBUTE_MARGIN_Y = 5
	static val NODE_PADDING_Y = 20
	
	val guiFactory = GUIFactory.eINSTANCE
	
	
	def createGUI(IPath outlet, String name) {
		guiFactory.createGUI(outlet.toOSString, name)
	}
	
	def getAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].head
	}
	
	def getAttribute(ComplexVariable compVar, String name) {
		compVar.findThe(Attribute)[
			switch it {
				ComplexAttribute: it.attribute.name == name
				PrimitiveAttribute: it.attribute.name == name
				default: false
			}
		]
	}
	
	def getComplexAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].filter(info.scce.dime.data.data.ComplexAttribute).head
	}
	
	def getPrimitiveAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].filter(info.scce.dime.data.data.PrimitiveAttribute).head
	}
	
	def getBidirectionalAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].filter(BidirectionalAttribute).head
	}
	
	dispatch def Attribute addAttribute(ComplexVariable compVar, String attrName) {
		val parentType = compVar.dataType
		val attr = parentType.getAttribute(attrName)
		compVar.addAttribute(attr)
	}
	
	dispatch def Attribute addAttribute(ComplexVariable it, info.scce.dime.data.data.PrimitiveAttribute attr) {
		val newAttr = newPrimitiveAttribute(attr, ATTRIBUTE_MARGIN_X, beneathBottomAttribute, maxAttributeWidth, ATTRIBUTE_DEFAULT_HEIGHT)
		increaseIfNecessary
		return newAttr
	}
	
	dispatch def Attribute addAttribute(ComplexVariable it, info.scce.dime.data.data.ComplexAttribute attr) {
		val newAttr = newComplexAttribute(attr, ATTRIBUTE_MARGIN_X, beneathBottomAttribute, maxAttributeWidth, ATTRIBUTE_DEFAULT_HEIGHT)
		increaseIfNecessary
		return newAttr
	}
	
	def unfoldAttribute(ComplexVariable compVar, String attrName) {
		val parentType = compVar.dataType
		val attr = parentType.getComplexAttribute(attrName)
		val type = attr.dataType
		val dataContext = compVar.container
		dataContext.newComplexVariable(type,
			compVar.x, compVar.bottom + VARIABLE_UNFOLDED_ATTRIBUTE_MARGIN_TOP, compVar.width, VARIABLE_DEFAULT_HEIGHT
		) => [
			name = attr.name
			compVar.newComplexAttributeConnector(it) => [
	  			associationName = attrName
			]
		]
	}
	
	def addVariable(DataContext it, Type varType, boolean varIsList, String varName) {
		newComplexVariable(varType,
			VARIABLE_MARGIN_X, beneathBottomVariable, maxVariableWidth, VARIABLE_DEFAULT_HEIGHT
		) => [
			isList  = varIsList
			name = varName
		]
	}
	
	def addVariable(DataContext it, PrimitiveType varType, boolean varIsList, String varName) {
		newPrimitiveVariable(
			VARIABLE_MARGIN_X, beneathBottomVariable, maxVariableWidth, VARIABLE_DEFAULT_HEIGHT
		) => [
			dataType = varType
			isList  = varIsList
			name = varName
		]
	}
	
	dispatch def addVariable(DataContext dataContext, info.scce.dime.process.process.ComplexInputPort port) {
		dataContext.addVariable(port.dataType, port.isList, port.name) as Variable
	}
	
	dispatch def addVariable(DataContext dataContext, info.scce.dime.process.process.PrimitiveInputPort port) {
		dataContext.addVariable(_dataExtension.toData(port.dataType).toGUI, port.isList, port.name) as Variable
	}
	
	dispatch def addVariable(DataContext dataContext, info.scce.dime.process.process.InputStatic port) {
		dataContext.addVariable(_dataExtension.toData(port.toPrimitiveType).toGUI, false, port.name) as Variable
	}
	
//	dispatch def addInputPort(Container dataFlowTarget, info.scce.dime.data.data.Type dataType) {
//		PortUtils.addInput(dataFlowTarget, dataType) as ComplexInputPort
//	}
//	
//	dispatch def addInputPort(Container dataFlowTarget, info.scce.dime.data.data.PrimitiveAttribute dataAttr) {
//		PortUtils.addInput(dataFlowTarget, dataAttr) as PrimitiveInputPort
//	}
//	
//	dispatch def addInputPort(Container dataFlowTarget, PrimitiveAttribute attr) {
//		PortUtils.addInput(dataFlowTarget, attr) as PrimitiveInputPort => [
//			it.name = attr.attribute.name
//		]
//	}
//	
//	dispatch def addInputPort(Container dataFlowTarget, ComplexAttribute attr) {
//		PortUtils.addInput(dataFlowTarget, attr.toComplex) as ComplexInputPort => [
//			it.name = attr.attribute.name
//		]
//	}
//	
//	dispatch def addInputPort(Container dataFlowTarget, PrimitiveVariable primVar) {
//		PortUtils.addInput(dataFlowTarget, primVar.toPrimitve) as PrimitiveInputPort => [
//			it.name = primVar.name
//		]
//	}
//	
//	dispatch def addInputPort(Container dataFlowTarget, ComplexVariable compVar) {
//		PortUtils.addInput(dataFlowTarget, compVar.toComplex) as ComplexInputPort => [
//			it.name = compVar.name
//		]
//	}
//	
//	dispatch def addTextInputPort(Container dataFlowTarget, String name) {
//		PortUtils.addInput(dataFlowTarget, PrimitiveType.TEXT) as PrimitiveInputPort => [
//			it.name = name
//		]
//	}
	
	dispatch def dataFlowTo(ComplexAttribute attr, ComplexInputPort port) {
		attr.newComplexRead(port)
	}
	
	dispatch def dataFlowTo(PrimitiveAttribute attr, PrimitiveInputPort port) {
		attr.newPrimitiveRead(port)
	}
	
	dispatch def dataFlowTo(ComplexVariable compVar, ComplexInputPort port) {
		compVar.newComplexRead(port)
	}
	
	dispatch def dataFlowTo(PrimitiveVariable primVar, PrimitiveInputPort port) {
		primVar.newPrimitiveRead(port)
	}
	
	dispatch def dataFlowFrom(PrimitiveInputPort inPort, PrimitiveVariable primVar) {
		primVar.newPrimitiveRead(inPort)
	}
	
	dispatch def dataFlowFrom(ComplexInputPort inPort, ComplexVariable compVar) {
		compVar.newComplexRead(inPort)
	}
	
	dispatch def dataFlowFrom(PrimitiveInputPort inPort, PrimitiveAttribute primAttr) {
		primAttr.newPrimitiveRead(inPort)
	}
	
	dispatch def dataFlowFrom(ComplexInputPort inPort, ComplexAttribute compAttr) {
		compAttr.newComplexRead(inPort)
	}
	
	def getInputPort(SIB sib, String name) {
		sib.findThe(InputPort)[it.name == name]
	}
	
	def getOutputPort(Branch branch, String name) {
		branch.findThe(OutputPort)[it.name == name]
	}
	
	def getBranch(SIB sib, String name) {
		sib.find(Branch)
			.filter[it.name == name]
			.head
	}
	
	def keepBranches(SIB sib, String... names) {
		sib.find(Branch)
			.filter[!names.contains(it.name)]
			.forEach[delete]
	}
	
	def layoutMiddleToCenter(Edge it) {
		transact[
			addBendpoint(
				targetElement.topCenter.x,
				sourceElement.middleLeft.y
			)
		]
	}
	
	def increaseIfNecessary(Container it) {
		increaseIfNecessaryRight
		increaseIfNecessaryBottom
	}
	
	dispatch def void increaseIfNecessaryBottom(DataContext container) {
		increaseIfNecessaryBottom(container, VARIABLE_MARGIN_Y)
	}
	
	dispatch def void increaseIfNecessaryBottom(Variable container) {
		increaseIfNecessaryBottom(container, ATTRIBUTE_MARGIN_Y)
	}
	
	def void increaseIfNecessaryBottom(Container container, int padding) {
		val botNode = container.bottomChild
		if (botNode != null) {
			container.resize(container.width, botNode.bottom + padding)
			switch it:container.container {
				Container: increaseIfNecessaryBottom
			}
		}
	}
	
	dispatch def void increaseIfNecessaryRight(DataContext container) {
		increaseIfNecessaryRight(container, VARIABLE_MARGIN_X)
	}
	
	dispatch def void increaseIfNecessaryRight(Variable container) {
		increaseIfNecessaryRight(container, ATTRIBUTE_MARGIN_X)
	}
	
	def void increaseIfNecessaryRight(Container container, int padding) {
		val rightNode = container.rightChild
		if (rightNode != null) {
			container.resize(container.width, rightNode.right + padding)
			switch it:container.container {
				Container: increaseIfNecessaryRight
			}
		}
	}
	
	def moveConnected(Node node, int deltaX, int deltaY) {
		moveConnected_rec(node, deltaX, deltaY, newHashSet(node))
	}
	
	def void moveConnected_rec(Node node, int deltaX, int deltaY, Set<Node> seen) {
		for (n : (node.successors + node.predecessors)) {
			if (seen.add(n)) {
				n.move(n.x + deltaX, n.y + deltaY)
				moveConnected_rec(n, deltaX, deltaY, seen)
			}
		}
	}
	
	def moveSuccessors(Node node, int deltaX, int deltaY) {
		moveSuccessors_rec(node, deltaX, deltaY, newHashSet(node))
	}
	
	def void moveSuccessors_rec(Node node, int deltaX, int deltaY, Set<Node> seen) {
		for (n : node.successors) {
			if (seen.add(n)) {
				n.move(n.x + deltaX, n.y + deltaY)
				moveSuccessors_rec(n, deltaX, deltaY, seen)
			}
		}
	}
	
	def alignWithTopOf(Node node, Node otherNode) {
		node.move(node.x, otherNode.y)
	}
	
	def alignWithBottomOf(Node node, Node otherNode) {
		node.move(node.x, otherNode.y + otherNode.height - node.height)
	}
	
	def moveToBottomOf(Node node, Node otherNode, int margin) {
		node.move(
			otherNode.center - node.width / 2,
			otherNode.bottom + margin
		)
	}
	
	def moveToBottom(Node it) {
		val oldX = x
		val oldY = y
		move(x, container.bottomChild.bottom + NODE_PADDING_Y)
		val deltaX = x - oldX
		val deltaY = y - oldY
		if (deltaX != 0 || deltaY != 0) {
			moveSuccessors(deltaX, deltaY)
		}
	}
	
	def moveToRight(Node it) {
		val oldX = x
		val oldY = y
		move(container.bottomChild.right + NODE_PADDING_Y, y)
		val deltaX = x - oldX
		val deltaY = y - oldY
		if (deltaX != 0 || deltaY != 0) {
			moveSuccessors(deltaX, deltaY)
		}
	}
	
	def moveToBottomRight(Node it) {
		val oldX = x
		val oldY = y
		move(
			container.bottomChild.right + NODE_PADDING_Y,
			container.bottomChild.bottom + NODE_PADDING_Y
		)
		val deltaX = x - oldX
		val deltaY = y - oldY
		if (deltaX != 0 || deltaY != 0) {
			moveSuccessors(deltaX, deltaY)
		}
	}
	
	dispatch def alignBeneathOf(Node node, Node otherNode) {
		node.moveToBottomOf(otherNode, NODE_PADDING_Y)
	}
	
	dispatch def alignRightOf(Node node, Node otherNode) {
		node.moveToRightOf(otherNode, NODE_PADDING_Y)
	}
	
	def moveToRightOf(Node node, Node otherNode, int margin) {
		node.move(
			otherNode.right + margin,
			otherNode.middle - node.height / 2
		)
	}
	
	def getCenter(Node it) {
		x + width / 2
	}
	
	def getBottom(Node it) {
		y + height
	}
	
	def getRight(Node it) {
		x + width
	}
	
	def getMiddle(Node it) {
		y + height / 2
	}
	
	def getBottomChild(ModelElementContainer it) {
		allNodes.sortBy[bottom].reverse.head
	}
	
	def getRightChild(Container it) {
		allNodes.sortBy[right].reverse.head
	}
	
	def beneathBottomAttribute(Container con) {
		beneathBottomChild(con, ATTRIBUTE_MARGIN_Y, VARIABLE_PADDING_TOP)
	}
	
	def beneathBottomVariable(Container con) {
		beneathBottomChild(con, VARIABLE_MARGIN_Y, DATACONTEXT_PADDING_TOP)
	}
	
	def beneathBottomChild(Container con, int margin, int min) {
		val botChild = con.bottomChild
		if (botChild !== null) {
			botChild.bottom + margin
		} else {
			min
		}
	}
	
	def maxAttributeWidth(Container con) {
		maxChildWidth(con, ATTRIBUTE_MARGIN_X)
	}
	
	def maxVariableWidth(Container con) {
		maxChildWidth(con, VARIABLE_MARGIN_X)
	}
	
	def maxChildWidth(Container con, int padding) {
		con.width - padding * 2
	}
	
	def applyLayout(Edge edge, EdgeLayout layout) {
		layout.apply(edge)
	}
	
	def getByID(String id) {
		ReferenceRegistry.instance.getEObject(id)
	}
	
	dispatch def setStaticInput(InputPort inPort, String value) {
		val name = inPort.name
		val sib = inPort.container
		new PrimitivePortToStatic().execute(inPort as PrimitiveInputPort)
		val staticInPort = sib.findThe(InputStatic)[it.name == name]
		switch it:staticInPort {
			TextInputStatic: it.value = value
			// BooleanInputStatic
			// IntegerInputStatic
			// RealInputStatic
			// TimestampInputStatic
		}
		return staticInPort
	}
}