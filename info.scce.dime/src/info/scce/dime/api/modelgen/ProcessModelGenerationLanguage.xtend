package info.scce.dime.api.modelgen

import de.jabc.cinco.meta.core.ge.style.generator.runtime.layout.EdgeLayout
import de.jabc.cinco.meta.core.ge.style.generator.runtime.layout.EdgeLayoutUtils
import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.Container
import graphmodel.Edge
import graphmodel.ModelElementContainer
import graphmodel.Node
import graphmodel.internal.InternalEdge
import info.scce.dime.data.data.BidirectionalAttribute
import info.scce.dime.data.data.ConcreteType
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.DataExtension
import info.scce.dime.process.actions.PrimitivePortToStatic
import info.scce.dime.process.factory.ProcessFactory
import info.scce.dime.process.helper.EdgeLayoutUtils.Location
import info.scce.dime.process.helper.PortUtils
import info.scce.dime.process.helper.ProcessExtension
import info.scce.dime.process.process.Attribute
import info.scce.dime.process.process.Branch
import info.scce.dime.process.process.BranchConnector
import info.scce.dime.process.process.ComplexAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.ComplexOutputPort
import info.scce.dime.process.process.ComplexVariable
import info.scce.dime.process.process.DataContext
import info.scce.dime.process.process.DataFlowSource
import info.scce.dime.process.process.DataFlowTarget
import info.scce.dime.process.process.InputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.OutputPort
import info.scce.dime.process.process.PrimitiveAttribute
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.PrimitiveOutputPort
import info.scce.dime.process.process.PrimitiveType
import info.scce.dime.process.process.PrimitiveVariable
import info.scce.dime.process.process.ProcessType
import info.scce.dime.process.process.SIB
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.Variable
import java.util.Set
import org.eclipse.core.runtime.IPath
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl

import static info.scce.dime.process.helper.EdgeLayoutUtils.getManhattanPoints
import static org.eclipse.emf.common.util.URI.createPlatformResourceURI
import static org.eclipse.emf.ecore.util.EcoreUtil.generateUUID

import static extension info.scce.dime.process.helper.SIBLayoutUtils.resizeAndLayout
import static extension org.eclipse.emf.ecore.util.EcoreUtil.setID

/**
 * Extension API for convenient process model generation.
 * 
 * @author Steve Bosselmann
 */
class ProcessModelGenerationLanguage extends CincoRuntimeBaseClass {
	
	extension DataExtension = DataExtension.instance
	extension ProcessExtension = new ProcessExtension
	extension EdgeLayoutUtils = new EdgeLayoutUtils
	extension PortUtils = new PortUtils
	
	public static val VARIABLE_DEFAULT_HEIGHT = 25
	public static val DATACONTEXT_PADDING_TOP = 23
	public static val VARIABLE_MARGIN_X = 10
	public static val VARIABLE_MARGIN_Y = 20
	public static val VARIABLE_PADDING_TOP = 23
	public static val VARIABLE_UNFOLDED_ATTRIBUTE_MARGIN_TOP = 20
	public static val ATTRIBUTE_DEFAULT_HEIGHT = 25
	public static val ATTRIBUTE_MARGIN_X = 10
	public static val ATTRIBUTE_MARGIN_Y = 5
	public static val BRANCH_TO_SIB_OFFSET = 30
	public static val SIB_TO_BRANCH_OFFSET = 10
	
	ProcessFactory processFactory = ProcessFactory.eINSTANCE
	
	
	def createBasicProcess(IPath outlet, String name) {
		createBasicProcess(outlet, name, true, true)
	}
	
	def createBasicProcess(IPath outlet, String name, boolean runPostCreate, boolean save) {
		val filePath = outlet.append(name).addFileExtension("process")
		val uri = createPlatformResourceURI(filePath.toOSString, true)
		val res = new ResourceSetImpl().createResource(uri)
	    val process = processFactory.createProcess => [
	    	processType = ProcessType.BASIC
	    	setID(generateUUID)
	    ]
	    res.contents.add(process.internalElement)
	    if (runPostCreate) {
	    	processFactory.postCreates(process)
	    }
	    if (save) {
	    	process.save
	    }
	    return process
	}
	
	def getAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].head
	}
	
	def getAttribute(ComplexVariable compVar, String name) {
		compVar.findThe(Attribute)[
			switch it {
				ComplexAttribute: it.attribute.name == name
				PrimitiveAttribute: it.attribute.name == name
				default: false
			}
		]
	}
	
	def getComplexAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].filter(info.scce.dime.data.data.ComplexAttribute).head
	}
	
	def getPrimitiveAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].filter(info.scce.dime.data.data.PrimitiveAttribute).head
	}
	
	def getBidirectionalAttribute(Type propType, String name) {
		propType.inheritedAttributes.filter[it.name == name].filter(BidirectionalAttribute).head
	}
	
	dispatch def Attribute addAttribute(ComplexVariable compVar, String attrName) {
		val parentType = compVar.dataType
		val attr = parentType.getAttribute(attrName)
		compVar.addAttribute(attr)
	}
	
	dispatch def Attribute addAttribute(ComplexVariable it, info.scce.dime.data.data.PrimitiveAttribute attr) {
		val newAttr = newPrimitiveAttribute(attr, ATTRIBUTE_MARGIN_X, beneathBottomAttribute, maxAttributeWidth, ATTRIBUTE_DEFAULT_HEIGHT)
		increaseIfNecessary
		return newAttr
	}
	
	dispatch def Attribute addAttribute(ComplexVariable it, info.scce.dime.data.data.ComplexAttribute attr) {
		val newAttr = newComplexAttribute(attr, ATTRIBUTE_MARGIN_X, beneathBottomAttribute, maxAttributeWidth, ATTRIBUTE_DEFAULT_HEIGHT)
		increaseIfNecessary
		return newAttr
	}
	
	def unfoldAttribute(ComplexVariable compVar, String attrName) {
		val parentType = compVar.dataType
		val attr = parentType.getComplexAttribute(attrName)
		val type = attr.dataType
		val dataContext = compVar.container
		dataContext.newComplexVariable(type,
			compVar.x, compVar.bottom + VARIABLE_UNFOLDED_ATTRIBUTE_MARGIN_TOP, compVar.width, VARIABLE_DEFAULT_HEIGHT
		) => [
			name = attr.name
			compVar.newComplexAttributeConnector(it) => [
	  			attributeName = attrName
			]
		]
	}
	
	def addVariable(DataContext ctx, ConcreteType type) {
		addVariable(ctx, type, type.name.toFirstLower)
	}
	
	def addVariable(DataContext it, ConcreteType type, String varName) {
		newComplexVariable(type,
			VARIABLE_MARGIN_X, beneathBottomVariable, maxVariableWidth, VARIABLE_DEFAULT_HEIGHT
		) => [
			name = varName
		]
	}
	
	dispatch def addInputPort(Container dataFlowTarget, Type dataType) {
		PortUtils.addInput(dataFlowTarget, dataType) as ComplexInputPort
	}
	
	dispatch def addInputPort(Container dataFlowTarget, info.scce.dime.data.data.PrimitiveAttribute dataAttr) {
		PortUtils.addInput(dataFlowTarget, dataAttr) as PrimitiveInputPort
	}
	
	dispatch def addInputPort(Container dataFlowTarget, PrimitiveAttribute attr) {
		PortUtils.addInput(dataFlowTarget, attr) as PrimitiveInputPort => [
			it.name = attr.attribute.name
		]
	}
	
	dispatch def addInputPort(Container dataFlowTarget, info.scce.dime.data.data.ComplexAttribute attr) {
		PortUtils.addInput(dataFlowTarget, attr) as ComplexInputPort
	}
	
	dispatch def addInputPort(Container dataFlowTarget, ComplexAttribute attr) {
		PortUtils.addInput(dataFlowTarget, attr.complexType) as ComplexInputPort => [
			it.name = attr.attribute.name
		]
	}
	
	dispatch def addInputPort(Container dataFlowTarget, PrimitiveVariable primVar) {
		PortUtils.addInput(dataFlowTarget, primVar.primitiveType) as PrimitiveInputPort => [
			it.name = primVar.name
		]
	}
	
	dispatch def addInputPort(Container dataFlowTarget, ComplexVariable compVar) {
		PortUtils.addInput(dataFlowTarget, compVar.complexType) as ComplexInputPort => [
			it.name = compVar.name
		]
	}
	
	dispatch def addBooleanInputPort(Container dataFlowTarget, String name) {
		PortUtils.addInput(dataFlowTarget, PrimitiveType.BOOLEAN) as PrimitiveInputPort => [
			it.name = name
		]
	}
	
	dispatch def addIntegerInputPort(Container dataFlowTarget, String name) {
		PortUtils.addInput(dataFlowTarget, PrimitiveType.INTEGER) as PrimitiveInputPort => [
			it.name = name
		]
	}
	
	dispatch def addRealInputPort(Container dataFlowTarget, String name) {
		PortUtils.addInput(dataFlowTarget, PrimitiveType.REAL) as PrimitiveInputPort => [
			it.name = name
		]
	}
	
	dispatch def addTimestampInputPort(Container dataFlowTarget, String name) {
		PortUtils.addInput(dataFlowTarget, PrimitiveType.TIMESTAMP) as PrimitiveInputPort => [
			it.name = name
		]
	}
	
	dispatch def addTextInputPort(Container dataFlowTarget, String name) {
		PortUtils.addInput(dataFlowTarget, PrimitiveType.TEXT) as PrimitiveInputPort => [
			it.name = name
		]
	}
	
	def addStaticInputPort(SIB sib, info.scce.dime.data.data.PrimitiveType type, String name, Object value) {
		switch type {
			case BOOLEAN:
				sib.addStaticBooleanInputPort(name) => [
					it.value = (value as Boolean)
				]
			case FILE:
				throw new UnsupportedOperationException("Static input ports of type FILE are not supported")
			case INTEGER:
				sib.addStaticIntegerInputPort(name) => [
					it.value = (value as Long)
				]
			case REAL:
				sib.addStaticRealInputPort(name) => [
					it.value = (value as Double)
				]
			case TEXT:
				sib.addStaticTextInputPort(name) => [
					it.value = (value as String)
				]
			case TIMESTAMP:
				sib.addStaticTimestampInputPort(name) => [
					it.value = (value as Long)
				]
		}
	}
	
	dispatch def addStaticBooleanInputPort(DataFlowTarget dataFlowTarget, String name) {
		dataFlowTarget.newBooleanInputStatic(0,0) => [
			it.name = name
			dataFlowTarget.resizeAndLayout
		]
	}
	
	dispatch def addStaticIntegerInputPort(DataFlowTarget dataFlowTarget, String name) {
		dataFlowTarget.newIntegerInputStatic(0,0) => [
			it.name = name
			dataFlowTarget.resizeAndLayout
		]
	}
	
	dispatch def addStaticRealInputPort(DataFlowTarget dataFlowTarget, String name) {
		dataFlowTarget.newRealInputStatic(0,0) => [
			it.name = name
			dataFlowTarget.resizeAndLayout
		]
	}
	
	dispatch def addStaticTextInputPort(DataFlowTarget dataFlowTarget, String name) {
		dataFlowTarget.newTextInputStatic(0,0) => [
			it.name = name
			dataFlowTarget.resizeAndLayout
		]
	}
	
	dispatch def addStaticTimestampInputPort(DataFlowTarget dataFlowTarget, String name) {
		dataFlowTarget.newTimestampInputStatic(0,0) => [
			it.name = name
			dataFlowTarget.resizeAndLayout
		]
	}
	
	dispatch def addTextOutputPort(DataFlowSource dataFlowSource, String name) {
		/*PortUtils.*/addOutput(dataFlowSource, PrimitiveType.TEXT) as PrimitiveInputPort => [
			it.name = name
		]
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, Type dataType) {
		val oldW = dataFlowSource.width
		val oldH = dataFlowSource.height
		val port = /*PortUtils.*/addOutput(dataFlowSource, dataType) as ComplexOutputPort
		val deltaW = dataFlowSource.width - oldW
		val deltaH = dataFlowSource.height - oldH
		if (deltaW != 0 || deltaH != 0) {
			dataFlowSource.moveSuccessors(deltaW, deltaH)
		}
		return port
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, info.scce.dime.data.data.PrimitiveAttribute dataAttr) {
		val oldW = dataFlowSource.width
		val oldH = dataFlowSource.height
		val port = /*PortUtils.*/addOutput(dataFlowSource, dataAttr) as PrimitiveOutputPort
		val deltaW = dataFlowSource.width - oldW
		val deltaH = dataFlowSource.height - oldH
		if (deltaW != 0 || deltaH != 0) {
			dataFlowSource.moveSuccessors(deltaW, deltaH)
		}
		return port
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, info.scce.dime.data.data.ComplexAttribute attr) {
		dataFlowSource.addOuputPort[
			/*PortUtils.*/addOutput(dataFlowSource, attr.dataType) as ComplexOutputPort
		]
	}
	
	dispatch def OutputPort addOutputPort(DataFlowSource dataFlowSource, PrimitiveAttribute attr) {
		val oldW = dataFlowSource.width
		val oldH = dataFlowSource.height
		val port = /*PortUtils.*/addOutput(dataFlowSource, attr.primitiveType) as PrimitiveOutputPort => [
			name = attr.attribute.name
		]
		val deltaW = dataFlowSource.width - oldW
		val deltaH = dataFlowSource.height - oldH
		if (deltaW != 0 || deltaH != 0) {
			dataFlowSource.moveSuccessors(deltaW, deltaH)
		}
		return port
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, ComplexAttribute attr) {
		val oldW = dataFlowSource.width
		val oldH = dataFlowSource.height
		val port = /*PortUtils.*/addOutput(dataFlowSource, attr.complexType) as ComplexOutputPort => [
			name = attr.attribute.name
		]
		val deltaW = dataFlowSource.width - oldW
		val deltaH = dataFlowSource.height - oldH
		if (deltaW != 0 || deltaH != 0) {
			dataFlowSource.moveSuccessors(deltaW, deltaH)
		}
		return port
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, PrimitiveVariable primVar) {
		val oldW = dataFlowSource.width
		val oldH = dataFlowSource.height
		val port = /*PortUtils.*/addOutput(dataFlowSource, primVar.primitiveType) as PrimitiveOutputPort => [
			name = primVar.name
		]
		val deltaW = dataFlowSource.width - oldW
		val deltaH = dataFlowSource.height - oldH
		if (deltaW != 0 || deltaH != 0) {
			dataFlowSource.moveSuccessors(deltaW, deltaH)
		}
		return port
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, ComplexVariable compVar) {
		val oldW = dataFlowSource.width
		val oldH = dataFlowSource.height
		val port = /*PortUtils.*/addOutput(dataFlowSource, compVar.complexType) as ComplexOutputPort => [
			name = compVar.name
		]
		val deltaW = dataFlowSource.width - oldW
		val deltaH = dataFlowSource.height - oldH
		if (deltaW != 0 || deltaH != 0) {
			dataFlowSource.moveSuccessors(deltaW, deltaH)
		}
		return port
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, PrimitiveInputPort input) {
		dataFlowSource.addOuputPort[
			/*PortUtils.*/addOutput(dataFlowSource, input.primitiveType) => [
				name = input.name
			]
		]
	}
	
	dispatch def addOutputPort(DataFlowSource dataFlowSource, ComplexInputPort input) {
		dataFlowSource.addOuputPort[
			/*PortUtils.*/addOutput(dataFlowSource, input.complexType) as ComplexOutputPort => [
				name = input.name
			]
		]
	}
	
	def addOuputPort(DataFlowSource dataFlowSource, (DataFlowSource)=>OutputPort portProducer) {
		val oldW = dataFlowSource.width
		val oldH = dataFlowSource.height
		val port = portProducer.apply(dataFlowSource)
		val deltaW = dataFlowSource.width - oldW
		val deltaH = dataFlowSource.height - oldH
		if (deltaW != 0 || deltaH != 0) {
			dataFlowSource.moveSuccessors(deltaW, deltaH)
		}
		return port
	}
	
	def addOutputPort(DataFlowSource branch, info.scce.dime.data.data.PrimitiveType type, String name) {
		branch.addOuputPort[
			branch.newPrimitiveOutputPort(0, 0) => [
				it.name = name
				isList = false
				dataType = PortUtils.toPrimitiveType(type)
			]
		]
	}
	
	dispatch def dataFlowTo(ComplexAttribute attr, ComplexInputPort port) {
		attr.newComplexRead(port)
	}
	
	dispatch def dataFlowTo(PrimitiveAttribute attr, PrimitiveInputPort port) {
		attr.newPrimitiveRead(port)
	}
	
	dispatch def dataFlowTo(ComplexVariable compVar, ComplexInputPort port) {
		compVar.newComplexRead(port)
	}
	
	dispatch def dataFlowTo(PrimitiveVariable primVar, PrimitiveInputPort port) {
		primVar.newPrimitiveRead(port)
	}
	
	dispatch def dataFlowTo(PrimitiveOutputPort outPort, PrimitiveVariable primVar) {
		outPort.newPrimitiveUpdate(primVar)
	}
	
	dispatch def dataFlowTo(ComplexOutputPort outPort, ComplexVariable compVar) {
		outPort.newComplexUpdate(compVar)
	}
	
	dispatch def dataFlowTo(PrimitiveOutputPort outPort, PrimitiveAttribute primAttr) {
		outPort.newPrimitiveUpdate(primAttr)
	}
	
	dispatch def dataFlowTo(PrimitiveOutputPort outPort, PrimitiveInputPort inPort) {
		outPort.newPrimitiveDirectDataFlow(inPort)
	}
	
	dispatch def dataFlowTo(ComplexOutputPort outPort, ComplexAttribute compAttr) {
		outPort.newComplexUpdate(compAttr)
	}
	
	dispatch def dataFlowTo(ComplexOutputPort outPort, ComplexInputPort inPort) {
		outPort.newComplexDirectDataFlow(inPort)
	}
	
	dispatch def dataFlowFrom(ComplexAttribute attr, ComplexOutputPort port) {
		port.newComplexUpdate(attr)
	}
	
	dispatch def dataFlowFrom(PrimitiveAttribute attr, PrimitiveOutputPort port) {
		port.newPrimitiveUpdate(attr)
	}
	
	dispatch def dataFlowFrom(ComplexVariable compVar, ComplexOutputPort port) {
		port.newComplexUpdate(compVar)
	}
	
	dispatch def dataFlowFrom(PrimitiveVariable primVar, PrimitiveOutputPort port) {
		port.newPrimitiveUpdate(primVar)
	}
	
	dispatch def dataFlowFrom(PrimitiveInputPort inPort, PrimitiveVariable primVar) {
		primVar.newPrimitiveRead(inPort)
	}
	
	dispatch def dataFlowFrom(ComplexInputPort inPort, ComplexVariable compVar) {
		compVar.newComplexRead(inPort)
	}
	
	dispatch def dataFlowFrom(PrimitiveInputPort inPort, PrimitiveAttribute primAttr) {
		primAttr.newPrimitiveRead(inPort)
	}
	
	dispatch def dataFlowFrom(ComplexInputPort inPort, ComplexAttribute compAttr) {
		compAttr.newComplexRead(inPort)
	}
	
	dispatch def dataFlowFrom(ComplexInputPort inPort, ComplexOutputPort outPort) {
		outPort.newComplexDirectDataFlow(inPort)
	}
	
	def setSuccessor(DataFlowSource branch, DataFlowTarget sib) {
		branch.newControlFlow(sib)
		sib.alignBeneathOf(branch)
	}
	
	def setPredecessor(DataFlowTarget sib, DataFlowSource branch) {
		branch.newControlFlow(sib)
		sib.alignBeneathOf(branch)
	}
	
	def getInputPort(DataFlowTarget sib, String name) {
		sib.findThe(InputPort)[it.name == name]
	}
	
	def getOutputPort(DataFlowSource branch, String name) {
		branch.findThe(OutputPort)[it.name == name]
	}
	
	def getBranch(DataFlowTarget sib, String name) {
		sib.findTargetsOf(BranchConnector)
			.filter(Branch)
			.filter[it.name == name]
			.head
	}
	
	def keepBranches(DataFlowTarget sib, String... names) {
		sib.findTargetsOf(BranchConnector)
			.filter(Branch)
			.filter[!names.contains(it.name)]
			.forEach[delete]
	}
	
	def layoutMiddleToCenter(Edge it) {
		transact[
			addBendpoint(
				targetElement.topCenter.x,
				sourceElement.middleLeft.y
			)
		]
	}
	
	def increaseIfNecessary(Container it) {
		increaseIfNecessaryRight
		increaseIfNecessaryBottom
	}
	
	dispatch def void increaseIfNecessaryBottom(DataContext container) {
		increaseIfNecessaryBottom(container, VARIABLE_MARGIN_Y)
	}
	
	dispatch def void increaseIfNecessaryBottom(Variable container) {
		increaseIfNecessaryBottom(container, ATTRIBUTE_MARGIN_Y)
	}
	
	def void increaseIfNecessaryBottom(Container container, int padding) {
		val botNode = container.bottomChild
		if (botNode != null) {
			container.resize(container.width, botNode.bottom + padding)
			switch it:container.container {
				Container: increaseIfNecessaryBottom
			}
		}
	}
	
	dispatch def void increaseIfNecessaryRight(DataContext container) {
		increaseIfNecessaryRight(container, VARIABLE_MARGIN_X)
	}
	
	dispatch def void increaseIfNecessaryRight(Variable container) {
		increaseIfNecessaryRight(container, ATTRIBUTE_MARGIN_X)
	}
	
	def void increaseIfNecessaryRight(Container container, int padding) {
		val rightNode = container.rightChild
		if (rightNode != null) {
			container.resize(container.width, rightNode.right + padding)
			switch it:container.container {
				Container: increaseIfNecessaryRight
			}
		}
	}
	
	def moveConnected(Node node, int deltaX, int deltaY) {
		moveConnected_rec(node, deltaX, deltaY, newHashSet(node))
	}
	
	def void moveConnected_rec(Node node, int deltaX, int deltaY, Set<Node> seen) {
		for (n : (node.successors + node.predecessors)) {
			if (seen.add(n)) {
				n.move(n.x + deltaX, n.y + deltaY)
				moveConnected_rec(n, deltaX, deltaY, seen)
			}
		}
	}
	
	def moveSuccessors(Node node, int deltaX, int deltaY) {
		moveSuccessors_rec(node, deltaX, deltaY, newHashSet(node))
	}
	
	def void moveSuccessors_rec(Node node, int deltaX, int deltaY, Set<Node> seen) {
		for (n : node.successors) {
			if (seen.add(n)) {
				n.move(n.x + deltaX, n.y + deltaY)
				moveSuccessors_rec(n, deltaX, deltaY, seen)
			}
		}
	}
	
	def alignWithTopOf(Node node, Node otherNode) {
		node.move(node.x, otherNode.y)
	}
	
	def alignWithBottomOf(Node node, Node otherNode) {
		node.move(node.x, otherNode.y + otherNode.height - node.height)
	}
	
	def moveToBottomOf(Node node, Node otherNode, int margin) {
		node.move(
			otherNode.center - node.width / 2,
			otherNode.bottom + margin
		)
	}
	
	def moveToBottom(Node it) {
		val oldX = x
		val oldY = y
		move(x, container.bottomChild.bottom + BRANCH_TO_SIB_OFFSET)
		val deltaX = x - oldX
		val deltaY = y - oldY
		if (deltaX != 0 || deltaY != 0) {
			moveSuccessors(deltaX, deltaY)
		}
	}
	
	def moveToRight(Node it) {
		val oldX = x
		val oldY = y
		move(container.bottomChild.right + BRANCH_TO_SIB_OFFSET, y)
		val deltaX = x - oldX
		val deltaY = y - oldY
		if (deltaX != 0 || deltaY != 0) {
			moveSuccessors(deltaX, deltaY)
		}
	}
	
	def moveToBottomRight(Node it) {
		val oldX = x
		val oldY = y
		move(
			container.bottomChild.right + BRANCH_TO_SIB_OFFSET,
			container.bottomChild.bottom + BRANCH_TO_SIB_OFFSET
		)
		val deltaX = x - oldX
		val deltaY = y - oldY
		if (deltaX != 0 || deltaY != 0) {
			moveSuccessors(deltaX, deltaY)
		}
	}
	
	dispatch def alignBeneathOf(Node node, Node otherNode) {
		node.moveToBottomOf(otherNode, BRANCH_TO_SIB_OFFSET)
	}
	
	dispatch def alignBeneathOf(DataFlowTarget node, DataFlowSource otherNode) {
		val oldX = node.x
		val oldY = node.y
		node.moveToBottomOf(otherNode, BRANCH_TO_SIB_OFFSET)
		val deltaX = node.x - oldX
		val deltaY = node.y - oldY
		if (deltaX != 0 || deltaY != 0) switch node {
			SIB: for (sibBranch : node.branchSuccessors) {
				sibBranch.move(sibBranch.x + deltaX, sibBranch.y + deltaY)
			}
		}
	}
	
	dispatch def alignBeneathOf(DataFlowSource node, DataFlowTarget otherNode) {
		node.moveToBottomOf(otherNode, SIB_TO_BRANCH_OFFSET)
	}
	
	dispatch def alignRightOf(Node node, Node otherNode) {
		node.moveToRightOf(otherNode, SIB_TO_BRANCH_OFFSET)
	}
	
	dispatch def alignRightOf(DataFlowSource branch, DataFlowTarget node) {
		val oldX = branch.x
		val oldY = branch.y
		branch.moveToRightOf(node, SIB_TO_BRANCH_OFFSET)
		val deltaX = branch.x - oldX
		val deltaY = branch.y - oldY
		if (deltaX != 0 || deltaY != 0) switch branch {
			Branch: for (sib : branch.SIBSuccessors) {
				sib.move(sib.x + deltaX, sib.y + deltaY)
			}
		}
	}
	
	dispatch def alignRightOf(DataFlowTarget node, DataFlowSource branch) {
		node.moveToRightOf(branch, BRANCH_TO_SIB_OFFSET)
	}
	
	def moveToRightOf(Node node, Node otherNode, int margin) {
		node.move(
			otherNode.right + margin,
			otherNode.middle - node.height / 2
		)
	}
	
	def getCenter(Node it) {
		x + width / 2
	}
	
	def getBottom(Node it) {
		y + height
	}
	
	def getRight(Node it) {
		x + width
	}
	
	def getMiddle(Node it) {
		y + height / 2
	}
	
	def getBottomChild(ModelElementContainer it) {
		allNodes.sortBy[bottom].reverse.head
	}
	
	def getRightChild(Container it) {
		allNodes.sortBy[right].reverse.head
	}
	
	def beneathBottomAttribute(Container con) {
		beneathBottomChild(con, ATTRIBUTE_MARGIN_Y, VARIABLE_PADDING_TOP)
	}
	
	def beneathBottomVariable(Container con) {
		beneathBottomChild(con, VARIABLE_MARGIN_Y, DATACONTEXT_PADDING_TOP)
	}
	
	def beneathBottomChild(Container con, int margin, int min) {
		val botChild = con.bottomChild
		if (botChild !== null) {
			botChild.bottom + margin
		} else {
			min
		}
	}
	
	def maxAttributeWidth(Container con) {
		maxChildWidth(con, ATTRIBUTE_MARGIN_X)
	}
	
	def maxVariableWidth(Container con) {
		maxChildWidth(con, VARIABLE_MARGIN_X)
	}
	
	def maxChildWidth(Container con, int padding) {
		con.width - padding * 2
	}
	
	def applyLayout(Edge edge, EdgeLayout layout) {
		layout.apply(edge)
	}
	
	def getByID(String id) {
		ReferenceRegistry.instance.getEObject(id)
	}
	
	dispatch def setStaticInput(InputPort inPort, String value) {
		val name = inPort.name
		val sib = inPort.container
		new PrimitivePortToStatic().execute(inPort as PrimitiveInputPort)
		val staticInPort = sib.findThe(InputStatic)[it.name == name]
		switch it:staticInPort {
			TextInputStatic: it.value = value
			// BooleanInputStatic
			// IntegerInputStatic
			// RealInputStatic
			// TimestampInputStatic
		}
		return staticInPort
	}
	
	def overtakeIncomingEdgesOf(Node newNode, Node node) {
		node.incoming.forEach[reconnectTarget(newNode)]
	}
	
	def overtakeOutgoingEdgesOf(Node newNode, Node node) {
		node.outgoing.forEach[reconnectSource(newNode)]
	}
	
	def overtakeBendpointsOf(Edge newEdge, Edge edge) {
		for (bp : (edge.internalElement as InternalEdge).bendpoints) {
			newEdge.addBendpoint(bp.x, bp.y)
		}
	}
	
	def applyManhattanLayout(Edge it) {
		for (Location p : getManhattanPoints(sourceElement, targetElement)) {
			addBendpoint(p.x, p.y)
		}
	}
}