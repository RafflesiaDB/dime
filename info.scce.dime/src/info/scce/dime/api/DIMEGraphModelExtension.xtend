package info.scce.dime.api

import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import graphmodel.GraphModel
import graphmodel.ModelElement
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import graphmodel.ModelElementContainer
import graphmodel.IdentifiableElement

class DIMEGraphModelExtension extends GraphModelExtension {
	protected extension ResourceExtension = new ResourceExtension	
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * 
	 * @param it - The model element in some model, for which the package should be built.
	 */
	dispatch def String getLocalPkg(ModelElement it) { rootElement.localPkg }
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * 
	 * @param it - The model, for which the package should be built.
	 */
	dispatch def getLocalPkg(GraphModel it) { buildLocalPkg(false) }
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * 
	 * @param it - The model element in some model, for which the package should be built.
	 */
	dispatch def String getLocalPkgWithFilename(ModelElement it) { rootElement.localPkgWithFilename }
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * 
	 * @param it - The model, for which the package should be built.
	 */
	dispatch def getLocalPkgWithFilename(GraphModel it) { buildLocalPkg(true) }
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * 
	 * @param model - The model element in some model, for which the package should be built.
	 * @param includeFileName - Include the name of the file (without extension) as the last element of the package.
	 */
	private def buildLocalPkg(GraphModel model, boolean includeFileName) {
		val project = model.eResource.project
		val absoluteProjectPath = project.fullPath.toFile.absolutePath
		val absoluteModelPath = switch (it : model.eResource.IResource.fullPath.toFile) {
			case !includeFileName: parentFile
			case includeFileName: it
		}.absolutePath
		if (!absoluteModelPath.startsWith(absoluteProjectPath)) throw new IllegalStateException(
			'''«model.id»: model path does not start with project path'''
		)
		val suffixPos = absoluteModelPath.lastIndexOf('.')
		// remove project path end suffix
		val relativeModelPath = if (includeFileName && suffixPos > 0) {
			absoluteModelPath.substring(absoluteProjectPath.length, suffixPos)
		}
		else {
			absoluteModelPath.substring(absoluteProjectPath.length)
		}
		
		relativeModelPath
			.replaceFirst("^(\\.|\\\\|\\/)*", '') // eliminate leading '/' or '\' or '.'
			.replaceAll("(\\\\|\\/)", '.') // replace all '/' or '\' by '.'
			.toLowerCase
	}
	
	def ModelElementContainer getPrimeReferencedContainer(IdentifiableElement it) {
		switch it {
			
			// DAD
			info.scce.dime.dad.dad.ProcessComponent: model
			info.scce.dime.dad.dad.LoginComponent: model
			info.scce.dime.dad.dad.FindLoginUserComponent: model
			info.scce.dime.dad.dad.DataComponent: model
			
			// GUI
			info.scce.dime.gui.gui.GUISIB: gui
			info.scce.dime.gui.gui.ProcessSIB: proMod as info.scce.dime.process.process.Process
			info.scce.dime.gui.gui.SecuritySIB: proMod as info.scce.dime.process.process.Process
			info.scce.dime.gui.gui.GuardSIB: process as info.scce.dime.process.process.Process
			
//			ComplexInputPort
//			ComplexOutputPort
//			ComplexVariable
//			PrimitiveAttribute
//			ComplexAttribute
//			PrimitiveListAttribute
//			ComplexListAttribute
//			GUIPlugin
//			ISSIB
//			FORSIB

			// Process
			info.scce.dime.process.process.GUISIB: gui
			info.scce.dime.process.process.ProcessSIB: proMod
			info.scce.dime.process.process.GuardedProcessSIB: proMod
			info.scce.dime.process.process.GuardProcessSIB: securityProcess
			
//			SearchSIB
//			CreateSIB
//			TransientCreateSIB
//			CreateUserSIB
//			ContainsSIB
//			ContainsJavaNativeSIB
//			IsOfTypeSIB
//			IterateSIB
//			IterateJavaNativeSIB
//			PutComplexToContextSIB
//			RetrieveOfTypeSIB
//			RetrieveCurrentUserSIB
//			RemoveFromListSIB
//			EnumSwitchSIB
//			RetrieveEnumLiteralSIB
//			SetAttributeValueSIB
//			UnsetAttributeValueSIB
//			AtomicSIB
//			ComplexInputPort
//			InputGeneric
//			ComplexAttributePort
//			PrimitiveAttributePort
//			JavaNativeInputPort
//			ProcessInputStatic
//			ComplexOutputPort
//			OutputGeneric
//			JavaNativeOutputPort
//			JavaNativeVariable
//			ComplexVariable
//			PrimitiveAttribute
//			ComplexAttribute
//			PrimitiveListAttribute
//			ComplexListAttribute
//			TypeVariable
//			TypeInput
//			NativeFrontendSIBReference
//			BasicProcessSIB
//			InteractableProcessSIB
//			InteractionProcessSIB
//			SecurityProcessSIB

			// Data
			info.scce.dime.data.data.ExtensionAttribute: process as info.scce.dime.process.process.Process
			
		}
	}
}
