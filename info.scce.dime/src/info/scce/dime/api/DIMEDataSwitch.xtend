package info.scce.dime.api

import info.scce.dime.data.data.util.DataSwitch
import info.scce.dime.data.helper.DataExtension

class DIMEDataSwitch<T> extends DataSwitch<T> {
	
	protected extension DataExtension = DataExtension.instance
	protected extension DIMEGraphModelExtension = new DIMEGraphModelExtension
}