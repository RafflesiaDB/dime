package info.scce.dime.search.helper;

import info.scce.dime.search.search.SearchInterface;

public class ModelHelper {
	public static int getParameterCount(SearchInterface cQI) {
		return cQI.getInputParameters().size()
				+ cQI.getOutputParameters().size();
	}
}
