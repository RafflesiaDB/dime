package info.scce.dime.search.checks;

import info.scce.dime.search.mcam.adapter.SearchAdapter;
import info.scce.dime.search.mcam.adapter.SearchId;
import info.scce.dime.search.search.DataEdge;
import info.scce.dime.search.search.Variable;
import info.scce.dime.checks.AbstractCheck;

public class VariableNotUsedCheck extends AbstractCheck<SearchId, SearchAdapter> {

	@Override
	public void doExecute(SearchAdapter adapter) {
		for (SearchId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Variable) {
				Variable var = (Variable) obj;
				if (var.getOutgoing(DataEdge.class).size() <= 0)
					addError(id, "not used");
			}
		}
	}
	
	@Override
	public void init() {}

}
