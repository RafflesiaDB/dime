package info.scce.dime.search.checks;

import java.util.ArrayList;

import info.scce.dime.search.mcam.adapter.SearchAdapter;
import info.scce.dime.search.mcam.adapter.SearchId;
import info.scce.dime.search.search.InputParameter;
import info.scce.dime.search.search.Variable;
import info.scce.dime.checks.AbstractCheck;

public class UniqueNamesCheck extends AbstractCheck<SearchId, SearchAdapter> {

	@Override
	public void doExecute(SearchAdapter adapter) {
		ArrayList<String> varNames = new ArrayList<>();
		ArrayList<String> inputNames = new ArrayList<>();
		
		for (SearchId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Variable) {
				Variable var = (Variable) obj;
				if (varNames.contains(var.getName()))
					addError(id, var.getName() + " not unique");
				varNames.add(var.getName());
			}
			if (obj instanceof InputParameter) {
				InputParameter ip = (InputParameter) obj;
				if (inputNames.contains(ip.getName()))
					addError(id, ip.getName() + " not unique");
				inputNames.add(ip.getName());
			}
		}
	}
	
	@Override
	public void init() {}

}
