package info.scce.dime.search.checks;

import graphmodel.Node;
import info.scce.dime.search.mcam.adapter.SearchAdapter;
import info.scce.dime.search.mcam.adapter.SearchId;
import info.scce.dime.search.search.CompareOperator;
import info.scce.dime.search.search.DataEdge;
import info.scce.dime.search.search.PrimitiveInputParameter;
import info.scce.dime.search.search.PrimitiveType;
import info.scce.dime.search.search.PrimitiveVariable;
import info.scce.dime.checks.AbstractCheck;

public class AssignmentTypesCheck extends AbstractCheck<SearchId, SearchAdapter> {

	@Override
	public void doExecute(SearchAdapter adapter) {
		for (SearchId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof DataEdge) {
				DataEdge dEdge = (DataEdge) obj;

				PrimitiveType srcType = getPrimitiveType(dEdge
						.getSourceElement());
				PrimitiveType tgtType = getPrimitiveType(dEdge
						.getTargetElement());

				if (srcType != null && tgtType != null) {
					if (!srcType.equals(tgtType)) {
						addError(id, "type mismatch! " + srcType + " != "
								+ tgtType);
					}
				}
			}
		}
	}
	
	@Override
	public void init() {}

	private PrimitiveType getPrimitiveType(Node node) {
		if (node instanceof PrimitiveInputParameter)
			return ((PrimitiveInputParameter) node).getDataType();
		if (node instanceof PrimitiveVariable)
			return ((PrimitiveVariable) node).getDataType();
		if (node instanceof CompareOperator)
			return ((CompareOperator) node).getOperand();
		return null;
	}
}
