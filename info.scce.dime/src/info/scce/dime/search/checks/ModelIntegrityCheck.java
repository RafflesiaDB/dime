package info.scce.dime.search.checks;

import info.scce.dime.checks.AbstractModelIntegrityCheck;
import info.scce.dime.search.mcam.adapter.SearchAdapter;
import info.scce.dime.search.mcam.adapter.SearchId;

public class ModelIntegrityCheck
	   extends AbstractModelIntegrityCheck<SearchId, SearchAdapter> {
}
