package info.scce.dime.search.hooks;

import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.search.helper.LayoutHelper;
import info.scce.dime.search.helper.ModelHelper;
import info.scce.dime.search.search.Parameter;
import info.scce.dime.search.search.SearchInterface;

public class ParameterPreDelete extends DIMEPreDeleteHook<Parameter>{

	@Override
	public void preDelete(Parameter cParameter) {
		if (cParameter.getContainer() instanceof SearchInterface) {
			SearchInterface cQI = (SearchInterface) cParameter.getContainer();
			LayoutHelper.resize(cQI, ModelHelper.getParameterCount(cQI)-1);
			LayoutHelper.layout(cQI, cParameter);
		}
	}

}
