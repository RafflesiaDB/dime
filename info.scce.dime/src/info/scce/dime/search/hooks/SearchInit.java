package info.scce.dime.search.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.search.search.Search;

public class SearchInit extends DIMEPostCreateHook<Search> {

	@Override
	public void postCreate(Search searchModel) {
		try {

			String fileName = searchModel.eResource().getURI().lastSegment();
			String fileExtension = searchModel.eResource().getURI()
					.fileExtension();
			String modelName = fileName.replace("." + fileExtension, "");

			searchModel.newDataContext(50, 50);
			searchModel.newSearchInterface(350, 50);
			searchModel.setModelName(modelName);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
