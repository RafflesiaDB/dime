package info.scce.dime.search.hooks;

import info.scce.dime.api.DIMEPostResizeHook;
import info.scce.dime.search.helper.LayoutHelper;
import info.scce.dime.search.search.SearchInterface;

public class SearchInterfacePostResize extends DIMEPostResizeHook<SearchInterface> {

	@Override
	public void postResize(SearchInterface cQI, int direction, int width,
			int height) {
		LayoutHelper.layout(cQI, null);
	}

}
