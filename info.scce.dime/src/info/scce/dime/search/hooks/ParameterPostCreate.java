package info.scce.dime.search.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.search.helper.LayoutHelper;
import info.scce.dime.search.helper.ModelHelper;
import info.scce.dime.search.search.Parameter;
import info.scce.dime.search.search.SearchInterface;

public class ParameterPostCreate extends DIMEPostCreateHook<Parameter>{
	
	@Override
	public void postCreate(Parameter parameter) {

		if (parameter.getContainer() instanceof SearchInterface) {
			SearchInterface cQI = (SearchInterface) parameter.getContainer();
			LayoutHelper.resize(cQI, ModelHelper.getParameterCount(cQI));
		}
		
	}

}
