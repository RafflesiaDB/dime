package info.scce.dime.search.hooks;

import org.eclipse.emf.ecore.EObject;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.search.helper.LayoutHelper;
import info.scce.dime.search.search.CompareOperator;
import info.scce.dime.search.search.SearchConditional;

public class SearchConditionalPostCreate extends DIMEPostCreateHook<SearchConditional>{

	@Override
	public void postCreate(SearchConditional conditional) {
		
		EObject eObj = conditional.getAttribute();
		if (eObj instanceof PrimitiveAttribute) {
			PrimitiveAttribute pa = (PrimitiveAttribute) eObj;
			PrimitiveType type = pa.getDataType();
			
			CompareOperator op;
			
			switch (type) {
			case INTEGER:
				op = conditional.newIntegerCompareOperator(LayoutHelper.PORT_X, LayoutHelper.CQ_Y);
				op.setOperand(info.scce.dime.search.search.PrimitiveType.INTEGER);
				break;
				
			case REAL:
				op = conditional.newRealCompareOperator(LayoutHelper.PORT_X, LayoutHelper.CQ_Y);
				op.setOperand(info.scce.dime.search.search.PrimitiveType.REAL);
				break;
				
			case TEXT:
				op = conditional.newTextCompareOperator(LayoutHelper.PORT_X, LayoutHelper.CQ_Y);
				op.setOperand(info.scce.dime.search.search.PrimitiveType.TEXT);
				break;
				
			case BOOLEAN:
				op = conditional.newBooleanCompareOperator(LayoutHelper.PORT_X, LayoutHelper.CQ_Y);
				op.setOperand(info.scce.dime.search.search.PrimitiveType.BOOLEAN);
				break;
				
			case TIMESTAMP:
				op = conditional.newTimestampCompareOperator(LayoutHelper.PORT_X, LayoutHelper.CQ_Y);
				op.setOperand(info.scce.dime.search.search.PrimitiveType.TIMESTAMP);
				break;

			default:
				break;
			}
			
		}
		
	}

}
