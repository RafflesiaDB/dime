package info.scce.dime.data.vp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.scce.dime.api.DIMEValuesProvider;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.Type; 

public class PrimitiveSuperAttrPossibleValuesProvider extends DIMEValuesProvider<PrimitiveAttribute, PrimitiveAttribute>{

	@Override
	public Map<PrimitiveAttribute,String> getPossibleValues(PrimitiveAttribute editedAttribute) {

		final Type typeOfEditedAttribute = (Type) editedAttribute.getContainer();
		List<Attribute> allAttributes = _dataExtension.getInheritedAttributes(typeOfEditedAttribute);
		
		Map<PrimitiveAttribute,String> collect = new HashMap<PrimitiveAttribute, String>(); 
			// start with all editedAttribute's type's attributes
			allAttributes.stream()
			//defined in the same type as the editedAttribute
			.filter(a -> !typeOfEditedAttribute.getAttributes().contains(a))
			// remove all attributes that are not primitive
			.filter(a->a instanceof PrimitiveAttribute)
			// remove all attributes that are of the wrong primtive type
			.filter(a -> ((PrimitiveAttribute)a).getPrimitiveAttributeView().getDataType().equals(editedAttribute.getPrimitiveAttributeView().getDataType()))
			.forEach(n->collect.put((PrimitiveAttribute) n, n.getAttributeView().getName()));
		return collect;
	
	}

}
