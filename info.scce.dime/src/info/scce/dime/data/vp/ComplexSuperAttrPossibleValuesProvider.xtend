package info.scce.dime.data.vp

import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Type
import info.scce.dime.api.DIMEValuesProvider

import static java.util.stream.Collectors.toMap

import static extension org.jooq.lambda.Seq.*

class ComplexSuperAttrPossibleValuesProvider extends DIMEValuesProvider<ComplexAttribute, ComplexAttribute> {
	
	override getPossibleValues(ComplexAttribute editedAttribute) {
		
		val typeOfEditedAttribute = (editedAttribute.getContainer() as Type)
		
		// start with all editedAttribute's type's attributes ...
		val it = typeOfEditedAttribute.inheritedAttributeSeq.seq.
			// ... exclude attributes defined in the same type as the editedAttribute ...
			removeAll(typeOfEditedAttribute.attributes.seq).
			// ... exclude all attributes that are not complex ...
			filter(ComplexAttribute).
			// ... exclude all attributes that are of the wrong type ...
			filter[dataType == editedAttribute.dataType]
			// ... and duplicate them for map creation
			.duplicate
		v1.zip(v2.map[name]).collect(toMap([v1], [v2]))
	}
}
