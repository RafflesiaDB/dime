package info.scce.dime.data.hooks

import info.scce.dime.api.DIMEPreDeleteHook
import info.scce.dime.data.data.Type
import info.scce.dime.data.data.Association

class TypePreDelete extends DIMEPreDeleteHook<Type> {
	
	override preDelete(Type cType) {
		(cType.getOutgoing(Association) + cType.getIncoming(Association)).forEach[delete]
	}
	
}