package info.scce.dime.data.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.data.Type
import info.scce.dime.util.DataID

class TypePostCreate extends DIMEPostCreateHook<Type> {
	
	override postCreate(Type type) {
		assertUniqueDataID(type)
	}
	
	def assertUniqueDataID(Type type) {
		val model = type.rootElement
		val types = 
			model.find(Type)
				.drop(ReferencedType)
				.drop(ReferencedUserType)
				.drop(ReferencedEnumType)
				.filter[it != type]
				.toSet
		
		DataID.assertUniqueDataID(type, types)
	}
}