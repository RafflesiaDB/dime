package info.scce.dime.data.hooks;

import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.TypeLayoutUtils;

public class UserTypePostCreate extends TypePostCreate {
	
	public static String attrUsernameName = "username";
	public static String attrPasswordName = "password";

	@Override
	public void postCreate(Type ut) {
		super.postCreate(ut);
		
		PrimitiveAttribute cAttrName = ut.newPrimitiveAttribute(1, 1);
		cAttrName.getPrimitiveAttributeView().setName(attrUsernameName);
		cAttrName.getPrimitiveAttributeView().setDataType(PrimitiveType.TEXT);
		cAttrName.getPrimitiveAttributeView().setIsList(false);
		
		PrimitiveAttribute cAttrPassword = ut.newPrimitiveAttribute(1, 1);
		cAttrPassword.getPrimitiveAttributeView().setName(attrPasswordName);
		cAttrPassword.getPrimitiveAttributeView().setDataType(PrimitiveType.TEXT);
		cAttrPassword.getPrimitiveAttributeView().setIsList(false);
		
		TypeLayoutUtils.resizeAndLayout((Type) ut);
	}

}
