package info.scce.dime.data.hooks

import info.scce.dime.api.DIMEPostCreateHook
import info.scce.dime.data.actions.UpdateExtensionAttribute
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.helper.TypeLayoutUtils
import info.scce.dime.process.process.Process
import info.scce.dime.util.DataID

class AttributePostCreate extends DIMEPostCreateHook<Attribute> {
	
	override postCreate(Attribute attribute) {
		
		assertUniqueDataID(attribute)
		
		if (attribute instanceof ExtensionAttribute) {
			attribute.name = (attribute.process as Process).modelName
			val uea = new UpdateExtensionAttribute
			if (uea.canExecute(attribute)) {
				uea.execute(attribute)
			}
		}
		
		TypeLayoutUtils.resizeAndLayout(attribute.container)
		sortOrder(attribute)
	}
	
	def assertUniqueDataID(Attribute attr) {
		val model = attr.rootElement
		val attrs = 
			model.find(Attribute)
				.filter[it.findParents(ReferencedType).isEmpty]
				.filter[it.findParents(ReferencedUserType).isEmpty]
				.filter[it.findParents(ReferencedEnumType).isEmpty]
				.filter[it != attr]
				.toSet
		
		DataID.assertUniqueDataID(attr, attrs)
	}

	def sortOrder(Attribute attribute) {
		val data = attribute.rootElement
		for (type : data.types) {
			val sortedAttrs = type.attributes.sortBy[it.name]
			var y = 32
			for (attr : sortedAttrs) {
				attr.moveTo(attr.container, attr.x, y)
				y += 18
			}
		}
	}
}
