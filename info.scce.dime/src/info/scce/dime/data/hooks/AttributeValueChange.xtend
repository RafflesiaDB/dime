package info.scce.dime.data.hooks

import de.jabc.cinco.meta.runtime.action.CincoPostAttributeChangeHook
import info.scce.dime.data.data.Attribute
import org.eclipse.emf.ecore.EStructuralFeature
import java.util.LinkedList
import java.util.Comparator
import org.eclipse.emf.ecore.EAttribute

class AttributeValueChange extends CincoPostAttributeChangeHook<Attribute>{
	
	override canHandleChange(Attribute arg0, EStructuralFeature arg1) {
		arg1 instanceof EAttribute
	}
	
	override handleChange(Attribute attribute, EStructuralFeature arg1) {
		if(!(arg1 instanceof EAttribute)) {
			return
		}
		var type = attribute.container
		if(type != null){
			var allAttributes = type.getAttributes();
			var myAttr = new LinkedList<Attribute>();
			myAttr.addAll(allAttributes);
			myAttr.sort(new Comparator<Attribute>() {

				override compare(Attribute o1, Attribute o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			//new positions
			var offset = 32;
			for( attr : myAttr){
				if(attr.y!=offset && attr.y > 1) {
					//attr.moveTo(attr.container,attr.x, offset);					
				}
				offset+= 18;
			}
				
			
		}
	}
	
}