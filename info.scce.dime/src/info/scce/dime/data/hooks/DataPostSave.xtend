package info.scce.dime.data.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostSaveHook
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Data
import java.util.Comparator
import java.util.LinkedList

class DataPostSave extends CincoPostSaveHook<Data>{
	
	override postSave(Data data) {
		for(type: data.types){
			var allAttributes = type.attributes
			var myAttr = new LinkedList
			myAttr.addAll(allAttributes.toList)
			myAttr.sort(new Comparator<Attribute>{
				
				override compare(Attribute o1, Attribute o2) {
					return o1.name.compareTo(o2.name)
				}
				
			})
			//new psotions
			var offset = 32;
			for(attr : myAttr){
				attr.moveTo(attr.container, attr.x, offset)
				offset+= 18
			}
			
		}
	}
	
}