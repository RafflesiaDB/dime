package info.scce.dime.data.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.BidirectionalAssociation;
import info.scce.dime.data.data.BidirectionalAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.TypeLayoutUtils;

public class BidirectionalAssociationInitialize extends DIMEPostCreateHook<BidirectionalAssociation>{

	@Override
	public void postCreate(BidirectionalAssociation association) {

//		CData rootModel = DataWrapper.wrapGraphModel(association.getRootElement(), getDiagram());
//		CBidirectionalAssociation cAssociation = rootModel.findCBidirectionalAssociation(association);

		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		Type sourceType = (Type) association.getSourceElement();
		BidirectionalAttribute cSourceAttribute = sourceType.newBidirectionalAttribute(1, 1);
		TypeLayoutUtils.resizeAndLayout(sourceType);
		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		cSourceAttribute.setDataType((Type) association.getTargetElement());
		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		final String targetTypeName = ((Type)association.getTargetElement()).getName();
		cSourceAttribute.setName(targetTypeName.substring(0, 1).toLowerCase() + targetTypeName.substring(1));

		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		Type targetType = (Type) association.getTargetElement();
		BidirectionalAttribute cTargetAttribute = targetType.newBidirectionalAttribute(1, 1);
		TypeLayoutUtils.resizeAndLayout(targetType);
		//TODO: remove cast once incoming/outgoing with inheritance has been fixed
		cTargetAttribute.setDataType((Type) association.getSourceElement());
		final String sourceTypeName = ((Type)association.getSourceElement()).getName();
		cTargetAttribute.setName(sourceTypeName.substring(0, 1).toLowerCase() + sourceTypeName.substring(1));

		// TODO: repair workaround once #15424 is resolved
		BidirectionalAttribute sourceAttribute = (BidirectionalAttribute)cSourceAttribute;
		BidirectionalAttribute targetAttribute = (BidirectionalAttribute)cTargetAttribute;

		cSourceAttribute.setOppositeAttribute(targetAttribute);
		cTargetAttribute.setOppositeAttribute(sourceAttribute);

		association.setTargetAttr(sourceAttribute);
		association.setSourceAttr(targetAttribute);

	}

}
