package info.scce.dime.data.hooks;

import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.data.data.Association;
import info.scce.dime.data.data.BidirectionalAssociation;

public class AssociationPreDelete extends DIMEPreDeleteHook<Association>{

	@Override
	public void preDelete(Association cAss) {
		cAss.getSourceAttr().delete();
		if (cAss instanceof BidirectionalAssociation) {
			((BidirectionalAssociation) cAss).getTargetAttr().delete();
		}
	}

}
