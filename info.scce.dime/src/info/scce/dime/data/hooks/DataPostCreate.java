package info.scce.dime.data.hooks;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Data;

public class DataPostCreate extends DIMEPostCreateHook<Data>{

	@Override
	public void postCreate(Data model) {
		String fileName = model.eResource().getURI().lastSegment();
		String fileExtension = model.eResource().getURI().fileExtension();
		String modelName = fileName.replace("." + fileExtension, "");
		model.setModelName(modelName);
	}

}
