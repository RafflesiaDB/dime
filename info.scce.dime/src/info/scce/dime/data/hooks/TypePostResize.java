package info.scce.dime.data.hooks;

import info.scce.dime.api.DIMEPostResizeHook;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.process.helper.LayoutConstants;

public class TypePostResize extends DIMEPostResizeHook<Type>{

	@Override
	public void postResize(Type cModelElement, int direction, int width, int height) {
		for (Attribute a : cModelElement.getAttributes()) {
			a.resize(width - LayoutConstants.ATTR_X*2, a.getHeight());
		}
		
	}

}
