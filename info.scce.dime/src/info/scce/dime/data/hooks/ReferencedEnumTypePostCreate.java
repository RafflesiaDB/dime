package info.scce.dime.data.hooks;

import java.util.List;

import info.scce.dime.api.DIMEPostCreateHook;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.BidirectionalAttribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.EnumLiteral;
import info.scce.dime.data.data.EnumType;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.ReferencedBidirectionalAttribute;
import info.scce.dime.data.data.ReferencedComplexAttribute;
import info.scce.dime.data.data.ReferencedEnumType;
import info.scce.dime.data.data.ReferencedPrimitiveAttribute;
import info.scce.dime.data.data.ReferencedUserAttribute;
import info.scce.dime.data.data.UserAttribute;
import info.scce.dime.data.helper.TypeLayoutUtils;

public class ReferencedEnumTypePostCreate extends DIMEPostCreateHook<ReferencedEnumType>{

	@Override
	public void postCreate(ReferencedEnumType referencedType) {
//			CData rootModel = DataWrapper.wrapGraphModel(referencedType.getRootElement(), getDiagram());
//			CType cReferencedType = rootModel.findCType(referencedType);
			
			EnumType lc = referencedType.getReferencedType();
			
			List<Attribute> attributes = _dataExtension.getInheritedAttributes(lc); 
			
			referencedType.getReferencedEnumTypeView().setName(lc.getEnumTypeView().getName());
			
			for (Attribute lcAttribute : attributes) {
				
				if (lcAttribute instanceof UserAttribute) {
					ReferencedUserAttribute refAttr = referencedType.newReferencedUserAttribute(lcAttribute, 1, 1);
					refAttr.getAttributeView().setName(lcAttribute.getAttributeView().getName());
				}
				else if (lcAttribute instanceof BidirectionalAttribute) {
					ReferencedBidirectionalAttribute refAttr = referencedType.newReferencedBidirectionalAttribute(lcAttribute, 1, 1);
					refAttr.getAttributeView().setName(lcAttribute.getAttributeView().getName());
				}
				else if (lcAttribute instanceof ComplexAttribute) {
					ReferencedComplexAttribute refAttr = referencedType.newReferencedComplexAttribute(lcAttribute, 1, 1);
					refAttr.getAttributeView().setName(lcAttribute.getAttributeView().getName());
				}
				else if (lcAttribute instanceof PrimitiveAttribute) {
					ReferencedPrimitiveAttribute refAttr = referencedType.newReferencedPrimitiveAttribute(lcAttribute, 1, 1);
					refAttr.getAttributeView().setName(lcAttribute.getAttributeView().getName());
				}
				else if (lcAttribute instanceof EnumLiteral) {
					// EnumLiterals are not displayed in submodels
				}
				else {
					throw new IllegalStateException("Reached else/default case in exhaustive if/switch. Please fix the code.");
				}
				
			}
			TypeLayoutUtils.resizeAndLayout(referencedType);
	}

}
