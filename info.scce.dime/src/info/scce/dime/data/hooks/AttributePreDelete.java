package info.scce.dime.data.hooks;

import info.scce.dime.api.DIMEPreDeleteHook;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.helper.TypeLayoutUtils;

public class AttributePreDelete extends DIMEPreDeleteHook<Attribute>{

	@Override
	public void preDelete(Attribute cAttribute) {
		Type sourceType = (Type) cAttribute.getContainer();
		TypeLayoutUtils.resizeAndLayoutBeforeDelete(sourceType, cAttribute);
	}

}
