package info.scce.dime.data.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import info.scce.dime.data.data.Attribute
import java.util.Comparator
import org.eclipse.emf.common.util.EList
import java.util.LinkedList
import info.scce.dime.data.data.Data

class AttributePostDelete extends CincoPostDeleteHook<Attribute> {
	
	override getPostDeleteFunction(Attribute attribute) {
		val data = attribute.getRootElement()
		return [data.sortOrder]
	}
	
	def sortOrder(Data data) {
		for( type: data.getTypes()){
			var EList<Attribute> allAttributes = type.getAttributes();
			var LinkedList<Attribute> myAttr = new LinkedList<Attribute>();
			myAttr.addAll(allAttributes);
			myAttr.sort(new Comparator<Attribute>() {
				
				override compare(Attribute o1, Attribute o2) {
					return o1.getName().compareTo(o2.getName());
				}

				
			});
			//new psotions
			var offset = 32;
			for(Attribute attr : myAttr){
				attr.moveTo(attr.getContainer(), attr.getX(), offset);
				offset+= 18;
			}
		}
	}
}