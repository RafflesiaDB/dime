package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.Association

class HideAssociation<T extends Association> extends DIMECustomAction<T> {
	
	override getName() {
		"Hide Association"
	}

	override canExecute(Association association) {
		var srcAttr = association.sourceAttr
		srcAttr.isIsList 
		!association.isHidden
	}

	override void execute(Association association) {
		association.isHidden = true
	}
}
