package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.process.process.Process
import info.scce.dime.data.data.ExtensionAttribute

class OpenExtensionAttribute extends DIMECustomAction<ExtensionAttribute> {
	
	override getName() {
		"Open Extension Attribute"
	}

	override canExecute(ExtensionAttribute attr) {
		attr.process !== null
	}

	override void execute(ExtensionAttribute attr) {
		val process = attr.process as Process
		process.openEditor
	}
}
