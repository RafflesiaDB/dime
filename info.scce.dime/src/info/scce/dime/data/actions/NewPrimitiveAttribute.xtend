package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.Type
import info.scce.dime.data.helper.TypeLayoutUtils
import info.scce.dime.data.data.EnumType
import java.util.LinkedList
import info.scce.dime.data.data.Attribute
import java.util.Comparator

class NewPrimitiveAttribute extends DIMECustomAction<Type> {
	
	override getName() {
		"New Primitive Attribute"
	}

	// TODO beautify after auto conversion to Xtend class
	override void execute(Type type) {
		var int currentAttrAmount = type.getAttributes().size()
		// TODO: Due to not existing inheritance of annotations, adding a new literal on doubleClick 
		// in EnumTypes has to be implemented here... 
		if (type instanceof EnumType) {
			type.newEnumLiteral(1, 1).setName('''literal«(currentAttrAmount + 1)»'''.toString)
		} else {
			type.newPrimitiveAttribute(1, 1).setName('''attr«(currentAttrAmount + 1)»'''.toString)
		}
		TypeLayoutUtils::resizeAndLayout(type)
		sortOrder(type)
	}
	
	def sortOrder(Type type) {
		var data = type.rootElement
		for (myType : data.getTypes()) {
			var allAttributes = type.getAttributes();
			var myAttr = new LinkedList<Attribute>();
			myAttr.addAll(allAttributes);
			myAttr.sort(new Comparator<Attribute>() {

				override compare(Attribute o1, Attribute o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			// new psotions
			var offset = 32;
			for (attr : myAttr) {
				attr.moveTo(attr.container, attr.x, offset);
				offset += 18;
			}

		}
	}
	
}
