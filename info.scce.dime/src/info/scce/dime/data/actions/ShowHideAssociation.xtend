package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.Association
import info.scce.dime.data.data.BidirectionalAssociation
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.Type
import java.util.ArrayList

class ShowHideAssociation<T extends ComplexAttribute> extends DIMECustomAction<T> {
	
	override getName() {
		"Toggle Show/Hide Association"
	}
	
	override canExecute(ComplexAttribute ca) {
		ca.correspondingAssociation !== null
	}

	// TODO beautify after auto conversion to Xtend class
	
	override void execute(ComplexAttribute ca) {
		println(ca.isIsList)
		var Association asso = getCorrespondingAssociation(ca)
		asso.setIsHidden(!asso.isIsHidden())
		
	}

	def private getCorrespondingAssociation(ComplexAttribute ca) {
		val type = (ca.getContainer() as Type)
		val allAssociations = new ArrayList<Association>(type.getIncoming(typeof(Association)))
		allAssociations.addAll(type.getOutgoing(typeof(Association)))
		for (Association asso : allAssociations) {
			if(((asso as Association)).getSourceAttr().equals(ca))
				return (asso as Association)
			if(asso instanceof BidirectionalAssociation)
				if(((asso as BidirectionalAssociation)).getTargetAttr().equals(ca))
					return (asso as Association)
		}
		return null
	}
}
