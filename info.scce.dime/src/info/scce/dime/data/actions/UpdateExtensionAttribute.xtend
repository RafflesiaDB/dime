package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.ExtensionAttribute
import info.scce.dime.process.process.ComplexInputPort
import info.scce.dime.process.process.PrimitiveInputPort
import info.scce.dime.process.process.InputStatic
import info.scce.dime.process.process.TextInputStatic
import info.scce.dime.process.process.IntegerInputStatic
import info.scce.dime.process.process.RealInputStatic
import info.scce.dime.process.process.TimestampInputStatic
import info.scce.dime.process.process.BooleanInputStatic

class UpdateExtensionAttribute extends DIMECustomAction<ExtensionAttribute> {
	
	override getName() {
		"Update Extension Attribute"
	}

	override canExecute(ExtensionAttribute attr) {
		attr.process !== null &&
		!(attr.process as info.scce.dime.process.process.Process).endSIBs.empty &&
		!(attr.process as info.scce.dime.process.process.Process).endSIBs.get(0).inputs.empty
	}

	override void execute(ExtensionAttribute attr) {
		val process = attr.process as info.scce.dime.process.process.Process
		val port = process.endSIBs.get(0).inputs.get(0)
		switch(port) {
			ComplexInputPort: {
				attr.isList = port.isIsList
				attr.dataType = port.dataType.name
			}
			PrimitiveInputPort: {
				attr.isList = port.isIsList
				attr.dataType = port.dataType.literal
			}
			InputStatic: {
				attr.isList = false
				attr.dataType = switch(port) {
					TextInputStatic: "Text"
					IntegerInputStatic: "Integer"
					RealInputStatic: "Real"
					TimestampInputStatic: "Timestamp"
					BooleanInputStatic: "Boolean"
				}
			}
		}
	}
}
