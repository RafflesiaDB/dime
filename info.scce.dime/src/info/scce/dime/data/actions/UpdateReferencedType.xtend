package info.scce.dime.data.actions

import info.scce.dime.api.DIMECustomAction
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.BidirectionalAttribute
import info.scce.dime.data.data.ComplexAttribute
import info.scce.dime.data.data.InheritorType
import info.scce.dime.data.data.PrimitiveAttribute
import info.scce.dime.data.data.ReferencedBidirectionalAttribute
import info.scce.dime.data.data.ReferencedComplexAttribute
import info.scce.dime.data.data.ReferencedPrimitiveAttribute
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserAttribute
import info.scce.dime.data.data.UserAttribute
import java.util.ArrayList
import java.util.HashMap
import info.scce.dime.data.data.Data
import org.eclipse.emf.ecore.util.EcoreUtil

class UpdateReferencedType extends DIMECustomAction<ReferencedType> {
	
	var Data data

	override getName() {
		"Update ReferencedType"
	}

	override canExecute(ReferencedType type) {
		type.referencedType instanceof InheritorType
	}

	// TODO beautify after auto conversion to Xtend class
	override execute(ReferencedType rType) {
		var InheritorType iType = rType.getReferencedType()
		// update name
		if(!rType.getName().equals(iType.getName())) rType.setName(iType.getName())
		// identify attributes state
		var ArrayList<Attribute> rAttributes = new ArrayList<Attribute>(rType.getAttributes())
		var ArrayList<Attribute> iAttribtues = new ArrayList<Attribute>(_dataExtension.getInheritedAttributes(iType))
		var ArrayList<Attribute> toDeleteAttr = new ArrayList<Attribute>(rAttributes)
		var HashMap<Attribute, Attribute> toUpdateAttr = new HashMap<Attribute, Attribute>()
		var ArrayList<Attribute> toAddAttr = new ArrayList<Attribute>()
		for (Attribute iAttr : iAttribtues) {
			var Attribute foundRAttr = null
			for (Attribute rAttr : rAttributes) {
				if (rAttr instanceof ReferencedPrimitiveAttribute) {
					var PrimitiveAttribute refAttr = ((rAttr as ReferencedPrimitiveAttribute)).getReferencedAttribute()
					if (refAttr != null && refAttr.getId().equals(iAttr.getId())) {
						foundRAttr = rAttr
						toDeleteAttr.remove(rAttr)
					}
				} else if (rAttr instanceof ReferencedUserAttribute) {
					var UserAttribute refAttr = ((rAttr as ReferencedUserAttribute)).getReferencedAttribute()
					if (refAttr != null && refAttr.getId().equals(iAttr.getId())) {
						foundRAttr = rAttr
						toDeleteAttr.remove(rAttr)
					}
				} else if (rAttr instanceof ReferencedBidirectionalAttribute) {
					var BidirectionalAttribute refAttr = ((rAttr as ReferencedBidirectionalAttribute)).
						getReferencedAttribute()
					if (refAttr != null && refAttr.getId().equals(iAttr.getId())) {
						foundRAttr = rAttr
						toDeleteAttr.remove(rAttr)
					}
				} else if (rAttr instanceof ReferencedComplexAttribute) {
					var ComplexAttribute refAttr = ((rAttr as ReferencedComplexAttribute)).getReferencedAttribute()
					if (refAttr != null && refAttr.getId().equals(iAttr.getId())) {
						foundRAttr = rAttr
						toDeleteAttr.remove(rAttr)
					}
				} else {
					throw new IllegalStateException("missing implementation for referencedAttribute")
				}
			}
			if(foundRAttr !== null) toUpdateAttr.put(iAttr, foundRAttr) else toAddAttr.add(iAttr)
		}
		// delete attributes
		for (Attribute attr : toDeleteAttr) {
			attr.delete()
		}
		// update attributes
		for (Attribute iAttr : toUpdateAttr.keySet()) {
			updateAttribute(rType, iAttr, toUpdateAttr.get(iAttr))
		}
		// add attributes
		for (Attribute iAttr : toAddAttr) {
			addAttribute(rType, iAttr)
		}
	}

	def private void addAttribute(ReferencedType rType, Attribute iAttr) {
		if (iAttr instanceof PrimitiveAttribute) {
			var PrimitiveAttribute origAttr = (iAttr as PrimitiveAttribute)
			var ReferencedPrimitiveAttribute newAttr = rType.newReferencedPrimitiveAttribute(origAttr, 0, 0)
			newAttr.setName(origAttr.getName())
			newAttr.setDataType(origAttr.getDataType())
			newAttr.setIsList(origAttr.isIsList())
		} else if (iAttr instanceof UserAttribute) {
			var UserAttribute origAttr = (iAttr as UserAttribute)
			var ReferencedUserAttribute newAttr = rType.newReferencedUserAttribute(origAttr, 0, 0)
			newAttr.setName(origAttr.getName())
			newAttr.setDataType(origAttr.getDataType())
			newAttr.setIsList(origAttr.isIsList())
		} else if (iAttr instanceof BidirectionalAttribute) {
			var BidirectionalAttribute origAttr = (iAttr as BidirectionalAttribute)
			var ReferencedBidirectionalAttribute newCAttr = rType.newReferencedBidirectionalAttribute(origAttr, 0, 0)
			newCAttr.setName(origAttr.getName())
			newCAttr.setDataType(origAttr.getDataType())
			newCAttr.setIsList(origAttr.isIsList())
		} else if (iAttr instanceof ComplexAttribute) {
			var ComplexAttribute origAttr = (iAttr as ComplexAttribute)
			var ReferencedComplexAttribute newCAttr = rType.newReferencedComplexAttribute(origAttr, 0, 0)
			newCAttr.setName(origAttr.getName())
			newCAttr.setDataType(origAttr.getDataType())
			newCAttr.setIsList(origAttr.isIsList()) 
		} else {
			throw new IllegalStateException("missing implementation for referencedAttribute")
		}
	}

	def private void updateAttribute(ReferencedType rType, Attribute iAttr, Attribute rAttr) {
		var Attribute cOldRAttribute = rAttr
		var Attribute cNewAttribute = null
		var String oldRAttrId = rAttr.getId()
		var int oldX = cOldRAttribute.getX()
		var int oldY = cOldRAttribute.getY()
		var int oldHeight = cOldRAttribute.getHeight()
		var int oldWidth = cOldRAttribute.getWidth()
		if (iAttr instanceof PrimitiveAttribute) {
			var PrimitiveAttribute origAttr = (iAttr as PrimitiveAttribute)
			var ReferencedPrimitiveAttribute newCAttr = rType.
				newReferencedPrimitiveAttribute(origAttr, oldX, oldY, oldWidth, oldHeight)
			EcoreUtil.setID(newCAttr, oldRAttrId);
			newCAttr.setName(origAttr.getName())
			newCAttr.setDataType(origAttr.getDataType())
			newCAttr.setIsList(origAttr.isIsList())
			cNewAttribute = newCAttr
		} else if (iAttr instanceof UserAttribute) {
			var UserAttribute origAttr = (iAttr as UserAttribute)
			var ReferencedUserAttribute newCAttr = rType.newReferencedUserAttribute(origAttr, oldX, oldY, oldWidth,
				oldHeight)
			EcoreUtil.setID(newCAttr, oldRAttrId);
			newCAttr.setName(origAttr.getName())
			newCAttr.setDataType(origAttr.getDataType())
			newCAttr.setIsList(origAttr.isIsList())
			cNewAttribute = newCAttr
		} else if (iAttr instanceof BidirectionalAttribute) {
			var BidirectionalAttribute origAttr = (iAttr as BidirectionalAttribute)
			var ReferencedBidirectionalAttribute newCAttr = rType.
				newReferencedBidirectionalAttribute(origAttr, oldX, oldY, oldWidth, oldHeight)
			EcoreUtil.setID(newCAttr, oldRAttrId);
			newCAttr.setName(origAttr.getName())
			newCAttr.setDataType(origAttr.getDataType())
			newCAttr.setIsList(origAttr.isIsList())
			cNewAttribute = newCAttr
		} else if (iAttr instanceof ComplexAttribute) {
			var ComplexAttribute origAttr = (iAttr as ComplexAttribute)
			var ReferencedComplexAttribute newCAttr = rType.newReferencedComplexAttribute(origAttr, oldX, oldY,
				oldWidth, oldHeight)
			EcoreUtil.setID(newCAttr, oldRAttrId);
			newCAttr.setName(origAttr.getName())
			newCAttr.setDataType(origAttr.getDataType())
			newCAttr.setIsList(origAttr.isIsList())
			cNewAttribute = newCAttr
		} else {
			throw new IllegalStateException("missing implementation for referencedAttribute")
		}
		cOldRAttribute.delete()
		// FIXME: Setting id in new api not possible. ID is now a parameter in new... Method
//		cNewAttribute.setId(oldRAttrId)
	}
}
