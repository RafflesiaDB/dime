package info.scce.dime.data.checks;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import info.scce.dime.api.DIMECheck;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.BidirectionalAttribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.InheritorType;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.ReferencedBidirectionalAttribute;
import info.scce.dime.data.data.ReferencedComplexAttribute;
import info.scce.dime.data.data.ReferencedPrimitiveAttribute;
import info.scce.dime.data.data.ReferencedType;
import info.scce.dime.data.data.ReferencedUserAttribute;
import info.scce.dime.data.data.UserAttribute;
import info.scce.dime.data.mcam.adapter.DataAdapter;
import info.scce.dime.data.mcam.adapter.DataId;

public class ReferencedTypesCheck extends DIMECheck<DataId, DataAdapter> {

	private DataAdapter adapter;

	@Override
	public void doExecute(DataAdapter adapter) {
		this.adapter = adapter;
		for (DataId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof ReferencedType) {
				ReferencedType rType = (ReferencedType) obj;
				EObject eObj = rType.getReferencedType();

				if (eObj instanceof InheritorType == false) {
					addError(id, "Reference is not a correct type or null");
				} else {
					checkReferencedType(id, rType, (InheritorType) eObj);
				}
			}
		}

	}

	private void checkReferencedType(DataId id, ReferencedType rType,
			InheritorType iType) {
		List<Attribute> attributes = new ArrayList<Attribute>(
				rType.getAttributes());
		List<Attribute> brokenAttributes = getBrokenAttributes(rType);

		if (!rType.getName().equals(iType.getName()))
			addError(id, "Type name differs, should be '" + iType.getName() + "'");

		for (Attribute iAttr : _dataExtension.getInheritedAttributes(iType)) {
			int count = 0;
			Attribute oAttr = null;
			for (Attribute rAttr : rType.getAttributes()) {
				if (brokenAttributes.contains(rAttr))
					continue;

				if (rAttr instanceof ReferencedPrimitiveAttribute) {
					PrimitiveAttribute refAttr = ((ReferencedPrimitiveAttribute) rAttr)
							.getReferencedAttribute();
					if (refAttr.getId().equals(iAttr.getId())) {
						count++;
						oAttr = rAttr;
					}
				}
				if (rAttr instanceof ReferencedComplexAttribute) {
					ComplexAttribute refAttr = ((ReferencedComplexAttribute) rAttr)
							.getReferencedAttribute();
					if (refAttr.getId().equals(iAttr.getId())) {
						count++;
						oAttr = rAttr;
					}
				}
				if (rAttr instanceof ReferencedBidirectionalAttribute) {
					BidirectionalAttribute refAttr = ((ReferencedBidirectionalAttribute) rAttr)
							.getReferencedAttribute();
					if (refAttr.getId().equals(iAttr.getId())) {
						count++;
						oAttr = rAttr;
					}
				}
				if (rAttr instanceof ReferencedUserAttribute) {
					UserAttribute refAttr = ((ReferencedUserAttribute) rAttr)
							.getReferencedAttribute();
					if (refAttr.getId().equals(iAttr.getId())) {
						count++;
						oAttr = rAttr;
					}
				}
			}
			if (count > 1) {
				addError(id, "Attribute '" + iAttr.getName() + "' exists " + count
						+ " times");
			} else if (count <= 0) {
				addError(id, "Attribute '" + iAttr.getName() + "' not found");
			} else {
				compareAttributes(adapter, oAttr, iAttr);
				attributes.remove(oAttr);
			}
		}
		for (Attribute attr : attributes) {
			addError(adapter.getIdByString(attr.getId()), "Reference for '" + attr.getName() + "' not found");
		}
	}

	private void compareAttributes(DataAdapter adapter, Attribute attr,
			Attribute rAttr) {
		
		DataId id = adapter.getIdByString(attr.getId());
		
		if (!attr.getName().equals(rAttr.getName())) {
			addError(id, "Name is incorrect, should be '" + rAttr.getName()
					+ "'");
		}
//		if (attr.isIsList() != rAttr.isIsList()) {
//			addError(id, "isList options differs, should be " + rAttr.isIsList());
//		}

		if (attr instanceof ReferencedPrimitiveAttribute
				&& rAttr instanceof PrimitiveAttribute) {
			ReferencedPrimitiveAttribute pAttr = (ReferencedPrimitiveAttribute) attr;
			PrimitiveAttribute pRAttr = (PrimitiveAttribute) rAttr;
			if (!pAttr.getReferencedAttribute().getDataType().equals(pRAttr.getDataType())) {
				addError(id, "PrimitiveType differs, should be '"
						+ pRAttr.getDataType() + "'");
			}
			if (pAttr.getReferencedAttribute().isIsList() != pRAttr.isIsList()) {
				addError(id, "isList options differs, should be " + rAttr.isIsList());
			}

		} else if (attr instanceof ReferencedComplexAttribute
				&& rAttr instanceof ComplexAttribute) {
			ReferencedComplexAttribute cAttr = (ReferencedComplexAttribute) attr;
			ComplexAttribute cRAttr = (ComplexAttribute) rAttr;
			if (!cRAttr.getDataType().equals(cAttr.getReferencedAttribute().getDataType())) {
				addError(id, "Type differs, should be '"
						+ cRAttr.getDataType().getName() + "'");
			}
			if (cRAttr.isIsList() != cAttr.getReferencedAttribute().isIsList()) {
				addError(id, "isList options differs, should be " + rAttr.isIsList());
			}

		} else if (attr instanceof ReferencedUserAttribute
				&& rAttr instanceof UserAttribute) {
			ReferencedUserAttribute cAttr = (ReferencedUserAttribute) attr;
			UserAttribute cRAttr = (UserAttribute) rAttr;
			if (!cRAttr.getDataType().equals(cAttr.getReferencedAttribute().getDataType())) {
				addError(id, "Type differs, should be '"
						+ cRAttr.getDataType().getName() + "'");
			}
			if (cRAttr.isIsList() != cAttr.getReferencedAttribute().isIsList()) {
				addError(id, "isList options differs, should be " + rAttr.isIsList());
			}

		} else if (attr instanceof ReferencedBidirectionalAttribute
				&& rAttr instanceof BidirectionalAttribute) {
			ReferencedBidirectionalAttribute cAttr = (ReferencedBidirectionalAttribute) attr;
			BidirectionalAttribute cRAttr = (BidirectionalAttribute) rAttr;
			if (!cRAttr.getDataType().equals(cAttr.getReferencedAttribute().getDataType())) {
				addError(id, "Type differs, should be '"
						+ cRAttr.getDataType().getName() + "'");
			}
			if (cRAttr.isIsList() != cAttr.getReferencedAttribute().isIsList()) {
				addError(id, "isList options differs, should be " + rAttr.isIsList());
			}

		} else {
			addError(id, "Attribute Type differs, should be '"
					+ rAttr.getClass().getSimpleName() + "'");
		}
	}

	private List<Attribute> getBrokenAttributes(ReferencedType rType) {
		ArrayList<Attribute> brokenAttributes = new ArrayList<>();
		for (Attribute attr : rType.getAttributes()) {
			if (attr instanceof ReferencedPrimitiveAttribute) {
				PrimitiveAttribute refAttr = ((ReferencedPrimitiveAttribute) attr)
						.getReferencedAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
			if (attr instanceof ReferencedComplexAttribute) {
				ComplexAttribute refAttr = ((ReferencedComplexAttribute) attr)
						.getReferencedAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
			if (attr instanceof ReferencedBidirectionalAttribute) {
				BidirectionalAttribute refAttr = ((ReferencedBidirectionalAttribute) attr)
						.getReferencedAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
			if (attr instanceof ReferencedUserAttribute) {
				UserAttribute refAttr = ((ReferencedUserAttribute) attr)
						.getReferencedAttribute();
				if (refAttr == null)
					brokenAttributes.add(attr);
			}
		}
		return brokenAttributes;
	}
}
