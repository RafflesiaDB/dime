package info.scce.dime.data.checks;

import info.scce.dime.checks.AbstractModelIntegrityCheck;
import info.scce.dime.data.mcam.adapter.DataAdapter;
import info.scce.dime.data.mcam.adapter.DataId;

public class ModelIntegrityCheck
	   extends AbstractModelIntegrityCheck<DataId, DataAdapter> {
}
