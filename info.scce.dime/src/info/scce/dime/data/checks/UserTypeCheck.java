package info.scce.dime.data.checks;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.data.data.UserType;
import info.scce.dime.data.hooks.UserTypePostCreate;
import info.scce.dime.data.mcam.adapter.DataAdapter;
import info.scce.dime.data.mcam.adapter.DataId;
import info.scce.dime.checks.AbstractCheck;

public class UserTypeCheck extends AbstractCheck<DataId, DataAdapter> {

	@Override
	public void doExecute(DataAdapter adapter) {
		for (DataId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof UserType) {
				boolean hasUsername = false;
				boolean hasPassword = false;
				
				for (Attribute attr : ((UserType) obj).getAttributes()) {
					if (UserTypePostCreate.attrUsernameName.equals(attr.getName()))
							if (attr instanceof PrimitiveAttribute) 
								if (((PrimitiveAttribute) attr).getDataType().equals(PrimitiveType.TEXT))
									hasUsername = true;
					
					if (UserTypePostCreate.attrPasswordName.equals(attr.getName()))
						if (attr instanceof PrimitiveAttribute) 
							if (((PrimitiveAttribute) attr).getDataType().equals(PrimitiveType.TEXT))
								hasPassword = true;
								
				}
				
				if (!hasUsername)
					addError(id, "missing primitive attribute '" + UserTypePostCreate.attrUsernameName + "' (TEXT)");
				if (!hasPassword)
					addError(id, "missing primitive attribute '" + UserTypePostCreate.attrPasswordName + "' (TEXT)");
			}
		}
		
	}

	@Override
	public void init() {}

}
