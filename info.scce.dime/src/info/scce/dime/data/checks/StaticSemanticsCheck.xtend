package info.scce.dime.data.checks

import info.scce.dime.data.data.Data
import info.scce.dime.data.data.Inheritance
import info.scce.dime.data.data.Type
import info.scce.dime.data.mcam.modules.checks.DataCheck

class StaticSemanticsCheck extends DataCheck {
	
	override check(Data model) {
		checkInheritanceCycles(model) 
	}
	
	def checkInheritanceCycles(Data model) {
		val cycles = newHashSet
		model.inheritorTypes.flatMap[ type |
			type.findPathsTo(type)
				.filter[!isEmpty]
				.filter[type.findSuccessorsVia(Inheritance).containsAll(it)]
				.filter[cycles.add(it.toSet)]
				.map[#[type] + it]
		].forEach[ path |
			model.addError('''Inheritance cycle: «path.filter(Type).map[it.name].join(" < ")»''')
		]
	}
}