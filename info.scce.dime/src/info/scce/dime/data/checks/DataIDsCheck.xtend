package info.scce.dime.data.checks

import graphmodel.Node
import info.scce.dime.data.data.Attribute
import info.scce.dime.data.data.Data
import info.scce.dime.data.data.ReferencedEnumType
import info.scce.dime.data.data.ReferencedType
import info.scce.dime.data.data.ReferencedUserType
import info.scce.dime.data.data.Type
import info.scce.dime.data.mcam.modules.checks.DataCheck
import info.scce.dime.util.DataID

class DataIDsCheck extends DataCheck {
	
	override check(Data model) {
		val types = 
			model.find(Type)
				.drop(ReferencedType)
				.drop(ReferencedUserType)
				.drop(ReferencedEnumType)
		
		validateDataIds(types)
		
		val attrs = 
			model.find(Attribute)
				.filter[it.findParents(ReferencedType).isEmpty]
				.filter[it.findParents(ReferencedUserType).isEmpty]
				.filter[it.findParents(ReferencedEnumType).isEmpty]
		
		validateDataIds(attrs)
	}
	
	def validateDataIds(Iterable<? extends Node> nodes) {
		val id16_on_node = <String,Node> newHashMap
		for (node : nodes) {
			val dataId = DataID.from(node)
			val id16 = dataId.escapedLowerCase16
			if (!id16_on_node.containsKey(id16)) {
				id16_on_node.put(id16, node)
			} else {
				val other = id16_on_node.get(id16)
				node.addError(
					'''ID «node.id» conflicts with ID «other.id» of «other.displayName» (both map on 16 chars «id16»)'''
				)
			}
		}
	}
	
	def String displayName(Node node) {
		switch node {
			Type: "Type '" + node.name + "'"
			Attribute: "Attribute '" + node.name + "' of " + node.findFirstParent(Type).displayName
			default: node.class.simpleName
		}
	}
}