package info.scce.dime.data.checks;

import java.util.ArrayList;

import info.scce.dime.api.DIMECheck;
import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.Inheritance;
import info.scce.dime.data.data.PrimitiveAttribute;
import info.scce.dime.data.data.Type;
import info.scce.dime.data.mcam.adapter.DataAdapter;
import info.scce.dime.data.mcam.adapter.DataId;

public class UniqueNamesCheck extends DIMECheck<DataId, DataAdapter> {


	@Override
	public void doExecute(DataAdapter adapter) {
		ArrayList<String> typeNames = new ArrayList<>();
		for (DataId id : adapter.getEntityIds()) {
			Object obj = id.getElement();
			if (obj instanceof Type) {
				Type type = (Type) obj;
				if (typeNames.contains(type.getName()))
					addError(id, type.getName()
							+ " not unique");
				typeNames.add(type.getName());
				
				ArrayList<String> attributeNames = new ArrayList<>();
				for (Attribute attribute : type.getAttributes()) {
					DataId attrId = adapter.getIdByString(attribute.getId());
					if (attributeNames.contains(attribute.getName()))
						addError(attrId, attribute.getName()
								+ " not unique");

					if (existsInSuperClasses(type, attribute))
						addError(attrId, attribute.getName()
								+ " already exists in type hierarchy");
					attributeNames.add(attribute.getName());

				}
			}
		}

	}
	
	public boolean existsInSuperClasses(Type type, Attribute attr) {
		ArrayList<Attribute> superAttrList = new ArrayList<>();
		for (Inheritance edge : type.getOutgoing(Inheritance.class)) {
			if (edge.getTargetElement() instanceof Type) {
				Type superType = (Type) edge.getTargetElement();
				superAttrList.addAll(_dataExtension.getInheritedAttributes(superType));
			}
		}
		if (superAttrList.size() <= 0)
			return false;

		Attribute overwrittenSuperAttr = null;
		if (attr instanceof PrimitiveAttribute)
			overwrittenSuperAttr = ((PrimitiveAttribute) attr).getSuperAttr();
		if (attr instanceof ComplexAttribute)
			overwrittenSuperAttr = ((ComplexAttribute) attr).getSuperAttr();

		for (Attribute superAttr : superAttrList) {
			if (superAttr.getName().equals(attr.getName())) {
				if (overwrittenSuperAttr != null
						&& superAttr.getId().equals(
								overwrittenSuperAttr.getId()))
					continue;
				if (superAttr.equals(attr))
					continue;
				return true;
			}
		}
		return false;
	}
}
