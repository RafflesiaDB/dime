package info.scce.dime.data.checks;

import graphmodel.GraphModel;
import graphmodel.ModelElement;
import info.scce.dime.data.mcam.adapter.DataAdapter;
import info.scce.dime.data.mcam.adapter.DataId;
import info.scce.dime.checks.AbstractCheck;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class LibCompReferenceCheck extends AbstractCheck<DataId, DataAdapter> {

	@Override
	public void doExecute(DataAdapter adapter) {

		EObject eObj = adapter.getModel();

		if (eObj instanceof GraphModel == false)
			return;

		GraphModel gModel = (GraphModel) eObj;

		TreeIterator<EObject> it = gModel.eAllContents();
		while (it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof ModelElement == false)
				continue;
			ModelElement me = (ModelElement) obj;
			EStructuralFeature libCompFeature = me.eClass()
					.getEStructuralFeature("libraryComponentUID");
			if (libCompFeature != null) {
				Object val = me.eGet(libCompFeature);
				if (val == null) {
					addError(adapter.getIdByString(me.getId()), "Reference is NULL");
				}
			}
		}

	}
	
	@Override
	public void init() {}
}