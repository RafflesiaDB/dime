package info.scce.dime.data.aps;

import info.scce.dime.data.data.Association;
import style.Appearance;
import style.StyleFactory;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;

public class AssociationAppearance implements StyleAppearanceProvider<Association>{

	@Override
	public Appearance getAppearance(Association association, String styleElementName) {
		Appearance app = StyleFactory.eINSTANCE.createAppearance();
		if (association.isIsHidden())
			app.setTransparency(1.0);
		else app.setTransparency(0.0);
		
		return app;
	}

}
