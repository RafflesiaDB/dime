package info.scce.dime.data.aps;

import info.scce.dime.data.data.Attribute;
import info.scce.dime.data.data.BidirectionalAttribute;
import info.scce.dime.data.data.ComplexAttribute;
import info.scce.dime.data.data.ExtensionAttribute;
import info.scce.dime.data.data.PrimitiveAttribute;
import style.Appearance;
import style.StyleFactory;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;

public class AttributeAppearance implements StyleAppearanceProvider<Attribute> {

	@Override
	public Appearance getAppearance(Attribute a, String element) {
		
		Appearance appearance = StyleFactory.eINSTANCE.createAppearance();
		

		if ("extensionPort".equals(element)) {
			if (a instanceof ExtensionAttribute) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		if ("inheritanceTriangle".equals(element)) {
			if (a instanceof ComplexAttribute && ((ComplexAttribute)a).getSuperAttr() != null) {
				appearance.setTransparency(0.0);
			}
			else if (a instanceof PrimitiveAttribute && ((PrimitiveAttribute)a).getSuperAttr() != null) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		else 
		if ("upperPort".equals(element)) {
			if (a instanceof ComplexAttribute) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		else 
		if ("lowerPort".equals(element)) {
			if (a instanceof BidirectionalAttribute) {
				appearance.setTransparency(0.0);
			}
			else {
				appearance.setTransparency(1.0);
			}
		}
		
		
		return appearance;
	}

}
	
	
