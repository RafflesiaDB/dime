package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import java.util.List;

public class GenericOutputBranch {

	private String name;
	private List<GenericPort> outputPorts;

	public GenericOutputBranch(String name, List<GenericPort> outputPorts) {
		this.name = name;
		this.outputPorts = outputPorts;
	}

	public String getName() {
		return name;
	}

	public List<GenericPort> getOutputPorts() {
		return outputPorts;
	}
}
