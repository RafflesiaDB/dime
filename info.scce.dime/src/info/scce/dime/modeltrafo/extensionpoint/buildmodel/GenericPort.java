package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

public class GenericPort {
	
	private final String name;
	private boolean isList;
	
	public GenericPort (String name) {
		this.name = name;
		this.isList = false;
	}
	
	public GenericPort (String name, boolean isList) {
		this.name = name;
		this.isList = isList;
	}

	public String getName() {
		return name;
	}

	public boolean isList() {
		return isList;
	}

	public void setList(boolean isList) {
		this.isList = isList;
	}
	
}
