package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import info.scce.dime.siblibrary.Type;

public class GenericJavaNativePort extends GenericPort {
		
	private Type type;
	
	public GenericJavaNativePort(String name) {
		super(name);
	}
	
	public GenericJavaNativePort(String name, boolean isList) {
		super(name, isList);
	}
	
	public GenericJavaNativePort(String name, boolean isList, Type type) {
		super(name, isList);
		this.type = type;
	}
	
	public Type getType() {
		return type;
	}
	
}
