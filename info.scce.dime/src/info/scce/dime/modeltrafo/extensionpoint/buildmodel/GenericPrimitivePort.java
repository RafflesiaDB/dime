package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import info.scce.dime.data.data.PrimitiveType;

public class GenericPrimitivePort extends GenericPort {
	
	private PrimitiveType type;
	
	public GenericPrimitivePort(String name, PrimitiveType type, boolean isList) {
		super(name, isList);
		this.type = type;
	}
	
	public GenericPrimitivePort(String name, PrimitiveType type) {
		super(name);
		this.type = type;
	}

	public PrimitiveType getType() {
		return type;
	}
}
