package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

public interface IGenericSIBBuilder <T extends EObject>{
	
	public default GenericSIBBuild<T> getSIBBuild(T model) {
		return new GenericSIBBuild<T>(model, getInputPorts(model), getOutputBranches(model), getName(model), getLabel(model));
	}
	
	public String getName (T model);
	
	public String getLabel (T model);
	
	public List<GenericPort> getInputPorts(T model);
	
	public List<GenericOutputBranch> getOutputBranches(T model);
}
