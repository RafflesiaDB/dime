package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

public class GenericSIBBuild<T extends EObject> {

	private final List<GenericPort> inputPorts;
	private final List<GenericOutputBranch> outputBranches;
	private final T modelObject;
	private final String name;
	private final String label;

	public GenericSIBBuild(T modelObject, List<GenericPort> inputPorts, List<GenericOutputBranch> outputBranches, String name, String label) {
		this.modelObject = modelObject;
		this.inputPorts = inputPorts;
		this.outputBranches = outputBranches;
		this.name = name;
		this.label = label;
	}

	public List<GenericPort> getInputPorts() {
		return inputPorts;
	}

	public String getName() {
		return name;
	}
	
	public String getLabel() {
		return label;
	}

	public T getModelObject() {
		return modelObject;
	}

	public List<GenericOutputBranch> getOutputBranches() {
		return outputBranches;
	}
}
