package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import info.scce.dime.data.data.Type;

public class GenericComplexPort extends GenericPort {
	
	private Type type;
	
	public GenericComplexPort(String name, Type type, boolean isList) {
		super(name, isList);
		this.type = type;
	}
	
	public GenericComplexPort(String name, Type type) {
		super(name);
		this.type = type;
	}

	public Type getType() {
		return type;
	}
}
