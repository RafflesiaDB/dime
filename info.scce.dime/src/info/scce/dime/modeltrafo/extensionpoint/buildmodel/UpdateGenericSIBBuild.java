package info.scce.dime.modeltrafo.extensionpoint.buildmodel;

import static info.scce.dime.process.helper.PortUtils.toPrimitiveType;
import static info.scce.dime.process.hooks.GenericSIBHook.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;

import info.scce.dime.data.data.Type;
import info.scce.dime.process.build.BranchCreator;
import info.scce.dime.process.process.Branch;
import info.scce.dime.process.process.ComplexInputPort;
import info.scce.dime.process.process.ComplexOutputPort;
import info.scce.dime.process.process.GenericSIB;
import info.scce.dime.process.process.IO;
import info.scce.dime.process.process.InputPort;
import info.scce.dime.process.process.JavaNativeInputPort;
import info.scce.dime.process.process.JavaNativeOutputPort;
import info.scce.dime.process.process.OutputPort;
import info.scce.dime.process.process.PrimitiveInputPort;
import info.scce.dime.process.process.PrimitiveOutputPort;
import info.scce.dime.process.process.PrimitiveType;

public class UpdateGenericSIBBuild {

	private String nameUpdate;
	private GenericSIBBuild<EObject> sibBuild;
	private GenericSIB sib;
	private String labelUpdate;
	private boolean delete;
	private Map<InputPort, GenericPort> inputPortsToUpdate = new HashMap<InputPort, GenericPort>();
	private List<GenericPort> inputPortsToAdd = new ArrayList<GenericPort>();
	private List<InputPort> inputPortsToDelete = new ArrayList<InputPort>();
	private Map<Branch, GenericOutputBranch> outputBranchesToUpdate = new HashMap<Branch, GenericOutputBranch>();
	private List<GenericOutputBranch> outputBranchesToAdd = new ArrayList<GenericOutputBranch>();
	private List<Branch> outputBranchesToDelete = new ArrayList<Branch>();
	private BranchCreator branchCreator;

	public UpdateGenericSIBBuild(GenericSIBBuild<EObject> sibBuild, GenericSIB sib, BranchCreator branchCreator) {
		this.branchCreator = branchCreator;
		this.sibBuild = sibBuild;
		this.sib = sib;
		this.delete = false;
		initialize();
	}

	public boolean needsUpdate() {
		return needsNameUpdate() || needsLabelUpdate() || needsInputPortsUpdate() || needsOutputBranchesUpdate()
				|| delete;
	}

	protected boolean needsNameUpdate() {
		return nameUpdate != null && !nameUpdate.isEmpty();
	}

	protected boolean needsLabelUpdate() {
		return labelUpdate != null && !labelUpdate.isEmpty();
	}

	protected boolean needsInputPortsUpdate() {
		return !inputPortsToUpdate.isEmpty() || !inputPortsToAdd.isEmpty() || !inputPortsToDelete.isEmpty();
	}

	protected boolean needsOutputBranchesUpdate() {
		return !outputBranchesToUpdate.isEmpty() || !outputBranchesToAdd.isEmpty() || !outputBranchesToDelete.isEmpty();
	}

	protected void initialize() {
		if (sibBuild != null) {
			if (!sibBuild.getName().equals(sib.getName())) {
				nameUpdate = sibBuild.getName();
			}
			if (!sibBuild.getLabel().equals(sib.getLabel())) {
				labelUpdate = sibBuild.getLabel();
			}
			List<IO> iosToDelete = new ArrayList<IO>();
			Map<IO, GenericPort> iosToUpdate = new HashMap<IO, GenericPort>();
			initializePorts(sibBuild.getInputPorts(), getInputPortsAsIO(sib.getInputPorts()), iosToUpdate,
					inputPortsToAdd, iosToDelete);
			updateInputPortsToDelete(iosToDelete);
			updateInputPortToUpdate(iosToUpdate);
			initializeBranches(sib.getBranchSuccessors(), sibBuild.getOutputBranches());
		} else {			
			delete = true;
		}
	}

	protected void initializePorts(List<GenericPort> genericPorts, List<IO> ports, Map<IO, GenericPort> portsToUpdate,
			List<GenericPort> portsToAdd, List<IO> portsToDelete) {
		for (GenericPort genericPort : genericPorts) {
			boolean matched = false;
			for (IO port : ports) {
				if (genericPort.getName().equals(getName(port))) {
					matched = true;
					if (genericPort.isList() != isList(port)) {
						portsToUpdate.put(port, genericPort);
					} else if ((genericPort instanceof GenericPrimitivePort && !isPrimitive(port))
							|| (genericPort instanceof GenericComplexPort && !isComplexType(port))
							|| (genericPort instanceof GenericJavaNativePort && !isJavaNative(port))) {
						matched = false;
						portsToAdd.add(genericPort);
						portsToDelete.add(port);
					} else if (genericPort instanceof GenericPrimitivePort && isPrimitive(port)) {
						GenericPrimitivePort genericPrimitiveInputPort = (GenericPrimitivePort) genericPort;
						if (getPrimitiveType(port) != toPrimitiveType(genericPrimitiveInputPort.getType())) {
							portsToUpdate.put(port, genericPort);
						}
					} else if (genericPort instanceof GenericComplexPort && isComplexType(port)) {
						GenericComplexPort genericComplexInputPort = (GenericComplexPort) genericPort;
						if (getComplexType(port) != genericComplexInputPort.getType()) {
							portsToUpdate.put(port, genericPort);
						}
					} else if (genericPort instanceof GenericJavaNativePort && isJavaNative(port)) {
						GenericJavaNativePort javaNativeInputPort = (GenericJavaNativePort) genericPort;
						if (getJavaNativeType(port) != javaNativeInputPort.getType()) {
							portsToUpdate.put(port, genericPort);
						}
					}
				}
			}
			if (!matched) {
				portsToAdd.add(genericPort);
			}
		}
		for (IO port : ports) {
			boolean matched = false;
			if (!portsToDelete.contains(port)) {
				for (GenericPort genericInputPort : genericPorts) {
					if (genericInputPort.getName().equals(getName(port))) {
						matched = true;
					}
				}
				if (!matched) {
					portsToDelete.add(port);
				}
			}
		}
	}

	protected void initializeBranches(List<Branch> outputBranches, List<GenericOutputBranch> genericOutputBranches) {
		for (GenericOutputBranch genericOutputBranch : genericOutputBranches) {
			boolean matched = false;
			for (Branch outputBranch : outputBranches) {
				if (outputBranch.getName().equals(genericOutputBranch.getName())) {
					matched = true;
					Map<IO, GenericPort> outputPortsToUpdate = new HashMap<IO, GenericPort>();
					List<GenericPort> outputPortsToAdd = new ArrayList<GenericPort>();
					List<IO> outputPortsToDelete = new ArrayList<IO>();
					initializePorts(genericOutputBranch.getOutputPorts(),
							getOutportPortsAsIO(outputBranch.getOutputPorts()), outputPortsToUpdate, outputPortsToAdd,
							outputPortsToDelete);
					if (!outputPortsToUpdate.isEmpty() || !outputPortsToAdd.isEmpty()
							|| !outputPortsToDelete.isEmpty()) {
						outputBranchesToUpdate.put(outputBranch, genericOutputBranch);
					}
				}
			}
			if (!matched) {
				outputBranchesToAdd.add(genericOutputBranch);
			}
		}
		for (Branch outputBranch : outputBranches) {
			boolean matched = false;
			for (GenericOutputBranch genericOutputBranch : genericOutputBranches) {
				if (outputBranch.getName().equals(genericOutputBranch.getName())) {
					matched = true;
				}
			}
			if (!matched) {
				outputBranchesToDelete.add(outputBranch);
			}
		}
	}

	public void update() {
		if (needsUpdate()) {
			if (delete) {
				sib.delete();
			} else {
				if (needsNameUpdate()) {
					sib.setName(nameUpdate);
				}
				if (needsLabelUpdate()) {
					sib.setLabel(labelUpdate);
				}
				if (needsInputPortsUpdate()) {
					for (InputPort port : inputPortsToDelete) {
						port.delete();
					}
					createInputPorts(inputPortsToAdd, sib);
					for (Entry<InputPort, GenericPort> entry : inputPortsToUpdate.entrySet()) {
						InputPort inputPort = entry.getKey();
						GenericPort genericInputPort = entry.getValue();
						if (inputPort instanceof PrimitiveInputPort) {
							((PrimitiveInputPort) inputPort)
									.setDataType(toPrimitiveType(((GenericPrimitivePort) genericInputPort).getType()));
						} else if (inputPort instanceof ComplexInputPort) {
							inputPort.delete();
							createComplexInputPort(genericInputPort, sib);
						} else if (inputPort instanceof JavaNativeInputPort) {
							inputPort.delete();
							createJavaNativeInputPort(genericInputPort, sib);
						}
					}
				}
				if (needsOutputBranchesUpdate()) {
					for (Branch outputBranch : outputBranchesToDelete) {
						outputBranch.delete();
					}
					for (GenericOutputBranch branchToAdd : outputBranchesToAdd) {
						Branch newBranch = branchCreator.getNewBranch();
						newBranch.setName(branchToAdd.getName());
						createOutputPorts(branchToAdd.getOutputPorts(), newBranch);
					}
					for (Entry<Branch, GenericOutputBranch> entry : outputBranchesToUpdate.entrySet()) {
						Branch outputBranch = entry.getKey();
						GenericOutputBranch genericOutputBranch = entry.getValue();
						Map<IO, GenericPort> outputPortsToUpdate = new HashMap<IO, GenericPort>();
						List<GenericPort> outputPortsToAdd = new ArrayList<GenericPort>();
						List<IO> outputPortsToDelete = new ArrayList<IO>();
						initializePorts(genericOutputBranch.getOutputPorts(),
								getOutportPortsAsIO(outputBranch.getOutputPorts()), outputPortsToUpdate, outputPortsToAdd,
								outputPortsToDelete);
						List<OutputPort> portsToDelete = getOutputPortsToDelete(outputPortsToDelete);
						Map<OutputPort, GenericPort> portsToUpdate = getOutputPortsToUpdate(outputPortsToUpdate);
						for (OutputPort port : portsToDelete) {
							port.delete();
						}
						createOutputPorts(outputPortsToAdd, outputBranch);
						for (Entry<OutputPort, GenericPort> updateEntry : portsToUpdate.entrySet()) {
							OutputPort outputPort = updateEntry.getKey();
							GenericPort genericOutputPort = updateEntry.getValue();
							if (outputPort instanceof PrimitiveOutputPort) {
								((PrimitiveOutputPort) outputPort)
										.setDataType(toPrimitiveType(((GenericPrimitivePort) genericOutputPort).getType()));
							} else if (outputPort instanceof ComplexOutputPort) {
								outputPort.delete();
								createComplexOutputPort(genericOutputPort, outputBranch);
							} else if (outputPort instanceof JavaNativeOutputPort) {
								outputPort.delete();
								createJavaNativeOutputPort(genericOutputPort, outputBranch);
							}
						}
					}
				}
			}
		}
	}

	protected void updateInputPortsToDelete(List<IO> ios) {
		for (IO io : ios) {
			if (io instanceof InputPort) {
				inputPortsToDelete.add((InputPort) io);
			}
		}
	}

	protected void updateInputPortToUpdate(Map<IO, GenericPort> iosToUpdate) {
		for (IO io : iosToUpdate.keySet()) {
			if (io instanceof InputPort) {
				inputPortsToUpdate.put((InputPort) io, iosToUpdate.get(io));
			}
		}
	}

	protected List<OutputPort> getOutputPortsToDelete(List<IO> ios) {
		List<OutputPort> result = new ArrayList<OutputPort>();
		for (IO io : ios) {
			if (io instanceof OutputPort) {
				result.add((OutputPort) io);
			}
		}
		return result;
	}

	protected Map<OutputPort, GenericPort> getOutputPortsToUpdate(Map<IO, GenericPort> iosToUpdate) {
		Map<OutputPort, GenericPort> outputPortsToUpdate = new HashMap<OutputPort, GenericPort>();
		for (IO io : iosToUpdate.keySet()) {
			if (io instanceof OutputPort) {
				outputPortsToUpdate.put((OutputPort) io, iosToUpdate.get(io));
			}
		}
		return outputPortsToUpdate;
	}

	protected List<IO> getInputPortsAsIO(List<InputPort> inputPorts) {
		List<IO> result = new ArrayList<IO>();
		for (InputPort port : inputPorts) {
			result.add(port);
		}
		return result;
	}

	protected List<IO> getOutportPortsAsIO(List<OutputPort> outputPorts) {
		List<IO> result = new ArrayList<IO>();
		for (OutputPort port : outputPorts) {
			result.add(port);
		}
		return result;
	}

	protected String getName(IO port) {
		if (port instanceof InputPort) {
			return ((InputPort) port).getName();
		} else if (port instanceof OutputPort) {
			return ((OutputPort) port).getName();
		}
		return null;
	}

	protected boolean isPrimitive(IO port) {
		return (port instanceof PrimitiveInputPort) || (port instanceof PrimitiveOutputPort);
	}

	protected PrimitiveType getPrimitiveType(IO port) {
		if (port instanceof PrimitiveInputPort) {
			return ((PrimitiveInputPort) port).getDataType();
		} else if (port instanceof PrimitiveOutputPort) {
			return ((PrimitiveOutputPort) port).getDataType();
		}
		return null;
	}

	protected boolean isComplexType(IO port) {
		return (port instanceof ComplexInputPort) || (port instanceof ComplexOutputPort);
	}

	protected Type getComplexType(IO port) {
		if (port instanceof ComplexInputPort) {
			return ((ComplexInputPort) port).getDataType();
		} else if (port instanceof ComplexOutputPort) {
			return ((ComplexOutputPort) port).getDataType();
		}
		return null;
	}
	
	protected boolean isJavaNative(IO port) {
		return (port instanceof JavaNativeInputPort) || (port instanceof JavaNativeOutputPort);
	}
	
	protected info.scce.dime.siblibrary.Type getJavaNativeType(IO port) {
		if (port instanceof JavaNativeInputPort) {
			return (info.scce.dime.siblibrary.Type) ((JavaNativeInputPort) port).getDataType();
		} else if (port instanceof JavaNativeOutputPort) {
			return (info.scce.dime.siblibrary.Type) ((JavaNativeOutputPort) port).getDataType();
		}
		return null;
	}

	protected boolean isList(IO port) {
		if (port instanceof InputPort) {
			return ((InputPort) port).isIsList();
		} else if (port instanceof OutputPort) {
			return ((OutputPort) port).isIsList();
		}
		return false;
	}
}
