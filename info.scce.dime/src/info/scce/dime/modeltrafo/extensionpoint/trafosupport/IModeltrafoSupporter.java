package info.scce.dime.modeltrafo.extensionpoint.trafosupport;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.transformation.ISIBGenerationProvider;

public interface IModeltrafoSupporter<T extends EObject> {
	
	public String getModelExtension();
	
	public String getSIBName();
	
	public String getIconPath();
	
	public IGenericSIBBuilder<T> getModellingProvider();
	
	public EClass getModelType();
	
	public ISIBGenerationProvider getGenerationProvider();
}
