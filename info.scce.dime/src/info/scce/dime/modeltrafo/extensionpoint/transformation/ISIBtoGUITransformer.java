package info.scce.dime.modeltrafo.extensionpoint.transformation;

import org.eclipse.emf.ecore.EObject;

import info.scce.dime.gui.gui.GUI;

public interface ISIBtoGUITransformer <T extends EObject> extends IGuiSIBGenerationProvider{
	
	public GUI transform (T referencedSIBModel);
	
}
