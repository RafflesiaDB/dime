package info.scce.dime.modeltrafo.extensionpoint.transformation;

import org.eclipse.emf.ecore.EObject;

import info.scce.dime.gui.gui.GUI;
import info.scce.dime.modeltrafo.extensionpoint.ModeltrafoExtensionProvider;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.process.process.Process;
import info.scce.dime.process.process.SIB;

public class GenericSIBGenerationProvider {
	
	public static <T extends EObject> boolean isGUI(T referencedSIBModel) {
		IModeltrafoSupporter<T> modelTrafoSupporter = ModeltrafoExtensionProvider.getSupportedModelElement(referencedSIBModel);
		return modelTrafoSupporter.getGenerationProvider() instanceof IGuiSIBGenerationProvider;
	}
	
	public static <T extends EObject> boolean isTransformable(T referencedSIBModel) {
		ISIBGenerationProvider generationProvider = getGenerationProvider(referencedSIBModel);
		if (generationProvider != null) {			
			return (generationProvider instanceof ISIBtoGUITransformer) || (generationProvider instanceof ISIBtoProcessTransformer);
		}
		return false;
	}

	public static <T extends EObject> SIB getConcreteSIBForGenericSIB(T referencedSIBModel) {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> Process getTransformedProcess(T referencedSIBModel) {
		ISIBGenerationProvider generationProvider = getGenerationProvider(referencedSIBModel);
		if (isTransformable(referencedSIBModel) && !isGUI(referencedSIBModel)) {
			ISIBtoProcessTransformer<T> transformer = (ISIBtoProcessTransformer<T>) generationProvider;
			return transformer.transform(referencedSIBModel);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> GUI getTransformedGUI(T referencedSIBModel) {
		ISIBGenerationProvider generationProvider = getGenerationProvider(referencedSIBModel);
		if (isTransformable(referencedSIBModel) && isGUI(referencedSIBModel)) {
			ISIBtoGUITransformer<T> transformer = (ISIBtoGUITransformer<T>) generationProvider;
			return transformer.transform(referencedSIBModel);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> void generateContent (T referencedSIBModel) {
		if (!isTransformable(referencedSIBModel)) {
			IGenericSIBGenerator<T> generator = (IGenericSIBGenerator<T>) getGenerationProvider(referencedSIBModel);
			generator.generateContent(referencedSIBModel);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> String getMethodCallSignatur(T referencedSIBModel) {
		if (!isTransformable(referencedSIBModel)) {
			IGenericSIBGenerator<T> generator = (IGenericSIBGenerator<T>) getGenerationProvider(referencedSIBModel);
			return generator.getMethodCallSignature(referencedSIBModel);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> String getResultTypeName(T referencedSIBModel) {
		if (!isTransformable(referencedSIBModel)) {
			IGenericSIBGenerator<T> generator = (IGenericSIBGenerator<T>) getGenerationProvider(referencedSIBModel);
			return generator.getResultTypeName(referencedSIBModel);
		}
		return null;
	}
	
	private static <T extends EObject> ISIBGenerationProvider getGenerationProvider (T referencedSIBModel) {
		IModeltrafoSupporter<T> modelTrafoSupporter = ModeltrafoExtensionProvider.getSupportedModelElement(referencedSIBModel);
		return modelTrafoSupporter.getGenerationProvider();
	}
}
