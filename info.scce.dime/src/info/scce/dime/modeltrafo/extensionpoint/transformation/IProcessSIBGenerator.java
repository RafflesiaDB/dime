package info.scce.dime.modeltrafo.extensionpoint.transformation;

import org.eclipse.emf.ecore.EObject;

public interface IProcessSIBGenerator<T extends EObject> extends IProcessSIBGenerationProvider, IGenericSIBGenerator<T> {

}
