package info.scce.dime.modeltrafo.extensionpoint.transformation;

import org.eclipse.emf.ecore.EObject;

public interface IGenericSIBGenerator <T extends EObject>{

	public void generateContent (T referencedSIBModel);
	
	public String getMethodCallSignature (T referencedSIBModel);
	
	public String getResultTypeName(T referencedSIBModel);
	
}
