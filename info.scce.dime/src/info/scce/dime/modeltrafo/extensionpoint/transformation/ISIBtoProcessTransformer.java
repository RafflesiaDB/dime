package info.scce.dime.modeltrafo.extensionpoint.transformation;

import org.eclipse.emf.ecore.EObject;

import info.scce.dime.process.process.Process;

public interface ISIBtoProcessTransformer <T extends EObject> extends IProcessSIBGenerationProvider{
	
	public Process transform(T referencedSIBModel);
	
}
