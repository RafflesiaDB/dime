package info.scce.dime.modeltrafo.extensionpoint;

public interface IModeltrafoExtensionConstants {
	
	public final String EXTENSIONPOINT_ID = "info.scce.dime.modeltrafo";
	public final String MODELTRAFO = "modeltrafo";
	public final String MODELTRAFO_MODELTRAFOSUPPORTER = "modeltrafosupporter";
}
