package info.scce.dime.modeltrafo.extensionpoint;

import java.util.List;

import de.jabc.cinco.meta.core.referenceregistry.implementing.IFileExtensionSupplier;

public class AdditionalFileExtensions implements IFileExtensionSupplier{

	@Override
	public List<String> getKnownFileExtensions() {
		return ModeltrafoExtensionProvider.getAllSupportedExtensions();
	}

}
